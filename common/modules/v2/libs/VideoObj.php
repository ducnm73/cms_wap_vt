<?php

namespace common\modules\v2\libs;

use common\helpers\Utils;
use common\models\VtVideoBase;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\models\LanguageConvert;

class VideoObj
{

    public static function serialize($id, $query, $cache = false, $name = null, $type = null, $durationPercent = [], $utm = [])
    {
        $appId = Yii::$app->id;
        $key = $id . "_" . $appId . "_" . $query->limit . "_" . $query->offset;

        if ($cache && Yii::$app->params['cache.enabled']) {
            $contents = Yii::$app->cache->getOrSet($key, function() use($query) {
                return $query->all();
            }, MobiTVRedisCache::CACHE_10MINUTE);
        } else {
           $contents = $query->all();
        }

        // translate $contents
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;
        if($currentLanguage!=$mainLanguage){
            $convertContent=[];
            foreach ($contents as $content){
                $multilang = $content['multilang'];
                if (is_null($multilang)) {
                    $convertContent[]=$content;
                }
                else{
                    $childPart1=LanguageConvert::arrayExclude($content,['name', 'description', 'tag', 'seo_title', 'seo_description', 'seo_keywords']);
                    $childPart2 = LanguageConvert::convertFieldsToArray($multilang,
                        ['name', 'description', 'tag', 'seo_title', 'seo_description', 'seo_keywords'],$currentLanguage, $content);
                    $convertContent[]=array_merge($childPart1, $childPart2);
                }
            }
            $contents = $convertContent;
        }

        $videos = array();
        foreach ($contents as $content) {
            $video = array();
            $video['id'] = $content['id'];
            if ($id == Obj::VIDEO_SEARCH) {
                $video['name'] = Utils::truncateWords($content['name'], 46);
            } else {
                $video['name'] = Utils::truncateWords($content['name'], 70);
            }
            $video['fullName'] = $content['name'];

            $video['description'] = $content['description'];

            if ($appId == 'app-web') {
                $video['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO_WEB_HOME);
                if ($id == Obj::VIDEO_HOT_2) {
                    $video['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO_WEB_HOME, true, false);
                }
                $video['animationImage'] = VtHelper::getThumbAnimationImg($content['file_bucket'], $content['file_path'], VtHelper::SIZE_VIDEO_WEB_HOME);
            } else {
                $video['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO);
                if ($id == Obj::HOME_VIDEO_V2 || $id == Obj::VIDEO_HOT) {
                    $video['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO, true, false);
                }

//                if ($video['id'] >= 2136970) {
//                    $video['animationImage'] = VtHelper::getThumbAnimationImg($content['file_bucket'], $content['file_path'], VtHelper::SIZE_VIDEO);
//                }
            }

            $video['type'] = 'VOD';
            $video['duration'] = Utils::durationToStr($content['duration']);
            $video['duration_origin'] = $content['duration'];
            $video['tag'] = $content['tag'];
            $video['play_times'] = Utils::convertPlayTimes($content['play_times']);
            $video['publishedTime'] = Utils::time_elapsed_string($content['published_time']);
            $video['status'] = $content['status'];
            $video['convert_status'] = $content['convert_status'];
            $video['reason'] = $content['reason'];
            $video['price_play'] = $content['price_play'];
            $video['link'] = Yii::$app->params['app.domain'] . "/video/" . $content['id'] . "/" . $content['slug'] . "?utm_source=APPSHARE";
            $video['can_comment'] = $content['can_comment'];

            $video['linkSocial'] = Yii::$app->params['app.domain'] . "/video/" . $content['id'] . "/" . $content['slug'] . "?utm_source=SOCIAL";

            if (!empty($durationPercent)) {
                $video['durationPercent'] = $durationPercent[$content['id']];
            }

            if (isset($content['user_bucket'])) {
                $video['userAvatarImage'] = VtHelper::getThumbUrl($content['user_bucket'], $content['user_path'], VtHelper::SIZE_AVATAR);
            } else {
//                $avatarImages = Yii::$app->params['random.avatar.channel'];
//                $randImage = $avatarImages[rand(0, count($avatarImages) - 1)];
//                $content["user_bucket"] = $randImage["bucket"];
//                $content["user_path"] = $randImage["path"];
                $video['userAvatarImage'] = VtHelper::getThumbUrl($content['user_bucket'], $content['user_path'], VtHelper::SIZE_AVATAR);
            }

            if (isset($content['full_name']) || isset($content['msisdn'])) {
                $video['userName'] = Utils::truncateWords(($content['full_name']) ? $content['full_name'] : Utils::hideMsisdnLast($content['msisdn']), 15);
                $video['fullUserName'] = $content['full_name'] ? $content['full_name'] : Utils::hideMsisdnLast($content['msisdn']);
                $video['msisdn'] = substr($content['msisdn'], 0, -3) . "xxx";
            }

            if (isset($content['user_id'])) {
                $video['userId'] = $content['user_id'];
            }

            if (isset($content['channel_id'])) {
                $video['channel_id'] = $content['channel_id'];
            }


            // them truong du lieu neu la WAP
            if ($appId == 'app-wap' || $appId == 'app-web') {
                $video['slug'] = $content['slug'];
            }

            if (isset($utm['click_source'])) {
                $video['click_source'] = $utm['click_source'];
            } else {
                $video['click_source'] = 'default';
            }

            if (isset($utm['click_medium'])) {
                $video['click_medium'] = $utm['click_medium'];
            } else {
                $video['click_medium'] = $id;
            }

            //Neu video bi tu choi status =3 and co feedback
            if ($content['status'] == VtVideoBase::STATUS_DELETE && isset($content['feedback_status'])) {
                $video['feedback_status'] = $content['feedback_status'];
                $video['feedback_reject_reason'] = $content['feedback_reject_reason'];
            }


            $videos[] = $video;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'type' => isset($type) ? $type : 'VOD',
            'content' => $videos
        ];
    }

}
