<?php

namespace common\modules\v2\libs;

use common\libs\VtHelper;
use common\models\VtUserBase;
use Yii;
use common\helpers\MobiTVRedisCache;

class MenuObj
{

    public static function serialize($id, $query, $cache = false, $userId, $limit)
    {
        $key = $id . "_" . $userId;
        //chi cache khi khong lay lich su xem cua khach hang
        if ($cache && \Yii::$app->params['cache.enabled']) {
            $channels = \Yii::$app->cache->get($key);
            if ($channels === false) {
                $channels = $query->all();
                $totalChannel = count($channels);
                if($totalChannel < $limit){
                    $hotChannels = VtUserBase::getHotUser($limit - $totalChannel);
                    $channels = array_merge($channels, $hotChannels);
                }

                \Yii::$app->cache->set($key, $channels, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $channels = $query->all();
            $totalChannel = count($channels);
            if($totalChannel < $limit){
                $hotChannels = VtUserBase::getHotUser($limit - $totalChannel);
                $channels = array_merge($channels, $hotChannels);
            }
        }


        $menus = array();
        foreach ($channels as $content) {
            $menu = array();
            $menu['id'] = $content['id'];
            $menu['name'] = $content['full_name'];
            $menu['avatarImage'] = VtHelper::getThumbUrl($menu['bucket'], $menu['path'], VtHelper::SIZE_AVATAR);
            $menus[] = $menu;
        }

        return [
            'id' => $key,
            'name' => '',
            'type' => 'MENU',
            'content' => $menus
        ];

    }
}
