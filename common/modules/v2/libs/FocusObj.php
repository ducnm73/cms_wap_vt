<?php

namespace common\modules\v2\libs;

use common\modules\v2\libs\Obj;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use yii\helpers\Url;

class FocusObj
{

    public static function serialize($id, $query, $focusTopicQuery, $cache = false, $name = null)
    {
        $appId= Yii::$app->id;
        $key = $id."_".$appId;
        //
        if ($cache && Yii::$app->params['cache.enabled']) {
            $contents = Yii::$app->cache->get($key.'_user');
            if ($contents === false) {
                $contents = $focusTopicQuery->all();
                Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $focusTopicQuery->all();
        }
        $focusContents = array();
        foreach ($contents as $content) {
            $topic = array();
            $topic['id'] = $content['id'];
            $topic['name'] = ($content['full_name']) ? $content['full_name'] : $content['msisdn'];
            $topic['avatarImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_AVATAR);
            $topic['type'] = 'PROFILE';
            // them truong du lieu neu la WAP
            if($appId=='app-wap'){
                $topic['link'] = Url::to(['account/view-profile', 'id'=>$content['id']]);
            }
            $focusContents[] = $topic;
        }

        if ($cache && Yii::$app->params['cache.enabled']) {
            $contents = Yii::$app->cache->get($key.'_topic');
            if ($contents === false) {
                $contents = $query->all();
                Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }
        // Lay focus theo TOPIC

        foreach ($contents as $content) {
            $topic = array();
            $topic['id'] = $content['id'];
            $topic['name'] = $content['name'];
            $topic['avatarImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_AVATAR);
            $topic['type'] = 'TOPIC';
            // them truong du lieu neu la WAP
            if($appId=='app-wap'){
                $topic['slug'] = $content['slug'];
                $topic['link'] = Url::to(['default/load-more','id'=>Obj::TOPIC_GROUP.$content['id'], 'slug'=>$content['slug']] );
            }
            $focusContents[] = $topic;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'type' => 'FOCUS',
            'content' => $focusContents
        ];

    }

}
