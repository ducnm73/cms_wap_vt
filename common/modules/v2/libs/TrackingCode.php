<?php

namespace common\modules\v2\libs;

use Yii;

class TrackingCode
{
    const view_home_page = 'view_home_page';
    const view_trending_page = 'view_trending_page';
    const view_channel_home = 'view_channel_home';
    const view_channel_videos = 'view_channel_videos';
    const view_channel_intro = 'view_channel_intro';
    const view_video_detail = 'view_video_detail';
    const view_search_result = 'view_search_result';
    const view_history = 'view_history';
    const view_follow_channel = 'view_follow_channel';
    const view_watch_later = 'view_watch_later';
    const view_playlist_detail = 'view_playlist_detail';
    const view_my_videos = 'view_my_videos';
    const view_list_channel = 'view_list_channel';
    const search_action = 'search_action';
    const goi_cuoc_register_action = 'goi_cuoc_register_action';
    const auto_play_action = 'auto_play_action';
    const upload_action = 'upload_action';
    const video_click_action = 'video_click_action';
    const channel_click_action = 'channel_click_action';
    const logout_action = 'logout_action';
    const follow_channel_action = 'follow_channel_action';
    const like_video_action = 'like_video_action';
    const share_video_action = 'share_video_action';
    const report_video_action = 'report_video_action';
    const comment_video_action = 'comment_video_action';
    const add_playlist_action = 'add_playlist_action';
    const video_tracking = 'video_tracking';
    const video_play_next_action = 'video_play_next_action';
    const video_pause_action = 'video_pause_action';
    const remove_video_from_playlist = 'remove_video_from_playlist';
    const delete_playlist_action = 'delete_playlist_action';
    const close_video_action = 'close_video_action';
    const follow_video_action = 'follow_video_action';
    const view_recommend_page = 'view_recommend_page';
    const tracking_event = 'tracking_event';




}
