<?php

namespace common\modules\v2\libs;

use common\helpers\Utils;
use common\models\VtVideoBase;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\modules\v2\libs\Obj;

class UserPlaylistVideoObj
{

    public static function serialize($id, $query, $cache = false, $name = null)
    {
        $appId = Yii::$app->id;

        $key = $id . "_" . $appId . "_" . $query->limit . "_" . $query->offset;

        if ($cache && \Yii::$app->params['cache.enabled']) {
            $contents = \Yii::$app->cache->get($key);

            if ($contents === false) {
                $contents = $query->all();
                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $result = array();

        foreach ($contents as $content) {
            $item = array();
            $item['id'] = $content['id'];
            $item['name'] = Utils::truncateWords($content['name'], 80);
            $item['description'] = $content['description'];
            $item['num_video'] = $content['num_video'];
            $item['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO);
            $item['type'] = "USER_PLAYLIST";

            //Add box video for TV
            $arrVideo = VtVideoBase::getVideosByPlaylist($content['id'], 8, 0, VtVideoBase::TYPE_VOD)->all();
            $dataVideo = [];
            foreach ($arrVideo as $tVideo){
                $video = [];
                $video['id'] = $tVideo['id'];
                $video['name'] = $tVideo['name'];
                $video['description'] = $tVideo['description'];
                $video['coverImage'] = VtHelper::getThumbUrl($tVideo['bucket'], $tVideo['path'], VtHelper::SIZE_VIDEO);
                $video['type'] = 'VOD';
                $video['duration'] = Utils::durationToStr($tVideo['duration']);
                $video['play_times'] =  Utils::convertPlayTimes($tVideo['play_times']);
                $video['publishedTime'] = Utils::time_elapsed_string($tVideo['published_time']);

                if (isset($tVideo['user_bucket']) && isset($tVideo['user_bucket'])) {
                    $video['userAvatarImage'] = VtHelper::getThumbUrl($tVideo['user_bucket'], $tVideo['user_path'], VtHelper::SIZE_AVATAR);
                }

                if (isset($tVideo['full_name']) || isset($tVideo['msisdn'])) {
                    $video['userName'] = ($tVideo['full_name']) ? $tVideo['full_name'] : $tVideo['msisdn'];
                }
                if (isset($tVideo['user_id'])) {
                    $video['userId'] = $tVideo['user_id'];
                }
                $dataVideo[] = $video;
            }

            $item['videos']= $dataVideo;

            $result[] = $item;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'type' => "USER_PLAYLIST",
            'content' => $result
        ];


    }
}