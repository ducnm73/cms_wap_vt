<?php

namespace common\modules\v2\libs;

use yii\helpers\Url;
use Yii;

class RecommendObj
{
    public static function serialize($id, $userInfo, $content)
    {
        $defaultContent = [
            'current_url' => Url::current(),
            'referrer_url' => Yii::$app->request->referrer,
            'local_time' => date('Y/m/d : H/i/s'),
            'show_recommendation' => null,
        ];

        return [
            'id' => $id,
            'type' => 'RECOMMEND',
            'content' => array_merge($userInfo, $defaultContent, $content)
        ];

    }

}
