<?php

namespace common\modules\v2\libs;

use common\helpers\Utils;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\modules\v2\libs\Obj;

class UserPlaylistObj
{

    public static function serialize($id, $query, $cache = false, $name = null)
    {
        $appId = Yii::$app->id;

        $key = $id . "_" . $appId . "_" . $query->limit . "_" . $query->offset;

        if ($cache && \Yii::$app->params['cache.enabled']) {
            $contents = \Yii::$app->cache->get($key);

            if ($contents === false) {
                $contents = $query->all();
                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $result = array();

        foreach ($contents as $content) {
            $item = array();
            $item['id'] = $content['id'];
            $item['name'] = Utils::truncateWords($content['name'], 255);
            $item['description'] = $content['description'];
            $item['num_video'] = $content['num_video'];
            $item['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO);
            $item['type'] = "USER_PLAYLIST";

            //Add box video for TV


            $result[] = $item;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'type' => "USER_PLAYLIST",
            'content' => $result
        ];


    }
}