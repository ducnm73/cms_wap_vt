<?php

namespace common\modules\v2\libs;

use common\models\VtVideoBase;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\modules\v2\libs\Obj;

class UserObj
{

    public static function serialize($id, $query, $cache = false, $name = null)
    {
        $appId = Yii::$app->id;
        $key = $id . "_" . $appId."_".$query->limit."_".$query->offset;

        if ($cache && \Yii::$app->params['cache.enabled']) {
            $contents = \Yii::$app->cache->get($key);

            if ($contents === false) {
                $contents = $query->all();
                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $result = array();
        foreach ($contents as $content) {
            $item = array();
            $item['id'] = $content['id'];
            $item['name'] = ($content['full_name']) ? $content['full_name'] : (substr($content['msisdn'],0,-3)."xxx");
            $item['avatarImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_AVATAR);
            $item['followCount'] = $content['follow_count'];
            $item['videoCount'] = $content['video_count'];
            $item['isFollow'] = ($content['follow_id']) ? 1 : 0;
            $result[] = $item;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'content' => $result,
            'type' => 'USER'
        ];


    }
}