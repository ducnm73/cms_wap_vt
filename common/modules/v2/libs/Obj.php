<?php

namespace common\modules\v2\libs;

use Yii;

class Obj {

    const CATEGORY_GROUP = 'category_group_';
    const TOPIC_GROUP = 'topic_group_';
    const VIDEO_OF_USER = 'video_of_user_';
    const BANNER = 'banner';
    const FOCUS = 'focus';
    const VIDEO_HOME = 'video_home';
    const VIDEO_SEARCH = 'video_search';
    const VIDEO_NEWSFEED = 'video_newsfeed';
    const VIDEO_NEW = 'video_new';
    const VIDEO_HOT = 'video_hot';
    const VIDEO_HOT_2 = 'video_hot_2';
    const VIDEO_HISTORY = 'video_history';
    const VIDEO_RECOMMEND = 'video_recommend';
    const VIDEO_FREE = 'video_free';
    const VIDEO_RELATE = 'video_relate';
    const VIDEO_MOST_RELATE = 'video_most_relate';
    const VIDEO_FOLLOW = 'video_follow';
    const VIDEO_OWNER = 'video_owner';
    const VIDEO_WATCH_LATER = 'video_watch_later';
    const VIDEO_OF_PLAYLIST = 'video_playlist_';
    const VIDEO_CHANNEL_FOLLOW = 'video_channel_follow';
    const LIST_CHANNEL_FOLLOW = 'list_channel_follow';
    const LIST_CHANNEL_FOLLOW_WITH_HOT = 'list_channel_follow_with_hot';
    const VIDEO_MOST_VIEW_OF_USER = 'video_most_view_of_user_';
    const VIDEO_NEW_OF_USER = 'video_new_of_user_';
    const VIDEO_OLD_OF_USER = 'video_old_of_user_';
    const VIDEO_MOST_VIEW_OF_CHANNEL = 'video_most_view_of_channel_';
    const VIDEO_NEW_OF_CHANNEL = 'video_new_of_channel_';
    const VIDEO_OLD_OF_CHANNEL = 'video_old_of_channel_';
    const LIST_HOT_CHANNEL = 'list_hot_channel';
    const MUSIC_NEW = 'music_new';
    const MUSIC_RECOMMEND = 'music_recommend';
    const MUSIC_HISTORY = 'music_history';
    const MUSIC_FREE = 'music_free';
    const PLAYLIST_SEARCH = 'playlist_search';
    const PLAYLIST_OWNER = 'playlist_owner';
    const USER_FOLLOW = 'user_follow';
    const USER_FOLLOW_VIDEO = 'user_follow_video';
    const USER_FOLLOW_MUSIC = 'user_follow_music';
    const FILM_HOT = 'film_hot';
    const FILM_RECOMMEND = 'film_recommend';
    const FILM_NEW = 'film_new';
    const FILM_FREE = 'film_free';
    const FILM_RELATE = 'film_relate';
    const MEMBER = 'member';
    const MEMBER_FOLLOW = 'member_follow';
    const MEMBER_FOLLOW_VIEW = 'member_follow_';
    const VIDEO_USER_LIKE = 'video_user_like_';
    const CHANNEL_FOLLOW = 'channel_follow';
    const CHANNEL_HOT = 'channel_hot';
    const CHANNEL_SEARCH = 'channel_search';
    const VIDEO_NEWEST_CHANNEL = 'video_newest_channel_';
    const HISTORY_SEARCH = 'history_search';
    const MENU = 'menu';
    const MY_PLAYLIST = 'my_playlist';
    const VIDEO_CUSTOMER_MOSTVIEW = 'video_customer_mostview';
    const VIDEO_CUSTOMER_NEWEST = 'video_customer_newest';
    const VIDEO_CUSTOMER_MOSTVIEW_1512 = 'video_customer_mostview_1512';
    const VIDEO_CUSTOMER_NEWEST_1512 = 'video_customer_newest_1512';
    const PROMOTION_WINNER = 'promotion_winner';
    const PLAYLIST_PUBLIC_OF_USER = 'playlist_public_';
    const CATEGORY_PARENT = 'category_parent';
    const CATEGORY_DETAIL = 'category_detail';
    const CATEGORY_CHILD = 'category_child';
    const CATEGORY_CHILD_VIDEO = 'category_video';
    const VIDEO_MOST_TRENDING_CATE = 'video_most_trending_of_cate';
    const VIDEO_MOST_VIEW_CATE = 'video_most_view_of_cate';
    const VIDEO_NEW_CATE = 'video_new_of_cate';
    const VIDEO_HOME_BOX = 'video_home_box';

    const HOME_CHANNEL_V2 = 'home_channel_v2';
    const HOME_VIDEO_V2 = 'home_video_v2';

    const RELATED_OF_VIDEO = 'related_of_video_';

    const TET_HOLIDAY = 'tet_holiday';
    const WORLD_CUP = 'world_cup';

    const CHANNEL_RELATED = 'channel_related_';

    const VIDEO_RECOMMEND_RELATE = 'video_recommend_relate';

    const VIDEO_OF_CATEGORY = 'video_of_category';

    const VIDEO_PAID = 'video_paid';


    public static function getName($name) {
        $mess = [
            self::BANNER => Yii::t('api', 'Banner'),
            self::VIDEO_SEARCH => Yii::t('api', 'Tìm kiếm video'),
            self::VIDEO_NEWSFEED => Yii::t('api', 'News feed'),
            self::VIDEO_NEW => Yii::t('api', 'Video mới cập nhật'),
            self::MUSIC_NEW => Yii::t('api', 'Nhạc mới cập nhật'),
            self::MUSIC_RECOMMEND => Yii::t('api', 'Có thể bạn thích'),
            self::VIDEO_HOT => Yii::t('api', 'Video hot'),
            self::VIDEO_HISTORY => Yii::t('wap', 'Tiếp tục xem'),
            self::VIDEO_RECOMMEND => Yii::t('api', 'Có thể bạn thích'),
            self::VIDEO_FREE => Yii::t('api', 'Video miễn phí'),
            self::PLAYLIST_SEARCH => Yii::t('api', 'Tìm kiếm Phim'),
            self::FILM_HOT => Yii::t('api', 'Hot nhất tuần'),
            self::FILM_RECOMMEND => Yii::t('api', 'Phim đề xuất'),
            self::FILM_NEW => Yii::t('api', 'Phim mới nhất'),
            self::FILM_FREE => Yii::t('api', 'Phim miễn phí'),
            self::CATEGORY_GROUP => Yii::t('api', 'Thể loại'),
            self::MEMBER => Yii::t('api', 'Thành viên'),
            self::MEMBER_FOLLOW => Yii::t('api', 'Đang theo dõi'),
            self::USER_FOLLOW => Yii::t('api', 'Theo dõi'),
            self::USER_FOLLOW_VIDEO => Yii::t('api', 'Theo dõi'),
            self::USER_FOLLOW_MUSIC => Yii::t('api', 'Theo dõi'),
            self::MUSIC_HISTORY => Yii::t('api', 'Xem gần đây'),
            self::MUSIC_FREE => Yii::t('api', 'Nhạc miễn phí'),
            self::VIDEO_MOST_VIEW_OF_CHANNEL => Yii::t('wap', 'Xem nhiều nhất'),
            self::VIDEO_NEWEST_CHANNEL => Yii::t('wap', 'Mới nhất'),
            self::VIDEO_HOME => Yii::t('api', 'Có thể bạn thích'),
            self::VIDEO_WATCH_LATER => Yii::t('wap', 'Video xem sau'),
            self::VIDEO_OWNER => Yii::t('api', 'Video của tôi'),
            self::MY_PLAYLIST => Yii::t('api', 'Danh sách playlist'),
            self::VIDEO_CUSTOMER_MOSTVIEW => Yii::t('wap', 'Xem nhiều nhất'),
            self::VIDEO_CUSTOMER_NEWEST => Yii::t('wap', 'Mới nhất'),
            self::VIDEO_CUSTOMER_MOSTVIEW_1512 => Yii::t('wap', 'Xem nhiều nhất'),
            self::VIDEO_CUSTOMER_NEWEST_1512 => Yii::t('wap', 'Mới nhất'),
            self::PROMOTION_WINNER => Yii::t('api', 'Trúng thưởng'),
            self::CATEGORY_PARENT => Yii::t('api', 'Chuyên mục'),
            self::CATEGORY_DETAIL => Yii::t('api', 'Chi tiết chuyên mục'),
            self::VIDEO_MOST_TRENDING_CATE => Yii::t('api', 'Video phổ biến nhất'),
            self::VIDEO_MOST_VIEW_CATE => Yii::t('api', 'Video xem nhiều nhất'),
            self::VIDEO_NEW_CATE => Yii::t('api', 'Video mới nhất'),
            self::TET_HOLIDAY => Yii::t('api', 'Tết'),
            self::WORLD_CUP => Yii::t('api', 'World Cup'),
            self::VIDEO_RECOMMEND_RELATE => Yii::t('api', 'Video liên quan'),
            self::VIDEO_OF_CATEGORY => Yii::t('api', 'Video theo thể loại'),
            self::VIDEO_PAID => Yii::t('api', 'Video trả phí'),
        ];

        if (isset($mess[$name])) {
            return $mess[$name];
        }
        return '';
    }

}
