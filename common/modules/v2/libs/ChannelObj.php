<?php

namespace common\modules\v2\libs;

use common\helpers\Utils;
use common\models\VtUserBase;
use common\models\VtChannelBase;
use common\models\VtVideoBase;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\modules\v2\libs\Obj;
use yii\helpers\ArrayHelper;

class ChannelObj
{

    public static function serialize($id, $query, $cache = false, $name = null, $appendHotContent = false, $limit = 10)
    {

        $appId = Yii::$app->id;
        $key = $id . "_" . $appId . "_" . $query->limit . "_" . $query->offset;

        if ($cache && \Yii::$app->params['cache.enabled']) {

            $contents = \Yii::$app->cache->get($key);

            if ($contents === false) {
                if ($query) {
                    $contents = $query->all();
                }

                $totalChannel = count($contents);

                if ($totalChannel < $limit && $appendHotContent) {
                    $hotChannels = VtChannelBase::getHotChannel($limit, $query->offset);
                    $contents = Utils::mergeById($contents, $hotChannels, 'id', $limit);
                }

                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            if ($query) {
                $contents = $query->all();
            }
            $totalChannel = count($contents);
            if (($totalChannel < $limit) && $appendHotContent) {
                $hotChannels = VtChannelBase::getHotChannel($limit, $query->offset);
                $contents = Utils::mergeById($contents, $hotChannels, 'id', $limit);
            }
        }

        $result = array();
        foreach ($contents as $content) {
            $item = array();
            $item['channel_id'] = $content['id'];

            $item['channel_name'] = Utils::truncateWords(($content['full_name']) ? $content['full_name'] : (substr($content['msisdn'], 0, -3) . "xxx"), 80);

            $item['channel_name_mini'] = Utils::truncateWords(($content['full_name']) ? $content['full_name'] : (substr($content['msisdn'], 0, -3) . "xxx"), 30);

//            if (!$content["bucket"]) {
//                $avatarImages = Yii::$app->params['random.avatar.channel'][0];
//                $content["bucket"] = $avatarImages["bucket"];
//                $content["path"] = $avatarImages["path"];
//            }

            if (!$content["channel_bucket"]) {
                $avatarChannels = Yii::$app->params['random.banner.channel'];
                $randImage = $avatarChannels[rand(0, count($avatarChannels) - 1)];
                $content["channel_bucket"] = $randImage["bucket"];
                $content["channel_path"] = $randImage["path"];
            }

            if ($appId == 'app-web') {
                $item['avatarImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_CHANNEL_LOGO_LIST);
            } else {
                $item['avatarImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_AVATAR);
            }

            $item['avatarImageH'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_CHANNEL_LOGO_DETAIL);
            $item['avatarImageHX'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_AVATAR);

            $item['coverImage'] = VtHelper::getThumbUrl($content['channel_bucket'], $content['channel_path'], VtHelper::SIZE_COVER);

            $item['num_follow'] = $content['follow_count'];
            $item['num_video'] = $content['video_count'];
            $item['description'] = $content['description'];

            if (array_key_exists('is_follow', $content)) {
                $item['isFollow'] = true;
            }

            if ($content['follow_id']>0) {
                $item['isFollow'] = true;
            }

            $result[] = $item;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'content' => $result,
            'type' => 'CHANNEL'
        ];


    }
}