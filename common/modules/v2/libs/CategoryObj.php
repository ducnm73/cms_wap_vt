<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\modules\v2\libs;
use backend\models\VtVideo;
use common\helpers\Utils;
use common\models\VtConfigBase;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\models\LanguageConvert;

class CategoryObj {

    public static function serialize($id, $query, $cache = false) {
        //var_dump($id);die;//category_parent
        //var_dump($query);die;

        $appId = Yii::$app->id;
        $key = $id . "_" . $appId . "_" . md5($query->createCommand()->rawSql);
        $contents = $query->all();
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;

        if($currentLanguage!=$mainLanguage){
            $convertContent=[];
            foreach ($contents as $content){
                $multilang = $content['multilang'];
                if (is_null($multilang)) {
                    $convertContent[]=$content;
                }
                else{
                    $childPart1=LanguageConvert::arrayExclude($content,['name', 'description']);
                    $childPart2 = LanguageConvert::convertFieldsToArray($multilang,
                        ['name', 'description'],$currentLanguage, $content);
                    $convertContent[]=array_merge($childPart1,$childPart2);
                }
            }
            $contents = $convertContent;
            //var_dump($contents);die;
        }


        if ($cache && Yii::$app->params['cache.enabled']) {
            $contentsCache = Yii::$app->cache->get($key);
            if ($contentsCache === false) {
                //$contents = $query->all();
                Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        }


//        if ($cache && Yii::$app->params['cache.enabled']) {
//            $contents = Yii::$app->cache->get($key);
//            if ($contents === false) {
//                $contents = $query->all();
//                Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
//            }
//        } else {
//            $contents = $query->all();
//        }


        $categorys = array();
        foreach ($contents as $content) {
            $category = array();
            $category['id'] = $content['id'];
            $category['name'] = $content['name'];
            //var_dump($category['name']);die;
            $category['name_short'] = Utils::truncateWords($content['name'],15);
            $category['description'] = $content['description'];
            $category['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_CATEGORY);

            if($appId == 'app-web' && $id!= Obj::CATEGORY_PARENT){
                $category['avatarImage'] = VtHelper::getThumbUrl($content['avatar_bucket'], $content['avatar_path'], VtHelper::SIZE_CHANNEL_LOGO_LIST);
            }else{
                $category['avatarImage'] = VtHelper::getThumbUrl($content['avatar_bucket'], $content['avatar_path'], VtHelper::SIZE_AVATAR);
            }

            $category['avatarImageHX'] = VtHelper::getThumbUrl($content['avatar_bucket'], $content['avatar_path'], VtHelper::SIZE_AVATAR);

            $category['slug'] = $content['slug'];
            $category['type'] = $content['type'];
            $play_times = VtVideo::getPlayTimesCategory($category['id']);
            $category['play_times'] = number_format($play_times, 0, ',', '.');
            //Them highlight event 20/10
            $category['is_event'] = ($category['id'] == VtConfigBase::getConfig("event.2010.id"))?1:0;

            $categorys[] = $category;
        }
        //var_dump($categorys);die;

        return [
            'id' => $id,
            'type' => 'CATEGORY',
            'name' => Obj::getName($id),
            'content' => $categorys
        ];
    }

}
