<?php

namespace common\modules\v2\controllers;

use common\libs\RecommendationService;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\libs\VtFlow;
use common\models\MongoDBModel;
use common\models\VtActionLogBase;
use common\models\VtConfigBase;
use common\models\VtHashtagBase;
use common\models\VtHistoryViewBase;
use common\models\VtPlaylistItemBase;
use common\models\VtUploadFeedbackBase;
use common\models\VtUserBase;
use common\models\VtUserChangeInfoBase;
use common\models\VtChannelBase;
use common\models\VtChannelFollowBase;
use common\models\VtUserPlaylistBase;
use common\models\VtUserPlaylistItemBase;
use common\modules\v2\libs\VideoObj;
use cp\models\VtVideo;
use Yii;
use common\models\VtFavouriteVideoBase;
use common\models\VtUserFollowBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\ResponseCode;
use common\modules\v2\libs\Obj;
use common\controllers\ApiController;
use common\helpers\Utils;
use yii\db\Expression;

class VideoController extends \common\modules\v1\controllers\VideoController
{

    public function actionGetDetail($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = null)
    {

        $id = trim(Yii::$app->request->get('id', 0));
        $playlistId = trim(Yii::$app->request->get('playlist_id', false));
        $playlistType = trim(Yii::$app->request->get('playlist_type', 'ADMIN'));
        $isShowPopup = Yii::$app->request->get('popup', 0);
        $networkDeviceId = trim(Yii::$app->request->get('network_device_id', ''));
        $deviceType = trim(Yii::$app->request->get('device_type', ''));

        $currentTime = 0;

        if (!$profileId) {
            $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['streaming.vodProfileId']));
        }

        if (!isset($acceptLossData)) {
            $acceptLossData = Yii::$app->request->get("accept_loss_data", 0);
        }

        $videoOfPlaylist = null;
        if (!empty($playlistId)) {

            $playlist = VtUserPlaylistBase::getDetail($playlistId);

            if (empty($playlist)) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => Yii::t('wap','Playlist không tồn tại.'),
                ];
            }

            if (empty($id)) {
                $items = VtUserPlaylistItemBase::getItemsByPlaylistId($playlistId);
                $id = $items[0]['id'];
            }


            if ($playlistType == 'USER') {
                $videoOfPlaylist = VideoObj::serialize(
                    Obj::VIDEO_OF_PLAYLIST . $playlistId, VtUserPlaylistItemBase::getItemsByPlaylistIdQuery($playlistId), false, $playlist['name']
                );
                $videoOfPlaylist['description'] = $playlist['description'];
                $videoOfPlaylist['fullUserName'] = $playlist['full_name'] ? $playlist['full_name'] : Utils::hideMsisdnLast($playlist['msisdn']);
            }
        }

        //anhntm 2910
        $video = VtVideoBase::getDetail($id, "", false, true);

        if (empty($video)) {
            $simple_string = $id;
            $ciphering = "AES-128-CTR";
            $iv_length = openssl_cipher_iv_length($ciphering);
            $options   = 0;
            $decryption_iv = '1234567891011121';
            $decryption_key = "W3docs";
            $iframeVideoDecryption = openssl_decrypt($simple_string, $ciphering, $decryption_key, $options, $decryption_iv);
            $id = $iframeVideoDecryption;

            $video = VtVideoBase::getDetail($id, "", false, true);
            if (empty($video)) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => Yii::t('wap','Video không tồn tại.'),
                ];
            }
        }

        if ($video['status'] == VtVideoBase::STATUS_DRAFT || $video['status'] == VtVideoBase::STATUS_DELETE) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Video chưa được phê duyệt. Quý khách vui lòng thử lại sau'),
            ];
        }

        $detailObj = array();
        $detailObj['id'] = $video['id'];
        $detailObj['name'] = $video['name']; 
        $detailObj['description'] = $video['description'];
        $detailObj['price_play'] = $video['price_play'];
        // Neu du lieu la phim cu: migrate
        if ($video['old_playlist_id'] && !$playlistId) {
            $playlistId = $video['old_playlist_id'];
            $detailObj['type'] = 'FILM';
            $video['type'] = 'FILM';
        }

        if (!empty($playlistId) && $video['type'] != 'FILM') {
            $detailObj['type'] = 'PLAYLIST';
        } else {
            $detailObj['type'] = $video['type'];
        }
        $video["tag"] = str_replace("#", "", str_replace(" #", ",", $video["tag"]));
        $tags = explode(",", $video["tag"]);
        $video["tags"] = $tags;
        $detailObj['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_COVER);
        $detailObj['likeCount'] = $video['like_count'];
        $detailObj['dislikeCount'] = $video['dislike_count'];
        $detailObj['play_times'] = number_format($video['play_times'], 0, ',', '.');
        $detailObj['suggest_package_id'] = $video['suggest_package_id'];
        $detailObj['tag'] = $video['tag'];
        if($video["tags"])
        $detailObj['tags'] = $video['tags'];
        $detailObj['duration'] = Utils::durationToStr($video['duration']);
        $detailObj['publishedTime'] = Utils::time_elapsed_string($video['published_time']);
        if (Yii::$app->id == 'app-api') {
            $detailObj['show_times'] = Utils::dateDiff($video['show_times']);
        } else {
            $detailObj['show_times'] = $video['show_times'];
        }
        $detailObj['isFavourite'] = 0;
        $detailObj['watchTime'] = 0;

        $detailObj['tagline'] = ($video['is_recommend']) ? 1 : (($video['is_hot']) ? 2 : 0);
        $detailObj['drm_content_id'] = $video['drm_content_id'];
        $detailObj['can_comment'] = $video['can_comment'];

        //2018-10-01
        if($id > 2332321){
            $detailObj['previewImage'] =str_replace( basename($video['file_path']), "P001.png",  VtHelper::getThumbUrl($video['file_bucket'], $video['file_path'], false, true));
        }

        if ($this->userId) {
            $objFavourite = VtFavouriteVideoBase::checkIsFavourite($this->userId, $id);
            $detailObj['isFavourite'] = ($objFavourite) ? $objFavourite['status'] : 0;
            $historyView = VtHistoryViewBase::getByItemId($this->userId, $this->msisdn, 'VOD', $id);
            if (!empty($historyView)) {
                $detailObj['watchTime'] = $historyView['time'];
            }
        }

        // TODO hardcode
        $detailObj['link'] = Yii::$app->params['app.domain'] . "/video/" . $id . "/" . $video['slug'] . "?utm_source=APPSHARE";
        $detailObj['linkSocial'] = Yii::$app->params['app.domain'] . "/video/" . $id . "/" . $video['slug'] . "?utm_source=SOCIAL";

        // them truong du lieu neu la WAP
        if (Yii::$app->id == 'app-wap' || Yii::$app->id == 'app-web') {
            $detailObj['slug'] = $video['slug'];
        }

        // TODO hardcode
        // Lay thong tin thue bao (hien thi kenh neu chua co keknh hien thi thong tin user)
        $objUser = VtChannelBase::getChannelById($video['channel_id']);

        if(empty($objUser)){
            $objUser = VtUserBase::getById($video['created_by']);
        }
        //$objUser = VtUserBase::getById($video['created_by']);

        $detailObj['owner']['id'] = $objUser['id'];
        if($objUser['full_name']) {
            $detailObj['owner']['name'] = $objUser['full_name'];
        } elseif(isset($objUser['msisdn'])) {
            $detailObj['owner']['name'] = Utils::hideMsisdnLast($objUser['msisdn']);
        } else {
            $detailObj['owner']['name'] = '';
        }
        // $detailObj['owner']['name'] = ($objUser['full_name']) ? $objUser['full_name'] : (($objUser['msisdn']) ? substr($objUser['msisdn'], 0, -3) . "xxx" : "");
        $detailObj['owner']['avatarImage'] = VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR);
        $detailObj['owner']['followCount'] = $objUser['follow_count'];
        if(isset($objUser['is_official'])){
            if($objUser['is_official']==1){
                $detailObj['owner']['official'] = true;
            }
        }

        if ($this->userId) {
            $userFollow = VtChannelFollowBase::getFollow($this->userId, $video['channel_id']);
            $detailObj['owner']['isFollow'] = $userFollow ? 1 : 0;
            $detailObj['owner']['notification_type'] = $userFollow ? $userFollow->notification_type : 0;
        } else {
            $detailObj['owner']['isFollow'] = $objUser['follow_count'];
            $detailObj['owner']['notification_type'] = 0;
        }
        // Lay thong tin chuyen muc

        $objCate = \api\models\VtGroupCategory::getById($video['category_id']);
        $detailObj['cate']['id'] = $objCate['id'];
        $detailObj['cate']['name'] = $objCate['name'];
        $detailObj['cate']['parent_id'] = $objCate['parent_id'];

        $streamingObj = VtFlow::viewVideo($this->msisdn, $video, $playlistId, $profileId, $this->userId, $this->authJson, $supportType, $acceptLossData, null, $this->distributionId, $networkDeviceId,$deviceType);
        //An noi dung tinh phi khi duyet APP iOS
        if ($this->needHiddenFreemiumContent) {
            if (array_key_exists('popup', $streamingObj)) {
                $streamingObj['popup'] = [];
            }
        }

        //Neu noi dung truyen thong GA thi related lay 30 ban ghi
        if ($isShowPopup) {
            $limitRelated = Yii::$app->params['app.googleadwords.relate.limit'];;
        } else {
            $limitRelated = Yii::$app->params['app.video.relate.limit'];
        }

        $relateds = null;
        if (!empty($deviceId) && Yii::$app->params['recommendation']['enable']) {

            $videoIds = RecommendationService::getVideoIds(RecommendationService::BOX_RELATED, $deviceId, $id, [
                'channel_id' => $video['created_by'],
                'channel_name' => $objUser['full_name'],
                'video_name' => $video['name'],
                'tag_name' => $video['tag']
            ], 1, $limitRelated);

            $relateds = null;
            if (!empty($videoIds)) {
                $relateds = VideoObj::serialize(
                    Obj::VIDEO_RECOMMEND_RELATE, VtVideoBase::getVideosByIdsQuery($videoIds, Yii::$app->params['app.search.page.limit'])
                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
                );
            }

        }

        if (!isset($relateds['content']) || count($relateds['content']) == 0) {
            $relateds = VideoObj::serialize(
                Obj::VIDEO_RELATE, VtVideoBase::getVideoRelatedQuery($video['id'], $video['category_id'], $video['created_by'], $limitRelated, 0, $video['published_time']), false
            );
        }

        //$limitRelated
        if ($this->userId || $this->msisdn) {
            $history = VtHistoryViewBase::getByItemId($this->userId, $this->msisdn, $video['type'], $id);
//            $currentTime = ($history) ? intval($history['time']) : 0;
            if ( intval($history['time']) == intval($history['duration']) || intval($history['time']) == intval($history['duration']) - 1){
                $currentTime = 0;
            }else{
                $currentTime = intval($history['time']);
            }
        }
//        echo '<pre>';
//        var_dump($history); die('eee');
//        if ($currentTime == $history['duration'] || $currentTime == $history['duration'] - 1) {
//            $currentTime = 0;
//            $history['time'] = 0;
//        }
//        echo '<pre>';
//        var_dump( $history, $currentTime, $history['time'],  $history['duration']); die('eeee');

        // Kiem tra xem loai playlist can lay
        if ($playlistType == 'ADMIN') {

            // Lay danh sach cac item thuoc playlist
            $items = VtPlaylistItemBase::getItemsByPlaylistId($playlistId);
            $parts = array();
            $cc = 0;
            $firstItemId = 0;
            $prevVideoId = 0;
            $nextVideoId = 0;

            $isVideoInPlaylist = false;

            foreach ($items as $item) {
                if ($firstItemId == 0) {
                    $firstItemId = $item['id'];
                }
                if ($id == $item['id']) {
                    $isVideoInPlaylist = true;
                    if ($cc > 0) {
                        $prevVideoId = $items[$cc - 1]['id'];
                    }
                    if ($cc < count($items) - 1) {
                        $nextVideoId = $items[$cc + 1]['id'];
                    }
                }
                $prefixAlias = Yii::$app->params['alias.prefix'];
                $parts[$cc]['id'] = $item['id'];
                $parts[$cc]['alias'] = $prefixAlias . (($item['alias']) ? $item['alias'] : ($cc + 1));
                $parts[$cc]['name'] = $item['name'];
                $parts[$cc]['slug'] = $item['slug'];
                $cc++;
            }

            if (!$nextVideoId) {
                if (isset($relateds['content']) && count($relateds['content']) > 0) {
                    $nextVideoId = $relateds['content'][0]['id'];
                }
            }
        }

        // Tim vi tri next , prev Video

        //Kiem tra xem the loai co thuoc the loai worlcup khong
        if (strpos(VtConfigBase::getConfig('category.worldcup') . ",", $video['category_id'] . ",") !== false) {
            $detailObj['cate']['is_world_cup_category'] = 1;
        }

        $is_check = 0;

        if($video['is_check'] != null) {
            $is_check = $video['is_check'];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'is_check' => $is_check,
                'detail' => $detailObj,
                'previousId' => $prevVideoId,
                'nextId' => $nextVideoId,
                'profile' => [],
                'streams' => $streamingObj,
                'relateds' => $relateds,
                'parts' => $parts,
                'currentTime' => $currentTime,
                'videoOfPlaylist' => $videoOfPlaylist,
            ]
        ];
    }


    public function actionRelatedVideoBox()
    {
        $id = trim(Yii::$app->request->get('id', 0));
        $channelId = trim(Yii::$app->request->get('channel_id', 0));
        $channelName = trim(Yii::$app->request->get('channel_name', ''));
        $videoName = trim(Yii::$app->request->get('video_name', ''));
        $tagName = trim(Yii::$app->request->get('tag_name', ''));
        $categoryId = trim(Yii::$app->request->get('category_id', 0));
        $categoryName = trim(Yii::$app->request->get('category_name', ''));
        $categoryParentId = trim(Yii::$app->request->get('category_parent_id', ''));

        $deviceId = trim(Yii::$app->request->get('device_id', ''));
        $limitRelated = Yii::$app->params['app.video.relate.limit'];

        $relateds = null;
//        if (!empty($deviceId) && Yii::$app->params['recommendation']['enable']) {
//
//            $videoIds = RecommendationService::getVideoIds(RecommendationService::BOX_RELATED, $deviceId, $id, [
//                'channel_id' => $channelId,
//                'channel_name' => $channelName,
//                'video_name' => $videoName,
//                'tag_name' => $tagName,
//                'category_id' => $categoryId,
//                'category_name' => $categoryName,
//                'category_parent_id' => $categoryParentId
//            ]);
//
//            $relateds = null;
//            if (!empty($videoIds)) {
//                $relateds = VideoObj::serialize(
//                    Obj::VIDEO_RECOMMEND_RELATE, VtVideoBase::getVideosByIdsQuery($videoIds, $limitRelated)
//                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
//                );
//            }
//
//        }

        if (!isset($relateds['content']) || count($relateds['content']) == 0) {
            $video = VtVideoBase::getDetail($id, "", false, true);

            $relateds = VideoObj::serialize(
                Obj::VIDEO_RELATE, VtVideoBase::getVideoRelatedQuery($video['id'], $video['category_id'], $video['created_by'], $limitRelated, 0, $video['published_time']), false
            );
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $relateds
        ];
    }


    public function actionGetVideoStream($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = null)
    {
        $id = trim(Yii::$app->request->get('id'));

        if (!$profileId) {
            $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['streaming.vodProfileId']));
        }
        $playlistId = trim(Yii::$app->request->get('playlist_id', false));

        if (!isset($acceptLossData)) {
            $acceptLossData = Yii::$app->request->get("accept_loss_data", 0);
        }

        $objVideo = VtVideoBase::getDetail($id);

        if ($objVideo) {
            $streamingObj = VtFlow::viewVideo($this->msisdn, $objVideo, $playlistId, $profileId, $this->userId, $this->authJson, $supportType, $acceptLossData, null, $this->distributionId);
            //An noi dung tinh phi khi duyet APP iOS
            if ($this->needHiddenFreemiumContent) {
                if (array_key_exists('popup', $streamingObj)) {
                    $streamingObj['popup'] = [];
                }
            }
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'streams' => $streamingObj,
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }
    }

    public function actionDownload($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = null)
    {
        $id = trim(Yii::$app->request->get('id'));

        if (!$profileId) {
            $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['download.vodProfileId']));
        }

        $playlistId = trim(Yii::$app->request->get('playlist_id', false));

        if (!isset($acceptLossData)) {
            $acceptLossData = Yii::$app->request->get("accept_loss_data", 0);
        }

        $objVideo = VtVideoBase::getDetail($id);
        if (!$objVideo) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }


        if (!VtConfigBase::getConfig('ALLOW_DOWNLOAD_VIDEO', 0)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Video không được phép download')
            ];
        }

        $streamingObj = VtFlow::viewVideo($this->msisdn, $objVideo, $playlistId, Yii::$app->params['download.vodProfileId'], $this->userId, $this->authJson, S3Service::OBJCDN, $acceptLossData, VtConfigBase::getConfig('ALLOW_DOWNLOAD_FREE_DONT_NEED_LOGIN', 0), $this->distributionId);
        //An noi dung tinh phi khi duyet APP iOS
        if ($this->needHiddenFreemiumContent) {
            if (array_key_exists('popup', $streamingObj)) {
                $streamingObj['popup'] = [];
            }
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'streams' => $streamingObj,
            ]
        ];
    }

    public function actionGetMoreContent()
    {
        $id = trim(Yii::$app->request->get('id', ''));
        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit', Yii::$app->params['app.showMore.limit']));

        $contents = VideoObj::serialize(
            Obj::VIDEO_MOST_RELATE, VtVideoBase::getHotVideo($limit, $offset), true, false, 'NEWSFEED'
        );


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        ];
    }

    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionToggleLikeVideo()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = intval(trim(Yii::$app->request->post('id', 0)));
        $status = intval(trim(Yii::$app->request->post('status', 0)));
        if (!in_array($status, [VtFavouriteVideoBase::STATUS_REMOVE, VtFavouriteVideoBase::STATUS_LIKE, VtFavouriteVideoBase::STATUS_DISLIKE])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Trạng thái không hợp lệ'),
            ];
        }

        $video = VtVideoBase::getDetail($id);

        if (empty($video)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        $favObj = VtFavouriteVideoBase::getFavourite($this->userId, $id);
        $lastStatus = ($favObj) ? $favObj['status'] : 0;

        if (!empty($favObj)) {
            if ($status == VtFavouriteVideoBase::STATUS_REMOVE) {
                $favObj->delete();
                $isLike = false;

            } elseif ($lastStatus != $status) {
                $favObj->status = $status;
                $favObj->save();

                //Dat log user dislike
                if ($status == VtFavouriteVideoBase::STATUS_DISLIKE) {
                    MongoDBModel::insertEvent(MongoDBModel::DISLIKE, $id, $this->userId, $this->msisdn, "", $video["created_by"]);
                } else {
                    MongoDBModel::insertEvent(MongoDBModel::LIKE, $id, $this->userId, $this->msisdn, "", $video["created_by"]);
                }
            }
        } elseif (in_array($status, [VtFavouriteVideoBase::STATUS_LIKE, VtFavouriteVideoBase::STATUS_DISLIKE])) {
            $favObj = new VtFavouriteVideoBase();
            $favObj->insertFavourite($this->userId, $id, $status);

            if ($status == VtFavouriteVideoBase::STATUS_DISLIKE) {
                MongoDBModel::insertEvent(MongoDBModel::DISLIKE, $id, $this->userId, $this->msisdn, "", $video["created_by"]);
            } else {
                MongoDBModel::insertEvent(MongoDBModel::LIKE, $id, $this->userId, $this->msisdn, "", $video["created_by"]);
            }
            //Dat log user like

        }

        if ($lastStatus == VtFavouriteVideoBase::STATUS_DISLIKE && $status == VtFavouriteVideoBase::STATUS_LIKE) {
            VtVideoBase::updateLikeVsDisLikeCount($id, 1, -1);
        } elseif ($lastStatus == 0 && $status == VtFavouriteVideoBase::STATUS_LIKE) {
            VtVideoBase::updateLikeVsDisLikeCount($id, 1, 0);
        } elseif ($lastStatus == VtFavouriteVideoBase::STATUS_LIKE && $status == VtFavouriteVideoBase::STATUS_DISLIKE) {
            VtVideoBase::updateLikeVsDisLikeCount($id, -1, 1);
        } elseif ($lastStatus == 0 && $status == VtFavouriteVideoBase::STATUS_DISLIKE) {
            VtVideoBase::updateLikeVsDisLikeCount($id, 0, 1);
        } elseif ($lastStatus == VtFavouriteVideoBase::STATUS_LIKE && $status == VtFavouriteVideoBase::STATUS_REMOVE) {
            VtVideoBase::updateLikeVsDisLikeCount($id, -1, 0);
        } elseif ($lastStatus == VtFavouriteVideoBase::STATUS_DISLIKE && $status == VtFavouriteVideoBase::STATUS_REMOVE) {
            VtVideoBase::updateLikeVsDisLikeCount($id, 0, -1);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'status' => $status
            ]
        ];
    }

    public function actionDownloadVideo()
    {

    }

    /**
     * API them , xoa khoi danh sach xem sau
     * @return array
     */
    public function actionToggleWatchLater()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = intval(trim(Yii::$app->request->post("id", '')));
        $status = intval(trim(Yii::$app->request->post("status", '-1')));

        if (!in_array($status, [0, 1])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Trạng thái không hợp lệ'),
            ];
        }

        $video = VtVideoBase::getDetail($id);
        if (empty($video)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Video không tồn tại hoặc chưa được phê duyệt.'),
            ];
        }

        $isWatchLater = false;

        $redisCache = Yii::$app->getCache();

        //Lay danh sach video trong danh sach xem sau
        $videoStr = $redisCache->get("video_list_watch_later_" . $this->userId);
        $arrVideo = explode(",", $videoStr);

        if ($status == 0) {
            //Neu videoId da co trong danh sach thi xoa item i
            $isWatchLater = false;

            $arrVideo = array_diff($arrVideo, [$id]);

            $redisCache->set("video_list_watch_later_" . $this->userId, implode(",", $arrVideo));
        } elseif ($status == 1) {
            //Neu chua co thi them vao
            $isWatchLater = true;
            if ($videoStr) {
                $arrVideo = array_diff($arrVideo, [$id]);

                if (count($arrVideo) >= Yii::$app->params['num.max.video.add.later']) {
                    unset($arrVideo[count($arrVideo) - 1]);
                }
                $videoStr = implode(",", $arrVideo);
                $redisCache->set("video_list_watch_later_" . $this->userId, $id . "," . $videoStr);
            } else {
                $redisCache->set("video_list_watch_later_" . $this->userId, $id);
            }
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isWatchLater' => $isWatchLater
            ]
        ];
    }

    public function actionEditVideo()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        // Specify the input name set in the javascript.
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {

            $id = trim(Yii::$app->request->post("id"));
            $objVideo = VtVideoBase::getDetail($id, "VOD", true, true);

            if ($objVideo->status != 1) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Video không được phép chỉnh sửa")
                ];
            }
            //todo mai anh fixxx
            if ($objVideo['created_by'] != $this->userId) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Video không có quyền chỉnh sửa")
                ];
            }
            set_time_limit(1200);
            $extImage = pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

            if (file_exists($_FILES['thumbnail']['tmp_name']) || is_uploaded_file($_FILES['thumbnail']['tmp_name'])) {
                if (!in_array(strtolower($extImage), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Định dạng file thumbnail upload không đúng")
                    ];
                }

                if ($_FILES['thumbnail']['size'] > 5242880) { //800 MB (size is also in bytes)
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Kích thước thumbnail tối đa 5MB")
                    ];
                }
                $imageBucket = Yii::$app->params['s3']['static.bucket'];
                $fileImagePath = Utils::generatePath($extImage);
                //PUSH Image file
                $arrImageS3 = S3Service::putObject($imageBucket, $_FILES['thumbnail']['tmp_name'], $fileImagePath);
                Yii::info("S3 PUSH IMAGE FILE=" . json_encode($arrImageS3) . "|" . $_FILES['thumbnail']['tmp_name'] . "|" . $fileImagePath);
                VtHelper::generateAllThumb($imageBucket, $_FILES['thumbnail']['tmp_name'], $fileImagePath);

                if ($arrImageS3['errorCode'] == S3Service::UNSUCCESS) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Upload file cloud không thành công. Vui lòng thử lại!")
                    ];
                }
                $objVideo->bucket = $imageBucket;
                $objVideo->path = $fileImagePath;
            }

            $videoName = trim(Yii::$app->request->post("title"));
            $description = trim(Yii::$app->request->post("description"));

            if (mb_strlen($videoName, 'UTF-8') > 255 || mb_strlen($videoName) <= 0) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Tên video phải nhập và số kí tự không lớn hơn 255 kí tự"),
                ];
            }
            if (mb_strlen($description, 'UTF-8') > 500) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Mô tả video không được lớn hơn 500 kí tự"),
                ];
            }
            $objVideo->name = $videoName;
            $objVideo->save(false);

            // Luu action log
            //$actionLog = "(" . $this->userId . ")" . "[" . $this->msisdn . "]" . $objVideo->name;
            //VtActionLogBase::insertLog(VtActionLogBase::M_API_VIDEO, $objVideo->id, VtActionLogBase::T_UPDATE, $actionLog, $this->userId, "API");

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('backend',"Chỉnh sửa thành công"),
                'data' => [
                    "id" => $objVideo->id,
                    "title" => $objVideo->name,
                    "coverImage" => VtHelper::getThumbUrl($objVideo->bucket, $objVideo->path, VtHelper::SIZE_VIDEO)
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Method Not Allowed")
            ];
        }
    }

    public function actionGetFeedbackUpload()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $videoId = Yii::$app->request->get('id');
        $objFeedback = VtUploadFeedbackBase::getUploadFeedbackByUser($this->userId, $videoId);

        if($objFeedback){
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
                'data' => [
                    "video_id"=>$objFeedback['video_id'],
                    "status"=>$objFeedback['status'],
                    "created_at"=>$objFeedback['created_at'],
                    "feedback_content"=>$objFeedback['feedback_content'],
                    "reject_reason"=>$objFeedback['reject_reason'],
                ]
            ];
        }else{
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không tồn tại'),

            ];
        }


    }

    public function actionFeedbackUploadVideo()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $feedBackContent = trim(Yii::$app->request->post('feedback_content'));
        $videoId = Yii::$app->request->post('id');

        if (Yii::$app->request->isPost) {

            if(VtUploadFeedbackBase::getUploadFeedbackByUser($this->userId, $videoId)){
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Nội dung đang chờ quản trị viên xử lý")
                ];
            };

            if(!$feedBackContent){
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Yêu cầu nhập nội dung phản hồi")
                ];
            }

            $objVideo = VtVideoBase::findOne(["id" => $videoId, "created_by" => $this->userId, "status" => VtVideoBase::STATUS_DELETE]);

            if (!$objVideo) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Nội dung phản hồi không hợp lệ"),
                ];
            }

            if ($_FILES["feedback_file"]['name']) {
                set_time_limit(1200);
                $ext = pathinfo($_FILES['feedback_file']['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Định dạng file video upload không đúng(jpg, jpeg, png, gif)")
                    ];
                }
                if ($_FILES['feedback_file']['size'] > 10485760) { //10 MB (size is also in bytes)
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Kích thước thumbnail tối đa 10MB")
                    ];
                }


                $imageBucket = Yii::$app->params['s3']['static.bucket'];
                $filePath = Utils::generatePath($ext);
                $arrImageS3 = S3Service::putObject($imageBucket, $_FILES['feedback_file']['tmp_name'], $filePath);
                if ($arrImageS3['errorCode'] != 0) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Đăng tải file thất bại, vui lòng thử lại sau!"),
                    ];
                }
            }

            if (mb_strlen($feedBackContent, 'UTF-8') > 1000 || mb_strlen($feedBackContent) == 0) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Yêu cầu nhập nội dung phản hồi"),
                ];
            }



            VtUploadFeedbackBase::insertUploadFeedback($this->userId, $videoId, $feedBackContent, $imageBucket, $filePath);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap','Gửi ý kiến thành công'),

        ];
    }


}
