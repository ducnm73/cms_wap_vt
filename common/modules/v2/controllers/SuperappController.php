<?php

namespace common\modules\v2\controllers;

use common\libs\RecommendationService;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\libs\VtFlow;
use common\models\VtConfigBase;
use common\models\VtHistoryViewBase;
use common\models\VtPlaylistItemBase;
use common\models\VtUserBase;
use common\models\VtUserPlaylistBase;
use common\models\VtUserPlaylistItemBase;
use common\modules\v2\libs\VideoObj;
use common\models\KpiLogBase;

use backend\models\VtVideo;
use common\helpers\Utils;
use common\models\VtFavouriteVideoBase;
use common\models\VtGroupCategoryBase;
use common\models\VtUserFollowBase;
use common\modules\v2\libs\CategoryObj;
use common\models\VtPackageBase;
use api\models\VtGroupCategory;
use Yii;
use common\modules\v2\libs\Obj;
use common\models\VtVideoBase;
use common\modules\v2\libs\ResponseCode;
use common\controllers\SuperapiController;
use common\libs\VtService;
use common\models\VtSmsMessageBase;
use common\models\VtSmsMtBase;
use wap\models\VtSmsMessage;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class SuperappController extends SuperapiController{

    public function actionGetAllPackage()
    {

        $allPackages = VtPackageBase::getAllPackage();

        $packagesMapName = ArrayHelper::map($allPackages, 'sub_service_name', 'name');
        $packagesMapCpDataPercentage = ArrayHelper::map($allPackages, 'type', 'cp_data_percentage');

        $Packages = [
            'id' => 'package_list',
            'type' => 'PACKAGE',
            'name' => 'package list',
            'content' => $allPackages
        ];

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                $Packages
            ]
        ];
    }

    public function actionGetDetail($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = null)
    {

        $id = trim(Yii::$app->request->get('id', 0));
        $playlistId = trim(Yii::$app->request->get('playlist_id', false));
        $playlistType = trim(Yii::$app->request->get('playlist_type', 'ADMIN'));
        $isShowPopup = Yii::$app->request->get('popup', 0);
        $networkDeviceId = trim(Yii::$app->request->get('network_device_id', ''));
        $deviceType = trim(Yii::$app->request->get('device_type', ''));
        $msisdn_app = trim(Yii::$app->request->get('msisdn', ''));

        $currentTime = 0;

        if (!$profileId) {
            $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['streaming.vodProfileId']));
        }

        if (!isset($acceptLossData)) {
            $acceptLossData = Yii::$app->request->get("accept_loss_data", 0);
        }

        $videoOfPlaylist = null;
        if (!empty($playlistId)) {

            $playlist = VtUserPlaylistBase::getDetail($playlistId);

            if (empty($playlist)) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => Yii::t('wap','Playlist không tồn tại.'),
                ];
            }

            if (empty($id)) {
                $items = VtUserPlaylistItemBase::getItemsByPlaylistId($playlistId);
                $id = $items[0]['id'];
            }


            if ($playlistType == 'USER') {
                $videoOfPlaylist = VideoObj::serialize(
                    Obj::VIDEO_OF_PLAYLIST . $playlistId, VtUserPlaylistItemBase::getItemsByPlaylistIdQuery($playlistId), false, $playlist['name']
                );
                $videoOfPlaylist['description'] = $playlist['description'];
                $videoOfPlaylist['fullUserName'] = $playlist['full_name'] ? $playlist['full_name'] : Utils::hideMsisdnLast($playlist['msisdn']);
            }
        }

        //anhntm 2910
        $video = VtVideoBase::getDetail($id, "", false, true);

        if (empty($video)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Video không tồn tại.'),
            ];
        }

        if ($video['status'] == VtVideoBase::STATUS_DRAFT || $video['status'] == VtVideoBase::STATUS_DELETE) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Video chưa được phê duyệt. Quý khách vui lòng thử lại sau'),
            ];
        }

        $detailObj = array();
        $detailObj['id'] = $video['id'];
        $detailObj['name'] = $video['name'];
        $detailObj['price_play'] = $video['price_play'];
        $detailObj['description'] = $video['description'];

        // Neu du lieu la phim cu: migrate
        if ($video['old_playlist_id'] && !$playlistId) {
            $playlistId = $video['old_playlist_id'];
            $detailObj['type'] = 'FILM';
            $video['type'] = 'FILM';
        }

        if (!empty($playlistId) && $video['type'] != 'FILM') {
            $detailObj['type'] = 'PLAYLIST';
        } else {
            $detailObj['type'] = $video['type'];
        }
        $video["tag"] = str_replace("#", "", str_replace(" #", ",", $video["tag"]));
        $tags = explode(",", $video["tag"]);
        $video["tags"] = $tags;
        $detailObj['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_COVER);
        $detailObj['likeCount'] = $video['like_count'];
        $detailObj['dislikeCount'] = $video['dislike_count'];
        $detailObj['play_times'] = number_format($video['play_times'], 0, ',', '.');
        $detailObj['suggest_package_id'] = $video['suggest_package_id'];
        $detailObj['tag'] = $video['tag'];
        if($video["tags"])
            $detailObj['tags'] = $video['tags'];
        $detailObj['duration'] = Utils::durationToStr($video['duration']);
        $detailObj['publishedTime'] = Utils::time_elapsed_string($video['published_time']);
        if (Yii::$app->id == 'app-api') {
            $detailObj['show_times'] = Utils::dateDiff($video['show_times']);
        } else {
            $detailObj['show_times'] = $video['show_times'];
        }
        $detailObj['isFavourite'] = 0;
        $detailObj['watchTime'] = 0;

        $detailObj['tagline'] = ($video['is_recommend']) ? 1 : (($video['is_hot']) ? 2 : 0);
        $detailObj['drm_content_id'] = $video['drm_content_id'];

        //2018-10-01
        if($id > 2332321){
            $detailObj['previewImage'] =str_replace( basename($video['file_path']), "P001.png",  VtHelper::getThumbUrl($video['file_bucket'], $video['file_path'], false, true));
        }

        if ($this->userId) {
            $objFavourite = VtFavouriteVideoBase::checkIsFavourite($this->userId, $id);
            $detailObj['isFavourite'] = ($objFavourite) ? $objFavourite['status'] : 0;
            $historyView = VtHistoryViewBase::getByItemId($this->userId, $this->msisdn, 'VOD', $id);
            if (!empty($historyView)) {
                $detailObj['watchTime'] = $historyView['time'];
            }
        }

        // TODO hardcode
        $detailObj['link'] = Yii::$app->params['cdn.site'] . "/video/" . $id . "/" . $video['slug'] . "?utm_source=APPSHARE";


        // them truong du lieu neu la WAP
        if (Yii::$app->id == 'app-wap' || Yii::$app->id == 'app-web') {
            $detailObj['slug'] = $video['slug'];
        }

        // TODO hardcode
        // Lay thong tin thue bao
        $objUser = VtUserBase::getById($video['created_by']);

        $detailObj['owner']['id'] = $objUser['id'];
        $detailObj['owner']['name'] = ($objUser['full_name']) ? $objUser['full_name'] : (($objUser['msisdn']) ? substr($objUser['msisdn'], 0, -3) . "xxx" : "");
        $detailObj['owner']['avatarImage'] = VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR);
        $detailObj['owner']['followCount'] = $objUser['follow_count'];

        if ($this->userId) {
            $userFollow = VtUserFollowBase::getFollow($this->userId, $video['created_by']);
            $detailObj['owner']['isFollow'] = $userFollow ? 1 : 0;
            $detailObj['owner']['notification_type'] = $userFollow ? $userFollow->notification_type : 0;
        } else {
            $detailObj['owner']['isFollow'] = 0;
            $detailObj['owner']['notification_type'] = 0;
        }
        // Lay thong tin chuyen muc

        $objCate = \api\models\VtGroupCategory::getById($video['category_id']);
        $detailObj['cate']['id'] = $objCate['id'];
        $detailObj['cate']['name'] = $objCate['name'];
        $detailObj['cate']['parent_id'] = $objCate['parent_id'];
        // truong hop supperap xem chi tiet video co truyen so dien thoai

        if($msisdn_app){
            $this->msisdn = $msisdn_app;
        }
        $streamingObj = VtFlow::viewVideo($this->msisdn, $video, $playlistId, $profileId, $this->userId, $this->authJson, $supportType, $acceptLossData, null, $this->distributionId, $networkDeviceId,$deviceType);

        if ($this->needHiddenFreemiumContent) {
            if (array_key_exists('popup', $streamingObj)) {
                $streamingObj['popup'] = [];
            }
        }
        //Neu noi dung truyen thong GA thi related lay 30 ban ghi
        if ($isShowPopup) {
            $limitRelated = Yii::$app->params['app.googleadwords.relate.limit'];;
        } else {
            $limitRelated = Yii::$app->params['app.video.relate.limit'];
        }

        $relateds = null;
        if (!empty($deviceId) && Yii::$app->params['recommendation']['enable']) {

            $videoIds = RecommendationService::getVideoIds(RecommendationService::BOX_RELATED, $deviceId, $id, [
                'channel_id' => $video['created_by'],
                'channel_name' => $objUser['full_name'],
                'video_name' => $video['name'],
                'tag_name' => $video['tag']
            ], 1, $limitRelated);

            $relateds = null;
            if (!empty($videoIds)) {
                $relateds = VideoObj::serialize(
                    Obj::VIDEO_RECOMMEND_RELATE, VtVideoBase::getVideosByIdsQuery($videoIds, Yii::$app->params['app.search.page.limit'])
                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
                );
            }

        }

        if (!isset($relateds['content']) || count($relateds['content']) == 0) {
            $relateds = VideoObj::serialize(
                Obj::VIDEO_RELATE, VtVideoBase::getVideoRelatedQuery($video['id'], $video['category_id'], $video['created_by'], $limitRelated, 0, $video['published_time']), false
            );
        }

        //$limitRelated


        if ($this->userId || $this->msisdn) {
            $history = VtHistoryViewBase::getByItemId($this->userId, $this->msisdn, $video['type'], $id);
//            $currentTime = ($history) ? intval($history['time']) : 0;
            if ( intval($history['time']) == intval($history['duration']) || intval($history['time']) == intval($history['duration']) - 1){
                $currentTime = 0;
            }else{
                $currentTime = intval($history['time']);
            }
        }
//        echo '<pre>';
//        var_dump($history); die('eee');
//        if ($currentTime == $history['duration'] || $currentTime == $history['duration'] - 1) {
//            $currentTime = 0;
//            $history['time'] = 0;
//        }
//        echo '<pre>';
//        var_dump( $history, $currentTime, $history['time'],  $history['duration']); die('eeee');

        // Kiem tra xem loai playlist can lay
        if ($playlistType == 'ADMIN') {

            // Lay danh sach cac item thuoc playlist
            $items = VtPlaylistItemBase::getItemsByPlaylistId($playlistId);
            $parts = array();
            $cc = 0;
            $firstItemId = 0;
            $prevVideoId = 0;
            $nextVideoId = 0;

            $isVideoInPlaylist = false;

            foreach ($items as $item) {
                if ($firstItemId == 0) {
                    $firstItemId = $item['id'];
                }
                if ($id == $item['id']) {
                    $isVideoInPlaylist = true;
                    if ($cc > 0) {
                        $prevVideoId = $items[$cc - 1]['id'];
                    }
                    if ($cc < count($items) - 1) {
                        $nextVideoId = $items[$cc + 1]['id'];
                    }
                }
                $prefixAlias = Yii::$app->params['alias.prefix'];
                $parts[$cc]['id'] = $item['id'];
                $parts[$cc]['alias'] = $prefixAlias . (($item['alias']) ? $item['alias'] : ($cc + 1));
                $parts[$cc]['name'] = $item['name'];
                $parts[$cc]['slug'] = $item['slug'];
                $cc++;
            }

            if (!$nextVideoId) {
                if (isset($relateds['content']) && count($relateds['content']) > 0) {
                    $nextVideoId = $relateds['content'][0]['id'];
                }
            }
        }

        // Tim vi tri next , prev Video

        //Kiem tra xem the loai co thuoc the loai worlcup khong

        if (strpos(VtConfigBase::getConfig('category.worldcup') . ",", $video['category_id'] . ",") !== false) {
            $detailObj['cate']['is_world_cup_category'] = 1;
        }

        $is_check = 0;

        if($video['is_check'] != null) {
            $is_check = $video['is_check'];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'is_check' => $is_check,
                'detail' => $detailObj,
                'previousId' => $prevVideoId,
                'nextId' => $nextVideoId,
                'profile' => [],
                'streams' => $streamingObj,
                'relateds' => $relateds,
                'parts' => $parts,
                'currentTime' => $currentTime,
                'videoOfPlaylist' => $videoOfPlaylist,
            ]
        ];
    }
    // list category
    public function actionGetCategory()
    {
        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit',Yii::$app->params['app.category.limit']));
        //Danh sach video co luot xem nhieu nhat
        $category = CategoryObj::serialize(
            Obj::CATEGORY_PARENT, VtGroupCategory::getParents($offset, $limit, true, 1), true
        );

        // danh sach video cua danh muc
        if($category['content']){
            foreach ($category['content'] as $key => $item) {
                $id = $item['id'];
                $name = $item['name'];
                $videoBox = VideoObj::serialize(
                    Obj::CATEGORY_CHILD_VIDEO . '_' . $id, VtVideoBase::getVideosByCategory($id, $limit,$offset), false, $name
                );
                $category['content'][$key]['content'] = $videoBox;
            }
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',"Thành công"),
            'data' => $category
        ];

    }
    public function actionGetDetailCategory($id = '', $offset = 0, $limit = 0, $order = 'NEW') {

        if ($limit == 0) {
            $limit = Yii::$app->params['app.category.limit'];
        }

        // Lay thong tin chuyen muc
        $category = VtGroupCategoryBase::getCategoryGroup($id);
        if (empty($category)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        // Lấy lượt view cả category
       // $play_times = VtVideo::getPlayTimesCategory($category['id']);
        $play_times = 0;

        $detailObj = array();
        $detailObj['id'] = $category['id'];
        $detailObj['name'] = $category['name'];
        $detailObj['description'] = $category['description'];
        $detailObj['type'] = $category['type'];
        $detailObj['avatarImage'] = VtHelper::getThumbUrl($category['avatar_bucket'], $category['avatar_path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($category['bucket'], $category['path'], VtHelper::SIZE_CATEGORY);
        $detailObj['play_times'] = number_format($play_times, 0, ',', '.');

        // Lay danh sach video pho bien theo chuyen muc
        $mostTrendingVideos = VideoObj::serialize(
            Obj::VIDEO_MOST_TRENDING_CATE . $id, VtVideoBase::getVideosByCate($id, $limit, $offset, $order), false, Obj::getName(Obj::VIDEO_MOST_TRENDING_CATE)
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
                'most_trending_video' => $mostTrendingVideos,
            ]
        ];
    }

    /**
     * Dang ky dich vu
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionRegisterService()
    {
        if ($this->isValidUser() && !$this->isValidMsisdn()) {
            $objUser = VtUserBase::getById($this->userId);
            if (!$objUser->msisdn) {
                return [
                    'responseCode' => ResponseCode::EMPTY_MSISDN,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            } else {
                $this->msisdn = $objUser->msisdn;
            }

        } else if (!$this->isValidMsisdn()) {
            return $this->authJson;
        }

        if($this->isValidUser() && $this->isValidMsisdn()) {
            /*Kiểm tra db*/
            $userDb = VtUserBase::findOne(['id' => $this->userId, 'msisdn' => $this->msisdn]);
            if(!$userDb) {
                return [
                    'responseCode' => ResponseCode::UNAUTHORIZED,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            }
        }

        $packageId = trim(Yii::$app->request->post('package_id', ''));
        $contentId = trim(Yii::$app->request->post('content_id', ''));

//        if (!$packageId || !preg_match('/^\d+$/', $packageId)) {
//            return [
//                'responseCode' => ResponseCode::UNSUCCESS,
//                'message' => Yii::t('web',"Mã gói cước không hợp lệ")
//            ];
//        }

        // -- Dat log KPI giam sat
        $startTimeLog = round(microtime(true) * 1000);
        $idLog = KpiLogBase::writeLog(KpiLogBase::ACTION_REGISTER, $packageId, KpiLogBase::ACTION_REGISTER);

        $source = Yii::$app->session->get('source', Yii::$app->params['app.source']);

        $errorCode = VtService::registerService($this->msisdn, $packageId, $source);
        $package = VtPackageBase::getDetailMultilang($packageId);
        $blacklistErrorCodes = explode(',', VtConfigBase::getConfig("sendsms_blacklist"));
        $sendSmsSources = explode(',', VtConfigBase::getConfig("sendsms_source"));

        if (in_array($source, $sendSmsSources) && !in_array($errorCode, $blacklistErrorCodes)) {

            $mt = new VtSmsMtBase();
            $content = VtSmsMessageBase::getConfig("register_sms_" . $packageId . "_" . $errorCode);
            if ($content) {
                $mt->sendSms($this->msisdn, $content);
            }
        }


        if ($errorCode === Yii::$app->params['register.reg.success']) {
            $msgConfig = VtConfigBase::getConfig("gateway_register_errorcode_msg_" . $errorCode, Yii::t('web','Đăng ký thành công'));
            // Dat log KPI giam sat
            $endTimeLog = round(microtime(true) * 1000);
            KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => $msgConfig . " " . $package['name'],
                'errorCode'=>977
            ];
        } else {
            $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Đăng ký không thành công"));

            if ($errorCode == 46) {
                $isConfirm = 0;
                //Kiem tra cau hinh xem co can confirm dang ky hay khong?

                switch (Yii::$app->id) {
                    case 'app-wap':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                        break;
                    case 'app-api':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                        break;
                    case 'app-web':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                        break;
                }
                //Neu dang ky sub de xem noi dung(confirm sms)
                if($contentId && $isConfirm){
                    $objContent = VtVideoBase::getDetail($contentId);
                    if($objContent){
                        $msgConfig =  VtConfigBase::getConfig("gateway_errorcode_msg_1234" , "");
                        $objVideo = VtVideoBase::getDetail($contentId);
                        $msgConfig = str_replace("%video_name%", $objVideo['name'], $msgConfig);
                        $msgConfig = str_replace("%package_price%", $package['fee'], $msgConfig);
                        $msgConfig = str_replace("%short_code%",  VtConfigBase::getConfig("SHORTCODE"), $msgConfig);
                        $msgConfig = str_replace("%sms_command%",  VtConfigBase::getConfig("sms_command"), $msgConfig);
                    }
                }

                $msgConfig = str_replace("%package%", $package['name'], $msgConfig);
                $msgConfig = str_replace("%short_code%",  VtConfigBase::getConfig("SHORTCODE"), $msgConfig);
                $msgConfig = str_replace("%sms_command%",  VtConfigBase::getConfig("sms_command"), $msgConfig);

                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => $msgConfig,
                    'content' => VtConfigBase::getConfig('sms_command', 'OK'),
                    'number' => VtConfigBase::getConfig('SHORTCODE', '1515'),
                    'is_confirm_sms' => $isConfirm,
                    'errorCode'=>46
                ];
            } else {
                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_FAIL);
            }

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => $msgConfig,
                'errorCode'=>$errorCode
            ];
        }

    }
}
