<?php

namespace common\modules\v2\controllers;

use api\models\VtUserPlaylistItem;
use common\helpers\ConsoleRunner;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\KpiUploadBase;
use common\models\LogPushNotificationSearchBase;
use common\models\MongoDBModel;
use common\models\VtAccountInfomationBase;
use common\models\VtActionLogBase;
use common\models\VtChannelFollowBase;
use common\models\VtCommentBase;
use common\models\VtCpBase;
use common\models\VtGcmBase;
use common\models\VtGroupCategoryBase;
use common\models\VtHistoryViewBase;
use common\models\VtReportUserUploadBase;
use common\models\VtUserBase;
use common\models\VtUserFollowBase;
use common\models\VtUserOtpBase;
use common\models\VtUserPlaylistBase;
use common\models\VtUserPlaylistItemBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\ChannelObj;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\UserPlaylistObj;
use cp\models\VtChannel;
use cp\models\VtVideo;
use Yii;
use common\modules\v2\libs\ResponseCode;
use yii\helpers\ArrayHelper;
use common\models\VtChannelBase;

class AccountController extends \common\modules\v1\controllers\AccountController
{

    public function actionTestUnmap()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $objUser = VtUserBase::getById($this->userId);
        $objUser->msisdn = '';
        $objUser->save();

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap','Thành công')
        ];
    }

    public function actionDeleteHistoryView()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $ids = str_replace(' ', '', trim(Yii::$app->request->post('ids', '')));
        $idArr = explode(',', trim($ids, ','));
        if (strtoupper($ids) == 'ALL') {
            VtHistoryViewBase::deleteAll($this->userId, $this->msisdn);
        } elseif (!empty($idArr)) {
            VtHistoryViewBase::delete($this->userId, $this->msisdn, $idArr);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap','Thành công')
        ];
    }


    public function actionUploadFile()
    {
        // 2021-01-29: disable upload on mobile
        /*$currentHour = (int)date('H', time());
        
        if($currentHour < 9 || $currentHour > 15) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => "Method Not Allowed"
            ];
        }*/

        if (!$this->isValidUser()) {
            return $this->authJson;
        }


        $AllChannel = VtChannelBase::getChannelById($this->userId);
        if(count($AllChannel) == 10){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ')
            ];
        }

        // Specify the input name set in the javascript.
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {

            set_time_limit(1200);
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

            $extImage = pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($ext), ['mp4', 'flv', '.mp4', '.flv', 'mov', '.mov', 'm4u', '.m4u'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Định dạng file video upload không đúng")
                ];
            }
           
            if ($_FILES['file']['size'] > 943718400) { //900 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' =>  Yii::t('wap',"Kích thước file video tối đa 900MB")
                ];
            }

            if (!in_array(strtolower($extImage), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Định dạng file thumbnail upload không đúng")
                ];
            }

            if ($_FILES['thumbnail']['size'] > 5242880) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Kích thước thumbnail tối đa 5MB")
                ];
            }

            $fileBucket = Yii::$app->params['s3']['video.bucket'];
            $imageBucket = Yii::$app->params['s3']['static.bucket'];

            $filePath = Utils::generatePath($ext);

            $fileImagePath = Utils::generatePath($extImage);

            $videoName = trim(Yii::$app->request->post("title"));
            $description = trim(Yii::$app->request->post("description"));
            $sessionId = trim(Yii::$app->request->post("session_id"));
            $mode = Yii::$app->request->post("mode", VtVideoBase::MODE_PUBLIC);
            $category_id = trim(Yii::$app->request->post("category_id", 0));
            $channel_id = trim(Yii::$app->request->post("channel_id"));
            if($channel_id) {
                $check_channel = VtChannel::CheckChannelUser($channel_id,$this->userId);
                if (empty($check_channel)) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Kênh không tồn tại.")
                    ];
                }
            }else{
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Vui lòng nhập ID Kênh của user.")
                ];
            }

            if (mb_strlen($videoName, 'UTF-8') > 255 || mb_strlen($videoName) <= 0) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Tên video phải nhập và số kí tự không lớn hơn 255 kí tự"),
                ];
            }

            if (mb_strlen($description, 'UTF-8') > 500) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Mô tả video không được lớn hơn 500 kí tự"),
                ];
            }

            if (!in_array($mode, [VtVideoBase::MODE_PRIVATE, VtVideoBase::MODE_PUBLIC])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Trạng thái không hợp lệ")
                ];
            }


//            $arrS3 = S3Service::putObject($fileBucket, $_FILES['file']['tmp_name'], $filePath);
//            Yii::info("S3 PUSH FILE=" . json_encode($arrS3) . "|" . $_FILES['file']['tmp_name'] . "|" . $filePath);

            //PUSH Image file
            // tam thơi dong lai vi khong up anh dai dien video
            $arrImageS3 = S3Service::putObject($imageBucket, $_FILES['thumbnail']['tmp_name'], $fileImagePath);
            Yii::info("S3 PUSH IMAGE FILE=" . json_encode($arrImageS3) . "|" . $_FILES['thumbnail']['tmp_name'] . "|" . $fileImagePath);
            VtHelper::generateAllThumb($imageBucket, $_FILES['thumbnail']['tmp_name'], $fileImagePath);

//            if ($arrS3['errorCode'] == S3Service::UNSUCCESS) {
//                return [
//                    'responseCode' => ResponseCode::UNSUCCESS,
//                    'message' => "Upload file cloud không thành công. Vui lòng thử lại!"
//                ];
//            }
            // tam thơi dong lai vi khong up anh dai dien video
            if ($arrImageS3['errorCode'] == S3Service::UNSUCCESS) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Upload ảnh lên cloud không thành công. Vui lòng thử lại!")
                ];
            }

            $cpId = null;
            $cpCode = null;
            $outsourceNeedReview = VtVideo::OUTSOURCE_NEED_APPROVE;
            $objUser = VtUserBase::getById($this->userId);

            if (!empty($objUser->cp_id)) {
                $cp = VtCpBase::getOneById($objUser->cp_id);

                if (!empty($cp)) {
                    $cpId = $cp['id'];
                    $cpCode = $cp['cp_code'];

                    //Kiem tra xem CP_CODE co phai user upload khong
                    if (strstr($cpCode, "USER_UPLOAD")) {
                        $outsourceNeedReview = VtVideo::OUTSOURCE_NEED_APPROVE;
                    } else {
                        $outsourceNeedReview = VtVideo::OUTSOURCE_DONT_NEED_APPROVE;
                    }

                }
            }

            $video = new VtVideo();

            //- Kiem tra xem khach hang da co thong tin ca nhan hay chua?Neu chua co thi chuyen status =0
            $objInfo = VtAccountInfomationBase::getByUserId($this->userId);
            if ($objInfo) {
                $status = VtVideoBase::STATUS_DRAFT;
            } else {
                $status = VtVideoBase::STATUS_TEMP;
            }
            $storageType = Yii::$app->params['storage.type'];
            $convertStatus = ($storageType == 0) ? VtVideoBase::CONVERT_STATUS_DRAFT : VtVideoBase::CONVERT_STATUS_TEMP;
            if($storageType == 0){ // Ko co CDN
                //lấy thư mục ảnh
                $san = Yii::$app->params['local.image'];
                //sinh ra tên thư mục con
                $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                //sinh ra tên file
                $fileName = Utils::generateUniqueFileName($extImage);
                //tạo thư mục lưu ảnh từ đường dẫn server
                $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                if (!mkdir($childFolder, 0777, true)) {
                    return ('Failed to create folders...');
                }
                //đường dẫn lưu vào cơ sở dữ liệu
                $path = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                //đường dẫn cắt ảnh
                $fileAvatarLocalPath = Yii::getAlias('@uploadFolder'). DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                //đường dẫn lưu ảnh đã cắt ( lưu đường dẫn cắt ảnh thì lấy cùng tên)
                $orgpath = $fileAvatarLocalPath ;
                // hàm lưu ảnh vào server
                move_uploaded_file($_FILES['thumbnail']['tmp_name'], $orgpath);
                // tạo ảnh cắt thumb
                VtHelper::generateAllThumb($fileBucket, $fileAvatarLocalPath, $orgpath,[],true);
            }

            $video->insertVideo($videoName, $description, $fileBucket, $filePath, VtVideoBase::TYPE_VOD, $cpId,
                $this->userId, $category_id, $convertStatus, $bucket = '', $path,
                $mode, '', $cpCode, $outsourceNeedReview, $status,$channel_id);

            //Backgroud upload
            $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $video->id . '.' . $ext;
            move_uploaded_file($_FILES['file']['tmp_name'], $orgpath);

            $kpi = KpiUploadBase::getBySessionId($sessionId);
            if ($kpi) {
                $now = strtotime('now');
                $kpi->video_id = $video->id;
                $kpi->upload_end_time = date('Y-m-d H:i:s', $now);
                $kpi->upload_duration = $now - strtotime($kpi->upload_start_time);
                $kpi->size = $_FILES['file']['size'];
                $kpi->save(false);
            }
// tam thơi dóng lai vi local không chay dc
            $cr = new ConsoleRunner(['file' => '@yiiConsole']);
            $cr->run('upload/put-object --id=' . $video->id . ' --orgPath=' . $orgpath . ' --sessionId=' . $sessionId);


            // Luu action log
//            $actionLog = "(" . $this->userId . ")" . "[" . $this->msisdn . "]" . $video->name;
//            VtActionLogBase::insertLog(VtActionLogBase::M_API_VIDEO, $video->id, VtActionLogBase::T_CREATE, $actionLog, $this->userId, "API");

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('web',"Đăng tải thành công"),
                'data' => [
                    "videoId" => $video->id,
                    "isHasAccountInfo" => ($objInfo) ? true : false
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => "Method Not Allowed"
            ];
        }
    }

    public function actionKpiUploadStart()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $sessionId = trim(Yii::$app->request->post('session_id', ''));

        $kpi = new KpiUploadBase();
        $kpi->session_id = $sessionId;
        $kpi->upload_start_time = date('Y-m-d H:i:s');
        $kpi->save();
        
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap','Thành công')
        ];
    }


    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionFollowChannel()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $followId = intval(trim(Yii::$app->request->post('id', 0)));
        $status = intval(trim(Yii::$app->request->post('status', -1)));
        $notificationType = intval(trim(Yii::$app->request->post('notification_type', 2)));

        if (!in_array($status, [VtChannelFollowBase::ACTIVE, VtChannelFollowBase::DEACTIVE])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Trạng thái không hợp lệ'),
            ];
        }

        if (!in_array($notificationType, [VtChannelFollowBase::NOTIFICATION_TYPE_ALWAYS, VtChannelFollowBase::NOTIFICATION_TYPE_NEVER, VtChannelFollowBase::NOTIFICATION_TYPE_SOMETIME])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Trạng thái thông báo không hợp lệ'),
            ];
        }

        if (!$followId) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        $objUser = VtChannelBase::getChannelById($followId);
        if (!$objUser) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap',"Kênh cần theo dõi không tồn tại!")
            ];
        }
        if ($this->userId == $objUser['user_id']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap',"Không thể theo dõi kênh chính mình!")
            ];
        }

        $followCount = $objUser->follow_count;
        $isFollow = false;

        $followObj = VtChannelFollowBase::getFollow($this->userId, $followId);

        if (!empty($followObj) && $status == VtChannelFollowBase::UNFOLLOW) {

            $followObj->delete();
            $isFollow = false;
            $followCount--;

          VtUserBase::updateFollowCount($objUser['user_id'], $isFollow);
          VtChannelBase::updateFollowCount($followId, $isFollow);
        } elseif (empty($followObj) && $status == VtChannelFollowBase::FOLLOW) {

            $followObj = new VtChannelFollowBase();
            $followObj->insertFollow($this->userId, $followId, $notificationType);
            $isFollow = true;
            $followCount++;
           VtUserBase::updateFollowCount($objUser['user_id'], $isFollow);
            VtChannelBase::updateFollowCount($followId, $isFollow);
            //Dat log event follow channel
           // MongoDBModel::insertEvent(MongoDBModel::FOLLOW, $followId, $this->userId, $this->msisdn, "", $followId);
        } elseif (!empty($followObj) && $status == VtChannelFollowBase::FOLLOW) {
            if ($followObj->notification_type == $notificationType) {
                $isFollow = true;
            } else {
                $followObj->notification_type = $notificationType;
                $followObj->save();
                $isFollow = true;
            }
            //Dat log event follow channel
           // MongoDBModel::insertEvent(MongoDBModel::FOLLOW, $followId, $this->userId, $this->msisdn, "", $followId);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isFollow' => intval($isFollow),
                'followCount' => intval($followCount)
            ]
        ];
    }

    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionFollowMultiChannel()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $ids = trim(Yii::$app->request->post('ids', ''));
        $notificationType = intval(trim(Yii::$app->request->post('notification_type', 2)));


        if (empty($ids)) {
            $channelIds = [];
        } else {
            $channelIds = array_reverse(explode(',', $ids));
        }

        //Merge xoa followId cu
        $actualChannels = VtUserFollowBase::find()
            ->andWhere(["user_id" => $this->userId])
            ->asArray()
            ->all();

        if (!empty($actualChannels)) { //delete authors tha does not belong anymore
            $followIds = ArrayHelper::getColumn($actualChannels, 'id');
            $actualChannelIds = ArrayHelper::getColumn($actualChannels, 'follow_id');
            //            $actualChannels->delete();

            VtUserFollowBase::deleteAll(['id' => $followIds]);
            VtUserBase::updateMultiFollowCount($actualChannelIds, false);
        }

        if (!empty($channelIds)) { //save the relations
            foreach ($channelIds as $channelId) {
                $followObj = new VtUserFollowBase();
                $followObj->insertFollow($this->userId, $channelId, $notificationType);
            }
            VtUserBase::updateMultiFollowCount($channelIds, true);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    public function actionGetNotification()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $limit = trim(Yii::$app->request->get('limit', 10));
        $offset = trim(Yii::$app->request->get('offset', 0));
        $dataResponse = array();

        $items = [];

        $listvideos = VtVideoBase::getAllVideosCommentByUser('', $this->userId, null, null, VtVideoBase::TYPE_VOD)->all();
        $arrVideo = [];
        foreach ($listvideos as $v){
            array_push($arrVideo, $v['id']);
        }
        $count = 0;
        if (!empty($listvideos)) {
            foreach ($listvideos as $key => $video) {
                $arrComment = VtCommentBase::getNotifyByUserId($this->userId,$video['id'], null, null,0);
                if(count($arrComment) > 0){
                    $user = VtUserBase::getById($arrComment[0]['user_id']);
                    if(isset($user)){
                        $count++;
                        $userName = $user['full_name'] ? $user['full_name']:$user['msisdn'];
                        $title = $userName.' '. Yii::t('wap','vừa thêm bình luận mới cho video').' '. $video['name'];
                        $item = [];
                        $item['isComment'] = 1;
                        $item['id'] = $video['id'];
                        $item['link'] = '/user/'.$this->userId.'?view_comment='.$video['id'].'&comment_id='.$arrComment[0]['id'].'&comment_status=0';
                        $item['aps'] = [
                            'alert' => $title,
                            'content' => $arrComment[0]['comment']
                        ];
                        $item['last_comment'] = $arrComment[0]['id'];
                        $item['sent_time'] = date("Y-m-d H:i:s", strtotime($arrComment[0]['created_at']));
                        $item['sent_time_format'] = Utils::time_elapsed_string(date("Y-m-d H:i:s", strtotime($arrComment[0]['created_at'])));
                        $item['type'] = $arrComment[0]['type'];
                        $item['full_name'] = ($user['full_name']) ? Utils::truncateWords($user['full_name'], 20) : (substr($user['msisdn'], 0, -3) . "xxx");
                        $item['user_id'] = $user['id'];
                        $item['avatarImage'] = VtHelper::getThumbUrl($user['bucket'], $user['path'], VtHelper::SIZE_AVATAR);;
                        $item['video_id'] = $video['id'];
                        $item['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_VIDEO);
                        $items[] = $item;
                    }

                }

            }
        }
        usort($items, function($a, $b) {
            return ($a['sent_time'] > $b['sent_time']) ? -1 : 1;
        });
        $items = array_slice($items, $offset, $limit);
        $dataResponse['notifications'] = $items;

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }
    public function actionGetNumberNotification(){
        $count = 0;
        $listvideos = VtVideoBase::getAllVideosCommentByUser('', $this->userId, null, null, VtVideoBase::TYPE_VOD)->all();
        if (!empty($listvideos)) {
            foreach ($listvideos as $key => $video) {
                $arrComment = VtCommentBase::getNotifyByUserId($this->userId, $video['id'], null, null, 0);
                if (count($arrComment) > 0) {
                    $user = VtUserBase::getById($arrComment[0]['user_id']);
                    if (isset($user)) {
                        $count++;
                    }
                }
            }
        }
        if($count >=100){
            $count = '99+';
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'count' => $count
            ]
        ];
    }
    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionMarkNotification()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', 0));

        $log = LogPushNotificationSearchBase::getLogsById($id);

        if (empty($log)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        if ($log->user_id != $this->userId) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap',"Bạn không đủ quyền thực hiện chức năng này!")
            ];
        }

        if ($log->is_read == 0) {
            $log->is_read = 1;
            $log->save();
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    public function actionToggleNotification()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $user = VtUserBase::getActiveUserById($this->userId);

        if (empty($user)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        if ($user->is_notification == VtUserBase::NOTIFICATION_READ) {
            $user->is_notification = VtUserBase::NOTIFICATION_NOT_READ;
            $isNotification = false;
        } else {
            $user->is_notification = VtUserBase::NOTIFICATION_READ;
            $isNotification = true;
        }
        $user->save();


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isNotification' => $isNotification
            ]
        ];
    }

    public function actionGetFollowChannel()
    {
//        if (!$this->isValidUser()) {
//            return $this->authJson;
//        }

        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit', Yii::$app->params['app.showMore.limit']));
        $includeHot = trim(Yii::$app->request->get('include_hot', 0));

        if (!empty($this->userId)) {
            $contents = ChannelObj::serialize(
                Obj::CHANNEL_FOLLOW, VtUserFollowBase::getChannelFollowQuery($this->userId, $limit, $offset), false, false, ($includeHot == 0) ? false : true, $limit
            );
        } else {
            $contents = ChannelObj::serialize(
                Obj::CHANNEL_FOLLOW, null, false, false, ($includeHot == 0) ? false : true, $limit
            );
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        ];
    }

    public function actionGetFollowContent($channelId=0)
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        //Lay danh sach kenh ma nguoi dung theo doi
        $channelFollow = ChannelObj::serialize(
            Obj::CHANNEL_FOLLOW, VtUserFollowBase::getChannelFollowQuery($this->userId), false, false
        );
        $arrCF = [];
        foreach ($channelFollow['content'] as $cF) {
            $arrCF[] = intval($cF['channel_id']);
        }

        $arrCF [] = $this->userId;

        $channelHot = ChannelObj::serialize(
            Obj::CHANNEL_HOT, VtUserBase::getHotUserQuery(Yii::$app->params['app.follow.content.limit'], 0, $arrCF), false, false, false, Yii::$app->params['app.follow.content.limit']
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'channel_follow' => $channelFollow,
                'channel_hot' => $channelHot,
            ]
        ];
    }

    public function actionGetMyPlaylists($limit = null, $offset = null)
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $contents = UserPlaylistObj::serialize(
            Obj::MY_PLAYLIST, VtUserPlaylistItemBase::getPlaylistByUserQuery($this->userId, $limit, $offset), false, false
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        ];
    }

    public function actionRegisterClientId()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $clientId = trim(Yii::$app->request->post('client_id', ''));
        $type = trim(Yii::$app->request->post('type', 0));

        if (empty($clientId) || !in_array($type, [1, 2, 3])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' =>  Yii::t('wap','Dữ liệu không hợp lệ'),
            ];
        }

//        $mtvGcm = VtGcmBase::getByUserId($this->userId);
        $mtvGcm = VtGcmBase::getByRegisterId($clientId);

        Yii::info('{RegisterClientId} userId:' . $this->userId . '|msisdn:' . $this->msisdn . '|registerId:' . $clientId . '|type:' . $type, 'info');

        if (!$mtvGcm) {
            $mtvGcm = new VtGcmBase();
            $mtvGcm->insertClientId($this->userId, $this->msisdn, $clientId, VtGcmBase::STATUS_ACTIVE, $type);
        } else {
            $mtvGcm->register_id = $clientId;
            $mtvGcm->msisdn = $this->msisdn;
            $mtvGcm->status = VtGcmBase::STATUS_ACTIVE;
            $mtvGcm->type = $type;
            $mtvGcm->save(false);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
        ];
    }

    public function actionUpdateMedia()
    {
        $id = trim(Yii::$app->request->post('id', ''));
        $name = trim(Yii::$app->request->post('name', ''));
        $description = trim(Yii::$app->request->post('description', ''));
        $tag = trim(Yii::$app->request->post('tag', ''));
        $mode = trim(Yii::$app->request->post('mode', VtVideoBase::MODE_PUBLIC));
        $category = trim(Yii::$app->request->post('category', ''));
        $channel_id = trim(Yii::$app->request->post('channel_id', ''));

        if (mb_strlen($name, 'UTF-8') > 255 || mb_strlen($name) <= 0) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap',"Tên video phải nhập và số kí tự không lớn hơn 255 kí tự"),
            ];
        }

        if (mb_strlen($description, 'UTF-8') > 500) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap',"Mô tả video không được lớn hơn 500 kí tự"),
            ];
        }

        if (!in_array($mode, [VtVideoBase::MODE_PRIVATE, VtVideoBase::MODE_PUBLIC])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => "Trạng thái không hợp lệ"
            ];
        }

        // if( ($video['created_by'] == $this->userId) && ($video['status'] == VtVideoBase::STATUS_TEMP || $video['status'] == VtVideoBase::STATUS_DRAFT )   ){
        if(($video = VtVideoBase::findOne($id)) && $video['status'] == VtVideoBase::STATUS_TEMP || $video['status'] == VtVideoBase::STATUS_DRAFT  ){
            VtVideo::updateMedia($id, $name, $description, $tag, $mode, $category,$channel_id);
            VtChannelBase::updateVideoCountByChannelId($channel_id);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',"Cập nhật thành công"),
            'data' => [
                "videoId" => $id
            ]
        ];
    }

    public function actionAccountInfomation()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $account = VtAccountInfomationBase::getByUserId(['user_id' => $this->userId]);
        if ($account) {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Đã có thông tin khách hàng'),
            ];
        }

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $isNew = false;
            if (!$account) {
                $account = $postData;
                $isNew = true;
            }
            //TODO validate
            if (!trim($postData['name'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Yêu cầu nhập họ và tên'),
                ];
            }

            if (!trim($postData['email'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Yêu cầu nhập email'),
                ];
            }

            if (!trim($postData['id_card_number'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Yêu cầu nhập Số chứng minh thư'),
                ];
            }
            $msisdn = trim($postData['msisdn']);
            $otp = trim($postData['otp']);
            if (!$msisdn) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Yêu cầu nhập số điện thoại unitel'),
                ];
            }
            if (($_FILES["id_card_image_frontside"]["size"] == 0)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Yêu cầu nhập mặt trước ảnh chứng minh thư'),
                ];
            }

            if (($_FILES["id_card_image_backside"]["size"] == 0)) {
                Yii::$app->session->setFlash("info", Yii::t('wap',"Yêu cầu nhập mặt sau ảnh chứng minh thư"));
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' =>  Yii::t('wap','Yêu cầu nhập mặt sau ảnh chứng minh thư'),
                ];
            }

            $fileBucket = Yii::$app->params['s3']['static.bucket'];

            if ($_FILES["id_card_image_frontside"]['name']) {
                //-- Validate image card frontside
                $ext = pathinfo($_FILES["id_card_image_frontside"]['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' =>  Yii::t('wap','Định dạng file ảnh mặt trước không đúng'),
                    ];
                }

                if ($_FILES["id_card_image_frontside"]['size'] > 8388608) { //8 MB (size is also in bytes)
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' =>  Yii::t('wap','File ảnh mặt trước dung lượng tối đa 8MB'),
                    ];
                }
                try {
                    $filePathFrontside = Utils::generatePath($ext);
                    S3Service::putObject($fileBucket, $_FILES["id_card_image_frontside"]['tmp_name'], $filePathFrontside);
                } catch (\Exception $exception) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Có lỗi upload ảnh, vui lòng thử lại sau'),
                    ];
                }

            }

            if ($_FILES["id_card_image_backside"]['name']) {
                //-- Validate image card backside
                $ext = pathinfo($_FILES["id_card_image_backside"]['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Định dạng file ảnh mặt sau không đúng'),
                    ];
                }

                if ($_FILES["id_card_image_backside"]['size'] > 8388608) { //8 MB (size is also in bytes)
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','File ảnh mặt sau dung lượng tối đa 8MB'),
                    ];
                }


                try {
                    $filePathBackside = Utils::generatePath($ext);
                    S3Service::putObject($fileBucket, $_FILES["id_card_image_backside"]['tmp_name'], $filePathBackside);
                } catch (\Exception $exception) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Có lỗi upload ảnh, vui lòng thử lại sau'),
                    ];
                }
            }
            $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
            if (VtUserOtpBase::checkOTP($msisdn, $otp)) {
            } else {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Mã OTP không hợp lệ, vui lòng lấy lại mã OTP'),
                ];
            }
            // Deactive OTP sau moi lan su dung
            if ($otp) {
                VtUserOtpBase::deactiveAllOtp($msisdn);
            }

            //
            if ($isNew) {
                $account = new VtAccountInfomationBase();
            }

            $account->name = trim($postData['name']);
            $account->email = trim($postData['email']);
            $account->id_card_number = trim($postData['id_card_number']);
            $account->id_card_created_at = trim($postData['id_card_created_at']);
            $account->id_card_created_by = trim($postData['id_card_created_by']);
            $account->user_id = $this->userId;
            $account->msisdn = $msisdn;

            if ($filePathFrontside) {
                $account->id_card_image_frontside = $filePathFrontside;
                $account->bucket = $fileBucket;
            }
            if ($filePathBackside) {
                $account->id_card_image_backside = $filePathBackside;
                $account->bucket = $fileBucket;
            }
            //--
            if (!$account->created_at) {
                $account->created_at = date("Y-m-d H:i:s");
                $account->created_by = $this->userId;
            }
            //--
            $account->updated_at = date("Y-m-d H:i:s");
            $account->updated_by = $this->userId;
            $account->save(false);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Hoàn thành thông tin khách hàng'),
            ];
        }

        if (!$account) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Cần nhập thông tin khách hàng'),
            ];
        }
    }

    public function actionReportUserUpload($limit = 0)
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        if (empty($limit)) {
            $limit = Yii::$app->params['report.limit'];
        }

        $currentRevenue = VtReportUserUploadBase::getRevenueByMonth($this->userId, date('Y-m', strtotime('now')));

        $histories = VtReportUserUploadBase::getByUserId($this->userId, $limit, 0, true);
        //var_dump($histories);die;
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'currentRevenue' => $currentRevenue,
                'currentRevenueStr' => Yii::$app->formatter->asDecimal($currentRevenue, 0),
                'histories' => $histories,
                'hintMessage' => Yii::t('wap','Chúng tôi sẽ thực hiện chi trả khi doanh thu đạt ngưỡng 500.000 KIP')
            ]
        ];
    }

    public function actionReportUserUploadHistory($limit = 0, $offset = 0)
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        if (empty($limit)) {
            $limit = Yii::$app->params['report.limit'];
        }

        $histories = VtReportUserUploadBase::getByUserId($this->userId, $limit, $offset, false);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'histories' => $histories
            ]
        ];

    }

    public function actionGetDurationWatchedByIds()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $strIds = Yii::$app->request->get("ids", "");
        $ids = explode(",", $strIds);

        if (!empty($ids)) {
            $histories = VtHistoryViewBase::getTimeByIds($this->userId, $this->msisdn, $ids, VtVideoBase::TYPE_VOD);
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'histories' => $histories
                ]
            ];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => []
        ];
    }


}
