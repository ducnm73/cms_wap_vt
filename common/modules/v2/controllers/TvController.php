<?php

namespace common\modules\v2\controllers;

use common\controllers\ApiController;
use common\models\VtSmartTvTokenBase;
use common\modules\v1\libs\ResponseCode;
use Yii;

class TvController extends ApiController
{

    public function actionGetCode()
    {
        $token = trim(Yii::$app->request->get("token", ""));

        if(!$token){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Token không hợp lệ'),
            ];
        }

        VtSmartTvTokenBase::deactiveCode($token);
        $code = VtSmartTvTokenBase::insertNewCode($token);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap', 'Thành công'),
            'data' => [
                'code' => $code
            ]
        ];
    }

    public function actionMapCode($code='')
    {
        if (!$this->isValidUser() && !$this->isValidMsisdn()) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Quý khách vui lòng đăng nhập tính năng này!'),
            ];
        }
        if(!$code){
            $code = Yii::$app->request->post("code", "");
        }
        $code = trim($code);
        //B1: kiem tra
        $objCode = VtSmartTvTokenBase::checkCode($code);
        if ($objCode) {
            $hourExpireActivate = Yii::$app->params['expire.time.activate.smartTV'];

            VtSmartTvTokenBase::updateAll(
                [
                    'msisdn' => $this->msisdn,
                    'user_id' => $this->userId,
                    'expire_time' => date("Y-m-d H:i:s", strtotime("+" . $hourExpireActivate . " hours"))],
                'id = :id', [':id' => $objCode['id']]);
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Không tìm thấy mã SmartTV')
            ];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap', 'Thành công')
        ];

    }
}