<?php

namespace common\modules\v2\controllers;

use common\libs\VtHelper;
use common\models\VtPlaylistItemBase;
use common\models\VtUserPlaylistBase;
use common\models\VtUserPlaylistItemBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\ResponseCode;
use common\helpers\Utils;
use common\modules\v2\libs\UserPlaylistObj;
use Yii;

class PlaylistController extends \common\modules\v1\controllers\PlaylistController {

    public function actionToggleAddVideo() {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', 0));
        $videoId = trim(Yii::$app->request->post('video_id', 0));
        $status = trim(Yii::$app->request->post('status', ''));

        $video = VtVideoBase::getDetail($videoId);

        if (empty($video)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        $userPlaylistObj = VtUserPlaylistBase::getDetail($id, true);

        if (empty($userPlaylistObj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        if ($userPlaylistObj->user_id != $this->userId && $userPlaylistObj != $this->msisdn) {
            return [
                'responseCode' => ResponseCode::NOT_OWNER,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_OWNER),
            ];
        }

        $playlistItemObj = VtUserPlaylistItemBase::getByPlayListAndItemId($id, $videoId);

        $isAdd = false;

        if (!empty($playlistItemObj)) {//Da ton tai ban ghi
            if ($status == VtUserPlaylistItemBase::STATUS_REMOVE) {
                $playlistItemObj->delete();
                $isAdd = false;
                VtUserPlaylistBase::updateNumVideo($id, -1);
            } elseif ($status == VtUserPlaylistItemBase::STATUS_ADD) {
                $isAdd = true;
            }
        } else {//Chua ton tai ban ghi
            if ($status == VtUserPlaylistItemBase::STATUS_ADD) {

                $count = VtUserPlaylistItemBase::countItemOfPlayList($id);
                if($count == 0){
                    $userPlaylistObj->bucket = $video['bucket'];
                    $userPlaylistObj->path = $video['path'];
                    $userPlaylistObj->save();
                }

                $favObj = new VtUserPlaylistItemBase();
                $favObj->insertItem($videoId, $id);
                $isAdd = true;
                VtUserPlaylistBase::updateNumVideo($id, 1);
            } elseif ($status == VtUserPlaylistItemBase::STATUS_REMOVE) {
                $isAdd = false;
            }
        }






        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isAdd' => $isAdd
            ]
        ];
    }

    public function actionCreate() {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $name = trim(Yii::$app->request->post('name', ''));
        $description = trim(Yii::$app->request->post('description', ''));

        if (strlen(utf8_decode($name)) < Yii::$app->params['playlist.name.minlength'] || strlen(utf8_decode($name)) > Yii::$app->params['playlist.name.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Quý khách vui lòng nhập tên từ {from} đến {to} ký tự', [
                    'from' => Yii::$app->params['playlist.name.minlength'],
                    'to' => Yii::$app->params['playlist.name.maxlength'],
                ]),
            ];
        }

        if (!empty($description) && (strlen(utf8_decode($description)) < Yii::$app->params['playlist.description.minlength'] || strlen(utf8_decode($description)) > Yii::$app->params['playlist.description.maxlength'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập mô tả từ ') . Yii::$app->params['playlist.description.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['playlist.description.maxlength'] . Yii::t('wap',' ký tự.')
            ];
        }



        $userPlaylist = new VtUserPlaylistBase();
        $userPlaylist->insertItem($this->msisdn, $this->userId, VtUserPlaylistBase::TYPE_USER, $name, $description, VtUserPlaylistBase::ACTIVE, VtUserPlaylistBase::STATUS_APPROVE);


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'playlist' => [
                    'id' => $userPlaylist->id,
                    'name' => $userPlaylist->name,
                    'description' => $userPlaylist->description,
                    'num_video' => $userPlaylist->num_video,
                    'coverImage' => VtHelper::getThumbUrl($userPlaylist->bucket, $userPlaylist->path, VtHelper::SIZE_VIDEO),
                    'type' => "USER_PLAYLIST",
                ]
            ]
        ];
    }

    public function actionUpdate() {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', 0));
        $name = trim(Yii::$app->request->post('name', ''));
        $description = trim(Yii::$app->request->post('description', ''));

        if (empty($name) || strlen(utf8_decode($name)) < Yii::$app->params['playlist.name.minlength'] || strlen(utf8_decode($name)) > Yii::$app->params['playlist.name.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập tên từ ') . Yii::$app->params['playlist.name.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['playlist.name.maxlength'] . Yii::t('wap',' ký tự.')
            ];
        }

        if (!empty($description) && (strlen(utf8_decode($description)) < Yii::$app->params['playlist.description.minlength'] || strlen(utf8_decode($description)) > Yii::$app->params['playlist.description.maxlength'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập mô tả từ ') . Yii::$app->params['playlist.description.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['playlist.description.maxlength'] . Yii::t('wap',' ký tự.')
            ];
        }

        $userPlaylistObj = VtUserPlaylistBase::getDetail($id, true);

        if (empty($userPlaylistObj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        //Kiem tra quyen
        if ($userPlaylistObj->user_id != $this->userId && $userPlaylistObj != $this->msisdn) {
            return [
                'responseCode' => ResponseCode::NOT_OWNER,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_OWNER),
            ];
        }


        if (!empty($name)) {
            $userPlaylistObj->name = $name;
        }

        $userPlaylistObj->description = $description;

        $userPlaylistObj->save();

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    public function actionDelete() {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $id = trim(Yii::$app->request->post('id', 0));
        $userPlaylistObj = VtUserPlaylistBase::getDetail($id, true);
        if (!$userPlaylistObj) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }


        //Kiem tra quyen
        if ($userPlaylistObj->user_id != $this->userId && $userPlaylistObj != $this->msisdn) {
            return [
                'responseCode' => ResponseCode::NOT_OWNER,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_OWNER),
            ];
        }
        $userPlaylistObj->status = VtUserPlaylistBase::STATUS_DELETE;
        $userPlaylistObj->save();

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    public function actionGetDetailPlaylist() {
        /*
         // Comment: chua check phan quyen, tat ca playlist deu public
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        */
        $id = trim(Yii::$app->request->get('id', 0));
        $userPlaylistObj = VtUserPlaylistBase::getDetail($id, false);
        //var_dump($userPlaylistObj);die;
        if (empty($userPlaylistObj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        //Kiem tra quyen
        // Comment: chua check phan quyen, tat ca playlist deu public

        /*
        if ($userPlaylistObj->user_id != $this->userId && $userPlaylistObj != $this->msisdn) {
            return [
                'responseCode' => ResponseCode::NOT_OWNER,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_OWNER),
            ];
        }
        */


        $num_count = VtVideoBase::getVideosByPlaylist($id)->count();

        $obj = array();
        $obj["id"] = $userPlaylistObj['id'];
        $obj["name"] = $userPlaylistObj['name'];
        $obj["description"] = $userPlaylistObj['description'];
        $obj['num_video'] = $num_count;
        $obj['avatarImage'] = VtHelper::getThumbUrl($userPlaylistObj['bucket'], $userPlaylistObj['path'], VtHelper::SIZE_AVATAR);
        $obj['coverImage'] = VtHelper::getThumbUrl($userPlaylistObj['bucket'], $userPlaylistObj['path'], VtHelper::SIZE_COVER);
        $obj['created_at'] = Utils::time_elapsed_string($userPlaylistObj['created_at']);
        $obj['userId'] = $userPlaylistObj['user_id'];

        $obj['userName'] = Utils::truncateWords(($userPlaylistObj['full_name']) ? $userPlaylistObj['full_name'] : $userPlaylistObj['msisdn'], 40);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $obj
        ];
    }

}
