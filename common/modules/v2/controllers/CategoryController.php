<?php

namespace common\modules\v2\controllers;

use backend\models\VtVideo;
use Yii;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\CategoryObj;
use common\modules\v2\libs\ResponseCode;
use common\models\VtGroupCategoryBase;
use common\libs\VtHelper;
use api\models\VtGroupCategory;
use common\models\VtVideoBase;
use common\modules\v2\libs\VideoObj;

class CategoryController extends \common\controllers\ApiController {

    public function actionGetHomeCategory($offset = 0, $limit = 0) {

        $isHot = Yii::$app->request->get("is_hot", false);

        if ($limit == 0) {
            $limit = Yii::$app->params['app.category.limit'];
        }

        //Danh sach video co luot xem nhieu nhat
        $category = CategoryObj::serialize(
                        Obj::CATEGORY_PARENT, VtGroupCategory::getParents($offset, $limit, true, $isHot), true
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',"Thành công"),
            'data' => $category
        ];
    }
    // list category
    public function actionGetCategory()
    {
        $isHot = Yii::$app->request->get("is_hot", false);

        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit',Yii::$app->params['app.category.limit']));

        //Danh sach video co luot xem nhieu nhat
        $category = CategoryObj::serialize(
            Obj::CATEGORY_PARENT, VtGroupCategory::getParents($offset, $limit, true, $isHot), true
        );

        // danh sach video cua danh muc
        if($category['content']){
            foreach ($category['content'] as $key => $item) {
                $id = $item['id'];
                $name = $item['name'];
                $videoBox = VideoObj::serialize(
                    Obj::CATEGORY_CHILD_VIDEO . '_' . $id, VtVideoBase::getVideosByCategory($id, $limit,$offset), false, $name
                );
                $category['content'][$key]['content'] = $videoBox;
            }
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',"Thành công"),
            'data' => $category
        ];

    }

    public function actionGetCategoryChild($id = '', $offset = '', $limit = '') {
        if (empty($id)) {
            $id = intval(trim(Yii::$app->request->get('id', 0)));
        }
        if (empty($offset)) {
            $offset = intval(trim(Yii::$app->request->get('offset', 0)));
        }
        if (empty($limit)) {
            $limit = intval(trim(Yii::$app->request->get('limit', 0)));
            $limit = ($limit == 0) ? Yii::$app->params['app.category.limit'] : $limit;
        }
        $dataResponse = array();
        // Lay thong tin chuyen muc
        $category = VtGroupCategoryBase::getCategoryGroup($id);
        if (empty($category)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }
        $detailObj = array();
        $detailObj['id'] = $category['id'];
        $detailObj['name'] = $category['name'];
        $detailObj['description'] = $category['description'];
        $detailObj['type'] = $category['type'];
        $detailObj['avatarImage'] = VtHelper::getThumbUrl($category['avatar_bucket'], $category['avatar_path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($category['bucket'], $category['path'], VtHelper::SIZE_CATEGORY);
        $detailObj['play_times'] = number_format($category['play_times'], 0, ',', '.');
        // Danh chuyen muc con
        $categorys = \common\modules\v2\libs\CategoryObj::serialize(
                        Obj::CATEGORY_PARENT, VtGroupCategoryBase::getChilds($id, $offset, $limit), true
        );
        $loop = Yii::$app->params['category.load.more.limit'];
        if (count($categorys['content']) > 0) {
            foreach ($categorys['content'] as $category) {
                $id = $category['id'];
                $name = $category['name'];
                $tmpBox = \common\modules\v2\libs\VideoObj::serialize(
                                Obj::CATEGORY_CHILD . '_' . $id, VtVideoBase::getVideoByCategory($id, null, 0, $loop), false, $name
                );
                $dataResponse[] = $tmpBox;
            }
        } else {
            $id = $detailObj['id'];
            $name = $detailObj['name'];
            $tmpBox = \common\modules\v2\libs\VideoObj::serialize(
                Obj::CATEGORY_CHILD . '_' . $id, VtVideoBase::getVideoByCategory($id, null, 0, $loop), false, $name
            );
            $dataResponse[] = $tmpBox;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',"Thành công"),
            'data' => [
                Obj::CATEGORY_DETAIL => $detailObj,
                'content' => $dataResponse
            ]
        ];
    }

    public function actionGetDetailCategory($id = '', $offset = 0, $limit = 0, $order = 'NEW') {
        if ($limit == 0) {
            $limit = Yii::$app->params['app.category.limit'];
        }

        // Lay thong tin chuyen muc
        $category = VtGroupCategoryBase::getCategoryGroup($id);
        if (empty($category)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }
        // Lấy lượt view cả category
        $play_times = VtVideo::getPlayTimesCategory($category['id']);
        $detailObj = array();
        $detailObj['id'] = $category['id'];
        $detailObj['name'] = $category['name'];
        $detailObj['description'] = $category['description'];
        $detailObj['type'] = $category['type'];
        $detailObj['avatarImage'] = VtHelper::getThumbUrl($category['avatar_bucket'], $category['avatar_path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($category['bucket'], $category['path'], VtHelper::SIZE_CATEGORY);
        $detailObj['play_times'] = number_format($play_times, 0, ',', '.');

        // Lay danh sach video pho bien theo chuyen muc
        $mostTrendingVideos = VideoObj::serialize(
                        Obj::VIDEO_MOST_TRENDING_CATE . $id, VtVideoBase::getVideosByCate($id, $limit, $offset, $order), false, Obj::getName(Obj::VIDEO_MOST_TRENDING_CATE)
        );
     
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
                'most_trending_video' => $mostTrendingVideos,
            ]
        ];
    }

}
