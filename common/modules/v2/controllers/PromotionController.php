<?php

namespace common\modules\v2\controllers;


use backend\models\VtConfig;
use common\models\VtPromotionWinnerBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\VideoObj;
use common\modules\v2\libs\ResponseCode;

class PromotionController extends \common\controllers\ApiController {

    public function actionGetPromotion() {

        $startAt = VtConfig::getConfig("promotion.start.at", "2017-11-15 00:00:00");

        //Danh sach video co luot xem nhieu nhat
        $mostView1512 = VideoObj::serialize(
            Obj::VIDEO_CUSTOMER_MOSTVIEW_1512, VtVideoBase::getMostViewByCustomer('',50, 0, 'iphone_x_201712160009'), true
        );

        //Danh sach video moi upload
        $newest1512 = VideoObj::serialize(
            Obj::VIDEO_CUSTOMER_NEWEST_1512, VtVideoBase::getNewByCustomer('',50, 0, 'iphone_x_201712160009'), true
        );

        //Danh sach video co luot xem nhieu nhat
        $mostView = VideoObj::serialize(
            Obj::VIDEO_CUSTOMER_MOSTVIEW, VtVideoBase::getMostViewByCustomer($startAt,50, 0, 'iphone_x_201801152359' ), true
        );

        //Danh sach video moi upload
        $newest = VideoObj::serialize(
            Obj::VIDEO_CUSTOMER_NEWEST, VtVideoBase::getNewByCustomer($startAt,50, 0, 'iphone_x_201801152359'), true
        );

        $winner= VideoObj::serialize(
            Obj::PROMOTION_WINNER, VtPromotionWinnerBase::getWinners(), true
        );

        $rangeStr = VtConfig::getConfig("promotion.iphonex.range", \Yii::t('wap', "Từ ngày 15.11.2017 đến 15.01.2018"));
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => \Yii::t('wap', "Thành công"),

            'data'=>[
                "mostview" => $mostView,
                "newest"=>$newest,
                "mostview1512" => $mostView1512,
                "newest1512"=>$newest1512,
                "winner"=>$winner,
                "rangeStr"=>$rangeStr
            ]
        ];
    }

}
