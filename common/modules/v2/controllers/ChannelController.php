<?php

namespace common\modules\v2\controllers;

use api\models\VtChannelRelated;
use common\controllers\ApiController;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtChannelBase;
use common\models\VtChannelFollowBase;
use common\models\VtChannelInfoBase;
use common\models\VtChannelRelatedBase;
use common\models\VtUserBase;
use common\models\VtUserChangeInfoBase;
use common\models\VtUserFollowBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\ChannelObj;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\ResponseCode;
use common\modules\v2\libs\VideoObj;
use common\models\UpdatePhoneNumberForm;
use common\models\UserChangePhoneRequestBase;
use Yii;
use yii\helpers\ArrayHelper;

class ChannelController extends ApiController
{
    public function actionGetUserDetail()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $id = trim(Yii::$app->request->get('id', 0));
        $type = trim(Yii::$app->request->get('type', ''));
        // nếu active =1 chỉ lấy danh sách kênh có video >0, còn lại lấy danh sách kênh đã active
        $active = trim(Yii::$app->request->get('active', 1));

        if (empty($id)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Bạn vui lòng nhập id user.'),
            ];
        }

        // thong tin chi tiet user
        $user_detail = VtUserBase::getActiveUserById($id);

        if (empty($user_detail)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','User id không tồn tại.'),
            ];
        }

        if($this->userId !=$id){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không có quyền truy cập.'),
            ];
        }
        //@todo: bo sung
        $detailObj = array();
        $detailObj['id'] = $user_detail['id'];
        $detailObj['name'] = Utils::truncateWords(($user_detail['full_name']) ? $user_detail['full_name'] : Utils::hideMsisdnLast($user_detail['msisdn']), 80);
        $detailObj['description'] = $user_detail['description'];
        $detailObj['num_follow'] = $user_detail['follow_count'];
        $detailObj['num_video'] = $user_detail['video_count'];
        $detailObj['totalViews'] = $user_detail['view_count'];
        $detailObj['msisdn'] = (string)Utils::hideMsisdnLast($user_detail['msisdn']);

        $detailObj['avatarImage'] = VtHelper::getThumbUrl($user_detail['bucket'], $user_detail['path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($user_detail['channel_bucket'], $user_detail['channel_path'], VtHelper::SIZE_CHANNEL3);
        $detailObj['coverImageWeb'] = VtHelper::getThumbUrl($user_detail['channel_bucket'], $user_detail['channel_path'], VtHelper::SIZE_CHANNEL2);

        $detailObj['created_at'] = $user_detail['created_at'];
        $detailObj['type'] = 'USER';
        $detailObj['isFollow'] = false;
        $detailObj['notification_type'] = VtChannelFollowBase::NOTIFICATION_TYPE_NEVER;

        if ($this->userId) {
            $userFollow = VtChannelFollowBase::getFollow($this->userId, $id);

            if ($userFollow) {
                $detailObj['isFollow'] = true;
                $detailObj['notification_type'] = $userFollow->notification_type;
            };
        }

        // list channel
        $channelLists = VtChannelBase::getChannelListById($id,$active);

        if($type!='app'){
            if($channelLists){
                foreach ($channelLists as $key => $item) {
                    //if($item['video_count'] > 0){
                    $channelLists[$key]['content'] = VideoObj::serialize(
                        Obj::VIDEO_NEW_OF_CHANNEL . $item['id'], VtVideoBase::getVideosByChannel($item['id'], Yii::$app->params['app.video.channel.limit'], 0, 'id'), true, Obj::getName(Obj::VIDEO_NEW_OF_CHANNEL)
                    );

                    // }
                }
            }
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
                'channel' => $channelLists,
            ]
        ];
    }

    public function actionGetDetail()
    {
        $id = trim(Yii::$app->request->get('id', 0));
        $type = trim(Yii::$app->request->get('type', ''));
        $channel = VtChannelBase::getChannelById($id);
        if (empty($channel)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Kênh không tồn tại.'),
            ];
        }
        $chanelInfo = VtChannelInfoBase::getChannelByChannelID($id);
        $detailObj = array();
        $detailObj['id'] = $channel['id'];
        $detailObj['name'] = Utils::truncateWords(($channel['full_name']) ? $channel['full_name'] : Utils::hideMsisdnLast($channel['user_id']), 80);
        $detailObj['user_id'] = $channel['user_id'];
        $detailObj['description'] = $channel['description'];
        $detailObj['num_follow'] = $channel['follow_count'];
        $detailObj['num_video'] = $channel['video_count'];
        $detailObj['totalViews'] = $channel['view_count'];

        $detailObj['avatarImage'] = VtHelper::getThumbUrl($channel['bucket'], $channel['path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($channel['channel_bucket'], $channel['channel_path'], VtHelper::SIZE_CHANNEL3);
        $detailObj['coverImageWeb'] = VtHelper::getThumbUrl($channel['channel_bucket'], $channel['channel_path'], VtHelper::SIZE_CHANNEL2);

        $detailObj['created_at'] = $channel['created_at'];
        $detailObj['type'] = 'CHANNEL';
        $detailObj['isFollow'] = false;
        $detailObj['notification_type'] = VtChannelFollowBase::NOTIFICATION_TYPE_NEVER;
        if (!empty($chanelInfo)) {
            $detailObj['name'] = Utils::truncateWords(($chanelInfo['full_name']) ? $chanelInfo['full_name'] : Utils::hideMsisdnLast($chanelInfo['user_id']), 80);
            $detailObj['description'] = $chanelInfo['description'];
            $detailObj['avatarImage'] = VtHelper::getThumbUrl($chanelInfo['bucket'], $chanelInfo['path'], VtHelper::SIZE_AVATAR);
            $detailObj['coverImage'] = VtHelper::getThumbUrl($chanelInfo['channel_bucket'], $chanelInfo['channel_path'], VtHelper::SIZE_CHANNEL3);
            $detailObj['coverImageWeb'] = VtHelper::getThumbUrl($chanelInfo['channel_bucket'], $chanelInfo['channel_path'], VtHelper::SIZE_CHANNEL2);
        }

        if ($this->userId) {
            $userFollow = VtChannelFollowBase::getFollow($this->userId, $id);
            if ($userFollow) {
                $detailObj['isFollow'] = true;
                $detailObj['notification_type'] = $userFollow->notification_type;
            };
        }

        $mostViewVideos = VideoObj::serialize(
            Obj::VIDEO_MOST_VIEW_OF_CHANNEL . $id, VtVideoBase::getVideosByChannel($id, Yii::$app->params['app.video.channel.limit'], 0, 'MOSTVIEW'), true, Obj::getName(Obj::VIDEO_MOST_VIEW_OF_CHANNEL)
        );

        $newestVideos = VideoObj::serialize(
            Obj::VIDEO_NEWEST_CHANNEL . $id, VtVideoBase::getVideosByChannel($id, Yii::$app->params['app.video.channel.limit'], 0, 'NEW'), true, Obj::getName(Obj::VIDEO_NEWEST_CHANNEL)
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'id' => $id,
                'detail' => $detailObj,
                'most_view_video' => $mostViewVideos,
                'newest_video' => $newestVideos,

            ]
        ];
    }

    public function actionGetDetailEdit()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->get('id', 0));
        $type = trim(Yii::$app->request->get('type', 'user'));
        $detailObj = array();
        if($type=='user'){
            $channel = VtUserChangeInfoBase::getUserById($id);
            $detailObj['reason'] = $channel['reason'];
            if(empty($channel)) {
                $channel = VtUserBase::getActiveUserById($id);
                $detailObj['num_follow'] = $channel['follow_count'];
            }
        }elseif($type=='channel'){
            $channel = VtChannelInfoBase::getChannelByChannelID($id);
            $detailObj['reason'] = $channel['reason'];
            if(empty($channel)){
                $channel = VtChannelBase::getChannelById($id);
                $detailObj['user_id'] = $channel['user_id'];
                $detailObj['num_follow'] = $channel['follow_count'];
            }
        }

        $detailObj['id'] = $channel['id'];
        $detailObj['name'] = Utils::truncateWords($channel['full_name'] , 80);
        $detailObj['description'] = $channel['description'];
        $detailObj['status'] = $channel['status'];
        $detailObj['avatarImage'] = VtHelper::getThumbUrl($channel['bucket'], $channel['path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($channel['channel_bucket'], $channel['channel_path'], VtHelper::SIZE_CHANNEL3);
        $detailObj['coverImageWeb'] = VtHelper::getThumbUrl($channel['channel_bucket'], $channel['channel_path'], VtHelper::SIZE_CHANNEL2);
        $detailObj['created_at'] = $channel['created_at'];

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'id' => $id,
                'detail' => $detailObj
            ]
        ];
    }

    public function actionCancelCurrentChangePhoneRequest() {
        if (!$this->isValidUser()) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không có quyền chỉnh sửa Kênh'),
            ];
        }

        $model = UpdatePhoneNumberForm::findOne(['msisdn' => $this->msisdn, 'status' => \common\models\UserChangePhoneRequestBase::STATUS_DRAFT]);

        if(!$model) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        $model->status = UserChangePhoneRequestBase::STATUS_USER_CANCEL;
        $model->save(false, ['status']);
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
        ];
    }

    public function actionUpdate()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method != "POST") {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ')
            ];
        }
        $id = trim(Yii::$app->request->post('id', 0));

        if(empty($id)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Đối tượng không tồn tại hoặc chưa được phê duyệt')
            ];
        }

        $name = trim(Yii::$app->request->post('name', ''));
        $description = trim(Yii::$app->request->post('description', ''));
        if (!$name || !$description) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ'),
            ];
        }

        if (empty($name) || strlen(utf8_decode($name)) < Yii::$app->params['channel.name.minlength'] || strlen(utf8_decode($name)) > Yii::$app->params['channel.name.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập tên từ ') . Yii::$app->params['channel.name.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['channel.name.maxlength'] . Yii::t('wap',' ký tự.')
            ];
        }

        if (empty($description) || (strlen(utf8_decode($description)) < Yii::$app->params['channel.description.minlength'] || strlen(utf8_decode($description)) > Yii::$app->params['channel.description.maxlength'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập mô tả từ ') . Yii::$app->params['channel.description.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['channel.description.maxlength'] . Yii::t('wap', ' ký tự.')
            ];
        }

        //set_time_limit(600);
        //ini_set('memory_limit', '50M');

        $fileAvatarPath = '';
        $fileBannerPath = '';


        if (!empty($_FILES['file_avatar']) && $_FILES['file_avatar']['size'] > 0) {
            $extAvatar = pathinfo($_FILES['file_avatar']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($extAvatar), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng ảnh avatar upload không đúng')
                ];
            }

            if ($_FILES['file_avatar']['size'] > 5242880) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh avatar tối đa 5MB')
                ];
            }
//            $fileAvatarPath = ($storageType == 1) ? Utils::generatePath($extAvatar) : Yii::$app->params['local.image']. DIRECTORY_SEPARATOR .Utils::generatePath($extAvatar);
        }


        if (!empty($_FILES['file_banner']) && $_FILES['file_banner']['size'] > 0) {
            $extBanner = pathinfo($_FILES['file_banner']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($extBanner), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng ảnh banner upload không đúng')
                ];
            }

            if ($_FILES['file_banner']['size'] > 5242880) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh banner tối đa 5MB')
                ];
            }
//            $fileBannerPath = ($storageType == 1) ? Utils::generatePath($extBanner) : Yii::$app->params['local.image']. DIRECTORY_SEPARATOR .Utils::generatePath($extBanner);
        }

        if ($this->isValidUser()) {

            $objChannel = VtChannelInfoBase::getChannelById($id);

            if(empty($objChannel)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Đối tượng không tồn tại hoặc chưa được phê duyệt')
                ];
            }
            if(empty($objChannel)) {
                $objChannel = new VtChannelInfoBase();
                $objChannel->created_at = date("Y-m-d H:i:s");
                $objChannel->channel_id = $id;
            }else{
                $objChannel->updated_at = date("Y-m-d H:i:s");
            }

            if (!empty($name)) {
                $objChannel->full_name = $name;
            }

            if (!empty($description)) {
                $objChannel->description = $description;
            }
            $fileBucket = "";
            $storageType = Yii::$app->params['storage.type'];
            if (!empty($_FILES['file_avatar']) && $_FILES['file_avatar']['size'] > 0) {
//                S3Service::putObject($imageBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath,true,'image');
//                VtHelper::generateAllThumb($imageBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);

//                lưu ảnh local
                $ext = pathinfo($_FILES['file_avatar']['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    echo json_encode([
                        "success" => false,
                        "error" => Yii::t('web','Định dạng file upload không đúng'),
                    ]);
                    return false;
                }
                if($storageType == 0){ // Ko co CDN
                    $san = Yii::$app->params['local.image'];
                    $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                    $fileName = Utils::generateUniqueFileName($ext);
                    $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                    if (!mkdir($childFolder, 0777, true)) {
                        return ('Failed to create folders...');
                    }
                    $fileAvatarPath = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    $fileAvatarLocalPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //$newFilePath = $folder.DIRECTORY_SEPARATOR.$fileName;
                    $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR.$folder. DIRECTORY_SEPARATOR.$fileName ;
                    // save oginal image
                    move_uploaded_file($_FILES['file_avatar']['tmp_name'], $orgpath);
                    // tao thumb
                    VtHelper::generateAllThumb($fileBucket, $fileAvatarLocalPath, $orgpath,[],true);
                }else{
                    $fileBucket = Yii::$app->params['s3']['static.bucket'];
                    $fileAvatarPath = Utils::generatePath($ext);
                    $fileName = pathinfo($_FILES['file_avatar']['name'], PATHINFO_FILENAME);
                    S3Service::putObject($fileBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);
                    VtHelper::generateAllThumb($fileBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);
                }
                $objChannel['bucket'] = $fileBucket;
                $objChannel['path'] = $fileAvatarPath;
            }
            list($width, $height) = getimagesize($_FILES['file_avatar']['tmp_name']);
            if($width > "180" || $height > "180") {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh avatar không hợp lệ')
                ];
            }
            if (!empty($_FILES['file_banner']) && $_FILES['file_banner']['size'] > 0) {
//                S3Service::putObject($imageBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath,true,'image');
//                VtHelper::generateAllThumb($imageBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                //                lưu ảnh local
                $ext = pathinfo($_FILES['file_banner']['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    echo json_encode([
                        "success" => false,
                        "error" => Yii::t('web','Định dạng file upload không đúng'),
                    ]);
                    return false;
                }
                if($storageType == 0){ // Ko co CDN
                    $san = Yii::$app->params['local.image'];
                    $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                    $fileName = Utils::generateUniqueFileName($ext);
                    $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                    if (!mkdir($childFolder, 0777, true)) {
                        return ('Failed to create folders...');
                    }
                    $fileBannerPath = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    $fileBannerLocalPath = Yii::getAlias('@uploadFolder'). DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //$newFilePath = $folder.DIRECTORY_SEPARATOR.$fileName;
                    $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR.$folder. DIRECTORY_SEPARATOR.$fileName ;
                    // save oginal image
                    move_uploaded_file($_FILES['file_banner']['tmp_name'], $orgpath);
                    // tao thumb
                    VtHelper::generateAllThumb($fileBucket, $fileBannerLocalPath, $orgpath,[],true);
                }else{
                    $fileBucket = Yii::$app->params['s3']['static.bucket'];
                    $fileBannerPath = Utils::generatePath($ext);
                    $fileName = pathinfo($_FILES['file_banner']['name'], PATHINFO_FILENAME);
                    S3Service::putObject($fileBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                    VtHelper::generateAllThumb($fileBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                }
                $objChannel['channel_bucket'] = $fileBucket;
                $objChannel['channel_path'] = $fileBannerPath;
            }
            list($width, $height) = getimagesize($_FILES['file_banner']['tmp_name']);
            if($width > "1920" || $height > "330") {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh banner không hợp lệ')
                ];
            }

            $objChannel->status =  VtChannelInfoBase::WAIT_APPROVE;
            $objChannel->save(false);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'channel' => [
                        'id' => $objChannel->id,
                        'name' => $objChannel->full_name,
                        'channel_id' => $objChannel->channel_id,
                        'status' => $objChannel->status,
                        'description' => $objChannel->description,
                        'avatarImage' => VtHelper::getThumbUrl($objChannel->bucket, $objChannel['path'], VtHelper::SIZE_AVATAR),
                        'coverImage' => VtHelper::getThumbUrl($objChannel->channel_bucket, $objChannel->channel_path, VtHelper::SIZE_CHANNEL3),
                        'coverImageWeb' => VtHelper::getThumbUrl($objChannel->channel_bucket, $objChannel->channel_path, VtHelper::SIZE_CHANNEL2)
                    ]
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không có quyền chỉnh sửa Kênh')

            ];
        }
    }

    public function actionGetInfoChannel()
    {
        $id = trim(Yii::$app->request->get('id', 0));
        $channel = VtChannelInfoBase::getChannelByChannelID($id);
        if (empty($channel)) {
            $channel = VtChannelBase::CheckChannelUser($id,$this->userId);
        }

        if (empty($channel)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Kênh không tồn tại.'),
            ];
        }

        $detailObj = array();
        $detailObj['id'] = $channel['id'];
        $detailObj['name'] = $channel['full_name'];
        $detailObj['description'] = $channel['description'];
        $detailObj['status'] = $channel['status'];
        $detailObj['reason'] = $channel['reason'];

        $detailObj['avatarImage'] = VtHelper::getThumbUrl($channel['bucket'], $channel['path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($channel['channel_bucket'], $channel['channel_path'], VtHelper::SIZE_CHANNEL3);
        $detailObj['coverImageWeb'] = VtHelper::getThumbUrl($channel['channel_bucket'], $channel['channel_path'], VtHelper::SIZE_CHANNEL2);

        $detailObj['type'] = 'CHANNEL';

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
            ]
        ];
    }

    public function actionGetChannelRecommend()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        //Lay danh sach kenh ma user da follow
        $followIds = VtChannelFollowBase::getFollowUserIds($this->userId);

        $limit = Yii::$app->params['app.video.channel.limit'];
        //Lay danh sach kenh hot, loai tru nhung kenh da follow
        $chanelBox = ChannelObj::serialize(Obj::CHANNEL_HOT, VtChannelBase::getHotUserQuery($limit, 0, $followIds, $this->userId), false, false, false, $limit);


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $chanelBox
        ];
    }


    public function actionAddChannelRelated()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $relatedId = Yii::$app->request->post("channel_related_id");
        if(!$relatedId){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Bắt buộc chọn kênh liên quan')
            ];
        }
        $channel = VtChannelBase::getChannelById($relatedId);

        $totalChannelRelated = VtChannelRelatedBase::findAll(["channel_id"=>$this->userId]);
        if(count($totalChannelRelated) >= Yii::$app->params['max.channel.related']  ){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Số kênh đã đạt tối đa: ').Yii::$app->params['max.channel.related']
            ];
        }

        if ($channel['user_id'] != $this->userId) {
            if (VtChannelRelated::addChannel($this->userId, $relatedId)) {
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap','Thành công')
                ];
            } else {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kênh đã thêm')
                ];
            }
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không thể thêm kênh của chính bạn')
            ];
        }
    }

    public function actionRemoveChannelRelated()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $relatedId = Yii::$app->request->post("channel_related_id");
        $channel = VtChannelBase::getChannelById($relatedId);

        if(empty($channel)){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Kênh không tồn tại')
            ];
        }
        if (VtChannelRelated::removeChannel($this->userId, $relatedId)) {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công')
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::UNSUCCESS)
            ];
        }

    }

    public function actionGetChannelRelated($user_id=0)
    {
        $userId = Yii::$app->request->get("channel_id", $user_id);
        $objUser = VtUserBase::getActiveUserById($userId);

        if(!$objUser){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Tài khoản không tồn tại')
            ];
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap','Thành công'),
            'data' => ChannelObj::serialize(Obj::CHANNEL_RELATED, VtChannelRelatedBase::getChannelRelatedQuery($userId, $this->userId), true, "", false)
        ];

    }

    public function actionGetChannelByCategory($userlId=0)
    {
        $userlId = Yii::$app->request->get("user_id",$userlId);

        $objUser = VtUserBase::getActiveUserById($userlId);

        if(!$objUser){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Tài khoản không tồn tại')
            ];
        }
        $alreadyAddRelated = VtChannelRelatedBase::findAll(["channel_id"=>$userlId]);
        $alreadyAddRelated_add = (ArrayHelper::getColumn($alreadyAddRelated,"channel_related_id"));


        $alreadyAddRelated_add[] = $userlId;
        // var_dump($alreadyAddRelated_add);die;
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap','Thành công'),
            'data' => ChannelObj::serialize(Obj::CHANNEL_RELATED, VtChannelBase::getHotChannelQuery(Yii::$app->params['max.channel.related'],0, $alreadyAddRelated_add,$this->userId), true, "", false)
        ];

    }

    public function actionInsert()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $method = $_SERVER["REQUEST_METHOD"];
        if ($method != "POST") {

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ')
            ];
        }

        $AllChannel = VtChannelBase::getCountChannelById($this->userId);

        if($AllChannel >= 10){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Bạn đã vượt qua số lượng 10 kênh trên hệ thống')
            ];
        }

        $name = trim(Yii::$app->request->post('name', ''));
        $description = trim(Yii::$app->request->post('description', ''));

        if (!$name || !$description) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ'),
            ];
        }

        if (empty($name) || strlen(utf8_decode($name)) < Yii::$app->params['channel.name.minlength'] || strlen(utf8_decode($name)) > Yii::$app->params['channel.name.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập tên từ ') . Yii::$app->params['channel.name.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['channel.name.maxlength'] . Yii::t('wap',' ký tự.')
            ];
        }

        if (empty($description) || (strlen(utf8_decode($description)) < Yii::$app->params['channel.description.minlength'] || strlen(utf8_decode($description)) > Yii::$app->params['channel.description.maxlength'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập mô tả từ ') . Yii::$app->params['channel.description.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['channel.description.maxlength'] . Yii::t('wap', ' ký tự.')
            ];
        }



        $imageBucket = Yii::$app->params['s3']['static.bucket'];
        $storageType = Yii::$app->params['storage.type'];
        $fileAvatarPath = '';
        $fileBannerPath = '';


        if (!empty($_FILES['file_avatar']) && $_FILES['file_avatar']['size'] > 0) {
            $extAvatar = pathinfo($_FILES['file_avatar']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($extAvatar), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng ảnh avatar upload không đúng')
                ];
            }

            if ($_FILES['file_avatar']['size'] > 5242880) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh avatar tối đa 5MB')
                ];
            }
            $fileAvatarPath = ($storageType == 1) ? Utils::generatePath($extAvatar) : Yii::$app->params['local.image']. DIRECTORY_SEPARATOR .Utils::generatePath($extAvatar);
        }


        if (!empty($_FILES['file_banner']) && $_FILES['file_banner']['size'] > 0) {
            $extBanner = pathinfo($_FILES['file_banner']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($extBanner), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng ảnh banner upload không đúng')
                ];
            }

            if ($_FILES['file_banner']['size'] > 5242880) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh banner tối đa 5MB')
                ];
            }
            $fileBannerPath = ($storageType == 1) ? Utils::generatePath($extBanner) : Yii::$app->params['local.image']. DIRECTORY_SEPARATOR .Utils::generatePath($extBanner);
        }

        if ($this->isValidUser()) {

            $objChannel = [];
            $objChannel['full_name'] = $name;
            $objChannel['description'] = $description;
            $storageType = Yii::$app->params['storage.type'];
            $fileBucket = "";
            if (!empty($_FILES['file_avatar']) && $_FILES['file_avatar']['size'] > 0) {
//                S3Service::putObject($imageBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath,true,'image');
//                VtHelper::generateAllThumb($imageBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);

//                lưu ảnh local
                $ext = pathinfo($_FILES['file_avatar']['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    echo json_encode([
                        "success" => false,
                        "error" => Yii::t('web','Định dạng file upload không đúng'),
                    ]);
                    return false;
                }
                if($storageType == 0){ // Ko co CDN
                    //lấy thư mục ảnh
                    $san = Yii::$app->params['local.image'];
                    //sinh ra tên thư mục con
                    $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                    //sinh ra tên file
                    $fileName = Utils::generateUniqueFileName($ext);
                    //tạo thư mục lưu ảnh từ đường dẫn server
                    $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                    if (!mkdir($childFolder, 0777, true)) {
                        return ('Failed to create folders...');
                    }
                    //đường dẫn lưu vào cơ sở dữ liệu
                    $fileAvatarPath = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //đường dẫn cắt ảnh
                    $fileAvatarLocalPath = Yii::getAlias('@uploadFolder'). DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //đường dẫn lưu ảnh đã cắt ( lưu đường dẫn cắt ảnh thì lấy cùng tên)
                    $orgpath = $fileAvatarLocalPath ;
                    // hàm lưu ảnh vào server
                    move_uploaded_file($_FILES['file_avatar']['tmp_name'], $orgpath);
                    // tạo ảnh cắt thumb
                    VtHelper::generateAllThumb($fileBucket, $fileAvatarLocalPath, $orgpath,[],true);
                }else{
                    $fileBucket = Yii::$app->params['s3']['static.bucket'];
                    $fileAvatarPath = Utils::generatePath($ext);
                    $fileName = pathinfo($_FILES['file_avatar']['name'], PATHINFO_FILENAME);
                    S3Service::putObject($fileBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);
                    VtHelper::generateAllThumb($fileBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);
                }
                $objChannel['bucket'] = $fileBucket;
                $objChannel['path'] = $fileAvatarPath;
            }
            list($width, $height) = getimagesize($_FILES['file_avatar']['tmp_name']);
            if($width > "180" || $height > "180") {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh avatar không hợp lệ')
                ];
            }
            if (!empty($_FILES['file_banner']) && $_FILES['file_banner']['size'] > 0) {
//                S3Service::putObject($imageBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath,true,'image');
//                VtHelper::generateAllThumb($imageBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                //                lưu ảnh local
                $ext = pathinfo($_FILES['file_banner']['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    echo json_encode([
                        "success" => false,
                        "error" => Yii::t('web','Định dạng file upload không đúng'),
                    ]);
                    return false;
                }
                if($storageType == 0){ // Ko co CDN
                    $san = Yii::$app->params['local.image'];
                    $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                    $fileName = Utils::generateUniqueFileName($ext);
                    $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                    if (!mkdir($childFolder, 0777, true)) {
                        return ('Failed to create folders...');
                    }
                    $fileBannerPath = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    $fileBannerLocalPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //$newFilePath = $folder.DIRECTORY_SEPARATOR.$fileName;
                    $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR.$folder. DIRECTORY_SEPARATOR.$fileName ;
                    // save oginal image
                    move_uploaded_file($_FILES['file_banner']['tmp_name'], $orgpath);
                    // tao thumb
                    VtHelper::generateAllThumb($fileBucket, $fileBannerLocalPath, $orgpath,[],true);
                    $fileBucket = "";
                }else{
                    $fileBucket = Yii::$app->params['s3']['static.bucket'];
                    $fileBannerPath = Utils::generatePath($ext);
                    $fileName = pathinfo($_FILES['file_banner']['name'], PATHINFO_FILENAME);
                    S3Service::putObject($fileBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                    VtHelper::generateAllThumb($fileBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                }
                $objChannel['channel_bucket'] = $fileBucket;
                $objChannel['channel_path'] = $fileBannerPath;
            }
            $imageBucket = $fileBucket;
            list($width, $height) = getimagesize($_FILES['file_banner']['tmp_name']);
            if($width > "1920" || $height > "330") {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh banner không hợp lệ')
                ];
            }

            $objChannel['status'] = VtChannelBase::WAIT_APPROVE;
            $objChannel['user_id'] = $this->userId;
            $objChannel['updated_at'] = date("Y-m-d H:i:s");

            $Channel = new VtChannelBase();
            $Channel->insertchannel($name,$description,$fileBannerPath,$fileAvatarPath,$this->userId,$imageBucket,VtChannelBase::WAIT_APPROVE);
            if($Channel['id']){
                $ChannelInfo = new VtChannelInfoBase();
                $ChannelInfo->insertChannelInfo($name,$description,$fileBannerPath,$fileAvatarPath,$Channel['id'],$imageBucket,VtChannelInfoBase::WAIT_APPROVE);
            }

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'channel' => [
                        'id' => $Channel->id,
                        'name' => $Channel->full_name,
                        'status' => $Channel->status,
                        'description' => $Channel->description,
                        'avatarImage' => VtHelper::getThumbUrl($Channel->bucket, $Channel['path'], VtHelper::SIZE_AVATAR),
                        'coverImage' => VtHelper::getThumbUrl($Channel->channel_bucket, $Channel->channel_path, VtHelper::SIZE_CHANNEL3),
                        'coverImageWeb' => VtHelper::getThumbUrl($Channel->channel_bucket, $Channel->channel_path, VtHelper::SIZE_CHANNEL2)
                    ]
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không có quyền tạo Kênh')

            ];
        }
    }

    // update info user
    public function actionUserUpdate()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        if(!Yii::$app->request->isPost) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ')
            ];
        }

        $id = trim(Yii::$app->request->post('id', 0));

        if(empty($id)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Đối tượng không tồn tại')
            ];
        }
//        $channels = VtChannelBase::getChannelAllById($this->userId);
        if($id != $this->userId){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không có quyền chỉnh sửa user khác'),
            ];
        }

        $name = trim(Yii::$app->request->post('name', ''));
        $description = trim(Yii::$app->request->post('description', ''));

        if (!$name) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu đầu vào không hợp lệ'),
            ];
        }

        if (empty($name) || strlen(utf8_decode($name)) < Yii::$app->params['channel.name.minlength'] || strlen(utf8_decode($name)) > Yii::$app->params['channel.name.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập tên từ ') . Yii::$app->params['channel.name.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['channel.name.maxlength'] . Yii::t('wap',' ký tự.')
            ];
        }

        if (empty($description) || (strlen(utf8_decode($description)) < Yii::$app->params['channel.description.minlength'] || strlen(utf8_decode($description)) > Yii::$app->params['channel.description.maxlength'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quý khách vui lòng nhập mô tả từ ') . Yii::$app->params['channel.description.minlength'] . Yii::t('wap',' đến ') . Yii::$app->params['channel.description.maxlength'] . Yii::t('wap', ' ký tự.')
            ];
        }

        $extAvatar = pathinfo($_FILES['file_avatar']['name'], PATHINFO_EXTENSION);
        $extBanner = pathinfo($_FILES['file_banner']['name'], PATHINFO_EXTENSION);

        $imageBucket = Yii::$app->params['s3']['static.bucket'];
        $storageType = Yii::$app->params['storage.type'];
        $fileAvatarPath ='';
        $fileBannerPath ='';

        if (!empty($_FILES['file_avatar']) && $_FILES['file_avatar']['size'] > 0) {
            if (!in_array(strtolower($extAvatar), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng ảnh avatar upload không đúng')
                ];
            }

            if ($_FILES['file_avatar']['size'] > 5242880) { //800 MB (size is also in bytes 5242880)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh avatar tối đa 5MB')
                ];
            }
            $fileAvatarPath = ($storageType == 1) ? Utils::generatePath($extAvatar) : Yii::$app->params['local.image']. DIRECTORY_SEPARATOR .Utils::generatePath($extAvatar);
        }

        if (!empty($_FILES['file_banner']) && $_FILES['file_banner']['size'] > 0) {
            if (!in_array(strtolower($extBanner), ['jpg', 'jpeg', 'png', 'gif', '.jpg', '.jpeg', '.png', '.gif'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng ảnh banner upload không đúng')
                ];
            }

            if ($_FILES['file_banner']['size'] > 5242880) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh banner tối đa 5MB')
                ];
            }
            $fileBannerPath = ($storageType == 1) ? Utils::generatePath($extBanner) : Yii::$app->params['local.image']. DIRECTORY_SEPARATOR .Utils::generatePath($extBanner);
        }

        if ($this->isValidUser()) {
            $objUser = VtUserChangeInfoBase::getUserById($id);

            if (empty($objUser)) {
//                return [
//                    'responseCode' => ResponseCode::NOT_FOUND,
//                    'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
//                ];
                $objUser = new VtUserChangeInfoBase();
            }

            if (!empty($name)) {
                $objUser->full_name = $name;
            }

            if (!empty($description)) {
                $objUser->description = $description;
            }

            $fileBucket = "";
            if (!empty($_FILES['file_avatar']) && $_FILES['file_avatar']['size'] > 0) {

                $ext = pathinfo($_FILES['file_avatar']['name'], PATHINFO_EXTENSION);

                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    echo json_encode([
                        "success" => false,
                        "error" => Yii::t('web','Định dạng file upload không đúng'),
                    ]);
                    return false;
                }
                if ($storageType == 0){ // Ko co CDN
                    //lấy thư mục ảnh
                    $san = Yii::$app->params['local.image'];
                    //sinh ra tên thư mục con
                    $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                    //sinh ra tên file
                    $fileName = Utils::generateUniqueFileName($ext);
                    //tạo thư mục lưu ảnh từ đường dẫn server
                    $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                    if (!mkdir($childFolder, 0777, true)) {
                        return ('Failed to create folders...');
                    }
                    //đường dẫn lưu vào cơ sở dữ liệu
                    $fileAvatarPath = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //đường dẫn cắt ảnh
                    $fileAvatarLocalPath = Yii::getAlias('@uploadFolder'). DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //đường dẫn lưu ảnh đã cắt ( lưu đường dẫn cắt ảnh thì lấy cùng tên)
                    $orgpath = $fileAvatarLocalPath ;
                    // hàm lưu ảnh vào server
                    move_uploaded_file($_FILES['file_avatar']['tmp_name'], $orgpath);
                    // tạo ảnh cắt thumb
                    VtHelper::generateAllThumb($fileBucket, $fileAvatarLocalPath, $orgpath,[],true);
                }else{
                    $fileBucket = Yii::$app->params['s3']['static.bucket'];
                    $fileAvatarPath = Utils::generatePath($ext);
                    $fileName = pathinfo($_FILES['file_avatar']['name'], PATHINFO_FILENAME);
                    S3Service::putObject($fileBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);
                    VtHelper::generateAllThumb($fileBucket, $_FILES['file_avatar']['tmp_name'], $fileAvatarPath);
                }
                $objUser['bucket'] = $fileBucket;
                $objUser['path'] = $fileAvatarPath;
            }
            list($width, $height) = getimagesize($_FILES['file_avatar']['tmp_name']);
            if($width > "180" || $height > "180") {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh avatar không hợp lệ')
                ];
            }
            if (!empty($_FILES['file_banner']) && $_FILES['file_banner']['size'] > 0) {
//                S3Service::putObject($imageBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath,true,'image');
//                VtHelper::generateAllThumb($imageBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                //                lưu ảnh local
                $ext = pathinfo($_FILES['file_banner']['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    echo json_encode([
                        "success" => false,
                        "error" => Yii::t('web','Định dạng file upload không đúng'),
                    ]);
                    return false;
                }
                if($storageType == 0){ // Ko co CDN
                    $san = Yii::$app->params['local.image'];
                    $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                    $fileName = Utils::generateUniqueFileName($ext);
                    $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;
                    if (!mkdir($childFolder, 0777, true)) {
                        return ('Failed to create folders...');
                    }
                    $fileBannerPath = ''. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    $fileBannerLocalPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                    //$newFilePath = $folder.DIRECTORY_SEPARATOR.$fileName;
                    $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR.$folder. DIRECTORY_SEPARATOR.$fileName ;
                    // save oginal image
                    move_uploaded_file($_FILES['file_banner']['tmp_name'], $orgpath);
                    // tao thumb
                    VtHelper::generateAllThumb($fileBucket, $fileBannerLocalPath, $orgpath,[],true);
                    $fileBucket = "";
                }else{
                    $fileBucket = Yii::$app->params['s3']['static.bucket'];
                    $fileBannerPath = Utils::generatePath($ext);
                    $fileName = pathinfo($_FILES['file_banner']['name'], PATHINFO_FILENAME);
                    S3Service::putObject($fileBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                    VtHelper::generateAllThumb($fileBucket, $_FILES['file_banner']['tmp_name'], $fileBannerPath);
                }
                $objUser['channel_bucket'] = $fileBucket;
                $objUser['channel_path'] = $fileBannerPath;
            }
            $imageBucket = $fileBucket;
            list($width, $height) = getimagesize($_FILES['file_banner']['tmp_name']);
            if($width > "1920" || $height > "330") {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước ảnh banner không hợp lệ')
                ];
            }
            $objUser->updated_at = date("Y-m-d H:i:s");
            $objUser->status = 0;
            $objUser->save(false);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'user' => [
                        'id' => $objUser->id,
                        'name' => $objUser->full_name,
                        'status' => $objUser->status,
                        'description' => $objUser->description,
                        'avatarImage' => VtHelper::getThumbUrl($objUser->bucket, $objUser['path'], VtHelper::SIZE_AVATAR),
                        'coverImage' => VtHelper::getThumbUrl($objUser->channel_bucket, $objUser->channel_path, VtHelper::SIZE_CHANNEL3),
                        'coverImageWeb' => VtHelper::getThumbUrl($objUser->channel_bucket, $objUser->channel_path, VtHelper::SIZE_CHANNEL2)
                    ]
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không có quyền chỉnh sửa user khác')

            ];
        }
    }

    public function actionGetUserUpdate()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $id = trim(Yii::$app->request->get('id', 0));
        $id = (int)$id;
        if (empty($id)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Bạn vui lòng nhập id user.'),
            ];
        }

        $detailObj = array();
        // thong tin chi tiet user
        $user_detail = VtUserChangeInfoBase::getUserById($id);
        if(empty($user_detail)){
            $user_detail = VtUserBase::getById($id);
        }

        $detailObj['id'] = $id;
        if($id != $this->userId) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Bạn không có quyền sửa thông tin tài khoản khác.'),
            ];
        }

        //@todo: bo sung

        $detailObj['name'] = Utils::truncateWords(($user_detail['full_name']) ? $user_detail['full_name'] : '', 80);
        $detailObj['description'] = $user_detail['description'];
        $detailObj['avatarImage'] = VtHelper::getThumbUrl($user_detail['bucket'], $user_detail['path'], VtHelper::SIZE_AVATAR);
        $detailObj['coverImage'] = VtHelper::getThumbUrl($user_detail['channel_bucket'], $user_detail['channel_path'], VtHelper::SIZE_CHANNEL3);
        $detailObj['coverImageWeb'] = VtHelper::getThumbUrl($user_detail['channel_bucket'], $user_detail['channel_path'], VtHelper::SIZE_CHANNEL2);
        $detailObj['created_at'] = $user_detail['created_at'];
        $detailObj['status'] = $user_detail['status'];

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
            ]
        ];
    }
}
