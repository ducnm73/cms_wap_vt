<?php

namespace common\modules\v2\controllers;

use backend\models\VtUser;
use common\controllers\ApiController;
use common\helpers\MobiTVRedisCache;
use common\helpers\Utils;
use common\libs\RecommendationService;
use common\libs\VtFlow;
use common\models\VtChannel;
use common\models\VtChannelBase;
use common\models\VtChannelSearchBase;
use common\models\VtChannelFollowBase;
use common\models\VtChannelRelatedBase;
use common\models\VtCommentBase;
use common\models\VtConfigBase;
use common\models\VtFavouriteVideoBase;
use common\models\VtGroupCategoryBase;
use common\models\VtHistoryViewBase;
use common\models\VtHotKeywordBase;
use common\models\VtPlaylistBase;
use common\models\VtSlideshowBase;
use common\models\VtSubBase;
use common\models\VtUserFollowBase;
use common\models\VtUserPlaylistBase;
use common\models\VtUserSearchBase;
use common\models\VtVideoHotBase;
use common\models\VtVideoHotCategoryBase;
use common\modules\v2\libs\CategoryObj;
use common\modules\v2\libs\ChannelObj;
use common\modules\v2\libs\MenuObj;
use common\models\VtUserPlaylistItemBase;
use common\models\VtPackageBase;
use common\modules\v2\libs\RecommendObj;
use common\modules\v2\libs\TrackingCode;
use common\modules\v2\libs\UserPlaylistObj;
use common\modules\v2\libs\UserPlaylistVideoObj;
use Yii;
use common\libs\VtHelper;
use common\models\VtVideoSearchBase;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\VideoObj;
use common\models\VtUserBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\ResponseCode;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class DefaultController extends
    \common\modules\v1\controllers\DefaultController
{

    public function actionGetHomeV3()
    {
        $dataResponse = array();
        /*$hashtagHomePage = VtHashtagBase::getHashtagHomePage(Yii::$app->params['hastag.limit.box.home'], 0);
        if ($hashtagHomePage) {
            $dataResponse[] = $hashtagHomePage;
        }*/

        $limitN = Yii::$app->params['video.limit.n.box.home'];

        //home_video_v2
        $videoBox = VideoObj::serialize(
            Obj::VIDEO_HOT_2, VtVideoBase::getVideoHomePage($limitN, true), true,  Yii::t('wap', 'Có thể bạn thích') , 'NEWSFEED'
        );

        //Lay danh sach video moi

        if (isset($videoBox['content']) && $videoBox['content']) {
            $dataResponse[] = $videoBox;
        }

        //--------- BOX video moi
        $newVideo = VideoObj::serialize(
            Obj::VIDEO_NEW, VtVideoBase::getNewVideo('', Yii::$app->params['app.home.limit']), true, false
        );
        if (isset($newVideo['content']) && $newVideo['content']) {
            $dataResponse[] = $newVideo;
        }

        //---------  BOX video da xem
        $boxVideoHistory = [];
        if ($this->userId) {

            $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, Yii::$app->params['app.home.limit']);

            $durationPercent = [];
            foreach ($histories as $history) {
                $parentIds[$history['itemId']] = $history['parent_id'];
                if ($history['duration']) {
                    // Neu thoi luong con lon hon 20s thi moi hien thi xem tiep
                    if ($history['duration'] - $history['time'] > 20) {
                        $historyIds[] = $history['itemId'];
                        $durationPercent[$history['itemId']] = 100 * $history['time'] / $history['duration'];
                    }
                } else {
                    $durationPercent[$history['itemId']] = 0;
                    $historyIds[] = $history['itemId'];
                }
            }

            $historyIds = array();
            foreach ($histories as $history) {
                $historyIds[] = $history['itemId'];
            }
            if (!empty($historyIds)) {
                $boxVideoHistory = VideoObj::serialize(
                    Obj::VIDEO_HISTORY, VtVideoBase::getByIdsQuery(null, $historyIds, Yii::$app->params['app.home.limit'])
                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')]), false, null, null, $durationPercent
                );

                if (isset($boxVideoHistory['content']) && $boxVideoHistory['content']) {
                    $dataResponse[] = $boxVideoHistory;
                }
            }
        }
        $limitHotChannel = Yii::$app->params['app.home.hotchannel.limit'];

        if (!$limitHotChannel) {
            $limitHotChannel = 5;
        }

        // Danh sach video theo kenh
        if (!$this->userId) {
            $arrChannel = [];
            $listHotChannel = VtUser::getHotUser($limitHotChannel);
            foreach ($listHotChannel as $channel) {
                $arrChannel[] = $channel;
            }
        } else {
            $channelFollows = VtUserFollowBase::getChannelFollowQuery($this->userId, $limitHotChannel)->all();
            $arrChannel = [];
            $arrChannelId = [];
            foreach ($channelFollows as $channel) {
                $arrChannel[] = $channel;
                $arrChannelId[] = $channel['id'];
            }

            if (count($channelFollows) < $limitHotChannel) {
                $listHotChannel = VtUser::getHotUser($limitHotChannel);

                $arrChannel = Utils::mergeById($arrChannel, $listHotChannel, 'id', $limitHotChannel);
            }
        }

        if (count($arrChannel)) {
            $alreadyAdd = [];
            // Voi moi kenh lay duoc, hien thi danh sach video moi nhat
            for ($i = 0; $i < count($arrChannel); $i++) {
                if (!in_array($arrChannel[$i]['id'], $alreadyAdd)) {
                    $alreadyAdd[] = $arrChannel[$i]['id'];
                    $newestVideos = VideoObj::serialize(
                        Obj::VIDEO_NEWEST_CHANNEL . $arrChannel[$i]['id'], VtVideoBase::getVideosByChannel($arrChannel[$i]['id'], Yii::$app->params['app.home.limit'], 0, 'NEW'), false, $arrChannel[$i]['full_name']
                    );

                    if (isset($newestVideos['content']) && $newestVideos['content']) {
                        $dataResponse[] = $newestVideos;
                    }
                }
            }
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse,
            'popup' => VtFlow::loadPromotionPopup($this->msisdn)
        ];
    }


    public function actionGetHomeV2()
    {
        $userId = trim(Yii::$app->request->get('userId', ''));
        $dataResponse = array();
        /*$hashtagHomePage = VtHashtagBase::getHashtagHomePage(Yii::$app->params['hastag.limit.box.home'], 0);
        if ($hashtagHomePage) {
            $dataResponse[] = $hashtagHomePage;
        }*/

        $limitN = Yii::$app->params['video.limit.n.box.home'];

        //home_video_v2
        $videoBox = VideoObj::serialize(
            Obj::VIDEO_HOT_2, VtVideoBase::getVideoHomePage($limitN, true), true,  Yii::t('wap', 'Có thể bạn thích') , 'NEWSFEED'
        );

        //Lay danh sach video moi

        if (isset($videoBox['content']) && $videoBox['content']) {
            $dataResponse[] = $videoBox;
        }

        //--------- BOX video moi
        $newVideo = VideoObj::serialize(
            Obj::VIDEO_NEW, VtVideoBase::getNewVideo('', Yii::$app->params['app.home.limit']), true, false
        );
        if (isset($newVideo['content']) && $newVideo['content']) {
            $dataResponse[] = $newVideo;
        }

        //---------  BOX video da xem
        $boxVideoHistory = [];
        if ($userId && $userId != '') {
            
            $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, Yii::$app->params['app.home.limit']);

            $durationPercent = [];
            foreach ($histories as $history) {
                $parentIds[$history['itemId']] = $history['parent_id'];
                if ($history['duration']) {
                    // Neu thoi luong con lon hon 20s thi moi hien thi xem tiep
                    if ($history['duration'] - $history['time'] > 20) {
                        $historyIds[] = $history['itemId'];
                        $durationPercent[$history['itemId']] = 100 * $history['time'] / $history['duration'];
                    }
                } else {
                    $durationPercent[$history['itemId']] = 0;
                    $historyIds[] = $history['itemId'];
                }
            }

            $historyIds = array();
            foreach ($histories as $history) {
                $historyIds[] = $history['itemId'];
            }
            if (!empty($historyIds)) {
                $boxVideoHistory = VideoObj::serialize(
                    Obj::VIDEO_HISTORY, VtVideoBase::getByIdsQuery(null, $historyIds, Yii::$app->params['app.home.limit'])
                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')]), false, null, null, $durationPercent
                );

                if (isset($boxVideoHistory['content']) && $boxVideoHistory['content']) {
                    $dataResponse[] = $boxVideoHistory;
                }
            }
        }
        $limitHotChannel = Yii::$app->params['app.home.hotchannel.limit'];

        if (!$limitHotChannel) {
            $limitHotChannel = 5;
        }

        // Danh sach video theo kenh
        if (!$this->userId) {
            $arrChannel = [];
            $listHotChannel = VtUser::getHotUser($limitHotChannel);
            foreach ($listHotChannel as $channel) {
                $arrChannel[] = $channel;
            }
        } else {
            $channelFollows = VtUserFollowBase::getChannelFollowQuery($this->userId, $limitHotChannel)->all();
            $arrChannel = [];
            $arrChannelId = [];
            foreach ($channelFollows as $channel) {
                $arrChannel[] = $channel;
                $arrChannelId[] = $channel['id'];
            }

            if (count($channelFollows) < $limitHotChannel) {
                $listHotChannel = VtUser::getHotUser($limitHotChannel);

                $arrChannel = Utils::mergeById($arrChannel, $listHotChannel, 'id', $limitHotChannel);
            }
        }

        if (count($arrChannel)) {
            $alreadyAdd = [];
            // Voi moi kenh lay duoc, hien thi danh sach video moi nhat
            for ($i = 0; $i < count($arrChannel); $i++) {
                if (!in_array($arrChannel[$i]['id'], $alreadyAdd)) {
                    $alreadyAdd[] = $arrChannel[$i]['id'];
                    $newestVideos = VideoObj::serialize(
                        Obj::VIDEO_NEWEST_CHANNEL . $arrChannel[$i]['id'], VtVideoBase::getVideosByChannel($arrChannel[$i]['id'], Yii::$app->params['app.home.limit'], 0, 'NEW'), false, $arrChannel[$i]['full_name']
                    );

                    if (isset($newestVideos['content']) && $newestVideos['content']) {
                        $dataResponse[] = $newestVideos;
                    }
                }
            }
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse,
            'popup' => VtFlow::loadPromotionPopup($this->msisdn)
        ];
    }

    public function actionGetHome()
    {
        $dataResponse = array();
        //@todo: sua lai thanh lay kenh
        $newFeed = VideoObj::serialize(
            Obj::VIDEO_NEWSFEED, VtVideoBase::getRecommendVideoMixQuery('', Yii::$app->params['app.home.limit']), true, false, 'NEWSFEED'
        );

        if (isset($newFeed['content']) && $newFeed['content']) {
            $dataResponse[] = $newFeed;
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse,
            'popup' => VtFlow::loadPromotionPopup($this->msisdn)
        ];
    }

    public function actionGetHomeWeb()
    {
        $dataResponse = array();
        /*$hashtagHomePage = VtHashtagBase::getHashtagHomePage(Yii::$app->params['hastag.limit.box.home'], 0);
        if ($hashtagHomePage) {
            $dataResponse[] = $hashtagHomePage;
        }*/

        $limitN = Yii::$app->params['video.limit.n.box.home'];

        //home_video_v2
        $videoBox = VideoObj::serialize(
            Obj::VIDEO_HOT_2, VtVideoBase::getVideoHomePage($limitN, true), true,  Yii::t('wap', 'Có thể bạn thích') , 'NEWSFEED'
        );

        //Lay danh sach video moi

        if (isset($videoBox['content']) && $videoBox['content']) {
            $dataResponse[] = $videoBox;
        }

        //--------- BOX video moi
        $newVideo = VideoObj::serialize(
            Obj::VIDEO_NEW, VtVideoBase::getNewVideo('', Yii::$app->params['app.home.limit']), true, false
        );
        if (isset($newVideo['content']) && $newVideo['content']) {
            $dataResponse[] = $newVideo;
        }

        //---------  BOX video da xem
        $boxVideoHistory = [];
        if ($this->userId) {

            $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, Yii::$app->params['app.home.limit']);

            $durationPercent = [];
            foreach ($histories as $history) {
                $parentIds[$history['itemId']] = $history['parent_id'];
                if ($history['duration']) {
                    // Neu thoi luong con lon hon 20s thi moi hien thi xem tiep
                    if ($history['duration'] - $history['time'] > 20) {
                        $historyIds[] = $history['itemId'];
                        $durationPercent[$history['itemId']] = 100 * $history['time'] / $history['duration'];
                    }
                } else {
                    $durationPercent[$history['itemId']] = 0;
                    $historyIds[] = $history['itemId'];
                }
            }

            $historyIds = array();
            foreach ($histories as $history) {
                $historyIds[] = $history['itemId'];
            }
            if (!empty($historyIds)) {
                $boxVideoHistory = VideoObj::serialize(
                    Obj::VIDEO_HISTORY, VtVideoBase::getByIdsQuery(null, $historyIds, Yii::$app->params['app.home.limit'])
                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')]), false, null, null, $durationPercent
                );

                if (isset($boxVideoHistory['content']) && $boxVideoHistory['content']) {
                    $dataResponse[] = $boxVideoHistory;
                }
            }
        }
        $limitHotChannel = Yii::$app->params['app.home.hotchannel.limit'];

        if (!$limitHotChannel) {
            $limitHotChannel = 5;
        }

        // Danh sach video theo kenh
        if (!$this->userId) {
            $arrChannel = [];
            $listHotChannel = VtUser::getHotUser($limitHotChannel);
            foreach ($listHotChannel as $channel) {
                $arrChannel[] = $channel;
            }
        } else {
            $channelFollows = VtUserFollowBase::getChannelFollowQuery($this->userId, $limitHotChannel)->all();
            $arrChannel = [];
            $arrChannelId = [];
            foreach ($channelFollows as $channel) {
                $arrChannel[] = $channel;
                $arrChannelId[] = $channel['id'];
            }

            if (count($channelFollows) < $limitHotChannel) {
                $listHotChannel = VtUser::getHotUser($limitHotChannel);

                $arrChannel = Utils::mergeById($arrChannel, $listHotChannel, 'id', $limitHotChannel);
            }
        }

        if (count($arrChannel)) {
            $alreadyAdd = [];
            // Voi moi kenh lay duoc, hien thi danh sach video moi nhat
            for ($i = 0; $i < count($arrChannel); $i++) {
                if (!in_array($arrChannel[$i]['id'], $alreadyAdd)) {
                    $alreadyAdd[] = $arrChannel[$i]['id'];
                    $newestVideos = VideoObj::serialize(
                        Obj::VIDEO_NEWEST_CHANNEL . $arrChannel[$i]['id'], VtVideoBase::getVideosByChannel($arrChannel[$i]['id'], Yii::$app->params['app.home.limit'], 0, 'NEW'), false, $arrChannel[$i]['full_name']
                    );

                    if (isset($newestVideos['content']) && $newestVideos['content']) {
                        $dataResponse[] = $newestVideos;
                    }
                }
            }
        }
        //Lay anh banner
        $bannerArr = VtSlideshowBase::getSlideShowQuery("HOME_BANNER", 1, 0)->asArray()->all();
        if ($bannerArr) {
            $banner = [
                "image" => VtHelper::getThumbUrl($bannerArr[0]['bucket'], $bannerArr[0]['path'], VtHelper::SIZE_CHANNEL2),
                "link" => $bannerArr[0]["href"]
            ];
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse,
            'popup' => VtFlow::loadPromotionPopup($this->msisdn),
            'banner' => $banner
        ];
    }


    /**
     * Suggest tim kiem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionSearchSuggestion()
    {
        $appId = Yii::$app->id;
        $query = trim(Yii::$app->request->get('query', ''));

        if (empty($query)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Vui lòng nhập nhập nội dung tìm kiếm')
            ];
        }

        if (strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }
        
        $videos = VtVideoSearchBase::search($query, 0, Yii::$app->params['app.search.suggestion.limit']);

        $videoSuggestionArr = [
            'name' => Yii::t('wap', 'Video'),
            'type' => 'VOD',
            'content' => array()
        ];
        foreach ($videos as $video) {

            $suggestion = array();
            $suggestion['id'] = $video['_source']['id'];
            $suggestion['name'] = Utils::truncateWords($video['_source']['name'], 50);
            $suggestion['coverImage'] = VtHelper::getThumbUrl($video['_source']['bucket'], $video['_source']['path'], VtHelper::SIZE_VIDEO);
            $suggestion['type'] = 'VOD';
            if ($appId == 'app-wap') {
                $suggestion['slug'] = $video['_source']['slug'];
            }
            $suggestion['linkSocial'] = Yii::$app->params['cdn.site'] . "video/" . $video['_source']['id'] . "/" . $video['_source']['slug'] . "?utm_source=SOCIAL";
            $videoSuggestionArr['content'][] = $suggestion;
        }

        $channels = VtUserSearchBase::search($query, 0, Yii::$app->params['app.search.suggestion.limit']);
        $channelSuggestionArr = [
            'name' => Yii::t('wap', 'Kênh'),
            'type' => 'CHANNEL',
            'content' => array()
        ];

        foreach ($channels as $channel) {
            //var_dump('ssss');die;
            $suggestion = array();
            $suggestion['id'] = $channel['_source']['id'];
            $suggestion['name'] = $channel['_source']['full_name'];

            $suggestion['video_count'] = $channel['_source']['video_count'];
            $suggestion['follow_count'] = $channel['_source']['follow_count'];
            $suggestion['num_video'] = $channel['_source']['video_count'];
            $suggestion['num_follow'] = $channel['_source']['follow_count'];

            $suggestion['coverImage'] = VtHelper::getThumbUrl($channel['_source']['bucket'], $channel['_source']['path'], VtHelper::SIZE_VIDEO);
            $suggestion['type'] = 'CHANNEL';
            if ($appId == 'app-wap') {
                $suggestion['slug'] = $channel['_source']['slug'];
            }
            $channelSuggestionArr['content'][] = $suggestion;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                $videoSuggestionArr,
                $channelSuggestionArr
            ]
        ];
    }

    /**
     * Chi tiet tim kiem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionSearch()
    {
        $query = trim(Yii::$app->request->get('query', ''));
        $search_type = trim(Yii::$app->request->get('type', ''));
        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit',''));
        if (empty($query) || strlen(utf8_decode($query)) > 255 || strlen(utf8_decode($query)) < 3) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối thiểu 3 ký tự, tối đa 255 kí tự')
            ];
        }
        if ($limit) {
            $searchVideos = VtVideoSearchBase::search($query, $offset, $limit, $search_type);
        } else {
            $searchVideos = VtVideoSearchBase::search($query, $offset, Yii::$app->params['app.search.first_page.limit'], $search_type);
        }

        $videoIds = ArrayHelper::getColumn($searchVideos, '_id');

        $searchChannels = VtChannelSearchBase::search($query, Yii::$app->params['app.search.first_page.limit']);
        $channelIds = ArrayHelper::getColumn($searchChannels, '_id');
        if (!empty($videoIds)) {
            $videos = VideoObj::serialize(
                Obj::VIDEO_SEARCH, VtVideoBase::getVideosByIdsQuery($videoIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
            );
        }

        if (!empty($userIds)) {
            $channels = ChannelObj::serialize(
                Obj::CHANNEL_SEARCH, VtChannelBase::getByIdsQuery($channelIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($channelIds)) . ')')])
            );
        }

        $dataResponse = array();

        if (isset($videos['content']) && $videos['content']) {
            $dataResponse[] = $videos;
        }

        if (isset($channels['content']) && $channels['content']) {
            $dataResponse[] = $channels;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }


    /**
     * Suggest tim kiem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionSearchSuggestionDistribution($categoryIds = [])
    {
        $appId = Yii::$app->id;
        $query = trim(Yii::$app->request->get('query', ''));

        if (empty($categoryIds)) {
            $categoryIds = explode(',', trim(Yii::$app->request->get('category_ids', '')));
        }

        if (empty($query)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Vui lòng nhập nhập nội dung tìm kiếm')
            ];
        }

        if (strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }

        $videos = VtVideoSearchBase::search($query, Yii::$app->params['app.search.suggestion.limit'], $categoryIds);

        $videoSuggestionArr = [
            'name' => 'Video',
            'type' => 'VOD',
            'content' => array()
        ];
        foreach ($videos as $video) {

            $suggestion = array();
            $suggestion['id'] = $video['_source']['id'];
            $suggestion['name'] = Utils::truncateWords($video['_source']['name'], 70);
            $suggestion['coverImage'] = VtHelper::getThumbUrl($video['_source']['bucket'], $video['_source']['path'], VtHelper::SIZE_VIDEO);
            $suggestion['type'] = 'VOD';
            if ($appId == 'app-wap') {
                $suggestion['slug'] = $video['_source']['slug'];
            }
            $videoSuggestionArr['content'][] = $suggestion;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                $videoSuggestionArr
            ]
        ];
    }

    /**
     * Chi tiet tim kiem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionSearchDistribution($categoryIds = [])
    {
        $query = trim(Yii::$app->request->get('query', ''));

        if (empty($categoryIds)) {
            $categoryIds = explode(',', trim(Yii::$app->request->get('category_ids', '')));
        }


        if (empty($categoryIds)) {
            $limit = Yii::$app->params['app.search.first_page.limit'];
        } else {
            $limit = Yii::$app->params['app.search.page.limit'];
        }

        if (empty($query) || strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }

        $searchVideos = VtVideoSearchBase::search($query, $limit, $categoryIds);
        $videoIds = ArrayHelper::getColumn($searchVideos, '_id');


        if (!empty($videoIds)) {
            $videos = VideoObj::serialize(
                Obj::VIDEO_SEARCH, VtVideoBase::getVideosByIdsQuery($videoIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
            );
        }

        $dataResponse = array();

        if (isset($videos['content']) && $videos['content']) {
            $dataResponse[] = $videos;
        }


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }

    public function actionSearchMoreVideo($categoryIds = [])
    {
        $query = trim(Yii::$app->request->get('query', ''));
        $limit = trim(Yii::$app->request->get('limit', 10));
        $offset = trim(Yii::$app->request->get('offset', 0));

        if (empty($categoryIds)) {
            $categoryIdsStr = trim(Yii::$app->request->get('category_ids', ''));
            if (!empty($categoryIdsStr)) {
                $categoryIds = explode(',', $categoryIdsStr);
            }
        }

        if (empty($query) || strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }

        $searchVideos = VtVideoSearchBase::search($query, $offset, $limit, $categoryIds);
        $searchIds = ArrayHelper::getColumn($searchVideos, '_id');

//        $provider = new ArrayDataProvider([
//            'allModels' => $searchIds,
//            'pagination' => [
//                'pageSize' => $limit,
//            ],
//        ]);
//        $videoIds = $provider->getModels();


        $videoIds = [];
        for ($i = $offset; $i < ($offset + $limit); $i++) {
            if (array_key_exists($i, $searchIds)) {
                $videoIds[] = $searchIds[$i];
            }
        }

        if (!empty($videoIds)) {
            $videos = VideoObj::serialize(
                Obj::VIDEO_SEARCH, VtVideoBase::getVideosByIdsQuery($videoIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
            );
        }

        $dataResponse = array();

        if (isset($videos['content']) && $videos['content']) {
            $dataResponse[] = $videos;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }


    public function actionSearchMoreChannel()
    {
        $query = trim(Yii::$app->request->get('query', ''));
        $limit = trim(Yii::$app->request->get('limit', 10));
        $offset = trim(Yii::$app->request->get('offset', 0));

        if (empty($query) || strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }

        $searchUsers = VtUserSearchBase::search($query, 300);
        $searchIds = ArrayHelper::getColumn($searchUsers, '_id');

//        $provider = new ArrayDataProvider([
//            'allModels' => $searchIds,
//            'pagination' => [
//                'pageSize' => $limit,
//            ],
//        ]);
//        $userIds = $provider->getModels();

        $userIds = [];
        for ($i = $offset; $i < ($offset + $limit); $i++) {
            if (array_key_exists($i, $searchIds)) {
                $userIds[] = $searchIds[$i];
            }
        }

        if (!empty($userIds)) {
            $channels = ChannelObj::serialize(
                Obj::CHANNEL_SEARCH, VtUserBase::getByIdsQuery($userIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($userIds)) . ')')])
            );
        }

        $dataResponse = array();

        if (isset($channels['content']) && $channels['content']) {
            $dataResponse[] = $channels;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }


    public function actionGetKeywords()
    {
        $limit = trim(Yii::$app->request->get('limit', Yii::$app->params['app.hot.keyword.limit']));

        $keywords = ArrayHelper::getColumn(VtHotKeywordBase::getActiveKeyword($limit), 'content');


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $keywords
        ];
    }

    /**
     * Trang Home
     * @author ducda2@viettel.com.vn
     * khanhdq modify to can be called from controller
     */
    public function actionGetMoreContent($id = '', $offset = 0, $limit = '')
    {
        //var_dump($id);die;//string(28) video_history
        $id = trim($id);
        $limit = trim(($limit) ? $limit : Yii::$app->params['app.showMore.limit']);
        $offset = trim($offset);
        $contents = array();

        switch (strtolower($id)) {

            case strtolower(Obj::VIDEO_HOT_2):

                if (empty($offset)) {
                    $limitN = Yii::$app->params['video.limit.n.box.home'];

                    //home_video_v2
                    $contents = VideoObj::serialize(
                        Obj::VIDEO_HOT_2, VtVideoBase::getVideoHomePage($limitN, true), true, Yii::t('wap', 'Có thể bạn thích'), 'NEWSFEED'
                    );

                } else {
                    $idsPage1 = explode(",", Yii::$app->cache->get("VIDEO_HOMEPAGE_IDS"));
                    $idsPage1 = null;
                    $contents = VideoObj::serialize(
                        Obj::VIDEO_HOT_2, VtVideoBase::getHotVideoWithOutIds($idsPage1, $limit, $offset), true
                    );
                }
                break;

            case strtolower(Obj::VIDEO_HOT):

                $idsPage1 = explode(",", Yii::$app->cache->get("VIDEO_HOMEPAGE_IDS"));
                $contents = VideoObj::serialize(
                    Obj::VIDEO_HOT, VtVideoBase::getHotVideoWithOutIds($idsPage1, $limit, $offset), true
                );

                break;
            case strtolower(Obj::TET_HOLIDAY):
                $categoryIdStr = VtConfigBase::getConfig('category.tet.holiday', 0);
                $arrCategory = explode(",", $categoryIdStr);
                $contents = VideoObj::serialize(
                    Obj::TET_HOLIDAY, VtVideoBase::getVideosByCategory($arrCategory, $limit, $offset), true
                );
                break;
            case strtolower(Obj::WORLD_CUP):
                $categoryIdStr = VtConfigBase::getConfig('category.worldcup', 0);
                $arrCategory = explode(",", $categoryIdStr);
                $contents = VideoObj::serialize(
                    Obj::WORLD_CUP, VtVideoBase::getVideosByCategory($arrCategory, $limit, $offset), true
                );
                break;
            case strtolower(Obj::VIDEO_HOME):

//                $deviceId = Yii::$app->request->get('device_id', null);
                $deviceId = '';

                if (!empty($deviceId) && Yii::$app->params['recommendation']['enable']) {
                    $recommendVideoIds = RecommendationService::getVideoIds(RecommendationService::BOX_RECOMMEND, $deviceId);

                    $provider = new ArrayDataProvider([
                        'allModels' => $recommendVideoIds,
                        'pagination' => [
                            'pageSize' => 10,
                        ],
                    ]);

                    $videoIds = $provider->getModels();

                } else {
                    $histories = [];
                    $categoryIds = [];
                    if ($this->isValidUser()) {
                        $categoryIds = array_filter(explode(',', Yii::$app->getCache()->get("category_list_watch_" . $this->userId)));
                        $histories = VtHistoryViewBase::getByUser($this->userId, $this->msisdn, 200, 0, VtHistoryViewBase::TYPE_VOD);
                    }

                    if (count($categoryIds) < Yii::$app->params['category.hot.limit']) {
                        $categories = VtGroupCategoryBase::getAllHotCategories(Yii::$app->params['category.hot.limit'] - count($categoryIds), $categoryIds);
                        $categoryIds = array_merge($categoryIds, ArrayHelper::getColumn($categories, 'id'));
                    }

                    if ($offset == 0) {
                        $videoHotLimit = Yii::$app->params['video.hot.random.limit'];
                        $videoIds = ArrayHelper::getColumn(VtVideoHotBase::getByCategoryIds($categoryIds, $videoHotLimit, $offset), 'video_id');
                        $videoIds = array_diff($videoIds, $histories);
                    } else {
                        $videoHotOffset = $offset + (Yii::$app->params['video.hot.random.limit'] - $limit);
                        $videoIds = ArrayHelper::getColumn(VtVideoHotBase::getByCategoryIds($categoryIds, $limit, $videoHotOffset), 'video_id');
                    }
                }


                $contents = VideoObj::serialize(
                    Obj::VIDEO_HOME, VtVideoBase::getByIdsQuery(null, $videoIds, $limit)
                    ->orderBy([new Expression('RAND()')]), false
                );
                break;

            case strtolower(Obj::VIDEO_NEW):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_NEW, VtVideoBase::getNewVideo("", $limit, $offset), true
                );
                break;
            case strtolower(Obj::VIDEO_RECOMMEND):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_RECOMMEND, VtVideoBase::getRecommendVideoMixQuery('', $limit, $offset), false
                );

                break;
            case strtolower(Obj::VIDEO_OWNER):

                if (!$this->isValidUser()) {
                    return $this->authJson;
                }

                $contents = VideoObj::serialize(
                    Obj::VIDEO_OWNER, VtVideoBase::getAllVideosByUser('', $this->userId, $limit, $offset, VtVideoBase::TYPE_VOD), false, Obj::getName(Obj::VIDEO_OWNER)
                );

                break;

            case strtolower(Obj::VIDEO_WATCH_LATER):

                if (!$this->isValidUser()) {
                    return $this->authJson;
                }

                $ids = array_filter(explode(',', Yii::$app->getCache()->get("video_list_watch_later_" . $this->userId)));

                if (!empty($ids)) {
                    $histories = VtHistoryViewBase::getTimeByIds($this->userId, $this->msisdn, $ids, VtVideoBase::TYPE_VOD);

                    foreach ($histories as $history) {
                        if ($history['duration']) {
                            $durationPercent[$history['itemId']] = 100 * $history['time'] / ($history['duration'] * 60);
                        } else {
                            $durationPercent[$history['itemId']] = 0;
                        }
                    }

                    $contents = VideoObj::serialize(
                        Obj::VIDEO_WATCH_LATER, VtVideoBase::getByIdsQuery('', $ids, $limit, $offset)
                        ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($ids)) . ')')]), false, Obj::getName(Obj::VIDEO_WATCH_LATER), null, $durationPercent
                    );
                } else {
                    $contents = [
                        'id' => Obj::VIDEO_WATCH_LATER,
                        'name' => Obj::getName(Obj::VIDEO_WATCH_LATER),
                        'type' => 'VOD'
                    ];
                }

                break;

            case strtolower(Obj::VIDEO_HISTORY):
                if (!$this->isValidUser()) {
                    return $this->authJson;
                }
                $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, 200, $offset);
                $durationPercent = [];
                foreach ($histories as $history) {
                    $historyIds[] = $history['itemId'];
                    $parentIds[$history['itemId']] = $history['parent_id'];
                    if ($history['duration']) {
                        $durationPercent[$history['itemId']] = 100 * $history['time'] / $history['duration'];
                    } else {
                        $durationPercent[$history['itemId']] = 0;
                    }
                }

                $historyIds = array();
                foreach ($histories as $history) {
                    $historyIds[] = $history['itemId'];
                }

                if (!empty($historyIds)) {
                    $contents = VideoObj::serialize(
                        Obj::VIDEO_HISTORY, VtVideoBase::getByIdsQuery('', $historyIds, $limit, $offset)
                        ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')]), false, null, null, $durationPercent
                    );
                } else {
                    $contents = [
                        'id' => Obj::VIDEO_HISTORY,
                        'name' => Obj::getName(Obj::VIDEO_HISTORY),
                        'type' => 'VOD'
                    ];
                }

                break;

            case strtolower(Obj::VIDEO_CHANNEL_FOLLOW):

                $arrChannel = [];
                if ($this->isValidUser()) {
                    $arrChannel = ArrayHelper::getColumn(VtUserFollowBase::getChannelFollowQuery($this->userId)->asArray()->all(), 'id');
                }
                $limitHotChannel = Yii::$app->params['app.home.hotchannel.limit'];

                if (!$limitHotChannel) {
                    $limitHotChannel = 5;
                }
                //Neu so kenh theo doi cu nguoi dung <5 thi them du lieu tu admin
                $totalChannelFollow = count($arrChannel);
                if ($totalChannelFollow < $limitHotChannel) {
                    $listHotChannel = VtUser::getHotUser($limitHotChannel - $totalChannelFollow);
                    $adminRecommendChannel = [];
                    foreach ($listHotChannel as $channel) {
                        // neu kenh hot khong thuoc kenh follow va chua den gioi han kenh show trang chu
                        if (!in_array($channel['id'], $arrChannel) && $totalChannelFollow < $limitHotChannel) {
                            $adminRecommendChannel[] = $channel['id'];
                        }
                    }

                    $arrChannel = array_merge($arrChannel, $adminRecommendChannel);
                }

                $contents = VideoObj::serialize(
                    Obj::VIDEO_CHANNEL_FOLLOW, VtVideoBase::getVideosByUser('', $arrChannel, $limit, $offset), false, false, 'VOD'
                );

                break;

            case strtolower(Obj::LIST_CHANNEL_FOLLOW):
                $contents = ChannelObj::serialize(
                    Obj::CHANNEL_FOLLOW, VtUserFollowBase::getChannelFollowQuery($this->userId, $limit, $offset), false, false, false, $limit
                );
                break;
            case strtolower(Obj::LIST_CHANNEL_FOLLOW_WITH_HOT):
                $contents = ChannelObj::serialize(
                    Obj::CHANNEL_FOLLOW, VtUserFollowBase::getChannelFollowQuery($this->userId, $limit, $offset), false, false, true, $limit
                );
                break;
            case strtolower(Obj::LIST_HOT_CHANNEL):
                $arrCF = [];
                if ($this->userId) {
                    $channelFollow = ChannelObj::serialize(
                        Obj::CHANNEL_FOLLOW, VtUserFollowBase::getChannelFollowQuery($this->userId), false, false
                    );
                    foreach ($channelFollow['content'] as $cF) {
                        $arrCF[] = $cF['channel_id'];
                    }
                }
                $contents = ChannelObj::serialize(
                    Obj::CHANNEL_FOLLOW, VtUserBase::getHotUserQuery($limit, $offset, $arrCF), true, false
                );
                break;

            case strtolower(Obj::MY_PLAYLIST):

                if (!$this->isValidUser()) {
                    return $this->authJson;
                }

                $contents = UserPlaylistObj::serialize(
                    Obj::MY_PLAYLIST, VtUserPlaylistItemBase::getPlaylistByUserQuery($this->userId, $limit, $offset), false, Obj::getName(Obj::MY_PLAYLIST)
                );

                break;
            case strtolower(Obj::HISTORY_SEARCH):
                $contents = \common\libs\VtUser::getHistorySearch($offset, $limit);
                break;
            case strtolower(Obj::CATEGORY_PARENT):
                $contents = \common\modules\v2\libs\CategoryObj::serialize(
                    Obj::CATEGORY_PARENT, \common\models\VtGroupCategoryBase::getParents($offset, $limit, true,false,'VOD' ), true
                );

                break;
            case strtolower(Obj::VIDEO_FREE):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_FREE, VtVideoBase::getVideosFree('', $limit, $offset), true
                );
                break;
            case strtolower(Obj::VIDEO_PAID):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_PAID, VtVideoBase::getVideosPaid('', $limit, $offset), true
                );
                break;

            default:

                if (strpos(strtolower(strtolower($id)), Obj::RELATED_OF_VIDEO) === 0) {
                    $videoId = trim(str_replace(Obj::RELATED_OF_VIDEO, '', $id));

                    $video = VtVideoBase::getDetail($videoId);
                    if($video){

                        $objUser = VtUserBase::getById($video['created_by']);
                        $objCategory = VtGroupCategoryBase::getById($video['category_id']);
                        $deviceId = Yii::$app->request->get('device_id', '');

                        $contents = null;
                        if (!empty($deviceId) && Yii::$app->params['recommendation']['enable']) {

                            $page = floor($offset / $limit) + 1;
                            $videoIds = RecommendationService::getVideoIds(RecommendationService::BOX_RELATED, $deviceId, $videoId, [
                                'channel_id' => $video['created_by'],
                                'channel_name' => $objUser['full_name'],
                                'video_name' => $video['name'],
                                'tag_name' => $video['tag'],
                                'category_id' => $objCategory['id'],
                                'category_name' => $objCategory['name'],
                                'category_parent_id' => $objCategory['parent_id'],
                            ], $page, $limit);


                            if (!empty($videoIds)) {
                                $contents = VideoObj::serialize(
                                    Obj::VIDEO_RECOMMEND_RELATE, VtVideoBase::getVideosByIdsQuery($videoIds, $limit)
                                    ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
                                );
                            }
                        }

                        if (!isset($contents['content']) || count($contents['content']) == 0) {
                            $contents = VideoObj::serialize(
                                Obj::RELATED_OF_VIDEO . $video['id'], VtVideoBase::getVideoRelatedQuery($video['id'], $video['category_id'], $video['created_by'], $limit, $offset, $video['published_time']), false
                            );
                        }

                    }
                } elseif (strpos(strtolower(strtolower($id)), Obj::VIDEO_OF_PLAYLIST) === 0) {

                    $arrTmp = explode("_", $id);

                    $objPlaylist = VtUserPlaylistBase::getDetail($arrTmp[2]);
                    if (!$objPlaylist) {
                        $contents = [
                            'id' => $id,
                            'name' => "PLAYLIST",
                            'type' => 'VOD'
                        ];
                    } else {
                        $contents = VideoObj::serialize(
                            $id, VtVideoBase::getVideosByPlaylist($arrTmp[2], $limit, $offset, VtVideoBase::TYPE_VOD), false, $objPlaylist['name']
                        );
                    }
                } elseif (strpos(strtolower(strtolower($id)), Obj::VIDEO_USER_LIKE) === 0) {
                    $arrTmp = explode("_", $id);
                    //chi cho phep truyen ID la so
                    if (!is_numeric($arrTmp[3]) || count($arrTmp) > 4) {
                        return "INVALID";
                    }
                    // TODO Phan quyen
                    $contents = VideoObj::serialize(
                        $id, VtFavouriteVideoBase::getListVideos($arrTmp[3], $limit, $offset), false
                    );
              } elseif (strpos(strtolower(strtolower($id)), Obj::VIDEO_NEW_OF_USER) === 0) {
                    $itemId = str_replace(Obj::VIDEO_NEW_OF_USER, '', $id);
                    $user = VtUserBase::getActiveUserById($itemId);
//                    if (!$user) {
//                        return "INVALID";
//                    }

                    $nameTitle = Yii::t('wap', 'Mới nhất');
                    // lay danh sach id của kenh
                    $channellist = VtChannelBase::getChannelListById($itemId);

                    if(!empty($channellist)){
                        $list_id = '';
                        foreach($channellist as $item){
                            $list_id .= $item['id'].',';
                        }
                        $list_id = substr($list_id, 0, -1);
                    }
                    if (!$list_id) {
                        return "INVALID";
                    }
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getNewOfChannel($list_id, $limit, $offset,'VOD','NEW'), true, $nameTitle
                    );


                } elseif (strpos(strtolower(strtolower($id)), Obj::VIDEO_OLD_OF_USER) === 0) {
                    $itemId = str_replace(Obj::VIDEO_OLD_OF_USER, '', $id);
                    $user = VtUserBase::getActiveUserById($itemId);
                    if (!$user) {
                        return "INVALID";
                    }

                    $nameTitle = Yii::t('wap', 'Cũ nhất');
                    // lay danh sach id của kenh
                    $channellist = VtChannelBase::getChannelListById($itemId);

                    if(!empty($channellist)){
                        $list_id = '';
                        foreach($channellist as $item){
                            $list_id .= $item['id'].',';
                        }
                        $itemId = substr($list_id, 0, -1);
                    }

                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getNewOfChannel($itemId, $limit, $offset,"VOD", 'OLD'), true, $nameTitle
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_MOST_VIEW_OF_USER) === 0) {
                    $itemId = str_replace(Obj::VIDEO_MOST_VIEW_OF_USER, '', $id);
                    $user = VtUserBase::getActiveUserById($itemId);

                    if (!$user) {
                        return "INVALID";
                    }
                    // lay danh sach id của kenh
                    $channellist = VtChannelBase::getChannelListById($itemId);
                    if(!empty($channellist)){
                        $list_id = '';
                        foreach($channellist as $item){
                            $list_id .= $item['id'].',';
                        }
                        $itemId = substr($list_id, 0, -1);
                    }

                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getNewOfChannel($itemId, $limit, $offset,"VOD", 'MOSTVIEW'), true, Obj::getName(Obj::VIDEO_MOST_VIEW_OF_USER)
                    );

                } elseif (strpos(strtolower(strtolower($id)), Obj::VIDEO_NEW_OF_CHANNEL) === 0) {
                    $itemId = str_replace(Obj::VIDEO_NEW_OF_CHANNEL, '', $id);

                    $channel = VtChannelBase::getChannelById($itemId);
                    if (!$channel) {
                        return [
                            'responseCode' => ResponseCode::SUCCESS,
                            'message' => 'Thành công',
                            'data' => [],
                        ];
                    }

                    $nameTitle = Yii::t('wap', 'Mới nhất');

                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByChannel($itemId, $limit, $offset,'id'), true, $nameTitle
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_OLD_OF_CHANNEL) === 0) {
                    $itemId = str_replace(Obj::VIDEO_OLD_OF_CHANNEL, '', $id);
                    $channel = VtChannelBase::getChannelById($itemId);

                    if (!$channel) {
                        return [
                            'responseCode' => ResponseCode::SUCCESS,
                            'message' => 'Thành công',
                            'data' => [],
                        ];
                    }

                    $nameTitle = Yii::t('wap', 'Cũ nhất');
                    //$nameTitle = ($user['full_name']) ? $user['full_name'] : (substr($user['msisdn'], 0, -3) . "xxx");
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByChannel($itemId, $limit, $offset,'IDOLD'), true, $nameTitle
                    );
                } elseif (strpos(strtolower(strtolower($id)), Obj::VIDEO_NEWEST_CHANNEL) === 0) {
                    $itemId = str_replace(Obj::VIDEO_NEWEST_CHANNEL, '', $id);

                    $channel = VtChannelBase::getChannelById($itemId);
                   
                    if (!$channel) {
                        return "INVALID";
                    }

                    $nameTitle = Yii::t('wap', 'Xem nhiều nhất');
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByChannel($itemId, $limit, $offset, 'NEW'), true, $nameTitle
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_MOST_VIEW_OF_CHANNEL) === 0) {
                    $itemId = str_replace(Obj::VIDEO_MOST_VIEW_OF_CHANNEL, '', $id);
                    $channel = vtChannelBase::getChannelById($itemId);
                    if (!$channel) {
                        return [
                            'responseCode' => ResponseCode::SUCCESS,
                            'message' => 'Thành công',
                            'data' => [],
                        ];
                    }
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByChannel($itemId, $limit, $offset, 'MOSTVIEW'), true, Obj::getName(Obj::VIDEO_MOST_VIEW_OF_CHANNEL)
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::PLAYLIST_PUBLIC_OF_USER) === 0) {
                    $userId = str_replace(Obj::PLAYLIST_PUBLIC_OF_USER, '', $id);

                    $isForSmartTv = Yii::$app->request->get("is_for_smart_tv",0);

                    if($isForSmartTv){
                        $contents = UserPlaylistVideoObj::serialize(
                            $id, VtUserPlaylistItemBase::getPlaylistByUserQuery($userId, $limit, $offset), true, false
                        );
                    }else{
                        $contents = UserPlaylistObj::serialize(
                            $id, VtUserPlaylistItemBase::getPlaylistByUserQuery($userId, $limit, $offset), true, false
                        );
                    }

                } else if (strpos(strtolower(strtolower($id)), Obj::CATEGORY_CHILD) === 0) {
                    //load them chuyen muc con
                    $id = str_replace(Obj::CATEGORY_CHILD . "_", '', $id);

                    $categorys = \common\modules\v2\libs\CategoryObj::serialize(
                        Obj::CATEGORY_PARENT, \common\models\VtGroupCategoryBase::getChilds($id, $offset, $limit), true
                    );
                    $tmpContent = array();
                    $loop = Yii::$app->params['category.load.more.limit'];
                    if (count($categorys['content']) > 0) {
                        foreach ($categorys['content'] as $category) {
                            $id = $category['id'];
                            $name = $category['name'];
                            $tmpBox = \common\modules\v2\libs\VideoObj::serialize(
                                Obj::CATEGORY_CHILD . '_' . $id, \common\models\VtVideoBase::getVideoByCategory($id, null, 0, $loop), false, $name
                            );
                            $tmpContent[] = $tmpBox;
                        }
                    }
                    $contents['content'] = $tmpContent;
                } else if (strpos(strtolower(strtolower($id)), Obj::CATEGORY_CHILD_VIDEO) === 0) {
                    $contentId = str_replace(Obj::CATEGORY_CHILD_VIDEO . "_", '', $id);
                    // lay video theo chuyen muc
                    $group = \backend\models\VtGroupCategory::getCategoryGroup($contentId);
                    if (count($group) == 0) {
                        return "INVALID";
                    }

                    $name = $group['name'];
                    $contents = \common\modules\v2\libs\VideoObj::serialize(
                        $id, \common\models\VtVideoBase::getVideoByCategory($contentId, null, 0, $limit), false, $name);
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_MOST_TRENDING_CATE) === 0) {
                    // lay video theo chuyen muc pho bien nhat
                    $itemId = str_replace(Obj::VIDEO_MOST_TRENDING_CATE . "_", '', $id);
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByCate($itemId, $limit, $offset, 'MOSTTRENDING'), false, Obj::getName(Obj::VIDEO_MOST_TRENDING_CATE)
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_MOST_VIEW_CATE) === 0) {
                    $itemId = str_replace(Obj::VIDEO_MOST_VIEW_CATE . "_", '', $id);
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByCate($itemId, $limit, $offset, 'MOSTVIEW'), false, Obj::getName(Obj::VIDEO_MOST_VIEW_CATE)
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_NEW_CATE) === 0) {
                    $itemId = str_replace(Obj::VIDEO_NEW_CATE . "_", '', $id);
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByCate($itemId, $limit, $offset, 'NEW'), false, Obj::getName(Obj::VIDEO_NEW_CATE)
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::VIDEO_OF_CATEGORY) === 0) {

                    $itemId = str_replace(Obj::VIDEO_OF_CATEGORY . "_", '', $id);

                    if (!is_numeric($itemId)) {
                        return "INVALID";
                    }
                    $objCategory = VtGroupCategoryBase::getActiveById($itemId);

                    $childOfCategory = Yii::$app->cache->get("CATEGORY_CHILD_" . $itemId);
                    if ($childOfCategory) {
                        $childOfCategory = explode(",", $childOfCategory);
                    } else {
                        $childs = VtGroupCategoryBase::getChilds($itemId)->all();
                        if ($childs) {
                            foreach ($childs as $key=>$item){
                                $childOfCategory[] = $item['id'];
                            }

                            Yii::$app->cache->set("CATEGORY_CHILD_" . $itemId, implode(",", $childOfCategory, MobiTVRedisCache::CACHE_1HOUR));
                        }
                    }

                    if ($childOfCategory) {
                        $contents = VideoObj::serialize(
                            $id, VtVideoBase::getVideosByCate(ArrayHelper::merge([$itemId], $childOfCategory), $limit, $offset, 'NEW', false), true,  Utils::truncateWords($objCategory['name'], 27)
                        );
                    } else {
                        $contents = VideoObj::serialize(
                            $id, VtVideoBase::getVideosByCate($itemId, $limit, $offset, 'NEW', false), true,   Utils::truncateWords($objCategory['name'], 27)
                        );
                    }


                }

        }

        //Kiem tra xem co tra ve popup 2010 hay khong?
        if (strpos(strtolower(strtolower($id)), Obj::VIDEO_OF_CATEGORY) === 0) {
            $itemId = str_replace(Obj::VIDEO_OF_CATEGORY . "_", '', $id);
            if($itemId == VtConfigBase::getConfig("event.2010.id", false)){
                $arrMessage2010 = \Yii::$app->params['message.for.2010'];
                $popupMessage=  $arrMessage2010[rand(0, count($arrMessage2010) -1)];
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                    'data' => $contents,
                    'popup'=>[
                        'message'=>$popupMessage,
                        'image'=>'http://myclip.vn/images/woman_01.png'
                    ]
                ];
            }
        }
        //var_dump($contents);die;
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        ];
    }


    /**
     * Ham xoa binh luan
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionDeleteComment()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', 0));

        $obj = VtCommentBase::getDetail($id, true);

        if (empty($obj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        if ($obj->user_id != $this->userId) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Bạn không đủ quyền thực hiện chức năng này!')
            ];
        }

        $obj->status = VtCommentBase::DELETED;
        $obj->updated_at = date('Y-m-d H:i:s');
        $obj->save();

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    /**
     * Ham tu choi duyet / duyet binh luan
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionApprovedComment()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $method = $_SERVER["REQUEST_METHOD"];
        if ($method != "POST") {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => "Method Not Allowed"
            ];
        }

        $id = trim(Yii::$app->request->post('id', 0));
        $status = trim(Yii::$app->request->post('status', -1));
        $obj = VtCommentBase::getDetail($id, true);

        if (empty($obj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        if (!in_array($status, [VtCommentBase::ACTIVE, VtCommentBase::DISAPPROVE])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap',"Trạng thái không hợp lệ")
            ];
        }

        try {
            $obj->status = $status;
            $obj->updated_at = date('Y-m-d H:i:s');
            $obj->save(false);
            if($status == 1){
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap',"Phê duyệt bình luận thành công"),
                    'status' => $obj->status
                ];
            }
            if($status == 2){
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap',"Từ chối bình luận thành công"),
                    'status' => $obj->status
                ];
            }
        }catch (\Exception $ex){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::UNSUCCESS),
                'status' => $obj->status
            ];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'status' => 1
        ];
    }

    /**
     * Ham cap nhat binh luan
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionUpdateComment()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', 0));
        $comment = trim(Yii::$app->request->post('comment', ''));

        if (strlen(utf8_decode($comment)) < Yii::$app->params['comment.minlength'] || strlen(utf8_decode($comment)) > Yii::$app->params['comment.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Quý khách vui lòng nhập bình luận từ ') . Yii::$app->params['comment.minlength'] . Yii::t('wap', ' đến ') . Yii::$app->params['comment.maxlength'] . Yii::t('wap', ' ký tự.')
            ];
        }

        $obj = VtCommentBase::getDetail($id, true);

        if (empty($obj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        if ($obj->user_id != $this->userId) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Bạn không đủ quyền thực hiện chức năng này!')
            ];
        }

        $obj->comment = $comment;
        $obj->status = VtCommentBase::WAIT_APPROVE;
        $obj->updated_at = date('Y-m-d H:i:s');
        $obj->save();

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    public function actionDeleteVideo()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', 0));

        $obj = VtVideoBase::getDetail($id, VtVideoBase::TYPE_VOD, true, true);

        if (empty($obj)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

        if ($obj->created_by != $this->userId) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Bạn không đủ quyền thực hiện chức năng này!')
            ];
        }

        $obj->status = VtVideoBase::STATUS_REMOVE;
        $obj->save(false);

        //Tinh toan so luong Video duoc active cua User
        VtUser::updateVideoCountByUserId($obj->created_by);
        // tinh toan so luong video dươc active của channel
        VtChannelBase::updateVideoCountByChannelId($obj->channel_id);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS)
        ];
    }

    public function actionCheckShowPopupWorldCup()
    {

        Yii::$app->response->format = 'json';
        if (!$this->msisdn) {
            return [
                'errorCode' => 201,
                'message' => Yii::t('wap', 'No have msisdn')
            ];
        }

        if (Yii::$app->session->get("check_show_popup_wc")) {
            return [
                'errorCode' => 201,
                'message' => Yii::t('wap', 'Already check popup')
            ];
        }

        $objSub = VtSubBase::getSub($this->msisdn);
        if ($objSub) {
            Yii::$app->session->set("check_show_popup_wc", 1);
            if ($objSub[0]['sub_service_name'] == 'MYCLIP_WC_GOITUAN_FREE') {
                return [
                    'errorCode' => 200,
                    'message' => 'Popup'
                ];
            }
        } else {
            Yii::$app->session->set("check_show_popup_wc", 1);
            return [
                'errorCode' => 200,
                'message' => 'Popup'
            ];
        }

    }

    /**
     * Suggest tim kiem
     * @author phumx@viettel.com.vn
     * @return array
     */
    public function actionSearchChannelRelatedSuggestion()
    {
        $appId = Yii::$app->id;
        $query = trim(Yii::$app->request->get('query', ''));

        if (empty($query)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Vui lòng nhập nhập nội dung tìm kiếm')
            ];
        }

        if (strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }

        $channels = VtChannelSearchBase::search($query,0, Yii::$app->params['app.search.suggestion.limit']);

        $channelSuggestionArr = [
            'name' => Yii::t('wap', 'Kênh'),
            'type' => 'CHANNEL',
            'content' => array()
        ];

        //--Danh sach kenh lien quan cuar user
        $channelRelated = VtChannelRelatedBase::findAll(["channel_id" => $this->userId]);
        if ($channelRelated) {
            $arrIds = ArrayHelper::getColumn($channelRelated, "channel_related_id");
        } else {
            $arrIds = [];
        }

        foreach ($channels as $channel) {
            //-- neu kenh khong trong danh sach kenh da add
            if (!in_array($channel['_source']['id'], $arrIds)) {
                $suggestion = array();
                $suggestion['id'] = $channel['_source']['id'];
                $suggestion['name'] = $channel['_source']['name'];
                $suggestion['video_count'] = ($channel['_source']['video_count'])?$channel['_source']['video_count']:$channel['_source']['video_count'];
                $suggestion['follow_count'] = ($channel['_source']['follow_count'])? $channel['_source']['follow_count']: 0;

                $suggestion['num_follow'] = ($channel['_source']['video_count'])?$channel['_source']['video_count']:$channel['_source']['video_count'];
                $suggestion['num_video'] = ($channel['_source']['follow_count'])? $channel['_source']['follow_count']: 0;

                $suggestion['avatarImage'] = VtHelper::getThumbUrl($channel['_source']['bucket'], $channel['_source']['path'], VtHelper::SIZE_AVATAR);
                $suggestion['type'] = 'CHANNEL';
                if ($appId == 'app-wap') {
                    $suggestion['slug'] = $channel['_source']['slug'];
                }
                $channelSuggestionArr['content'][] = $suggestion;
            }

        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                $channelSuggestionArr
            ]
        ];
    }

    /**
     * Suggest list package
     * @author manhdt@viettel.com.vn
     * @return array
     */
    public function actionGetAllPackage()
    {

        $allPackages = VtPackageBase::getAllPackage();

        $packagesMapName = ArrayHelper::map($allPackages, 'sub_service_name', 'name');
        $packagesMapCpDataPercentage = ArrayHelper::map($allPackages, 'type', 'cp_data_percentage');

        $Packages = [
            'id' => 'package_list',
            'type' => 'PACKAGE',
            'name' => 'package list',
            'content' => $allPackages
        ];

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                $Packages
            ]
        ];
    }


    /**
     * Get recommend for smarttv
     */
    public function actionGetRecommendForSmartTv()
    {
        $limit = Yii::$app->request->get("limit", 8);
        $offset = Yii::$app->request->get("offset", 8);

        $videos = VtVideoBase::getRecommendVideoMixQuery('', $limit, $offset)->all();

        $arrItems = [];

        foreach ($videos as $video){
            $item = [];
            $item["title"]=$video["name"];
            $item["image_ratio"]='16by9';

            $videoId = $video["id"];

            $item["action_data"]= "{\"videoIdx\": $videoId}"; //"{\"videoIdx:\"" .$video["id"]."}" ;

            $item["is_playable"]=true;
            $item['image_url'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_COVER);
            $arrItems[] = $item;
        }

        return [
            "sections"=>[[
                "title"=> Yii::t('wap', "VOD recommended"),
                "tiles"=>$arrItems
            ]]
        ];

    }


}
