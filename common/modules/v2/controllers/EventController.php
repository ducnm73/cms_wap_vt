<?php

namespace common\modules\v2\controllers;

use common\helpers\Utils;
use common\libs\VtHelper;
use common\models\MongoDBModel;
use common\models\VtAwardBase;
use common\models\VtConfigBase;
use common\models\VtCpBase;
use common\modules\v2\libs\ResponseCode;

class EventController extends \common\controllers\ApiController
{

    public function actionLoadConfig(){

        $arrData['start_event_date']= VtConfigBase::getConfig("START_DATE_EVENT");


        $arrData['event_month_range']= [];

        foreach (\Yii::$app->params['award.month.report'] as $key=>$value){
            $arrTmp=[];
            $arrTmp["key"]=$key;
            $arrTmp["value"]=$value;
            $arrData['event_month_range'][]=$arrTmp;
        }


        $arrData['terms-condition']='<div class="ctn">
                    <p>
                        <strong>
                            1. Giới thiệu chương trình
                        </strong>
                    </p>
                    <p>Nhằm mục đích tạo ra sân chơi dành cho khách hàng, MyClip tổ chức triển khai chương trình Tích
                        điểm tặng quà, Khách hàng tương tác trên MyClip có cơ hội nhận HÀNG NGHÌN giải thưởng thẻ cào và
                        có cơ hội trúng Iphone X</p>
                   
                    <p><strong>2. Thời gian triển khai</strong></p>
                    <p> Thời gian triển khai từ 22/05/2018 – 22/08/2018</p>
                    <div id="time-event">
                        <p>Tháng thứ nhất: Từ 22/05 đến 21/06</p>
                        <p>Tuần 1: 22/05-28/05</p>
                        <p>Tuần 2: 29/05-04/06</p>
                        <p>Tuần 3: 05/06-11/06</p>
                        <p>Tuần 4: 12/06-18/06</p>
                        <p>Tháng thứ hai: Từ 22/06 đến 21/07</p>
                        <p>Tuần 1: 22/06-28/06</p>
                        <p>Tuần 2: 29/06-5/07</p>
                        <p>Tuần 3: 06/07-12/07</p>
                        <p>Tuần 4: 13/07-19/07</p>
                        <p>Tháng thứ ba: 22/07 đến 22/08</p>
                        <p>Tuần 1: 22/07-28/07</p>
                        <p>Tuần 2: 29/07-04/08</p>
                        <p>Tuần 3: 05/08-11/08</p>
                        <p>Tuần 4: 12/08-18/08</p>
                    </div>

                    <p><strong>3. Đối tượng tham gia</strong></p>
                    <p>Tất cả các thuê bao Viettel tham gia mạng xã hội MyClip</p>
                    <p><strong>4. Nội dung chương trình</strong></p>
                    <p><strong>4.1 Cơ cấu giải thưởng</strong></p>
                    <p>Khách hàng tham gia chương trình bằng cách tương tác với mạng xã hội MyClip có các giải thưởng
                        sau:</p>
                    <p>(1) 500 khách hàng đăng ký gói tháng MyClip đầu tiên trong tháng nhận thẻ cào 10.000 đồng</p>
                    <p>(2) 2.500 khách hàng đầu tiên đăng ký gói ngày MyClip duy trì dịch vụ 7 ngày (trừ cước 7 lần)
                        nhận thẻ cào 10.000 đồng</p>
                    <p> (3) 1.000 lượt share video MyClip lên facebook đầu tiên trong tháng nhận thẻ cào 10.000 đồng</p>
                    <p>(4) 500 khách hàng có video upload được bình luận nhiều nhất trên MyClip trong tháng nhận thẻ cào
                        30.000 đồng</p>
                    <p>(5) 500 khách hàng có video upload được like nhiều nhất trên MyClip trong tháng nhận thẻ cào
                        30.000 đồng</p>
                    <p>(6) 500 khách hàng có kênh video upload được theo dõi nhiều nhất trên MyClip trong tháng nhận thẻ
                        cào 30.000 đồng</p>
                    <p>(7) 2 khách hàng có video upload được bình luận nhiều nhất trên MyClip trong tuần nhận thẻ cào
                        20.000 đồng</p>
                    <p>(8) 2 khách hàng có video upload được like nhiều nhất trên MyClip trong tuần nhận thẻ cào 20.000
                        đồng</p>
                    <p>(9) 2 khách hàng có kênh video upload được theo dõi nhiều nhất trên MyClip trong tuần nhận thẻ
                        cào 20.000 đồng</p>
                    <p>(10) 500 khách hàng đầu tiên upload video trên MyClip trong tháng nhận thẻ cào 10.000 đồng</p>
                    <p>(11) Khách hàng Tích điểm cao nhất trong tháng trúng Iphone X: </p>
                    <p><strong>4.2. Cơ chế tích điểm nhận Iphone X như sau</strong></p>

                    <p>- Đăng ký thành công gói cước tháng cộng 10.000 điểm</p>
                    <p>- Đăng ký thành công gói cước ngày MyClip và duy trì dịch vụ 7 ngày (trừ cước thành công 7 lần)
                        cộng 10.000 điểm</p>
                    <p> - Share video MyClip trên Facebook cộng 1000 điểm</p>
                    <p> - Upload Video trên UClip cộng 10.000 điểm</p>

                    <p> - Nội dung do khách hàng Upload lên MyClip, video đó được</p>
                    <p> + 1 lượt Xem video trên MyClip cộng 100 điểm</p>
                    <p> + 1 lượt Like video trên MyClip cộng 100 điểm</p>
                    <p> + 1 lượt bình luận video trên MyClip cộng 100 điểm</p>
                    <p> + 1 lượt dislike trên MyClip trừ 20 điểm</p>

                    <p> o 1 lượt theo dõi kênh trên MyClip được 100 điểm</p>

                    <p><strong> * Chi tiết về cách tính điểm: <a href="http://myclip.vn/event/info">XEM TẠI ĐÂY</a> </strong></p>

                    <p>Lưu ý: Giải thưởng là thẻ cào Viettel, Iphone X không có giá trị quy đổi bằng tiền mặt</p>
                    <p>- Nhóm giải 4,5,6: Một thuê bao chỉ được nhận 1 trong 3 giải trong 1 tháng.</p>
                    <p>- Nhóm giải 1,2,3,4,5,6,10,11: Một thuê bao chỉ được nhận 1 giải duy nhất trong 3 tháng</p>

                    <p>- Nhóm giải 7,8,9: Một thuê bao chỉ được nhận 1 trong 3 giải.1 Thuê bao chỉ được nhận một giải
                        tuần duy nhất trong tháng.
                    </p>

                    <p>
                        - Nhóm giải thưởng 4,5,6,7,8,9,11: Nếu trong trường hợp khách hàng có số điểm bằng nhau căn cứ
                        vào cơ cấu điểm khách hàng tích lũy được theo thứ tự ưu tiên share, comment, like, xem, theo dõi
                        cao hơn sẽ được giải. Nếu cơ cấu điểm bằng nhau thì căn cứ vào thời gian share video đầu tiên
                        sớm hơn sẽ được giải
                    </p>
                    <p>
                        - Nhóm giải thưởng 1,2,3,10: sẽ căn cứ thời gian của khách hàng theo mili giây để trao giải.
                    </p>

                    <p>- Sau khi có điểm tổng kết tháng, điểm tháng sau sẽ được trở lại về 0 để khách hàng tích điểm
                        lại từ đầu. Quy chế cộng điểm các tháng tương tự nhau.
                    </p>

                    <p> <a href="http://myclip.vn/event/info"><strong>5. Quy định giải thưởng và trao giải:</strong></a> </p>
                    <p><a style="text-align: center" href="http://myclip.vn/event/guide">BẮT ĐẦU NGAY</a></p>
                    <p>Chương trình do Tổng Công ty Viễn thông Viettel tổ chức và chịu trách nhiệm trước pháp luật. Apple không trực tiếp hoặc gián tiếp tham gia và không có bất kỳ liên quan gì đến tổ chức, thể lệ và giải thưởng của chương trình</p>
                </div>';

        $arrData['rule'] = '<div class="ctn">
                    <p><strong>1. Tên chương trình khuyến mại:</strong> Tích điểm tặng quà</p>
                    <p><strong>2. Thời gian khuyến mại:</strong> Từ ngày 22/05 đến hết ngày 22/08/2018.</p>
                    <p><strong>3. Địa bàn khuyến mại:</strong> Toàn quốc.</p>
                    <p><strong>4. Hình thức khuyến mại:</strong> Tích điểm tặng quà.</p>
                    <p><strong>5. Đối tượng được hưởng khuyến mại</strong></p>
                    <p>- Các thuê bao di động Viettel sử dụng dịch vụ MyClip của Viettel.</p>
                    <p>- Toàn bộ các thuê bao test, các thuê bao nghiệp vụ, thuê bao của cán bộ công nhân viên đang làm
                        việc tại Viettel không được tham gia xét giải của chương trình này.
                    </p>
                    <p><strong>6. Cơ cấu giải thưởng</strong></p>
                    <strong>* Cơ cấu giải thưởng tháng</strong>
                    <img src="http://myclip.vn/images/award-image88.png"/>
                    <p>- Giải thưởng là thẻ cào Viettel, Iphone X không có giá trị quy đổi ra tiền mặt.</p>
                    <p><strong>7. Nội dung chi tiết thể lệ chương trình khuyến mại</strong></p>
                    <p><strong>7.1. Cách thức tham gia chương trình khuyến mại</strong></p>

                    <p> - Trong thời gian khuyến mại, khách hàng sử dụng dịch vụ MyClip (bao gồm cả web/wap/app) bằng
                        cách đăng ký dịch vụ, xem video, bình luận ... trên hệ thống sẽ được tích điểm để đổi quà. Chi
                        tiết như sau
                    </p>
                    <p> (1). Đăng ký dịch vụ MyClip tích điểm nhận thẻ cào: Đăng ký dịch vụ MyClip bằng cách soạn tin XN
                        (gói ngày), XN 30 (gói tháng) gửi 1515 hoặc đăng ký tại http://myclip.vn để tích điểm
                    </p>
                    <p> (2). Share video MyClip trên facebook, bình luận video trên MyClip, like video trên MyClip, theo
                        dõi kênh video trên MyClip tích điểm nhận thẻ cào: Share video MyClip trên facebook, bình luận
                        video trên MyClip, like video trên MyClip, theo dõi kênh video trên MyClip tại http://myclip.vn
                    </p>
                    <p> (3). Upload video trên MyClip tích điểm nhận thẻ cào: Upload video trên MyClip tại
                        http://myclip.vn
                    </p>
                    <p> (4). Tích điểm nhận Iphone X: Xem tại mục 7.3
                    </p>
                    <p> • Viettel sẽ gửi thông báo về mã số thẻ cào tới khách hàng trúng thưởng qua tin nhắn: [TB] Cam
                        on Quy khach da su dung MyClip. Quy khach da trung thuong the cao. Ma the cao < ma the cao >.
                    </p>

                    <p><strong>7.2. Quy định về bằng chứng xác định trúng thưởng</strong></p>
                    <p>- Khách hàng nhận giải thưởng là khách hàng sở hữu thuê bao tham gia đăng ký và tương tác MyClip
                        đáp ứng tiêu chuẩn Ban tổ chức đề ra .</p>
                    <p>- Đối với khách hàng nhận giải thưởng Iphone X, Viettel sẽ thực hiện xác minh số điện thoại của
                        chính chủ thuê bao để thực hiện trao giải thưởng.</p>
                    <p>- Đối với giải thưởng thẻ cào, thực hiện trao giải trao giải theo danh sách khách hàng trúng
                        thưởng trên trang <a href="http://myclip.vn">http://myclip.vn</a></p>

                    <p><strong>7.3. Cách thức thức xác định trúng thưởng</strong></p>
                    <p> Khách hàng tham gia chương trình bằng cách tương tác với mạng xã hội MyClip có cơ hội nhận được
                        giải thưởng. Cách thức tương tác và cơ cấu giải thưởng như sau:
                    </p>
                    <p> (1) 500 khách hàng đăng ký gói tháng MyClip đầu tiên trong tháng nhận thẻ cào 10.000 đồng
                    </p>
                    <p> (2) 2.500 khách hàng đầu tiên đăng ký gói ngày MyClip duy trì dịch vụ 7 ngày (trừ cước 7 lần)
                        nhận thẻ cào 10.000 đồng
                    </p>
                    <p> (3) 1.000 lượt share video MyClip lên facebook đầu tiên trong tháng nhận thẻ cào 10.000 đồng
                    </p>
                    <p> (4) 500 khách hàng có video upload được bình luận nhiều nhất trên MyClip trong tháng nhận thẻ
                        cào 30.000 đồng
                    </p>
                    <p> (5) 500 khách hàng có video upload được like nhiều nhất trên MyClip trong tháng nhận thẻ cào
                        30.000 đồng
                    </p>
                    <p> (6) 500 khách hàng có kênh video upload được theo dõi nhiều nhất trên MyClip trong tháng nhận
                        thẻ cào 30.000 đồng
                    </p>
                    <p> (7) 2 khách hàng có video upload được bình luận nhiều nhất trên MyClip trong tuần nhận thẻ cào
                        20.000 đồng
                    </p>
                    <p> (8) 2 khách hàng có video upload được like nhiều nhất trên MyClip trong tuần nhận thẻ cào 20.000
                        đồng
                    </p>
                    <p> (9) 2 khách hàng có kênh video upload được theo dõi nhiều nhất trên MyClip trong tuần nhận thẻ
                        cào 20.000 đồng
                    </p>
                    <p> (10) 500 khách hàng đầu tiên upload video trên MyClip trong tháng nhận thẻ cào 10.000 đồng
                    </p>
                    <p> (11) Khách hàng Tích điểm cao nhất trong tháng trúng Iphone X: Cơ chế tích điểm như sau
                    </p>
                    <p>- Đăng ký thành công gói tháng cộng 10.000 điểm</p>
                    <p>- Đăng ký thành công gói ngày cộng 10.000 điểm</p>
                    <p>- Share video MyClip trên facebook cộng 1000 điểm</p>
                    <p>- Upload Video trên UClip cộng 10.000 điểm</p>
                    <p>- Nội dung do khách hàng Upload lên MyClip, video đó được</p>
                    <p>+ 1 lượt Xem video trên MyClip cộng 100 điểm</p>
                    <p>+ 1 lượt Like video trên MyClip cộng 100 điểm</p>
                    <p>+ 1 lượt bình luận video trên MyClip cộng 100 điểm</p>
                    <p>+ 1 lượt dislike trên MyClip trừ 20 điểm </p>
                    <p>+ 1 lượt theo dõi kênh trên MyClip được 100 điểm</p>

                    <p>Lưu ý:</p>
                    <p>+ Nhóm giải 4,5,6: Một thuê bao chỉ được nhận 1 trong 3 giải trong 1 tháng.</p>
                    <p>+ Nhóm giải 1,2,3,4,5,6,10,11: Một thuê bao chỉ được nhận 1 giải duy nhất trong 3 tháng</p>
                    <p>+ Nhóm giải 7,8,9: Một thuê bao chỉ được nhận 1 trong 3 giải.1 Thuê bao chỉ được nhận một giải
                        tuần duy nhất trong tháng.</p>
                    <p>+ Nhóm giải thưởng 4,5,6,7,8,9,11: Nếu trong trường hợp khách hàng có số điểm bằng nhau căn cứ
                        vào cơ cấu điểm khách hàng tích lũy được theo thứ tự ưu tiên share, comment, like, xem, theo dõi
                        cao hơn sẽ được giải. Nếu cơ cấu điểm bằng nhau thì căn cứ vào thời gian share video đầu tiên
                        sớm hơn sẽ được giải</p>
                    <p>+ Nhóm giải thưởng 1,2,3,10: sẽ căn cứ thời gian của khách hàng theo mili giây để trao giải.</p>
                    <p>+ Sau khi có điểm tổng kết tháng, điểm tháng sau sẽ được trở lại về 0 để khách hàng tích điểm lại
                        từ đầu. Quy chế cộng điểm các tháng tương tự nhau.</p>

                    <p>+ Thời gian diễn ra giải tuần như sau</p>

                    <p>Tháng thứ nhất: Từ 22/05 đến 21/06</p>
                    <p>Tuần 1: 22/05-28/05</p>
                    <p>Tuần 2: 29/05-04/06</p>
                    <p>Tuần 3: 05/06-11/06</p>
                    <p>Tuần 4: 12/06-18/06</p>
                    <p>Tháng thứ hai: Từ 22/06 đến 21/07</p>
                    <p>Tuần 1: 22/06-28/06</p>
                    <p>Tuần 2: 29/06-5/07</p>
                    <p>Tuần 3: 06/07-12/07</p>
                    <p>Tuần 4: 13/07-19/07</p>
                    <p>Tháng thứ ba: 22/07 đến 22/08</p>
                    <p>Tuần 1: 22/07-28/07</p>
                    <p>Tuần 2: 29/07-04/08</p>
                    <p>Tuần 3: 05/08-11/08</p>
                    <p>Tuần 4: 12/08-18/08</p>

                    <p><strong>• Quy định giới hạn việc cộng điểm như sau</strong></p>
                    <p> + Đăng ký dịch vụ MyClip (10.000 điểm): Đăng ký Gói tháng trong tháng, Đăng ký gói dịch vụ ngày.
                        Điều kiện đăng ký thành công ( Trừ tiền thành công), Chỉ tính 1 lần cho một thuê bao, Chỉ tính
                        cho thuê bao mới đăng ký, hủy đi đăng ký lại không tính, Đối với gói cước ngày duy trì 7 ngày
                        (charge cước 7 ngày thành công)
                    </p>
                    <p> +Upload video trên MyClip (10.000 điểm): Upload được tính trên 1 thuê bao như sau: Áp dụng cho
                        người dùng Upload (Không ứng dụng cho đối tác cung cấp nội dung), Kênh được tạo mới, Upload lên
                        được duyệt theo quy định, Chỉ tính tối đa cộng điểm 3 video/ngày
                    </p>
                    <p> + Share video trên facebook (1000 điểm): Share được tính trên 1 thuê bao như sau: Người dùng sử
                        dụng 3G/4G viettel, Điều kiện thuê bao đăng ký MyClip, Chia sẻ thành công được MyClip ghi nhận
                        trên trạng thái công khai, 1 chia sẻ chỉ được tính cho 1 video, 1 lần, Tính tối đa chia sẻ 1
                        người 3 video/ một ngày
                    </p>
                    <p> + Xem video trên MyClip (100 điểm): XEM (View) được tính trên 1 thuê bao như sau: Người dùng sử
                        dụng 3G/4G viettel, Điều kiện thuê bao đăng ký MyClip, Xem trong ít nhất 30s, Tính 1 lần cho 1
                        video, Tính tối đa 10 video trong một ngày
                    </p>
                    <p> + Thích (Like) video trên MyClip (100 điểm): Like được tính trên 1 thuê bao như sau: Người dùng
                        sử dụng 3G/4G viettel, Thuê bao đăng ký dịch vụ MyClip, Video like là video đã được xem > 30s,
                        Tính 1 lần cho 1 video, Tính tối đa 10 video trong một ngày, Chỉ được tính hoặc like và hoặc
                        dislike
                    </p>
                    <p> + Bình luận (100 điểm): Bình luận được tính trên 1 thuê bao như sau: Người dùng sử dụng 3G/4G
                        viettel, Thuê bao đang ký dịch vụ MyClip, Video bình luận là video đã được xem > 30s, Tính 1 lần
                        cho 1 video, Tính 10 tối đa 10 bình luận trong một ngày
                    </p>
                    <p> + Theo dõi kênh trên MyClip (100 điểm): Follow được tính như sau: Người dùng sử dụng 3G/4G
                        viettel, Thuê bao đăng ký dịch vụ MyClip, Người dùng xem tối thiểu 3 video của kênh, video đã
                        được xem > 30s, Chỉ được tính tối đa 05 theo dõi trong ngày, Một thuê bao chỉ tính tối đa 50
                        theo dõi kênh trong tháng
                    </p>
                    <p> + Không thích video trên MyClip (dislike) DisLike được tính trên 1 thuê bao như sau: Người dùng
                        sử dụng 3G/4G viettel, Thuê bao đăng ký dịch vụ MyClip, Video dislike là video đã được xem >
                        30s, Tính 1 lần cho 1 video, Tính tối đa 5 video trong một ngày, Chỉ được tính hoặc like và hoặc
                        dislike
                    </p>


                    <p><strong>7.4. Quy trình, cách thức, thủ tục nhận thưởng:</strong></p>
                    <p>- Thời gian trao thưởng: Không quá 30 ngày từ ngày kết thúc chương trình</p>
                    <p>
                        - Đối với giải thưởng thẻ cào, Viettel tiến hành gửi mã thẻ cào cho khách hàng thông qua hệ
                        thống SMS sau khi công bố danh sách khách hàng trúng thưởng hợp lệ của tháng.
                    </p>
                    <p>
                    - Đối với giải thường Iphone X, ngay sau khi xếp hạng điểm tích lũy khách hàng và chọn được khách
                    hàng trúng thưởng, Viettel sẽ thông báo trực tiếp cho khách hàng trúng thưởng và đăng danh sách
                    khách hàng trúng thưởng trên website/wapsite: http://myclip.vn ngay khi có kết quả xác minh khách
                    hàng của Trung tâm Chăm sóc khách hàng gửi lại.
                    </p>
                    <p>
                        - Chi nhánh Viettel các tỉnh/Thành phố thực hiện trao thưởng khách hàng sau khi có Quyết định
                        trao thưởng của Ban Tổng Giám đốc.
                    </p>
                    <p>
                        • Chi nhánh Tỉnh/Thành phố tự liên hệ với khách hàng trúng thưởng, hướng dẫn khách hàng các thủ
                        tục nhận giải thưởng. Thời gian trao giải thưởng cho khách hàng trúng giải: không quá 30 ngày kể
                        từ ngày kết thúc chương trình.
                    </p>
                    <p>
                        • Chi nhánh tỉnh/TP tổ chức trao giải, chụp ảnh khi trao giải và gửi ảnh về Phòng Truyền thông
                        để làm tư liệu truyền thông. Địa điểm trao giải thưởng được thực hiện tại văn phòng đại diện của
                        các Chi nhánh Tỉnh/Thành phố của Tập đoàn Công nghiệp - Viễn thông Quân đội.
                    </p>
                    <p>
                        - Các thủ tục cần có để nhận giải:
                    </p>
                    <p>
                        • Trường hợp người trúng thưởng là chính chủ thuê bao: Khi nhận giải thưởng chủ thuê bao trúng
                        thưởng phải xuất trình CMND/ Hộ chiếu còn hiệu lực theo quy định (Giấy CMND không quá 15 năm kể
                        từ ngày được cấp/ Hộ chiếu phải còn hiệu lực trong vòng 06 tháng tính đến thời điểm nhận thưởng
                        hoặc các giấy tờ thay thế khác nhưng phải có ảnh và có xác nhận của chính quyền địa phương trong
                        vòng 06 tháng tính đến thời điểm nhận thưởng).
                    </p>
                    <p>
                        • Nếu khách hàng ủy quyền cho người khác đi nhận giải cần mang theo: Chứng minh nhân dân bản
                        photo và bản gốc của chủ thuê bao, Chứng minh nhân dân bản photo và bản gốc của người được ủy
                        quyền, Giấy ủy quyền đi nhận giải thưởng của người trúng thưởng.
                    </p>
                    <p>
                        • Trường hợp người trúng thưởng không phải là chính chủ thuê bao: Khi nhận giải thưởng, người
                        trúng thưởng phải xuất trình chứng minh thư nhân dân, cung cấp ít nhất 05 số điện thoại thường
                        xuyên liên lạc trong vòng 06 tháng gần nhất.
                    </p>

                    <p><strong>8. Trách nhiệm thông báo:</strong></p>
                    <p>
                        - Tổng Công ty Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp - Viễn thông Quân đội có
                        trách nhiệm thông báo đầy đủ chi tiết nội dung của thể lệ chương trình khuyến mại, trị giá giải
                        thưởng, tỷ lệ trúng thưởng tại các điểm bán hàng và thông báo đầy đủ danh sách khách hàng trúng
                        thưởng trên ít nhất 01 phương tiện thông tin đại chúng sau khi chương trình khuyến mại kết thúc.
                    </p>
                    <p><strong>9. Các quy định khác:</strong></p>
                    <p>
                        - Khách hàng trúng thưởng phải chịu chi phí đi lại, ăn ở cho việc nhận thưởng của mình.
                    </p>
                    <p>
                        - Khách hàng trúng thưởng phải nộp thuế thu nhập không thường xuyên (nếu có) theo quy định của
                        pháp luật hiện hành. Nếu được sự chấp thuận của khách hàng trúng thưởng Tổng Công ty Viễn Thông
                        Viettel - Chi nhánh Tập đoàn Công nghiệp - Viễn thông Quân đội sẽ khấu trừ khoản thuế thu nhập
                        không thường xuyên (nếu có) theo quy định của pháp luật trên trị giá giải thưởng mà khách hàng
                        đã trúng và thay mặt khách hàng nộp theo quy định.
                    </p>
                    <p>
                        - Khách hàng tham dự chương trình khuyến mại này vẫn được hưởng quyền lợi từ các chương trình
                        khuyến mại khác của Tổng Công ty Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp - Viễn
                        thông Quân đội (nếu đủ điều kiện).
                    </p>
                    <p>
                        - Tổng Công ty Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp - Viễn thông Quân đội hoàn
                        toàn chịu trách nhiệm trong việc quản lý tính chính xác của bằng chứng xác định trúng thưởng và
                        đưa bằng chứng xác định trúng thưởng vào lưu thông trong chương trình khuyến mại. Trường hợp
                        bằng chứng xác định trúng thưởng của Tổng Công ty phát hành có sai sót gây hiểu nhầm cho khách
                        hàng trong việc trúng thưởng, Tổng Công ty Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp -
                        Viễn thông Quân đội có trách nhiệm trao các giải thưởng này cho khách hàng trúng thưởng.
                    </p>
                    <p>
                        - Việc tổ chức chương trình khuyến mại phải đảm bảo tính công bằng, minh bạch và khách quan.
                    </p>
                    <p>
                        - Tổng Công ty Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp Viễn thông Quân đội sẽ sử
                        dụng tên và hình ảnh của khách hàng trúng thưởng cho mục đích quảng cáo thương mại nếu được
                        khách hàng trúng thưởng chấp thuận.
                    </p>
                    <p>
                        - Trong trường hợp xảy ra tranh chấp liên quan đến chương trình khuyến mại này, Tổng Công ty
                        Viễn Thông Viettel - Chi nhánh Tập đoàn Công nghiệp Viễn thông Quân đội có trách nhiệm trực tiếp
                        giải quyết, nếu không thỏa thuận được tranh chấp sẽ được xử lý theo quy định của pháp luật.
                    </p>
                    <p>
                        - Ban Tổ chức được toàn quyền sử dụng video clip dự thi, cũng như thông tin cá nhân (tên tuổi,
                        hình ảnh) của người dự thi để quảng bá cho cuộc thi trên tất cả phương tiện thông tin đại chúng
                        và mạng xã hội trong và sau khi cuộc thi mà không phải trả bất kỳ khoản phí nào.
                    </p>
                    <p>
                        - Ban Tổ chức không có quyền sở hữu video clip đoạt giải (bán video clip cho bên thứ ba).
                        Tác giả chịu toàn bộ trách nhiệm về pháp lý của nội dung, tư liệu (nhạc, hình ảnh, sự
                        kiện) trong video clip trước pháp luật. Ban Tổ chức không chịu trách nhiệm về việc tranh chấp
                        tác quyền bản quyền video clip.
                    </p>
                    <p>
                        - Những người thắng cuộc có trách nhiệm và nghĩa vụ thực hiện theo đúng các quy định của Ban tổ
                        chức và những quy định của Nhà nước về các khoản thuế, phí và lệ phí khác (nếu có).
                    </p>
                    <p>
                        - Mọi khiếu nại liên quan đến kết quả chỉ có hiệu lực trong vòng 48 giờ kể từ khi công bố kết
                        quả công khai trên trang web và fanpage cuộc thi. Sau thời gian quy định trên, Ban tổ chức sẽ
                        không tiến hành giải quyết bất cứ khiếu nại nào.
                    </p>
                    <p>
                        - Video clip vi phạm nội quy của ban tổ chức hay thuần phong mỹ tục Việt Nam, pháp luật Việt
                        Nam, mang tính chất kích động, thể hiện ý kiến chủ quan đều sẽ bị loại bỏ mà không cần
                        thông báo trước. Nếu có bằng chứng hiển nhiên người tham gia gian lận và sao chép tác phẩm thì
                        kết quả của người thắng cuộc sẽ bị hủy bỏ.
                    </p>
                    <p>
                        - Đối với các tác giả dùng các công cụ hack view, chạy view ảo,… hoặc Ban tổ chức phát hiện hành
                        vi bất thường, Ban tổ chức có quyền từ chối trao giải cho cá nhân/nhóm này.
                    </p>
                    <p>
                        - Nếu có một vấn đề phát sinh trước, trong hoặc sau chương trình, mà vấn đề này nằm ngoài quy
                        định đang có, Ban Tổ chức sẽ giữ toàn quyền thảo luận để đưa ra quyết định xử lý vấn đề.
                    </p>
                    <p>Chương trình do Tổng Công ty Viễn thông Viettel tổ chức và chịu trách nhiệm trước pháp luật. Apple không trực tiếp hoặc gián tiếp tham gia và không có bất kỳ liên quan gì đến tổ chức, thể lệ và giải thưởng của chương trình</p>
                </div>';

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => 'Thành công',
            'data' => $arrData,

        ];

    }
    public function actionGetPoints()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $dataResponse = MongoDBModel::getReportByUserId($this->userId, $this->msisdn);
        $arrPoint = [];
        $arrPoint['register_point'] = 0;
        $arrPoint['upload_point'] = 0;
        $arrPoint['share_point'] = 0;
        $arrPoint['watch_point'] = 0;
        $arrPoint['like_point'] = 0;
        $arrPoint['comment_point'] = 0;
        $arrPoint['follow_point'] = 0;
        $arrPoint['dislike_point'] = 0;
        $arrPoint['like_fanpage_point'] = 0;
        $arrPoint['total_point'] = 0;

        foreach ($dataResponse as $item) {

            $arrPoint['register_point'] = $arrPoint['register_point'] + $item['register_point'];
            $arrPoint['upload_point'] = $arrPoint['upload_point'] + $item['upload_point'];
            $arrPoint['share_point'] = $arrPoint['share_point'] + $item['share_point'];
            $arrPoint['watch_point'] = $arrPoint['watch_point'] + $item['watch_point'];
            $arrPoint['like_point'] = $arrPoint['like_point'] + $item['like_point'];
            $arrPoint['comment_point'] = $arrPoint['comment_point'] + $item['comment_point'];
            $arrPoint['follow_point'] = $arrPoint['follow_point'] + $item['follow_point'];
            $arrPoint['dislike_point'] = $arrPoint['dislike_point'] + $item['dislike_point'];
            $arrPoint['like_fanpage_point'] = $arrPoint['like_fanpage_point'] + $item['like_fanpage_point'];

            $arrPoint['total_point'] = $arrPoint['total_point'] + $item['total_point'];

        }

        $arrPoint['report_at'] = ($arrPoint['report_at']) ? $arrPoint['report_at'] : date("Y-m-d H:i:s");
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => \Yii::t('wap', 'Thành công'),
            'data' => $arrPoint
        ];
    }

    public function actionGetReportByRange()
    {
        $fromDate = \Yii::$app->request->get("from_date");
        $toDate = \Yii::$app->request->get("to_date");
        $currentPage = \Yii::$app->request->get("page", 1);
        $awardTypes = \Yii::$app->params['event.award.type.week'];
        $limitAwardPerPage = \Yii::$app->params['event.limit.award.per.page'];

        $fromDate2 = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
        $toDate2 = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
         				
        $arrReport = [];
        $dataR = MongoDBModel::getAwardReport($fromDate2, $toDate2, $awardTypes);
        //Remove Award Black list
        $dataReponse = [];
        foreach ($dataR as $item) {
            if (!in_array($item['from_date'], \Yii::$app->params['award.blacklist.date'])) {
                $dataReponse[] = $item;
            }
        }

        $totalItem = count($dataReponse);

        $dataReponse = array_slice($dataReponse, ($currentPage - 1) * $limitAwardPerPage, $limitAwardPerPage);

        $cc = 0;
        foreach ($dataReponse as $item) {
            $arrReport[$cc]["user_id"] = $item["user_id"];
            $arrReport[$cc]["msisdn"] = substr_replace(Utils::getMobileNumber($item["msisdn"], Utils::MOBILE_SIMPLE), "xxxx", -7, 4);;
            $arrReport[$cc]["award_type"] = $item["award_type"];
            $arrReport[$cc]["award_name"] = VtAwardBase::getByType($item["award_type"]);
            $arrReport[$cc]["point"] = $item["point"];
            $cc++;
        }

        $totalPage = ceil($totalItem / $limitAwardPerPage);

        $startDate = VtConfigBase::getConfig("START_DATE_EVENT");

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => \Yii::t('wap', 'Thành công'),
            'data' => $arrReport,
            'totalPage' => $totalPage,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'currentPage' => $currentPage,
            'startDate' => $startDate
        ];
    }

    public function actionGetReportByMonth()
    {
        $monthReport = \Yii::$app->request->get("month_report");//2018-04
        $awardTypes = \Yii::$app->params['event.award.type.month'];
        $currentPage = \Yii::$app->request->get("page", 1);

        $arrReportMonthRange = \Yii::$app->params['award.month.report.range'];
        $strRange = explode(",", $arrReportMonthRange[$monthReport]);

        if (count($strRange) == 2) {
            $fromDate = $strRange[0] ;
            $toDate = $strRange[1];
        } else {
            $fromDate = $monthReport . "-01";
            $toDate = $monthReport . "-31";
        }

        $limitAwardPerPage = \Yii::$app->params['event.limit.award.per.page'];

        $fromDate2 = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
        $toDate2 = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));

        $arrReport = [];
        $dataReponse = MongoDBModel::getAwardReport($fromDate2, $toDate2, $awardTypes);
        $totalItem = count($dataReponse);

        $dataReponse = array_slice($dataReponse, ($currentPage - 1) * $limitAwardPerPage, $limitAwardPerPage);

        $cc = 0;
        foreach ($dataReponse as $item) {
            $arrReport[$cc]["user_id"] = $item["user_id"];
            $arrReport[$cc]["msisdn"] = substr_replace(Utils::getMobileNumber($item["msisdn"], Utils::MOBILE_SIMPLE), "xxxx", -7, 4);;
            $arrReport[$cc]["award_type"] = $item["award_type"];
            $arrReport[$cc]["award_name"] = VtAwardBase::getByType($item["award_type"]);
            $arrReport[$cc]["point"] = $item["point"];
            $cc++;
        }
        $totalPage = ceil($totalItem / $limitAwardPerPage);
        $reportRange = \Yii::$app->params['award.month.report'];

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => \Yii::t('wap', 'Thành công'),
            'data' => $arrReport,
            'totalPage' => $totalPage,
            'monthReport' => $monthReport,
            'currentPage' => $currentPage,
            'reportRange' => $reportRange
        ];
    }
}
