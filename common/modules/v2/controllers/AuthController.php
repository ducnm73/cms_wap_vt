<?php

namespace common\modules\v2\controllers;

use common\libs\S3Service;
use common\models\VtSmartTvTokenBase;
use common\models\VtCommentBase;
use common\models\VtSubBase;
use common\models\VtUserTokenBase;
use Yii;
use common\helpers\Des;
use common\helpers\Utils;
use common\libs\VtHelper;
use common\models\VtConfigBase;
use common\models\VtUserBase;
use common\models\VtAccountInfomationBase;
use common\modules\v2\libs\ResponseCode;
use common\helpers\MobiTVRedisCache;
use yii\httpclient\Client;
use common\models\VtChannelBase;

class AuthController extends \common\modules\v1\controllers\AuthController
{

    /**
     * Ham xac thuc, cung cap 4 loai grant_type: auto_login, login,login_social, refresh_token
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionAuthorize()
    {
        $grantType = trim(Yii::$app->request->post('grant_type', 'auto_login'));

        $osType = trim(Yii::$app->request->post('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->post('os_version_code', ''));

        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';
        $userAgentEncrypted = md5(Yii::$app->params['userAgent.secretCode'] . $userAgent);
        $accessTokenExpiredTime = time() + Yii::$app->params['accessToken.timeout'];
        $refreshTokenExpiredTime = time() + Yii::$app->params['refreshToken.timeout'];

        Yii::info("AUTH | REQUEST =" . $grantType);
        $isAutoLogin = false;
        $user = null;
        $msisdn = '';
        $needShowMapAccount = 0;
        $isShowSuggest = 0;
        $isForceUpdateAPP = 0;
        $isUpdateAPP = 0;

        $loginVia = "NA";


        $isShowUpdateAPP = VtConfigBase::getConfig("IS_SHOW_UPDATE_APP_" . strtoupper($osType), 0);

        if ($grantType === 'auto_login') {
            $loginVia = "DETECT";
            //@todo: dang hardcode, bo di khi trien khai that
            $msisdn = VtHelper::getMsisdn();

            if (!empty($msisdn)) {
                $isAutoLogin = true;
                $user = VtUserBase::getByMsisdn($msisdn);

                if (!$user) {
                    $user = new VtUserBase();
                    $user->insertAutoLogin($msisdn);
                }

                Yii::info("AUTH | AUTO LOGIN GET ID =" . $user->id . "| USER NAME=" . $user->full_name);
            }
        } else if ($grantType === 'login') {
            $loginVia = "LOGIN";
            $username = trim(Yii::$app->request->post('username', ''));
            $password = Yii::$app->request->post('password', '');
            $captcha = trim(Yii::$app->request->post('captcha', ''));
            $captchaToken = 'no-cap-t-cha';
            $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
            if (preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches)) {
                $captchaToken = md5(VtHelper::getAgentIp() . $matches[1]);
            }

            if (empty($this->accessToken)) {
                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap','Access Token không được để trống')
                ];
            }
            if (empty($username) || empty($password)) {
                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap','Quý khách vui lòng nhập Số thuê bao và Mật khẩu')
                ];
            }

            $msisdn = Utils::getMobileNumber($username, Utils::MOBILE_GLOBAL);
            $redisCache = Yii::$app->cache;
            $ip = md5(VtHelper::getAgentIp());

            $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
            $ipUser = md5(VtHelper::getAgentIp() . $username);
            $countLock = intval($redisCache->get('count_lock_' . $ipUser));

            //-- Kiem tra khoa tai khoan trong 10phut
            $isLockIPUser = $redisCache->get('lock_login_' . $ipUser);

            $configCaptchaShow = Yii::$app->params['login.captcha.show.count'];
            $configCountLock = Yii::$app->params['login.lock.count'];

            if ($countLock >= $configCountLock || $isLockIPUser) {
                if ($isLockIPUser) {
                    $redisCache->set('count_lock_' . $ipUser, 0, MobiTVRedisCache::CACHE_1DAY);
                } else {
                    $redisCache->set('lock_login_' . $ipUser, 1, MobiTVRedisCache::CACHE_10MINUTE);
                    // - Xoa bo dem lock
                    $redisCache->delete('count_lock_' . $ipUser);
                    $redisCache->delete('count_login_fail_' . $ip);
                }
                return [
                    'responseCode' => ResponseCode::LOCK_USER,
                    'message' => Yii::t('wap','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!')
                ];
            }

            if ($countLoginFail >= $configCaptchaShow) {
                if (empty($captcha)) {
                    return [
                        'responseCode' => ResponseCode::CAPTCHA_EMPTY,
                        'message' => Yii::t('wap','Quý khách vui lòng nhập mã xác nhận')
                    ];
                } else {
                    $serverCaptcha = Yii::$app->cache->get("captcha_" . $captchaToken);
                    // refreshCaptcha
                    $randomCode = Utils::generateVerifyCode();
                    Yii::$app->cache->set("captcha_" . $captchaToken, $randomCode, MobiTVRedisCache::CACHE_15MINUTE);// luu captcha trong 15P

                    if (empty($serverCaptcha) || $serverCaptcha !== $captcha) {
                        return [
                            'responseCode' => ResponseCode::CAPTCHA_INVALID,
                            'message' => Yii::t('wap', 'Mã xác nhận không hợp lệ'),
                        ];
                    }
                }
            }

            $user = VtUserBase::getByMsisdnOrEmail($msisdn, $username);

            //@todo: kiem tra xu ly not + bo sung captcha
            if (!empty($user) && $user->status == VtUserBase::ACTIVE && $user->checkPassword($password)) {
                $msisdn = $user->msisdn;
                if (!$user->changed_password) {
                    $needChangePassword = true;
                }

                // - Dang nhap thanh cong thi xoa lock
                $redisCache->delete('count_lock_' . $ipUser);
                $redisCache->delete('count_login_fail_' . $ip);
            } else {
                // Neu empty user thi chi tang lock login , khong khoa user
                if (empty($user)) {
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                } else {
                    $redisCache->set('count_lock_' . $ipUser, $countLock + 1, MobiTVRedisCache::CACHE_1DAY);
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                }

                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap','Thông tin đăng nhập không hợp lệ'),
                    'captcha' => ($countLoginFail >= ($configCaptchaShow - 1)) ? 1 : 0
                ];
            }

        } else if ($grantType === 'login-social') {
            $socialId = trim(Yii::$app->request->post('social_id', ''));
            $accessToken = trim(Yii::$app->request->post('access_token', ''));
            $needShowMapAccount = 0;
            switch (strtolower(trim($socialId))) {
                case "facebook":
                    $loginVia = "FACEBOOK";
                    try {
                        $response = Utils::getUrlContent(Yii::$app->params['getprofile.facebook'] . $accessToken . '&fields=name,email,first_name,last_name,picture.type(large),cover');
                        $jsonResponse = json_decode($response, true);

                        if ($jsonResponse['id'] && $jsonResponse['name']) {

                            $user = VtUserBase::getByOauthId($jsonResponse['id']);
                            if (!$user) {

                                $imageUrl = str_replace('sz=50', 'sz=200', $jsonResponse['picture']['data']['url']);

                                $bucketName = '';
                                $newPath = '';

                                $client = new Client([
                                    'transport' => 'yii\httpclient\CurlTransport'
                                ]);

                                $response = $client->createRequest()
                                    ->setMethod('get')
                                    ->setUrl($imageUrl)
                                    ->setOptions([
                                        CURLOPT_CONNECTTIMEOUT => 5, // connection timeout
                                        CURLOPT_TIMEOUT => 30, // data receiving timeout
                                    ])
                                    ->send();
                                if ($response->isOk) {

                                    $ext = 'jpg';
                                    $tmpFile = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                                    $fp = fopen($tmpFile, 'w');
                                    fwrite($fp, $response->content);
                                    fclose($fp);

                                    $bucketName = Yii::$app->params['s3']['static.bucket'];
                                    $newPath = Utils::generatePath($ext);

                                    S3Service::putObject($bucketName, $tmpFile, $newPath);
                                    VtHelper::generateAllThumb($bucketName, $tmpFile, $newPath);
                                    unlink($tmpFile);
                                }

                                $user = new VtUserBase();
                                $user->insertUserSocial($jsonResponse['id'], $jsonResponse['name'], $jsonResponse['email'], $bucketName, $newPath, "FACEBOOK");
                                $user->full_name = $jsonResponse['name'];
                                $needShowMapAccount = 1;
                            } else {
                                $msisdn = $user->msisdn;
                            }
                        } else {
                            return [
                                'responseCode' => ResponseCode::FORBIDDEN,
                                'message' => Yii::t('wap','Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                            ];
                        }

                    } catch (\Exception $ex) {
                        return [
                            'responseCode' => ResponseCode::FORBIDDEN,
                            'message' => Yii::t('wap','Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                        ];
                    }

                    break;
                case "google":
                    $loginVia = "GOOGLE";
                    try {
                        $response = Utils::getUrlContent(Yii::$app->params['getprofile.google'] . $accessToken);
                        $jsonResponse = json_decode($response, true);
                        if ($jsonResponse['sub']) {
                            $user = VtUserBase::getByOauthId($jsonResponse['sub']);
                            if (!$user) {

                                if (isset($jsonResponse['picture'])) {
                                    $imageUrl = $jsonResponse['picture'];
                                } else {
                                    $imageUrl = $jsonResponse['image']['url'];
                                }

                                $bucketName = '';
                                $newPath = '';

                                $client = new Client([
                                    'transport' => 'yii\httpclient\CurlTransport'
                                ]);

                                $response = $client->createRequest()
                                    ->setMethod('get')
                                    ->setUrl($imageUrl)
                                    ->setOptions([
                                        CURLOPT_CONNECTTIMEOUT => 5, // connection timeout
                                        CURLOPT_TIMEOUT => 30, // data receiving timeout
                                    ])
                                    ->send();
                                if ($response->isOk) {

                                    $ext = 'jpg';
                                    $tmpFile = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                                    $fp = fopen($tmpFile, 'w');
                                    fwrite($fp, $response->content);
                                    fclose($fp);

                                    $bucketName = Yii::$app->params['s3']['static.bucket'];
                                    $newPath = Utils::generatePath($ext);

                                    S3Service::putObject($bucketName, $tmpFile, $newPath);
                                    VtHelper::generateAllThumb($bucketName, $tmpFile, $newPath);
                                    unlink($tmpFile);
                                }


                                $user = new VtUserBase();
                                $nameS = ($jsonResponse['name']) ? $jsonResponse['name'] : $jsonResponse['email'];
                                $user->insertUserSocial($jsonResponse['sub'], $nameS, $jsonResponse['email'], $bucketName, $newPath, "GOOGLE");
                                $user->full_name = $nameS;
                                $needShowMapAccount = 1;
                            } else {
                                $msisdn = $user->msisdn;

                            }
                        } else {
                            return [
                                'responseCode' => ResponseCode::FORBIDDEN,
                                'message' => Yii::t('wap','Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                            ];
                        }

                    } catch (\Exception $ex) {
                        return [
                            'responseCode' => ResponseCode::FORBIDDEN,
                            'message' => Yii::t('wap','Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                        ];
                    }
                    break;
            }


        } else if ($grantType === 'refresh_token') {
            $loginVia = "REFRESH";
            $refreshToken = trim(Yii::$app->request->post('refresh_token', ''));

            $userToken = VtUserTokenBase::getByToken($refreshToken);
            if ($userToken) {
                if ($userAgent != $userToken['user_agent'] || strtotime($userToken['token_expired_time']) < strtotime('now')) {
                    Yii::info("AUTH| osType=" . $osType . "|osVersionCode=" . $osVersionCode . "|configOsVersionCode=" . VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0));
                    //Kiem tra xem co can update APP hay khong
                    if ($osType && $osVersionCode) {
                        $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                        $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                        $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
                    }
                    return [
                        'responseCode' => ResponseCode::REFRESH_FORBIDDEN,
                        'message' => Yii::t('wap','Xác thực không thành công'),
                        'data' => [
                            'isForceUpdateAPP' => $isForceUpdateAPP,
                            'isUpdateAPP' => ($isShowUpdateAPP) ? $isUpdateAPP : 0
                        ]
                    ];
                } else {
                    $msisdn = $userToken->msisdn;
                    $user = VtUserBase::getById($userToken->user_id);
                }
            } else {
                return [
                    'responseCode' => ResponseCode::REFRESH_FORBIDDEN,
                    'message' => Yii::t('wap','Tài khoản của quý khách đang được sử dụng trên thiết bị khác. Vui lòng kiểm tra lại'),
                    'data' => [
                        'isForceUpdateAPP' => $isForceUpdateAPP,
                        'isUpdateAPP' => ($isShowUpdateAPP) ? $isUpdateAPP : 0
                    ]
                ];
            }
        }elseif($grantType=='smart_tv'){

            $smartTvToken = Yii::$app->request->post('smart_tv_token','');

            if(!$smartTvToken){
                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap','Xác thực không thành công, yêu cầu mã token'),
                ];
            }
            // KIEM TRA MAP TAI KHOAN
            $objTokenSmartTV = VtSmartTvTokenBase::checkTokenLogin($smartTvToken);
            if($objTokenSmartTV){
                if($objTokenSmartTV['user_id']){
                    $user =  VtUserBase::getActiveUserById($objTokenSmartTV['user_id']);
                    $msisdn = $user->msisdn;
                }else{
                    $user = VtUserBase::getByMsisdn($objTokenSmartTV['msisdn']);
                    $msisdn = $objTokenSmartTV['msisdn'];
                }
                VtSmartTvTokenBase::updateAll(["status"=>2], 'id = :id', [ 'id'=>$objTokenSmartTV['id'] ]);

            }else{
                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap','Xác thực không thành công, vui lòng xem lại quá trình đăng nhập'),
                ];
            }
        }

        $firstTimeLoginAPP = 0;

        if (Utils::isValidMsisdn($msisdn) || !empty($user) || isset($userToken)) {
            $userId = ($user) ? $user->id : null;
            $refreshToken = Utils::generateGuid();
            $refeshTokenExpiredTimeStamp = date("Y-m-d H:i:s", $refreshTokenExpiredTime);

            $ip = VtHelper::getAgentIp();

            if (isset($userToken) && $userToken) {
                $userToken->token = $refreshToken;
                $userToken->ip = $ip;
                $userToken->token_expired_time = $refeshTokenExpiredTimeStamp;
                $userToken->last_login = date('Y-m-d H:i:s', strtotime('now'));
                $userToken->user_agent = $userAgent;
                $userToken->save(false);

                $secretString = "$accessTokenExpiredTime&$userToken->user_id&$userToken->msisdn&$userAgentEncrypted";

            } elseif ($user) {
                $userToken = VtUserTokenBase::getByUserId($user->id);

                if ($userToken) {
                    $userToken->msisdn = $msisdn;
                    $userToken->token = $refreshToken;
                    $userToken->ip = $ip;
                    $userToken->token_expired_time = $refeshTokenExpiredTimeStamp;
                    $userToken->last_login = date('Y-m-d H:i:s', strtotime('now'));
                    $userToken->last_login_type = $isAutoLogin;
                    $userToken->user_agent = $userAgent;
                    $userToken->save(false);
                } else {
                    $userToken = new VtUserTokenBase();
                    $userToken->insertUserToken($refreshToken, $ip, $user->id, $msisdn, $refeshTokenExpiredTimeStamp, $isAutoLogin, $userAgent);
                    //First time login APP
                    $firstTimeLoginAPP = 1;
                }

                $secretString = "$accessTokenExpiredTime&$user->id&$msisdn&$userAgentEncrypted";

            } else {
                $userToken = VtUserTokenBase::getByMsisdn($msisdn);

                if ($userToken) {
                    $userToken->msisdn = $msisdn;
                    $userToken->token = $refreshToken;
                    $userToken->ip = $ip;
                    $userToken->token_expired_time = $refeshTokenExpiredTimeStamp;
                    $userToken->last_login = date('Y-m-d H:i:s', strtotime('now'));
                    $userToken->last_login_type = $isAutoLogin;
                    $userToken->user_agent = $userAgent;
                    $userToken->save(false);
                } else {
                    $userToken = new VtUserTokenBase();
                    $userToken->insertUserToken($refreshToken, $ip, null, $msisdn, $refeshTokenExpiredTimeStamp, $isAutoLogin, $userAgent);
                    //First time login APP
                    $firstTimeLoginAPP = 1;
                }
                $secretString = "$accessTokenExpiredTime&0&$msisdn&$userAgentEncrypted";
            }
            //Kiem tra dieu kien hien thi suggest Topic
            if ($user && $user->is_show_suggest == 0) {
                $isShowSuggest = 1;
                $user->is_show_suggest = 1;
                $user->save(false);
            } elseif ($msisdn && $isAutoLogin && !$user) {

                //Neu la 3G ma chua co key trong redis thi hien thi suggest Topic
                $redisCache = Yii::$app->getCache();
                $objSuggest = $redisCache->get("suggest_" . Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL));
                if (!$objSuggest) {
                    $isShowSuggest = 1;
                    $redisCache->set("suggest_" . Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL), 1);
                }
            }

            Yii::info("AUTH| osType=" . $osType . "|osVersionCode=" . $osVersionCode . "|configOsVersionCode=" . VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0));
            //Kiem tra xem co can update APP hay khong
            if ($osType && $osVersionCode) {
                $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
                //Kiem tra xem co can hidden noi dung tinh phi IOS
                if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                    $secretString = $secretString . "&1";
                } else {
                    //Khong can hidden noi dung tinh phi duyet APP IOS
                    $secretString = $secretString . "&0";
                }
            } else {
                //Khong can hidden noi dung tinh phi duyet APP IOS
                $secretString = $secretString . "&0";
            }
            // Them tham so google, facebook neu dang nhap qua MXH
            $secretString = $secretString . "&" . $loginVia;

            //Ma hoa accessToken
            $accessToken = Des::RSAEncrypt($secretString);

            //Neu lan dau tien dang nhap APP, kiem tra xem KH co dc huong KM hay khong?
            $isShowPromotionAPP = 0;

            if ($firstTimeLoginAPP && $this->msisdn) {
                //Kiem tra xem khach hang da dang ky goi cuoc hay chua? Neu da tung dang ky goi cuoc thi khong duoc KM
                $isShowPromotionAPP = VtSubBase::checkPromotionFirstTimeUsageAPP($this->msisdn);
                $configPromotionInstallAPP = VtConfigBase::getConfig('is.on.promotion.intall.app', 0);
                $isShowPromotionAPP = $isShowPromotionAPP & $configPromotionInstallAPP;
            }
            // dem so luong thong bao comment
            $channels = VtChannelBase::getCountChannelActiveById($user->id);
            if($channels == 0){
                $statusUpload = 5;
            }
            $accountInfo = VtAccountInfomationBase::getByUserId($user->id);
            if($accountInfo->status){
                $statusUpload =  $accountInfo->status ;
                if($channels == 0 && $accountInfo->status == 2){
                    $statusUpload = 5;
                }
            }else{
                $statusUpload = 0;
            }



            $count_comment = VtCommentBase::getCountByUserId($user->id,0);
            $result = [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'accessToken' => $accessToken,
                    'refressToken' => $refreshToken,
                    'fullname' => (empty($user) || !$user->full_name) ? (string)Utils::hideMsisdnLast($msisdn) : $user->full_name,
                    'description' => (empty($user) || !$user->description) ? '' : $user->description,
                    'avatarImage' => (!empty($user)) ? VtHelper::getThumbUrl($user['bucket'], $user['path'], VtHelper::SIZE_AVATAR) : '',
                    'coverImage' => (!empty($user)) ? VtHelper::getThumbUrl($user['channel_bucket'], $user['channel_path'], VtHelper::SIZE_COVER) : '',
                    'msisdn' => (string)Utils::hideMsisdnLast($msisdn),
                    'userId' => ($user->id) ? $user->id : 0,
                    'expiredTime' => Yii::$app->params['accessToken.timeout'],
                    'isShowSuggestTopic' => intval($isShowSuggest),
                    'needShowMapAccount' => $needShowMapAccount,
                    'isForceUpdateAPP' => $isForceUpdateAPP,
                    'isUpdateAPP' => ($isShowUpdateAPP) ? $isUpdateAPP : 0,
                    'firstTimeLoginAPP' => $firstTimeLoginAPP,
                    'isShowPromotionAPP' => $isShowPromotionAPP,
                    'isupload' => $statusUpload,
                    'notify_comment' => $count_comment,
                    'theme' => ($user->theme) ? $user->theme : 0,
                ]
            ];

            if (isset($needChangePassword) && $needChangePassword) {
                $result['data']['needChangePassword'] = true;
            } else {
                $result['data']['needChangePassword'] = false;
            }

            Yii::info("AUTH|RESPONSE=" . json_encode($result));

            return $result;

        } else {
            Yii::info("AUTH| osType=" . $osType . "|osVersionCode=" . $osVersionCode . "|configOsVersionCode=" . VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0));
            //Kiem tra xem co can update APP hay khong
            if ($osType && $osVersionCode) {
                $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
            }

            return [
                'responseCode' => ResponseCode::FORBIDDEN,
                'message' => ResponseCode::getMessage(ResponseCode::FORBIDDEN),
                'data' => [
                    'isForceUpdateAPP' => $isForceUpdateAPP,
                    'isUpdateAPP' => ($isShowUpdateAPP) ? $isUpdateAPP : 0
                ]
            ];
        }
    }

}