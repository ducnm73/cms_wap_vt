<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 9/29/2016
 * Time: 10:24 AM
 */

namespace common\modules\v2\controllers;

use common\controllers\ApiController;
use common\helpers\MobiTVRedisCache;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\libs\VtService;
use common\models\VtAccountInfomationBase;
use common\models\VtConfigBase;
use common\models\VtContractBase;
use common\models\VtUserBase;
use common\models\VtUserOtpBase;
use common\models\VtVideoBase;
use common\modules\v1\controllers\AuthController;
use common\modules\v2\libs\ResponseCode;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;

class ContractController extends ApiController
{

    public function actionCondition()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);
//        var_dump($contract);die;
        if ($contract) {

            //Neu chua co thong tin khach hang vt_account_infomation ma da co hop dong trang thai >0
            // thuc hien  insert thong tin khach hang vao vt_account_infomation
            $objInfomation = VtAccountInfomationBase::getByUserId($this->userId);
            //chua co thi insert thong tin vao vt_account_infomation
            if (!$objInfomation) {
                $accountInfomation = new VtAccountInfomationBase();
                $accountInfomation->name = $contract->name;
                $accountInfomation->name = $contract->name;
                $accountInfomation->email = $contract->email;
                $accountInfomation->status = 1;
                $accountInfomation->id_card_number = $contract->id_card_number;
                $accountInfomation->id_card_created_at = $contract->id_card_created_at;
                $accountInfomation->id_card_created_by = $contract->id_card_created_by;
                $accountInfomation->created_at = $contract->created_at;
                $accountInfomation->created_by = $contract->created_by;
                $accountInfomation->user_id = $contract->user_id;
                $accountInfomation->is_confirm = 1;
                $accountInfomation->id_card_image_frontside = $contract->id_card_image_frontside;
                $accountInfomation->id_card_image_backside = $contract->id_card_image_backside;
                $accountInfomation->bucket = $contract->bucket;
                $accountInfomation->msisdn = $contract->msisdn;
                $accountInfomation->created_at = date("Y-m-d H:i:s");
                $accountInfomation->updated_at = date("Y-m-d H:i:s");

                $accountInfomation->save(false);
            }

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công'),
                'data' => [
                    'contract' => [
                        'id' => $contract->id,
                        'status' => $contract->status,
						'name'=>$contract->name,
						'email'=>$contract->email,
						'id_card_number'=>$contract->id_card_number,
						'id_card_created_at'=>$contract->id_card_created_at,
						'id_card_created_by'=>$contract->id_card_created_by,
						'address'=>$contract->address,
						'msisdn'=>$contract->msisdn,
						'payment_type'=>$contract->payment_type,
						'tax_code'=>$contract->tax_code,
						'created_at'=>$contract->created_at,
						'created_by'=>$contract->created_by,
						'account_number'=>$contract->account_number,
						'bank_name'=>$contract->bank_name,
						'bank_department'=>$contract->bank_department,
						'user_id'=>$contract->user_id,
						'msisdn_pay'=>$contract->msisdn_pay,
						'reason'=>$contract->reason,
						'card_image_frontside'=>"http://static3.myclip.vn/".$contract->bucket."/".$contract->id_card_image_frontside,
						'card_image_backside'=>"http://static3.myclip.vn/".$contract->bucket."/".$contract->id_card_image_backside
                    ],
                    'condition' => VtConfigBase::getConfig("contract.condition.short")
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => '',
                'data' => [
                    'contract' => null,
                    'condition' => VtConfigBase::getConfig("contract.condition.short")
                ]
            ];
        }


    }

    public function actionPersonalInfomation()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);
        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            //Neu khong phai draff da duyet thi khong duoc sua
            if ( $contract && $contract->status == 2 ) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Hợp đồng đã tồn tại')
                ];
            }

            //Load infomation
            if ($postData['info_type'] == "ACCOUNT_INFOMATION") {
                $objInfomation = VtAccountInfomationBase::getByUserId($this->userId);
            }

            $isNew = false;
            if (!$contract) {
                $contract = $postData;
                $isNew = true;
            }
            //TODO validate
            if (!trim($postData['name'])) {
                if ($objInfomation['name']) {
                    $postData['name'] = $objInfomation['name'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập họ và tên')
                    ];
                }
            }


            if (!trim($postData['email'])) {
                if ($objInfomation['email']) {
                    $postData['email'] = $objInfomation['email'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập email')
                    ];
                }
            }
            else
            {
                if(!preg_match("/^[a-zA-Z0-9_.-]+\@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/", trim($postData['email'])))
                {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập Email hợp lệ')
                    ];
                }
            }
//            else {
//                if (!filter_var(trim($postData['email']), FILTER_VALIDATE_EMAIL)) {
//                    return [
//                        'responseCode' => ResponseCode::UNSUCCESS,
//                        'message' => Yii::t('wap','Yêu cầu nhập email hợp lệ')
//                    ];
//                }
//            }

            if (!trim($postData['id_card_number'])) {
                if ($objInfomation['id_card_number']) {
                    $postData['id_card_number'] = $objInfomation['id_card_number'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập Số chứng minh thư')
                    ];
                }
            }else{
//                if(!preg_match("/^([0-9]|[a-z]|[\-])*$/", trim($postData['id_card_number'])))
//                {
//                    return [
//                        'responseCode' => ResponseCode::UNSUCCESS,
//                        'message' => Yii::t('wap','Yêu cầu nhập Số chứng minh thư kiểu số hoặc chữ')
//                    ];
//                }else{
//                    if(preg_match("/^([0])*$/", trim($postData['id_card_number'])))
//                    {
//                        return [
//                            'responseCode' => ResponseCode::UNSUCCESS,
//                            'message' => Yii::t('wap','Yêu cầu nhập Số chứng minh thư đúng định dạng')
//                        ];
//                    }
//                    else
//                    {
                        $objContractCheck = VtContractBase::getByIdentifyId( trim($postData['id_card_number']) );
                        if( $objContractCheck && ($contract->id !=$objContractCheck->id) ){
                            return [
                                'responseCode' => ResponseCode::UNSUCCESS,
                                'message' => Yii::t('wap','Số chứng minh thư đã sử dụng ở tài khoản khác.')
                            ];
                        }
//                    }
//                }
            }

            if (!trim($postData['id_card_created_at'])) {
                if ($objInfomation['id_card_created_at']) {
                    $postData['id_card_created_at'] = $objInfomation['id_card_created_at'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập ngày cấp chứng minh thư')
                    ];
                }
            } else {
                if (!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $postData['id_card_created_at']) || \DateTime::createFromFormat('d/m/Y', $postData['id_card_created_at'])->format('d/m/Y') != $postData['id_card_created_at']) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập ngày cấp chứng minh thư dạng dd/mm/yyyy')
                    ];
                }

                $today = date('d-m-Y');
                $postData_date = str_replace('/', '-', trim($postData['id_card_created_at']));
                $check_today = strtotime($postData_date) - strtotime($today);
//                var_dump($check_today);die;
                if($check_today > 0) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập ngày cấp chứng minh thư nhỏ hơn hoặc bằng ngày hiện tại')
                    ];
                }
            }
            if (!trim($postData['id_card_created_by'])) {
                if ($objInfomation['id_card_created_by']) {
                    $postData['id_card_created_by'] = $objInfomation['id_card_created_by'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập nơi cấp chứng minh thư')
                    ];
                }
            }
            else
            {
                $name = str_replace(' ','',$postData['id_card_created_by']);
                if(preg_match("/^[0-9]+$/", $name))
//                if(!preg_match("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$", $name))
                {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập nơi cấp chứng minh thư')
                    ];
                }
            }

            if (!trim($postData['msisdn'])) {
                if ($objInfomation['msisdn']) {
                    $postData['msisdn'] = $objInfomation['msisdn'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập số điện thoại unitel')
                    ];
                }
            }
            $fileBucket = Yii::$app->params['s3']['static.bucket'];


            if (($_FILES["id_card_image_frontside"]["size"] == 0 && $isNew) || (!$isNew && !$contract->id_card_image_frontside)) {
                if ($objInfomation['id_card_image_frontside']) {
                    $filePathFrontside = $objInfomation['id_card_image_frontside'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập mặt trước ảnh chứng minh thư')
                    ];
                }

            }

            if (($_FILES["id_card_image_backside"]["size"] == 0 && $isNew) || (!$isNew && !$contract->id_card_image_backside)) {
                if ($objInfomation['id_card_image_frontside']) {
                    $filePathFrontside = $objInfomation['id_card_image_frontside'];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập mặt sau ảnh chứng minh thư')
                    ];
                }
            }


            if ($_FILES["id_card_image_frontside"]['name']) {
                //-- Validate image card frontside
                $ext = pathinfo($_FILES["id_card_image_frontside"]['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {

                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Định dạng file ảnh mặt trước không đúng')
                    ];
                }

                if ($_FILES["id_card_image_frontside"]['size'] > 8388608) { //8 MB (size is also in bytes)
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap', 'File ảnh mặt trước dung lượng tối đa 8MB')
                    ];
                }
                try {
                    $filePathFrontside = Utils::generatePath($ext);
                    S3Service::putObject($fileBucket, $_FILES["id_card_image_frontside"]['tmp_name'], $filePathFrontside);
                } catch (\Exception $exception) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Có lỗi upload ảnh, vui lòng thử lại sau')
                    ];
                }

            }

            if ($_FILES["id_card_image_backside"]['name']) {
                //-- Validate image card backside
                $ext = pathinfo($_FILES["id_card_image_backside"]['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Định dạng file ảnh mặt sau không đúng')
                    ];
                }

                if ($_FILES["id_card_image_backside"]['size'] > 8388608) { //8 MB (size is also in bytes)
                    Yii::$app->session->setFlash("info", Yii::t('wap',"File ảnh mặt sau dung lượng tối đa 8MB"));
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','File ảnh mặt sau dung lượng tối đa 8MB')
                    ];
                }

                try {
                    $filePathBackside = Utils::generatePath($ext);
                    S3Service::putObject($fileBucket, $_FILES["id_card_image_backside"]['tmp_name'], $filePathBackside);
                } catch (\Exception $exception) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Có lỗi upload ảnh, vui lòng thử lại sau')
                    ];
                }
            }

            $otp = trim($postData['otp']);
            if (!trim($postData['otp'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Yêu cầu nhập OTP')
                ];
            }

            if (!VtUserOtpBase::checkOTP(trim($postData['msisdn']), $otp)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Mã OTP không hợp lệ')
                ];
            }

            if ($otp) {
                VtUserOtpBase::deactiveAllOtp(trim($postData['msisdn']));
            }

            //
            if ($isNew) {
                $contract = new VtContractBase();
                $contract->contract_code = $this->userId . "-VAS/TLC–USER/MYCLIP/" . date("Y");

            }

            $contract->name = trim($postData['name']);


            $contract->email = trim($postData['email']);
            $contract->id_card_number = trim($postData['id_card_number']);
            $contract->id_card_created_at = trim($postData['id_card_created_at']);
            $contract->id_card_created_by = trim($postData['id_card_created_by']);

            if ($filePathFrontside) {
                $contract->id_card_image_frontside = $filePathFrontside;
                $contract->bucket = $fileBucket;
            } elseif ($objInfomation) {
                $contract->id_card_image_frontside = $objInfomation['id_card_image_frontside'];
                $contract->bucket = $objInfomation['bucket'];
            }

            if ($filePathBackside) {
                $contract->id_card_image_backside = $filePathBackside;
                $contract->bucket = $fileBucket;
            } elseif ($objInfomation) {
                $contract->id_card_image_backside = $objInfomation['id_card_image_backside'];
                $contract->bucket = $objInfomation['bucket'];
            }

            $contract->msisdn = Utils::getMobileNumber(trim($postData['msisdn']), Utils::MOBILE_GLOBAL);
            $contract->user_id = $this->userId;

            //--
            if (!$contract->created_at) {
                $contract->created_at = date("Y-m-d H:i:s");
                $contract->created_by = $this->userId;
            }

            //--
            $contract->updated_at = date("Y-m-d H:i:s");
            $contract->updated_by = $this->userId;

            $contract->save(false);
            //Kiem tra xem da co thong tin trong bang vt_account_infomation chua?
            $objInfomation = VtAccountInfomationBase::getByUserId($this->userId);
            //chua co thi insert thong tin vao vt_account_infomation
            if (!$objInfomation) {
                $accountInfomation = new VtAccountInfomationBase();
                $accountInfomation->name = $contract->name;
                $accountInfomation->name = $contract->name;
                $accountInfomation->email = $contract->email;
                $accountInfomation->status = 1;
                $accountInfomation->id_card_number = $contract->id_card_number;
                $accountInfomation->id_card_created_at = $contract->id_card_created_at;
                $accountInfomation->id_card_created_by = $contract->id_card_created_by;
                $accountInfomation->created_at = $contract->created_at;
                $accountInfomation->created_by = $contract->created_by;
                $accountInfomation->user_id = $contract->user_id;
                $accountInfomation->is_confirm = 1;
                $accountInfomation->id_card_image_frontside = $contract->id_card_image_frontside;
                $accountInfomation->id_card_image_backside = $contract->id_card_image_backside;
                $accountInfomation->bucket = $contract->bucket;
                $accountInfomation->msisdn = $contract->msisdn;
                $objInfomation->updated_at = date("Y-m-d H:i:s");
                $objInfomation->created_at = date("Y-m-d H:i:s");
                $accountInfomation->save(false);
            }else{
                $objInfomation->name = $contract->name;
                $objInfomation->name = $contract->name;
                $objInfomation->email = $contract->email;
                $objInfomation->status = 1;
                $objInfomation->id_card_number = $contract->id_card_number;
                $objInfomation->id_card_created_at = $contract->id_card_created_at;
                $objInfomation->id_card_created_by = $contract->id_card_created_by;
                $objInfomation->created_by = $contract->created_by;
                $objInfomation->user_id = $contract->user_id;
                $objInfomation->is_confirm = 1;
                $objInfomation->id_card_image_frontside = $contract->id_card_image_frontside;
                $objInfomation->id_card_image_backside = $contract->id_card_image_backside;
                $objInfomation->bucket = $contract->bucket;
                $objInfomation->msisdn = $contract->msisdn;
                $objInfomation->updated_at = date("Y-m-d H:i:s");
                $objInfomation->save(false);
            }

            //Cap nhat nhung video cua khach hang co status =0 trong 24h ve status =1
            VtVideoBase::updateTempToDraft24H($this->userId);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công')
            ];

        }

    }

    public function actionAccount()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);
        if (!$contract) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Trạng thái hợp đồng không hợp lệ')
            ];
        }

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();

            //Neu khong phai draff thi khong duoc sua
            if ($contract->status != 0) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Trạng thái hợp đồng không hợp lệ')
                ];
            }
            $paymentType = trim(strtoupper($postData['payment_type']));

            if ($paymentType == 'BANK_ACCOUNT') {
                if (!trim($postData['tax_code'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập mã số thuế')
                    ];
                }
                if (!trim($postData['account_number'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập tài khoản ngân hàng')
                    ];
                }else{
                    if(!is_numeric(trim($postData['account_number']))){
                        return [
                            'responseCode' => ResponseCode::UNSUCCESS,
                            'message' => Yii::t('wap','Yêu cầu nhập tài khoản ngân hàng kiểu số')
                        ];
                    }
                }
                if (!trim($postData['bank_name'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập tên ngân hàng')
                    ];
                }
                if (!trim($postData['bank_department'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập chi nhánh ngân hàng')
                    ];
                }
                if (!trim($postData['address'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập địa chỉ')
                    ];
                }
                $contract->tax_code = trim($postData['tax_code']);
                $contract->account_number = trim($postData['account_number']);
                $contract->bank_name = trim($postData['bank_name']);
                $contract->bank_department = trim($postData['bank_department']);
                $contract->payment_type = trim($postData['payment_type']);
                $contract->address = trim($postData['address']);
                $contract->save(false);

            } else if ($paymentType == 'CASH') {
                $msisdnPay = trim(Yii::$app->request->post('payment_mobile_number'));
                if (!$msisdnPay || !is_numeric($msisdnPay)) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập số thuê bao')
                    ];
                }
                if (!trim($postData['tax_code'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập mã số thuế')
                    ];
                }
                if (!trim($postData['address'])) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Yêu cầu nhập địa chỉ')
                    ];
                }
                $contract->address = trim($postData['address']);
                $contract->payment_type = trim($postData['payment_type']);
                $contract->tax_code = trim($postData['tax_code']);
                $contract->msisdn_pay = Utils::getMobileNumber($msisdnPay, Utils::MOBILE_GLOBAL);
                $contract->save(false);

            } else {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Hình thức thanh toán không hợp lệ')
                ];
            }

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công')
            ];
        }

    }

    public function actionContract()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);

        if (Yii::$app->request->isPost) {
            $btnContract = strtolower( trim(Yii::$app->request->post("action-contract")) );

            //
            if ($btnContract && !in_array($btnContract, [ "modify-contract", "change-failure-contract", "submit-contract" ])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Tham số không hợp lệ')
                ];
            }

            if ($btnContract == 'modify-contract') {
                //Neu khong phai cho duyet thi khong duoc sua
                if ($contract->status != 1) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Trạng thái không hợp lệ')
                    ];
                }
                $contract->status = 0;
                $contract->save(false);
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap','Thành công')
                ];
            } else if ($btnContract == 'change-failure-contract') {
                if ($contract->status == 3) {
                    $contract->status = 0;
                    $contract->reason = "";
                    $contract->save(false);
                    return [
                        'responseCode' => ResponseCode::SUCCESS,
                        'message' => Yii::t('wap','Thành công')
                    ];
                } else {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Trạng thái không hợp lệ')
                    ];
                }

            } else if( $btnContract == "submit-contract"){
                //Neu khong phai cho duyet thi khong duoc sua
                if ($contract->status != 0) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap','Trạng thái không hợp lệ')
                    ];
                }
                $contract->status = 1;
                $contract->save(false);

                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap','Hợp đồng đã tạo thành công, đang chờ phê duyệt.')
                ];
            }
        }

    }

    public function actionGetOtp()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }


        $msisdn = trim(Yii::$app->request->get("msisdn", ""));
        $contract = VtContractBase::getByUserId($this->userId);
        /* @var VtContractBase $contract */
        if (!$msisdn) {
            $msisdn = $contract->msisdn;
        }
        return AuthController::actionPushOtp($msisdn);
    }

    public function actionViewContract()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $user = VtUserBase::getById($this->userId);

        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);
        $filePath = Yii::$app->basePath . '/web/temp/';
        $fileName = "ສັນຍາ-" . uniqid() . '.docx';
        $templateProcessor = new TemplateProcessor($filePath . "ContractForm.docx");
        $templateProcessor->setValue('contract_code', $contract->contract_code); //${name}
        $templateProcessor->setValue('name', $contract->name); //${name}
        $templateProcessor->setValue('address', $contract->address); //${name}
        $templateProcessor->setValue('id_card_number', $contract->id_card_number); //${name}
        $templateProcessor->setValue('tax_code', $contract->tax_code); //${name}
        $templateProcessor->setValue('msisdn', $contract->msisdn); //${name}

        //$arrPaymentType=["CASH"=>"Chuyển tiền qua VTPAY", "MOBILE_ACCOUNT"=>"Tài khoản di động"];
        //Thay tai khoan di dong thanh tai khoan ngan hang
        $arrPaymentType = ["CASH" => Yii::t('wap',"Chuyển tiền qua VTPAY"), "MOBILE_ACCOUNT" => Yii::t('wap',"Tài khoản di động"), "BANK_ACCOUNT" => Yii::t('wap',"Tài khoản ngân hàng")];

        $templateProcessor->setValue('payment_type', $arrPaymentType[$contract->payment_type]); //${name}

        $templateProcessor->setValue('account_number', $contract->account_number); //${name}
        $templateProcessor->setValue('bank_name', $contract->bank_name); //${name}
        $templateProcessor->setValue('bank_department', $contract->bank_department); //${name}
        $templateProcessor->setValue('channel_name', $user->full_name); //${name}
        $templateProcessor->setValue('email', $contract->email); //${name}

        $templateProcessor->saveAs($filePath . $fileName);

        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/octetstream");
        header("Content-Disposition: attachment; filename=" . basename($filePath . $fileName));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($filePath . $fileName));
        @readfile($filePath . $fileName);
        @unlink($filePath . $fileName);
    }

    public function actionGetAccountOtp()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $msisdn = Yii::$app->request->post('msisdn');
        if (!$msisdn) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Yêu cầu nhập số thuê bao')
            ];
        }

        $limitOtpByUser = Yii::$app->cache->get("acc_otp_limit_" . $this->userId);
        if (!$limitOtpByUser) {
            Yii::$app->cache->set("acc_otp_limit_" . $this->userId, 1, MobiTVRedisCache::CACHE_1DAY);
        } else {
            Yii::$app->cache->set("acc_otp_limit_" . $this->userId, $limitOtpByUser + 1, MobiTVRedisCache::CACHE_1DAY);
        }

        if ($limitOtpByUser >= 3) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Quá hạn mức nhận OTP trong ngày')
            ];
        }

        $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
        // $authController = new \common\modules\v1\controllers\AuthController();
        return AuthController::actionPushOtp($msisdn);

    }

    public function actionLoadInfomation()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        if (!$this->msisdn) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' =>Yii::t('wap', 'Không có số thuê bao'),
            ];
        }
        //Load from DB
        $accountInfomation = VtAccountInfomationBase::getByUserId($this->userId);

        if ($accountInfomation) {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
                'data' => [
                    "isHasVideo"=>(VtVideoBase::findOne(['created_by'=> $this->userId]))?1:0,
                    "infoType" => "ACCOUNT_INFOMATION",// OR CM_INFOMATION
                    "msisdn" => $this->msisdn,
                    "name" => $accountInfomation->name,
                    "email" => $accountInfomation->email,
                    "status" => intval($accountInfomation->status),
                    "reason_reject" => $accountInfomation->reason,
                    "id_card_number" => $accountInfomation->id_card_number,
                    "id_card_created_at" => $accountInfomation->id_card_created_at,
                    "id_card_created_by" => $accountInfomation->id_card_created_by,
                    "id_card_image_frontside" => VtHelper::getThumbUrl($accountInfomation->bucket, $accountInfomation->id_card_image_frontside, false),
                    "id_card_image_backside" => VtHelper::getThumbUrl($accountInfomation->bucket, $accountInfomation->id_card_image_backside, false)
                ]
            ];
        } else {
            //TODO Load from CM
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap', 'Thành công'),
            'data' => [
                "isHasVideo"=>(VtVideoBase::findOne(['created_by'=> $this->userId]))?1:0,
                "msisdn" => $this->msisdn,
            ]
        ];
    }

    public function actionInsertContract(){
        VtService::insertNewContract();

    }
}