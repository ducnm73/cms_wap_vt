<?php

namespace common\modules\v2\controllers;

use common\libs\VtHelper;
use common\models\VtSubBase;
use common\models\VtVideoBase;
use common\modules\v2\libs\ResponseCode;
use Yii;


class KpiController extends \common\modules\v1\controllers\AuthController
{

    public function actionInit()
    {
        $videoId = Yii::$app->request->get('video_id');
        $playUrl = Yii::$app->request->get('play_url');
        $osVersion = Yii::$app->request->get('os_version');
        $osType = Yii::$app->request->get('os_type');
        if ($videoId && $playUrl) {

            $collection = Yii::$app->mongodb->getCollection('play_log');
            $objVideo = VtVideoBase::findOne(["id" => $videoId]);
            $objSub = VtSubBase::getSub($this->msisdn);

            $objToken = $collection->insert(
                [
                    'video_id' => intval($videoId),
                    'play_url' => $playUrl,
                    'channel_id' => intval(($objVideo) ? $objVideo->channel_id : 0),
                    'created_by' => intval(($objVideo) ? $objVideo->created_by : 0),
                    'category_id' => intval(($objVideo) ? $objVideo->category_id : 0),
                    'is_sub' => intval(($objSub) ? 1 : 0),
                    'duration_watching' => floatval(0),
                    'pause_times' => intval(0),
                    'seek_times' => intval(0),
                    'wait_times' => intval(0),
                    'duration_buffer' => floatval(0),
                    'current_time' => floatval(0),
                    'ip' => VtHelper::getAgentIp(),
                    'user_id' => strval($this->userId),
                    'msisdn' => strval($this->msisdn),
                    'os_version' => $osVersion,
                    'os_type' => $osType,
                    'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'init_time' => floatval(-1),
                    'bandwidth_avg' => floatval(0),
                    'buffer_times_over_3s' => intval(0),
                ]
            );

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'token' => $objToken->__toString(),
                'frequency' => 10
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Require video_id vs play_url"),

            ];
        }


    }

    public function actionTrace()
    {

        $token = Yii::$app->request->get('token');
        $durationWatching = Yii::$app->request->get('duration_watching');
        $pauseTimes = Yii::$app->request->get('pause_times');
        $waitTimes = Yii::$app->request->get('wait_times');
        $seekTimes = Yii::$app->request->get('seek_times');
        $durationBuffer = Yii::$app->request->get('duration_buffer');
        $curentTime = Yii::$app->request->get('current_time');
        $initTime = Yii::$app->request->get('init_time', -1);
        $bandWidthAvg = Yii::$app->request->get('bandwidth_avg');
        $bufferTimesOver3s = Yii::$app->request->get('buffer_times_over_3s');
        $bandWidth = Yii::$app->request->get('bandwidth');

        if ($token) {
            $collection = Yii::$app->mongodb->getCollection('play_log');
			// var_dump($collection); die('xxx');
            $objToken = $collection->update(
                ['_id' => $token],
                [
                    'duration_watching' => floatval($durationWatching),
                    'pause_times' => intval($pauseTimes),
                    'seek_times' => intval($seekTimes),
                    'wait_times' => intval($waitTimes),
                    'duration_buffer' => floatval($durationBuffer),
                    'current_time' => floatval($curentTime),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'init_time' => floatval($initTime),
                    'bandwidth_avg' => floatval($bandWidthAvg),
                    'buffer_times_over_3s' => intval($bufferTimesOver3s)
                ]
            );

            //save collection tmpLog
            $collection = Yii::$app->mongodb->getCollection('play_tmp_log');
            $objToken = $collection->insert(
                [
                    'parent_id' => $token,
                    'bandwidth' => $bandWidth,
                    'created_at' => date("Y-m-d H:i:s")
                ]
            );


            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'is_success' => $objToken
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Require Token'),
            ];
        }

    }

    public function actionReportBySession()
    {
        $totalError = Yii::$app->request->get('total_error');
        $totalSuccess = Yii::$app->request->get('total_success');
        $responseTimeAvg = Yii::$app->request->get('response_time_avg');

        $osVersion = Yii::$app->request->get('os_version');
        $osType = Yii::$app->request->get('os_type');
        $clientId = Yii::$app->request->get('client_id');

        if ($clientId) {
            $collection = Yii::$app->mongodb->getCollection('monitor_access_log');
            $objToken = $collection->insert(
                [
                    'total_error' => intval($totalError),
                    'total_success' => intval($totalSuccess),
                    'response_time_avg' => floatval($responseTimeAvg),
                    'ip' => VtHelper::getAgentIp(),
                    'user_id' => strval($this->userId),
                    'msisdn' => strval($this->msisdn),
                    'os_version' => $osVersion,
                    'os_type' => $osType,
                    'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'client_id' => $clientId
                ]
            );
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'token' => $objToken->__toString(),
                'frequency' => 10
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Require clientId"),
            ];
        }
    }

    public function actionReportByScreen()
    {
        $completeTime = Yii::$app->request->get('complete_time');
        $screenId = Yii::$app->request->get('screen_id');
        $osVersion = Yii::$app->request->get('os_version');
        $osType = Yii::$app->request->get('os_type');
        $clientId = Yii::$app->request->get('client_id');

        if ($clientId) {
            $collection = Yii::$app->mongodb->getCollection('monitor_screen_log');
            $objToken = $collection->insert(
                [
                    'complete_time' => intval($completeTime),
                    'screen_id' => intval($screenId),
                    'ip' => VtHelper::getAgentIp(),
                    'user_id' => strval($this->userId),
                    'msisdn' => strval($this->msisdn),
                    'os_version' => $osVersion,
                    'os_type' => $osType,
                    'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'client_id' => $clientId
                ]
            );

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'token' => $objToken->__toString(),
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Require clientId"),
            ];
        }
    }

}