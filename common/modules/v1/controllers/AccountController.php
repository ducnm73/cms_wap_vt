<?php

namespace common\modules\v1\controllers;

use common\helpers\Des;
use common\helpers\FineUploaderTraditional;
use common\helpers\MobiTVRedisCache;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\KpiLogBase;
use common\models\LogMapAccountBase;
use common\models\VtActionLogBase;
use common\models\VtBuyPlaylistBase;
use common\models\VtBuyVideoBase;
use common\models\VtChannelBase;
use common\models\VtChannelFollowBase;
use common\models\VtFavouriteVideoBase;
use common\models\VtGroupCategoryBase;
use common\models\VtGroupTopicBase;
use common\models\VtHistoryViewBase;
use common\models\VtPackageBase;
use common\models\VtPlaylistBase;
use common\models\VtSubBase;
use common\models\VtUserBase;
use common\models\VtUserFollowBase;
use common\models\VtUserOtpBase;
use common\models\VtUserTokenBase;
use common\modules\v1\libs\Obj;
use common\modules\v1\libs\UserObj;
use common\modules\v1\libs\VideoObj;
use wap\models\VtGroupTopic;
use wap\models\VtPlaylistItem;
use wap\models\VtUser;
use wap\models\VtVideo;
use Yii;
use common\models\VtConfigBase;
use common\models\VtGcmBase;
use common\models\VtSmsMessageBase;
use common\models\VtSmsMtBase;
use common\models\VtVideoBase;
use common\modules\v1\libs\ResponseCode;
use wap\models\VtSmsMessage;
use common\helpers\Utils;
use common\libs\VtService;
use common\controllers\ApiController;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class AccountController extends ApiController
{
    /**
     * Dang ky KM APP dich vu
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionRegisterPromotionApp()
    {
        if ($this->isValidUser() && !$this->isValidMsisdn()) {
            $objUser = VtUserBase::getById($this->userId);
            if (!$objUser->msisdn) {
                return [
                    'responseCode' => ResponseCode::EMPTY_MSISDN,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            } else {
                $this->msisdn = $objUser->msisdn;
            }

        } else if (!$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $packageId = VtConfigBase::getConfig('promotion.install.app.package.id', '');

        if (!$packageId || !preg_match('/^\d+$/', $packageId)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web',"Mã gói cước không hợp lệ")
            ];
        }

        //Kiem tra xem khach hang da dang ky goi cuoc hay chua? Neu da tung dang ky goi cuoc thi khong duoc KM
        $checkPromotion = VtSubBase::checkPromotionFirstTimeUsageAPP($this->msisdn);
        if (!$checkPromotion) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => VtConfigBase::getConfig("check_promotion_invalid", Yii::t('web',"Thuê bao không được nhận khuyến mãi"))
            ];
        }

        // -- Dat log KPI giam sat
        $startTimeLog = round(microtime(true) * 1000);
        $idLog = KpiLogBase::writeLog(KpiLogBase::ACTION_REGISTER, $packageId, KpiLogBase::ACTION_REGISTER);

        $source = Yii::$app->session->get('source', Yii::$app->params['app.source']);

        $errorCode = VtService::registerNoRenew($this->msisdn, $packageId, $source);
        $package = VtPackageBase::getDetail($packageId);
        $blacklistErrorCodes = explode(',', VtConfigBase::getConfig("sendsms_blacklist"));
        $sendSmsSources = explode(',', VtConfigBase::getConfig("sendsms_source"));

        if (in_array($source, $sendSmsSources) && !in_array($errorCode, $blacklistErrorCodes)) {

            $mt = new VtSmsMtBase();
            $content = VtSmsMessageBase::getConfig("register_sms_" . $packageId . "_" . $errorCode);
            if ($content) {
                $mt->sendSms($this->msisdn, $content);
            }
        }


        if ($errorCode === Yii::$app->params['register.reg.success']) {
            $msgConfig = VtConfigBase::getConfig("gateway_register_errorcode_msg_" . $errorCode, Yii::t('web',"Đăng ký thành công"));
            // Dat log KPI giam sat
            $endTimeLog = round(microtime(true) * 1000);
            KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => $msgConfig . $package['name']
            ];
        } else {
            $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Đăng ký không thành công"));

            if ($errorCode == 46) {
                $isConfirm = 0;
                //Kiem tra cau hinh xem co can confirm dang ky hay khong?

                switch (Yii::$app->id) {
                    case 'app-wap':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                        break;
                    case 'app-api':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                        break;
                    case 'app-web':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                        break;
                }
                $msgConfig = str_replace("%package%", $package['name'], $msgConfig);

                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);

                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => $msgConfig,
                    'content' => VtConfigBase::getConfig('sms_command', 'OK'),
                    'number' => VtConfigBase::getConfig('SHORTCODE', '1515'),
                    'is_confirm_sms' => $isConfirm
                ];
            } else {
                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_FAIL);
            }

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => $msgConfig
            ];
        }

    }

    /**
     * Dang ky dich vu
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionRegisterService()
    {
        if ($this->isValidUser() && !$this->isValidMsisdn()) {
            $objUser = VtUserBase::getById($this->userId);
            if (!$objUser->msisdn) {
                return [
                    'responseCode' => ResponseCode::EMPTY_MSISDN,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            } else {
                $this->msisdn = $objUser->msisdn;
            }

        } else if (!$this->isValidMsisdn()) {
            return $this->authJson;
        }

        if($this->isValidUser() && $this->isValidMsisdn()) {
            /*Kiểm tra db*/
            $userDb = VtUserBase::findOne(['id' => $this->userId, 'msisdn' => $this->msisdn]);
            if(!$userDb) {
                return [
                    'responseCode' => ResponseCode::UNAUTHORIZED,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            }
        }

        $packageId = trim(Yii::$app->request->post('package_id', ''));
        $contentId = trim(Yii::$app->request->post('content_id', ''));

        if (!$packageId || !preg_match('/^\d+$/', $packageId)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web',"Mã gói cước không hợp lệ")
            ];
        }

//        //Kiem tra xem khach hang co dang dang ky goi addon 4GPLAY truoc do khong?
//        $arrRegistedPackage = VtSubBase::getSub($this->msisdn);
//        foreach ($arrRegistedPackage as $mySub) {
//            $arrAddonPackage = explode(",", VtConfigBase::getConfig("addon4gplay_package"));
//            if ($arrAddonPackage && in_array($mySub['package_id'], $arrAddonPackage)) {
//                return [
//                    'responseCode' => ResponseCode::UNSUCCESS,
//                    'message' => VtConfigBase::getConfig("msg_register_addon4gplay_invalid")
//                ];
//            }
//
//            $arrBundlePackage = explode(",", VtConfigBase::getConfig("bundle_package"));
//            if ($arrBundlePackage && in_array($mySub['package_id'], $arrBundlePackage)) {
//                return [
//                    'responseCode' => ResponseCode::UNSUCCESS,
//                    'message' => VtConfigBase::getConfig("msg_register_bundle_invalid")
//                ];
//            }
//
//        }

        // -- Dat log KPI giam sat
        $startTimeLog = round(microtime(true) * 1000);
        $idLog = KpiLogBase::writeLog(KpiLogBase::ACTION_REGISTER, $packageId, KpiLogBase::ACTION_REGISTER);

        $source = Yii::$app->session->get('source', Yii::$app->params['app.source']);

        $errorCode = VtService::registerService($this->msisdn, $packageId, $source);
        $package = VtPackageBase::getDetailMultilang($packageId);
        $blacklistErrorCodes = explode(',', VtConfigBase::getConfig("sendsms_blacklist"));
        $sendSmsSources = explode(',', VtConfigBase::getConfig("sendsms_source"));

        if (in_array($source, $sendSmsSources) && !in_array($errorCode, $blacklistErrorCodes)) {

            $mt = new VtSmsMtBase();
            $content = VtSmsMessageBase::getConfig("register_sms_" . $packageId . "_" . $errorCode);
            if ($content) {
                $mt->sendSms($this->msisdn, $content);
            }
        }


        if ($errorCode === Yii::$app->params['register.reg.success']) {
            $msgConfig = VtConfigBase::getConfig("gateway_register_errorcode_msg_" . $errorCode, Yii::t('web','Đăng ký thành công'));
            // Dat log KPI giam sat
            $endTimeLog = round(microtime(true) * 1000);
            KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => $msgConfig . " " . $package['name'],
                'errorCode'=>977
            ];
        } else {
            $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Đăng ký không thành công"));

            if ($errorCode == 46) {
                $isConfirm = 0;
                //Kiem tra cau hinh xem co can confirm dang ky hay khong?

                switch (Yii::$app->id) {
                    case 'app-wap':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                        break;
                    case 'app-api':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                        break;
                    case 'app-web':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                        break;
                }
                //Neu dang ky sub de xem noi dung(confirm sms)
                if($contentId && $isConfirm){
                    $objContent = VtVideoBase::getDetail($contentId);
                    if($objContent){
                        $msgConfig =  VtConfigBase::getConfig("gateway_errorcode_msg_1234" , "");
                        $objVideo = VtVideoBase::getDetail($contentId);
                        $msgConfig = str_replace("%video_name%", $objVideo['name'], $msgConfig);
                        $msgConfig = str_replace("%package_price%", $package['fee'], $msgConfig);
                        $msgConfig = str_replace("%short_code%",  VtConfigBase::getConfig("SHORTCODE"), $msgConfig);
                        $msgConfig = str_replace("%sms_command%",  VtConfigBase::getConfig("sms_command"), $msgConfig);
                    }
                }

                $msgConfig = str_replace("%package%", $package['name'], $msgConfig);
                $msgConfig = str_replace("%short_code%",  VtConfigBase::getConfig("SHORTCODE"), $msgConfig);
                $msgConfig = str_replace("%sms_command%",  VtConfigBase::getConfig("sms_command"), $msgConfig);

                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => $msgConfig,
                    'content' => VtConfigBase::getConfig('sms_command', 'OK'),
                    'number' => VtConfigBase::getConfig('SHORTCODE', '1515'),
                    'is_confirm_sms' => $isConfirm,
                    'errorCode'=>46
                ];
            } else {
                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_FAIL);
            }

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => $msgConfig,
                'errorCode'=>$errorCode
            ];
        }

    }

    /**
     * Dang ky dich vu
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionRegisterNoWaitService()
    {
        if ($this->isValidUser() && !$this->isValidMsisdn()) {
            $objUser = VtUserBase::getById($this->userId);
            if (!$objUser->msisdn) {
                return [
                    'responseCode' => ResponseCode::EMPTY_MSISDN,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            } else {
                $this->msisdn = $objUser->msisdn;
            }

        } else if (!$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $packageId = trim(Yii::$app->request->post('package_id', ''));
        $contentId = trim(Yii::$app->request->post('content_id', ''));

        if (!$packageId || !preg_match('/^\d+$/', $packageId)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web',"Mã gói cước không hợp lệ")
            ];
        }

        //Kiem tra xem khach hang co dang dang ky goi addon 4GPLAY truoc do khong?
        $arrRegistedPackage = VtSubBase::getSub($this->msisdn);
        foreach ($arrRegistedPackage as $mySub) {
            $arrAddonPackage = explode(",", VtConfigBase::getConfig("addon4gplay_package"));
            if ($arrAddonPackage && in_array($mySub['package_id'], $arrAddonPackage)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => VtConfigBase::getConfig("msg_register_addon4gplay_invalid")
                ];
            }

            $arrBundlePackage = explode(",", VtConfigBase::getConfig("bundle_package"));
            if ($arrBundlePackage && in_array($mySub['package_id'], $arrBundlePackage)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => VtConfigBase::getConfig("msg_register_bundle_invalid")
                ];
            }

        }

        // -- Dat log KPI giam sat
        $startTimeLog = round(microtime(true) * 1000);
        $idLog = KpiLogBase::writeLog(KpiLogBase::ACTION_REGISTER, $packageId, KpiLogBase::ACTION_REGISTER);

        $source = Yii::$app->session->get('source', Yii::$app->params['app.source']);

        $errorCode = VtService::registerNoWaitService($this->msisdn, $packageId, $source);

        $package = VtPackageBase::getDetail($packageId);
        $blacklistErrorCodes = explode(',', VtConfigBase::getConfig("sendsms_blacklist"));
        $sendSmsSources = explode(',', VtConfigBase::getConfig("sendsms_source"));

        if (in_array($source, $sendSmsSources) && !in_array($errorCode, $blacklistErrorCodes)) {

            $mt = new VtSmsMtBase();
            $content = VtSmsMessageBase::getConfig("register_sms_" . $packageId . "_" . $errorCode);
            if ($content) {
                $mt->sendSms($this->msisdn, $content);
            }
        }


        if ($errorCode === Yii::$app->params['register.reg.success']) {
            $msgConfig = VtConfigBase::getConfig("gateway_register_errorcode_msg_" . $errorCode, Yii::t('web',"Đăng ký thành công"));
            // Dat log KPI giam sat
            $endTimeLog = round(microtime(true) * 1000);
            KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => $msgConfig . $package['name'],
                'errorCode'=>0
            ];
        } else {
            $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Đăng ký không thành công"));

            if ($errorCode == 46) {
                $isConfirm = 0;
                //Kiem tra cau hinh xem co can confirm dang ky hay khong?

                switch (Yii::$app->id) {
                    case 'app-wap':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                        break;
                    case 'app-api':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                        break;
                    case 'app-web':
                        $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                        break;
                }
                //Neu dang ky sub de xem noi dung(confirm sms)
                if ($contentId && $isConfirm) {
                    $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_1234", "");
                    $objVideo = VtVideoBase::getDetail($contentId);
                    $msgConfig = str_replace("%video_name%", $objVideo['video'], $msgConfig);
                    $msgConfig = str_replace("%package_price%", $package['price'], $msgConfig);
                }

                $msgConfig = str_replace("%package%", $package['name'], $msgConfig);
                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => $msgConfig,
                    'content' => VtConfigBase::getConfig('sms_command', 'OK'),
                    'number' => VtConfigBase::getConfig('SHORTCODE', '1515'),
                    'is_confirm_sms' => $isConfirm,
                    'errorCode'=>46
                ];
            }elseif($errorCode = 977){
                //Cau hinh ma 997
                $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_997", "");

                $endTimeLog = round(microtime(true) * 1000);
                //-- Insert Redis cache set can view content free.
                $redisCache =  Yii::$app->cache;
                //neu chua co ban ghi xem free khi doi dang ky, thi insert
                if(!$redisCache->get("VIEW_WAIT_REGISTER_".$this->userId)){
                    $redisCache->set("VIEW_WAIT_REGISTER_".$this->userId, date("Y-m-d"), MobiTVRedisCache::CACHE_1DAY);
                    $redisCache->set("NUM_VIEW_WAIT_REGISTER_".$this->userId, 1, MobiTVRedisCache::CACHE_1DAY);
                }else{
                    $redisCache->set("NUM_VIEW_WAIT_REGISTER_".$this->userId, intval($redisCache->get("NUM_VIEW_WAIT_REGISTER_".$this->userId)) +1, MobiTVRedisCache::CACHE_1DAY);
                }

                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => $msgConfig,
                    'errorCode'=>977
                ];

            } else {
                //Dat log KPI giam sat
                $endTimeLog = round(microtime(true) * 1000);
                KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_FAIL);
            }

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => $msgConfig,
                'errorCode'=>$errorCode
            ];
        }

    }



    /**
     * Huy dich vu
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionUnregisterService()
    {
        if (!$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $packageId = trim(Yii::$app->request->post('package_id', ''));
        if (!$packageId || !preg_match('/^\d+$/', $packageId)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web',"Mã gói cước không hợp lệ")
            ];
        }
        $source = Yii::$app->params['app.source'];
        // Kiem tra xem goi cuoc co duoc phep huy hay khong? (4GPLAY)
        $arrAddonPackage = explode(",", VtConfigBase::getConfig("addon4gplay_package"));
        if ($arrAddonPackage && in_array($packageId, $arrAddonPackage)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => VtConfigBase::getConfig("msg_cancel_addon4gplay_invalid")
            ];
        }

        $arrBundlePackage = explode(",", VtConfigBase::getConfig("bundle_package"));
        if ($arrBundlePackage && in_array($packageId, $arrBundlePackage)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => VtConfigBase::getConfig("msg_cancel_bundle_invalid")
            ];
        }

        // -- Dat log KPI giam sat
        $startTimeLog = round(microtime(true) * 1000);
        $idLog = KpiLogBase::writeLog(KpiLogBase::ACTION_CANCEL, $packageId, KpiLogBase::ACTION_CANCEL);

        $errorCode = VtService::cancelService($this->msisdn, $packageId, $source);

        $blacklistErrorCodes = explode(',', VtConfigBase::getConfig("sendsms_blacklist"));
        $sendSmsSources = explode(',', VtConfigBase::getConfig("sendsms_source"));

        if (in_array($source, $sendSmsSources) && !in_array($errorCode, $blacklistErrorCodes)) {
            $mt = new VtSmsMtBase();
            $content = VtSmsMessageBase::getConfig("cancel_sms_" . $packageId . "_" . $errorCode);
            if ($content) {
                $mt->sendSms($this->msisdn, $content);
            }
        }

        if ($errorCode === Yii::$app->params['register.unreg.success']) {

            $msgDefault = Yii::t('web', "Hủy thành công");
            $msgConfig = VtConfigBase::getConfig("gateway_cancel_errorcode_msg_" . $errorCode);

            // Dat log kpi giam sat
            $endTimeLog = round(microtime(true) * 1000);
            KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_SUCCESS);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ($msgConfig) ? $msgConfig : $msgDefault
            ];
        } else {
            $msgDefault = Yii::t('web', "Hủy không thành công");
            $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode);

            // Dat log kpi giam sat
            $endTimeLog = round(microtime(true) * 1000);
            KpiLogBase::updateLog($idLog, ($endTimeLog - $startTimeLog), $errorCode, $errorCode, KpiLogBase::TRANSACTION_FAIL);

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => ($msgConfig) ? $msgConfig : $msgDefault
            ];
        }

    }

    /**
     * Ham cap nhat thoi diem xem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionWatchTime()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }


        $id = trim(Yii::$app->request->post('id', ''));
        $time = trim(Yii::$app->request->post('time', ''));
        $parentId = trim(Yii::$app->request->post('parent_id', 0));

        if (empty($id) || empty($time)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Dữ liệu không hợp lệ'),
            ];
        }

        $video = VtVideoBase::getDetail($id, [VtVideoBase::TYPE_VOD, VtVideoBase::TYPE_FILM]);
        if (!$video) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap','Video không tồn tại'),
            ];
        }

        $historyView = new VtHistoryViewBase();
        $historyView->insertHistory($this->userId, $this->msisdn, $video, $time, $parentId);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',ResponseCode::getMessage(ResponseCode::SUCCESS)),
        ];
    }

    /**
     * Ham cap nhat thoi diem xem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionRegisterClientId()
    {
        if (!$this->isValidUser() && !$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $clientId = trim(Yii::$app->request->post('client_id', ''));
        $type = trim(Yii::$app->request->post('type', 0));

        if (empty($clientId) || !in_array($type, [1, 2, 3])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web','Dữ liệu không hợp lệ'),
            ];
        }

//        $mtvGcm = VtGcmBase::getByUserId($this->userId);
        $mtvGcm = VtGcmBase::getByRegisterId($clientId);

        Yii::info('{RegisterClientId} userId:' . $this->userId . '|msisdn:' . $this->msisdn . '|registerId:' . $clientId . '|type:' . $type, 'info');

        if (!$mtvGcm) {
            $mtvGcm = new VtGcmBase();
            $mtvGcm->insertClientId($this->userId, $this->msisdn, $clientId, VtGcmBase::STATUS_ACTIVE, $type);
        } else {
            $mtvGcm->register_id = $clientId;
            $mtvGcm->msisdn = $this->msisdn;
            $mtvGcm->status = VtGcmBase::STATUS_ACTIVE;
            $mtvGcm->type = $type;
            $mtvGcm->save(false);
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
        ];
    }

    /**
     * Lay chi tiet profile cua ca nhan
     * @return array
     */
    public function actionGetUserProfile()
    {
        $userId = trim(Yii::$app->request->get('id', 0));
        if (!$userId) {
            $userId = $this->userId;
            if (!$this->isValidUser()) {
                return $this->authJson;
            }
        }
        $objUser = VtUserBase::getById($userId);
        if (!$objUser) {
            return [
                'responseCode' => ResponseCode::USER_UNREGISTERED,
                'message' => Yii::t('web','Thuê bao chưa đăng ký')
            ];
        } else if ($objUser->status == VtUserBase::INACTIVE || $objUser->status == VtUserBase::BANNED) {
            return [
                'responseCode' => ResponseCode::USER_INACTIVE,
                'message' => Yii::t('wap','Thuê bao không tồn tại hoặc bị tạm khóa')
            ];
        } else {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('web','Thành công'),
                'data' => [
                    'user_detail' => [
                        'id' => $objUser['id'],
                        'name' => ($objUser['full_name']) ? $objUser['full_name'] : $objUser['msisdn'],
                        'followCount' => $objUser['follow_count'],
                        'avatarImage' => VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR),
                        'videoCount' => $objUser['video_count']
                    ]
                ]
            ];

        }
    }

    /**
     * @author PhuMX
     * @return Xem profile cua cac thanh vien khac
     * @return array
     */
    public function actionViewProfile()
    {
        $userId = trim(Yii::$app->request->get('id', 0));

        $objUser = VtUserBase::getById($userId);
        if (!$objUser) {
            return [
                'responseCode' => ResponseCode::USER_UNREGISTERED,
                'message' => Yii::t('wap','Thuê bao chưa đăng ký')
            ];
        } else if ($objUser->status == VtUserBase::INACTIVE || $objUser->status == VtUserBase::BANNED) {
            return [
                'responseCode' => ResponseCode::USER_INACTIVE,
                'message' => Yii::t('wap','Thuê bao không tồn tại hoặc bị tạm khóa')
            ];
        } else {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công'),
                'data' => [
                    'user_detail' => [
                        'id' => $objUser['id'],
                        'name' => ($objUser['full_name']) ? $objUser['full_name'] : (substr($objUser['msisdn'], 0, -3) . "xxx"),
                        'followCount' => $objUser['follow_count'],
                        'avatarImage' => VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR),
                        'videoCount' => $objUser['video_count']
                    ]
                ]
            ];
        }
    }

    /**
     * Lay danh sach noi dung da xem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionGetHistoryContent()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $histories = VtHistoryViewBase::getByUser($this->userId, $this->msisdn, Yii::$app->params['app.page.limit']);

        $historyIds = array();
        foreach ($histories as $history) {
            $historyIds[] = $history['_source']['item_id'];
        }

        $historyVideos = VideoObj::serialize(
            Obj::VIDEO_HISTORY,
            VtVideoBase::getVideosByIdsQuery($historyIds, Yii::$app->params['app.page.limit'], 0)
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')])
        );

        $dataResponse = array();

        if (isset($historyVideos) && $historyVideos['content']) {
            $dataResponse[] = $historyVideos;
        }
        //var_dump(ResponseCode::SUCCESS);die;
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',ResponseCode::getMessage(ResponseCode::SUCCESS)),
            'data' => $dataResponse
        ];
    }


    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionToggleFollow()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Quý khách vui lòng đăng ký thành viên để sử dụng tính năng này!'),
                ];
            } else {
                return $this->authJson;
            }
        }
        $followId = trim(Yii::$app->request->post('follow_user_id', 0));

        if (!$followId) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }
        if ($this->userId == $followId) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web',"Không thể theo dõi chính mình!")
            ];
        }
        $objUser = VtChannelBase::getChannelById($followId);
        if (!$objUser) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('web',"Thành viên cần theo dõi không tồn tại!")
            ];
        }
        $followCount = $objUser->follow_count;
        $isFollow = false;

        $followObj = VtChannelFollowBase::getFollow($this->userId, $followId);

        if (!empty($followObj)) {
            $followObj->delete();
            $isFollow = false;
            $followCount--;
        } else {
            $followObj = new VtChannelFollowBase();
            $followObj->insertFollow($this->userId, $followId);
            $isFollow = true;
            $followCount++;
        }
        VtUserBase::updateFollowCount($objUser['user_id'], $isFollow);
        VtChannelBase::updateFollowCount($this->userId, $isFollow);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap',ResponseCode::getMessage(ResponseCode::SUCCESS)),
            'data' => [
                'isFollow' => $isFollow,
                'followCount' => $followCount
            ]
        ];

    }

    /**
     * Dang ky dich vu
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionBuy()
    {
        if ($this->isValidUser() && !$this->isValidMsisdn()) {
            $objUser = VtUserBase::getById($this->userId);
            if (!$objUser->msisdn) {
                return [
                    'responseCode' => ResponseCode::EMPTY_MSISDN,
                    'message' => Yii::t('web','Quý khách vui lòng sử dụng số thuê bao Unitel để sử dụng dịch vụ')
                ];
            } else {
                $this->msisdn = $objUser->msisdn;
            }
        } else if (!$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $itemId = trim(Yii::$app->request->post('item_id', ''));
        $type = trim(Yii::$app->request->post('type', ''));
        if (!$itemId || !$type || !in_array($type, Yii::$app->params['type.buy'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web',"Dữ liệu không hợp lệ")
            ];
        }

        switch ($type) {
            case 'FILM':
            case 'VOD':
                $objVideo = VtVideoBase::getDetail($itemId, [VtVideoBase::TYPE_VOD, VtVideoBase::TYPE_FILM]);
                if (!$objVideo) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Nội dung không hợp lệ")
                    ];
                }
                $fee = $objVideo['price_play'];
                $source = Yii::$app->params['app.source'];
                $errorCode = VtService::paymentService(VtService::PAYMENT_ACTION_BUY . $type, $this->msisdn, $fee, $itemId, $type, $source);
                if ($errorCode === "0") {
                    $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Mua lẻ thành công"));
                    $objBuy = new VtBuyVideoBase();
                    $objBuy->insertTransaction($this->msisdn, $this->userId, $itemId);
                    return [
                        'responseCode' => ResponseCode::SUCCESS,
                        'message' => $msgConfig
                    ];
                } else {
                    $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Mua lẻ không thành công"));
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => $msgConfig
                    ];
                }

                break;
            case 'PLAYLIST':
                $objPlaylist = VtPlaylistBase::getDetail($itemId, VtPlaylistBase::TYPE_FILM);
                if (!$objPlaylist) {
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => Yii::t('wap',"Nội dung không hợp lệ")
                    ];
                }
                $fee = $objPlaylist['price_play'];
                $source = Yii::$app->params['app.source'];
                $errorCode = VtService::paymentService(VtService::PAYMENT_ACTION_BUY . $type, $this->msisdn, $fee, $itemId, $type, $source);
                if ($errorCode === "0") {
                    $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Mua lẻ thành công"));
                    $objBuy = new VtBuyPlaylistBase();
                    $objBuy->insertTransaction($this->msisdn, $this->userId, $itemId);
                    return [
                        'responseCode' => ResponseCode::SUCCESS,
                        'message' => $msgConfig
                    ];
                } else {
                    $msgConfig = VtConfigBase::getConfig("gateway_errorcode_msg_" . $errorCode, Yii::t('web',"Mua lẻ không thành công"));
                    return [
                        'responseCode' => ResponseCode::UNSUCCESS,
                        'message' => $msgConfig
                    ];
                }

                break;
            default:
                // Khong xu ly cac case khong khai bao
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' =>Yii::t('web', "Không hợp lệ")
                ];
        }
    }

    /**
     * @author PHUMX
     * Lay danh sach thanh vien
     * @return array
     */
    public function actionGetListMember()
    {

        $dataResponse[] = UserObj::serialize(
            Obj::MEMBER,
            VtUserBase::getListMember($this->userId, Yii::$app->params['app.page.limit'], 0),
            false
        );

        $dataResponse[] = UserObj::serialize(
            Obj::MEMBER_FOLLOW,
            VtUserFollowBase::getFollowUser($this->userId),
            false
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];

    }

    /**
     *Api upload video
     * @return array
     */
    public function actionUploadFile()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Quý khách vui lòng đăng ký thành viên để sử dụng tính năng này!'),
                ];
            } else {
                return $this->authJson;
            }
        }
        // Specify the input name set in the javascript.
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {
            set_time_limit(1200);
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($ext), ['mp4', 'flv', '.mp4', '.flv', 'mov', '.mov', 'm4u', '.m4u'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Định dạng file upload không đúng")
                ];
            }
            if ($_FILES['file']['size'] > 838860800) { //800 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('web',"Kích thước tối đa 800MB")
                ];
            }

            $fileBucket = Yii::$app->params['s3']['video.bucket'];
            $filePath = Utils::generatePath($ext);

            $videoName = trim(Yii::$app->request->post("title"));
            $description = trim(Yii::$app->request->post("description"));
            $categoryId = Yii::$app->request->post("category");

            if (mb_strlen($videoName, 'UTF-8') > 255 || mb_strlen($videoName) <= 0) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('web',"Tên video phải nhập và số kí tự không lớn hơn 255 kí tự"),
                ];
            }

            if (mb_strlen($description, 'UTF-8') > 500) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('web',"Mô tả video không được lớn hơn 500 kí tự"),
                ];
            }

            if (!VtGroupCategoryBase::checkCategory($categoryId, 'VOD')) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('web',"Thể loại không hợp lệ")
                ];
            }

            $arrS3 = S3Service::putObject($fileBucket, $_FILES['file']['tmp_name'], $filePath);
            Yii::info("S3 PUSH FILE=" . json_encode($arrS3) . "|" . $_FILES['file']['tmp_name'] . "|" . $filePath);

            if ($arrS3['errorCode'] == S3Service::UNSUCCESS) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('web',"Upload file cloud không thành công. Vui lòng thử lại!")
                ];
            }

            $type = $_POST['type'];
            $playlistId = $_POST['playlist_id'];
            //@todo: validate type
            $video = new VtVideo();
            $video->insertVideo($videoName, $description, $fileBucket, $filePath, VtVideoBase::TYPE_VOD, null, $this->userId, $categoryId, VtVideoBase::CONVERT_STATUS_DRAFT);

            // Luu action log
            //$actionLog = "(" . $this->userId . ")" . "[" . $this->msisdn . "]" . $video->name;
            //VtActionLogBase::insertLog(VtActionLogBase::M_API_VIDEO, $video->id, VtActionLogBase::T_CREATE, $actionLog, $this->userId, "API");

            if (!empty($playlistId)) {
                $playlistItem = new VtPlaylistItem();
                $playlistItem->insertItem($video->id, $playlistId);
            }
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap',"Đăng tải thành công"),
                'data' => [
                    "videoId" => $video->id
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Method Not Allowed")
            ];
        }
    }

    /**
     * API upload anh cho video
     * @return array
     */
    public function actionUploadImageFile()
    {

        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' =>Yii::t('wap', 'Quý khách vui lòng đăng ký thành viên để sử dụng tính năng này!'),
                ];
            } else {
                return $this->authJson;
            }
        }

        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {
            $videoId = Yii::$app->request->post("video_id");
            if (!VtVideoBase::checkCustomerPermissionEditVideo($this->userId, $videoId)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Không có quyền truy cập")
                ];
            };
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Định dạng file upload không đúng"),
                ];
            }
            if ($_FILES['file']['size'] > 20971520) { //20 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Kích thước tối đa 20MB")
                ];
            }
            $fileBucket = Yii::$app->params['s3']['static.bucket'];
            $filePath = Utils::generatePath($ext);
            $fileName = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
            S3Service::putObject($fileBucket, $_FILES['file']['tmp_name'], $filePath);
            VtHelper::generateAllThumb($fileBucket, $_FILES['file']['tmp_name'], $filePath);

            $video = VtVideo::findOneById($videoId);
            $video->bucket = $fileBucket;
            $video->path = $filePath;
            $video->updated_at = date('Y-m-d H:i:s');
            $video->save(false);

            // Luu action log
            //$actionLog = "(" . $this->userId . ")" . "[" . $this->msisdn . "]" . $video->name . "|UPLOAD IMAGE";
            //VtActionLogBase::insertLog(VtActionLogBase::M_API_VIDEO, $video->id, VtActionLogBase::T_CREATE, $actionLog, $this->userId, "API");

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap',"Đăng tải thành công"),
                'videoId' => $videoId
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Method Not Allowed")
            ];
        }

    }

    /**
     * Map tai khoan google, facebook voi msisdn
     * @return array
     */
    public function actionMapAccount()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $msisdn = trim(Yii::$app->request->get('msisdn', 0));
        $otp = trim(Yii::$app->request->get('otp', 0));

        $osType = trim(Yii::$app->request->get('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', ''));

        if (!$msisdn || !$otp) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Vui lòng nhập số thuê bao và mã xác thực')
            ];
        }

        $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);

        if ($this->msisdn == $msisdn) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Số thuê bao của quý khách đã được liên kết với tài khoản này!')
            ];
        }

        //Kiem tra xem khach hang da lay ma OTP hay chua?
        if (!VtUserOtpBase::hasActiveOTP($msisdn)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Qúy khách vui lòng lấy mã xác thực để thực hiện liên kết tài khoản')
            ];
        }

        // Kiem tra OTP
        if (VtUserOtpBase::checkOTP($msisdn, $otp)) {
            $msisdnSource = '';
            $userIdTarget = '';

            $objUser = VtUserBase::getById($this->userId);

            // Kiem tra xem so thue bao da nam trong tai khoan nao khac chua
            // Neu so thue bao can map da nam trong tai khoan khac thif update social_id tu tai khoan hienj tai sang tai khoan moi
            $objMsisdn = VtUserBase::getByMsisdn($msisdn);
            if ($objMsisdn) {

                //Neu chua co full name thi map
                if (!$objMsisdn->full_name) {
                    $objMsisdn->full_name = $objUser->full_name;
                    $objMsisdn->full_name_slug = Utils::removeSignOnly($objUser->full_name);
                }

                if (!$objMsisdn->email) {
                    $objMsisdn->email = $objUser->email;
                }

                if ($this->loginVia == 'GOOGLE') {
                    $objMsisdn->google_oauth_id = $objUser->google_oauth_id;
                } else {
                    $objMsisdn->oauth_id = $objUser->oauth_id;
                }

                $objMsisdn->save(false);

                // Xoa so thue bao dang ton tai khoi vt_user va ha user cu xuong
                if ($this->loginVia == 'GOOGLE') {
                    //Neu googleid = facebookid thi xoa ca 2
                    if ($objUser->google_oauth_id == $objUser->oauth_id) {
                        $objUser->oauth_id = '';
                    }
                    $objUser->google_oauth_id = '';
                } else {
                    //Neu googleid = facebookid thi xoa ca 2
                    if ($objUser->google_oauth_id == $objUser->oauth_id) {
                        $objUser->google_oauth_id = '';
                    }
                    $objUser->oauth_id = '';
                }

                $objUser->save(false);

                $this->msisdn = $msisdn;
                //cap nhat msisdn cho token
                $objToken = VtUserTokenBase::getByUserId($objMsisdn->id);
                $accessToken = '';

                if ($objToken) {
                    $objToken->msisdn = $msisdn;
                    $objToken->save(false);
                    $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';
                    $accessTokenExpiredTime = time() + Yii::$app->params['accessToken.timeout'];
                    $userAgentEncrypted = md5(Yii::$app->params['userAgent.secretCode'] . $userAgent);
                    $secretString = "$accessTokenExpiredTime&$objToken->user_id&$msisdn&$userAgentEncrypted";
                    $accessToken = Des::RSAEncrypt($secretString);
                } else {
                    $accessTokenExpiredTime = time() + Yii::$app->params['accessToken.timeout'];
                    $refreshTokenExpiredTime = time() + Yii::$app->params['refreshToken.timeout'];
                    $refreshToken = Utils::generateGuid();
                    $refeshTokenExpiredTimeStamp = date("Y-m-d H:i:s", $refreshTokenExpiredTime);
                    $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';
                    $userAgentEncrypted = md5(Yii::$app->params['userAgent.secretCode'] . $userAgent);

                    $ip = VtHelper::getAgentIp();
                    $userToken = new VtUserTokenBase();
                    $userToken->insertUserToken($refreshToken, $ip, $objMsisdn->id, $msisdn, $refeshTokenExpiredTimeStamp, 0, $userAgent);

                    $secretString = "$accessTokenExpiredTime&$objMsisdn->id&$msisdn&$userAgentEncrypted";

                    //Kiem tra xem co can update APP hay khong
                    if ($osType && $osVersionCode) {
                        $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                        $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                        $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
                        //Kiem tra xem co can hidden noi dung tinh phi IOS
                        if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                            $secretString = $secretString . "&1";
                        } else {
                            //Khong can hidden noi dung tinh phi duyet APP IOS
                            $secretString = $secretString . "&0";
                        }
                    } else {
                        //Khong can hidden noi dung tinh phi duyet APP IOS
                        $secretString = $secretString . "&0";
                    }
                    // Them tham so google, facebook neu dang nhap qua MXH
                    $secretString = $secretString . "&" . $this->loginVia;

                    $accessToken = Des::RSAEncrypt($secretString);
                }

                LogMapAccountBase::insertLog($msisdn, $this->userId, $msisdn, $objMsisdn->id, "Ton tai msisdn " . $msisdn . ' voi id=' . $objMsisdn->id . "Thuc hien update oauth_id=" . $objUser->oauth_id . " sang tai khoan cu " . $this->userId . ", va xoa oauth_id");

                $isShowPromotionAPP = 0;
                if ($this->msisdn) {
                    //Kiem tra xem khach hang da dang ky goi cuoc hay chua? Neu da tung dang ky goi cuoc thi khong duoc KM
                    $isShowPromotionAPP = VtSubBase::checkPromotionFirstTimeUsageAPP($this->msisdn);
                    $configPromotionInstallAPP = VtConfigBase::getConfig('is.on.promotion.intall.app', 0);
                    $isShowPromotionAPP = $isShowPromotionAPP & $configPromotionInstallAPP;
                }

                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap','Liên kết tài khoản thành công'),
                    'data' => [
                        'newAccessToken' => $accessToken,
                        'isShowPromotionAPP' => $isShowPromotionAPP
                    ]
                ];

            } else {
                //Neu chua ton tai so thue bao

                LogMapAccountBase::insertLog($msisdn, 0, $objUser->msisdn, $objUser->id, "So thue bao " . $msisdn . " chua ton tai trong tai khoan nao, thuc hien map moi.");

                $objUser->msisdn = $msisdn;
                $objUser->save(false);
                $this->msisdn = $msisdn;
                //cap nhat msisdn cho token
                $objToken = VtUserTokenBase::getByUserId($this->userId);
                $accessToken = '';

                if ($objToken) {
                    $objToken->msisdn = $msisdn;
                    $objToken->save(false);
                    $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';
                    $accessTokenExpiredTime = time() + Yii::$app->params['accessToken.timeout'];
                    $userAgentEncrypted = md5(Yii::$app->params['userAgent.secretCode'] . $userAgent);
                    $secretString = "$accessTokenExpiredTime&$objToken->user_id&$objToken->msisdn&$userAgentEncrypted";

                    //Kiem tra xem co can update APP hay khong
                    if ($osType && $osVersionCode) {
                        $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                        $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                        $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
                        //Kiem tra xem co can hidden noi dung tinh phi IOS
                        if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                            $secretString = $secretString . "&1";
                        } else {
                            //Khong can hidden noi dung tinh phi duyet APP IOS
                            $secretString = $secretString . "&0";
                        }
                    } else {
                        //Khong can hidden noi dung tinh phi duyet APP IOS
                        $secretString = $secretString . "&0";
                    }
                    // Them tham so google, facebook neu dang nhap qua MXH
                    $secretString = $secretString . "&" . $this->loginVia;

                    $accessToken = Des::RSAEncrypt($secretString);
                }
                $isShowPromotionAPP = 0;
                if ($this->msisdn) {
                    //Kiem tra xem khach hang da dang ky goi cuoc hay chua? Neu da tung dang ky goi cuoc thi khong duoc KM
                    $isShowPromotionAPP = VtSubBase::checkPromotionFirstTimeUsageAPP($this->msisdn);
                    $configPromotionInstallAPP = VtConfigBase::getConfig('is.on.promotion.intall.app', 0);
                    $isShowPromotionAPP = $isShowPromotionAPP & $configPromotionInstallAPP;
                }
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap','Liên kết tài khoản thành công'),
                    'data' => [
                        'newAccessToken' => $accessToken,
                        'isShowPromotionAPP' => $isShowPromotionAPP
                    ]
                ];
            }

        } else {
            $count = intval(Yii::$app->getCache()->get("OTP_FAIL_COUNT_" . $msisdn));
            $count++;
            Yii::$app->getCache()->set("OTP_FAIL_COUNT_" . $msisdn, $count, MobiTVRedisCache::CACHE_15MINUTE);
            // HUY OTP khi khach hang nhap sai qua 3 lan
            if ($count >= Yii::$app->params['otp']['failToDestroy']) {
                VtUserOtpBase::deactiveAllOtp($msisdn);
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Quý khách vui lòng lấy lại OTP')
                ];
            }
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Mã xác thực không đúng')
            ];

        }
    }

    public function actionUploadAvatar()
    {

        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Quý khách vui lòng đăng ký thành viên để sử dụng tính năng này!'),
                ];
            } else {
                return $this->authJson;
            }
        }

        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {

            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Định dạng file upload không đúng'),
                ];
            }
            if ($_FILES['file']['size'] > 20971520) { //20 MB (size is also in bytes)
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Kích thước tối đa 20MB')
                ];
            }
            $fileBucket = Yii::$app->params['s3']['static.bucket'];
            $filePath = Utils::generatePath($ext);
            S3Service::putObject($fileBucket, $_FILES['file']['tmp_name'], $filePath);
            VtHelper::generateAllThumb($fileBucket, $_FILES['file']['tmp_name'], $filePath);

            $objUser = VtUserBase::getById($this->userId);
            $objUser->bucket = $fileBucket;
            $objUser->path = $filePath;
            $objUser->save(false);

            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Upload ảnh đại diện thành công'),
                'data' => [
                    'avatarImage' => (!empty($objUser)) ? VtHelper::getThumbUrl($objUser->bucket, $objUser->path, VtHelper::SIZE_AVATAR) : '',
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Method Not Allowed")
            ];
        }

    }

    public function actionCheckLinkSocial()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $user = VtUserBase::getById($this->userId);
        if($user){
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công'),
                'data' => [
                    'facebook_id' => ($user->oauth_id) ? $user->oauth_id : 0,
                    'google_id' => ($user->google_oauth_id) ? $user->google_oauth_id : 0
                ]
            ];
        }else{
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Lấy thông tin thất bại')
            ];
        }

    }

    public function actionRemoveLinkSocial()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        $type = Yii::$app->request->get('type');
        $user = VtUserBase::getById($this->userId);

        if ($user) {
            switch ( strtoupper($type) ) {
                case 'GOOGLE':
                    $user->google_oauth_id = '';
                    break;
                case 'FACEBOOK':
                    $user->oauth_id = '';
                    break;
            }
            $user->save(false);
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap','Thành công'),
            ];
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap','Không tìm thấy tài khoản'),
            ];
        }
    }

}