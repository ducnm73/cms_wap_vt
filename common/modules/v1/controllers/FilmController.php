<?php
namespace common\modules\v1\controllers;


use common\models\VtConfigBase;
use common\models\VtHistoryViewBase;
use common\models\VtVideoBase;
use Yii;

use common\modules\v1\libs\Obj;
use common\controllers\ApiController;
use api\models\VtPlaylist;
use common\modules\v1\libs\BannerObj;
use common\models\VtSlideshowBase;
use common\helpers\Utils;
use common\models\VtPlaylistBase;
use common\modules\v1\libs\PlaylistObj;
use common\modules\v1\libs\ResponseCode;
use common\models\VtGroupCategoryBase;

class FilmController extends ApiController
{

    public function actionGetHomeFilm()
    {
        //Dung cho duyet APP iOS
        $hiddenPackage = false;
        $osType = trim(Yii::$app->request->get('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', ''));
        Yii::info("TRACK HIDDEN HOME FILM| osType=" . $osType . "|osVersionCode=" . $osVersionCode);
        //Kiem tra xem co can update APP hay khong
        if ($osType && $osVersionCode) {
            //Kiem tra xem co can hidden noi dung tinh phi IOS
            if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                $hiddenPackage = true;
            }
        }
        // Neu can hidden noi dung de duyet app iOS
        if ($this->needHiddenFreemiumContent || $hiddenPackage) {
            Yii::info("TRACK | HIDDEN HOME FILM");
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' =>  Yii::t('web', 'Thành công'),
                'data' => []
            ];
        }


        $dataResponse = array();
        $banners = false;
        // Lay phim da xem
        if ($this->userId || $this->msisdn) {
            $filmHistory = VtHistoryViewBase::getByContentType($this->userId, $this->msisdn, 'FILM', 100);

            $itemIds = array();
            $parentIds = array();
            foreach ($filmHistory as $history) {
                $parentIds[$history['_source']['item_id']] = $history['_source']['parent_id'];
                $itemIds[] = $history['_source']['item_id'];
            }

            // Lay limit theo page.limit
            if (!empty($itemIds)) {
                $banners = BannerObj::serialize(
                    Obj::BANNER,
                    VtVideoBase::getFilmByIdsQuery($itemIds, Yii::$app->params['app.slideshow.limit']),
                    true,
                    VtSlideshowBase::LOCATION_FILM,
                    $parentIds
                );
            }
        }

        if (!$banners) {
            $banners = BannerObj::serialize(
                Obj::BANNER,
                VtSlideshowBase::getSlideShowQuery(VtSlideshowBase::LOCATION_FILM, Yii::$app->params['app.slideshow.limit']),
                true,
                VtSlideshowBase::LOCATION_FILM
            );
        }

        $boxFilmNew = PlaylistObj::serialize(
            VtPlaylist::TYPE_FILM,
            Obj::FILM_NEW,
            VtPlaylistBase::getPlayListNewByType(VtPlaylistBase::TYPE_FILM, Yii::$app->params['app.page.film.limit'], 0),
            true
        );

        $boxFilmHot = PlaylistObj::serialize(
            VtPlaylist::TYPE_FILM,
            Obj::FILM_HOT,
            VtPlaylistBase::getPlayListHot(VtPlaylistBase::TYPE_FILM, Yii::$app->params['app.page.film.limit'], 0),
            true
        );

        $boxFilmSuggest = PlaylistObj::serialize(
            VtPlaylist::TYPE_FILM,
            Obj::FILM_RECOMMEND,
            VtPlaylistBase::getPlayListRecommend(VtPlaylistBase::TYPE_FILM, Yii::$app->params['app.page.film.limit'], 0),
            true
        );

        if ($banners['content']) {
            $dataResponse[] = $banners;
        }
        if ($boxFilmNew['content']) {
            $dataResponse[] = $boxFilmNew;
        }
        if ($boxFilmHot['content']) {
            $dataResponse[] = $boxFilmHot;
        }
        if ($boxFilmSuggest['content']) {
            $dataResponse[] = $boxFilmSuggest;
        }

        //@todo hardcode off cache
        $cfArrVodSubType = Utils::getSystemConfig('film.category.list', true);
        $arrVodSubType = explode(",", $cfArrVodSubType);
        foreach ($arrVodSubType as $categoryId) {
            $objCategory = VtGroupCategoryBase::getCategoryGroup($categoryId, VtGroupCategoryBase::TYPE_FILM);
            if ($objCategory) {
                $tmpBox = PlaylistObj::serialize(
                    VtPlaylistBase::TYPE_FILM,
                    Obj::CATEGORY_GROUP . $categoryId,
                    VtPlaylistBase::getPlayListByCategory($categoryId, Yii::$app->params['app.page.film.limit'], 0),
                    true,
                    $objCategory['name']
                );
                if (count($tmpBox['content'])) {
                    $dataResponse[] = $tmpBox;
                }
            }
        }

        /*
        $boxFilmFree = PlaylistObj::serialize(
            VtPlaylist::TYPE_FILM,
            Obj::FILM_FREE,
            VtPlaylistBase::getPlayListFree(VtPlaylistBase::TYPE_FILM, Yii::$app->params['app.page.film.limit'], 0),
            true
        );
        if ($boxFilmFree['content']) {
            $dataResponse[] = $boxFilmFree;
        }
        */

        //@todo hardcode off cache
        //- IF CONFIG ORDER BOX
        $orderStr = Utils::getSystemConfig('film.order.list', true);

        if ($orderStr) {
            $tmpBox = array();
            $orderList = explode(",", $orderStr);
            foreach ($orderList as $index) {
                if (isset($dataResponse[$index])) {
                    $tmpBox[] = $dataResponse[$index];
                }
            }
            $dataResponse = $tmpBox;
        }

        $response = [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];

        return $response;
    }


}