<?php

namespace common\modules\v1\controllers;

use common\libs\VtFlow;
use common\models\VtChannelBase;
use common\models\VtFavouriteVideoBase;
use common\models\VtFeedBackBase;
use common\models\VtGroupTopicBase;
use common\models\VtPlaylistTopicBase;
use common\models\VtUserSearchBase;
use common\models\VtVideoTopicBase;
use common\modules\v1\libs\UserFollowObj;
use common\modules\v1\libs\UserObj;
use cp\models\VtConfig;
use Yii;
use common\libs\VtHelper;
use common\models\VtPackageBase;
use common\models\VtPlaylistSearchBase;
use common\models\VtSubBase;
use common\models\VtUserFollowBase;
use common\models\VtUserTopicBase;
use common\models\VtVideoSearchBase;
use common\modules\v1\libs\FocusObj;
use wap\models\VtPackage;
use common\modules\v1\libs\Obj;
use common\modules\v1\libs\VideoObj;
use common\helpers\Utils;
use common\libs\Search;
use common\models\VtCommentBase;
use common\models\VtCommentLikeBase;
use common\models\VtConfigBase;
use common\models\VtPlaylistBase;
use common\models\VtUserBase;
use common\models\VtVideoBase;
use common\modules\v1\libs\PlaylistObj;
use common\modules\v1\libs\ResponseCode;
use common\controllers\ApiController;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\VtSlideshowBase;
use common\modules\v1\libs\BannerObj;
use common\models\VtGroupCategoryBase;
use common\models\VtHistoryViewBase;

class DefaultController extends ApiController
{

    public function actionGetHome()
    {
        $dataResponse = array();

        $banners = BannerObj::serialize(
            Obj::BANNER, VtSlideshowBase::getSlideShowQuery(VtSlideshowBase::LOCATION_HOME, Yii::$app->params['app.slideshow.limit']), true, VtSlideshowBase::LOCATION_HOME
        );

        if ($this->msisdn || $this->userId) {
            $focus = FocusObj::serialize(
                Obj::FOCUS, VtUserTopicBase::getTopicLiked($this->userId, $this->msisdn, Yii::$app->params['app.focus.limit']), VtUserFollowBase::getFollowUser($this->userId, Yii::$app->params['app.focus.limit']), false
            );
        }

        $newFeed = VideoObj::serialize(
            Obj::VIDEO_NEWSFEED, VtVideoBase::getHotVideo(Yii::$app->params['app.home.limit']), true, false, 'NEWSFEED'
        );

        if (isset($banners['content']) && $banners['content']) {
            $dataResponse[] = $banners;
        }

        if (isset($focus['content']) && $focus['content']) {
            $dataResponse[] = $focus;
        }

        if (isset($newFeed['content']) && $newFeed['content']) {
            $dataResponse[] = $newFeed;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse,
            'popup' => VtFlow::loadPromotionPopup($this->msisdn)
        ];
    }
    /**
     * Ham lay danh sach video bao gom binh luan cua video
     * @author manh@viettel.com.vn
     */
    public function actionGetListVideoComment()
    {

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $userId= $this->userId;
        $type = trim(Yii::$app->request->get('type', 'VOD'));

        // $contentId = trim(Yii::$app->request->get('content_id', 0));
        $limit = trim(Yii::$app->request->get('limit', 10));
        $offset = trim(Yii::$app->request->get('offset', 0));
        $status = trim(Yii::$app->request->get('status',4));
        $videoId = trim(Yii::$app->request->get('videoId',0));
        $commentId = trim(Yii::$app->request->get('commentId',0));

        if (!empty($status)) {
            if (!in_array($status, [VtVideoBase::STATUS_DRAFT, VtVideoBase::STATUS_APPROVE,4])) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap',"Trạng thái không hợp lệ")
                ];
            }
        }
        if ($limit <= 0) {
            $limit =10;
        }

        // danh sach video cua user
        if(isset($videoId) && $videoId != 0){
            $videoComment = VtVideoBase::find()->asArray()->where(['id' => $videoId])->all();
            if (!$videoComment) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
                ];
            }else{
                $listvideos = $videoComment;
            }
        }else{
            $listvideosAll = VtVideoBase::getAllVideosCommentByUser('', $this->userId,'','',VtVideoBase::TYPE_VOD)->all();

            $items = [];
            $count = 0;

            if (!empty($listvideosAll)) {
                foreach ($listvideosAll as $key => $video) {

                    $arrComment = VtCommentBase::getNotifyByUserId($this->userId,$video['id'], '', '',$status);
                    if(count($arrComment) > 0){
                        $item = [];
                        $item['id'] = $video['id'];
                        $item['sent_time'] = date("Y-m-d H:i:s", strtotime($arrComment[0]['created_at']));
                        $item['video_id'] = $video['id'];
                        $item['name'] = $video['name'];
                        $item['slug'] = $video['slug'];
                        $item['published_time'] = $video['published_time'];
                        $item['play_times'] = $video['play_times'];
                        $item['comment_count'] = $video['comment_count'];
                        $item['channel_id'] = $video['channel_id'];
                        $item['user_id'] = $video['user_id'];
                        $item['bucket'] = $video['bucket'];
                        $item['path'] = $video['path'];
                        $item['can_comment'] = $video['can_comment'];
                        $items[] = $item;

                        $count++;
                    }
                }
            }
            usort($items, function($a, $b) {
                return ($a['sent_time'] > $b['sent_time']) ? -1 : 1;
            });
            $items = array_slice($items, $offset, $limit);
            $listvideos = $items;
        }

        $listvideos_news = [];
        if (!empty($listvideos)){
            foreach ($listvideos as $key => $video) {
                $new_video = array();
                $new_video['id'] = $video['id'];
                $new_video['name'] = $video['name'];
                $new_video['slug'] = $video['slug'];
                $new_video['published_time'] = $video['published_time'];
                $new_video['play_times'] = $video['play_times'];
                $new_video['comment_count'] = $video['comment_count'];
                $new_video['channel_id'] = $video['channel_id'];
                $new_video['user_id'] = $video['user_id'];
                $new_video['can_comment'] = $video['can_comment'];
                $new_video['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_VIDEO);

                $parentComments = VtCommentBase::getAllByUserId($video['id'], '', '', $status, $commentId);

                $parentIds = array();
                foreach ($parentComments as $parentComment) {
                    $parentIds[] = $parentComment['id'];
                }

                $childComments = VtCommentBase::getByContentId($this->userId, $video['id'], null, null, $parentIds, $status);

                foreach ($parentComments as $key => $parentComment) {
                    $parentIds[] = $parentComment['id'];
                    $commentLike = VtCommentLikeBase::getLike($this->userId, $parentComment['id'],null);

                    if(count($commentLike) == 0){
                        $parentComments[$key]['like'] = 0;
                    }else{
                        $parentComments[$key]['like'] = $commentLike['like'];
                    }
                    if($commentLike['like'] == 1){
                        $parentComments[$key]['is_like'] = true;
                    }
                    if($commentLike['like'] == 2){
                        $parentComments[$key]['isDislike'] = true;
                    }

                    $vtUser = VtUserBase::getById($parentComment['user_id']);
                    //@todo: xu ly truong hop khong co full_name
                    $parentComments[$key]['full_name'] = ($vtUser['full_name']) ? Utils::truncateWords($vtUser['full_name'], 20) : (substr($vtUser['msisdn'], 0, -3) . "xxx");
                    $parentComments[$key]['avatarImage'] = VtHelper::getThumbUrl($vtUser['bucket'], $vtUser['path'], VtHelper::SIZE_AVATAR);
                    $parentComments[$key]['coverImage'] = VtHelper::getThumbUrl($vtUser['channel_bucket'], $vtUser['channel_path'], VtHelper::SIZE_COVER);
                    $parentComments[$key]['created_at_format'] = Utils::time_elapsed_string($parentComments[$key]['created_at']);
                    $parentComments[$key]['status'] = $parentComments[$key]['status'];
                    $parentComments[$key]['like_count'] = $parentComments[$key]['like_count'];
                    $parentComments[$key]['dislike_count'] = $parentComments[$key]['dislike_count'];

                    $childNum = 0;
                    foreach ($childComments as $ckey => $childComment) {
                        if ($parentComment['id'] == $childComment['parent_id']) {
                            $childNum++;
                            $childCommentLike = VtCommentLikeBase::getLike($this->userId, $childComment['id'],null);
                            if(count($childCommentLike) == 0){
                                $childComments[$ckey]['like'] = 0;
                            }else{
                                $childComments[$ckey]['like'] = $childCommentLike['like'];
                            }
                            if($childCommentLike['like'] == 1){
                                $childComments[$ckey]['is_like'] = true;
                            }
                            if($childCommentLike['like'] == 2){
                                $childComments[$ckey]['isDislike'] = true;
                            }

                            $childComments[$ckey]['status'] = $childComment['status'];
                            $vtUser_comment = VtUserBase::getById($childComment['user_id']);
                            //@todo: xu ly truong hop khong co full_name
                            $childComments[$ckey]['full_name'] = ($vtUser_comment['full_name']) ? Utils::truncateWords($vtUser_comment['full_name'], 20) : (substr($vtUser_comment['msisdn'], 0, -3) . "xxx");
                            $childComments[$ckey]['avatarImage'] = VtHelper::getThumbUrl($vtUser_comment['bucket'], $vtUser_comment['path'], VtHelper::SIZE_AVATAR);
                            $childComments[$ckey]['coverImage'] = VtHelper::getThumbUrl($vtUser_comment['channel_bucket'], $vtUser_comment['channel_path'], VtHelper::SIZE_COVER);
                            $childComments[$ckey]['created_at_format'] = Utils::time_elapsed_string($childComments[$ckey]['created_at']);
                            $childComments[$ckey]['status'] = $childComment['status'];
                            $childComments[$key]['like_count'] = $childComments[$key]['like_count'];
                            $childComments[$key]['dislike_count'] = $childComments[$key]['dislike_count'];
                            $parentComments[$key]['children'][] = $childComments[$ckey];
                        }
                    }
                    $parentComments[$key]['comment_count'] = $childNum;
                }

                $new_video['list_comment'] = $parentComments;
                if($parentComments){
                    $new_ar[]= $new_video;
                }
            }
            $listvideos_news = $new_ar;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $listvideos_news
        ];
    }
    /**
     * Ham lay danh sach binh luan
     * @author ducda2@viettel.com.vn
     */
    public function actionGetListComment()
    {

        $contentId = trim(Yii::$app->request->get('content_id', 0));
        $limit = trim(Yii::$app->request->get('limit', 10));
        //$status = trim(Yii::$app->request->get('status', ''));
        if (empty($limit)) {
            $limit = 10;
        }

        $offset = trim(Yii::$app->request->get('offset', 0));
        $commentId = trim(Yii::$app->request->get('comment_id', 0));

        if (empty($contentId)) {

            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Dữ liệu không hợp lệ'),
            ];
        }
        $video = VtVideoBase::getDetail($contentId);

        if (!empty($commentId)) {
            $parentComments = VtCommentBase::getByContentId($this->userId, $contentId, $limit, $offset, $commentId);
            foreach ($parentComments as $key => $parentComment) {
                $parentIds[] = $parentComment['id'];
                $commentLike = VtCommentLikeBase::getLike($this->userId, $commentId,'');
                $parentComments[$key]['like'] = $commentLike['like']?$commentLike['like']:0;
                
                if($commentLike['like'] == 1){
                    $parentComments[$key]['is_like']  = true;
                }
                if($commentLike['like'] == 2){
                    $parentComments[$key]['isDislike']  = true;
                }
            }
        } else {
            $parentComments = VtCommentBase::getByContentId($this->userId, $contentId, $limit, $offset, null);
            $parentIds = array();
            foreach ($parentComments as $parentComment) {
                $parentIds[] = $parentComment['id'];
            }

            $childComments = VtCommentBase::getByContentId($this->userId, $contentId, null, null, $parentIds);
            foreach ($parentComments as $key => $parentComment) {
                $parentIds[] = $parentComment['id'];

                $commentLike = VtCommentLikeBase::getLike($this->userId, $parentComment['id'],'');

                    $parentComments[$key]['like'] = $commentLike['like']?$commentLike['like']:0;
                
                $parentComments[$key]['is_comment'] = $video['can_comment'];
                if($commentLike['like'] == 1){
                    $parentComments[$key]['is_like']  = true;
                }
                if($commentLike['like'] == 2){
                    $parentComments[$key]['isDislike']  = true;
                }
                //@todo: xu ly truong hop khong co full_name
                $parentComments[$key]['full_name'] = ($parentComments[$key]['full_name']) ? Utils::truncateWords($parentComments[$key]['full_name'], 20) : (substr($parentComments[$key]['msisdn'], 0, -3) . "xxx");
                $parentComments[$key]['avatarImage'] = VtHelper::getThumbUrl($parentComments[$key]['bucket'], $parentComments[$key]['path'], VtHelper::SIZE_AVATAR);
                $parentComments[$key]['coverImage'] = VtHelper::getThumbUrl($parentComments[$key]['channel_bucket'], $parentComments[$key]['channel_path'], VtHelper::SIZE_COVER);
                $parentComments[$key]['created_at_format'] = Utils::time_elapsed_string($parentComments[$key]['created_at']);

                $childNum = 0;
                foreach ($childComments as $ckey => $childComment) {
                    if ($parentComment['id'] == $childComment['parent_id']) {
                        $childNum++;

                        $childCommentLike = VtCommentLikeBase::getLike($this->userId, $childComment['id'],'');
                        
                       
                       $childComments[$ckey]['like'] = $childCommentLike['like']?$childCommentLike['like']:0;
                        
                        if($childCommentLike['like'] == 1){
                            $childComments[$ckey]['is_like']  = true;
                        }
                        if($childCommentLike['like'] == 2){
                            $childComments[$ckey]['isDislike']  = true;
                        }
                        //@todo: xu ly truong hop khong co full_name
                        $childComments[$ckey]['full_name'] = ($childComments[$ckey]['full_name']) ? Utils::truncateWords($childComments[$ckey]['full_name'], 20) : (substr($childComments[$ckey]['msisdn'], 0, -3) . "xxx");
                        $childComments[$ckey]['avatarImage'] = VtHelper::getThumbUrl($childComments[$ckey]['bucket'], $childComments[$ckey]['path'], VtHelper::SIZE_AVATAR);
                        $childComments[$ckey]['coverImage'] = VtHelper::getThumbUrl($childComments[$ckey]['channel_bucket'], $childComments[$ckey]['channel_path'], VtHelper::SIZE_COVER);
                        $childComments[$ckey]['created_at_format'] = Utils::time_elapsed_string($childComments[$ckey]['created_at']);
                        $childComments[$ckey]['is_comment'] = $video['can_comment'];
                        $parentComments[$key]['children'][] = $childComments[$ckey];
                    }
                }
                $parentComments[$key]['comment_count'] = $childNum;
            }
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $parentComments
        ];
    }

    /**
     * Ham gui binh luan
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionPostComment()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Quý khách vui lòng đăng ký thành viên để sử dụng các tính năng dịch vụ'),
                ];
            } else {
                return $this->authJson;
            }
        }
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method != "POST") {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => "Method Not Allowed"
            ];
        }

        $contentId = trim(Yii::$app->request->post('content_id', 0));
        $parentId = trim(Yii::$app->request->post('parent_id', 0));
        $comment = trim(Yii::$app->request->post('comment', ''));

        if (empty($comment)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Dữ liệu không hợp lệ')
            ];
        }

        if (strlen(utf8_decode($comment)) < Yii::$app->params['comment.minlength'] || strlen(utf8_decode($comment)) > Yii::$app->params['comment.maxlength']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Quý khách vui lòng nhập bình luận từ ') . Yii::$app->params['comment.minlength'] . Yii::t('wap', ' đến ') . Yii::$app->params['comment.maxlength'] . Yii::t('wap', ' ký tự.')
            ];
        }

        if (empty($contentId) || (!empty($parentId) && !is_numeric($parentId))) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Thông tin bình luân không hợp lệ. Vui lòng kiểm tra lỗi.')
            ];
        }

        $content = '';
        $type = 'VOD';
        switch ($type) {
            case 'VOD':
                $video = VtVideoBase::getDetail($contentId, $type);
                if ($video) {
                    $content = $video['name'];
                }
                break;
            case 'PLAYLIST':
                $playlist = VtPlaylistBase::getDetail($contentId);
                if ($playlist) {
                    $content = $playlist['name'];
                }
                break;
        }

        if (empty($content)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => Yii::t('wap', 'Nội dung bình luận không tồn tại')
            ];
        }

        $vtComment = new VtCommentBase();
        $listvideos = VtVideoBase::getAllVideosByUser( '', $this->userId, null, null)->all();
        foreach ($listvideos as $video){
            $arrIdVideo[] = $video['id'];
        }
        if(in_array($contentId,$arrIdVideo)){
            $status = 1;
            $vtComment->insertComment($contentId, $type, $this->userId, $comment, $content, $parentId,$status);
        }else{
            $status = 0;
            $vtComment->insertComment($contentId, $type, $this->userId, $comment, $content, $parentId,$status);
        }


        $vtUser = VtUserBase::getById($this->userId);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'id' => $vtComment->id,
                'type' => $vtComment->type,
                'full_name' => ($vtUser->full_name) ? $vtUser->full_name : $vtUser->msisdn,
                'user_id' => $vtUser->id,
                'avatarImage' => VtHelper::getThumbUrl($vtUser->bucket, $vtUser->path, VtHelper::SIZE_AVATAR),
                'comment' => $vtComment->comment,
                'like_count' => 0,
                'dislike_count' => 0,
                'parent_id' => $vtComment->parent_id,
                'created_at' => date('Y-m-d H:i:s'),
                'created_at_format' => Utils::time_elapsed_string($vtComment->created_at),
                "like" => 0,
                "content_id" => $contentId,
                "status" => $status,
                "is_comment" => $status
            ]
        ];
    }

    /**
     * Ham gui binh luan
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionToggleLikeComment()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Quý khách vui lòng đăng ký thành viên để sử dụng các tính năng dịch vụ'),
                ];
            } else {
                return $this->authJson;
            }
        }

        $type = trim(Yii::$app->request->post('type', ''));
        $commentId = trim(Yii::$app->request->post('comment_id', ''));
        $contentId = trim(Yii::$app->request->post('content_id', ''));

        if (empty($commentId) || empty($contentId) || !in_array(strtoupper($type), Yii::$app->params['object.type'])) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Thông tin không hợp lệ'),
            ];
        }

        $favObj = VtCommentLikeBase::getLike($this->userId, $commentId);

        if (!empty($favObj)) {
            $favObj->delete();
            $isLike = false;
        } else {
            $favObj = new VtCommentLikeBase();
            $favObj->insertLike($this->userId, $commentId, $contentId, $type);
            $isLike = true;
        }

        VtCommentBase::updateLikeCount($commentId, $isLike);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isLike' => $isLike
            ]
        ];
    }

    public function actionLikeDislikeComment()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Quý khách vui lòng đăng ký thành viên để sử dụng các tính năng dịch vụ'),
                ];
            } else {
                return $this->authJson;
            }
        }
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {
            $typelike = trim(Yii::$app->request->post('like', ''));
            $commentId = trim(Yii::$app->request->post('comment_id', ''));
            $contentId = trim(Yii::$app->request->post('content_id', ''));
            if (empty($commentId) || empty($contentId) || !in_array($typelike,array(1,2))||!preg_match("/^\d+\.?\d*$/", $commentId)||!preg_match("/^\d+\.?\d*$/", $contentId)) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Thông tin không hợp lệ'),
                ];
            }
            $video = VtVideoBase::findOneById($contentId);
            if (!$video) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
                ];
            }
            $comment = VtCommentBase::getById($commentId);
            if (!$comment) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => Yii::t('wap', 'Nội dung bình luận không tồn tại'),
                ];
            }

            $favObj = VtCommentLikeBase::getLike($this->userId, $commentId,$typelike);
            if ($favObj) {
                $favObj->delete();
                $typelike = 0;
            } else {
                // check dữ liệu xem đã co trường họp ngươc lại chưa neu co thi xoa di
                if($typelike==1){
                    $dislike = VtCommentLikeBase::getLike($this->userId, $commentId,2);
                    if($dislike){
                        $dislike->like = 1;
                        $dislike->save();
                    }else{
                        $newObj = new VtCommentLikeBase();
                        $newObj->user_id = $this->userId;
                        $newObj->comment_id = $commentId;
                        $newObj->content_id = $contentId;
                        $newObj->type = $video['type'];
                        $newObj->like = $typelike;
                        $newObj->created_at = date('Y-m-d H:i:s');
                        $newObj->updated_at = date('Y-m-d H:i:s');
                        $newObj->save();
                    }
                }
                if($typelike==2){
                    $islike = VtCommentLikeBase::getLike($this->userId, $commentId,1);
                    if($islike){
                        $islike->like = 2;
                        $islike->save();
                    }else{
                        $newObj = new VtCommentLikeBase();
                        $newObj->user_id = $this->userId;
                        $newObj->comment_id = $commentId;
                        $newObj->content_id = $contentId;
                        $newObj->type = $video['type'];
                        $newObj->like = $typelike;
                        $newObj->created_at = date('Y-m-d H:i:s');
                        $newObj->updated_at = date('Y-m-d H:i:s');
                        $newObj->save();
                    }

                }

            }
            VtCommentBase::updateLikeDislikeCount($commentId);
            $likeCount = VtCommentLikeBase::getLikeCount($commentId,1);
            $disLikeCount = VtCommentLikeBase::getLikeCount($commentId,2);
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Method Not Allowed")
            ];
        }


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'typelike' => $typelike,
                'like_count' => $likeCount,
                'dislike_count' => $disLikeCount
            ]
        ];
    }

    /**
     * Suggest tim kiem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionSearchSuggestion()
    {
        $appId = Yii::$app->id;
        $query = trim(Yii::$app->request->get('query', ''));
        if (empty($query)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Vui lòng nhập nhập nội dung tìm kiếm')
            ];
        }

        if (strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự')
            ];
        }

        $videos = VtVideoSearchBase::search($query, 0, Yii::$app->params['app.search.suggestion.limit']);

        $videoSuggestionArr = [
            'name' => 'Video',
            'type' => 'VOD',
            'content' => array()
        ];
        foreach ($videos as $video) {

            $suggestion = array();
            $suggestion['id'] = $video['_source']['id'];
            $suggestion['name'] = $video['_source']['name'];
            $suggestion['coverImage'] = VtHelper::getThumbUrl($video['_source']['bucket'], $video['_source']['path'], VtHelper::SIZE_VIDEO);
            $suggestion['type'] = 'VOD';
            if ($appId == 'app-wap') {
                $suggestion['slug'] = $video['_source']['slug'];
            }
            $videoSuggestionArr['content'][] = $suggestion;
        }

        $playlists = VtPlaylistSearchBase::search($query, Yii::$app->params['app.search.suggestion.limit']);

        $playlistSuggestionArr = [
            'name' => 'Playlist',
            'type' => 'PLAYLIST',
            'content' => array()
        ];
        foreach ($playlists as $playlist) {
            $suggestion = array();
            $suggestion['id'] = $playlist['_source']['id'];
            $suggestion['name'] = $playlist['_source']['name'];
            $suggestion['coverImage'] = VtHelper::getThumbUrl($playlist['_source']['bucket'], $playlist['_source']['path'], VtHelper::SIZE_FILM);
            $suggestion['type'] = 'PLAYLIST';
            if ($appId == 'app-wap') {
                $suggestion['slug'] = $playlist['_source']['slug'];
            }
            $playlistSuggestionArr['content'][] = $suggestion;
        }

        $channels = VtUserSearchBase::search($query, 0, Yii::$app->params['app.search.suggestion.limit']);

        $channelSuggestionArr = [
            'name' => 'Channel',
            'type' => 'CHANNEL',
            'content' => array()
        ];
        foreach ($channels as $channel) {
            $suggestion = array();
            $suggestion['id'] = $channel['_source']['id'];
            $suggestion['name'] = $channel['_source']['name'];
            $suggestion['coverImage'] = VtHelper::getThumbUrl($channel['_source']['bucket'], $channel['_source']['path'], VtHelper::SIZE_CHANNEL);
            $suggestion['type'] = 'CHANNEL';
            if ($appId == 'app-wap') {
                $suggestion['slug'] = $channel['_source']['slug'];
            }
            $channelSuggestionArr['content'][] = $suggestion;
        }

        //Dung cho duyet APP iOS
        $hiddenPackage = false;
        $osType = trim(Yii::$app->request->get('os_type', false));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', false));
        Yii::info("TRACK SEARCH SUGGEST| osType=" . $osType . "|osVersionCode=" . $osVersionCode);
        //Kiem tra xem co can update APP hay khong
        if ($osType && $osVersionCode) {
            //Kiem tra xem co can hidden noi dung tinh phi IOS
            if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                $hiddenPackage = true;
            }
        }
        // Neu can hidden noi dung de duyet app iOS
        if ($this->needHiddenFreemiumContent || $hiddenPackage) {
            Yii::info("TRACK | HIDDEN SEARCH SUGGEST");
            $playlistSuggestionArr = [
                'name' => 'Playlist',
                'type' => 'PLAYLIST',
                'content' => array()
            ];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                $videoSuggestionArr,
                $playlistSuggestionArr,
                $channelSuggestionArr
            ]
        ];
    }

    /**
     * Chi tiet tim kiem
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionSearch()
    {
        $query = trim(Yii::$app->request->get('query', ''));

        if (empty($query) || strlen(utf8_decode($query)) > 255) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung tìm kiếm không hợp lệ. Tối đa 255 kí tự'),
            ];
        }

        $searchVideos = VtVideoSearchBase::search($query, 0, Yii::$app->params['app.search.page.limit']);
        $videoIds = ArrayHelper::getColumn($searchVideos, '_id');

        $searchPlaylists = VtPlaylistSearchBase::search($query, Yii::$app->params['app.search.page.limit']);
        $playlistIds = ArrayHelper::getColumn($searchPlaylists, '_id');

        if (!empty($videoIds)) {
            $videos = VideoObj::serialize(
                Obj::VIDEO_SEARCH, VtVideoBase::getVideosByIdsQuery($videoIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($videoIds)) . ')')])
            );
        }

        if (!empty($playlistIds)) {
            $playlists = PlaylistObj::serialize(
                VtPlaylistBase::TYPE_FILM, Obj::PLAYLIST_SEARCH, VtPlaylistBase::getPlaylistsByIdsQuery($playlistIds, Yii::$app->params['app.search.page.limit'])
                ->orderBy([new Expression('FIELD (p.id, ' . implode(',', array_filter($playlistIds)) . ')')])
            );
        }


        //Dung cho duyet APP iOS
        $hiddenPackage = false;
        $osType = trim(Yii::$app->request->get('os_type', false));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', false));
        Yii::info("TRACK SEARCH| osType=" . $osType . "|osVersionCode=" . $osVersionCode);
        //Kiem tra xem co can update APP hay khong
        if ($osType && $osVersionCode) {
            //Kiem tra xem co can hidden noi dung tinh phi IOS
            if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                $hiddenPackage = true;
            }
        }
        // Neu can hidden noi dung de duyet app iOS
        if ($this->needHiddenFreemiumContent || $hiddenPackage) {
            Yii::info("TRACK | HIDDEN SEARCH");
            $playlists = '';
        }
        // Ket thuc duyet APP iOS

        $dataResponse = array();
        if (isset($videos['content']) && $videos['content']) {
            $dataResponse[] = $videos;
        }

        if (isset($playlists['content']) && $playlists['content']) {
            $dataResponse[] = $playlists;
        }
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }

    /**
     * Load thong tin cau hinh
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionGetSetting()
    {
        $appId = Yii::$app->id;
        $quality = Yii::$app->params['setting.quality'];
        $feedBack = \common\helpers\Utils::getInterlizationParamsApp('setting.feedback', true, 'wap');
        // var_dump($feedBack); die('eee');
        if ($appId == "app-wap") {
            $htmlContent = [
                [
                    'type' => 'termCondition',
                    'content' => VtConfigBase::getI18nConfig('WAP_TERM_CONDITION'),
                ],
                [
                    'type' => 'intro',
                    'content' => VtConfigBase::getI18nConfig('WAP_SERVICE_INTRODUCTION')
                ]
            ];
        } else {
            $htmlContent = [
                [
                    'type' => 'term-condition',
                    'content' => VtConfigBase::getConfig('HTML_TERM_CONDITION'),
                ],
                [
                    'type' => 'intro',
                    'content' => VtConfigBase::getConfig('HTML_INTRO')
                ],
                [
                    'type' => 'contact',
                    'content' => VtConfigBase::getConfig('HTML_CONTACT')
                ],
                [
                    'type' => 'privacy',
                    'content' => VtConfigBase::getConfig('HTML_PRIVACY')
                ]
                ,
                [
                    'type' => 'summary',
                    'content' => VtConfigBase::getConfig('HTML_SUMMARY')
                ]
            ];
        }

        //Dung cho duyet APP iOS, can hidden tat ca cac the loai phim
        $hiddenPackage = false;
        $osType = trim(Yii::$app->request->get('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', ''));
        Yii::info("TRACK GET SETTING| osType=" . $osType . "|osVersionCode=" . $osVersionCode);
        //Kiem tra xem co can update APP hay khong
        if ($osType && $osVersionCode) {
            //Kiem tra xem co can hidden noi dung tinh phi IOS
            if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                $hiddenPackage = true;
            }
        }
        // Neu can hidden noi dung de duyet app iOS
        if ($this->needHiddenFreemiumContent || $hiddenPackage) {
            $type = 'VOD';
        } else {
            $type = ['VOD', 'FILM'];
        }
        // Ket thuc duyet APP iOS

        $categories = VtGroupCategoryBase::getAllActiveCategory($type);
        $tmpCategory = array();
        $cc = 0;

        // Danh sach the loai nhac, video
        $musicCategoryList = explode(",", VtConfigBase::getConfig('music.category.list'));
        $videoCategoryList = explode(",", VtConfigBase::getConfig('video.category.list'));
        $filmCategoryList = explode(",", VtConfigBase::getConfig('film.category.list'));

        foreach ($categories as $category) {
            if (in_array($category['id'], $musicCategoryList) && $category['type'] = 'VOD') {
                $tmpCategory[$cc]['id'] = $category['id'];
                $tmpCategory[$cc]['name'] = $category['name'];
                $tmpCategory[$cc]['type'] = $category['type'];
                $tmpCategory[$cc]['getMoreContentId'] = Obj::CATEGORY_GROUP . $category['id'];
                $tmpCategory[$cc]['filter_type'] = 'MUSIC';
                $cc++;
            }
            if (in_array($category['id'], $videoCategoryList) && $category['type'] = 'VOD') {
                $tmpCategory[$cc]['id'] = $category['id'];
                $tmpCategory[$cc]['name'] = $category['name'];
                $tmpCategory[$cc]['type'] = $category['type'];
                $tmpCategory[$cc]['getMoreContentId'] = Obj::CATEGORY_GROUP . $category['id'];
                $tmpCategory[$cc]['filter_type'] = 'VIDEO';
                $cc++;
            }
            if (in_array($category['id'], $filmCategoryList) && $category['type'] = 'FILM') {
                $tmpCategory[$cc]['id'] = $category['id'];
                $tmpCategory[$cc]['name'] = $category['name'];
                $tmpCategory[$cc]['type'] = $category['type'];
                $tmpCategory[$cc]['getMoreContentId'] = Obj::CATEGORY_GROUP . $category['id'];
                $tmpCategory[$cc]['filter_type'] = 'FILM';
                $cc++;
            }
        }

        $eventName = VtConfigBase::getConfig("event.name");

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isOnEventIphoneX' => VtConfigBase::getConfig("IS_ON_EVENT_IPHONEX_APP", 0),
                'quality' => $quality,
                'feedBack' =>  $feedBack,
                'htmlContent' => $htmlContent,
                'categories' => $tmpCategory,
                'acceptLostDataTimeout' => Yii::$app->params['accept_lost_data.timeout'],
                'event' => $eventName
            ]
        ];
    }

    public function actionListPackage($distributionId = null)
    {

        $infoMessage = Yii::t('wap', Yii::$app->params['list.package.infoMessage']);
        $packageName = "";

        //DUng cho duyet APP iOS
        $hiddenPackage = false;

        if (empty($distributionId)) {
            $distributionId = Yii::$app->request->get('distribution_id');
        }

        $osType = trim(Yii::$app->request->get('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', ''));
        Yii::info("TRACK LISTPACKAGE| osType=" . $osType . "|osVersionCode=" . $osVersionCode);
        //Kiem tra xem co can update APP hay khong
        if ($osType && $osVersionCode) {
            //Kiem tra xem co can hidden noi dung tinh phi IOS
            if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                $hiddenPackage = true;
            }
        }
        // Kiem tra neu la khuyen mai thi khong hien thi danh sach goi cuoc nua?!
        $popupPromotion = VtFlow::loadPromotionPopup($this->msisdn, true);
        if ($popupPromotion) {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
                'data' => [],
                'popup' => $popupPromotion
            ];
        }

        $arrPackage = VtPackageBase::getListPackage($distributionId);

        $strSub = array();
        $data = array();

        if ($this->msisdn) {
            $arrSub = VtSubBase::getSub($this->msisdn);
            //var_dump($sub);die;
            foreach ($arrSub as $sub) {
                $strSub[] = $sub['package_id'];
            }
        }

        foreach ($arrPackage as $package) {
            //var_dump($arrPackage);die;
            if ((empty($distributionId) && empty($package['distribution_id'])) || (empty($distributionId) && in_array($package['id'], $strSub)) || (!empty($distributionId) && $distributionId == $package['distribution_id'])) {

                $item = array();
                $item['id'] = $package['id'];
                $item['name'] = $package['name'];
                $item['fee'] = $package['fee'];
                $item['short_description'] = $package['short_description'];
                $item['description'] = $package['description'];

                $cycleArr = explode(' ', $package['charge_range']);
                $item['cycle'] = isset($cycleArr[1]) ? $cycleArr[1] : '';

                if (in_array($package['id'], $strSub)) {
                    $packageName = $package['name'];
                    $item['status'] = 1;
                    $arrReplace = array('#packageName' => htmlspecialchars($item['name'], ENT_QUOTES, 'UTF-8'), '#cycle' => Utils::convertDay($package['charge_range']), '#price' => $package['fee']);
                    $item['popup'][] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('wap',Yii::$app->params['list.package.cancel.confirm']));

                } else {

                    $item['status'] = 0;
                    $arrReplace = array('#packageName' => htmlspecialchars($item['name'], ENT_QUOTES, 'UTF-8'), '#cycle' => Utils::convertDay($package['charge_range']), '#price' => $package['fee']);
//                    $item['popup'][] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::$app->params['list.package.confirm']);
                    $item['popup'][] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('wap',Yii::$app->params['list.package.confirm']));
                    //                $item['popup'][] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::$app->params['list.package.confirm2']);
                }
                //Chi hien thi nhung thue bao da dang ky goi cuoc hoac hien thi frontend
                if ($item['status'] || $package['is_display_frontend']) {
                    $data[] = $item;
                }
            }
        }
        // chi hien thi message khi dang ky goi
        if ($packageName) {
            $infoMessage = str_replace("#msisdn", Utils::hideMsisdnLast($this->msisdn), str_replace("#packageName", $packageName, $infoMessage));
        }

        // Neu can hidden noi dung de duyet app iOS
//        if ($this->needHiddenFreemiumContent || $hiddenPackage) {
//            Yii::info("TRACK | HIDDEN PACKAGE");
//            return [
//                'responseCode' => ResponseCode::SUCCESS,
//                'message' => Yii::t('wap', 'Thành công'),
//                'data' => []
//            ];
//        } else {
        Yii::info("TRACK |SHOW PACKAGE");

        $isConfirm = 0;
        //Kiem tra cau hinh xem co can confirm dang ky hay khong?

        switch (Yii::$app->id) {
            case 'app-wap':
                $isConfirm = VtConfig::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                break;
            case 'app-api':
                $isConfirm = VtConfig::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                break;
            case 'app-web':
                $isConfirm = VtConfig::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                break;
        }

        $source = Yii::$app->session->get('source');
        //Kiem tra source, neu thuoc whitelist thi khong can conffimr (GA)
        $whiteList = VtConfigBase::getConfig('register.whitelist.no.confirm');
        if (strpos(strtoupper($whiteList), strtoupper($source)) !== false) {
            $isConfirm = 0;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap', 'Thành công'),
            'data' => $data,
            'infoMessage' => $infoMessage,
            'isConfirmSMS' => $isConfirm,
            'is_confirm_sms' => $isConfirm
        ];
        // }
    }

    /**
     * Trang Home
     * @author ducda2@viettel.com.vn
     */
    public function actionGetMoreContent()
    {
        $id = trim(Yii::$app->request->get('id', ''));
        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit', Yii::$app->params['app.showMore.limit']));
        //Dung cho duyet APP iOS
        $hiddenContent = false;
        $osType = trim(Yii::$app->request->get('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->get('os_version_code', ''));
        Yii::info("TRACK GET MORE CONTENT| osType=" . $osType . "|osVersionCode=" . $osVersionCode);
        //Kiem tra xem co can update APP hay khong
        if ($osType && $osVersionCode) {
            //Kiem tra xem co can hidden noi dung tinh phi IOS
            if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                $hiddenContent = true;
                if (in_array(strtolower($id), [strtolower(Obj::FILM_FREE), strtolower(Obj::FILM_NEW)])) {
                    return [
                        'responseCode' => ResponseCode::SUCCESS,
                        'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                        'data' => []
                    ];
                }
            }
        }

        $contents = array();

        switch (strtolower($id)) {
            case strtolower(Obj::VIDEO_NEWSFEED):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_NEWSFEED, VtVideoBase::getHotVideo($limit, $offset), true, false, 'NEWSFEED'
                );
                break;
            case strtolower(Obj::VIDEO_NEW):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_NEW, VtVideoBase::getNewVideo(VtVideoBase::VOD_FILTER, $limit, $offset), true
                );
                break;
            case strtolower(Obj::MUSIC_NEW):
                $contents = VideoObj::serialize(
                    Obj::MUSIC_NEW, VtVideoBase::getNewVideo(VtVideoBase::MUSIC_FILTER, $limit, $offset), true
                );
                break;
            case strtolower(Obj::VIDEO_RECOMMEND):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_RECOMMEND, VtVideoBase::getRecommendVideo(VtVideoBase::VOD_FILTER, $limit, $offset), true
                );
                break;
            case strtolower(Obj::MUSIC_RECOMMEND):
                $contents = VideoObj::serialize(
                    Obj::MUSIC_RECOMMEND, VtVideoBase::getRecommendVideo(VtVideoBase::MUSIC_FILTER, $limit, $offset), true
                );
                break;
            case strtolower(Obj::VIDEO_FREE):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_FREE, VtVideoBase::getVideosFree(VtVideoBase::VOD_FILTER, $limit, $offset), true
                );
                break;
            case strtolower(Obj::MUSIC_FREE):
                $contents = VideoObj::serialize(
                    Obj::MUSIC_FREE, VtVideoBase::getVideosFree(VtVideoBase::MUSIC_FILTER, $limit, $offset), true
                );
                break;
            case strtolower(Obj::VIDEO_HISTORY):
                if (!$this->isValidUser()) {
                    return $this->authJson;
                }
                $category = explode(",", VtConfigBase::getConfig("video.category.list"));
                //Lay 100 item( sau do loc lay app.page.limit item)
                $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, $category, 200, $offset);

                $historyIds = array();
                foreach ($histories as $history) {
                    $historyIds[] = $history['_source']['item_id'];
                }
                if (!empty($historyIds)) {
                    $contents = VideoObj::serialize(
                        Obj::VIDEO_HISTORY, VtVideoBase::getByIdsQuery(VtVideoBase::VOD_FILTER, $historyIds, $limit, $offset)
                        ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')]), false
                    );
                }
                break;
            case strtolower(Obj::MUSIC_HISTORY):
                if (!$this->isValidUser()) {
                    return $this->authJson;
                }
                $category = explode(",", VtConfigBase::getConfig("music.category.list"));
                //Lay 100 item( sau do loc lay app.page.limit item)
                $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, $category, 200, $offset);

                $historyIds = array();
                foreach ($histories as $history) {
                    $historyIds[] = $history['_source']['item_id'];
                }
                if (!empty($historyIds)) {
                    $contents = VideoObj::serialize(
                        Obj::MUSIC_HISTORY, VtVideoBase::getByIdsQuery(VtVideoBase::MUSIC_FILTER, $historyIds, $limit, $offset)
                        ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')]), false
                    );
                }
                break;
            case strtolower(Obj::FILM_NEW):
                $contents = PlaylistObj::serialize(
                    VtPlaylistBase::TYPE_FILM, Obj::FILM_NEW, VtPlaylistBase::getPlayListNewByType(VtPlaylistBase::TYPE_FILM, $limit, $offset), true
                );
                break;
            case strtolower(Obj::FILM_HOT):
                $contents = PlaylistObj::serialize(
                    VtPlaylistBase::TYPE_FILM, Obj::FILM_HOT, VtPlaylistBase::getPlayListHot(VtPlaylistBase::TYPE_FILM, $limit, $offset), true
                );
                break;
            case strtolower(Obj::FILM_RECOMMEND):
                $contents = PlaylistObj::serialize(
                    VtPlaylistBase::TYPE_FILM, Obj::FILM_RECOMMEND, VtPlaylistBase::getPlayListRecommend(VtPlaylistBase::TYPE_FILM, $limit, $offset), true
                );
                break;
            case strtolower(Obj::FILM_FREE):
                $contents = PlaylistObj::serialize(
                    VtPlaylistBase::TYPE_FILM, Obj::FILM_FREE, VtPlaylistBase::getPlayListFree(VtPlaylistBase::TYPE_FILM, $limit, $offset), true
                );
                break;
            case strtolower(Obj::USER_FOLLOW):
                $contents = UserFollowObj::serialize(
                    Obj::USER_FOLLOW, VtUserFollowBase::getFollowUser($this->userId, $limit, $offset)
                );
                break;
            case strtolower(Obj::USER_FOLLOW_VIDEO):
                $contents = UserFollowObj::serialize(
                    Obj::USER_FOLLOW_VIDEO, VtUserFollowBase::getFollowUser($this->userId, $limit, $offset)
                );
                break;
            case strtolower(Obj::USER_FOLLOW_MUSIC):
                $contents = UserFollowObj::serialize(
                    Obj::USER_FOLLOW_MUSIC, VtUserFollowBase::getFollowUser($this->userId, $limit, $offset)
                );
                break;
            case strtolower(Obj::MEMBER):
                $contents = UserObj::serialize(
                    Obj::MEMBER, VtUserBase::getListMember($this->userId, $limit, $offset)
                );
                break;
            case strtolower(Obj::MEMBER_FOLLOW):
                $contents = UserObj::serialize(
                    Obj::MEMBER_FOLLOW, VtUserFollowBase::getFollowUser($this->userId, $limit, $offset)
                );
                break;
            case strtolower(Obj::VIDEO_PAID):
                $contents = VideoObj::serialize(
                    Obj::VIDEO_PAID, VtVideoBase::getVideosPaid(VtVideoBase::VOD_FILTER, $limit, $offset), true
                );
                break;

            default:
                if (strpos(strtolower(strtolower($id)), Obj::VIDEO_OF_USER) === 0) {
                    $arrTmp = explode("_", $id);
                    $contents = VideoObj::serialize(
                        $id, VtVideoBase::getVideosByUser(false, $arrTmp[3], $limit, $offset), true, "Video"
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::CATEGORY_GROUP) === 0) {
                    $arrTmp = explode("_", $id);
                    //chi cho phep truyen ID la so
                    if (!is_numeric($arrTmp[2]) || count($arrTmp) > 3) {
                        return "INVALID";
                    }
                    $objCategory = VtGroupCategoryBase::getCategoryGroup($arrTmp[2]);
                    if ($objCategory) {
                        if ($objCategory['type'] == 'VOD') {
                            $contents = VideoObj::serialize(
                                Obj::CATEGORY_GROUP . $arrTmp[2], VtVideoBase::getVideosByCategory($arrTmp[2], $limit, $offset), true, $objCategory['name']
                            );
                        } elseif ($objCategory['type'] == 'FILM') {
                            $contents = PlaylistObj::serialize(
                                VtPlaylistBase::TYPE_FILM, Obj::CATEGORY_GROUP . $arrTmp[2], VtPlaylistBase::getPlayListByCategory($arrTmp[2], $limit, $offset), true, $objCategory['name']
                            );
                        }
                    }
                } else if (strpos(strtolower($id), strtolower(Obj::TOPIC_GROUP)) === 0) {
                    $arrTmp = explode("_", $id);
                    //chi cho phep truyen ID la so
                    if (!is_numeric($arrTmp[2]) || count($arrTmp) > 3) {
                        return "INVALID";
                    }
                    $objTopic = VtGroupTopicBase::getDetail($arrTmp[2]);
                    if ($objTopic) {
                        switch ($objTopic['type']) {
                            case 'VOD':
                                $contents = VideoObj::serialize(
                                    $id, VtVideoTopicBase::getVideosTopic($arrTmp[2], $limit, $offset), true, $objTopic['name']
                                );
                                break;
                            case 'FILM':
                                $contents = PlaylistObj::serialize(
                                    $objTopic['type'], $id, VtPlaylistTopicBase::getPlaylistsTopic($arrTmp[2], $limit, $offset), true, $objTopic['name']
                                );
                                break;
                        }
                    }
                } else if (strpos(strtolower($id), strtolower(Obj::VIDEO_USER_LIKE)) === 0) {
                    $arrTmp = explode("_", $id);
                    //chi cho phep truyen ID la so
                    if (!is_numeric($arrTmp[3]) || count($arrTmp) > 4) {
                        return "INVALID";
                    }
                    // TODO Phan quyen
                    $contents = VideoObj::serialize(
                        $id, VtFavouriteVideoBase::getListVideos($arrTmp[3], $limit, $offset), false
                    );
                } else if (strpos(strtolower(strtolower($id)), Obj::MEMBER_FOLLOW_VIEW) === 0) {
                    $arrTmp = explode("_", $id);
                    //chi cho phep truyen ID la so
                    if (!is_numeric($arrTmp[2]) || count($arrTmp) > 3) {
                        return "INVALID";
                    }
                    $contents = UserObj::serialize(
                        $id, VtUserFollowBase::getFollowUser($arrTmp[2], $limit, $offset)
                    );
                }
                break;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        ];
    }

    public function actionFeedBack()
    {
        if (!$this->isValidUser() && !$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $id = trim(Yii::$app->request->post('id', ''));
        $content = trim(Yii::$app->request->post('content', ''));
        $itemId = trim(Yii::$app->request->post('item_id', null));
        $type = Yii::$app->request->post('type', null);
        $type = isset($type) ? trim($type) : $type;
        //var_dump($content);die;
        if ((!empty($type) && !in_array($type, Yii::$app->params['object.type']))
            || empty($id) || empty($content)
            || !in_array($id, ArrayHelper::getColumn( Yii::$app->params['setting.feedback'], 'id')
            )
        ) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Dữ liệu không hợp lệ')
            ];
        }

        if (strlen(utf8_decode($content)) < 10 || strlen(utf8_decode($content)) > 2000) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Nội dung từ 10 đến 2000 kí tự ')
            ];
        }

        if (VtFeedBackBase::countFeedbackByUserThisDay($this->userId, $this->msisdn) >= Yii::$app->params['feedback.limit']) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Thực hiện báo cáo thành công, chúng tôi sẽ xem xét và xử lý trong 24h')
            ];
        }

        $feedBack = new VtFeedBackBase();
        $feedBack->insertFeedBack($this->userId, $this->msisdn, $id, $content, $itemId, $type);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('wap', 'Thực hiện báo cáo thành công, chúng tôi sẽ xem xét và xử lý trong 24h')
        ];
    }

    /**
     * lay danh sach tat ca cac topic
     * @return array
     */
    public function actionGetTopics()
    {
        if (!$this->isValidUser() && !$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $topics = VtGroupTopicBase::getAllActiveTopic();
        $userTopic = VtUserTopicBase::getTopicLiked($this->userId, $this->msisdn, false, true)->all();

        $tmpUT = [];
        foreach ($userTopic as $uT) {
            $tmpUT[] = $uT['id'];
        }
        $contents = [];
        $cc = 0;
        foreach ($topics as $topic) {
            $contents[$cc]['id'] = $topic['id'];
            $contents[$cc]['name'] = $topic['name'];
            $contents[$cc]['coverImage'] = VtHelper::getThumbUrl($topic['bucket'], $topic['path'], VtHelper::SIZE_AVATAR);
            $contents[$cc]['type'] = $topic['type'];
            if (in_array($topic['id'], $tmpUT)) {
                $contents[$cc]['is_like'] = 1;
            } else {
                $contents[$cc]['is_like'] = 0;
            }
            $cc++;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        ];
    }

    /**
     * Ham gui binh luan
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionToggleLikeTopic()
    {
        if (!$this->isValidUser() && !$this->isValidMsisdn()) {
            return $this->authJson;
        }

        $topicId = trim(Yii::$app->request->post('id', ''));

        if (empty($topicId)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Thông tin không hợp lệ'),
            ];
        }
        $objTopic = VtGroupTopicBase::getDetail($topicId);
        if (!$objTopic) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }
        $favObj = VtUserTopicBase::getLike($this->userId, $this->msisdn, $topicId);

        if (!empty($favObj)) {
            $favObj->delete();
            $isLike = false;
        } else {
            $favObj = new VtUserTopicBase();
            $favObj->insertLike($this->userId, $this->msisdn, $topicId);
            $isLike = true;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isLike' => $isLike
            ]
        ];
    }

    public function actionTriggerComment()
    {
        if (!$this->isValidUser() && !$this->isValidMsisdn()) {
            return $this->authJson;
        }
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {
            $videoId = trim(Yii::$app->request->post('id', ''));
            $status = trim(Yii::$app->request->post('status', ''));
            if (empty($videoId)|| !in_array($status, array(0,1,'auto'))) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Thông tin không hợp lệ'),
                ];
            }
            $video = VtVideoBase::findOneById($videoId);
            if (!$video) {
                return [
                    'responseCode' => ResponseCode::NOT_FOUND,
                    'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
                ];
            }
            if($status == 'auto'){
                if($video['can_comment'] == 0){
                    $status = 1;
                }else{
                    $status = 0;
                }
            }
            $checkChannel = VtChannelBase::CheckChannelUser($video['channel_id'],$this->userId);
            if(count($checkChannel) > 0 || $video['created_by'] == $this->userId){
                $trigger = VtVideoBase::triggerComment($videoId,$status);
                if($trigger){
                    $video = VtVideoBase::findOneById($videoId);
                    if($status == 1){
                        return [
                            'responseCode' => ResponseCode::SUCCESS,
                            'message' => Yii::t('wap',"Bật comment thành công"),
                            'status' => $status
                        ];
                    }else{
                        return [
                            'responseCode' => ResponseCode::SUCCESS,
                            'message' => Yii::t('wap',"Tắt comment thành công"),
                            'status' => $status
                        ];
                    }
                }
            }else{
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap','Bạn không đủ quyền thực hiện chức năng này!'),
                ];
            }
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', "Method Not Allowed")
            ];
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'status' => $status
        ];
    }

}
