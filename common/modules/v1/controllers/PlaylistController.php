<?php
namespace common\modules\v1\controllers;

use api\models\VtFavouritePlaylist;
use common\libs\S3Service;
use common\libs\VtFlow;
use common\models\VtHistoryViewBase;
use common\models\VtUserFollowBase;
use common\models\VtVideoBase;
use common\modules\v1\libs\Obj;
use common\modules\v1\libs\PlaylistObj;
use Yii;
use common\libs\VtHelper;
use common\models\VtPlaylistBase;
use common\models\VtPlaylistItemBase;
use common\models\VtUserBase;
use common\modules\v1\libs\ResponseCode;
use common\controllers\ApiController;
use common\models\VtFavouritePlaylistBase;

class PlaylistController extends ApiController
{

    public function actionGetDetail($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = null)
    {

        $id = trim(Yii::$app->request->get('id', 0));
        $playlist = VtPlaylistBase::getDetail($id);
        $supportType = (Yii::$app->session->get("is_mp4_only", false)) ? S3Service::OBJCDN : S3Service::VODCDN;
        $currentTime = 0;
        if (!$profileId) {
            $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['streaming.vodProfileId']));
        }

        $videoId = trim(Yii::$app->request->get('video_id', 0));

        if(!isset($acceptLossData)){
            $acceptLossData = Yii::$app->request->get("accept_loss_data", 0);
        }

        if (empty($playlist)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        $detailObj = array();
        $detailObj['id'] = $playlist['id'];
        $detailObj['name'] = $playlist['name'];
        $detailObj['description'] = $playlist['description'];
        $detailObj['type'] = $playlist['type'];
        // Neu type = Film thi lay anh doc
        if ($playlist['type'] == VtPlaylistBase::TYPE_FILM) {
            $detailObj['coverImage'] = VtHelper::getThumbUrl($playlist['bucket'], $playlist['path'], VtHelper::SIZE_FILM);
        } else {
            $detailObj['coverImage'] = VtHelper::getThumbUrl($playlist['bucket'], $playlist['path'], VtHelper::SIZE_COVER);
        }
        $detailObj['likeCount'] = $playlist['like_count'];

        $detailObj['link'] = Yii::$app->params['cdn.site'] . "/phim/" . $id."/".$playlist['slug']."?utm_source=APPSHARE";

        $detailObj['slug'] = $playlist['slug'];
        if ($this->userId) {
            $detailObj['isFavourite'] = (VtFavouritePlaylistBase::checkIsFavourite($this->userId, $id)) ? 1 : 0;
        } else {
            $detailObj['isFavourite'] = 0;
        }
        $detailObj['play_times'] = number_format($playlist['play_times'], 0, ',', '.');

        $detailObj['suggest_package_id'] = $playlist['suggest_package_id'];

        // Lay thong tin thue bao Playlist
        $objUser = VtUserBase::getById($playlist['created_by']);

        $detailObj['owner']['id'] = $objUser['id'];
        $detailObj['owner']['name'] = ($objUser['full_name']) ? $objUser['full_name'] :  ( ($objUser['msisdn'])?substr($objUser['msisdn'],0,-3)."xxx":"" );
        $detailObj['owner']['avatarImage'] = VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR);
        $detailObj['owner']['followCount'] = $objUser['follow_count'];
        if ($this->userId) {
            $detailObj['owner']['isFollow'] = (VtUserFollowBase::getFollow($this->userId, $playlist['created_by'])) ? 1 : 0;
        } else {
            $detailObj['owner']['isFollow'] = 0;
        }

        if ($objUser) {
            $playlistUserFollow = VtPlaylistBase::getPlayListByUserId($playlist['created_by'], $playlist['type'], Yii::$app->params['app.film.relate.limit'], 0)->all();
            $c = 0;
            foreach ($playlistUserFollow as $v) {
                $detailObj['owner']['films'][$c]['id'] = $v['id'];
                $detailObj['owner']['films'][$c]['name'] = $v['name'];
                $detailObj['owner']['films'][$c]['coverImage'] = VtHelper::getThumbUrl($v['bucket'], $v['path'], VtHelper::SIZE_FILM);
                $detailObj['owner']['films'][$c]['slug'] = $v['slug'];
                $c++;
            }
        }

        // Lay danh sach cac item thuoc playlist
        $items = VtPlaylistItemBase::getItemsByPlaylistId($id);
        $parts = array();
        $cc = 0;
        $firstItemId = 0;
        $isVideoInPlaylist = false;

        foreach ($items as $item) {
            if ($firstItemId == 0) {
                $firstItemId = $item['id'];
            }
            if ($videoId == $item['id']) {
                $isVideoInPlaylist = true;
            }
            $prefixAlias = Yii::$app->params['alias.prefix'];
            $parts[$cc]['id'] = $item['id'];
            $parts[$cc]['alias'] = $prefixAlias . (($item['alias']) ? $item['alias'] : ($cc + 1));
            $parts[$cc]['name'] = $item['name'];
            $cc++;
        }

        if (!$videoId) {
            $videoId = $firstItemId;
            $isVideoInPlaylist = true;
        }

        $detailObj['currentVideoId'] = $videoId;
        if (!$isVideoInPlaylist) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }
        //Lay video dau tien de xem
        $video = VtVideoBase::getDetail($videoId);

        if (!$video) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];

        } else {
            if ($this->userId || $this->msisdn) {
                $history = VtHistoryViewBase::getByItemId($this->userId, $this->msisdn, $videoId);
                $currentTime = ($history) ? intval($history->time) : 0;
            }
        }
        //Thay doi goi cuoc suggestion theo goi cua playlist
        $video['suggest_package_id'] = $playlist['suggest_package_id'];

        // Thay anh cover la anh video
        $detailObj['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_COVER);

        $streams = VtFlow::viewVideo($this->msisdn, $video, $id, $profileId, $this->userId, $this->authJson, $supportType, $acceptLossData);

        //An noi dung tinh phi khi duyet APP iOS
        if ($this->needHiddenFreemiumContent) {
            if (isset($streams['popup'])) {
                $streams['popup'] = [];
            }
        }

        $relateds = PlaylistObj::serialize(
            VtPlaylistBase::TYPE_FILM,
            Obj::FILM_RELATE,
            VtPlaylistBase::getPlayListNewByType(VtPlaylistBase::TYPE_FILM, Yii::$app->params['app.film.relate.limit'], 0),
            true
        );

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
                'parts' => $parts,
                'profile' => [],
                'streams' => $streams,
                'relateds' => $relateds,
                'currentTime' => $currentTime
            ]
        ];

    }

    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionToggleLikePlaylist()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => 'Quý khách vui lòng đăng ký thành viên để sử dụng tính năng này!',
                ];
            } else {
                return $this->authJson;
            }
        }
        $id = trim(Yii::$app->request->post('id', 0));

        $playlist = VtPlaylistBase::getDetail($id);

        if (empty($playlist)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        $favObj = VtFavouritePlaylistBase::getFavourite($this->userId, $id);

        if (!empty($favObj)) {
            $favObj->delete();
            $isLike = false;
        } else {
            $favObj = new VtFavouritePlaylistBase();
            $favObj->insertFavourite($this->userId, $id);
            $isLike = true;
        }

        VtPlaylistBase::updateLikeCount($id, $isLike);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isLike' => $isLike
            ]
        ];

    }

}