<?php
namespace common\modules\v1\controllers;

use common\helpers\CaptchaGenerate;
use common\libs\S3Service;
use common\libs\VtFlow;
use common\models\VtUserTokenBase;
use common\models\VtUserViewBase;
use Yii;
use common\controllers\ApiController;
use common\helpers\Des;
use common\helpers\Utils;
use common\libs\VtHelper;
use common\models\VtConfigBase;
use common\models\VtSmsMtBase;
use common\models\VtTokenBase;
use common\models\VtUserBase;
use common\models\VtUserOtpBase;
use common\modules\v1\libs\ResponseCode;
use common\helpers\MobiTVRedisCache;
use yii\authclient\AuthAction;
use yii\httpclient\Client;

class AuthController extends ApiController
{

    /**
     * Ham xac thuc, cung cap 4 loai grant_type: auto_login, login,login_social, refresh_token
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionAuthorize()
    {

        $grantType = trim(Yii::$app->request->post('grant_type', 'auto_login'));

        $osType = trim(Yii::$app->request->post('os_type', ''));
        $osVersionCode = trim(Yii::$app->request->post('os_version_code', ''));

        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';
        $userAgentEncrypted = md5(Yii::$app->params['userAgent.secretCode'] . $userAgent);
        $accessTokenExpiredTime = time() + Yii::$app->params['accessToken.timeout'];
        $refreshTokenExpiredTime = time() + Yii::$app->params['refreshToken.timeout'];

        Yii::info("AUTH | REQUEST =" . $grantType);
        $isAutoLogin = false;
        $user = null;
        $msisdn = '';
        $needShowMapAccount = 0;
        $isShowSuggest = 0;
        $isForceUpdateAPP = 0;
        $isUpdateAPP = 0;
        $isShowUpdateAPP = VtConfigBase::getConfig("IS_SHOW_UPDATE_APP_" . strtoupper($osType), 0);


        if ($grantType === 'auto_login') {
            //@todo: dang hardcode, bo di khi trien khai that
            
            $msisdn = VtHelper::getMsisdn();

			Yii::info("AUTH | AUTO LOGIN GET MSISDN =" . $msisdn, 'info');
            if (!empty($msisdn)) {
                $isAutoLogin = true;
                $user = VtUserBase::getByMsisdn($msisdn);
                Yii::info("AUTH | AUTO LOGIN GET ID =" . $user->id."| USER NAME=".$user->full_name);
            }
        } else if ($grantType === 'login') {
            $username = trim(Yii::$app->request->post('username', ''));
            $password = trim(Yii::$app->request->post('password', ''));
            $captcha = trim(Yii::$app->request->post('captcha', ''));
            $captchaToken = 'no-cap-t-cha';
            $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
            if (preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches)) {
                $captchaToken = md5(VtHelper::getAgentIp() . $matches[1]);
            }

            if (empty($this->accessToken)) {
                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap', 'Access Token không được để trống')
                ];
            }
            if (empty($username) || empty($password)) {
                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap', 'Quý khách vui lòng nhập Số thuê bao và Mật khẩu')
                ];
            }

            $msisdn = Utils::getMobileNumber($username, Utils::MOBILE_GLOBAL);
            $redisCache = Yii::$app->cache;
            $ip = md5(VtHelper::getAgentIp());

            $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
            $ipUser = md5(VtHelper::getAgentIp() . $username);
            $countLock = intval($redisCache->get('count_lock_' . $ipUser));

            //-- Kiem tra khoa tai khoan trong 10phut
            $isLockIPUser = $redisCache->get('lock_login_' . $ipUser);

            $configCaptchaShow = Yii::$app->params['login.captcha.show.count'];
            $configCountLock = Yii::$app->params['login.lock.count'];

            if ($countLock >= $configCountLock || $isLockIPUser) {
                if ($isLockIPUser) {
                    $redisCache->set('count_lock_' . $ipUser, 0, MobiTVRedisCache::CACHE_1DAY);
                } else {
                    $redisCache->set('lock_login_' . $ipUser, 1, MobiTVRedisCache::CACHE_10MINUTE);
                    // - Xoa bo dem lock
                    $redisCache->delete('count_lock_' . $ipUser);
                    $redisCache->delete('count_login_fail_' . $ip);
                }
                return [
                    'responseCode' => ResponseCode::LOCK_USER,
                    'message' => Yii::t('wap', 'Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!')
                ];
            }

            if ($countLoginFail >= $configCaptchaShow) {
                if (empty($captcha)) {
                    return [
                        'responseCode' => ResponseCode::CAPTCHA_EMPTY,
                        'message' => Yii::t('wap', 'Quý khách vui lòng nhập mã xác nhận')
                    ];
                } else {
                    $serverCaptcha = Yii::$app->cache->get("captcha_" . $captchaToken);
                    // refreshCaptcha
                    $randomCode = Utils::generateVerifyCode();
                    Yii::$app->cache->set("captcha_" . $captchaToken, $randomCode, MobiTVRedisCache::CACHE_15MINUTE);// luu captcha trong 15P

                    if (empty($serverCaptcha) || $serverCaptcha !== $captcha) {
                        return [
                            'responseCode' => ResponseCode::CAPTCHA_INVALID,
                            'message' => Yii::t('wap', 'Mã xác nhận không hợp lệ')
                        ];
                    }
                }
            }

            $user = VtUserBase::getByMsisdnOrEmail($msisdn, $username);

            //@todo: kiem tra xu ly not + bo sung captcha
            if (!empty($user) && $user->status == VtUserBase::ACTIVE && $user->checkPassword($password)) {

                $msisdn = $user->msisdn;
                if (!$user->changed_password) {
                    $needChangePassword = true;
                }

                // - Dang nhap thanh cong thi xoa lock
                $redisCache->delete('count_lock_' . $ipUser);
                $redisCache->delete('count_login_fail_' . $ip);
            } else {
                // Neu empty user thi chi tang lock login , khong khoa user
                if (empty($user)) {
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                } else {
                    $redisCache->set('count_lock_' . $ipUser, $countLock + 1, MobiTVRedisCache::CACHE_1DAY);
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                }

                return [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('wap', 'Thông tin đăng nhập không hợp lệ'),
                    'captcha' => ($countLoginFail >= ($configCaptchaShow - 1)) ? 1 : 0
                ];
            }

        } else if ($grantType === 'login-social') {
            $socialId = trim(Yii::$app->request->post('social_id', ''));
            $accessToken = trim(Yii::$app->request->post('access_token', ''));
            $needShowMapAccount = 0;
            switch (strtolower(trim($socialId))) {
                case "facebook":
                    try {
                        $response = Utils::getUrlContent(Yii::$app->params['getprofile.facebook'] . $accessToken . '&fields=name,email,first_name,last_name,picture.type(large),cover');
                        $jsonResponse = json_decode($response, true);

                        if ($jsonResponse['id'] && $jsonResponse['name']) {

                            $user = VtUserBase::getByOauthId($jsonResponse['id']);
                            if (!$user) {

                                $imageUrl = str_replace('sz=50', 'sz=200', $jsonResponse['picture']['data']['url']);

                                $bucketName = '';
                                $newPath = '';

                                $client = new Client([
                                    'transport' => 'yii\httpclient\CurlTransport'
                                ]);

                                $response = $client->createRequest()
                                    ->setMethod('get')
                                    ->setUrl($imageUrl)
                                    ->setOptions([
                                        CURLOPT_CONNECTTIMEOUT => 5, // connection timeout
                                        CURLOPT_TIMEOUT => 30, // data receiving timeout
                                    ])
                                    ->send();
                                if ($response->isOk) {

                                    $ext = 'jpg';
                                    $tmpFile = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                                    $fp = fopen($tmpFile, 'w');
                                    fwrite($fp, $response->content);
                                    fclose($fp);

                                    $bucketName = Yii::$app->params['s3']['static.bucket'];
                                    $newPath = Utils::generatePath($ext);

                                    S3Service::putObject($bucketName, $tmpFile, $newPath);
                                    VtHelper::generateAllThumb($bucketName, $tmpFile, $newPath);
                                    unlink($tmpFile);
                                }

                                $user = new VtUserBase();
                                $user->insertUserSocial($jsonResponse['id'], $jsonResponse['name'], $jsonResponse['email'], $bucketName, $newPath, 'FACEBOOK');
                                $user->full_name = $jsonResponse['name'];
                                $needShowMapAccount = 1;

                            } else {
                                $msisdn = $user->msisdn;
                            }
                        } else {
                            return [
                                'responseCode' => ResponseCode::FORBIDDEN,
                                'message' => Yii::t('wap', 'Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                            ];
                        }

                    } catch (\Exception $ex) {
                        return [
                            'responseCode' => ResponseCode::FORBIDDEN,
                            'message' => Yii::t('wap', 'Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                        ];
                    }

                    break;
                case "google":
                    try {
                        $response = Utils::getUrlContent(Yii::$app->params['getprofile.google'] . $accessToken);
                        $jsonResponse = json_decode($response, true);
                        if ($jsonResponse['sub']) {
                            $user = VtUserBase::getByOauthId($jsonResponse['sub']);
                            if (!$user) {


                                $imageUrl = $jsonResponse['image']['url'];

                                $bucketName = '';
                                $newPath = '';

                                $client = new Client([
                                    'transport' => 'yii\httpclient\CurlTransport'
                                ]);

                                $response = $client->createRequest()
                                    ->setMethod('get')
                                    ->setUrl($imageUrl)
                                    ->setOptions([
                                        CURLOPT_CONNECTTIMEOUT => 5, // connection timeout
                                        CURLOPT_TIMEOUT => 30, // data receiving timeout
                                    ])
                                    ->send();
                                if ($response->isOk) {

                                    $ext = 'jpg';
                                    $tmpFile = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                                    $fp = fopen($tmpFile, 'w');
                                    fwrite($fp, $response->content);
                                    fclose($fp);

                                    $bucketName = Yii::$app->params['s3']['static.bucket'];
                                    $newPath = Utils::generatePath($ext);

                                    S3Service::putObject($bucketName, $tmpFile, $newPath);
                                    VtHelper::generateAllThumb($bucketName, $tmpFile, $newPath);
                                    unlink($tmpFile);
                                }

                                $user = new VtUserBase();
                                $nameS = ($jsonResponse['name']) ? $jsonResponse['name'] : $jsonResponse['email'];
                                $user->insertUserSocial($jsonResponse['sub'], $nameS, $jsonResponse['email'], $bucketName, $newPath,'GOOGLE');
                                $user->full_name = $nameS;
                                $needShowMapAccount = 1;
                            } else {
                                $msisdn = $user->msisdn;

                            }
                        } else {
                            return [
                                'responseCode' => ResponseCode::FORBIDDEN,
                                'message' => Yii::t('wap', 'Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                            ];
                        }

                    } catch (\Exception $ex) {
                        return [
                            'responseCode' => ResponseCode::FORBIDDEN,
                            'message' => Yii::t('wap', 'Xác thực không thành công. Lấy thông tin thuê bao thất bại.'),
                        ];
                    }
                    break;
            }


        } else if ($grantType === 'refresh_token') {

            $refreshToken = trim(Yii::$app->request->post('refresh_token', ''));

            $userToken = VtUserTokenBase::getByToken($refreshToken);
            if ($userToken) {
                if ($userAgent != $userToken['user_agent'] || strtotime($userToken['token_expired_time']) < strtotime('now')) {
                    Yii::info("AUTH| osType=" . $osType . "|osVersionCode=" . $osVersionCode . "|configOsVersionCode=" . VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0));
                    //Kiem tra xem co can update APP hay khong
                    if ($osType && $osVersionCode) {
                        $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                        $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                        $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
                    }
                    return [
                        'responseCode' => ResponseCode::FORBIDDEN,
                        'message' => Yii::t('wap', 'Xác thực không thành công'),
                        'data' => [
                            'isForceUpdateAPP' => $isForceUpdateAPP,
                            'isUpdateAPP' => ($isShowUpdateAPP)?$isUpdateAPP:0
                        ]
                    ];
                } else {
                    $msisdn = $userToken->msisdn;
                    $user = VtUserBase::getById($userToken->user_id);
                }
            }
        }

        if (Utils::isValidMsisdn($msisdn) || !empty($user) || isset($userToken)) {

            $userId = ($user)?$user->id:null;


            $refreshToken = Utils::generateGuid();
            $refeshTokenExpiredTimeStamp = date("Y-m-d H:i:s", $refreshTokenExpiredTime);

            $ip = VtHelper::getAgentIp();

            if (isset($userToken) && $userToken) {
                $userToken->token = $refreshToken;
                $userToken->ip = $ip;
                $userToken->token_expired_time = $refeshTokenExpiredTimeStamp;
                $userToken->last_login = date('Y-m-d H:i:s', strtotime('now'));
                $userToken->user_agent = $userAgent;
                $userToken->save(false);

                $secretString = "$accessTokenExpiredTime&$userToken->user_id&$userToken->msisdn&$userAgentEncrypted";

            } elseif ($user) {
                $userToken = VtUserTokenBase::getByUserId($user->id);

                if ($userToken) {
                    $userToken->msisdn = $msisdn;
                    $userToken->token = $refreshToken;
                    $userToken->ip = $ip;
                    $userToken->token_expired_time = $refeshTokenExpiredTimeStamp;
                    $userToken->last_login = date('Y-m-d H:i:s', strtotime('now'));
                    $userToken->last_login_type = $isAutoLogin;
                    $userToken->user_agent = $userAgent;
                    $userToken->save(false);
                } else {
                    $userToken = new VtUserTokenBase();
                    $userToken->insertUserToken($refreshToken, $ip, $user->id, $msisdn, $refeshTokenExpiredTimeStamp, $isAutoLogin, $userAgent);
                }

                $secretString = "$accessTokenExpiredTime&$user->id&$msisdn&$userAgentEncrypted";

            } else {
                $userToken = VtUserTokenBase::getByMsisdn($msisdn);

                if ($userToken) {
                    $userToken->msisdn = $msisdn;
                    $userToken->token = $refreshToken;
                    $userToken->ip = $ip;
                    $userToken->token_expired_time = $refeshTokenExpiredTimeStamp;
                    $userToken->last_login = date('Y-m-d H:i:s', strtotime('now'));
                    $userToken->last_login_type = $isAutoLogin;
                    $userToken->user_agent = $userAgent;
                    $userToken->save(false);
                } else {
                    $userToken = new VtUserTokenBase();
                    $userToken->insertUserToken($refreshToken, $ip, null, $msisdn, $refeshTokenExpiredTimeStamp, $isAutoLogin, $userAgent);
                }
                $secretString = "$accessTokenExpiredTime&0&$msisdn&$userAgentEncrypted";
            }
            //Kiem tra dieu kien hien thi suggest Topic
            if ($user && $user->is_show_suggest == 0) {
                $isShowSuggest = 1;
                $user->is_show_suggest = 1;
                $user->save(false);
            } elseif ($msisdn && $isAutoLogin && !$user) {

                //Neu la 3G ma chua co key trong redis thi hien thi suggest Topic
                $redisCache = Yii::$app->getCache();
                $objSuggest = $redisCache->get("suggest_" . Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL));
                if (!$objSuggest) {
                    $isShowSuggest = 1;
                    $redisCache->set("suggest_" . Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL), 1);
                }
            }

            Yii::info("AUTH| osType=" . $osType . "|osVersionCode=" . $osVersionCode . "|configOsVersionCode=" . VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0));
            //Kiem tra xem co can update APP hay khong
            if ($osType && $osVersionCode) {
                $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
                //Kiem tra xem co can hidden noi dung tinh phi IOS
                if (strtoupper($osType) == 'IOS' && version_compare($osVersionCode, VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0)) > 0) {
                    $secretString = $secretString . "&1";
                } else {
                    //Khong can hidden noi dung tinh phi duyet APP IOS
                    $secretString = $secretString . "&0";
                }
            } else {
                //Khong can hidden noi dung tinh phi duyet APP IOS
                $secretString = $secretString . "&0";
            }
            //Ma hoa accessToken
            $accessToken = Des::RSAEncrypt($secretString);
           
            $result = [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'accessToken' => $accessToken,
                    'refressToken' => $refreshToken,
                    'fullname' => (empty($user) || !$user->full_name) ? (string)Utils::hideMsisdnLast($msisdn) : $user->full_name,
                    'avatarImage' => (!empty($user)) ? VtHelper::getThumbUrl($user['bucket'], $user['path'], VtHelper::SIZE_AVATAR): '',
                    'msisdn' => (string)Utils::hideMsisdnLast($msisdn),
                    'userId' => ($user->id) ? $user->id : 0,
                    'expiredTime' => Yii::$app->params['accessToken.timeout'],
                    'isShowSuggestTopic' => intval($isShowSuggest),
                    'needShowMapAccount' => $needShowMapAccount,
                    'isForceUpdateAPP' => $isForceUpdateAPP,
                    'isUpdateAPP' => ($isShowUpdateAPP)?$isUpdateAPP:0,
                    'theme' => ($user->theme) ? $user->theme : 0,
                ]
            ];

            if (isset($needChangePassword) && $needChangePassword) {
                $result['data']['needChangePassword'] = true;
            } else {
                $result['data']['needChangePassword'] = false;
            }

            Yii::info("AUTH|RESPONSE=" . json_encode($result));

            return $result;

        } else {
            Yii::info("AUTH| osType=" . $osType . "|osVersionCode=" . $osVersionCode . "|configOsVersionCode=" . VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0));
            //Kiem tra xem co can update APP hay khong
            if ($osType && $osVersionCode) {
                $arrForceUpdateAPP = VtConfigBase::getConfig("FORCE_UPDATE_APP_" . strtoupper($osType), 0);
                $isForceUpdateAPP = (in_array($osVersionCode, explode(",", $arrForceUpdateAPP))) ? 1 : 0;
                $isUpdateAPP = (version_compare(VtConfigBase::getConfig("VERSION_APP_" . strtoupper($osType), 0), $osVersionCode) > 0) ? 1 : 0;
            }

            return [
                'responseCode' => ResponseCode::FORBIDDEN,
                'message' => ResponseCode::getMessage(ResponseCode::FORBIDDEN),
                'data' => [
                    'isForceUpdateAPP' => $isForceUpdateAPP,
                    'isUpdateAPP' => ($isShowUpdateAPP)?$isUpdateAPP:0
                ]
            ];
        }
    }

    /**
     * Doi mat khau
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionChangePassword()
    {
        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        $checkEmpty = trim(Yii::$app->request->post('check_empty'));
        if ($checkEmpty == 1) {
            $user = VtUserBase::getActiveUserById($this->userId);
            if ($user->password == '' && $user->salt == '') {
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap', 'Thành công')
                ];
            } else {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Thất bại')
                ];
            }
        }

        $password = Yii::$app->request->post('password', '');
        $newPassword = Yii::$app->request->post('new_password', '');

        $repeatPassword = Yii::$app->request->post('repeat_password', '');
        $captcha = trim(Yii::$app->request->post('captcha', ''));

        $captchaToken = 'no-cap-t-cha';
        $authHeader = trim(Yii::$app->request->getHeaders()->get('Authorization'));
        if (preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches)) {
            $captchaToken = md5(VtHelper::getAgentIp() . $matches[1]);
        }

        $serverCaptcha = Yii::$app->cache->get("captcha_" . $captchaToken);
        // THAY DOI CAPTCHA
        $randomCode = Utils::generateVerifyCode();
        Yii::$app->cache->set("captcha_" . $captchaToken, $randomCode, MobiTVRedisCache::CACHE_15MINUTE);// luu captcha trong 15P

        if (empty($serverCaptcha) || $serverCaptcha !== $captcha) {

            return [
                'responseCode' => ResponseCode::CAPTCHA_INVALID,
                'message' => Yii::t('wap', 'Mã xác nhận không đúng')
            ];
        }

        $user = VtUserBase::getActiveUserById($this->userId);

        $hashCurrentPassword = $user->password;

        $hashInputPassword = sha1($user->salt . $password);
        $hashInputNewPassword = sha1($user->salt . $newPassword);
        if (!$user) {
            return [
                'responseCode' => ResponseCode::INVALID_OLD_PASSWORD,
                'message' => ResponseCode::getMessage(ResponseCode::INVALID_OLD_PASSWORD)
            ];
            //Neu mkcu = mk nhap vao hoac khach hang chua co mat khau
        } elseif ($hashCurrentPassword == $hashInputPassword || ($user->password == '' && $user->salt == '')) {

            if ($newPassword != $repeatPassword) {
                return [
                    'responseCode' => ResponseCode::INVALID_MAP_PASSWORD,
                    'message' => ResponseCode::getMessage(ResponseCode::INVALID_MAP_PASSWORD)
                ];
            } elseif ($hashCurrentPassword == $hashInputNewPassword) {
                return [
                    'responseCode' => ResponseCode::INVALID_MAP_NEW_PASSWORD,
                    'message' => ResponseCode::getMessage(ResponseCode::INVALID_MAP_NEW_PASSWORD)
                ];

            } elseif (!empty($newPassword) && strlen($newPassword) >= 6 && strlen($newPassword) <= 128) {
                $user->salt = md5(uniqid());
                $user->password = sha1($user->salt . $newPassword);
                $user->changed_password = 1;
                $user->save(false);
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap', 'Đổi mật khẩu thành công!')
                ];

            } else {
                return [
                    'responseCode' => ResponseCode::INVALID_NEW_PASSWORD,
                    'message' => ResponseCode::getMessage(ResponseCode::INVALID_NEW_PASSWORD)
                ];
            }
        } else {

            return [
                'responseCode' => ResponseCode::INVALID_OLD_PASSWORD,
                'message' => ResponseCode::getMessage(ResponseCode::INVALID_OLD_PASSWORD)
            ];
        }

    }

    /**
     * Dang xuat
     * @author ducda2@viettel.com.vn
     * @return array
     */
    public function actionLogout()
    {
        $refreshToken = trim(Yii::$app->request->post('refresh_token', ''));
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';

        if (empty($refreshToken)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Dữ liệu không hợp lệ, thiếu refresh_token'),
            ];
        }

        $token = VtUserTokenBase::getByToken($refreshToken);

        if ($token) {

            if ($userAgent == $token['user_agent'] && strtotime($token['token_expired_time']) > strtotime('now')) {
                //Set thoi gian logout = thoi diem hien tai

                $token->token_expired_time = date('Y-m-d H:i:s');
                $token->save();

                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap', 'Đăng xuất thành công'),
                ];
            }
        }

        return [
            'responseCode' => ResponseCode::UNSUCCESS,
            'message' => Yii::t('wap', 'Đăng xuất thất bại'),
        ];

    }


    public function actionGetCaptcha()
    {
        if (!$this->accessToken) {
            return [
                'responseCode' => ResponseCode::FORBIDDEN,
                'message' => ResponseCode::getMessage(ResponseCode::FORBIDDEN)
            ];
        }
        $captchaToken = 'no-cap-t-cha';
        $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
        if (!preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches)) {
            return [
                'responseCode' => ResponseCode::FORBIDDEN,
                'message' => Yii::t('wap', 'accessToken không hợp lệ'),
            ];
        } else {

            $captchaToken = md5(VtHelper::getAgentIp() . $matches[1]);
            $captcha = new \yii\captcha\CaptchaAction(null, $this);
            $randomCode = Utils::generateVerifyCode();
            Yii::$app->cache->set("captcha_" . $captchaToken, $randomCode, MobiTVRedisCache::CACHE_15MINUTE);// luu captcha trong 15P
            $captcha->fixedVerifyCode = $randomCode;
            return $captcha->run();
        }
    }

    /**
     * Nhan tin nhan xac thuc OTP
     * @return array
     */
    public function actionPushOtp($msisdn = '')
    {
        if (!$msisdn) {
            $msisdn = Utils::getMobileNumber(trim(Yii::$app->request->post('msisdn', '')), Utils::MOBILE_GLOBAL);
        }
        if(empty($msisdn)){
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Yêu cầu nhập số điện thoại unitel'),
            ];
        }
        //Neu la WAP
        if (!$msisdn && Yii::$app->id == 'app-wap') {

            $arrTmp = trim(Yii::$app->request->post('GetOTPForm'));
            $msisdn = Utils::getMobileNumber($arrTmp['msisdn'], Utils::MOBILE_GLOBAL);
        }

        if (Utils::isValidMsisdn($msisdn)) {
            $ip = VtHelper::getAgentIp();
            $msisdnNumThisDay = VtUserOtpBase::countPerDayByMsisdn($msisdn);
            $ipNumThisDay = VtUserOtpBase::countPerDayByIp($ip);

            if ($msisdnNumThisDay < Yii::$app->params['otp']['msisdnPerDay'] && $ipNumThisDay < Yii::$app->params['otp']['ipPerDay']) {

                VtUserOtpBase::deactiveAllOtp($msisdn);

                $otp = Utils::generateOtp(Yii::$app->params['otp']['length']);
                $expiredTime = date('Y-m-d H:i:s', strtotime('now') + Yii::$app->params['otp']['timeout']);

                $userOtp = new VtUserOtpBase();
                $userOtp->insertOtp($msisdn, $ip, $otp, $expiredTime);

                $smsMessage = str_replace('%otp%', $otp, VtConfigBase::getConfig('OTP_TEMPLATE'));
                $smsMt = new VtSmsMtBase();
                $smsMt->sendSms($msisdn, $smsMessage);

                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('wap', 'Thành công'),
                ];
            } else {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Quý khách đã nhận quá số lượng tin nhắn xác thực trong ngày'),
                ];
            }
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Số điện thoại không hợp lệ. Quý khách vui lòng nhập định dạng số điện thoại Unitel'),
            ];
        }
    }

    /**
     * Dang ky tai khoan bang so dien thoai
     * @author PhuMX
     */
    public function actionSignUpByMsisdn()
    {

        $msisdn = trim(Yii::$app->request->post('msisdn', ''));
        $password = trim(Yii::$app->request->post('password', ''));
        $otp = trim(Yii::$app->request->post('otp', ''));
        $captcha = trim(Yii::$app->request->post('captcha', ''));

        $captchaToken = 'no-cap-t-cha';
        $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
        if (preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches)) {
            $captchaToken = md5(VtHelper::getAgentIp() . $matches[1]);
        }

        $redisCache = Yii::$app->cache;
        $ip = md5(VtHelper::getAgentIp());
        $numOtpFail = intval($redisCache->get('otp_spam_' . $ip));
        $lockOtpFail = Yii::$app->params['otp.lock.count'];
        $isShowCaptcha = 0;

        if ($numOtpFail >= $lockOtpFail) {
            $isShowCaptcha = 1;
            if (!$captcha) {
                return [
                    'responseCode' => ResponseCode::CAPTCHA_EMPTY,
                    'message' => Yii::t('wap', 'Quý khách vui lòng nhập mã xác nhận'),
                    'captcha' => $isShowCaptcha
                ];
            }
            $serverCaptcha = Yii::$app->cache->get("captcha_" . $captchaToken);
            // refreshCaptcha
            $randomCode = Utils::generateVerifyCode();
            Yii::$app->cache->set("captcha_" . $captchaToken, $randomCode, MobiTVRedisCache::CACHE_15MINUTE);// luu captcha trong 15P

            if (empty($serverCaptcha) || $serverCaptcha !== $captcha) {
                return [
                    'responseCode' => ResponseCode::CAPTCHA_INVALID,
                    'message' => Yii::t('wap', 'Mã xác nhận không hợp lệ'),
                    'captcha' => $isShowCaptcha
                ];
            }
            // refreshCaptcha
            $randomCode = Utils::generateVerifyCode();
            Yii::$app->cache->set("captcha_" . $captchaToken, $randomCode, MobiTVRedisCache::CACHE_15MINUTE);// luu captcha trong 15P

        }
        if (!$msisdn || !$password || !$otp) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Yêu cầu nhập dữ liệu'),
                'captcha' => $isShowCaptcha
            ];
        }

        $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
        // kiem tra thue bao co hop le
        if (!Utils::isValidMsisdn($msisdn)) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Số thuê bao không hợp lệ'),
                'captcha' => $isShowCaptcha
            ];
        };
        if (strlen(trim($password)) < 8 || strlen(trim($password)) > 128) {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('wap', 'Độ dài mật khẩu từ 8 đến 128 kí tự'),
                'captcha' => $isShowCaptcha
            ];
        }

        // Kiem tra otp co ton tai
        if (VtUserOtpBase::checkOTP($msisdn, $otp)) {
            // Kiem tra xem user co ton tai?
            if (!VtUserBase::getByMsisdn($msisdn)) {
                $salt = uniqid('', true);
                $hashPassword = hash('sha1', $salt . $password);
                // Thuc hien dang ky
                $objUser = new VtUserBase();
                $objUser->insertUserMsisdn(
                    Yii::$app->getRequest()->getUserIP(),
                    $msisdn,
                    Yii::$app->getRequest()->getUserAgent(),
                    $hashPassword,
                    $salt
                );
                // xoa key
                $isShowCaptcha = 0;
                $redisCache->delete('otp_spam_' . $ip);
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => Yii::t('web', 'Đăng ký thành công'),
                    'captcha' => $isShowCaptcha
                ];
            } else {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Đăng ký không thành công, số thuê bao đã tồn tại'),
                    'captcha' => $isShowCaptcha
                ];
            }

        } else {

            if ($numOtpFail >= $lockOtpFail) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Mã OTP không đúng hoặc đã hết hạn'),
                    'captcha' => $isShowCaptcha
                ];
            } else {
                $redisCache->set('otp_spam_' . $ip, $numOtpFail + 1, MobiTVRedisCache::CACHE_15MINUTE);
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => Yii::t('wap', 'Mã OTP không đúng hoặc đã hết hạn'),
                    'captcha' => $isShowCaptcha
                ];
            }

        }
    }

    public function actionTestCache()
    {
        if (!YII_DEBUG) {
            return ('Not available');
        }

        $redisCache = Yii::$app->cache;
        $msisdn = Yii::$app->request->get('msisdn');

        $ip = md5(VtHelper::getAgentIp());
        $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
        echo "IP = " . VtHelper::getAgentIp() . "<br>";
        echo "FAIL NUMBER = " . $countLoginFail . "<br>";

        $ipUser = md5(VtHelper::getAgentIp() . $msisdn);
        $countLock = intval($redisCache->get('count_lock_' . $ipUser));
        echo "MSISDN=" . $msisdn . " & IP=" . VtHelper::getAgentIp() . "| FAIL LOCK=" . $countLock;

        return null;
    }

    /**
     * change theme dark - light mode
     * @author Manhdt
     */
    public function actionThemeMsisdn()
    {
        $theme = trim(Yii::$app->request->post('theme', ''));       
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        if($this->userId){
            $objUser = VtUserBase::getById($this->userId);

            if($objUser->theme == 0){
                $objUser->theme = 1;
            }else{
                $objUser->theme = 0;
            }

            $objUser->save(false);
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => Yii::t('web', 'Thay đổi giao diện thành công'),
                'theme' =>$objUser->theme,
            ];
        }

    }
}