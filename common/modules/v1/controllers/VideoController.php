<?php
namespace common\modules\v1\controllers;


use Aws\CloudFront\Exception\Exception;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\libs\VtFlow;
use common\models\VtConfigBase;
use common\models\VtHistoryViewBase;
use common\models\VtUserBase;
use common\modules\v1\libs\VideoObj;
use Yii;
use common\models\VtFavouriteVideoBase;
use common\models\VtUserFollowBase;
use common\models\VtVideoBase;
use common\modules\v1\libs\ResponseCode;
use common\modules\v1\libs\UserFollowObj;
use common\modules\v1\libs\Obj;
use common\controllers\ApiController;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\VtGroupCategoryBase;
use common\helpers\Utils;

class VideoController extends ApiController
{

    public function actionGetDetail($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = null)
    {
        $id = trim(Yii::$app->request->get('id', 0));
        $currentTime = 0;
        if (!$profileId) {
            $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['streaming.vodProfileId']));

        }
        $playlistId = trim(Yii::$app->request->get('playlist_id', false));

        if(!isset($acceptLossData)){
            $acceptLossData = Yii::$app->request->get("accept_loss_data", 0);
        }


        $video = VtVideoBase::getDetail($id);
        if (empty($video)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => 'Video không tồn tại.',
            ];
        }
        $detailObj = array();
        $detailObj['id'] = $video['id'];
        $detailObj['name'] = $video['name'];
        $detailObj['description'] = $video['description'];
        $detailObj['type'] = $video['type'];
        $detailObj['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_COVER);
        $detailObj['likeCount'] = $video['like_count'];
        $detailObj['play_times'] = number_format($video['play_times'], 0, ',', '.');
        $detailObj['suggest_package_id'] = $video['suggest_package_id'];

        if ($this->userId) {
            $detailObj['isFavourite'] = (VtFavouriteVideoBase::checkIsFavourite($this->userId, $id)) ? 1 : 0;
        } else {
            $detailObj['isFavourite'] = 0;
        }

        // TODO hardcode
        $detailObj['link'] = Yii::$app->params['cdn.site'] . "/video/" . $id."/".$video['slug']."?utm_source=APPSHARE";

        // them truong du lieu neu la WAP
        if (Yii::$app->id == 'app-wap') {
            $detailObj['slug'] = $video['slug'];
        }

        // TODO hardcode
        // Lay thong tin thue bao
        $objUser = VtUserBase::getById($video['created_by']);

        $detailObj['owner']['id'] = $objUser['id'];
        $detailObj['owner']['name'] = ($objUser['full_name']) ? $objUser['full_name'] :  (($objUser['msisdn']) ? substr($objUser['msisdn'], 0, -3) . "xxx" : "" );
        $detailObj['owner']['avatarImage'] = VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR);
        $detailObj['owner']['followCount'] = $objUser['follow_count'];
        if ($this->userId) {
            $detailObj['owner']['isFollow'] = (VtUserFollowBase::getFollow($this->userId, $video['created_by'])) ? 1 : 0;
        } else {
            $detailObj['owner']['isFollow'] = 0;
        }

        if ($objUser) {
            $videoUserFollow = VtVideoBase::getVideosByUser(false, $video['created_by'], Yii::$app->params['app.video.relate.limit'], 0)->all();
            $c = 0;
            foreach ($videoUserFollow as $v) {
                $detailObj['owner']['videos'][$c]['id'] = $v['id'];
                $detailObj['owner']['videos'][$c]['name'] = $v['name'];
                $detailObj['owner']['videos'][$c]['coverImage'] = VtHelper::getThumbUrl($v['bucket'], $v['path'], VtHelper::SIZE_COVER);
                $detailObj['owner']['videos'][$c]['slug'] = $v['slug'];
                $detailObj['owner']['videos'][$c]['duration'] = Utils::durationToStr($v['duration']);
                $c++;
            }
        }

        $streamingObj = VtFlow::viewVideo($this->msisdn, $video, $playlistId, $profileId, $this->userId, $this->authJson, $supportType, $acceptLossData);

        //An noi dung tinh phi khi duyet APP iOS
        if ($this->needHiddenFreemiumContent) {
            if (array_key_exists('popup', $streamingObj)) {
                $streamingObj['popup'] = [];
            }
        }

        $relateds = VideoObj::serialize(
            Obj::VIDEO_RELATE,
            VtVideoBase::getRelatedByCategoryQuery($video['id'], $video['category_id'], Yii::$app->params['app.video.relate.limit'], 0),
            false
        );

        if ($this->userId || $this->msisdn) {
            $history = VtHistoryViewBase::getByItemId($this->userId, $this->msisdn, $video['type'], $id);
            $currentTime = ($history) ? intval($history->time) : 0;
        }
        //var_dump($detailObj);die();
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'detail' => $detailObj,
                'profile' => [],
                'streams' => $streamingObj,
                'relateds' => $relateds,
                'currentTime' => $currentTime
            ]
        ];

    }

    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionToggleLikeVideo()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => 'Quý khách vui lòng đăng ký thành viên để sử dụng tính năng này!',
                ];
            } else {
                return $this->authJson;
            }
        }

        $id = trim(Yii::$app->request->post('id', 0));

        $video = VtVideoBase::getDetail($id);

        if (empty($video)) {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND),
            ];
        }

        $favObj = VtFavouriteVideoBase::getFavourite($this->userId, $id);

        if (!empty($favObj)) {
            $favObj->delete();
            $isLike = false;
        } else {
            $favObj = new VtFavouriteVideoBase();
            $favObj->insertFavourite($this->userId, $id);
            $isLike = true;
        }

        VtVideoBase::updateLikeCount($id, $isLike);

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => [
                'isLike' => $isLike
            ]
        ];
    }

    public function actionGetHomeVideo()
    {
        try {
            if ($this->userId || $this->msisdn) {
                $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, 200);

                $durationPercent = [];
                foreach ($histories as $history) {
                    $historyIds[] = $history['itemId'];
                    $parentIds[$history['itemId']] = $history['parent_id'];
                    if ($history['duration']) {
                        $durationPercent[$history['itemId']] = 100 * $history['time'] / $history['duration'];
                    } else {
                        $durationPercent[$history['itemId']] = 0;
                    }
                }
                // Lay limit theo page.limit
                if (!empty($historyIds)) {
                    $historyVideos = VideoObj::serialize(
                        Obj::VIDEO_HISTORY,
                        VtVideoBase::getByIdsQuery(VtVideoBase::VOD_FILTER, $historyIds, Yii::$app->params['app.page.limit'], 0)
                            ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')])
                    );
                }
            }
        } catch (\Exception $ex) {
            Yii::error("[HOME_VIDEO]Get history error:" . $ex->getMessage());
        }

        $recommendVideos = VideoObj::serialize(
            Obj::VIDEO_RECOMMEND,
            VtVideoBase::getRecommendVideo(VtVideoBase::VOD_FILTER, Yii::$app->params['app.page.limit']),
            true
        );

        $dataResponse = array();

        if (isset($historyVideos['content']) && $historyVideos['content']) {
            $dataResponse[] = $historyVideos;
        }

        if (isset($recommendVideos['content']) && $recommendVideos['content']) {
            $dataResponse[] = $recommendVideos;
        }
        //@todo hardcode off cache
        $cfArrVodSubType = Utils::getSystemConfig('video.category.list', false);
        $arrVodSubType = explode(",", $cfArrVodSubType);
        foreach ($arrVodSubType as $categoryId) {
            if ($categoryId > 0) {
                $objCategory = VtGroupCategoryBase::getCategoryGroup($categoryId, VtGroupCategoryBase::TYPE_VOD);
                if ($objCategory) {
                    $tmpBox = VideoObj::serialize(
                        Obj::CATEGORY_GROUP . $categoryId,
                        VtVideoBase::getVideosByCategory($categoryId, Yii::$app->params['app.page.limit'], 0),
                        true,
                        $objCategory['name']
                    );
                    if (count($tmpBox['content'])) {
                        $dataResponse[] = $tmpBox;
                    }
                }
            }
        }
        /*
        $boxVideoFree = VideoObj::serialize(
            Obj::VIDEO_FREE,
            VtVideoBase::getVideosFree(VtVideoBase::VOD_FILTER, Yii::$app->params['app.page.limit'], 0),
            true
        );

        if ($boxVideoFree['content']) {
            $dataResponse[] = $boxVideoFree;
        }
*/
        //@todo hardcode off cache
        //- IF CONFIG ORDER BOX
        $orderStr = Utils::getSystemConfig('video.order.list', true);
        if ($orderStr) {
            $tmpBox = array();
            $orderList = explode(",", $orderStr);
            foreach ($orderList as $index) {
                if (isset($dataResponse[$index])) {
                    $tmpBox[] = $dataResponse[$index];
                }
            }
            $dataResponse = $tmpBox;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];

    }

    public function actionGetHomeMusic()
    {
        try {
            if ($this->userId || $this->msisdn) {
                $category = explode(",", VtConfigBase::getConfig("music.category.list"));
                //Lay 100 item( sau do loc lay app.page.limit item)
                $histories = VtHistoryViewBase::getByCategory($this->userId, $this->msisdn, $category, 200);

                $historyIds = array();
                foreach ($histories as $history) {
                    $historyIds[] = $history['_source']['item_id'];
                }
                // Lay limit theo page.limit
                if (!empty($historyIds)) {
                    $historyVideos = VideoObj::serialize(
                        Obj::VIDEO_HISTORY,
                        VtVideoBase::getByIdsQuery(VtVideoBase::MUSIC_FILTER,$historyIds, Yii::$app->params['app.page.limit'], 0)
                            ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($historyIds)) . ')')])
                    );
                }
            }
        } catch (\Exception $ex) {
            Yii::error("[HOME_VIDEO]Get history error:" . $ex->getMessage());
        }

        $recommendVideos = VideoObj::serialize(
            Obj::MUSIC_RECOMMEND,
            VtVideoBase::getRecommendVideo(VtVideoBase::MUSIC_FILTER, Yii::$app->params['app.page.limit']),
            true
        );

        $dataResponse = array();

        if (isset($historyVideos['content']) && $historyVideos['content']) {
            $dataResponse[] = $historyVideos;
        }

        if (isset($recommendVideos['content']) && $recommendVideos['content']) {
            $dataResponse[] = $recommendVideos;
        }
        //@todo hardcode off cache
        $cfArrVodSubType = Utils::getSystemConfig('music.category.list', false);
        $arrVodSubType = explode(",", $cfArrVodSubType);

        foreach ($arrVodSubType as $categoryId) {
            if ($categoryId > 0) {
                $objCategory = VtGroupCategoryBase::getCategoryGroup($categoryId, VtGroupCategoryBase::TYPE_VOD);
                if ($objCategory) {
                    $tmpBox = VideoObj::serialize(
                        Obj::CATEGORY_GROUP . $categoryId,
                        VtVideoBase::getVideosByCategory($categoryId, Yii::$app->params['app.page.limit'], 0),
                        true,
                        $objCategory['name']
                    );
                    if (count($tmpBox['content'])) {
                        $dataResponse[] = $tmpBox;
                    }
                }
            }
        }
        /*
        $boxVideoFree = VideoObj::serialize(
            Obj::MUSIC_FREE,
            VtVideoBase::getVideosFree(VtVideoBase::MUSIC_FILTER, Yii::$app->params['app.page.limit'], 0),
            true
        );

        if ($boxVideoFree['content']) {
            $dataResponse[] = $boxVideoFree;
        }
*/
        //@todo hardcode off cache
        //- IF CONFIG ORDER BOX
        $orderStr = Utils::getSystemConfig('music.order.list', true);
        if ($orderStr) {
            $tmpBox = array();
            $orderList = explode(",", $orderStr);
            foreach ($orderList as $index) {
                if (isset($dataResponse[$index])) {
                    $tmpBox[] = $dataResponse[$index];
                }
            }
            $dataResponse = $tmpBox;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];

    }


    public function actionGetFriendsVideo()
    {
        $filterType = Yii::$app->request->get("filter_type", '');

        if (!$this->isValidUser()) {
            return $this->authJson;
        }

        switch (strtoupper($filterType)) {
            case "VIDEO":
                $boxUserFollow = UserFollowObj::serialize(
                    Obj::USER_FOLLOW_VIDEO,
                    VtUserFollowBase::getFollowUser($this->userId, Yii::$app->params['app.page.limit'], 0)
                );
                break;
            case "MUSIC":
                $boxUserFollow = UserFollowObj::serialize(
                    Obj::USER_FOLLOW_MUSIC,
                    VtUserFollowBase::getFollowUser($this->userId, Yii::$app->params['app.page.limit'], 0)
                );
                break;
            default:
                $boxUserFollow = UserFollowObj::serialize(
                    Obj::USER_FOLLOW,
                    VtUserFollowBase::getFollowUser($this->userId, Yii::$app->params['app.page.limit'], 0)
                );
                break;
        }


        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $boxUserFollow
        ];

    }

    public function actionGetNewVideo()
    {
        $newVideo = VideoObj::serialize(
            Obj::VIDEO_NEW,
            VtVideoBase::getNewVideo(Yii::$app->params['app.page.limit']),
            true
        );

        $dataResponse = array();

        if (isset($newVideo['content']) && $newVideo['content']) {
            $dataResponse[] = $newVideo;
        }

        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];

    }

    public function actionGetVideoStream()
    {
        $id = trim(Yii::$app->request->get('id'));
        $profileId = trim(Yii::$app->request->get('profile_id', Yii::$app->params['streaming.vodProfileId']));
        $playlistId = trim(Yii::$app->request->get('playlist_id', false));
        $acceptLossData = Yii::$app->getSession()->get("accept_loss_data", 0);

        $objVideo = VtVideoBase::getDetail($id);

        if ($objVideo) {
            $streamingObj = VtFlow::viewVideo($this->msisdn, $objVideo, $playlistId, $profileId, $this->userId, $this->authJson, $supportType, $acceptLossData);
            //An noi dung tinh phi khi duyet APP iOS
            if ($this->needHiddenFreemiumContent) {
                if (array_key_exists('popup', $streamingObj)) {
                    $streamingObj['popup'] = [];
                }
            }
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                'data' => [
                    'streams' => $streamingObj,
                ]
            ];
        } else {
            return [
                'responseCode' => ResponseCode::NOT_FOUND,
                'message' => ResponseCode::getMessage(ResponseCode::NOT_FOUND)
            ];
        }

    }

}