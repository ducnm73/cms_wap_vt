<?php

namespace common\modules\v1\libs;

use common\helpers\Utils;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\modules\v1\libs\Obj;

class PlaylistObj
{

    public static function serialize($type, $id, $query, $cache = false, $name = null)
    {
        $appId = Yii::$app->id;

        $key = $id . "_" . $appId."_".$query->limit."_".$query->offset;

        if ($cache && \Yii::$app->params['cache.enabled']) {
            $contents = \Yii::$app->cache->get($key);

            if ($contents === false) {
                $contents = $query->all();
                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $result = array();
        foreach ($contents as $content) {
            $item = array();

            $item['id'] = $content['id'];
            $item['name'] = $content['name'];
            $item['description'] = $content['description'];
            $item['slug'] = $content['slug'];
            $item['play_times'] = Utils::convertPlayTimes($content['play_times']);
            $item['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_FILM);
            $item['type'] = $content['type'];
            $result[] = $item;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'type' => $type,
            'content' => $result
        ];


    }
}