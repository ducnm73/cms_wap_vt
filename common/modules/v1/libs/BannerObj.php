<?php

namespace common\modules\v1\libs;

use api\models\VtGroupCategory;
use common\helpers\Utils;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;

class BannerObj
{

    public static function serialize($id, $query, $cache = false, $location, $parentId = '')
    {
        $key = $id . "_" . $location;
        //chi cache khi khong lay lich su xem cua khach hang
        if ($cache && \Yii::$app->params['cache.enabled'] && !$parentId) {
            $contents = \Yii::$app->cache->get($key);
            if ($contents === false) {
                $contents = $query->all();
                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $categories = VtGroupCategory::getAllCategory();

        $arrCateType = [];
        foreach ($categories as $cate) {
            $arrCateType[$cate['id']] = $cate['type'];
        }

        $banners = array();
        foreach ($contents as $content) {
            $banner = array();
            $banner['id'] = $content['id'];
            $banner['name'] = $content['name'];
            $banner['description'] = '';
            $content['history_view'] = 0;
            // Neu lay du lieu xem lai cua phim
            if (($parentId[$content['item_id']] > 0)) {
                $banner['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO);
                //select truc tiep tap phim can xem
                $content['video_id'] = $content['item_id'];
                $banner['video_id'] = $content['item_id'];
                $banner['history_view'] = 1;

            } else {
                $banner['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_BANNER);
            }
            $banner['type'] = $content['type'];
            //Neu co parentId thi lay ID cua parent( tap phim thi lay id cua bo phim)
            $content['item_id'] = ($parentId[$content['item_id']] > 0) ? $parentId[$content['item_id']] : $content['item_id'];

            switch ($content['type']) {
                case 'TOPIC':
                    $content['item_id'] = Obj::TOPIC_GROUP . $content['item_id'];
                    $banner['boxType'] = 'VOD';
                    break;
                case 'CATEGORY':
                    $banner['boxType'] = ($arrCateType[$content['item_id']]) ? $arrCateType[$content['item_id']] : "VOD";
                    $content['item_id'] = Obj::CATEGORY_GROUP . $content['item_id'];
                    break;
            }

            $banner['itemId'] = $content['item_id'];
            $banner['link'] = $content['href'];
            // them truong du lieu neu la WAP
            if (Yii::$app->id == 'app-wap') {
                $banner['redirectUrl'] = Utils::createLinkBanner($content);
            }
            $banners[] = $banner;
        }

        return [
            'id' => $key,
            'name' => '',
            'type' => 'BANNER',
            'content' => $banners
        ];

    }
}
