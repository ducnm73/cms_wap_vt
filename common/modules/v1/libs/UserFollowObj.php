<?php

namespace common\modules\v1\libs;

use common\helpers\Utils;
use common\models\VtVideoBase;
use Monolog\Handler\Curl\Util;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;
use common\modules\v1\libs\Obj;

class UserFollowObj
{

    public static function serialize($id, $query, $cache = false, $name = null)
    {
        $appId = Yii::$app->id;

        $key = $id . "_" . $appId."_".$query->limit."_".$query->offset;

        if ($cache && \Yii::$app->params['cache.enabled']) {
            $contents = \Yii::$app->cache->get($key);

            if ($contents === false) {
                $contents = $query->all();
                \Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $result = array();
        foreach ($contents as $content) {

            $item = array();
            $item['friend_id'] = $content['id'];
            $item['friend_name'] = ($content['full_name']) ? $content['full_name'] : $content['msisdn'];
            $item['avatarImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_AVATAR);

            switch($id){
                case Obj::USER_FOLLOW_VIDEO:
                    $videos = VtVideoBase::getVideosByUser(VtVideoBase::VOD_FILTER, $content['id'], Yii::$app->params['video.folow.limit'], 0)->all();
                    break;
                case Obj::USER_FOLLOW_MUSIC:
                    $videos = VtVideoBase::getVideosByUser(VtVideoBase::MUSIC_FILTER, $content['id'], Yii::$app->params['video.folow.limit'], 0)->all();
                    break;
                default:
                    $videos = VtVideoBase::getVideosByUser(false, $content['id'], Yii::$app->params['video.folow.limit'], 0)->all();
                    break;
            }

            $tmpVideos = array();
            $cc=0;
            foreach ($videos as $video) {
                $tmpVideos[$cc]['id'] = $video['id'];
                $tmpVideos[$cc]['name'] = $video['name'];
                $tmpVideos[$cc]['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_VIDEO);
                $tmpVideos[$cc]['duration'] = Utils::durationToStr($video['duration']);
                $tmpVideos[$cc]['slug'] = $video['slug'];
                $tmpVideos[$cc]['type'] = 'VOD';
                $cc++;
            }
            $item['videos'] = $tmpVideos;
            $result[] = $item;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'content' => $result,
            'type'=> 'USER_FOLLOW_VIDEO'
        ];


    }
}