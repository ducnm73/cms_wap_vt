<?php

namespace common\modules\v1\libs;

use common\helpers\Utils;
use League\Flysystem\Util;
use Yii;
use common\libs\VtHelper;
use common\helpers\MobiTVRedisCache;

class VideoObj
{

    public static function serialize($id, $query, $cache = false, $name = null, $type = null)
    {
        $appId = Yii::$app->id;

        $key = $id . "_" . $appId . "_" . $query->limit . "_" . $query->offset;

        if ($cache && Yii::$app->params['cache.enabled']) {
            $contents = Yii::$app->cache->get($key);
            if ($contents === false) {
                $contents = $query->all();
                Yii::$app->cache->set($key, $contents, MobiTVRedisCache::CACHE_10MINUTE);
            }
        } else {
            $contents = $query->all();
        }

        $videos = array();
        foreach ($contents as $content) {
            $video = array();
            $video['id'] = $content['id'];
            $video['name'] = $content['name'];
            $video['description'] = $content['description'];
            $video['coverImage'] = VtHelper::getThumbUrl($content['bucket'], $content['path'], VtHelper::SIZE_VIDEO);
            $video['type'] = 'VOD';
            $video['duration'] = Utils::durationToStr($content['duration']);
            $video['play_times'] =  Utils::convertPlayTimes($content['play_times']);
            $video['publishedTime'] = Utils::time_elapsed_string($content['published_time']);

            if (isset($content['user_bucket']) && isset($content['user_bucket'])) {
                $video['userAvatarImage'] = VtHelper::getThumbUrl($content['user_bucket'], $content['user_path'], VtHelper::SIZE_AVATAR);
            }

            if (isset($content['full_name']) || isset($content['msisdn'])) {
                $video['userName'] = ($content['full_name']) ? $content['full_name'] : $content['msisdn'];
            }

            if (isset($content['user_id'])) {
                $video['userId'] = $content['user_id'];
            }

            // them truong du lieu neu la WAP
            if ($appId == 'app-wap') {
                $video['slug'] = $content['slug'];
            }
            $videos[] = $video;
        }

        return [
            'id' => $id,
            'name' => ($name) ? $name : Obj::getName($id),
            'type' => isset($type) ? $type : 'VOD',
            'content' => $videos
        ];

    }

}
