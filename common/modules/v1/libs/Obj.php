<?php

namespace common\modules\v1\libs;

use Yii;

class Obj
{
    const CATEGORY_GROUP = 'category_group_';
    const TOPIC_GROUP = 'topic_group_';
    const VIDEO_OF_USER = 'video_of_user_';

    const BANNER = 'banner';
    const FOCUS = 'focus';
    const VIDEO_PAID = 'VIDEO_PAID';

    const VIDEO_SEARCH = 'video_search';
    const VIDEO_NEWSFEED = 'video_newsfeed';
    const VIDEO_NEW = 'video_new';
    const VIDEO_HOT = 'video_hot';
    const VIDEO_HISTORY = 'video_history';
    const VIDEO_RECOMMEND = 'video_recommend';
    const VIDEO_FREE = 'video_free';
    const VIDEO_RELATE = 'video_relate';
    const MUSIC_NEW = 'music_new';
    const MUSIC_RECOMMEND = 'music_recommend';
    const MUSIC_HISTORY = 'music_history';
    const MUSIC_FREE = 'music_free';

    const PLAYLIST_SEARCH = 'playlist_search';
    const USER_FOLLOW = 'user_follow';
    const USER_FOLLOW_VIDEO = 'user_follow_video';
    const USER_FOLLOW_MUSIC = 'user_follow_music';

    const FILM_HOT = 'film_hot';
    const FILM_RECOMMEND = 'film_recommend';
    const FILM_NEW = 'film_new';
    const FILM_FREE = 'film_free';
    const FILM_RELATE = 'film_relate';

    const MEMBER = 'member';
    const MEMBER_FOLLOW = 'member_follow';
    const MEMBER_FOLLOW_VIEW = 'member_follow_';


    const VIDEO_USER_LIKE = 'video_user_like_';

    public static function getName($name)
    {
        $mess = [
            self::BANNER => Yii::t('api', 'Banner'),
            self::VIDEO_SEARCH => Yii::t('api', 'Tìm kiếm video'),
            self::VIDEO_NEWSFEED => Yii::t('api', 'News feed'),
            self::VIDEO_NEW => Yii::t('api', 'Video mới cập nhật'),
            self::MUSIC_NEW => Yii::t('api', 'Nhạc mới cập nhật'),
            self::MUSIC_RECOMMEND => Yii::t('api', 'Có thể bạn thích'),
            self::VIDEO_HOT => Yii::t('api', 'Video hot'),
            self::VIDEO_HISTORY => Yii::t('api', 'Xem gần đây'),
            self::VIDEO_RECOMMEND => Yii::t('api', 'Có thể bạn thích'),
            self::VIDEO_FREE => Yii::t('api', 'Video miễn phí'),
            self::PLAYLIST_SEARCH => Yii::t('api', 'Tìm kiếm Phim'),
            self::FILM_HOT => Yii::t('api', 'Hot nhất tuần'),
            self::FILM_RECOMMEND => Yii::t('api', 'Phim đề xuất'),
            self::FILM_NEW => Yii::t('api', 'Phim mới nhất'),
            self::FILM_FREE => Yii::t('api', 'Phim miễn phí'),
            self::CATEGORY_GROUP => Yii::t('api', 'Thể loại'),
            self::MEMBER => Yii::t('api', 'Thành viên'),
            self::MEMBER_FOLLOW => Yii::t('api', 'Đang theo dõi'),
            self::USER_FOLLOW => Yii::t('api', 'Theo dõi'),
            self::USER_FOLLOW_VIDEO => Yii::t('api', 'Theo dõi'),
            self::USER_FOLLOW_MUSIC => Yii::t('api', 'Theo dõi'),
            self::MUSIC_HISTORY => Yii::t('api', 'Xem gần đây'),
            self::MUSIC_FREE => Yii::t('api', 'Nhạc miễn phí'),

        ];
        if ($mess[$name]) {
            return $mess[$name];
        }
        return '';
    }


}
