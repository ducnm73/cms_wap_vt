<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class VtChannelFollowBase extends \common\models\db\VtChannelFollowDB {
    const FOLLOW = 1;
    const UNFOLLOW = 0;

    const ACTIVE = 1;
    const DEACTIVE = 0;
    const NOTIFICATION_TYPE_NEVER = 0;
    const NOTIFICATION_TYPE_SOMETIME = 1;
    const NOTIFICATION_TYPE_ALWAYS = 2;

    public static function getFollowUser($userId, $limit = null, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->from('vt_channel_follow uf')
            ->select('u.*, uf.follow_id')
            ->innerJoin('vt_user u', 'u.id = uf.follow_id and u.status = :status', [':status' => self::ACTIVE])
            ->where(['uf.user_id' => $userId])
            ->orderBy('uf.id DESC');
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }


    public static function getFollowUserIds($userId)
    {
        $query = self::find()
            ->asArray()
            ->from('vt_channel_follow uf')
            ->innerJoin('vt_user as u', 'uf.follow_id = u.id')
            ->select('uf.follow_id')
            ->where(['uf.user_id' => $userId, 'u.status' => VtUserBase::ACTIVE]);

        $results = $query->all();

        return ArrayHelper::getColumn($results, 'follow_id');
    }

    public static function getFollowUserNotification($userId)
    {
        $query = self::find()
            ->asArray()
            ->from('vt_channel_follow uf')
            ->select('uf.follow_id')
          //  ->leftJoin('vt_user u', 'u.id = uf.follow_id and u.status = :status', [':status' => self::ACTIVE])
            ->where(['uf.user_id' => $userId])
            ->andWhere(['<>','uf.notification_type', 0])
            ->orderBy('uf.id DESC');

        return $query->all();
    }

    /**
     *
     * @param $user_id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getFollow($userId, $followId)
    {
        $query = self::find()
            ->where('user_id = :user_id', [':user_id' => $userId])
            ->andWhere(['follow_id' => $followId]);

        return $query->one();
    }

    public function insertFollow($userId, $id, $notificationType = self::NOTIFICATION_TYPE_ALWAYS)
    {
        $this->user_id = $userId;
        $this->follow_id = $id;
        $this->notification_type = $notificationType;
        $this->save(false);
    }

    public static function getChannelFollowQuery($userId, $limit = null, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->from('vt_channel_follow uf')
            ->select(['u.*', 'uf.follow_id', new Expression("1 as is_follow")])
            ->innerJoin('vt_channel u', 'u.id = uf.follow_id')
            ->where(['uf.user_id' => $userId, 'u.status'=>1])
            ->orderBy('uf.id DESC');
        
        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }
//var_dump($query->createCommand()->getRawSql());die;
        return $query;
    }

    public static function countFollowByChannel($channelId)
    {
        $query = self::find()
            ->from('vt_channel_follow uf')
            ->where(['uf.follow_id' => $channelId]);

        return $query->count("id");
    }

    public static function getChannelHotQuery($userId, $limit = null, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->from('vt_channel_follow uf')
            ->select('u.*, uf.follow_id')
            ->innerJoin('vt_channel u', 'u.id = uf.follow_id and u.status = :status', [':status' => self::ACTIVE])
            ->where(['uf.user_id' => $userId, 'u.is_hot' => 1])
            ->orderBy('u.priority DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }

        return $query;
    }
    /* code by tran manh */
    public static function getAllUserFollow($type = null) {
        $query = self::find()
            ->asArray();
        return $query->all();
    }

    // update channel_related_id
    public static function updateUserFollowId($Id,$follow_id) {
        return self::updateAll(
            [
                'follow_id' => $follow_id,
            ], 'id = :id', [
                ':id' => $Id
            ]
        );
    }
    public static function removeChannelFollow($userId, $followId)
    {
        $objFollow = self::findOne(['user_id' => $userId, 'follow_id' => $followId]);
        if ($objFollow) {
            $objFollow->delete();
            return true;
        } else {
            return false;
        }
    }
}