<?php

namespace common\models;

use backend\models\VtPlaylistItem;
use Yii;

class VtBuyPlaylistBase extends \common\models\db\VtBuyPlaylistDB
{
    /**
     * @author PhuMX
     * Kiem tra xem khach hang da mua playlist chua video chua?
     * @param $msisdn
     * @param $video
     */
    public static function checkBuyPlaylist($msisdn, $video)
    {
        return self::find()
            ->asArray()
            ->from('vt_buy_playlist bp')
            ->leftJoin('vt_playlist_item pi', 'bp.playlist_id=pi.playlist_id AND bp.msisdn=:msisdn', [':msisdn' => $msisdn])
            ->where(['bp.msisdn' => $msisdn, 'pi.item_id' => $video['id']])
            ->one();
    }

    public function insertTransaction($msisdn, $userId, $itemId)
    {
        $this->msisdn = $msisdn;
        $this->user_id = $userId;
        $this->playlist_id = $itemId;
        $this->created_at = date("Y-m-d H:i:s");
        $this->save(false);
    }


}