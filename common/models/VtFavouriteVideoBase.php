<?php

namespace common\models;

use common\libs\VtHelper;
use Yii;

class VtFavouriteVideoBase extends \common\models\db\VtFavouriteVideoDB
{

    const STATUS_DISLIKE = -1;
    const STATUS_LIKE = 1;
    const STATUS_REMOVE = 0;

    /**
     * Lay ra thong tin yeu thich cua KH
     * @param $user_id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getFavourite($userId, $id)
    {
        $query = self::find()
            ->where('user_id = :user_id', [':user_id' => $userId])
            ->andWhere(['video_id' => $id]);

        return $query->one();
    }

    public function insertFavourite($userId, $id, $status)
    {
        $this->user_id = $userId;
        $this->video_id = $id;
        $this->status = $status;
        $this->save(false);
    }

    /**
     * @author PhuMX
     * @param $userId
     * Lay danh sach video yeu thich cua user
     */
    public static function getListVideos($userId, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*')
            ->from('vt_favourite_video f')
            ->leftJoin('vt_video v', 'f.video_id=v.id')
            ->where(['f.user_id' => $userId, 'v.status' => VtVideoBase::STATUS_APPROVE, 'v.is_active' => VtVideoBase::ACTIVE])
            ->andWhere('v.published_time <= :published_time', [':published_time' => VtHelper::currentCacheTime()])
            ->orderBy('f.id DESC');
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    public static function checkIsFavourite($userId, $videoId){

        return self::find()->asArray()
            ->where(['user_id'=>$userId, 'video_id'=>$videoId])
            ->one();
    }

}