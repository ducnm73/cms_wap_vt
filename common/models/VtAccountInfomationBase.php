<?php

namespace common\models;

use Yii;

class VtAccountInfomationBase extends \common\models\db\VtAccountInfomationDB {
    public static function getByUserId($userId){
        return self::find()
            ->where(["user_id" => $userId])
            ->one();
    }
}