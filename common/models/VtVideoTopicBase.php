<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtVideoTopicBase extends \common\models\db\VtVideoTopicDB
{

    public function insertVideo($videoId, $topicId)
    {

        if(!self::checkVideoInTopic($videoId, $topicId)){
            $this->video_id = $videoId;
            $this->topic_id = $topicId;
            $this->save();
        }

    }

    public static function checkVideoInTopic($videoId, $topicId){
        $query = self::find()
            ->where([
                'video_id' => $videoId,
                'topic_id' => $topicId
            ]);
        return $query->count();
    }


    /**
     * @author PhuMX
     * Lay danh sach video thuoc chu de
     * @param $topicId
     * @param bool $limit
     * @param bool $offset
     * @return $this
     */
    public static function getVideosTopic($topicId, $limit = false, $offset = false)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*')
            ->from('vt_group_topic t')
            ->leftJoin('vt_video_topic vt', 't.id=vt.topic_id AND vt.topic_id=:topicId', [':topicId' => $topicId])
            ->leftJoin('vt_video v', 'v.id = vt.video_id')
            ->where([
                't.id' => $topicId,
                't.is_active' => VtGroupTopicBase::ACTIVE,
                'v.status' => VtVideoBase::STATUS_APPROVE,
                'v.is_active' => VtVideoBase::ACTIVE,])
            ->andWhere('v.published_time<=:publishedTime', [':publishedTime' => Utils::currentCacheTime()])
            ->orderBy('vt.id DESC');
        if($limit){
            $query->limit($limit);
        }
        if($offset){
            $query->offset($offset);
        }

        return $query;
    }


}