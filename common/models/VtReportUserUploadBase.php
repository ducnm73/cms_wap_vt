<?php

namespace common\models;

use Elasticsearch\ClientBuilder;
use Yii;
use yii\helpers\ArrayHelper;

class VtReportUserUploadBase extends \common\models\db\VtReportUserUploadDB {

    const DRAFT = -1;
    const FIX = 0;
    const PAID = 1;

    public static function getByUserId($userId, $limit, $offset = 0, $withLastMonth = false)
    {
        $query = self::find()
            ->asArray()
            ->select('report_month, revenue, status')
            ->where(
                [
                    'user_id' => $userId
                ]
            )
            ->orderBy('report_month DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }
        $results = $query->all();
        $arrs = [];
        foreach($results as $result){
            $arr['month'] = date('m/Y', strtotime($result['report_month']));
            $arr['revenue'] = (int) $result['revenue'];
            $arr['revenue_str'] = Yii::$app->formatter->asDecimal((int) $result['revenue'], 0);
            $arr['status'] = Yii::t('wap',((array_key_exists($result['status'], Yii::$app->params['revenue.user.upload.value']))? Yii::$app->params['revenue.user.upload.value'][$result['status']]:''));
            //var_dump($arr['status']);die;
            $arr['display_more'] = 1;
            $arrs[] = $arr;
        }
        $lastMonth = strtotime('last month');

        if($withLastMonth && (count($arrs) == 0 || $arrs[0]['month'] != date('m/Y', $lastMonth))){

            $lastMonthRevenue = self::getRevenueByMonth($userId, date('Y-m', $lastMonth));

            if($lastMonthRevenue > 0){
                array_unshift($arrs , [
                    "month" => date('m/Y', $lastMonth),
                    "revenue" => $lastMonthRevenue,
                    "revenue_str" => Yii::$app->formatter->asDecimal($lastMonthRevenue, 0),
                    "status" => Yii::t('wap',((array_key_exists(VtReportUserUploadBase::DRAFT, Yii::$app->params['revenue.user.upload.value']))? Yii::$app->params['revenue.user.upload.value'][VtReportUserUploadBase::DRAFT]:'')),
                    "display_more" => 0
                ]);
            }
        }

        return $arrs;
    }


    public static function getRevenueByMonth($userId, $month){
        $packageTypes = ArrayHelper::getColumn(VtPackageBase::getAllSubViewPackage(), 'type');

        $fromDate = $month . '-01';
        $toDate = date("Y-m-t", strtotime($fromDate));

        $fromTime = date('Y-m-d H:i:s', strtotime($fromDate . ' 00:00:00'));
        $toTime = date('Y-m-d H:i:s', strtotime($toDate . ' 23:59:59'));

        $given = new \DateTime($fromTime);
        $given->setTimezone(new \DateTimeZone("UTC"));
        $utcFromTime = $given->format("Y-m-d H:i:s");

        $given = new \DateTime($toTime);
        $given->setTimezone(new \DateTimeZone("UTC"));
        $utcToTime = $given->format("Y-m-d H:i:s");

        $params = [
            "size" => 0,
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "range" => [
                                "datetime" => [
                                    "gte" => $utcFromTime,
                                    "lte" => $utcToTime,
                                    "format" => "yyyy-MM-dd HH:mm:ss"
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "aggs" => [
                "package" => [
                    "filter"  => [
                        "bool" => [
                            "must" => [
                                [
                                    "term" => [
                                        "cp_code" => "user_upload_" . $userId
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "aggs" => [
                        "package" => [
                            "terms" => [
                                "field" => "package_type"
                            ]
                        ]
                    ]
                ]
            ]

        ];

        $revenueParams = [

            "size" => 0,
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "bool" => [
                                "should" => [
                                    [
                                        "match" => [
                                            "action" => "REG"
                                        ]
                                    ],
                                    [
                                        "match" => [
                                            "action" => "CHARGE"
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "range" => [
                                "datetime" => [
                                    "gte" => $utcFromTime,
                                    "lte" => $utcToTime,
                                    "format" => "yyyy-MM-dd HH:mm:ss"
                                ]
                            ]
                        ],
                        [
                            "range" => [
                                "fee" => [
                                    "gt" => 0
                                ]
                            ]
                        ],
                        [
                            "terms" => [
                                "content" => $packageTypes
                            ]
                        ]
                    ]
                ]
            ],
            "aggs" => [
                "content" => [
                    "terms" => [
                        "field" => "content"
                    ],
                    "aggs" => [
                        "fee" => [
                            "sum" => [
                                "field" => "fee"
                            ]
                        ]
                    ]
                ]
            ]
        ];


        $allPackages = VtPackageBase::getAllPackage();

        $packagesMapName = ArrayHelper::map($allPackages, 'sub_service_name', 'name');
        $packagesMapCpDataPercentage = ArrayHelper::map($allPackages, 'type', 'cp_data_percentage');

        $url = Yii::$app->params['elasticsearch-curl'] + "/log-sub-view/_search";
        $results =  self::callElk($url,$params);


        $url = Yii::$app->params['elasticsearch-curl'] + "/log-transaction/_search";
        $revenueResults = self::callElk($url,$revenueParams);

        $rawContentByPackageTypes = $revenueResults['aggregations']['content']['buckets'];
        $revenuesByPackage = [];
        $totalViewCount = 0;
        $totalFee = 0;

        foreach ($rawContentByPackageTypes as $revenueByPackageType) {

            $keyByPackageType = $revenueByPackageType['key'];

            $revenuesByPackage[$keyByPackageType]['count'] = $revenueByPackageType['doc_count'];
            $revenuesByPackage[$keyByPackageType]['fee'] = $revenueByPackageType['fee']['value'];


            $totalFee += $revenueByPackageType['fee']['value'] - $revenueByPackageType['doc_count'] * (isset($packagesMapCpDataPercentage[$keyByPackageType]) ? $packagesMapCpDataPercentage[$keyByPackageType] : 0);
        }

        $contentsByCp = [];
        $rawContentBypackages = $results['aggregations']['package']['package']['buckets'];

        foreach ($rawContentBypackages as $package) {
            $totalViewCount += $package['doc_count'];
            $packageKey = strtoupper($package['key']);

            if (ArrayHelper::keyExists($packageKey, $packagesMapName)) {
                $packageName = $packagesMapName[$packageKey];
            } else {
                $packageName = 'Không xác định';
            }

            $contentsByCp[$userId][$packageName]['view_count'] = (isset($contentsByCp[$userId][$packageName]['view_count']) ? $contentsByCp[$userId][$packageName]['view_count'] : 0) + $package['doc_count'];

        }

        $totalViewCount = $results['hits']['total'];
        if(isset($contentsByCp[$userId]) && is_array($contentsByCp[$userId])){
            foreach($contentsByCp[$userId] as $package){
                $contentsByCp[$userId]['view_count'] = (isset($contentsByCp[$userId]['view_count']) ? $contentsByCp[$userId]['view_count'] : 0) + $package['view_count'];
            }
            $percentage = $contentsByCp[$userId]['view_count'] / $totalViewCount;

            $revenue = $percentage * $totalFee * VtConfigBase::getConfig('CP.REVENUE.PERCENTAGE', 1) / 1.1 * 90 / 100;
        }else{
            $revenue = 0;
        }

        return round($revenue);
    }

    public static function callElk($url,$params){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 3,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $results = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);
        curl_close($curl);
        return $results;
    }

}