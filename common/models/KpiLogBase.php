<?php

namespace common\models;

use Yii;

class KpiLogBase extends \common\models\db\KpiLogDB
{

    const ACTION_REGISTER = "REGISTER";
    const ACTION_CANCEL = "CANCEL";
    const APPLICATION_CODE = "MYCLIP";

    const TRANSACTION_SUCCESS = 0;
    const TRANSACTION_FAIL = 1;


    public static function writeLog($serviceCode, $requestContent,  $actionName)
    {
        $log = new KpiLogBase();

        $log->ApplicationCode = self::APPLICATION_CODE;
        $log->ServiceCode = $serviceCode;
        $log->SessionID = self::APPLICATION_CODE . uniqid();
        $log->RequestContent = $requestContent;
        $log->StartTime = date("Y-m-d H:i:s");
        $log->ActionName = $actionName;

        $log->save(false);

        return $log->id;
    }

    public static function updateLog($id, $duration, $errorCode, $responseContent, $transactionStatus)
    {

        $log = KpiLogBase::findOne(['id' => $id]);

        if ($log) {
            $log->ResponseContent = $responseContent;
            $log->EndTime = date('Y-m-d H:i:s');
            $log->Duration = $duration;
            $log->ErrorCode = $errorCode;
            $log->ErrorDescription = self::errorMsg($errorCode);
            $log->TransactionStatus = $transactionStatus;
            $log->save(false);
        }

    }

    public static function errorMsg($errorCode)
    {
        return $errorCode;
    }


}