<?php

namespace common\models;

use cp\models\VtVideo;
use Yii;

class VtVideoSearchBase extends \yii\elasticsearch\ActiveRecord {

    public static function type() {
        return 'video';
    }

    public static function index() {
        return 'vt-video';
    }

    public static function search($textQuery, $offset = 0, $limit = 12, $categoryIds = [], $type = '') {
        return self::searchV3($textQuery, $offset, $limit, $categoryIds, $type);
    }

    public static function searchV3($textQuery, $offset = 0, $limit = 12, $categoryIds = [], $type = '') {
        $filters = [
            "must" => [
                [
                    "term" => ["is_active" => true]
                ],
                [
                    "term" => ["status" => 2]
                ],
            ]
        ];
        if($type == "hashtag") {
            $params = [
                "from" => $offset,
                "size" => $limit,
                "query" => [
                    "bool" => [
                        "must" => [
                            "multi_match" => [
                                "query" => $textQuery,
                                "type" => "phrase_prefix",
                                "fields" => ["tag"],
                                // "analyzer" => "my_analyzer",
                                "tie_breaker" => 0.3,
                                "minimum_should_match" => "30%"
                            ]
                        ],
                        "filter" => [
                            "bool" => $filters
                        ]
                    ]
                ]
            ];
        } else {
            $params = [
                "from" => $offset,
                "size" => $limit,
                "query" => [
                    "bool" => [
                        "must" => [
                            "multi_match" => [
                                "query" => $textQuery,
                                "type" => "phrase_prefix",
                                "fields" => ["test_search^6","name^5", "channel_name^4", "description^3", "name_slug^2", "description_slug"],
                                // "analyzer" => "my_analyzer",
                                "tie_breaker" => 0.3,
                                "minimum_should_match" => "30%"
                            ]
                        ],
                        "filter" => [
                            "bool" => $filters
                        ]
                    ]
                ]
            ];
        }
        
        $url = Yii::$app->params['elasticsearch-curl'] . '/vt-video/_search';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            // CURLOPT_PROXY => '192.168.193.12:3128',
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));



        $results = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);
        curl_close($curl);
        $searchResults = [];
        if (isset($results['hits']['hits'])) {
            $searchResults = $results['hits']['hits'];
        }
        return $searchResults;
    }

}
