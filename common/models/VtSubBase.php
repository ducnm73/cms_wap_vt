<?php

namespace common\models;

use Yii;

class VtSubBase extends \common\models\db\VtSubDB
{

    const STATUS_ACTIVE = 1;

    /**
     * @author: PhuMX
     * Lay tat ca cac goi sub active cua thue bao
     * @param $msisdn
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSub($msisdn)
    {
        return self::find()->asArray()
            ->where(['msisdn' => $msisdn, 'status' => self::STATUS_ACTIVE])
            ->all();
    }

    public static function getOneSubByMsisdn($msisdn)
    {
        //var_dump($msisdn);die;
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;

        $query = self::find()
            ->select('s.*, t.quota_views, t.name as package_name, t.multilang')
            //->asArray()
            ->from(self::tableName() . ' s')
            ->leftJoin(VtPackageBase::tableName(). ' t', 's.package_id = t.id')
            ->where(['s.msisdn' => $msisdn, 's.status' => self::STATUS_ACTIVE]);
        //->one();
//        $aa =  $query->asArray()->one();
//        var_dump($query);die;
        if ($mainLanguage == $currentLanguage) {

            return $query->asArray()->one();
        } else {
            //var_dump($currentLanguage);die;//en
            $temp = $query->asArray()->one();
            $multilang = $temp['multilang'];
            //var_dump($multilang);die;
            if (is_null($multilang)) {
                return $temp;
            }
            $responData=LanguageConvert::arrayExclude($temp,['name','short_description','description']);
            $multiLangFieldPart = LanguageConvert::convertFieldsToArray($multilang,
                ['name','short_description','description'],$currentLanguage, $temp);
            //var_dump(array_merge($responData,$multiLangFieldPart));die;
            return array_merge($responData,$multiLangFieldPart);
        }
    }

    /**
     * @author: PhuMX
     * Lay tat ca cac goi sub  cua thue bao
     * @param $msisdn
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllSub($msisdn)
    {
        return self::find()->asArray()
            ->where(['msisdn' => $msisdn])
            ->all();
    }

    /**
     * Kiem tra khuyen mai khach hang
     * @param $msisdn
     */
    public static function checkPromotionFirstTimeUsageAPP($msisdn)
    {
        $allSub = self::find()->asArray()
            ->where(['msisdn' => $msisdn])
            ->one();
        if ($allSub) {
            return 0;
        } else {
            return 1;
        }
    }
}