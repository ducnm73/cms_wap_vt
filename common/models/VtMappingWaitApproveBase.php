<?php

namespace common\models;

use Yii;

class VtMappingWaitApproveBase extends \common\models\db\VtMappingWaitApproveDB {

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    public static function isWaitApprove($userId)
    {
        if(Yii::$app->params['auto.approve.filter.by.channel']){
            $query = self::find()
                ->from(self::tableName() . ' m')
                ->where([
                    'local_user_id' => $userId,
                    'is_active' => self::ACTIVE
                ]);

            return ($query->count() > 0) ? true : false;
        }else{
            return false;
        }

    }
}