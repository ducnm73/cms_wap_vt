<?php

namespace common\models;

use Yii;

class VtUploadFeedbackBase extends \common\models\db\VtUploadFeedbackDB
{
    const STATUS_FEEDBACK = 1;
    const STATUS_APPROVE = 2;
    const STATUS_REJECT = 3;

    public static function getUploadFeedbackByUser($userId, $videoId)
    {

        return self::find()->asArray()
            ->where(["created_by" => $userId, "video_id" => $videoId])
            ->one();
    }

    public static function insertUploadFeedback($userId, $videoId, $content, $bucket, $path)
    {

        $objUploadFeedback = new VtUploadFeedbackBase();
        $objUploadFeedback->video_id = $videoId;
        $objUploadFeedback->created_by = $userId;
        $objUploadFeedback->created_at = date("Y-m-d H:i:s");
        $objUploadFeedback->updated_at = date("Y-m-d H:i:s");
        $objUploadFeedback->status = self::STATUS_FEEDBACK;
        $objUploadFeedback->feedback_content = $content;
        $objUploadFeedback->bucket = $bucket;
        $objUploadFeedback->path = $path;

        $objUploadFeedback->save(false);
    }

}