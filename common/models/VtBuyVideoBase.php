<?php

namespace common\models;

use Yii;

class VtBuyVideoBase extends \common\models\db\VtBuyVideoDB
{
    /**
     * @author PhuMX
     * @param $msisdn
     * @param $video
     * @return Kiem tra xem khach hang da mua le video chua?
     */
    public static function checkBuyVideo($msisdn, $video)
    {
        return self::find()
            ->asArray()
            ->where(['msisdn' => $msisdn, 'video_id' => $video['id']])
            ->one();
    }

    public function insertTransaction($msisdn, $userId, $itemId)
    {
        $this->msisdn = $msisdn;
        $this->user_id = $userId;
        $this->video_id = $itemId;
        $this->created_at = date("Y-m-d H:i:s");
        $this->save(false);
    }

}