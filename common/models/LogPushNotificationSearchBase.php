<?php

namespace common\models;

use Yii;

class LogPushNotificationSearchBase extends \yii\elasticsearch\ActiveRecord
{
    public static function type()
    {
        return 'notification';
    }

    public static function index()
    {
        return 'log_push_notification';
    }

    public function attributes()
    {
        return ['client_id', 'sent_time', 'message', 'item_id', 'group_id', 'type', 'user_id', 'is_read'];
    }

    public static function getLogsByClientId($clientId, $offset = 0, $limit = 12)
    {
        $query = self::find()->asArray()
            ->where([
                "client_id" => $clientId
            ])
            ->offset($offset)
            ->limit($limit)
            ->orderBy(['sent_time' => SORT_DESC]);

        return $query->all();
    }

    public static function getLogsByUserId($userId, $offset = 0, $limit = 12)
    {
        $query = self::find()
            ->where([
                "user_id" => $userId
            ])
            ->offset($offset)
            ->limit($limit)
            ->orderBy(['sent_time' => SORT_DESC]);

        //$a = $query->all();
        //echo $query->createCommand()->getRawSql(); die;
        //var_dump($query); die();
        return $query->all();
    }

    public static function getLogsById($id)
    {
        $query = self::find()
//            ->asArray()
            ->where([
                "_id" => $id
            ])
        ;

        return $query->one();
    }


}