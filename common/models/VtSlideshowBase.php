<?php

namespace common\models;

use Yii;

class VtSlideshowBase extends \common\models\db\VtSlideshowDB
{

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    const LOCATION_HOME = 'HOME';
    const LOCATION_VOD = 'VOD';
    const LOCATION_FILM = 'FILM';



    public static function getSlideShowQuery($location, $limit, $offset = null)
    {

        $query = self::find()
            ->asArray()
            ->from('vt_slideshow v')
            ->where('v.location = :location', [':location' => $location])
            ->andWhere('v.begin_time <= now() or v.begin_time is null')
            ->andWhere('v.end_time >= now() or v.end_time is null')
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->orderBy('v.position');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }


    public static function getSlideShowByLocation($location, $limit, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('name, type, item_id, href, bucket, path')
            ->from('vt_slideshow v')
            ->where('v.location = :location', [':location' => $location])
            ->andWhere('v.begin_time <= now()')
            ->andWhere('v.end_time >= now()')
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->orderBy('v.position');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query->all();
    }
}