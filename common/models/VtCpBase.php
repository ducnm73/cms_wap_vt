<?php

namespace common\models;

use Yii;

class VtCpBase extends \common\models\db\VtCpDB {

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    const CAN_VIEW_REPORT = 1;
    const CANNOT_VIEW_REPORT = 0;

    const TYPE_CRAWLER = 0;
    const TYPE_CP = 1;
    const TYPE_USER_UPLOAD = 2;

    public static function getAllCp()
    {
        return self::find()
            ->asArray()
            ->orderBy('id ASC')
            ->all();
    }

    public static function getAllCpByType($type)
    {
        return self::find()
            ->asArray()
            ->where(['type' => $type])
            ->orderBy('id ASC')
            ->all();
    }

    public static function getOneById($id)
    {
        return self::find()
            ->asArray()
            ->where([
                'id' => $id
            ])
            ->one();
    }
}