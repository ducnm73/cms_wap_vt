<?php

namespace common\models;

use Yii;

class KpiUploadBase extends \common\models\db\KpiUploadDB {

    public function insertKpi($sessionId, $startTime) {

        $this->session_id = $sessionId;
        $this->upload_start_time = $startTime;
        $this->save();
    }

    public static function getBySessionId($sessionId){
        $query = self::find()
            ->where([
                'session_id' => $sessionId,
            ]);

        return $query->one();
    }

}