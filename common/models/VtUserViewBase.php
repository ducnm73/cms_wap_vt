<?php

namespace common\models;

use Yii;

class VtUserViewBase extends \common\models\db\VtUserViewDB
{


    public static function getByUser($userId, $msisdn)
    {
        if (empty($msisdn)) {
            return self::find()
                ->where([
                    'user_id' => $userId
                ])
                ->one();
        } else {
            return self::find()
                ->where([
                    'msisdn' => $msisdn
                ])
                ->one();
        }
    }


    public function insertLog($userId, $msisdn)
    {
        $this->user_id = $userId;
        $this->msisdn = $msisdn;
        $this->first_time = date('Y-m-d H:i:s');

        return $this->save(false);
    }

    public function getQuota()
    {
        if(strtotime(date('Y-m-d 00:00:00', strtotime($this->first_time))) == strtotime(date('Y-m-d 00:00:00'))){
            return Yii::$app->params['quota.free.firsttime'];
        }else if(strtotime($this->first_time) > strtotime('-30 day')){
            return Yii::$app->params['quota.free.before30day'];
        }else{
            return Yii::$app->params['quota.free.after30day'];
        }
    }


}