<?php

namespace common\models;

use common\helpers\MobiTVRedisCache;
use common\helpers\Utils;
use common\libs\VtHelper;
use Yii;

class VtConfigBase extends \common\models\db\VtConfigDB
{
    public static function getConfig($key, $defaultValue = '')
    {
        // su dung cache config
        if (Yii::$app->params['cache.enabled'] === true) {
            $valueConfig = self::getDb()->cache(function ($db, $defaultValue = '') use ($key) {
                $config = self::find()
                    ->asArray()
                    ->select('config_key, config_value, multilanguage')
                    ->where('config_key = :config_key', [':config_key' => $key])
                    ->one();

                if ((!empty($config) || empty($defaultValue)) && $config['config_value'] != NULL && VtHelper::isJSON($config['multilanguage'])) {
                    $configValue = LanguageConvert::convertFieldsToArray($config['multilanguage'], ['config_value'], Yii::$app->language, $defaultValue);
                    $value = $configValue['config_value'] ? $configValue['config_value'] : $config['config_value'];
                    return $value;
                } else if(!empty($config)) {
                    return $config['config_value'];
                } else {
                    return $defaultValue;
                }
            }, MobiTVRedisCache::CACHE_10MINUTE);

            if ($valueConfig != NULL && strlen($valueConfig)) {
                return $valueConfig;
            } else {
                return $defaultValue;
            }

        } else {
            $config = self::find()
                ->asArray()
                ->select('config_key, config_value, multilanguage')
                ->where('config_key = :config_key', [':config_key' => $key])
                ->one();

            if ((!empty($config) || empty($defaultValue)) && $config['config_value'] != NULL) {
                if(VtHelper::isJSON($config['multilanguage'])) {
                    $configValue = LanguageConvert::convertFieldsToArray($config['multilanguage'], ['config_value'], Yii::$app->language, $config);
                    if($configValue['config_value']) {
                        $value = $configValue['config_value'];
                    } else {
                        $value = $config['config_value'];
                    }
                } else {
                    $value = $config['config_value'];
                }
                return $value;
            } else {
				if($config['config_value']) {
					return $config['config_value'];
				} else {
					return $defaultValue;
				}
            }
        }
    }


    public static function getI18nConfig($key, $defaultValue = '') {
        if (Yii::$app->params['cache.enabled'] === true) {
            $valueConfig = self::getDb()->cache(function ($db, $defaultValue = '') use ($key) {
                $config = self::find()
                    ->asArray()
                    ->select('config_key, config_value, enable_multilang, multilanguage')
                    ->where('config_key = :config_key', [':config_key' => $key])
                    ->one();

                if ((!empty($config) || empty($defaultValue)) && $config['config_value'] != NULL) {
                    if($config['enable_mulilang'] == true) {
                        \common\helpers\Utils::convertMultilangFields($config, ['config_value'], Yii::$app->language, 'multilanguage');
                    }

                    return $config['config_value'];
                } else {
                    return $defaultValue;
                }

            }, MobiTVRedisCache::CACHE_10MINUTE);

            if ($valueConfig != NULL && strlen($valueConfig)) {
                return $valueConfig;
            } else {
                return $defaultValue;
            }
        } else {
            $config = self::find()
                ->asArray()
                ->select('config_key, config_value, enable_multilang, multilanguage')
                ->where('config_key = :config_key', [':config_key' => $key])
                ->one();

            if ((!empty($config) || empty($defaultValue)) && $config['config_value'] != NULL) {
                if($config['enable_multilang'] == true) {
                    Utils::convertMultilangFields($config, ['config_value'], Yii::$app->language);
                }

                return $config['config_value'];
            } else {
                return $defaultValue;
            }
        }
    }

    public function getMultilangData() {
        $multilangs = json_decode($this->multilang, true);
        $multilangModels = [];

        if(is_array($multilangs)) {
            $langs = [];

            foreach ($multilangs as $key => $value) {
                $lang = substr($key, strripos($key, '_') + 1);

                if(!in_array($lang, $langs)) {
                    $langs[] = $lang;
                }
            }

            foreach ($langs as $lang) {
                $multilangModel = new ConfigMultilang(['lang' => $lang]);
                $multilangModel->config_value = isset($multilangs["config_value_$lang"]) ? $multilangs["config_value_$lang"] : '';
                $multilangModel->description = isset($multilangs["description_$lang"]) ? $multilangs["description_$lang"] : '';
                $multilangModels[] = $multilangModel;
            }
        }

        return $multilangModels;
    }

    public function setMultilangData($datas) {
        $values = [];

        if(is_array($datas)) {
            foreach ($datas as $data) {
                $values['config_value_' . $data['lang']] = $data['config_value'];
                $values['description_' . $data['lang']] = $data['description'];
            }
        }

        $this->multilang = empty($values) ? null : json_encode($values);
    }
}
