<?php

namespace common\models;

use common\helpers\MobiTVRedisCache;
use Yii;

class VtAwardBase extends \common\models\db\VtAwardDB {

    public static function getByType($key, $defaultValue=''){
        // su dung cache config
        if (Yii::$app->params['cache.enabled'] === true) {
            $valueConfig = self::getDb()->cache(function ($db, $defaultValue) use ($key) {
                $config = self::find()
                    ->asArray()
                    ->select('award_name')
                    ->where('award_type = :award_type', [':award_type' => $key])
                    ->one();

                if ((!empty($config) || empty($defaultValue)) && $config['award_name'] != NULL) {
                    return $config['award_name'];
                } else {
                    return $defaultValue;
                }

            }, MobiTVRedisCache::CACHE_1HOUR);

            if ($valueConfig != NULL && strlen($valueConfig)) {
                return $valueConfig;
            } else {
                return $defaultValue;
            }

        } else {
            $config = self::find()
                ->asArray()
                ->select('award_name')
                ->where('award_type = :award_type', [':award_type' => $key])
                ->one();

            if ((!empty($config) || empty($defaultValue)) && $config['award_name'] != NULL) {
                return $config['award_name'];
            } else {
                return $defaultValue;
            }
        }
    }

}