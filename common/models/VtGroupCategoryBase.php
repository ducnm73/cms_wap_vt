<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use common\libs\VtHelper;

class VtGroupCategoryBase extends \common\models\db\VtGroupCategoryDB {

    const ACTIVE = 1;
    const DEACTIVE = 0;
    const TYPE_FILM = 'FILM';
    const TYPE_VOD = 'VOD';

    const HOT = 1;
    const NO_HOT = 0;

    /**
     * Lay ra 1 the loai id hoac type
     * @author phumx
     * @param null $id
     * @param null $type
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCategoryGroup($id = null, $type = null) {
        $query = self::find()
            ->asArray()
            ->where(['is_active' => self::ACTIVE]);
        if ($id != null) {
            $query->andWhere(['id' => $id]);
        }
        if ($type != null) {
            $query->andWhere(['type' => $type]);
        }

        return $query->one();
    }

    /**
     * @author PhuMX
     * @return lay danh sach menu
     * @param $type
     */
    public static function getMenuByType($type = false) {

        $query = self::find()
            ->asArray()
            ->select('id, name, type')
            ->where(['is_active' => self::ACTIVE])
            ->orderBy('name ASC');
        if ($type) {
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }

    public static function getAllCategory($type = null) {
        $query = self::find()
            ->asArray()
            ->select('*')
            ->orderBy('id DESC');

        if (!empty($type)) {
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }

    public static function getAllActiveCategory($type = null) {
        $query = self::find()
            ->asArray()
            ->select(["*", "concat(name,' - ', type) as name_detail"])
            ->where(['is_active' => self::ACTIVE])
            ->andWhere(['type' => self::TYPE_VOD])
            ->orderBy(['name' => SORT_ASC]);

        if (!empty($type)) {
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }

    /**
     * Kiem tra xem category co hop le hay khong
     * @param $categoryId
     */
    public static function checkCategory($categoryId, $type = null) {
        $query = self::find()
            ->asArray()
            ->where(['id' => $categoryId, 'is_active' => self::ACTIVE]);
        if ($type) {
            $query->andWhere(['type' => $type]);
        }
        return $query->one();
    }

    public static function searchByName($q, $limit = 10) {
        $query = self::find()
            ->asArray()
            ->select('id, name as text')
            ->andWhere(['like', 'name', $q])
            ->limit($limit);

        return $query->all();
    }

    /**
     * Lay ra 1 the loai id hoac type
     * @author phumx
     * @param null $id
     * @param null $type
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCategoryByIds($id = null, $type = null) {
        $query = self::find()
            ->asArray()
            ->where(['is_active' => self::ACTIVE]);
        if ($id != null) {
            $query->andWhere(['id' => $id]);
        }
        if ($type != null) {
            $query->andWhere(['type' => $type]);
        }
        if(is_array($id)){
            $ids = implode(',', array_filter($id));

            if($ids) {
                $query->orderBy([new Expression('FIELD (id, ' . $ids . ')')]);
            }
        }

        return $query->all();
    }

    public static function getParents($offset = null, $limit = null, $q = false, $isHot=false,$type='') {

        //da sua trong CategoryObj
        $query = self::find()
            ->asArray()
            ->where(['is_active' => self::ACTIVE]);

        if(empty($q)){
            $query->andWhere('parent_id is null');
        }

        if($isHot){
            $query->andWhere(['is_hot'=>1]);
        }

        if ($type) {
            $query->andWhere(['type' => $type]);
        }
        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        $query->orderBy('name ASC');

        return ($q) ? $query : $result = $query->all();
    }

    public static function getChilds($id, $offset = null, $limit = null, $type = null) {
        $query = self::find()
            ->asArray()
            ->where(['is_active' => self::ACTIVE])
            ->andWhere('parent_id = :parent_id', [':parent_id' => $id]);
        if (!empty($type)) {
            $query->andWhere('type=:type', [':type' => $type]);
        }
        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy("positions desc");
        return $query;
    }

    public static function getById($id) {
        $query = self::find()
            ->where([
                'id' => $id
            ]);

        return $query->one();
    }

    public static function getPosition() {
        $count = self::find()
            ->where(['is_active' => self::ACTIVE])
            ->count();
        $array = array();
        for ($i = 0; $i < $count; $i ++) {
            $array[$i] = $i;
        }
        return $array;
    }

    public static function getAllActiveChildCategory($type = null) {
        $query = self::find()
            ->asArray()
            ->select(["*", "concat(name,' - ', type) as name_detail"])
            ->where(['is_active' => self::ACTIVE])
            ->andWhere('parent_id is not null')
            ->orderBy('id ASC');

        if (!empty($type)) {
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }

    public static function getAllHotCategories($limit = null, $ignoreIds = []) {
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
                'is_hot' => self::HOT
            ]);

        if(!empty($ignoreIds)){
            $query->andWhere(['not in', 'id', $ignoreIds]);
        }

        $query->orderBy('positions DESC');

        if ($limit != null) {
            $query->limit($limit);
        }

        return $query->all();
    }

    public static function getActiveById($id) {
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
                'id' => $id
            ]);

        return $query->one();
    }


    public static function getCategory(){
        $query = self::find()->asArray()
            ->select('id,name')
            ->where(['is_active' => self::ACTIVE]);
        return $query->all();
    }

    public function generateAvatarThumbs() {
        return VtHelper::generateThumbs($this->avatar_path, $this->avatar_bucket);
    }

    public function generateThumbs() {
        return VtHelper::generateThumbs($this->path, $this->bucket);
    }
}
