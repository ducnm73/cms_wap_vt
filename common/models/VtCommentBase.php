<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtCommentBase extends \common\models\db\VtCommentDB
{
    //0:wait approve, 1: active, 2: disapprove, 3: deleted

    const WAIT_APPROVE = 0;
    const ACTIVE = 1;
    const DISAPPROVE = 2;
    const DELETED = 3;
    const ALL = 4;

    public function getUser()
    {
        return $this->hasOne(VtUserBase::className(), ['id' => 'user_id']);
    }

    public function getAuth_user()
    {
        return $this->hasOne(AuthUser::className(), ['id' => 'approve_by']);
    }

    public function insertComment($contentId, $type, $userId, $comment, $content, $parentId,$status)
    {
        $this->content_id = $contentId;
        $this->type = $type;
        $this->user_id = $userId;
        $this->comment = $comment;
        $this->content = $content;
        $this->parent_id = ($parentId) ? $parentId : 0;
        $this->status = $status;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->setBadWordFilterComment($comment);
        $this->save(false);
    }

    /**
     * Ham check noi dung xau
     * @param $comment
     */
    public function setBadWordFilterComment($comment)
    {
        $comment = Utils::removeSignOnly($comment);

        $badWords = VtCommentBadWordBase::getAllBadWord();

        $matchFound = preg_match_all("/\b(" . implode($badWords, "|") . ")\b/i", $comment);

        if ($matchFound) {
            $isBadContent = 1;
        } else {
            $isBadContent = 0;
        }

        $this->bad_word_filter = $isBadContent;
    }

    /**
     * Lay ra danh sach binh luan theo noi dung
     * @param $userId
     * @param $type
     * @param $contentId
     * @param null $limit
     * @param null $lastId
     * @param array $parentIds
     * @return mixed
     */
    public static function getByContentId($userId, $contentId, $limit = null, $offset = null, $parentIds = array(),$status = null)
    {

        $q = self::find()
            ->asArray()
            ->select('c.id, c.type, c.comment, c.parent_id, c.created_at,c.status, c.like_count,c.dislike_count, u.full_name, u.msisdn, u.id as user_id, u.path, u.bucket, u.channel_bucket,u.channel_path')
            ->from(self::tableName().' c')
            ->leftJoin(VtUserBase::tableName(). ' u', 'c.user_id = u.id')
            ->where('type = :type', [':type' => 'VOD'])
            //->andWhere(['<>', 'c.user_id', $userId])
            ->andWhere('content_id = :content_id', [':content_id' => $contentId]);

        if (!empty($limit)) {
            $q->limit($limit);
        }

        if (!empty($userId)) {
            if ($status == self::ACTIVE) {
                $q->andWhere('c.status = :status', [':status' => self::ACTIVE]);
            }elseif($status == self::DISAPPROVE){
                $q->andWhere('c.status = :status', [':status' => self::DISAPPROVE]);
            }elseif($status == self::ALL){
                $q->andWhere('c.status >= 0');
            }else{
                $q->andWhere('((user_id = :user_id and (c.status = :wait_approve_status or c.status = :active_status)) or c.status = :active_status)',
                    [
                        ':user_id' => $userId,
                        ':wait_approve_status' => self::WAIT_APPROVE,
                        ':active_status' => self::ACTIVE
                    ]
                );
            }
        } else {
            $q->andWhere('c.status = :status', [':status' => self::ACTIVE]);
        }

        if (!empty($offset)) {
            $q->offset($offset);
        }

        if (!empty($parentIds)) {
            if (is_array($parentIds)) {
                $q->andWhere(['parent_id' => $parentIds]);
            } else {
                $q->andWhere('parent_id = :parent_id OR parent_id is null', [':parent_id' => $parentIds]);
            }
        } else {
            $q->andWhere('parent_id = 0 OR parent_id is null');
        }

        $q->orderBy('created_at DESC, id DESC');

        return $q->all();
    }
    /**
     * Lay ra danh sach binh luan theo noi dung
     * @param $userId
     * @param $type
     * @param $contentId
     * @param null $limit
     * @param null $lastId
     * @param array $parentIds
     * @return mixed
     */
    public static function getAllByUserId($contentIds, $limit = null, $offset = null,$status = null, $parentIds = array())
    {

        $q = self::find()
            ->asArray()
            ->select('c.id, c.type, c.comment, c.parent_id, c.created_at, c.status, c.like_count,c.dislike_count, u.full_name, u.msisdn, u.id as user_id, u.path, u.bucket, u.channel_bucket,u.channel_path')
            ->from(self::tableName().' c')
            ->leftJoin(VtUserBase::tableName(). ' u', 'c.user_id = u.id')
            ->andWhere(['in', 'c.content_id', $contentIds]);

        if (!empty($limit)) {
            $q->limit($limit);
        }

        if ($status == self::ACTIVE) {
            $q->andWhere('c.status = :status', [':status' => self::ACTIVE]);
        }elseif($status == self::DISAPPROVE){
            $q->andWhere('c.status = :status', [':status' => self::DISAPPROVE]);
        }elseif($status == self::WAIT_APPROVE){
            $q->andWhere('c.status = :status', [':status' => self::WAIT_APPROVE]);
        }

        if (!empty($offset)) {
            $q->offset($offset);
        }
        // không loc dk vơi status = 0
        if ($status != self::DISAPPROVE and $status != self::WAIT_APPROVE) {
            if (!empty($parentIds)) {
                if (is_array($parentIds)) {
                    $q->andWhere(['parent_id' => $parentIds]);
                } else {
                    $q->andWhere('parent_id = :parent_id OR parent_id is null', [':parent_id' => $parentIds]);
                }
            } else {
                $q->andWhere('parent_id = 0 OR parent_id is null');
            }
        }

        $q->orderBy('created_at DESC, id DESC');

        return $q->all();
    }

    /** Đếm so luong comment cua user
     * @param $userid
     * @throws \yii\db\Exception
     */
    public static function getCountByUserId($userId,$status = null)
    {
        $q = self::find()
            ->asArray()
            ->select('c.id, c.type, c.comment, c.parent_id, c.created_at, c.status, c.like_count, u.full_name, u.msisdn, u.id as user_id, u.path, u.bucket, u.channel_bucket,u.channel_path')
            ->from(self::tableName().' c')
            ->leftJoin(VtUserBase::tableName(). ' u', 'c.user_id = u.id');

        if (!empty($userId)) {
            $q->andWhere('((user_id = :user_id and (c.status = :wait_approve_status or c.status = :active_status)) or c.status = :active_status)',
                [
                    ':user_id' => $userId,
                    ':wait_approve_status' => self::WAIT_APPROVE,
                    ':active_status' => self::ACTIVE
                ]
            );
        }
        if ($status == self::ACTIVE) {
            $q->andWhere('c.status = :status', [':status' => self::ACTIVE]);
        }elseif($status == self::DISAPPROVE){
            $q->andWhere('c.status = :status', [':status' => self::DISAPPROVE]);
        }elseif($status == self::WAIT_APPROVE){
            $q->andWhere('c.status = :status', [':status' => self::WAIT_APPROVE]);
        }

        return $q->count();
    }

    /** Cap nhat so luong comment +1
     * @param $commentId
     * @throws \yii\db\Exception
     */
    public static function incrementComment($commentId)
    {
        Yii::$app->db->createCommand("UPDATE vt_comment SET comment=comment+1 WHERE id = :id")
            ->bindValue(':id', $commentId)
            ->execute();

    }

    /** Cap nhat so luong comment -1
     * @param $commentId
     * @throws \yii\db\Exception
     */
    public static function decrementComment($commentId)
    {
        Yii::$app->db->createCommand("UPDATE vt_comment SET comment=comment-1 WHERE id = :id")
            ->bindValue(':id', $commentId)
            ->execute();

    }

    /**
     * Cap nhat like count
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateLikeCount($id, $increase)
    {
        if ($increase) {
            $rawQuery = "UPDATE vt_comment SET like_count=like_count+1 WHERE id = :id";
        } else {
            $rawQuery = "UPDATE vt_comment SET like_count=like_count-1 WHERE id = :id";
        }

        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->execute();
    }

    public static function updateLikeDislikeCount($id)
    {
        $likeCount = VtCommentLikeBase::getLikeCount($id,1);
        $disLikeCount = VtCommentLikeBase::getLikeCount($id,2);
        $rawQuery = "UPDATE vt_comment SET like_count=:like_count, dislike_count=:dislike_count  WHERE id = :id";

        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->bindValue(':like_count', $likeCount)
            ->bindValue(':dislike_count', $disLikeCount)
            ->execute();
    }


    public static function getDetail($id, $isObject = false, $status = [self::WAIT_APPROVE, self::ACTIVE,self::DISAPPROVE])
    {
        $q = self::find()
            ->where([
                'id' => $id,
                'status' => $status
            ]);

        if($isObject){
            return $q->one();
        }else{
            return $q->asArray()->one();
        }

    }

    public static function getById($id)
    {
        $query = self::find()
            ->where([
                'id' => $id
            ]);

        return $query->one();
    }
    public static function getArrById($id)
    {
        $query = self::find()
            ->where([
                'id' => $id
            ]);

        return $query->asArray()->one();
    }
    public static function getNotifyByUserId($userId,$arrVideo,  $offset = null,$limit = null,$status = null, $parentIds = array())
    {
        $q = self::find()
            ->asArray()
            ->select('c.id, c.type, c.comment, c.parent_id, c.created_at, c.status, c.like_count,c.dislike_count, u.full_name, u.msisdn, u.id as user_id, u.path, u.bucket, u.channel_bucket,u.channel_path,c.content_id')
            ->from(self::tableName().' c')
            ->leftJoin(VtUserBase::tableName(). ' u', 'c.user_id = u.id')
            ->andWhere(['<>', 'c.user_id', $userId])
            ->andWhere(['in', 'c.content_id', $arrVideo]);
        if (!empty($offset)) {
            $q->offset($offset);
        }
        if (!empty($limit)) {
            $q->limit($limit);
        }

        if ($status == self::ACTIVE) {
            $q->andWhere('c.status = :status', [':status' => self::ACTIVE]);
        }elseif($status == self::DISAPPROVE){
            $q->andWhere('c.status = :status', [':status' => self::DISAPPROVE]);
        }elseif($status == self::WAIT_APPROVE){
            $q->andWhere('c.status = :status', [':status' => self::WAIT_APPROVE]);
        }
        $q->orderBy('created_at DESC, id DESC');
        return $q->all();
    }
}