<?php

namespace common\models;

use Yii;

class VtPlaylistItemBase extends \common\models\db\VtPlaylistItemDB
{
    public function getVideo()
    {
        return $this->hasOne(VtVideoBase::className(), ['id' => 'item_id']);
    }

    public function insertItem($itemId, $playlistId)
    {
        $this->item_id = $itemId;
        $this->playlist_id = $playlistId;
        $this->save();
    }


    /**
     * @author Lay danh sach items theo Playlist
     * @param $playlistId
     */
    public static function getItemsByPlaylistId($playlistId)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, pi.alias')
            ->from('vt_playlist_item pi')
            ->leftJoin('vt_video v', 'v.id = pi.item_id')
            ->where(['playlist_id' => $playlistId, 'v.is_active' => VtVideoBase::ACTIVE, 'v.status' => VtVideoBase::STATUS_APPROVE])
            ->orderBy('pi.alias, LENGTH(v.name), v.name');

        return $query->all();
    }


    /**
     * @author Lay danh sach items theo Playlist
     * @param $playlistId
     */
    public static function getAllItemsByPlaylistId($playlistId, $isObj = false)
    {
        $query = self::find()
            ->joinWith('video')
//            ->select('v.*, pi.alias')
            ->from('vt_playlist_item pi')
//            ->leftJoin('vt_video v', 'v.id = pi.item_id')
            ->where([
                'playlist_id' => $playlistId
            ])
            ->orderBy('pi.alias');
    
        return $query->all();
    }

    public static function getOneByItemId($itemId){
        $query = self::find()
            ->asArray()
            ->from('vt_playlist_item pi')
            ->where(['pi.item_id' => $itemId]);

        return $query->one();
    }

    public static function getByPlayListAndItemId($playlistId, $itemId){
        $query = self::find()
            ->asArray()
            ->from('vt_playlist_item pi')
            ->where([
                'pi.playlist_id' => $playlistId,
                'pi.item_id' => $itemId

            ]);

        return $query->one();

    }


}