<?php

namespace common\models;

use Yii;

class VtContractBase extends \common\models\db\VtContractDB {
    const PAYMENT_VIA_VTPAY = "CASH";// VTPAY
    const PAYMENT_VIA_BANK = "BANK_ACCOUNT";//BANK

    public static function getByUserId($userId){
        return self::find()
            ->where(["user_id" => $userId])
            ->one();
    }

    public static function getByIdentifyId($idCardNumber){
        return self::find()
            ->where(["id_card_number" => $idCardNumber])
            ->one();

    }

}