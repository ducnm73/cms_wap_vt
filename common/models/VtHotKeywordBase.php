<?php

namespace common\models;

use Yii;

class VtHotKeywordBase extends \common\models\db\VtHotKeywordDB {

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    public static function getActiveKeyword($limit = 10)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*')
            ->from(self::tableName() . ' v')
            ->where([
                'is_active' => self::ACTIVE
            ])
            ->orderBy('position DESC')
            ->limit($limit);

        return $query->all();
    }

}