<?php

namespace common\models;

use Yii;

class VtFavouritePlaylistBase extends \common\models\db\VtFavouritePlaylistDB {
    /**
     * Lay ra thong tin yeu thich cua KH
     * @param $msisdn
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getFavourite($userId, $playlistId)
    {
        $query = self::find()
            ->where('user_id = :userId', [':userId' => $userId])
            ->andWhere(['playlist_id'=> $playlistId]);

        return $query->one();
    }

    public function insertFavourite($userId, $playlistId)
    {
        $this->user_id = $userId;
        $this->playlist_id = $playlistId;
        $this->save(false);
    }

    public static function checkIsFavourite($userId, $playlistId){

        return self::find()->asArray()
            ->where(['user_id'=>$userId, 'playlist_id'=>$playlistId])
            ->one();
    }
}