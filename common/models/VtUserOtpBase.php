<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtUserOtpBase extends \common\models\db\VtUserOtpDB
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Them moi ban ghi otp
     * @author ducda2
     * @param $msisdn
     * @param $otp
     * @param $expiredTime
     */
    public function insertOtp($msisdn, $ip, $otp, $expiredTime)
    {
        $this->msisdn = $msisdn;
        $this->otp = $otp;
        $this->ip = $ip;
        $this->expired_time = $expiredTime;
        $this->created_at = date('Y-m-d H:i:s');
        $this->status = self::STATUS_ACTIVE;
        $this->save(false);
    }

    /**
     * Update trang thai ve deactive doi voi cac otp cu
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     */
    public static function deactiveAllOtp($msisdn)
    {

        self::updateAll(['status' => 0], ['msisdn' => $msisdn, 'status' => self::STATUS_ACTIVE]);

    }


    /** Kiem tra OTP
     * @author PhuMX
     * @param $msisdn
     * @param $otp
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function checkOTP($msisdn, $otp)
    {
        $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
        return self::find()->asArray()
            ->where(['msisdn' => $msisdn, 'otp' => $otp, 'status' => self::STATUS_ACTIVE])
            ->andWhere('expired_time > :now', [':now' => date("Y-m-d H:i:d")])
            ->one();
    }


    /**
     * Dem so tin nhan gui den 1 thue bao trong ngay
     * @param $msisdn
     * @return int|string
     */
    public static function countPerDayByMsisdn($msisdn)
    {
        return self::find()
            ->where(['msisdn' => $msisdn])
            ->andWhere('created_at between :beginTime and :endTime', [
                ':beginTime' => date('Y-m-d') . ' 00:00:00',
                ':endTime' => date('Y-m-d') . ' 23:59:59'
            ])
            ->count();
    }


    /**
     * Dem so tin nhan gui tu 1 IP trong ngay
     * @param $ip
     * @return int|string
     */
    public static function countPerDayByIp($ip)
    {
        return self::find()
            ->where(['ip' => $ip])
            ->andWhere('created_at between :beginTime and :endTime', [
                ':beginTime' => date('Y-m-d') . ' 00:00:00',
                ':endTime' => date('Y-m-d') . ' 23:59:59'
            ])
            ->count();
    }

    /**
     * Kiem tra xem KH da lay ma OTP hay chua?
     * @param $msisdn
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function hasActiveOTP($msisdn)
    {
        return self::find()->asArray()
            ->where(['msisdn' => $msisdn, 'status' => self::STATUS_ACTIVE])
            ->andWhere('expired_time > :now', [':now' => date("Y-m-d H:i:d")])
            ->one();
    }
}