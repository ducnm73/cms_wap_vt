<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtCommentBadWordBase extends \common\models\db\VtCommentBadWordDB
{

    public static function getAllBadWord()
    {
        $query = self::find()
            ->asArray()
            ->select('content');

        $results = $query->all();

        $arr = array();
        foreach ($results as $result) {
            $arr[] = Utils::removeSignOnly($result['content']);
        }

        return $arr;
    }


}