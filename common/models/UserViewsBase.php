<?php

namespace common\models;

use Yii;

class UserViewsBase extends \common\models\db\UserViewsDB {
    public static function getThuebaotheoSolanSudung($moreThan, $lessThan, $from, $to)
    {
        $query = self::find()->asArray()
            ->select('msisdn')
            ->groupBy('msisdn')
            ->having(['>', 'COUNT(msisdn)', $moreThan])
            ->andHaving(['<', 'COUNT(msisdn)', $lessThan])
            ->where(['BETWEEN', 'date', $from, $to]);
        $count = $query->count();
        return $count;
    }

    public static function getChitietThuebaotheoSolanSudung($from, $to)
    {
        $query = self::find()->asArray()
            ->groupBy('msisdn')
            ->having(['>', 'COUNT(msisdn)', $from])
            ->andHaving(['<', 'COUNT(msisdn)', $to]);
        return $query->all();
    }
}