<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtPlaylistBase extends \common\models\db\VtPlaylistDB
{
    const STATUS_TEMP = 0;
    const STATUS_DRAFT = 1;
    const STATUS_APPROVE = 2;
    const STATUS_DELETE = 3;

    const TYPE_FILM = 'FILM';
    const TYPE_VOD = 'VOD';

    const ACTIVE = 1;
    const INACTIVE = 0;

    public function getCategory()
    {
        return $this->hasOne(VtGroupCategoryBase::className(), ['id' => 'category_id']);
    }


    public function getTopics()
    {
        return $this->hasMany(VtGroupTopicBase::className(), ['id' => 'topic_id'])
            ->viaTable(VtPlaylistTopicBase::tableName(), ['playlist_id' => 'id']);
    }

    public function setTopics($value)
    {
        return $value;
    }


    /**
     * @author: PhuMX
     * @param $type
     * @param $limit
     * @param $offset
     * @return lay danh sach playlist theo type
     */
    public static function getPlayListByType($type, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->where(['type' => $type, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE]);

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;

    }

    /**
     * @author: PHUMX
     * @param $type
     * @param $limit
     * @param $offset
     * @return lay danh danh playlist moi nhat theo type
     */
    public static function getPlayListNewByType($type, $limit, $offset)
    {

        $query = self::find()
            ->asArray()
            ->where(['type' => $type, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE])
            ->orderBy('updated_at DESC');

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    /**
     * @author PhuMX
     * @param $attribute
     * @param $type
     * @param $limit
     * @param $offset
     * @return Lay danh sach playlist theo thuoc tinh va type
     */
    public static function getPlayListByAttribute($attribute, $type, $limit, $offset)
    {

        $query = self::find()
            ->asArray()
            ->where(['attributes' => $attribute, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE]);

        if ($type) {
            $query->andWhere(['type' => $type]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    /**
     * @author PhuMX
     * @param $attribute
     * @param $type
     * @param $limit
     * @param $offset
     * @return Lay danh sach playlist theo thuoc tinh va type
     */
    public static function getPlayListHot($type, $limit, $offset)
    {

        $query = self::find()
            ->asArray()
            ->where(['is_hot' => 1, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE]);

        if ($type) {
            $query->andWhere(['type' => $type]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    /**
     * @author PhuMX
     * @param $attribute
     * @param $type
     * @param $limit
     * @param $offset
     * @return Lay danh sach playlist theo thuoc tinh va type
     */
    public static function getPlayListRecommend($type, $limit, $offset)
    {

        $query = self::find()
            ->asArray()
            ->where(['is_recommend' => 1, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE])
            ->orderBy('updated_at desc');

        if ($type) {
            $query->andWhere(['type' => $type]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }


    /**
     * @author: PhuMX
     * @param $type
     * @param $limit
     * @param $offset
     * @return lay danh sach playlist theo category id
     */
    public static function getPlayListByCategory($categoryId, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->where(['category_id' => $categoryId, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE])
            ->orderBy('updated_at desc');

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;

    }

    /**
     * @author PhuMX
     * Lay chi tiet playlist
     * @param $id
     */
    public static function getDetail($id, $type = "")
    {
        //var_dump($id);die;
        $query = self::find()
            ->asArray()
            ->where([
                    'id' => $id,
                    'status' => self::STATUS_APPROVE,
                    'is_active' => self::ACTIVE
                ]
            );
        if ($type) {
            $query->andWhere([
                    'type' => $type
                ]
            );
        }
        return $query->one();
    }

    /**
     * Query lay playlist lien quan
     * @param $id
     * @param $categoryId
     * @param $limit
     * @param $offset
     * @return $this
     */
    public static function getRelatedByCategoryQuery($id, $categoryId, $limit, $offset)
    {
        $query = self::find()
            ->where(['status' => self::STATUS_APPROVE, 'category_id' => $categoryId, 'is_active' => self::ACTIVE])
            ->andWhere('id <> :id', [':id' => $id])
            ->orderBy('updated_at DESC');

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }

        return $query;
    }

    /**
     * Lay playlist lien quan
     * @param $id
     * @param $categoryId
     * @param $limit
     * @param $offset
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRelatedByCategory($id, $categoryId, $limit, $offset)
    {
        $query = self::getRelatedByCategoryQuery($id, $categoryId, $limit, $offset);

        return $query->all();
    }

    /**
     * @author: PHUMX
     * @param $type
     * @param $limit
     * @param $offset
     * @return lay danh danh playlist moi nhat theo type
     */
    public static function getPlayListFree($type, $limit, $offset)
    {

        $query = self::find()
            ->asArray()
            ->where(['type' => $type, 'status' => self::STATUS_APPROVE, 'price_play' => 0, 'is_active' => self::ACTIVE])
            ->orderBy('updated_at DESC');

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    /**
     * Cap nhat like count
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateLikeCount($id, $increase)
    {
        if ($increase) {
            $rawQuery = "UPDATE vt_playlist SET like_count = IFNULL(like_count, 0) + 1 WHERE id = :id";
        } else {
            $rawQuery = "UPDATE vt_playlist SET like_count = IFNULL(like_count, 0) - 1 WHERE id = :id";
        }

        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->execute();
    }

    public static function getPlaylistsByIdsQuery($id, $limit = 10, $offset = null, $type = self::TYPE_FILM)
    {
        $query = self::find()
            ->select('p.*')
            ->asArray()
            ->from(self::tableName() . ' p')
            ->where(
                [
                    'p.id' => $id,
                    'p.status' => self::STATUS_APPROVE,
                    'p.is_active' => self::ACTIVE
                ]
            );

        if (!empty($type)) {
            $query->andWhere(['type' => $type]);
        }


        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;

    }

    /**
     * Kiem tra xem video co thuoc playlist
     * @param $videoId
     * @param $playlistId
     * @return array|bool
     */
    public static function checkVideoInPlaylist($videoId, $playlistId)
    {

        return self::find()
            ->select('p.id, p.name, p.price_play')
            ->from('vt_playlist p')
            ->leftJoin('vt_playlist_item pi', 'p.id = pi.playlist_id')
            ->where(['p.status' => self::STATUS_APPROVE, 'pi.item_id' => $videoId, 'pi.playlist_id' => $playlistId, 'p.is_active' => self::ACTIVE])
            ->one();
    }

    /**
     * @author: PhuMX
     * @param $type
     * @param $limit
     * @param $offset
     * @return lay danh sach playlist theo userId
     */
    public static function getPlayListByUserId($userId, $type, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->where(['created_by' => $userId, 'type' => $type, 'status' => self::STATUS_APPROVE, 'is_active' => self::ACTIVE])
            ->orderBy('id DESC');

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;

    }

    public static function searchByName($q, $limit = 10)
    {
        $query = self::find()
            ->asArray()
            ->select('id, name as text')
            ->where([
                'is_active' => self::ACTIVE,
                'status' => self::STATUS_APPROVE
            ])
            ->andWhere(['like', 'name', $q])
            ->limit($limit);

        return $query->all();
    }

    /**
     * @param $videoId
     * @param int $increase
     * @return int
     * @throws Cap nhat so luot xem cua Playlist
     */
    public static function updatePlayTimes($videoId)
    {
        $rawQuery = "UPDATE vt_playlist SET play_times = IFNULL(play_times, 0) + 1 WHERE id=:id";
        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $videoId)
            ->execute();
    }

    public static function getDataSynNeedApprove($limit)
    {
        $query = self::find()
            ->where([
                'status' => self::STATUS_DRAFT
            ])
            ->andWhere(['not', ['syn_id' => null]])
            ->limit($limit);

        return $query->all();
    }
}