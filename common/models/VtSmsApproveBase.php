<?php

namespace common\models;

use Yii;

class VtSmsApproveBase extends \common\models\db\VtSmsApproveDB {

    public function isHasPermistionApprove(){
        return Yii::$app->user->can('approve-promotion-sms-permistion');
    }

    public  function isHasPermistionRemove(){
        return Yii::$app->user->can('remove-promotion-sms-permistion');
    }

    public  function isHasPermistionOpenSMS(){
        return Yii::$app->user->can('open-promotion-sms-permistion');
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sms_code' => Yii::t('app', 'Mã tin nhắn'),
            'sms_content' => Yii::t('app', 'Nội dung tin nhắn đã duyệt'),
            'sms_content_draft' => Yii::t('app', 'Nội dung tin nhắn chờ duyệt'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'note' => Yii::t('app', 'Ghi chú'),
            'status' => Yii::t('app', 'Trạng thái'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}

