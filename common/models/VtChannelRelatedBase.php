<?php

namespace common\models;

use backend\models\VtChannelRelated;
use Yii;
use yii\helpers\ArrayHelper;

class VtChannelRelatedBase extends \common\models\db\VtChannelRelatedDB
{


    public static function addChannel($channelId, $relatedChannelId)
    {
        $objChannel = self::findOne(['channel_id' => $channelId, 'channel_related_id' => $relatedChannelId]);
        if (!$objChannel) {
            $channelR = new VtChannelRelated();
            $channelR->channel_id = $channelId;
            $channelR->channel_related_id = $relatedChannelId;
            $channelR->save(false);
            return true;

        } else {
            return false;
        }

    }

    public static function removeChannel($channelId, $relatedChannelId)
    {
        $objChannel = self::findOne(['channel_id' => $channelId, 'channel_related_id' => $relatedChannelId]);
        if ($objChannel) {
            $objChannel->delete();
            return true;
        } else {
            return false;
        }
    }

    public static function getChannelRelatedQuery($channelId, $userId=0){

//        $channelRelated = self::findAll(["channel_id"=>$channelId]);
//        $userId = $channelId;
//        if($channelRelated){
//            $arrIds = ArrayHelper::getColumn($channelRelated, "channel_related_id");
//        }else{
//            $arrIds=[];
//        }

        if($userId){
            $query =  self::find()
                ->asArray()
                ->select('uci.*, uf.channel_related_id')
                 ->from('vt_channel uci')
                ->leftJoin('vt_channel_related uf', 'uci.id=uf.channel_related_id')
                ->where(['uci.status' => VtChannelBase::ACTIVE])
                ->andWhere(['uf.channel_id'=>$userId])
                ->orderBy('uci.follow_count DESC')
                ->limit(20);
        }else{

            $query = self::find()->asArray()
                ->select('uci.*')
                ->from('vt_channel uci')
                ->where(['uci.status' => VtChannelBase::ACTIVE])
//                ->andWhere(['uci.id'=>$arrIds])
                ->orderBy('uci.follow_count DESC')
                ->limit(20);
        }
        return $query;
    }

    public static function getAllChannelrelated() {
        $query = self::find()
            ->asArray();
        return $query->all();
    }

    // update channel_related_id
    public static function updateChannelIDByChannelRelatedId($channelId,$channelrelatedId) {
        return self::updateAll(
            [
                'channel_related_id' => $channelrelatedId,
            ], 'id = :id', [
                ':id' => $channelId
            ]
        );
    }
}