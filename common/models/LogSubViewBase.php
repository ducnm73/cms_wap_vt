<?php

namespace common\models;

use Yii;

class LogSubViewBase extends \common\models\db\LogSubViewDB {

    public function insertLog($video, $sub, $subType)
    {
        $this->datetime = date('Y-m-d H:i:s');
        $this->msisdn = $sub['msisdn'];
        $this->action = $video['type'];
        $this->video_id = $video['id'];
        $this->sub_type = $subType;
        $this->cp_id = $video['cp_id'];
        $this->cp_code = $video['cp_code'];
        $this->package_type = $sub['sub_service_name'];
        $this->package_id = $sub['package_id'];
        $this->first_charge = 0;
        $this->other_info = 'WAP';
        return $this->save();
    }


    public static function checkVodWatched($video, $sub)
    {
        $query = self::find()
            ->where('msisdn = :msisdn', [':msisdn' => $sub['msisdn']])
            ->andWhere('action = :action', [':action' => $video['type']])
            ->andWhere('datetime > :last_charge_time', [':last_charge_time' => $sub['last_charge_time']])
            ->andWhere('video_id = :video_id', [':video_id' => $video['id']]);

        return $query->count();
    }

}