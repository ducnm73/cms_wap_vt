<?php

namespace common\models;

use Yii;

class VtPlaylistSearchBase extends \yii\elasticsearch\ActiveRecord
{
    public static function type()
    {
        return 'playlist';
    }

    public static function index()
    {
        return 'myvideo';
    }

    public static function search($textQuery, $limit = 12)
    {
        $query = self::find()->asArray()->query([
            "multi_match" => [
                "fields" => ["name^4", "description^3", "name_slug^2", "description_slug"],
                "type" => "best_fields",
                "analyzer" => "my_analyzer",
                "query" => $textQuery,
                "fuzziness" => 0,
                "tie_breaker" => 0.3
            ]
        ])
        ->filterWhere([
            "is_active" => VtPlaylistBase::ACTIVE,
            "status" => VtPlaylistBase::STATUS_APPROVE
        ])
        ->limit($limit);

        return $query->all();
    }


}