<?php

namespace common\models;

use Yii;

class VtGcmBase extends \common\models\db\VtGcmDB {

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Ham luu clientId
     * @author ducda2@viettel.com.vn
     * @param $userId
     * @param $msisdn
     * @param $registerId
     * @param $status
     * @param $type
     */
    public function insertClientId($userId, $msisdn, $registerId, $status, $type)
    {
        $this->user_id = $userId;
        $this->msisdn = $msisdn;
        $this->register_id = $registerId;
        $this->status = $status;
        $this->type = $type;
        $this->save();
    }


    /**
     * Lay ra ban ghi theo user_id
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByUserId($userId)
    {
        return self::find()
            ->where('user_id = :user_id', [':user_id'=>$userId])
            ->one();
    }


    /**
     * Lay ban ghi dua theo register_id
     * @param $registerId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByRegisterId($registerId)
    {
        return self::find()
            ->where('register_id = :register_id', [':register_id'=>$registerId])
            ->one();
    }

}