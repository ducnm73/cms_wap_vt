<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

class VtUserSearchBase extends \yii\elasticsearch\ActiveRecord
{


    public static function type()
    {
        return 'vt-channel';
    }

    public static function index()
    {
        return 'mychannel';
    }

//    public static function search($textQuery, $limit = 12)
//    {
//        $query = self::find()->asArray()->query([
//            "multi_match" => [
//                "fields" => ["full_name^4", "description^3", "full_name_slug^2", "description_slug"],
//                "type" => "best_fields",
//                "analyzer" => "my_analyzer",
//                "query" => $textQuery,
//                "fuzziness" => 0,
//                "tie_breaker" => 0.3
//            ]
//        ])
//        ->filterWhere([
//            "status" => VtUserBase::ACTIVE
//        ])
//        ->limit(300);
//
//        $results = $query->all();
//
//        $topResults = [];
//        for($i = 0;$i < count($results);$i++){
//
//            if($results[$i]['_score'] >= 8){
//                $topResults[] = $results[$i];
//                unset($results[$i]);
//            }else{
//                $results[$i]['_video_count'] = $results[$i]['_source']['video_count'];
//            }
//        }
//
//        ArrayHelper::multisort($results, ['_video_count'], [SORT_DESC]);
//
//
//        return array_slice(array_merge($topResults, $results), 0, $limit);
//    }


    public static function search($textQuery, $offset = 0, $limit = 12)
    {
        //var_dump($textQuery);die;
        return self::searchV3($textQuery, $offset, $limit);
        /*$slugQuery = Utils::removeSignOnly($textQuery);

        if($slugQuery == $textQuery){
            return self::searchV1($textQuery, $offset, $limit);
        }else{
            return self::searchV2($textQuery, $offset, $limit);
        }*/
    }

    public static function searchV3($textQuery, $offset = 0, $limit = 12) {

        $filters = [
            "must" => [
                [
                    "term" => ["status" => true]
                ],
                [
                    "range" =>  [
                        "video_count"=> [
                            "gt" => 0
                        ]
                    ]
                ]
            ]
        ];

        $params = [

            "from" => $offset,
            "size" => $limit,
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "multi_match" => [
                                "query" => $textQuery,
//                                "type" => "phrase",
                                "fields" =>  ["name^4", "description^3", "full_name_slug^2", "description_slug"],
                                "type" => "phrase_prefix",
//                                "analyzer" => "my_analyzer",
                                // "fuzziness" => 0,
                                "tie_breaker" => 0.3
                            ]
                        ],
                        /*[
                            "multi_match" => [
                                "query" => $textQuery,
                                "type" => "best_fields",
                                "fields" => ["name^4", "test_search^2"],
                                "minimum_should_match" => "95%",
                                "fuzziness" => 2

                            ]
                        ]*/
                    ],
//
//                    "filter" => [
//                        "bool" => $filters
//                    ]

                ]

            ]

        ];

        $url = Yii::$app->params['elasticsearch-curl'] . 'mychannel/_search';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 3,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"

            ),
        ));

        $results = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);

        curl_close($curl);


        $searchResults = [];
        if (isset($results['hits']['hits'])) {
            $searchResults = $results['hits']['hits'];
        }

        $topResults = [];
        for($i = 0;$i < count($searchResults);$i++){
            if($searchResults[$i]['_score'] >= 8){
                $topResults[] = $searchResults[$i];
                unset($searchResults[$i]);
            }else{
                $searchResults[$i]['_video_count'] = $searchResults[$i]['_source']['video_count'];
            }
        }
        //var_dump($searchResults);die;

        ArrayHelper::multisort($searchResults, ['_video_count'], [SORT_DESC]);

        return array_slice(array_merge($topResults, $searchResults), 0, $limit);
    }


}