<?php

namespace common\models;

use Yii;
use yii\db\Expression;

class VtVideoHotCategoryBase extends \common\models\db\VtVideoHotCategoryDB {

    public static function getByIds($ids, $limit = 10) {
        $query = self::find()
            ->asArray()
            ->where(['category_id' => $ids])
            ->limit($limit)
            ->orderBy([new Expression('FIELD (category_id, ' . implode(',', array_filter($ids)) . ')')]);




        return $query->all();
    }

    public static function getAllHotCategories($limit = 10)
    {
        $query = self::find()
            ->asArray()
            ->where(['is_hot' => 1])
            ->limit($limit);

        return $query->all();
    }

}