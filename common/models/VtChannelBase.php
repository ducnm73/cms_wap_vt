<?php

namespace common\models;

use Yii;

class VtChannelBase extends \common\models\db\VtChannelDB {
    const WAIT_APPROVE = 0;
    const APPROVED = 1;
    const DECLINED = 2;
    const ACTIVE = 1;
    const HOT = 1;

    public function getStatusString() {
        switch ($this->status) {
            case self::WAIT_APPROVE:
                return Yii::t('backend', 'Chờ duyệt');
            case self::APPROVED:
                return Yii::t('backend', 'Đã duyệt');
            case self::DECLINED:
                return Yii::t('backend', 'Không duyệt');
        }
    }

    public function getOfficialString() {
//        switch ($this->is_official) {
//            case self::WAIT_APPROVE:
//                return Yii::t('backend', 'No Official');
//            case self::APPROVED:
//                return Yii::t('backend', 'Official');
//        }
    }

    // check xem channel có phai la kenh cua user hay không
    public static function CheckChannelUser($id, $userId) {
        $query = self::find()
            ->where([
                'id' => $id,
                'user_id' => $userId,
            ]);

        return $query->one();
    }
    // check official by user
    public static function CheckChannelOfficialUser($userId) {
        $query = self::find()
            ->where([
                'user_id' => $userId,
                'status' => self::APPROVED,
                // 'is_official' => self::APPROVED,
            ]);

        return $query->one();
    }

    public static function getChannelAllById($id,$status= null) {
        $query = self::find()
            ->asArray()
            ->select('cl.*, cl.full_name as name')
            ->from(self::tableName() . ' cl')
            ->where([
                'cl.user_id' => $id,
            ])
            ->orderBy('cl.id DESC');

        if (!empty($status)) {
            $query->andWhere([
                'cl.status' => $status,
            ]);
        }

        return $query->all();
    }

    public static function getChannelById($id) {
        $query = self::find()
            ->where([
                'id' => $id,
                'status' => self::APPROVED
            ]);
        return $query->one();
    }

    public static function getChannelListById($id,$active=0) {
        $query = self::find()
            ->asArray()
            ->select('cl.*, cl.full_name as name')
            ->from(self::tableName() . ' cl')
            ->where([
                'cl.user_id' => $id,
                'cl.status' => 1,
            ])
            ->orderBy('cl.full_name ASC');

        if ($active==1) {
            $query->andWhere('cl.video_count > :video_count', [':video_count' => 0]);
        }
        return $query->all();
    }

    public static function getCountChannelById($id) {
        $query = self::find()
            ->where([
                'user_id' => $id
            ]);

        return $query->count();
    }

    public static function getCountChannelActiveById($id) {
        $query = self::find()
            ->where([
                'user_id' => $id,
                'status' => 1
            ]);

        return $query->count();
    }
    /* danh sach kenh lien quan */
    public static function getRelatedChannelQuery($limit = 10, $offset = null, $distinctIds = [], $thisId = null) {
        $query = self::find()
            ->asArray()
            ->select('u.*')
            ->from(self::tableName() . ' u')
            ->andWhere(['u.status' => self::ACTIVE]);
        if($thisId) {
            $query->andWhere(['<>','u.id', $thisId]);
            $query->andWhere(['<>','u.user_id', $thisId]);
        }
        if ($distinctIds) {
            $query->andWhere(['not in', 'u.id', $distinctIds]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy('u.follow_count DESC');

        return $query;
    }


    public static function getHotChannelQuery($limit = 10, $offset = null, $distinctIds = [], $thisId = null) {
        $query = self::find()
            ->asArray()
            ->select('u.*')
            ->from(self::tableName() . ' u')
            ->where(['u.is_hot' => self::HOT])
            ->andWhere(['u.status' => self::ACTIVE]);
        if($thisId) {
            $query->andWhere(['<>','u.id', $thisId]);
            $query->andWhere(['<>','u.user_id', $thisId]);
        }
        if ($distinctIds) {
            $query->andWhere(['not in', 'u.id', $distinctIds]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy('u.follow_count DESC');

        return $query;
    }

//    public static function  getHotChannel($limit = 10, $offset = null) {
//        $query = self::getHotChannelQuery($limit, $offset);
//
//        return $query->all();
//    }

    public static function getHotChannel($limit = 10, $offset = null, $distinctIds = [], $thisId = null) {
        $cache = \Yii::$app->cache;
        $key = 'VtChannelBase_getHotChannel' . md5($limit . $offset . $distinctIds . $thisId);
        $data = $cache->get($key);

        if (!$data) {
            $query = self::getHotChannelQuery($limit, $offset, $distinctIds, $thisId);
            $data = $query->all();
            $cache->set($key, $data, CACHE_TIME_OUT);
        }
        return $data;
    }


    public function insertchannel($full_name, $description, $fileBannerPath, $fileAvatarPath, $userId, $imageBucket='',$status = self::WAIT_APPROVE, $follow_count = self::WAIT_APPROVE,$view_count = self::WAIT_APPROVE,$video_count = self::WAIT_APPROVE,$is_hot = self::WAIT_APPROVE)
    {
        $this->full_name = $full_name;
        $this->description = $description;
        $this->channel_path = $fileBannerPath;
        $this->path = $fileAvatarPath;
        $this->user_id = $userId;
        $this->bucket = $imageBucket;
        $this->channel_bucket = $imageBucket;
        $this->status = $status;
        $this->follow_count = $follow_count;
        $this->video_count = $video_count;
        $this->view_count = $view_count;
        $this->is_hot = $is_hot;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->save(false);
    }

    /**
     * Cap nhat truong follow_count tang hoac giam dua theo bien $increase
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateFollowCount($id, $increase) {
        if ($increase) {
            $rawQuery = "UPDATE vt_channel SET follow_count=follow_count + 1 WHERE id=:id";
        } else {
            $rawQuery = "UPDATE vt_channel SET follow_count=follow_count - 1 WHERE id=:id";
        }

        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->execute();
    }

    public static function getByIdsQuery($ids, $limit = 10, $offset = null) {
        $query = self::find()
            ->asArray()
            ->select('v.*')
            ->from(self::tableName() . ' v')
            ->where([
                'v.id' => $ids
            ])
            ->andWhere(['v.status' => self::ACTIVE]);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    public static function getHotUserQuery($limit = 10, $offset = null, $distinctIds = [], $thisId = null) {
        $query = self::find()
            ->asArray()
            ->select('u.*')
            ->from(self::tableName() . ' u')
            ->where(['u.is_hot' => self::HOT])
            ->andWhere(['u.status' => self::ACTIVE]);
        if($thisId) {
            $query->andWhere(['<>','u.id', $thisId]);
        }
        if ($distinctIds) {
            $query->andWhere(['not in', 'u.id', $distinctIds]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy('u.priority DESC');
        return $query;
    }

    public static function getChannelQuery($limit = 10, $offset = null, $channelIds = [],$sort = null) {
        $query = self::find()
            ->asArray()
            ->select('u.*')
            ->from(self::tableName() . ' u')
            ->andWhere(['u.status' => self::ACTIVE]);
        if ($channelIds) {
            $query->andWhere(['in', 'u.id', $channelIds]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        if (!empty($sort)) {
            if ($sort =='play_times') {
                $query->orderBy('u.view_count DESC');
            }
            if ($sort =='created_at') {
                $query->orderBy('u.created_at DESC');
            }
        }else{
            $query->orderBy('u.priority DESC');
        }
//var_dump($query->createCommand()->getRawSql());
        return $query;
    }

    /**
     * @param $channelId
     * @param int $increase
     * @return int
     * @throws Cap nhat so luot xem cua kenh
     */
    public static function updateViewcount($channelId)
    {
        $rawQuery = "UPDATE vt_channel SET view_count = IFNULL(view_count, 0) + 1 WHERE id=:id";
        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $channelId)
            ->execute();
    }

    /**
     * @param $channelId
     * @param int $increase
     * @return int
     * @throws Cap nhat so luong video cua kenh
     */
    public static function updateVideoCountByChannelId($channelId) {

        $videoCount = VtVideoBase::countActiveVideoByChannel($channelId);

        return self::updateAll(
            [
                'video_count' => $videoCount,
            ], 'id = :id', [
                ':id' => $channelId
            ]
        );
    }

    public static function getVideoTotal($userid)
    {
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' c')
            ->select('SUM(video_count) as total')
            ->where(['c.user_id' => $userid]);
        return $query->one();
    }
    public static function getChannelActiveById($id) {
        $query = self::find()
            ->asArray()
            ->select('cl.*, cl.full_name as name')
            ->from(self::tableName() . ' cl')
            ->where([
                'cl.user_id' => $id,
                'status' => 1
            ])
            ->orderBy('cl.id DESC');

        return $query->all();
    }
}