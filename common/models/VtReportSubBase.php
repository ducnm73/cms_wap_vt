<?php

namespace common\models;

use Yii;

class VtReportSubBase extends \common\models\db\VtReportSubDB
{

    public static function getByDate($packageTypes, $beginDate, $endDate)
    {
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' r')
            ->where(['package_type' => $packageTypes])
            ->andWhere('report_date between :begin_date and :end_date', [':begin_date' => $beginDate, ':end_date' => $endDate]);

        return $query->all();
    }

    public static function getArrByDate($packageTypes, $beginDate, $endDate)
    {
        $reports = self::getByDate($packageTypes, $beginDate, $endDate);

        $arr = [];
        foreach ($reports as $report) {
            $arr[$report['report_date']][$report['package_type']] = $report['active_sub'];
        }

        return $arr;
    }

}