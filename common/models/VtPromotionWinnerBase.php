<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtPromotionWinnerBase extends \common\models\db\VtPromotionWinnerDB {


    public static function getWinners(){

        return self::find()
            ->asArray()
            ->select("v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn")
            ->from("vt_promotion_winner w")
            ->leftJoin("vt_video v", 'v.id = w.video_id')
            ->leftJoin("vt_user u", "u.id = w.user_id")
            ->where(["w.is_active"=>1])
            ->andWhere(
                [
                    'v.type' => VtVideoBase::TYPE_VOD,
                    'v.status' => VtVideoBase::STATUS_APPROVE,
                    'v.is_active' => VtVideoBase::ACTIVE,
                    'v.is_no_copyright' => 0
                ])
            ->andWhere('v.syn_id is null')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy("w.priority DESC");

    }
}