<?php

namespace common\models;

use Yii;

class VtPackageBase extends \common\models\db\VtPackageDB
{

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    public static function getAllPackage()
    {
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;
        //var_dump($currentLanguage);die;
        $query = self::find()
            //->asArray()
            ->orderBy('group_type, priority');
            //->all();

        if ($mainLanguage == $currentLanguage) {
            return $query->asArray()->all();
        } else {
            $temp = $query->asArray()->all();
//            $multilang = $temp['multilang'];
//            if (is_null($multilang)) {
//                return $temp;
//            }
//            $responData=LanguageConvert::arrayExclude($temp,['name','short_description','description']);
//            $multiLangFieldPart = LanguageConvert::convertFieldsToArray($multilang,
//                ['name','short_description','description'],$currentLanguage, $temp);
//
//            return array_merge($responData,$multiLangFieldPart);

            $respon=[];
            foreach ($temp as $childArr) {
                $multilang = $childArr['multilang'];
                if (is_null($multilang)) {
                    $respon[]=$childArr;
                }
                $childPart1=LanguageConvert::arrayExclude($childArr,['name','short_description','description']);
                $childPart2 = LanguageConvert::convertFieldsToArray($multilang,
                    ['name','short_description','description'],$currentLanguage, $childArr);

                $respon[]=array_merge($childPart1,$childPart2);
                //var_dump($respon);die;
            }
            return $respon;
        }

    }

    public static function getListPackage($distributionId = null)
    {
         $query = self::find()
            ->where(['is_active' => self::ACTIVE]);

         if(!empty($distributionId)){
             $query->andWhere(['distribution_id' => $distributionId]);
         }
         $query->orderBy('group_type, priority');

        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;
        if ($mainLanguage == $currentLanguage && $mainLanguage != 'mz') {
            return $query->asArray()->all();

        } else {
            $temp = $query->asArray()->all();
            // var_dump($temp);die;
            $respon=[];
            foreach ($temp as $childArr) {
                $multilang = $childArr['multilang'];
                if (is_null($multilang)) {
                    $respon[]=$childArr;
                }
                $childPart1=LanguageConvert::arrayExclude($childArr,['name','short_description','description']);
                $childPart2 = LanguageConvert::convertFieldsToArray($multilang,
                    ['name','short_description','description'],$currentLanguage, $childArr);

                $respon[]=array_merge($childPart1,$childPart2);
                //var_dump($respon);die;
            }
            return $respon;
        }
    }

    public static function getSuggestPackage($packageId = null)
    {
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
                'is_display_frontend' => self::ACTIVE
            ])
            ->orderBy('priority');

        if(!empty($packageId)){
            $query->andWhere(['id' => $packageId]);
        }

        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;
        if ($mainLanguage == $currentLanguage && $mainLanguage != 'mz') {
            return $query->one();

        } else {
            $temp = $query->one();
            $multilang = $temp['multilang'];
            if(is_null($temp)) {
                return $temp;
            } else {
                $childPart1=LanguageConvert::arrayExclude($temp,['name','short_description','description']);
                $childPart2 = LanguageConvert::convertFieldsToArray($multilang,
                    ['name','short_description','description'],$currentLanguage, $temp);

                $temp = array_merge($childPart1,$childPart2);
            }
            return $temp;
        }
    }

    public static function getDistributionPackage($distributionId){
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
                'is_display_frontend' => self::ACTIVE,
                'distribution_id' => $distributionId
            ])
            ->orderBy('priority');

        return $query->one();
    }

    public static function getDistributionPackageById($id){
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
                'id' => $id,

            ])
            ->andWhere(['not', ['distribution_id' => null]])
            ->orderBy('priority');

        return $query->all();
    }

    public static function getAllSubViewPackage(){
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
                'distribution_id' => null
            ])
            ->orderBy('priority');

        return $query->all();
    }

    public static function getAllDistributionPackage(){
        $query = self::find()
            ->asArray()
            ->where([
                'is_active' => self::ACTIVE,
            ])
            ->andWhere(['not', ['distribution_id' => null]])
            ->orderBy('priority');

        return $query->all();
    }

    public static function getDistributionPackageByCpId($cpId){
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' p')
            ->leftJoin('vt_distribution d', 'p.distribution_id = d.id')
            ->where([
                'p.is_active' => self::ACTIVE,
                'd.cp_id' => $cpId,
            ])
            ->andWhere(['not', ['distribution_id' => null]])
            ->orderBy('priority');

        return $query->all();
    }

    public static function getDetail($id)
    {
        return self::find()
            ->asArray()
            ->where(['id' => $id])
            ->one();
    }

    public static function getDetailMultilang($id)
    {
        $query = self::find()
            ->asArray()
            ->where(['id' => $id]);

        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;
        if ($mainLanguage == $currentLanguage && $mainLanguage != 'mz') {
            return $query->one();

        } else {
            $temp = $query->one();
            $multilang = $temp['multilang'];
            if(is_null($temp)) {
                return $temp;
            } else {
                $childPart1=LanguageConvert::arrayExclude($temp,['name','short_description','description']);
                $childPart2 = LanguageConvert::convertFieldsToArray($multilang,
                    ['name','short_description','description'],$currentLanguage, $temp);

                $temp = array_merge($childPart1,$childPart2);
            }
            return $temp;
        }
    }

}