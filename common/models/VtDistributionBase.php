<?php

namespace common\models;

use Yii;

class VtDistributionBase extends \common\models\db\VtDistributionDB {

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    public static function getOneBySubDomain($subDomain){
        $query = self::find()
            ->asArray()
            ->where([
                'sub_domain' => $subDomain,
                'is_active' => self::ACTIVE
            ]);

        return $query->one();
    }
}