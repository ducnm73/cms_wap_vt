<?php

namespace common\models;

use Yii;

class VtFeedBackBase extends \common\models\db\VtFeedBackDB {
    public $numOfRecord;
    public $videoName;

    /**
     * Insert thong bao loi tu khach hang
     * @param $msisdn
     * @param $id
     * @param $content
     */
    public function insertFeedBack($userId, $msisdn, $requestType, $content, $itemId, $type)
    {
        $this->msisdn = $msisdn;
        $this->user_id = $userId;
        $this->content = $content;
        $this->request_type = $requestType;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->item_id = $itemId;
        $this->type = $type;

        $this->save(false);
    }

    /** Dem so luot feedback cua user trong ngay hien tai
     * @param $msisdn
     * @return $this
     */
    public static function countFeedbackByUserThisDay($userId, $msisdn)
    {
        return self::find()
            ->where(['date(created_at)' => date('Y-m-d')])
            ->andWhere('msisdn = :msisdn OR user_id = :userId ', [':msisdn'=>$msisdn, ':userId'=>$userId ])
            ->count();
    }

    /** list total of reports from user depending each video and feedback type
     * @param $fromTime, $toTime, $feedbackTypeId
     * @return $this
     * @author thuynd10
     */
    public static function summaryFeedBack($fromTime, $toTime, $feedbackTypeId){
        $feedBackGeneral = self::find()
            ->select(['count(id) as numOfRecord', 'request_type'])
            ->where(['>=', 'created_at', $fromTime])
            ->andWhere(['<=', 'created_at', $toTime]);
        if($feedbackTypeId != null){
            $feedBackGeneral->andWhere(['request_type' => $feedbackTypeId]);
        }
        $feedBackGeneral = $feedBackGeneral->groupBy(['request_type'])->all();
        return $feedBackGeneral;
    }

    /** Explain total of reports from user for each video depending feedback type
     * @param $fromTime, $toTime, $feedbackTypeId
     * @return $this
     * @author thuynd10
     */
    public static function detailFeedBack($fromTime, $toTime, $feedbackTypeId){
        $query = self::find()
            ->select(['count(vfb.id) as numOfRecord', 'vfb.request_type', 'vfb.item_id', 'vv.name as videoName'])
            ->from('vt_feed_back vfb')
            ->leftJoin('vt_video vv', '`vv`.`id` = `vfb`.`item_id`')
            ->where(['>=', 'vfb.created_at', $fromTime])
            ->andWhere(['<=', 'vfb.created_at', $toTime]);
        if($feedbackTypeId != null){
            $query->andWhere(['vfb.request_type' => $feedbackTypeId]);
        }
        $query = $query->groupBy(['vfb.request_type', 'vfb.item_id'])->orderBy('vfb.request_type');
        return $query;
    }

    /** list user who report a specific video, then export them into an excel file.
     * @param $fromTime, $toTime, $feedbackTypeId, $itemId (video id)
     * @return $this
     * @author thuynd10
     */
    public static function listReporter($fromDate, $toDate, $feedbackTypeId, $itemId){
        $feedBack = self::find()
            ->select(['created_at', 'msisdn'])
            ->where(['>=', 'created_at', $fromDate])
            ->andWhere(['<=', 'created_at', $toDate])
            ->andWhere(['request_type' => $feedbackTypeId])
            ->andWhere(['item_id' => $itemId])
            ->asArray()
            ->all();
        return $feedBack;
    }
}