<?php

namespace common\models;

use Yii;

class VtSmsMessageBase extends \common\models\db\VtSmsMessageDB {

    public static function getConfig($key)
    {
        // su dung cache config
        if (Yii::$app->params['cache.enabled'] == true) {
            return self::getDb()->cache(function ($db) use ($key) {
                $config = self::find()
                    ->asArray()
                    ->select('sms_code, sms_message')
                    ->where('sms_code = :sms_code', [':sms_code' => $key])
                    ->one();
                return $config['sms_message'];
            });
        } else {
            $config = self::find()
                ->asArray()
                ->select('sms_code, sms_message')
                ->where('sms_code = :sms_code', [':sms_code' => $key])
                ->one();
            return $config['sms_message'];
        }
    }
}