<?php

namespace common\models;

use common\helpers\Utils;
use Yii;
use yii\db\Expression;

class VtUserBase extends \common\models\db\VtUserDB {

    const ACTIVE = 1;
    const INACTIVE = 0;
    const BANNED = 2;
    const NEED_CHANGE_PASWORD = 0;
    const DONT_NEED_CHANGE_PASWORD = 1;
    const HOT = 1;
    const NOTIFICATION_READ = 1;
    const NOTIFICATION_NOT_READ = 0;

    /**
     * Luu thong tin user register theo msisdn
     * @param $msisdn
     * @param $ip
     * @param $userAgent
     * @param $hashPassword
     * @param $salt GD
     */
    public function insertUserMsisdn($ip, $msisdn, $userAgent, $hashPassword, $salt) {
        $this->msisdn = $msisdn;
        $this->full_name = $msisdn;
        // $this->ip = $ip;
        // $this->user_agent = $userAgent;
        $this->status = self::ACTIVE;
        $this->changed_password = self::DONT_NEED_CHANGE_PASWORD;


        if (!empty($hashPassword)) {
            $this->password = $hashPassword;
        }
        if (!empty($salt)) {
            $this->salt = $salt;
        }

        $this->save(false);
    }

    /**
     * Lay ra ban ghi active dua theo id
     * @author ducda2@viettel.com.vn
     * @param $token
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getById($id) {
        $query = self::find()
                ->where([
            'id' => $id
        ]);

        return $query->one();
    }

    /**
     * Lay ra ban ghi active dua theo id
     * @author ducda2@viettel.com.vn
     * @param $token
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getActiveUserById($id) {
        $query = self::find()
                ->where([
            'id' => $id,
            'status' => self::ACTIVE
        ]);

        return $query->one();
    }

    /**
     * Lay ra ban ghi dua theo so dien thoai
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByMsisdn($msisdn) {
        $query = self::find()
                ->where('msisdn = :msisdn', [':msisdn' => $msisdn]);

       // var_dump($query->createCommand()->rawSql); die('xxx');
        return $query->one();
    }

    /**
     * Lay ra ban ghi dua theo so dien thoai hoac
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByMsisdnOrEmail($msisdn, $email) {
        $query = self::find()
                ->where('msisdn = :msisdn or email = :email', [':msisdn' => $msisdn, 'email' => $email]);

        return $query->one();
    }

    public function checkPassword($password) {
        $algorithmOld = 'sha1';
        $algorithmNew = 'sha256';

        //if (!is_callable($algorithmOld) || !is_callable($algorithmNew)) {
        //    throw new \Exception(sprintf('The algorithm callable "%s" is not callable.', $algorithmNew." ".$algorithmNew));
        //}
        //Check nhung user cu theo thuat cu sha1
        if ($this->password == hash($algorithmOld, $this->salt . $password)) {
            return true;
        } else {
            return $this->password == hash($algorithmNew, $this->salt . $password);
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->salt = md5(uniqid());
        // Update pass moi theo sha256
        $algorithm = 'sha1';
        //if (!is_callable($algorithm)) {
        //    throw new sfException(sprintf('The algorithm callable "%s" is not callable.', $algorithm));
        // }
        $this->password = hash($algorithm, $this->salt . $password);

        $this->changed_password = 1;
        $this->save(false);
    }

    /**
     * Cap nhat truong follow_count tang hoac giam dua theo bien $increase
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateFollowCount($id, $increase) {
        if ($increase) {
            $rawQuery = "UPDATE vt_user SET follow_count=follow_count + 1 WHERE id=:id";
        } else {
            $rawQuery = "UPDATE vt_user SET follow_count=follow_count - 1 WHERE id=:id";
        }

        return Yii::$app->db->createCommand($rawQuery)
                        ->bindValue(':id', $id)
                        ->execute();
    }

    public static function updateMultiFollowCount($ids, $increase)
    {
        if ($increase) {
            return self::updateAll(['follow_count' => new Expression('follow_count + 1')], ['in', 'id', $ids]);
        } else {
            return self::updateAll(['follow_count' => new Expression('follow_count - 1')], ['in', 'id', $ids]);
        }
    }


    /**
     * @author PhuMX
     * Laydanh sach thanh vien, sap xep theo so luong follow
     * @param bool $limit
     * @param bool $offset
     * @return $this
     */
    public static function getListMember($userId = false, $limit = false, $offset = false) {
        $query = self::find()->asArray()
                ->from('vt_user u')
                ->where(['u.status' => self::ACTIVE])
                ->orderBy('u.follow_count DESC');
        //Neu co truyen user dang nhap thi check xem co dang follow
        if ($userId) {
            $query->select('u.*, uf.follow_id');
            $query->leftJoin('vt_user_follow uf', 'u.id=uf.follow_id AND uf.user_id=:userId', [':userId' => $userId]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    public function findOneByMsisdn($msisdn) {
        $query = self::find()
                ->where(['msisdn' => $msisdn]);

        return $query->one();
    }

    public static function getByOauthId($oAuthId) {
        return self::find()
                        ->where(['oauth_id' => $oAuthId])
                        ->orWhere(['google_oauth_id'=>$oAuthId])
                        ->one();
    }

    public static function getByGoogleOauthId($oAuthId) {
        return self::find()
            ->where(['google_oauth_id' => $oAuthId])
            ->one();
    }

    public function insertUserSocial($oAuthId, $name, $email, $bucket = '', $path = '', $loginVia='') {
        $this->email = $email;
        $this->full_name = $name;
        $this->full_name_slug = Utils::removeSignOnly($name);
        if($loginVia=='GOOGLE'){
            $this->google_oauth_id = $oAuthId;
        }else{
            $this->oauth_id = $oAuthId;
        }

        $this->status = self::ACTIVE;
        $this->last_login = date("Y-m-d H:i:s");
        $this->bucket = $bucket;
        $this->path = $path;
        $this->save(false);
    }

    public function insertAutoLogin($msisdn) {
        $this->msisdn = $msisdn;
        $this->status = self::ACTIVE;
        $this->last_login = date("Y-m-d H:i:s");
        $this->save(false);
    }

    public static function updateVideoCountByUserId($userId) {
        $videoCount = VtVideoBase::countActiveVideoByUser($userId);

        return self::updateAll(
                        [
                    'video_count' => $videoCount,
                        ], 'id = :id', [
                    ':id' => $userId
                        ]
        );
    }

    public function getStatusString() {
        switch ($this->status) {
            case self::ACTIVE:
                return Yii::t('backend', 'Hoạt động');
            case self::INACTIVE:
                return Yii::t('backend', 'Chưa active');
            case self::BANNED:
                return Yii::t('backend', 'Bị khóa');
        }
    }

    public static function getByIdsQuery($ids, $limit = 10, $offset = null) {
        $query = self::find()
                ->asArray()
                ->select('v.*')
                ->from(self::tableName() . ' v')
                ->where([
                    'v.id' => $ids
                ])
                ->andWhere(['v.status' => self::ACTIVE]);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    public static function getHotUserQuery($limit = 10, $offset = null, $distinctIds = [], $thisId = null) {
        $query = self::find()
                ->asArray()
                ->select('u.*')
                ->from(self::tableName() . ' u')
                ->where(['u.is_hot' => self::HOT])
                ->andWhere(['u.status' => self::ACTIVE]);
        if($thisId) {
            $query->andWhere(['<>','u.id', $thisId]);
        }
        if ($distinctIds) {
            $query->andWhere(['not in', 'u.id', $distinctIds]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy('u.priority DESC');
        return $query;
    }

    public static function getHotUser($limit = 10, $offset = null) {
        $query = self::getHotUserQuery($limit, $offset);
        return $query->all();
    }

    public static function getFollowByUserId($userId, $limit = 10, $offset = null) {
        $query = self::find()
                ->asArray()
                ->select('u.*')
                ->from(self::tableName() . ' u')
                ->leftJoin('vt_user_follow uf', 'u.id=uf.follow_id')
                ->where([
            'u.status' => self::ACTIVE,
            'uf.user_id' => $userId
        ]);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy('u.id DESC');
        return $query;
    }

    public static function searchByName($name, $limit = 10) {
        $query = self::find()
                ->asArray()
                ->select('id, full_name as text')
                ->Where(['like', 'full_name', $name])
                ->limit($limit);

        return $query->all();
    }

    public static function getAllUser($type = null) {
        $query = self::find()
                ->asArray();
        return $query->all();
    }

    public static function updateRandomAvatar($id){

        $videoObj = self::findOne(["id"=>$id]);

        if($videoObj){
            //Neu chua co anh Avatar
            if(!$videoObj["bucket"]){
                    $avatarImages = Yii::$app->params['random.avatar.channel'];
                    $randImage = $avatarImages[rand(0, count($avatarImages)-1)];
                    $videoObj["bucket"]= $randImage["bucket"];
                    $videoObj["path"]= $randImage["path"];
            }
            //Neu chua co anh Avatar
            if(!$videoObj["channel_bucket"]){
                $avatarChannels = Yii::$app->params['random.banner.channel'];
                $randImage = $avatarChannels[rand(0, count($avatarChannels)-1)];
                    $videoObj["channel_bucket"]= $randImage["bucket"];
                    $videoObj["channel_path"]= $randImage["path"];
            }
            $videoObj->save(false);
        }
    }

    public static function getLastestPosition()
    {
        $query = self::find()
            ->asArray()
            ->where(['is_hot' => self::HOT])
            ->orderBy(['priority' => SORT_ASC]);

        $result = $query->one();

        if ($result) {
            return $result['priority'] - 1;
        } else {
            return 0;
        }

    }

    public function generateAvatarThumbs() {
        return VtHelper::generateThumbs($this->path, $this->bucket);
    }

    public function generateThumbs() {
        return VtHelper::generateThumbs($this->channel_path, $this->channel_bucket);
    }
}
