<?php

namespace common\models;

use Yii;

class VtUserTopicBase extends \common\models\db\VtUserTopicDB
{
    /**
     * Lay danh sach the loai nguoi dung thich
     * @param $userId
     * @return array
     */
    public static function getTopicLiked($userId, $msisdn, $limit = false, $isReal = false)
    {
        if ($isReal) {
            $topicAdminSuggestId = 0;
        } else {
            $topicAdminSuggestId = VtConfigBase::getConfig('topic_admin_suggest', '168,169,170,171,172,173,177,178,179,181');
        }
        //Lay du lieu cua User va du lieu cua admin them vao( admin them co user_id = 0 va msisdn = 0)
        $query = self::find()
            ->asArray()
            ->select('DISTINCT(t.id),t.*')
            ->from(self::tableName() . ' v')
            ->leftJoin(VtGroupTopicBase::tableName() . ' t', 't.id = v.topic_id and t.is_active = 1')
            ->where('(user_id = :userId AND user_id >0) OR (msisdn= :msisdn AND msisdn>0) OR (t.id IN (' . $topicAdminSuggestId . ') )', ['userId' => $userId, 'msisdn' => $msisdn])
            ->andWhere('t.is_active =1 ')
            ->orderBy('user_id DESC, msisdn DESC');

        if ($limit) {
            $query->limit($limit);
        }

        return $query;
    }

    public static function getLike($userId, $msisdn, $topicId)
    {
        return self::find()
            ->where('((user_id = :userId AND user_id >0) OR (msisdn= :msisdn AND msisdn>0) ) AND topic_id = :topicId ', ['userId' => $userId, 'msisdn' => $msisdn, 'topicId' => $topicId])
            ->one();
    }

    public function insertLike($userId, $msisdn, $topicId)
    {
        $this->user_id = ($userId) ? $userId : 0;
        $this->msisdn = ($msisdn) ? $msisdn : 0;
        $this->topic_id = $topicId;
        $this->save(false);
    }


}