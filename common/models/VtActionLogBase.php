<?php

namespace common\models;

use common\libs\VtHelper;
use wap\models\VtActionLog;
use Yii;

class VtActionLogBase extends \common\models\db\VtActionLogDB
{

    const T_CREATE = 'NEW';
    const T_UPDATE = 'UPDATE';
    const T_DEACTIVE = 'DEACTIVE';
    const T_APPROVED = 'APPROVED';
    const T_WAIT_APPROVED = 'WAIT_APPROVED';
    const T_RECONVERT = 'RECONVERT';

    const M_ADMIN_VIDEO = 'ADMIN_VIDEO';
    const M_ADMIN_PLAYLIST = 'ADMIN_PLAYLIST';
    const M_FRONTEND_VIDEO = 'FRONTEND_VIDEO';
    const M_FRONTEND_PLAYLIST = 'FRONTEND_PLAYLIST';
    const M_API_VIDEO = 'API_VIDEO';
    const M_API_PLAYLIST = 'API_PLAYLIST';

    const M_VT_USER_PLAYLIST = 'VT_USER_PLAYLIST';
    const CMS_CREATED_PLAYLIST = 'CMS_CREATED_PLAYLIST';
    const CMS_UPDATED_PLAYLIST = 'CMS_UPDATED_PLAYLIST';
    const CMS_DELETE_PLAYLIST = 'CMS_DELETE_PLAYLIST';
    const CMS_FINISH_PLAYLIST = 'CMS_FINISH_PLAYLIST';


    const TYPE_CREATE = 'CREATE';
    const TYPE_UPDATE = 'UPDATE';
    const TYPE_DELETE = 'DELETE';
    const TYPE_VIEW = 'VIEW';
    const TYPE_LOGIN = 'LOGIN';
    const TYPE_LOGOUT = 'LOGOUT';
    const TYPE_IMPORT = 'IMPORT';

    const MODULE_LOGIN = 'LOGIN';
    const MODULE_VIDEO = 'VIDEO';
    const MODULE_PLAYLIST = 'PLAYLIST';
    const MODULE_CATEGORY = 'CATEGORY';
    const MODULE_TOPIC = 'TOPIC';
    const MODULE_USER_PLAYLIST = 'USER_PLAYLIST';
    const MODULE_USER = 'USER';
    const MODULE_CHANNEL = 'CHANNEL';
    const MODULE_AUTH_USER = 'AUTH_USER';
    const MODULE_COMMENT = 'COMMENT';
    const MODULE_SPAM = 'SPAM';
    const MODULE_MAPPING_CRAWLER = 'MAPPING_CRAWLER';
    const MODULE_MAPPING_TOPIC = 'MAPPING_TOPIC';
    const MODULE_MAPPING_USER = 'MAPPING_USER';
    const MODULE_CONFIG = 'CONFIG';
    const MODULE_REPORT = 'REPORT';
    const MODULE_CUSTOMER_CARE = 'CUSTOMER_CARE';
    const MODULE_PROMOTION_DATA = 'PROMOTION_DATA';
    const MODULE_CONTRACT = 'CONTRACT';
    const MODULE_UPLOADER_COMPLAIN = 'UPLOADER_COMPLAIN';
    const MODULE_ACCOUNT_INFORMATION = 'ACCOUNT_INFORMATION';
    const MODULE_USER_CHANGE_INFO = 'USER_CHANGE_INFO';
    const MODULE_CHANNEL_CHANGE_INFO = 'CHANNEL_CHANGE_INFO';

    const SOURCE_BACKEND = 'BACKEND';
    const SOURCE_WEBSITE = 'WEBSITE';
    const SOURCE_WAPSITE = 'WAPSITE';
    const SOURCE_CP = 'CP';
    const SOURCE_APP = 'APP';


//    public static function insertLog($module, $contentId, $type, $action, $user, $source = 'ADMIN') {
//        $log = new VtActionLogBase();
//        $log->module = $module;
//        $log->content_id = $contentId;
//        $log->type = $type;
//        $log->action = $action;
//        $log->created_at = date('Y-m-d H:i:s');
//        $log->user_id = $user;
//        $log->source = $source;
//        $log->save(false);
//    }


    public static function insertLog($module, $object, $type, $userId, $source)
    {
        $objectArr = ( is_object($object) )? $object->getAttributes() : null;

        $log = new VtActionLogBase();
        $log->module = $module;
        $log->content_id = is_array($objectArr) ? $objectArr['id'] : null;
        $log->type = $type;
        $log->detail = is_array($objectArr) ? json_encode($objectArr) : null;
        $log->user_id = $userId;
        $log->source = $source;
        $log->created_at = date('Y-m-d H:i:s');
        $log->ip = VtHelper::getAgentIp();
        $log->url = Yii::$app->request->getAbsoluteUrl();
        return $log->save();
    }


}
