<?php

namespace common\models;

use Yii;

class VtGroupTopicBase extends \common\models\db\VtGroupTopicDB {

    const ACTIVE = 1;
    const INACTIVE = 0;

    const TYPE_VOD = 'VOD';
    const TYPE_FILM = 'FILM';

    public function getVideos() {
        return $this->hasMany(VtVideoBase::className(), ['id' => 'video_id'])
            ->viaTable(VtVideoTopicBase::tableName(), ['topic_id' => 'id']);
    }

    
    public static function getDetail($id){
        return self::find()->asArray()
            ->where(['id'=>$id, 'is_active'=>self::ACTIVE])
            ->one();
    }
    
    public static function getAllTopic($type = null){
        $query = self::find()
            ->asArray()
            ->select('*')
            ->orderBy('id DESC');

        if(!empty($type)){
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }

    public static function getAllActiveTopic($type = null){
        $query = self::find()
            ->asArray()
            ->select(["*", "concat(name,' - ', type) as name_detail"])
            ->where(['is_active' => self::ACTIVE])
            ->orderBy('id DESC');

        if(!empty($type)){
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }


    public static function searchByName($q, $limit = 10)
    {
        $query = self::find()
            ->asArray()
            ->select('id, name as text')
            ->andWhere(['like', 'name', $q])
            ->limit($limit);

        return $query->all();
    }


}