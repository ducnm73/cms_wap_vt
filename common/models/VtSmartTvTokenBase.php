<?php

namespace common\models;

use common\libs\VtHelper;
use Yii;

class VtSmartTvTokenBase extends \common\models\db\VtSmartTvTokenDB {

    public static function checkTokenLogin($smartToken)
    {
        return self::find()
            ->asArray()
            ->where(['token' => $smartToken, 'ip' => VtHelper::getAgentIp(), 'status' => 1,])
            ->andWhere('expire_time > :expire AND (user_id > 0 OR msisdn >0)', [':expire' => date("Y-m-d H:i:s")])
            ->one();
    }

    public static function deactiveCode($token)
    {
        self::updateAll(['status' => 0], 'status=:status AND token=:token', [':status' => 1, ':token' => $token]);
    }

    public static function insertNewCode($token)
    {

        $hourExpire = Yii::$app->params['expire.time.token.smartTV'];
        $code = VtHelper::generateCode(8);

        $objSmartTV = new VtSmartTvTokenBase();
        $objSmartTV->token = $token;
        $objSmartTV->ip = VtHelper::getAgentIp();
        $objSmartTV->status = 1;
        $objSmartTV->expire_time = date("Y-m-d H:i:s", strtotime("+" . $hourExpire . " hours"));
        $objSmartTV->code = $code;
        $objSmartTV->msisdn = 0;
        $objSmartTV->user_id = 0;

        $objSmartTV->save(false);

        return $code;
    }

    public static function checkCode($code)
    {
        return self::find()
            ->asArray()
            ->where(['code' => $code, 'status' => 1, 'user_id' => 0])
            ->andWhere('expire_time > :expire', [':expire' => date("Y-m-d H:i:s")])
            ->one();

    }

    public static function generateCode($length){

        $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}