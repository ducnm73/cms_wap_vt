<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class VtUserDrmBase extends \common\models\db\VtUserDrmDB
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Lay ra toan bo ban ghi dua theo so dien thoai va userId
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @param $userId
     * @return \yii\db\ActiveRecord
     */
    public static function getByMsisdnAndUserId($msisdn, $userId)
    {
        $query = self::find();

        if (!empty($msisdn)) {
            $query->andWhere(['msisdn' => $msisdn]);
        }

        if (!empty($userId)) {
            $query->andWhere(['user_id' => $userId]);
        }

        return $query->all();
    }

    /**
     * Insert ban ghi
     * @param $msisdn
     * @param $userId
     * @param $deviceId
     * @param $networkDeviceId
     * @param $entitlementId
     * @param $deviceType
     */
    public function insertUserDrm($msisdn, $userId, $deviceId, $networkDeviceId, $entitlementId, $deviceType, $smsPackageId)
    {
        $this->msisdn = $msisdn;
        $this->user_id = $userId;
        $this->device_id = $deviceId;
        $this->network_device_id = $networkDeviceId;
        $this->entitlement_id = $entitlementId;
        $this->device_type = $deviceType;
        $this->sms_package_id = $smsPackageId;
        $this->save();
    }


}