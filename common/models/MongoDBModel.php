<?php
/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 4/23/2018
 * Time: 9:41 AM
 */

namespace common\models;


use MongoDB\BSON\UTCDateTime;
use yii\mongodb\Query;

class MongoDBModel
{
    const LIKE = "LIKE";
    const DISLIKE = "DISLIKE";
    const SHARE_FACEBOOK = "SHARE_FACEBOOK";
    const COMMENT = "COMMENT";
    const UPLOAD = "UPLOAD";
    const FOLLOW = "FOLLOW";
    const LIKE_SHARE_FANPAGE = "LIKE_SHARE_FANPAGE";

    public static function insertEvent($action, $contentId, $userId, $msisdn, $otherInfo = "", $ownerBy=0)
    {
        //Neu user co so thue bao thi moi insert event log
        if (!$userId || !$msisdn) {
            return ["error" => "No have msisdn"];
        }

        if( in_array( $action, [self::SHARE_FACEBOOK, self::LIKE, self::COMMENT, self::DISLIKE, self::LIKE_SHARE_FANPAGE] ) ){
            $objSub = VtSubBase::getSub($msisdn);
            if(!$objSub){
                \Yii::error("[EVENT] MSISDN:".$msisdn." NOT A SUB. NOT ADD:".$action);
                return ["error" => "No have sub"];
            }
        }

        try {
            $collection = \Yii::$app->mongodb->getCollection('event_log');

            $objToken = $collection->insert(
                [
                    'created_at' => date("Y-m-d H:i:s"),
                    'created_at_m' => new UTCDateTime(),
                    'action' => $action,
                    'content' => intval($contentId),
                    'user_id' =>  intval($userId),
                    'msisdn' => intval($msisdn),
                    'user_id_str' =>  strval($userId),
                    'msisdn_str' => strval($msisdn),
                    'owner_by'=>intval($ownerBy),
                    'other_info' => $otherInfo,
                    'trace_info' => 0
                ]
            );

            return [
                'token' => $objToken->__toString(),
            ];
        } catch (\Exception $exception) {
            \Yii::error("MongoDB:" . $exception->getMessage());

            return [
                'error' => $exception->getMessage()
            ];
        }


    }

    public static function getReportByUserId($userId, $msisdn)
    {
        $query = new Query();
        // compose the query
        $query->from('event_report')
            ->where(["user_id" => intval($userId)])
            ->orWhere(["msisdn"=>intval($msisdn)]);

        return $query->all();

    }

    public static function getAwardReport($fromDate, $toDate, $awardTypes, $limit = 10, $offset = 0)
    {

        $query = new Query();
        // compose the query
        $query->from('award_report')
            ->where(['from_date' => ['$gte' => $fromDate]])
            ->andWhere(['to_date' => ['$lte' => $toDate]])
            ->andWhere(['award_type' => ['$in' => $awardTypes]]);

        //--

        return $query->all();
    }

}