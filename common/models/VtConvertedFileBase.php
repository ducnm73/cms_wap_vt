<?php

namespace common\models;

use Yii;

class VtConvertedFileBase extends \common\models\db\VtConvertedFileDB
{

    public static function getConvertFile($video, $profileId)
    {
        $result = self::find()
            ->where(['video_id' => $video['id'], 'profile_id' => $profileId])
            ->one();

        if(!$result){
            $result = self::find()
                ->where(['video_id' => $video['id'], 'profile_id' => 3])
                ->one();
			
            if(!$result){
                $result = self::find()
                    ->where(['video_id' => $video['id'], 'profile_id' => 2])
                    ->one();
					
            }
        }
        return $result;
    }
    public static function getProfiles($videoId, $profileId = '')
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, p.name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_profile p', 'p.id = v.profile_id')
            ->where(['v.video_id' => $videoId]);
           
        if (!empty($profileId)) {
            $query = $query->andWhere(['v.profile_id' => $profileId])
                    ->one();
        }else{
           $query= $query->all();

        }
        return $query;
    }



}