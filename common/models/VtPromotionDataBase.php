<?php

namespace common\models;

use Yii;

class VtPromotionDataBase extends \common\models\db\VtPromotionDataDB {

    public static function checkPromotion($msisdn){

        return self::find()
            ->asArray()
            ->where('msisdn = :msisdn AND expired_at > :dateNow', ['msisdn'=>$msisdn, 'dateNow'=> date("Y-m-d H:i:s")])
            ->limit(1)
            ->one();
    }
}