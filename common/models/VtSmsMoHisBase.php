<?php

namespace common\models;

use Yii;

class VtSmsMoHisBase extends \common\models\db\VtSmsMoHisDB {

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Số điện thoại'),
            'command' => Yii::t('app', 'Cú pháp'),
            'param' => Yii::t('app', 'Tham số'),
            'err_code' => Yii::t('app', 'Mã lỗi'),
            'process_time' => Yii::t('app', 'Thời gian thực thi'),
            'channel' => Yii::t('app', 'Đầu số'),
            'action_id' => Yii::t('app', 'Action ID'),
            'receive_time' => Yii::t('app', 'Thời gian nhận'),
            'action_type' => Yii::t('app', 'Loại'),
            'ip' => Yii::t('app', 'Ip'),
        ];
    }
}