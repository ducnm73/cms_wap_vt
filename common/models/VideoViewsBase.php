<?php

namespace common\models;

use Yii;

class VideoViewsBase extends \common\models\db\VideoViewsDB {
    public static function getNewViewByCategory($fromTime, $toTime, $categoryId)
    {
        $query = self::find()->asArray()
            ->select('t3.name, t1.category_id AS id, COUNT(*) as count')
            ->from(VtVideoBase::tableName() . ' t1')
            ->leftJoin(self::tableName() . ' t2', 't1.id = t2.video_id')
            ->leftJoin(VtGroupCategoryBase::tableName() . ' t3', 't1.category_id = t3.id')
            ->where(['BETWEEN', 't2.date', $fromTime, $toTime])
            ->andWhere(['t1.category_id' => $categoryId])
            ->groupBy('t1.category_id');
        return $query->all();
    }

    public static function getAccumViewByCategory($toTime, $categoryId)
    {
        $query = self::find()->asArray()
            ->select('t3.name, t1.category_id AS id, COUNT(*) as count')
            ->from(VtVideoBase::tableName() . ' t1')
            ->leftJoin(self::tableName() . ' t2', 't1.id = t2.video_id')
            ->leftJoin(VtGroupCategoryBase::tableName() . ' t3', 't1.category_id = t3.id')
            ->where(['<=', 't2.date', $toTime])
            ->andWhere(['t1.category_id' => $categoryId])
            ->groupBy('t1.category_id');
        return $query->all();
    }

    public static function getNewViewByVideo($fromTime, $toTime, $videoId)
    {
        $query = self::find()->asArray()
            ->select('t1.name, t1.id, COUNT(*) as count')
            ->from(VtVideoBase::tableName() . ' t1')
            ->leftJoin(self::tableName() . ' t2', 't1.id = t2.video_id')
            ->where(['BETWEEN', 't2.date', $fromTime, $toTime])
            ->andWhere(['t1.id' => $videoId])
            ->groupBy('t1.id');
        return $query->all();
    }

    public static function getAccumViewByVideo($toTime, $videoId)
    {
        $query = self::find()->asArray()
            ->select('t1.name, t1.id, COUNT(*) as count')
            ->from(VtVideoBase::tableName() . ' t1')
            ->leftJoin(self::tableName() . ' t2', 't1.id = t2.video_id')
            ->where(['<=', 't2.date', $toTime])
            ->andWhere(['t1.id' => $videoId])
            ->groupBy('t1.id');
        return $query->all();
    }
}