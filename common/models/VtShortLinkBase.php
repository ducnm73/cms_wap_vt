<?php

namespace common\models;

use common\helpers\MobiTVRedisCache;
use Yii;

class VtShortLinkBase extends \common\models\db\VtShortLinkDB
{

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    public static function getRoutes()
    {
        $strRoutes = Yii::$app->cache->get("ROUTES_SHORT_LINK");
        if (!$strRoutes) {
            $arrRoutes = self::find()->asArray()
                ->where(["is_active" => self::STATUS_ACTIVE])
                ->all();
            $strRoutes = json_encode($arrRoutes);
            Yii::$app->cache->set("ROUTES_SHORT_LINK", $strRoutes, MobiTVRedisCache::CACHE_30MINUTE);
        }

        return json_decode($strRoutes);

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'short_link' => Yii::t('app', 'Link ngắn'),
            'full_link' => Yii::t('app', 'Link chuyển hướng'),
            'params' => Yii::t('app', 'Tham số'),
            'route' => Yii::t('app', 'Loại link'),
            'is_active' => Yii::t('app', 'Kích hoạt'),
            'other_info' => Yii::t('app', 'Thông tin khác'),
        ];
    }
}