<?php

namespace common\models;

use Yii;

class VtSmsMtBase extends \common\models\db\VtSmsMtDB {

    /**
     * Gui tin nhan text
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @param $content
     */
    public function sendSms($msisdn, $content)
    {
        $this->mo_his_id = 0;
        $this->msisdn = $msisdn;
        $this->message = $content;
        $this->receive_time = date('Y-m-d H:i:s');
        $this->channel = VtConfigBase::getConfig('SHORTCODE');
        $this->save(false);
    }


}