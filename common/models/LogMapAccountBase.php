<?php

namespace common\models;

use Yii;

class LogMapAccountBase extends \common\models\db\LogMapAccountDB {

    public static function insertLog($msisdnSource, $userIdSource, $msisdnTarget, $userIdTarget, $description = ''){

        $objLog = new LogMapAccountBase();

        $objLog->msisdn_source = $msisdnSource;
        $objLog->msisdn_target = $msisdnTarget;
        $objLog->user_id_source = $userIdSource;
        $objLog->user_id_target = $userIdTarget;

        $objLog->created_at = date("Y-m-d H:i:s");

        $objLog->description = $description;
        
        $objLog->save(false);

    }
}