<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtUserPlaylistItemBase extends \common\models\db\VtUserPlaylistItemDB {

    const STATUS_ADD = 1;
    const STATUS_REMOVE = 0;

    public function getVideo() {
        return $this->hasOne(VtVideoBase::className(), ['id' => 'item_id']);
    }

    public function insertItem($itemId, $playlistId) {
        $this->item_id = $itemId;
        $this->playlist_id = $playlistId;
        $this->save();
    }

    public static function getItemsByPlaylistIdQuery($playlistId) {
        $query = self::find()
                ->asArray()
                ->select('v.*, pi.alias, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
                ->from('vt_user_playlist_item pi')
                ->leftJoin('vt_video v', 'v.id = pi.item_id')
                ->leftJoin('vt_user u', 'u.id=v.created_by')
                ->where([
                    'playlist_id' => $playlistId,
                    'v.is_active' => VtVideoBase::ACTIVE,
                    'v.status' => VtVideoBase::STATUS_APPROVE,
                    'v.is_no_copyright' => 0,
                ])
                ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
//            ->orderBy('pi.alias, LENGTH(v.name), v.name')
                ->orderBy('v.published_time DESC');

        return $query;
    }

    /**
     * @author Lay danh sach items theo Playlist
     * @param $playlistId
     */
    public static function getItemsByPlaylistId($playlistId) {
        $query = self::getItemsByPlaylistIdQuery($playlistId);

        return $query->all();
    }

    /**
     * @author Lay danh sach items theo Playlist
     * @param $playlistId
     */
    public static function getAllItemsByPlaylistId($playlistId, $isObj = false) {
        $query = self::find()
                ->joinWith('video')
                ->from('vt_playlist_item pi')
                ->where([
                    'playlist_id' => $playlistId
                ])
                ->orderBy('pi.alias');
     
        return $query->all();
    }

    public static function getOneByItemId($itemId) {
        $query = self::find()
                ->asArray()
                ->from('vt_user_playlist_item pi')
                ->where(['pi.item_id' => $itemId]);

        return $query->one();
    }

    public static function getByPlayListAndItemId($playlistId, $itemId) {
        $query = self::find()
                ->from('vt_user_playlist_item pi')
                ->where([
            'pi.playlist_id' => $playlistId,
            'pi.item_id' => $itemId
        ]);

        return $query->one();
    }

    /**
     * @author Lay danh sach playlist theo userId
     * @param $playlistId
     */
    public static function getPlaylistByUserQuery($userId, $limit = null, $offset = null) {
        $query = self::find()
                ->asArray()
                ->select('*')
                ->from('vt_user_playlist p')
//            ->leftJoin('vt_user_playlist_item pi', 'p.id = pi.playlist_id')
//            ->leftJoin('vt_video v', 'v.id = pi.item_id')
                ->where([
            'p.user_id' => $userId,
            'p.is_active' => 1,
            'p.status' => VtVideoBase::STATUS_APPROVE
                ]
        );
        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    public static function countItemOfPlayList($playlistId) {
        $query = self::find()
                ->from('vt_user_playlist_item pi')
                ->where([
            'pi.playlist_id' => $playlistId
        ]);

        return $query->count();
    }

    public static function getByVideoId($playlistId, $videoId) {

        $query = self::find()
                ->asArray()
                ->from('vt_user_playlist_item pi')
                ->where([
            'pi.playlist_id' => $playlistId,
            'pi.item_id' => $videoId
        ]);

        return $query->all();
    }

    public static function updatePos($playlistId, $contentId, $pos) {

        $rawQuery = "UPDATE vt_user_playlist_item SET position = :pos  WHERE item_id = :item_id and playlist_id =:playlist_id";


        return Yii::$app->db->createCommand($rawQuery)
                        ->bindValue(':item_id', $contentId)
                        ->bindValue(':playlist_id', $playlistId)
                        ->bindValue(':pos', $pos)
                        ->execute();
    }

    public static function deletePlaylist($playlistId, $contentId) {
        $rawQuery = "delete from vt_user_playlist_item WHERE item_id = :item_id and playlist_id =:playlist_id";
        return Yii::$app->db->createCommand($rawQuery)
                        ->bindValue(':item_id', $contentId)
                        ->bindValue(':playlist_id', $playlistId)
                        ->execute();
    }

}
