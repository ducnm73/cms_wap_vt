<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtVideoRecommendBase extends \common\models\db\VtVideoRecommendDB
{

    public static function getByActiveQuery($limit = 10, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.video_id')
            ->from(self::tableName() . ' v')
            ->leftJoin("vt_video v2", 'v.video_id = v2.id')
            ->where([
                'v.is_active' => VtVideoBase::ACTIVE,
                'v2.status' => 2,
                'v2.is_active' => 1,
                'v2.is_no_copyright' => 0,
                'v2.type' => 'VOD'
            ])
            ->andWhere('v2.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.created_at DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }
}