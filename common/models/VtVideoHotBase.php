<?php

namespace common\models;

use common\helpers\Utils;
use Yii;

class VtVideoHotBase extends \common\models\db\VtVideoHotDB {

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    public static function getByCategoryIds($categoryIds, $limit = 10, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.video_id')
            ->from(self::tableName() . ' v')
            ->where([
                'v.category_id' => $categoryIds,
                'v.is_active' => self::ACTIVE
            ])
            ->orderBy('v.created_at DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query->all();
    }

    public static function getByCategoryIdsAndActive($categoryIds, $limit = 10, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.video_id')
            ->from(self::tableName() . ' v')
            ->leftJoin("vt_video v2",'v.video_id=v2.id' )
            ->where([
                'v.category_id' => $categoryIds,
                'v.is_active' => self::ACTIVE,
                'v2.status'=>2,
                'v2.is_active'=>1,
                'v2.is_no_copyright'=>0,
                'v2.type'=>'VOD'
            ])
            ->andWhere('v2.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.created_at DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query->all();
    }
}