<?php

namespace common\models;

use Yii;

class VtMappingCrawlerBase extends \common\models\db\VtMappingCrawlerDB
{

    public function getUser()
    {
        return $this->hasOne(VtUserBase::className(), ['id' => 'local_user_id']);
    }

    public function getCp()
    {
        return $this->hasOne(VtCpBase::className(), ['id' => 'cp_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(VtGroupCategoryBase::className(), ['id' => 'local_category_id']);
    }

    public function getCategoryName()
    {
        $objCategory = VtGroupCategoryBase::findOne($this->local_category_id);
        return ($objCategory) ? ($objCategory->id . '-' . $objCategory->name) : $this->local_category_id;
    }


    public function getWapUserName()
    {
        $objUsername = VtUserBase::getById($this->local_user_id);
        return ($objUsername) ? ($objUsername->id . '-' . $objUsername->full_name) : $this->local_user_id;
    }

}