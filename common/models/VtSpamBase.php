<?php

namespace common\models;

use Yii;

class VtSpamBase extends \common\models\db\VtSpamDB {

    const STATUS_DRAFT = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_FINISH = 2;

    const CHANNEL_ALL = 1;
    const CHANNEL_APP = 2;
    const CHANNEL_SMS = 3;

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Tên chương trình'),
            'content' => Yii::t('app', 'Nội dung chương trình'),
            'is_active' => Yii::t('app', 'Kích hoạt'),
            'is_sent' => Yii::t('app', 'Đã gửi'),
            'send_time' => Yii::t('app', 'Thời gian gửi'),
            'path' => Yii::t('app', 'Đường dẫn'),
            'number' => Yii::t('app', 'Số lượng thuê bao'),
            'current_line' => Yii::t('app', 'Current Line'),
            'rule' => Yii::t('app', 'Rule'),
            'created_at' => Yii::t('app', 'Thời gian tạo'),
            'updated_at' => Yii::t('app', 'Thời gian cập nhật'),
            'created_by' => Yii::t('app', 'Tạo bởi'),
            'updated_by' => Yii::t('app', 'Cập nhật bởi'),
            'item_type' => Yii::t('app', 'Loại nội dung'),
            'item_id' => Yii::t('app', 'ID nội dung'),
            'item_group_id' => Yii::t('app', 'ID nhóm nội dung'),
            'status' => Yii::t('app', 'Trạng thái'),
            'channel' => Yii::t('app', 'Kênh truyền thông'),
        ];
    }

    public function getStatusString()
    {
        switch ($this->status) {
            case 0:
                $status = "Chưa gửi";
                break;
            case 1:
                $status = "Đang gửi";
                break;
            case 2:
                $status = "Đã gửi";
                break;
        }

        return $status;
    }


}