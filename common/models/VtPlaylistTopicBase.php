<?php

namespace common\models;

use Yii;

class VtPlaylistTopicBase extends \common\models\db\VtPlaylistTopicDB {

    /**
     * @author PhuMX
     * Lay danh sach playlist thuoc chu de
     * @param $topicId
     * @param bool $limit
     * @param bool $offset
     * @return $this
     */
    public static function getPlaylistsTopic($topicId, $limit = false, $offset = false)
    {
        $query = self::find()
            ->asArray()
            ->select('p.*')
            ->from('vt_group_topic t')
            ->leftJoin('vt_playlist_topic vt', 't.id=vt.topic_id AND vt.topic_id=:topicId', [':topicId' => $topicId])
            ->leftJoin('vt_playlist p', 'p.id = vt.playlist_id')
            ->where([
                't.id' => $topicId,
                't.is_active' => VtGroupTopicBase::ACTIVE,
                'p.status' => VtPlaylistBase::STATUS_APPROVE,
                'p.is_active' => VtPlaylistBase::ACTIVE])
            ->orderBy('vt.id DESC');
        if($limit){
            $query->limit($limit);
        }
        if($offset){
            $query->offset($offset);
        }

        return $query;
    }
}