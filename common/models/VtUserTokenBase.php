<?php

namespace common\models;

use Yii;

class VtUserTokenBase extends \common\models\db\VtUserTokenDB
{

    /**
     * Lay ra ban ghi dua theo so dien thoai
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByMsisdn($msisdn)
    {
        $query = self::find()
            ->where(['msisdn' => $msisdn]);

        return $query->one();
    }


    /**
     * Lay ra ban ghi dua theo user_id
     * @author ducda2@viettel.com.vn
     * @param $userId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByUserId($userId)
    {
        $query = self::find()
            ->where(['user_id' => $userId]);

        return $query->one();
    }




    /**
     * Luu thong tin user theo token
     * @param $refreshToken
     * @param $ip
     * @param $expiredTime
     * @param $loginType
     * @param string $userAgent
     * @param null $imei
     */
    public function insertUserToken($refreshToken, $ip, $userId, $msisdn, $expiredTime, $loginType, $userAgent = '', $imei = null)
    {
        $this->token = $refreshToken;
        $this->user_id = $userId;
        $this->msisdn = $msisdn;
        $this->ip = $ip;
        $this->token_expired_time = $expiredTime;
        $this->last_login_type = $loginType;
        $this->user_agent = $userAgent;
        $this->imei = $imei;
        $this->save(false);
    }


    /**
     * Lay ra ban ghi dua theo token
     * @author ducda2@viettel.com.vn
     * @param $token
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByToken($token)
    {
        $query = self::find()
            ->where('token = :token', [':token' => $token]);

        return $query->one();
    }
}