<?php

namespace common\models;

use Yii;

class VtChannelInfoBase extends \common\models\db\VtChannelInfoDB {
    const WAIT_APPROVE = 0;
    const APPROVED = 1;
    const DECLINED = 2;
    const ACTIVE = 1;
    const HOT = 1;

    public function getStatusString() {
        switch ($this->status) {
            case self::WAIT_APPROVE:
                return Yii::t('backend', 'Chờ duyệt');
            case self::APPROVED:
                return Yii::t('backend', 'Đã duyệt');
            case self::DECLINED:
                return Yii::t('backend', 'Không duyệt');
        }
    }

    // check xem channel có phai la kenh cua user hay không
    public static function CheckChannelUser($id, $channelID) {
        $query = self::find()
            ->where([
                'id' => $id,
                'channel_id' => $channelID,
            ]);

        return $query->one();
    }

    public static function getChannelByChannelID($id) {
        $query = self::find()
            ->where([
                'channel_id' => $id,
            ]);
        return $query->one();
    }

    public static function getChannelById($id) {
        $query = self::find()
            ->where([
                'id' => $id,
            ]);
        return $query->one();
    }

    public function insertChannelInfo($full_name, $description, $fileBannerPath, $fileAvatarPath, $channel_id, $imageBucket='',$status = self::WAIT_APPROVE)
    {

        $this->full_name = $full_name;
        $this->description = $description;
        $this->channel_path = $fileBannerPath;
        $this->path = $fileAvatarPath;
        $this->channel_id = $channel_id;
        $this->bucket = $imageBucket;
        $this->channel_bucket = $imageBucket;
        $this->status = $status;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->save(false);
    }


}