<?php

namespace common\models;

use Yii;

class PackageViewsBase extends \common\models\db\PackageViewsDB {
    public static function getNewViewByPackage($fromTime, $toTime, $packageID = null)
    {
        $query = self::find()->asArray()
            ->select('sub_service_name, count(*) as count')
            ->from(VtPackageBase::tableName() . ' t1')
            ->leftJoin(self::tableName() . ' t2', 't1.id = t2.package_id')
            ->where(['between', 't2.date', $fromTime, $toTime])
            ->groupBy('t1.sub_service_name');
        
        if ($packageID) {
            $query->andWhere(['t2.package_id' => $packageID]);
        }

        return $query->all();

    }

    public static function getAccumViewByPackage($time, $packageID = null)
    {
        $query = self::find()->asArray()
            ->select('sub_service_name, count(*) as count')
            ->from(VtPackageBase::tableName() . ' t1')
            ->leftJoin(self::tableName() . ' t2', 't1.id = t2.package_id')
            ->where(['<', 't2.date', $time])
            ->groupBy('t1.sub_service_name');

        if ($packageID) {
            $query->where(['t2.package_id' => $packageID]);
        }

        return $query->all();
    }
}