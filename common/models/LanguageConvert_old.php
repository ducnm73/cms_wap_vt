<?php
/**
 * Created by PhpStorm.
 * User: anhntm23
 * Date: 11/1/2018
 * Time: 10:15 AM
 */

namespace common\models;
use Yii;


class LanguageConvert
{
    public static function getLang( $field,$multilan,$default) {

        $lang = Yii::$app->language;//
        $key = $field . '_' . $lang;
        if(isset($multilan->$key))
        {
            return $multilan->$key;
        }
        else{
            return "";
        }

    }
}