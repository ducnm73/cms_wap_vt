<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "target_kpi".
 *
 * @property string $id
 * @property integer $report_year
 * @property integer $report_month
 * @property string $display_time_app
 * @property string $display_time_web
 * @property double $success_access_rate_app
 * @property double $success_access_rate_web
 * @property double $avg_upload_speed
 * @property double $error_upload_rate
 * @property string $encode_video_time
 * @property double $error_encode_rate
 * @property string $wait_time
 * @property double $buffer_rate
 * @property double $buffer_duration_rate
 * @property double $buffer_over_rate
 */
class TargetKpiDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'target_kpi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_year', 'report_month', 'display_time_app', 'display_time_web', 'success_access_rate_app', 'success_access_rate_web',
                'avg_upload_speed', 'error_upload_rate', 'encode_video_time', 'error_encode_rate', 'wait_time', 'buffer_rate',
                'buffer_duration_rate', 'buffer_over_rate'], 'required'],
            [['report_year', 'report_month', 'display_time_app', 'display_time_web', 'encode_video_time', 'wait_time'], 'integer'],
            [['success_access_rate_app', 'success_access_rate_web', 'avg_upload_speed', 'error_upload_rate', 'error_encode_rate', 'buffer_rate', 'buffer_duration_rate', 'buffer_over_rate'], 'number'],
            [['report_year', 'report_month'], 'unique', 'targetAttribute' => ['report_year', 'report_month'], 'message' => 'Đã tồn tại']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_year' => 'Năm báo cáo (*)',
            'report_month' => 'Tháng báo cáo (*)',
            'display_time_app' => 'Thời gian hiển thị trang trên app (giây) (*)',
            'display_time_web' => 'Thời gian hiển thị trang trên web (giây) (*)',
            'success_access_rate_app' => 'Tỉ lệ truy cập thành công trên app (%) (*)',
            'success_access_rate_web' => 'Tỉ lệ truy cập thành công trên web (%) (*)',
            'avg_upload_speed' => 'Tốc độ upload trung bình (Kbps) (*)',
            'error_upload_rate' => 'Tỷ lệ upload lỗi (%) (*)',
            'encode_video_time' => 'Thời gian encode video (giây) (*)',
            'error_encode_rate' => 'Tỷ lệ encode lỗi (%) (*)',
            'wait_time' => 'Thời gian chờ để bắt đầu xem được video (giây) (*)',
            'buffer_rate' => 'Tỷ lệ số lần bị buffer khi đang xem video (%) (*)',
            'buffer_duration_rate' => 'Tỷ lệ thời gian bị buffer khi đang xem video (%) (*)',
            'buffer_over_rate' => 'Tỷ lệ số lần bị buffer vượt quá 3s (%) (*)',
        ];
    }
}
