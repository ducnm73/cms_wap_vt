<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_upload_feedback".
 *
 * @property string $id
 * @property string $video_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $feedback_content
 * @property string $reject_reason
 * @property string $bucket
 * @property string $path
 */
class VtUploadFeedbackDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_upload_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['updated_at'], 'safe'],
            [['reject_reason'], 'string'],
            [['created_at', 'bucket', 'path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_id' => 'Video ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'reject_reason' => 'Reject Reason',
            'bucket' => 'Bucket',
            'path' => 'Path',
        ];
    }
}
