<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_channel_info".
 *
 * @property integer $id
 * @property integer $channel_id
 * @property integer $status
 * @property string $full_name
 * @property string $bucket
 * @property string $path
 * @property string $channel_bucket
 * @property string $channel_path
 * @property string $description
 * @property string $reason
 * @property string $created_at
 * @property string $updated_at
 */
class VtChannelInfoDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_channel_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name', 'bucket', 'path', 'channel_bucket', 'channel_path', 'reason'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_id' => 'Channel ID',
            'status' => 'Status',
            'full_name' => 'Full Name',
            'bucket' => 'Bucket',
            'path' => 'Path',
            'channel_bucket' => 'Channel Bucket',
            'channel_path' => 'Channel Path',
            'description' => 'Description',
            'reason' => 'Lý do từ chối',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
