<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_token".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $user_id
 * @property string $ip
 * @property string $token
 * @property string $token_expired_time
 * @property string $user_agent
 * @property string $imei
 * @property string $last_login
 * @property integer $last_login_type
 */
class VtUserTokenDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'user_id', 'last_login_type'], 'integer'],
            [['token', 'last_login_type'], 'required'],
            [['token_expired_time', 'last_login'], 'safe'],
            [['ip'], 'string', 'max' => 20],
            [['token'], 'string', 'max' => 50],
            [['user_agent'], 'string', 'max' => 500],
            [['imei'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'user_id' => Yii::t('app', 'User ID'),
            'ip' => Yii::t('app', 'Ip'),
            'token' => Yii::t('app', 'Token'),
            'token_expired_time' => Yii::t('app', 'Token Expired Time'),
            'user_agent' => Yii::t('app', 'User Agent'),
            'imei' => Yii::t('app', 'Imei'),
            'last_login' => Yii::t('app', 'Last Login'),
            'last_login_type' => Yii::t('app', 'Last Login Type'),
        ];
    }
}
