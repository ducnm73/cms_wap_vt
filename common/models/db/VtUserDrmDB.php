<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_drm".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $user_id
 * @property string $device_id
 * @property string $network_device_id
 * @property string $entitlement_id
 * @property string $updated_at
 * @property string $created_at
 * @property string $device_type
 * @property string $sms_package_id
 */
class VtUserDrmDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_drm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'user_id'], 'integer'],
            [['device_id', 'network_device_id', 'entitlement_id', 'device_type'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            [['device_id', 'network_device_id', 'entitlement_id', 'sms_package_id'], 'string', 'max' => 255],
            [['device_type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'user_id' => Yii::t('app', 'User ID'),
            'device_id' => Yii::t('app', 'Device ID'),
            'network_device_id' => Yii::t('app', 'Network Device ID'),
            'entitlement_id' => Yii::t('app', 'Entitlement ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'device_type' => Yii::t('app', 'Device Type'),
            'sms_package_id' => Yii::t('app', 'Sms Package ID'),
        ];
    }
}
