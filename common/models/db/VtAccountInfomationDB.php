<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_account_infomation".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $status
 * @property string $id_card_number
 * @property string $id_card_created_at
 * @property string $id_card_created_by
 * @property string $created_at
 * @property integer $created_by
 * @property integer $user_id
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $is_confirm
 * @property string $id_card_image_frontside
 * @property string $id_card_image_backside
 * @property string $bucket
 * @property string $msisdn
 * @property string $reason
 */
class VtAccountInfomationDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_account_infomation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'id_card_number', 'id_card_created_at', 'id_card_created_by'], 'required'],
            [['status', 'created_by', 'user_id', 'updated_by', 'is_confirm'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'email', 'id_card_image_frontside', 'id_card_image_backside', 'bucket'], 'string', 'max' => 255],
            [['id_card_number', 'id_card_created_at', 'id_card_created_by'], 'string', 'max' => 30],
            [['msisdn'], 'string', 'max' => 20],
            [['reason'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend','Họ và tên'),
            'email' => Yii::t('backend','Email'),
            'status' => Yii::t('backend','Trạng thái'),
            'id_card_number' => Yii::t('backend','Số CMND'),
            'id_card_created_at' => Yii::t('backend','Ngày cấp CMND'),
            'id_card_created_by' => Yii::t('backend','Nơi cấp CMND'),
            'created_at' => Yii::t('backend','Ngày tạo'),
            'created_by' => Yii::t('backend','Người tạo'),
            'user_id' => Yii::t('backend','ID kênh'),
            'updated_at' => Yii::t('backend','Ngày cập nhật'),
            'updated_by' => Yii::t('backend','Người cập nhật'),
            'is_confirm' => Yii::t('backend','Xác nhận'),
            'id_card_image_frontside' => Yii::t('backend','Ảnh mặt trước CMND'),
            'id_card_image_backside' => Yii::t('backend','Ảnh mặt sau CMND'),
            'bucket' => Yii::t('backend','Bucket'),
            'msisdn' => Yii::t('backend','SĐT xác nhận'),
            'reason' => Yii::t('backend','Lý do không duyệt'),
        ];
    }
}
