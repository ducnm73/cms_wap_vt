<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_comment_bad_word".
 *
 * @property string $id
 * @property string $content
 */
class VtCommentBadWordDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_comment_bad_word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Content'),
        ];
    }
}
