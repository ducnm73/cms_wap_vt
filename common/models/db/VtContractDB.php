<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_contract".
 *
 * @property integer $id
 * @property string $contract_code
 * @property string $name
 * @property string $email
 * @property string $id_card_number
 * @property string $id_card_created_at
 * @property string $id_card_created_by
 * @property string $bucket
 * @property string $id_card_image_frontside
 * @property string $id_card_image_backside
 * @property string $address
 * @property string $msisdn
 * @property string $payment_type
 * @property string $tax_code
 * @property string $created_at
 * @property integer $created_by
 * @property string $account_number
 * @property string $bank_name
 * @property string $bank_department
 * @property integer $user_id
 * @property string $msisdn_pay
 * @property integer $status
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $reason
 * @property string $sync_id
 * @property string $approved_at
 */
class VtContractDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_code', 'name', 'email', 'id_card_number', 'id_card_created_at', 'id_card_created_by', 'id_card_image_frontside', 'id_card_image_backside', 'address', 'msisdn', 'payment_type', 'tax_code'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'user_id', 'status', 'updated_by'], 'integer'],
            [['contract_code', 'id_card_number', 'id_card_created_at', 'id_card_created_by', 'address', 'msisdn', 'payment_type', 'tax_code', 'account_number', 'msisdn_pay'], 'string', 'max' => 30],
            [['name', 'email', 'bucket', 'id_card_image_frontside', 'id_card_image_backside', 'bank_name', 'bank_department'], 'string', 'max' => 255],
            [['contract_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_code' => Yii::t('backend','Mã hợp đồng'),
            'name' => Yii::t('backend','Tên'),
            'email' => Yii::t('backend','Email'),
            'id_card_number' => Yii::t('backend','Số CMND'),
            'id_card_created_at' => Yii::t('backend','Ngày cấp CMND'),
            'id_card_created_by' => Yii::t('backend','Nơi cấp CMND'),
            'bucket' => 'Bucket',
            'id_card_image_frontside' => Yii::t('backend','Id Card Image Frontside'),
            'id_card_image_backside' => Yii::t('backend','Id Card Image Backside'),
            'address' => Yii::t('backend','Địa chỉ thường trú'),
            'msisdn' => Yii::t('backend','Số thuê bao liên hệ'),
            'payment_type' => Yii::t('backend','Hình thức thanh toán'),
            'tax_code' => Yii::t('backend','Mã số thuế'),
            'created_at' => Yii::t('backend','Thời điểm tạo'),
            'created_by' => Yii::t('backend','Tạo bởi'),
            'account_number' => Yii::t('backend','Mã tài khoản Ngân Hàng'),
            'bank_name' => Yii::t('backend','Tên Ngân Hàng'),
            'bank_department' => Yii::t('backend','Chi nhánh'),
            'user_id' => Yii::t('backend','User ID'),
            'msisdn_pay' => Yii::t('backend','Số thuê bao nhận thanh toán'),
            'status' => Yii::t('backend','Trạng thái'),
            'updated_at' => Yii::t('backend','Cập nhật lúc'),
            'updated_by' => Yii::t('backend','Cập nhật bởi'),
            'reason' => Yii::t('backend','Lý do từ chối'),
        ];
    }
}
                               


