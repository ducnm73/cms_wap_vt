<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_otp".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $otp
 * @property integer $status
 * @property string $expired_time
 * @property string $created_at
 * @property string $ip
 */
class VtUserOtpDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_otp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'status'], 'integer'],
            [['expired_time', 'created_at'], 'safe'],
            [['otp'], 'string', 'max' => 20],
            [['ip'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'otp' => Yii::t('app', 'Otp'),
            'status' => Yii::t('app', 'Status'),
            'expired_time' => Yii::t('app', 'Expired Time'),
            'created_at' => Yii::t('app', 'Created At'),
            'ip' => Yii::t('app', 'Ip'),
        ];
    }
}
