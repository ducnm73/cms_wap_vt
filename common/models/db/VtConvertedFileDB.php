<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_converted_file".
 *
 * @property string $id
 * @property string $video_id
 * @property string $bucket
 * @property string $path
 * @property string $profile_id
 * @property string $file_size
 * @property string $encrypted_path
 * @property integer $is_migrated
 * @property integer $is_downloaded
 * @property string $file_size_real
 */
class VtConvertedFileDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_converted_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'profile_id', 'file_size', 'is_migrated', 'is_downloaded', 'file_size_real'], 'integer'],
            [['bucket', 'path'], 'string', 'max' => 255],
            [['encrypted_path'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'file_size' => Yii::t('app', 'File Size'),
            'encrypted_path' => Yii::t('app', 'Encrypted Path'),
            'is_migrated' => Yii::t('app', 'Is Migrated'),
            'is_downloaded' => Yii::t('app', 'Is Downloaded'),
            'file_size_real' => Yii::t('app', 'File Size Real'),
        ];
    }
}
