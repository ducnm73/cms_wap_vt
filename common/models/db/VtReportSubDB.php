<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_report_sub".
 *
 * @property integer $id
 * @property string $package_id
 * @property string $package_type
 * @property string $active_sub
 * @property string $report_date
 */
class VtReportSubDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_report_sub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id'], 'required'],
            [['package_id', 'active_sub'], 'integer'],
            [['report_date'], 'safe'],
            [['package_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'package_id' => Yii::t('app', 'Package ID'),
            'package_type' => Yii::t('app', 'Package Type'),
            'active_sub' => Yii::t('app', 'Active Sub'),
            'report_date' => Yii::t('app', 'Report Date'),
        ];
    }
}
