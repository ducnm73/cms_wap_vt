<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_video_topic".
 *
 * @property string $id
 * @property string $video_id
 * @property integer $topic_id
 */
class VtVideoTopicDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_video_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'topic_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_id' => 'Video ID',
            'topic_id' => 'Topic ID',
        ];
    }
}
