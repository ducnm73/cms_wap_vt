<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "auth_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $num_login_fail
 * @property string $last_time_login
 * @property integer $is_first_login
 * @property string $user_id
 * @property string $cp_id
 */
class AuthUserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at', 'num_login_fail', 'is_first_login', 'user_id', 'cp_id'], 'integer'],
            [['last_time_login'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'num_login_fail' => Yii::t('app', 'Num Login Fail'),
            'last_time_login' => Yii::t('app', 'Last Time Login'),
            'is_first_login' => Yii::t('app', 'Is First Login'),
            'user_id' => Yii::t('app', 'User ID'),
            'cp_id' => Yii::t('app', 'Cp ID'),
        ];
    }
}
