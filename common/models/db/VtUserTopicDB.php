<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_topic".
 *
 * @property string $id
 * @property string $user_id
 * @property string $msisdn
 * @property string $topic_id
 */
class VtUserTopicDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'msisdn', 'topic_id'], 'integer'],
            [['topic_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'msisdn' => 'Msisdn',
            'topic_id' => 'Topic ID',
        ];
    }
}
