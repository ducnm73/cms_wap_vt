<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_promotion_data".
 *
 * @property string $id
 * @property string $code
 * @property string $msisdn
 * @property string $created_at
 * @property string $expired_at
 * @property string $popup
 */
class VtPromotionDataDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_promotion_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'expired_at'], 'safe'],
            [['code'], 'string', 'max' => 30],
            [['msisdn'], 'string', 'max' => 20],
            [['popup'], 'string', 'max' => 255],
            [['msisdn', 'code'], 'unique', 'targetAttribute' => ['msisdn', 'code'], 'message' => 'The combination of Code and Msisdn has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => Yii::t('backend','Mã khuyến mãi'),
            'msisdn' => Yii::t('backend','Số thuê bao'),
            'created_at' => Yii::t('backend','Thời điểm import'),
            'expired_at' => Yii::t('backend','Thời điểm hết hạn'),
            'popup' => Yii::t('backend','Popup'),
        ];
    }
}
