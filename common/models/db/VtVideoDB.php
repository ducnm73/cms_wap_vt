<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_video".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string $category_id
 * @property string $attributes
 * @property string $published_time
 * @property integer $is_active
 * @property integer $is_check
 * @property string $price_download
 * @property string $price_play
 * @property string $play_times_real
 * @property string $play_times
 * @property integer $status
 * @property string $reason
 * @property string $approve_by
 * @property string $type
 * @property string $cp_id
 * @property string $cp_code
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $tag
 * @property string $related_id
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $is_no_copyright
 * @property string $like_count
 * @property string $bucket
 * @property string $path
 * @property integer $is_hot
 * @property integer $is_recommend
 * @property integer $convert_status
 * @property string $convert_start_time
 * @property string $convert_end_time
 * @property string $duration
 * @property string $file_path
 * @property string $file_bucket
 * @property string $convert_priority
 * @property string $resolution
 * @property string $snapshot_count
 * @property integer $upload_process
 * @property string $syn_id
 * @property string $name_slug
 * @property string $description_slug
 * @property string $suggest_package_id
 * @property string $review_by
 * @property string $sync_review_by
 * @property integer $mode
 * @property integer $dislike_count
 * @property string $comment_count
 * @property string $old_playlist_id
 * @property string $show_times
 * @property integer $can_download
 * @property string $view_time
 * @property string $outsource_review_at
 * @property string $outsource_review_by
 * @property integer $outsource_status
 * @property integer $outsource_need_review
 * @property string $outsource_review_duration
 * @property string $admin_review_first_times_at
 * @property string $admin_review_second_times_at
 * @property string $drm_content_id
 * @property string $multilang
 * @property string $channel_id
 */
class VtVideoDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'reason', 'related_id', 'description_slug'], 'string'],
            [['category_id','channel_id', 'attributes', 'is_active', 'is_check', 'price_download', 'price_play', 'play_times_real', 'play_times', 'status', 'approve_by', 'cp_id', 'created_by', 'updated_by', 'is_no_copyright', 'is_hot', 'is_recommend', 'convert_status', 'duration', 'convert_priority', 'snapshot_count', 'upload_process', 'syn_id', 'suggest_package_id', 'review_by', 'sync_review_by', 'mode','comment_count', 'old_playlist_id', 'can_download', 'outsource_review_by', 'outsource_status', 'outsource_need_review', 'outsource_review_duration'], 'integer'],
            [['published_time', 'created_at', 'updated_at', 'convert_start_time', 'convert_end_time', 'show_times', 'view_time', 'outsource_review_at'], 'safe'],
            [['created_at', 'updated_at', 'file_path'], 'required'],
            [['name', 'slug', 'path', 'file_path', 'file_bucket', 'name_slug', 'drm_content_id'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 10],
            [['like_count', 'dislike_count','play_times'], 'number'],
            [['cp_code', 'bucket'], 'string', 'max' => 50],
            [['tag', 'seo_description', 'seo_keywords'], 'string', 'max' => 1000],
            [['seo_title'], 'string', 'max' => 200],
            [['resolution'], 'string', 'max' => 21],
            [['multilang'], 'string', 'max' => 4000],
            [['syn_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'slug' => Yii::t('app', 'Slug'),
            'category_id' => Yii::t('app', 'Category ID'),
            'attributes' => Yii::t('app', 'Attributes'),
            'published_time' => Yii::t('app', 'Published Time'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_check' => Yii::t('app', 'Login required'),
            'price_download' => Yii::t('app', 'Price Download'),
            'price_play' => Yii::t('app', 'Price Play'),
            'play_times_real' => Yii::t('app', 'Play Times Real'),
            'play_times' => Yii::t('app', 'Play Times'),
            'status' => Yii::t('app', 'Status'),
            'reason' => Yii::t('app', 'Reason'),
            'approve_by' => Yii::t('app', 'Approve By'),
            'type' => Yii::t('app', 'Type'),
            'cp_id' => Yii::t('app', 'Cp ID'),
            'cp_code' => Yii::t('app', 'Cp Code'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'tag' => Yii::t('app', 'Tag'),
            'related_id' => Yii::t('app', 'Related ID'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'is_no_copyright' => Yii::t('app', 'Is No Copyright'),
            'like_count' => Yii::t('app', 'Like Count'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'is_hot' => Yii::t('app', 'Is Hot'),
            'is_recommend' => Yii::t('app', 'Is Recommend'),
            'convert_status' => Yii::t('app', 'Convert Status'),
            'convert_start_time' => Yii::t('app', 'Convert Start Time'),
            'convert_end_time' => Yii::t('app', 'Convert End Time'),
            'duration' => Yii::t('app', 'Duration'),
            'file_path' => Yii::t('app', 'File Path'),
            'file_bucket' => Yii::t('app', 'File Bucket'),
            'convert_priority' => Yii::t('app', 'Convert Priority'),
            'resolution' => Yii::t('app', 'Resolution'),
            'snapshot_count' => Yii::t('app', 'Snapshot Count'),
            'upload_process' => Yii::t('app', 'Upload Process'),
            'syn_id' => Yii::t('app', 'Syn ID'),
            'name_slug' => Yii::t('app', 'Name Slug'),
            'description_slug' => Yii::t('app', 'Description Slug'),
            'suggest_package_id' => Yii::t('app', 'Suggest Package ID'),
            'review_by' => Yii::t('app', 'Review By'),
            'sync_review_by' => Yii::t('app', 'Sync Review By'),
            'mode' => Yii::t('app', 'Mode'),
            'dislike_count' => Yii::t('app', 'Dislike Count'),
            'comment_count' => Yii::t('app', 'Comment Count'),
            'old_playlist_id' => Yii::t('app', 'Old Playlist ID'),
            'show_times' => Yii::t('app', 'Show Times'),
            'can_download' => Yii::t('app', 'Can Download'),
            'view_time' => Yii::t('app', 'View Time'),
            'outsource_review_at' => Yii::t('app', 'Outsource Review At'),
            'outsource_review_by' => Yii::t('app', 'Outsource Review By'),
            'outsource_status' => Yii::t('app', 'Outsource Status'),
            'outsource_need_review' => Yii::t('app', 'Outsource Need Review'),
            'outsource_review_duration' => Yii::t('app', 'Outsource Review Duration'),
            'admin_review_first_times_at' => Yii::t('app', 'First Time Admin Review At'),
            'admin_review_second_times_at' => Yii::t('app', 'Second Time Admin Review At'),
	        'drm_content_id' => Yii::t('app', 'Drm Content ID'),
	        'channel_id' => Yii::t('app', 'Channel ID'),
            'multilang' => 'Multilang',

        ];
    }
}
