<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_slideshow".
 *
 * @property string $id
 * @property string $type
 * @property string $item_id
 * @property string $position
 * @property string $location
 * @property string $name
 * @property string $href
 * @property string $begin_time
 * @property string $end_time
 * @property integer $is_active
 * @property string $bucket
 * @property string $path
 * @property string $updated_by
 * @property string $created_by
 */
class VtSlideshowDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_slideshow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'position', 'is_active', 'updated_by', 'created_by'], 'integer'],
            [['begin_time', 'end_time'], 'safe'],
            [['type'], 'string', 'max' => 10],
            [['location'], 'string', 'max' => 50],
            [['name', 'bucket', 'path'], 'string', 'max' => 255],
            [['href'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'item_id' => Yii::t('app', 'Item ID'),
            'position' => Yii::t('app', 'Position'),
            'location' => Yii::t('app', 'Location'),
            'name' => Yii::t('app', 'Name'),
            'href' => Yii::t('app', 'Href'),
            'begin_time' => Yii::t('app', 'Begin Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'is_active' => Yii::t('app', 'Is Active'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
