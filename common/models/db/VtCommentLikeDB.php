<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_comment_like".
 *
 * @property string $id
 * @property string $user_id
 * @property string $comment_id
 * @property string $content_id
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property int $like
 */
class VtCommentLikeDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_comment_like';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'comment_id', 'content_id','like'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'comment_id' => Yii::t('app', 'Comment ID'),
            'content_id' => Yii::t('app', 'Content ID'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'like'=> Yii::t('app', 'Like'),
        ];
    }
}
