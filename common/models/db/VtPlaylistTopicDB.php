<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_playlist_topic".
 *
 * @property string $id
 * @property string $playlist_id
 * @property integer $topic_id
 */
class VtPlaylistTopicDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_playlist_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playlist_id', 'topic_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'playlist_id' => Yii::t('app', 'Playlist ID'),
            'topic_id' => Yii::t('app', 'Topic ID'),
        ];
    }
}
