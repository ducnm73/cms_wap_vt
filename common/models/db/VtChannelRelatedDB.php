<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_channel_related".
 *
 * @property string $id
 * @property string $channel_id
 * @property string $channel_related_id
 */
class VtChannelRelatedDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_channel_related';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'channel_id', 'channel_related_id'], 'integer'],
            [['channel_id', 'channel_related_id'], 'unique', 'targetAttribute' => ['channel_id', 'channel_related_id'], 'message' => 'The combination of Channel ID and Channel Related ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'channel_related_id' => Yii::t('app', 'Channel Related ID'),
        ];
    }
}
