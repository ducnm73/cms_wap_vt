<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_gcm".
 *
 * @property string $id
 * @property string $user_id
 * @property string $register_id
 * @property integer $status
 * @property string $type
 * @property string $msisdn
 */
class VtGcmDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_gcm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['register_id'], 'string'],
            [['status', 'msisdn'], 'integer'],
            [['user_id'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'register_id' => 'Register ID',
            'status' => 'Status',
            'type' => 'Type',
            'msisdn' => 'Msisdn',
        ];
    }
}
