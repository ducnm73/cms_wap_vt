<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_profile".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_active
 * @property integer $min_org_res
 */
class VtProfileDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'min_org_res'], 'required'],
            [['is_active', 'min_org_res'], 'integer'],
            [['name'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'is_active' => Yii::t('app', 'Is Active'),
            'min_org_res' => Yii::t('app', 'Min Org Res'),
        ];
    }
}
