<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "video_views".
 *
 * @property integer $video_id
 * @property integer $view_count
 * @property string $date
 */
class VideoViewsDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id'], 'required'],
            [['video_id', 'view_count'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'video_id' => Yii::t('app', 'Video ID'),
            'view_count' => Yii::t('app', 'View Count'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
