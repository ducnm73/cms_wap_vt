<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user".
 *
 * @property string $id
 * @property string $msisdn
 * @property integer $status
 * @property string $password
 * @property string $salt
 * @property string $full_name
 * @property string $email
 * @property string $oauth_id
 * @property string $follow_count
 * @property string $otp
 * @property string $last_login
 * @property integer $changed_password
 * @property string $bucket
 * @property string $path
 * @property string $video_count
 * @property integer $is_show_suggest
 * @property string $channel_bucket
 * @property string $channel_path
 * @property integer $is_notification
 * @property string $description
 * @property string $full_name_slug
 * @property string $description_slug
 * @property integer $is_hot
 * @property integer $priority
 * @property string $created_at
 * @property string $view_count
 * @property string $google_oauth_id
 * @property string $cp_id
 */
class VtUserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'status', 'follow_count', 'changed_password', 'video_count', 'is_show_suggest', 'is_notification', 'is_hot', 'priority', 'view_count', 'cp_id'], 'integer'],
            [['last_login', 'created_at'], 'safe'],
            [['password'], 'string', 'max' => 100],
            [['salt', 'bucket', 'path', 'channel_bucket', 'channel_path'], 'string', 'max' => 255],
            [['full_name', 'email', 'oauth_id', 'full_name_slug', 'google_oauth_id'], 'string', 'max' => 50],
            [['otp'], 'string', 'max' => 10],
            [['description', 'description_slug'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'status' => Yii::t('app', 'Status'),
            'password' => Yii::t('app', 'Password'),
            'salt' => Yii::t('app', 'Salt'),
            'full_name' => Yii::t('app', 'Full Name'),
            'email' => Yii::t('app', 'Email'),
            'oauth_id' => Yii::t('app', 'Oauth ID'),
            'follow_count' => Yii::t('app', 'Follow Count'),
            'otp' => Yii::t('app', 'Otp'),
            'last_login' => Yii::t('app', 'Last Login'),
            'changed_password' => Yii::t('app', 'Changed Password'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'video_count' => Yii::t('app', 'Video Count'),
            'is_show_suggest' => Yii::t('app', 'Is Show Suggest'),
            'channel_bucket' => Yii::t('app', 'Channel Bucket'),
            'channel_path' => Yii::t('app', 'Channel Path'),
            'is_notification' => Yii::t('app', 'Is Notification'),
            'description' => Yii::t('app', 'Description'),
            'full_name_slug' => Yii::t('app', 'Full Name Slug'),
            'description_slug' => Yii::t('app', 'Description Slug'),
            'is_hot' => Yii::t('app', 'Is Hot'),
            'priority' => Yii::t('app', 'Priority'),
            'created_at' => Yii::t('app', 'Created At'),
            'view_count' => Yii::t('app', 'View Count'),
            'google_oauth_id' => Yii::t('app', 'Google Oauth ID'),
            'cp_id' => Yii::t('app', 'Cp ID'),
        ];
    }
}
