<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_comment".
 *
 * @property string $id
 * @property string $content_id
 * @property string $type
 * @property string $user_id
 * @property string $content
 * @property string $parent_id
 * @property integer $status
 * @property string $bad_word_filter
 * @property string $approve_by
 * @property string $reason
 * @property int $like_count
 * @property int $dislike_count
 * @property string $created_at
 * @property string $updated_at
 * @property string $comment
 * @property string $comment_count
 */
class VtCommentDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content_id', 'user_id', 'parent_id', 'status', 'approve_by', 'like_count','dislike_count', 'comment_count'], 'integer'],
            [['user_id', 'content'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['comment'], 'string'],
            [['type'], 'string', 'max' => 10],
            [['content'], 'string', 'max' => 255],
            [['bad_word_filter', 'reason'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content_id' => Yii::t('app', 'Content ID'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User ID'),
            'content' => Yii::t('app', 'Content'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'status' => Yii::t('app', 'Status'),
            'bad_word_filter' => Yii::t('app', 'Bad Word Filter'),
            'approve_by' => Yii::t('app', 'Approve By'),
            'reason' => Yii::t('app', 'Reason'),
            'like_count' => Yii::t('app', 'Like Count'),
            'dislike_count' => Yii::t('app', 'Dislike Count'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'comment' => Yii::t('app', 'Comment'),
            'comment_count' => Yii::t('app', 'Comment Count'),
        ];
    }
}
