<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "log_transaction".
 *
 * @property string $id
 * @property string $user_id
 * @property string $msisdn
 * @property string $action
 * @property string $fee
 * @property string $error_code
 * @property string $content
 * @property string $datetime
 * @property string $other_info
 */
class LogTransactionDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'msisdn', 'fee'], 'integer'],
            [['datetime'], 'safe'],
            [['action', 'error_code', 'content', 'other_info'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'action' => Yii::t('app', 'Action'),
            'fee' => Yii::t('app', 'Fee'),
            'error_code' => Yii::t('app', 'Error Code'),
            'content' => Yii::t('app', 'Content'),
            'datetime' => Yii::t('app', 'Datetime'),
            'other_info' => Yii::t('app', 'Other Info'),
        ];
    }
}
