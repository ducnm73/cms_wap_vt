<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "log_map_account".
 *
 * @property string $id
 * @property string $user_id_source
 * @property string $msisdn_source
 * @property string $msisdn_target
 * @property string $user_id_target
 * @property string $created_at
 * @property string $description
 */
class LogMapAccountDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_map_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['user_id_source', 'msisdn_source', 'msisdn_target', 'user_id_target'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id_source' => 'User Id Source',
            'msisdn_source' => 'Msisdn Source',
            'msisdn_target' => 'Msisdn Target',
            'user_id_target' => 'User Id Target',
            'created_at' => 'Created At',
            'description' => 'Description',
        ];
    }
}
