<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "log_sub_view".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $action
 * @property string $video_id
 * @property string $sub_type
 * @property string $datetime
 * @property string $package_type
 * @property string $cp_id
 * @property string $package_id
 * @property string $first_charge
 * @property string $other_info
 * @property string $cp_code
 */
class LogSubViewDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_sub_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'cp_id', 'package_id', 'first_charge'], 'integer'],
            [['datetime'], 'required'],
            [['datetime'], 'safe'],
            [['msisdn'], 'string', 'max' => 15],
            [['action'], 'string', 'max' => 50],
            [['sub_type', 'other_info', 'cp_code'], 'string', 'max' => 20],
            [['package_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'action' => Yii::t('app', 'Action'),
            'video_id' => Yii::t('app', 'Video ID'),
            'sub_type' => Yii::t('app', 'Sub Type'),
            'datetime' => Yii::t('app', 'Datetime'),
            'package_type' => Yii::t('app', 'Package Type'),
            'cp_id' => Yii::t('app', 'Cp ID'),
            'package_id' => Yii::t('app', 'Package ID'),
            'first_charge' => Yii::t('app', 'First Charge'),
            'other_info' => Yii::t('app', 'Other Info'),
            'cp_code' => Yii::t('app', 'Cp Code'),
        ];
    }
}
