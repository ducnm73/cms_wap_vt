<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_sms_message".
 *
 * @property string $id
 * @property string $sms_code
 * @property string $sms_message
 * @property string $description
 */
class VtSmsMessageDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_sms_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['sms_code'], 'string', 'max' => 50],
            [['sms_message'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_code' => 'Sms Code',
            'sms_message' => 'Sms Message',
            'description' => 'Description',
        ];
    }
}
