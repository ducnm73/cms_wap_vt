<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "kpi_upload".
 *
 * @property string $id
 * @property string $session_id
 * @property string $video_id
 * @property string $upload_start_time
 * @property string $upload_end_time
 * @property string $upload_duration
 * @property string $s3_start_time
 * @property string $s3_end_time
 * @property string $s3_duration
 * @property string $error_code
 * @property string $size
 */
class KpiUploadDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'upload_duration', 's3_duration', 'size'], 'integer'],
            [['upload_start_time', 'upload_end_time', 's3_start_time', 's3_end_time'], 'safe'],
            [['session_id', 'error_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'upload_start_time' => Yii::t('app', 'Upload Start Time'),
            'upload_end_time' => Yii::t('app', 'Upload End Time'),
            'upload_duration' => Yii::t('app', 'Upload Duration'),
            's3_start_time' => Yii::t('app', 'S3 Start Time'),
            's3_end_time' => Yii::t('app', 'S3 End Time'),
            's3_duration' => Yii::t('app', 'S3 Duration'),
            'error_code' => Yii::t('app', 'Error Code'),
            'size' => Yii::t('app', 'Size'),
        ];
    }
}
