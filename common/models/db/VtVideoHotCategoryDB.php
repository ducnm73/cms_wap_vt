<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_video_hot_category".
 *
 * @property string $id
 * @property string $category_id
 * @property string $video_ids
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_hot
 */
class VtVideoHotCategoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_video_hot_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'is_hot'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['video_ids'], 'string', 'max' => 1000],
            [['category_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'video_ids' => Yii::t('app', 'Video Ids'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_hot' => Yii::t('app', 'Is Hot'),
        ];
    }
}
