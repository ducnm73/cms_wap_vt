<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_mapping_crawler".
 *
 * @property string $id
 * @property string $crawler_id
 * @property integer $local_user_id
 * @property integer $local_category_id
 * @property string $name
 * @property string $cp_id
 */
class VtMappingCrawlerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_mapping_crawler';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crawler_id', 'local_user_id', 'local_category_id', 'cp_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['crawler_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'crawler_id' => Yii::t('app', 'Crawler ID'),
            'local_user_id' => Yii::t('app', 'Local User ID'),
            'local_category_id' => Yii::t('app', 'Local Category ID'),
            'name' => Yii::t('app', 'Name'),
            'cp_id' => Yii::t('app', 'Cp ID'),
        ];
    }
}
