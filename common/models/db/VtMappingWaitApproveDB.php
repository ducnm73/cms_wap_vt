<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_mapping_wait_approve".
 *
 * @property string $id
 * @property string $local_user_id
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class VtMappingWaitApproveDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_mapping_wait_approve';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['local_user_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'local_user_id' => Yii::t('app', 'Local User ID'),
            'is_active' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
