<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_report_user_upload".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $revenue
 * @property string $report_month
 * @property integer $status
 */
class VtReportUserUploadDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_report_user_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'revenue', 'status'], 'integer'],
            [['report_month'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'revenue' => Yii::t('app', 'Revenue'),
            'report_month' => Yii::t('app', 'Report Month'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
