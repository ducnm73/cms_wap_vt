<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_channel".
 *
 * @property integer $id
 * @property integer $status
 * @property string $full_name
 * @property integer $follow_count
 * @property string $bucket
 * @property string $path
 * @property integer $video_count
 * @property integer $is_show_suggest
 * @property string $channel_bucket
 * @property string $channel_path
 * @property string $description
 * @property integer $is_hot
 * @property integer $priority
 * @property string $created_at
 * @property integer $view_count
 * @property integer $user_id
 * @property string $updated_at
 */
class vtChannelDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['follow_count', 'video_count', 'priority', 'view_count', 'user_id'], 'integer'],
            [['created_at','updated_at'], 'safe'],
            [['status', 'is_show_suggest', 'is_hot'], 'string', 'max' => 1],
            [['full_name'], 'string', 'max' => 100, 'tooLong' => Yii::t('backend', 'Please enter the name from 1 to 100 character')],
            [['bucket', 'path', 'channel_bucket', 'channel_path'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000, 'tooLong' => Yii::t('backend', 'Please enter a description from 1 to 1000 character')],
            [['full_name'], 'required', 'message' => Yii::t('backend', 'Tên channel') . Yii::t('backend', ' không được để trống')],
            [['user_id'], 'required', 'message' => Yii::t('backend', 'Tên user').Yii::t('backend', ' không được để trống')],
            [['description'], 'required'],
            [['channel_path', 'path'], 'image', 'extensions' => 'png, jpg, gif', 'wrongExtension' => Yii::t('backend', 'The image file is specified: jpg, png, gif')],
            [['channel_path'], 'image', 'maxSize' => 1024*1024*5, 'tooBig' => Yii::t('backend', 'Maximum avatar image size is 5MB')],
            [['path'], 'image', 'maxSize' => 1024*1024*5, 'tooBig' => Yii::t('backend', 'Maximum banner image size is 5MB')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => '0: draft|1: active|2: banned',
            'full_name' => 'Full Name',
            'follow_count' => 'Follow Count',
            'bucket' => 'Bucket',
            'path' => 'Path',
            'video_count' => 'Tong so video dang duoc duyet',
            'is_show_suggest' => 'Is Show Suggest',
            'channel_bucket' => 'Channel Bucket',
            'channel_path' => 'Channel Path',
            'description' => 'Description',
            'is_hot' => 'Is Hot',
            'priority' => 'Do uu tien cua kenh, so cang lon uu tien cang cao',
            'created_at' => 'Created At',
            'view_count' => 'View Count',
            'user_id' => 'User ID',
        ];
    }
}