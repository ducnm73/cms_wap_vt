<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_smart_tv_token".
 *
 * @property string $id
 * @property string $token
 * @property string $code
 * @property integer $status
 * @property string $expire_time
 * @property string $ip
 * @property string $user_id
 */
class VtSmartTvTokenDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_smart_tv_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['expire_time'], 'safe'],
            [['token'], 'string', 'max' => 100],
            [['code', 'ip'], 'string', 'max' => 50],
            [['user_id'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'code' => 'Code',
            'status' => 'Status',
            'expire_time' => 'Expire Time',
            'ip' => 'Ip',
            'user_id' => 'User ID',
        ];
    }
}
