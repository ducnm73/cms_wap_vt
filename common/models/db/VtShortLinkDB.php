<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_short_link".
 *
 * @property string $id
 * @property string $short_link
 * @property string $route
 * @property string $params
 * @property integer $is_active
 * @property string $full_link
 * @property string $other_info
 */
class VtShortLinkDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_short_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_link', 'route'], 'required'],
            [['is_active'], 'integer'],
            [['short_link', 'route', 'other_info'], 'string', 'max' => 255],
            [['params', 'full_link'], 'string', 'max' => 1000],
            [['short_link'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'short_link' => Yii::t('app', 'Short Link'),
            'route' => Yii::t('app', 'Route'),
            'params' => Yii::t('app', 'Params'),
            'is_active' => Yii::t('app', 'Is Active'),
            'full_link' => Yii::t('app', 'Full Link'),
            'other_info' => Yii::t('app', 'Other Info'),
        ];
    }
}
