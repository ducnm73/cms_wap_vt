<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_buy_playlist".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $user_id
 * @property string $playlist_id
 * @property string $created_at
 */
class VtBuyPlaylistDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_buy_playlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'user_id', 'playlist_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'user_id' => 'User ID',
            'playlist_id' => 'Playlist ID',
            'created_at' => 'Created At',
        ];
    }
}
