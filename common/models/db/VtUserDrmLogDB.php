<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_drm_log".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $user_id
 * @property string $device_id
 * @property string $network_device_id
 * @property integer $action
 * @property string $created_at
 * @property string $entitlement_id
 * @property string $device_type
 * @property string $updated_at
 * @property string $error_code
 * @property string $sms_package_id
 */
class VtUserDrmLogDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_drm_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'user_id', 'action'], 'integer'],
            [['device_id', 'network_device_id', 'entitlement_id', 'device_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['device_id', 'network_device_id', 'entitlement_id', 'sms_package_id'], 'string', 'max' => 255],
            [['device_type'], 'string', 'max' => 20],
            [['error_code'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'user_id' => Yii::t('app', 'User ID'),
            'device_id' => Yii::t('app', 'Device ID'),
            'network_device_id' => Yii::t('app', 'Network Device ID'),
            'action' => Yii::t('app', 'Action'),
            'created_at' => Yii::t('app', 'Created At'),
            'entitlement_id' => Yii::t('app', 'Entitlement ID'),
            'device_type' => Yii::t('app', 'Device Type'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'error_code' => Yii::t('app', 'Error Code'),
            'sms_package_id' => Yii::t('app', 'Sms Package ID'),
        ];
    }
}
