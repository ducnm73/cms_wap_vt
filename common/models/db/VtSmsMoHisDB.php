<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_sms_mo_his".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $command
 * @property string $param
 * @property integer $err_code
 * @property string $process_time
 * @property string $channel
 * @property integer $action_id
 * @property string $receive_time
 * @property string $action_type
 * @property string $ip
 */
class VtSmsMoHisDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_sms_mo_his';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'command', 'process_time', 'receive_time'], 'required'],
            [['err_code', 'action_id'], 'integer'],
            [['process_time', 'receive_time'], 'safe'],
            [['msisdn'], 'string', 'max' => 15],
            [['command'], 'string', 'max' => 50],
            [['param'], 'string', 'max' => 200],
            [['channel'], 'string', 'max' => 10],
            [['action_type', 'ip'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'command' => Yii::t('app', 'Command'),
            'param' => Yii::t('app', 'Param'),
            'err_code' => Yii::t('app', 'Err Code'),
            'process_time' => Yii::t('app', 'Process Time'),
            'channel' => Yii::t('app', 'Channel'),
            'action_id' => Yii::t('app', 'Action ID'),
            'receive_time' => Yii::t('app', 'Receive Time'),
            'action_type' => Yii::t('app', 'Action Type'),
            'ip' => Yii::t('app', 'Ip'),
        ];
    }
}
