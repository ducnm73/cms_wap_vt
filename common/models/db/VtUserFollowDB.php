<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_follow".
 *
 * @property string $id
 * @property string $user_id
 * @property string $follow_id
 * @property string $notification_type
 */
class VtUserFollowDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_follow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'follow_id'], 'required'],
            [['user_id', 'follow_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'follow_id' => Yii::t('app', 'Follow ID'),
        ];
    }
}
