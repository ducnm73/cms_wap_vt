<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_org_file".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property integer $progress
 * @property integer $status
 * @property string $bucket
 * @property string $path
 * @property string $mime
 * @property string $size
 * @property string $dimensions
 * @property string $last_update
 * @property integer $conversion_times
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $duration
 * @property integer $height
 * @property integer $width
 * @property integer $priority
 * @property string $checksum
 *
 * @property VtConvertedFileDB[] $vtConvertedFileDBs
 * @property VtVideoDB[] $vtVideoDBs
 */
class VtOrgFileDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_org_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['progress', 'status', 'size', 'conversion_times', 'created_by', 'updated_by', 'duration', 'height', 'width', 'priority'], 'integer'],
            [['last_update', 'created_at', 'updated_at'], 'safe'],
            [['created_at', 'updated_at', 'height', 'width'], 'required'],
            [['title', 'bucket', 'path'], 'string', 'max' => 255],
            [['mime'], 'string', 'max' => 63],
            [['dimensions'], 'string', 'max' => 15],
            [['checksum'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'progress' => Yii::t('app', 'Progress'),
            'status' => Yii::t('app', 'Status'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'mime' => Yii::t('app', 'Mime'),
            'size' => Yii::t('app', 'Size'),
            'dimensions' => Yii::t('app', 'Dimensions'),
            'last_update' => Yii::t('app', 'Last Update'),
            'conversion_times' => Yii::t('app', 'Conversion Times'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'duration' => Yii::t('app', 'Duration'),
            'height' => Yii::t('app', 'Height'),
            'width' => Yii::t('app', 'Width'),
            'priority' => Yii::t('app', 'Priority'),
            'checksum' => Yii::t('app', 'Checksum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtConvertedFileDBs()
    {
        return $this->hasMany(VtConvertedFileDB::className(), ['org_file_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtVideoDBs()
    {
        return $this->hasMany(VtVideoDB::className(), ['org_file_id' => 'id']);
    }
}
