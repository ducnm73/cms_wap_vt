<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_award".
 *
 * @property integer $id
 * @property string $award_type
 * @property string $award_name
 */
class VtAwardDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_award';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['award_type', 'award_name'], 'required'],
            [['award_type', 'award_name'], 'string', 'max' => 255],
            [['award_type'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'award_type' => Yii::t('app', 'Award Type'),
            'award_name' => Yii::t('app', 'Award Name'),
        ];
    }
}
