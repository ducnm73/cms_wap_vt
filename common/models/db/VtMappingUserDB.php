<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_mapping_user".
 *
 * @property string $id
 * @property string $username
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $cp_id
 * @property string $local_user_id
 * @property integer $local_category_id
 * @property integer $local_channel_id
 */
class VtMappingUserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_mapping_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['status', 'created_at', 'updated_at', 'cp_id', 'local_user_id', 'local_category_id', 'local_channel_id'], 'integer'],
            [['username', 'email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'cp_id' => Yii::t('app', 'Cp ID'),
            'local_user_id' => Yii::t('app', 'Local User ID'),
            'local_category_id' => Yii::t('app', 'Local Category ID'),
            'local_channel_id' => Yii::t('app', 'Local channel ID'),
        ];
    }
}
