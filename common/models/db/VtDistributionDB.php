<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_distribution".
 *
 * @property integer $id
 * @property string $name
 * @property string $sub_domain
 * @property string $description
 * @property string $category_ids
 * @property integer $is_active
 */
class VtDistributionDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_distribution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sub_domain'], 'required'],
            [['is_active'], 'integer'],
            [['name', 'sub_domain'], 'string', 'max' => 255],
            [['description', 'category_ids'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'sub_domain' => Yii::t('app', 'Sub Domain'),
            'description' => Yii::t('app', 'Description'),
            'category_ids' => Yii::t('app', 'Category Ids'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }
}
