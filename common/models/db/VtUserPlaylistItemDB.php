<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_playlist_item".
 *
 * @property string $id
 * @property string $item_id
 * @property string $playlist_id
 * @property string $alias
 * @property string $position
 */
class VtUserPlaylistItemDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_playlist_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'playlist_id', 'position'], 'integer'],
            [['alias'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'playlist_id' => 'Playlist ID',
            'alias' => 'Alias',
            'position' => 'Position',
        ];
    }
}
