<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_video_hot".
 *
 * @property string $id
 * @property string $category_id
 * @property string $video_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_active
 */
class VtVideoHotDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_video_hot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'video_id', 'is_active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['video_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }
}
