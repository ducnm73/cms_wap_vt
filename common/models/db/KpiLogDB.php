<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "kpi_log".
 *
 * @property string $id
 * @property string $ApplicationCode
 * @property string $ServiceCode
 * @property integer $SessionID
 * @property string $IP_Port_ParentNode
 * @property string $IP_Port_CurrentNode
 * @property string $RequestContent
 * @property string $ResponseContent
 * @property string $StartTime
 * @property string $EndTime
 * @property string $Duration
 * @property string $ErrorCode
 * @property string $ErrorDescription
 * @property string $TransactionStatus
 * @property string $ActionName
 * @property string $UserName
 * @property string $Account
 */
class KpiLogDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SessionID', 'Duration'], 'integer'],
            [['StartTime', 'EndTime'], 'safe'],
            [['ApplicationCode', 'ServiceCode', 'IP_Port_ParentNode', 'IP_Port_CurrentNode', 'RequestContent', 'ResponseContent', 'ErrorCode', 'ErrorDescription', 'TransactionStatus', 'ActionName', 'UserName', 'Account'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ApplicationCode' => 'Application Code',
            'ServiceCode' => 'Service Code',
            'SessionID' => 'Session ID',
            'IP_Port_ParentNode' => 'Ip  Port  Parent Node',
            'IP_Port_CurrentNode' => 'Ip  Port  Current Node',
            'RequestContent' => 'Request Content',
            'ResponseContent' => 'Response Content',
            'StartTime' => 'Start Time',
            'EndTime' => 'End Time',
            'Duration' => 'Duration',
            'ErrorCode' => 'Error Code',
            'ErrorDescription' => 'Error Description',
            'TransactionStatus' => 'Transaction Status',
            'ActionName' => 'Action Name',
            'UserName' => 'User Name',
            'Account' => 'Account',
        ];
    }
}
