<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_feed_back".
 *
 * @property string $id
 * @property string $user_id
 * @property string $msisdn
 * @property string $item_id
 * @property string $content
 * @property integer $is_received
 * @property string $type
 * @property string $request_type
 * @property string $created_at
 * @property string $updated_at
 */
class VtFeedBackDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_feed_back';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'msisdn', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'msisdn', 'item_id', 'is_received'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 50],
            [['request_type'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'msisdn' => 'Msisdn',
            'item_id' => 'Item ID',
            'content' => 'Content',
            'is_received' => 'Is Received',
            'type' => 'Type',
            'request_type' => 'Request Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
