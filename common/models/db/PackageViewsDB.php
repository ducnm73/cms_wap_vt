<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "package_views".
 *
 * @property integer $package_id
 * @property integer $view_count
 * @property string $date
 */
class PackageViewsDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id'], 'required'],
            [['package_id', 'view_count'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'package_id' => Yii::t('app', 'Package ID'),
            'view_count' => Yii::t('app', 'View Count'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
