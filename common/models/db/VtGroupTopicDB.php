<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_group_topic".
 *
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $description
 * @property integer $is_active
 * @property string $bucket
 * @property string $path
 * @property string $slug
 * @property string $created_by
 * @property string $updated_by
 * @property array $topics
 */
class VtGroupTopicDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_group_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['is_active', 'created_by', 'updated_by'], 'integer'],
            [['name', 'bucket', 'path', 'slug'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'is_active' => Yii::t('app', 'Is Active'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'slug' => Yii::t('app', 'Slug'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
