<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_package".
 *
 * @property integer $id
 * @property string $name
 * @property string $sub_service_name
 * @property string $short_description
 * @property string $description
 * @property string $type
 * @property string $group_type
 * @property integer $is_active
 * @property string $fee
 * @property string $charge_range
 * @property string $package_type
 * @property integer $priority
 * @property integer $is_display_frontend
 * @property double $cp_data_percentage
 * @property integer $is_block_check
 * @property string $quota_views
 * @property string $distribution_id
 * @property string $multilang
 */
class VtPackageDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sub_service_name', 'type', 'group_type'], 'required'],
            [['is_active', 'fee', 'priority', 'is_display_frontend', 'is_block_check', 'quota_views', 'distribution_id'], 'integer'],
            [['name', 'short_description'], 'string', 'max' => 255],
            [['sub_service_name', 'type', 'group_type', 'charge_range', 'package_type'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 1000],
            [['multilang'], 'safe'],
            [['cp_data_percentage'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'sub_service_name' => Yii::t('app', 'Sub Service Name'),
            'short_description' => Yii::t('app', 'Short Description'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'group_type' => Yii::t('app', 'Group Type'),
            'is_active' => Yii::t('app', 'Is Active'),
            'fee' => Yii::t('app', 'Fee'),
            'charge_range' => Yii::t('app', 'Charge Range'),
            'package_type' => Yii::t('app', 'Package Type'),
            'priority' => Yii::t('app', 'Priority'),
            'is_display_frontend' => Yii::t('app', 'Is Display Frontend'),
            'cp_data_percentage' => Yii::t('app', 'Cp Data Percentage'),
            'is_block_check' => Yii::t('app', 'Is Block Check'),
            'quota_views' => Yii::t('app', 'Quota Views'),
            'distribution_id' => Yii::t('app', 'Distribution ID'),
            'multilang' => Yii::t('app', 'Multilang'),
        ];
    }
}
