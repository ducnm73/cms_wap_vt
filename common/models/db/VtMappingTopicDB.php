<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_mapping_topic".
 *
 * @property string $id
 * @property string $crawler_category_id
 * @property integer $local_topic_id
 * @property string $local_category_id
 * @property string $name
 * @property integer $type
 */
class VtMappingTopicDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_mapping_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crawler_category_id', 'local_topic_id', 'local_category_id', 'type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['crawler_category_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'crawler_category_id' => Yii::t('app', 'Crawler Category ID'),
            'local_topic_id' => Yii::t('app', 'Local Topic ID'),
            'local_category_id' => Yii::t('app', 'Local Category ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
