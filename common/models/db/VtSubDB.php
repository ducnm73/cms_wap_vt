<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_sub".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $user_id
 * @property integer $package_id
 * @property string $sub_service_name
 * @property integer $status
 * @property string $fee
 * @property string $register_time
 * @property string $expired_time
 * @property string $cancel_time
 * @property string $last_charge_time
 * @property string $source
 */
class VtSubDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_sub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'package_id', 'sub_service_name', 'register_time', 'expired_time', 'last_charge_time', 'source'], 'required'],
            [['msisdn', 'user_id', 'package_id', 'status', 'fee'], 'integer'],
            [['register_time', 'expired_time', 'cancel_time', 'last_charge_time'], 'safe'],
            [['sub_service_name', 'source'], 'string', 'max' => 50],
            [['msisdn', 'package_id'], 'unique', 'targetAttribute' => ['msisdn', 'package_id'], 'message' => 'The combination of Msisdn and Package ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'user_id' => 'User ID',
            'package_id' => 'Package ID',
            'sub_service_name' => 'Sub Service Name',
            'status' => 'Status',
            'fee' => 'Fee',
            'register_time' => 'Register Time',
            'expired_time' => 'Expired Time',
            'cancel_time' => 'Cancel Time',
            'last_charge_time' => 'Last Charge Time',
            'source' => 'Source',
        ];
    }
}
