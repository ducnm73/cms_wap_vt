<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_view".
 *
 * @property string $id
 * @property string $user_id
 * @property string $msisdn
 * @property string $first_time
 */
class VtUserViewDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn'], 'integer'],
            [['first_time'], 'safe'],
            [['user_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'msisdn' => 'Msisdn',
            'first_time' => 'First Time',
        ];
    }
}
