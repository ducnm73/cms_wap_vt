<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_change_info".
 *
 * @property string $id
 * @property string $user_id
 * @property integer $status
 * @property string $full_name
 * @property string $bucket
 * @property string $path
 * @property string $channel_bucket
 * @property string $channel_path
 * @property string $description
 * @property string $reason
 * @property string $created_at
 * @property string $updated_at
 */
class VtUserChangeInfoDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_change_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name', 'bucket', 'path', 'channel_bucket', 'channel_path', 'reason'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Trạng thái',
            'full_name' => 'Tên kênh',
            'bucket' => 'Bucket',
            'path' => 'Path',
            'channel_bucket' => 'Channel Bucket',
            'channel_path' => 'Channel Path',
            'description' => 'Mô tả',
            'reason' => 'Lý do',
            'created_at' => 'Tạo lúc',
            'updated_at' => 'Cập nhật lúc',
        ];
    }
}
