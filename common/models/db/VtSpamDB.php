<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_spam".
 *
 * @property string $id
 * @property string $name
 * @property string $content
 * @property integer $is_active
 * @property integer $is_sent
 * @property string $send_time
 * @property string $path
 * @property integer $number
 * @property integer $current_line
 * @property string $rule
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $item_type
 * @property string $item_id
 * @property string $item_group_id
 * @property integer $status
 * @property integer $channel
 * @property string $file_path
 * @property string $bucket
 */
class VtSpamDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_spam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'send_time', 'updated_by', 'channel'], 'required'],
            [['is_active', 'is_sent', 'number', 'current_line', 'created_by', 'updated_by', 'item_group_id', 'status', 'channel'], 'integer'],
            [['send_time', 'created_at', 'updated_at'], 'safe'],
            [['name', 'path', 'item_type'], 'string', 'max' => 255],
            [['content'], 'string', 'max' => 500],
            [['rule'], 'string', 'max' => 1000],
            [['item_id'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'content' => Yii::t('app', 'Content'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_sent' => Yii::t('app', 'Is Sent'),
            'send_time' => Yii::t('app', 'Send Time'),
            'path' => Yii::t('app', 'Path'),
            'number' => Yii::t('app', 'Number'),
            'current_line' => Yii::t('app', 'Current Line'),
            'rule' => Yii::t('app', 'Rule'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'item_type' => Yii::t('app', 'Item Type'),
            'item_id' => Yii::t('app', 'Item ID'),
            'item_group_id' => Yii::t('app', 'Item Group ID'),
            'status' => Yii::t('app', 'Status'),
            'channel' => Yii::t('app', 'Channel'),
        ];
    }
}
