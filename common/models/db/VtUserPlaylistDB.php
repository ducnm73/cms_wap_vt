<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_user_playlist".
 *
 * @property string $id
 * @property string $type
 * @property string $name
 * @property string $description
 * @property integer $is_active
 * @property integer $status
 * @property string $user_id
 * @property string $msisdn
 * @property string $created_at
 * @property string $updated_at
 * @property integer $num_video
 * @property string $bucket
 * @property string $path
 * @property string $source_display
 * @property string $created_by
 * @property string $updated_by
 * @property string $play_times
 * @property integer $is_finish
 */
class VtUserPlaylistDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_user_playlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['is_active', 'status', 'user_id', 'msisdn', 'num_video', 'created_by', 'updated_by', 'play_times', 'is_finish'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 25],
            [['name', 'bucket', 'path', 'source_display'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'description' => 'Description',
            'is_active' => 'Is Active',
            'status' => 'Status',
            'user_id' => 'User ID',
            'msisdn' => 'Msisdn',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'num_video' => 'Num Video',
            'bucket' => 'Bucket',
            'path' => 'Path',
            'source_display' => 'Source Display',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'play_times' => 'Play Times',
            'is_finish' => 'Is Finish',
        ];
    }
}
