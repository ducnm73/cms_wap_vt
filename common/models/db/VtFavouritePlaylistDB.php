<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_favourite_playlist".
 *
 * @property string $id
 * @property string $playlist_id
 * @property string $user_id
 */
class VtFavouritePlaylistDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_favourite_playlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playlist_id', 'user_id'], 'required'],
            [['playlist_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'playlist_id' => Yii::t('app', 'Playlist ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }
}
