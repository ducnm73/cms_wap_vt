<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_action_log".
 *
 * @property string $id
 * @property string $module
 * @property string $content_id
 * @property string $type
 * @property string $user_id
 * @property string $action
 * @property string $source
 * @property string $created_at
 * @property string $detail
 * @property string $ip
 * @property string $url
 */
class VtActionLogDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_action_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content_id', 'user_id'], 'integer'],
            [['created_at'], 'required'],
            [['created_at'], 'safe'],
            [['detail'], 'string'],
            [['module', 'type', 'source', 'ip'], 'string', 'max' => 50],
            [['action'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module' => Yii::t('app', 'Module'),
            'content_id' => Yii::t('app', 'Content ID'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User ID'),
            'action' => Yii::t('app', 'Action'),
            'source' => Yii::t('app', 'Source'),
            'created_at' => Yii::t('app', 'Created At'),
            'detail' => Yii::t('app', 'Detail'),
            'ip' => Yii::t('app', 'Ip'),
            'url' => Yii::t('app', 'Url'),
        ];
    }
}
