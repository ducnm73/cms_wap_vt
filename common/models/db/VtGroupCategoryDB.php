<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_group_category".
 *
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $description
 * @property integer $is_active
 * @property string $bucket
 * @property string $path
 * @property string $positions
 * @property string $slug
 * @property string $created_by
 * @property string $updated_by
 * @property string $parent_id
 * @property string $play_times
 * @property string $play_times_real
 * @property string $avatar_path
 * @property string $avatar_bucket
 * @property integer $is_hot
 */
class VtGroupCategoryDB extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_group_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['is_active', 'positions', 'created_by', 'updated_by', 'parent_id', 'play_times', 'play_times_real', 'is_hot'], 'integer'],
            [['name', 'bucket', 'path', 'slug', 'avatar_path', 'avatar_bucket'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'is_active' => Yii::t('app', 'Is Active'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => Yii::t('app', 'Path'),
            'positions' => Yii::t('app', 'Positions'),
            'slug' => Yii::t('app', 'Slug'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'play_times' => Yii::t('app', 'Play Times'),
            'play_times_real' => Yii::t('app', 'Play Times Real'),
            'avatar_path' => Yii::t('app', 'Avatar Path'),
            'avatar_bucket' => Yii::t('app', 'Avatar Bucket'),
            'is_hot' => Yii::t('app', 'Is Hot'),
        ];
    }
}
