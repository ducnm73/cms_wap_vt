<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_playlist_item".
 *
 * @property string $id
 * @property string $item_id
 * @property string $playlist_id
 * @property string $alias
 */
class VtPlaylistItemDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_playlist_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'playlist_id'], 'integer'],
            [['alias'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'playlist_id' => Yii::t('app', 'Playlist ID'),
            'alias' => Yii::t('app', 'Alias'),
        ];
    }
}
