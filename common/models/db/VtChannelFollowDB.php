<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_channel_follow".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $follow_id
 * @property integer $notification_type
 */
class VtChannelFollowDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_channel_follow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'follow_id'], 'required'],
            [['user_id', 'follow_id'], 'integer'],
            [['notification_type'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'user_id theo doi follow_id',
            'follow_id' => 'user_id theo doi follow_id',
            'notification_type' => 'Notification Type',
        ];
    }
}
