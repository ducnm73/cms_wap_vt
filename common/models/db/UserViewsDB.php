<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "user_views".
 *
 * @property string $msisdn
 * @property integer $view_count
 * @property string $date
 */
class UserViewsDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'date'], 'required'],
            [['view_count'], 'integer'],
            [['date'], 'safe'],
            [['msisdn'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'msisdn' => 'Msisdn',
            'view_count' => 'View Count',
            'date' => 'Date',
        ];
    }
}
