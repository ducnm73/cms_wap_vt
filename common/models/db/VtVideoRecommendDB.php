<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_video_recommend".
 *
 * @property string $id
 * @property string $video_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_active
 */
class VtVideoRecommendDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_video_recommend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'is_active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['video_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }
}
