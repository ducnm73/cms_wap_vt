<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_config".
 *
 * @property string $id
 * @property string $config_key
 * @property string $config_value
 * @property string $description
 * @property string $can_edit
 */
class VtConfigDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_value'], 'string'],
            [['config_value'], 'string'],
            [['config_key', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'config_key' => Yii::t('app', 'KEY'),
            'config_value' => Yii::t('app', 'VALUE'),
            'description' => Yii::t('app', 'Mô tả'),
        ];
    }
}
