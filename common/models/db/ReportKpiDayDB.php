<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "report_kpi".
 *
 * @property string $id
 * @property string $type
 * @property string $report_day
 * @property string $avg_time
 * @property string $total_trans
 * @property string $failure_trans
 * @property double $success_rate
 * @property double $failure_rate
 * @property double $avg_bandwidth
 */
class ReportKpiDayDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_kpi_day';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_day'], 'required'],
            [['avg_time', 'total_trans', 'failure_trans'], 'integer'],
            [['success_rate', 'failure_rate', 'avg_bandwidth'], 'number'],
            [['type'], 'string', 'max' => 255],
            [['report_day'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Loại KPI'),
            'report_day' => Yii::t('app', 'Ngày báo cáo'),
            'avg_time' => Yii::t('app', 'Thời gian trung bình(ms)'),
            'total_trans' => Yii::t('app', 'Tổng giao dịch'),
            'failure_trans' => Yii::t('app', 'Giao dịch lỗi'),
            'success_rate' => Yii::t('app', 'Tỷ lệ thành công(%)'),
            'failure_rate' => Yii::t('app', 'Tỷ lệ lỗi(%)'),
            'avg_bandwidth' => Yii::t('app', 'Băng thông trung bình(Kbps)'),
        ];
    }
}
