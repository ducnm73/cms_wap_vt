<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_sms_mt_his".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $message
 * @property string $mo_his_id
 * @property string $sent_time
 * @property integer $status
 * @property string $receive_time
 * @property string $channel
 * @property integer $process_id
 */
class VtSmsMtHisDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_sms_mt_his';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'sent_time', 'receive_time'], 'required'],
            [['message'], 'string'],
            [['mo_his_id', 'status', 'process_id'], 'integer'],
            [['sent_time', 'receive_time'], 'safe'],
            [['msisdn'], 'string', 'max' => 15],
            [['channel'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'message' => Yii::t('app', 'Message'),
            'mo_his_id' => Yii::t('app', 'Mo His ID'),
            'sent_time' => Yii::t('app', 'Sent Time'),
            'status' => Yii::t('app', 'Status'),
            'receive_time' => Yii::t('app', 'Receive Time'),
            'channel' => Yii::t('app', 'Channel'),
            'process_id' => Yii::t('app', 'Process ID'),
        ];
    }
}
