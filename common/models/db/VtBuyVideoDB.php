<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_buy_video".
 *
 * @property string $id
 * @property string $msisdn
 * @property string $user_id
 * @property string $video_id
 * @property string $created_at
 */
class VtBuyVideoDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_buy_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'user_id', 'video_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'user_id' => 'User ID',
            'video_id' => 'Video ID',
            'created_at' => 'Created At',
        ];
    }
}
