<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_cp".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $cp_code
 * @property integer $is_active
 * @property string $percentage_revenue
 * @property integer $view_report_sub
 * @property integer $type
 */
class VtCpDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_cp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_active', 'percentage_revenue', 'view_report_sub', 'type'], 'integer'],
            [['name', 'cp_code'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'cp_code' => Yii::t('app', 'Cp Code'),
            'is_active' => Yii::t('app', 'Is Active'),
            'percentage_revenue' => Yii::t('app', 'Percentage Revenue'),
            'view_report_sub' => Yii::t('app', 'View Report Sub'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
