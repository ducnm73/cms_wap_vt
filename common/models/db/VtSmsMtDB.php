<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_sms_mt".
 *
 * @property string $id
 * @property string $mo_his_id
 * @property string $msisdn
 * @property string $message
 * @property string $receive_time
 * @property integer $status
 * @property string $channel
 * @property integer $process_id
 * @property string $process_start_time
 * @property integer $retry
 */
class VtSmsMtDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_sms_mt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mo_his_id', 'status', 'process_id', 'retry'], 'integer'],
            [['msisdn'], 'required'],
            [['message'], 'string'],
            [['receive_time', 'process_start_time'], 'safe'],
            [['msisdn'], 'string', 'max' => 15],
            [['channel'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mo_his_id' => Yii::t('app', 'Mo His ID'),
            'msisdn' => Yii::t('app', 'Msisdn'),
            'message' => Yii::t('app', 'Message'),
            'receive_time' => Yii::t('app', 'Receive Time'),
            'status' => Yii::t('app', 'Status'),
            'channel' => Yii::t('app', 'Channel'),
            'process_id' => Yii::t('app', 'Process ID'),
            'process_start_time' => Yii::t('app', 'Process Start Time'),
            'retry' => Yii::t('app', 'Retry'),
        ];
    }
}
