<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_promotion_winner".
 *
 * @property string $id
 * @property string $user_id
 * @property string $video_id
 * @property integer $is_active
 * @property integer $priority
 */
class VtPromotionWinnerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_promotion_winner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'required'],
            [['id', 'user_id', 'video_id', 'is_active', 'priority'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'video_id' => 'Video ID',
            'is_active' => 'Is Active',
            'priority' => 'Priority',
        ];
    }
}
