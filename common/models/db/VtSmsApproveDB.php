<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vt_sms_approve".
 *
 * @property string $id
 * @property string $sms_code
 * @property string $sms_content
 * @property string $sms_content_draft
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $note
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class VtSmsApproveDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_sms_approve';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['sms_code', 'note'], 'string', 'max' => 255],
            [['sms_content', 'sms_content_draft'], 'string', 'max' => 1000],
            [['sms_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sms_code' => Yii::t('app', 'Sms Code'),
            'sms_content' => Yii::t('app', 'Sms Content'),
            'sms_content_draft' => Yii::t('app', 'Sms Content Draft'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'note' => Yii::t('app', 'Note'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
