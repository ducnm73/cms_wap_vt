<?php

namespace common\models;

use Yii;

class VtUserChangeInfoBase extends \common\models\db\VtUserChangeInfoDB {

    const WAIT_APPROVE = 0;
    const APPROVED = 1;
    const DECLINED = 2;

    public function getStatusString() {
        switch ($this->status) {
            case self::WAIT_APPROVE:
                return Yii::t('backend', 'Chờ duyệt');
            case self::APPROVED:
                return Yii::t('backend', 'Đã duyệt');
            case self::DECLINED:
                return Yii::t('backend', 'Không duyệt');
        }
    }

    public static function getUserById($id) {
        $query = self::find()
            ->where([
                'user_id' => $id
            ]);

        return $query->one();
    }

}