<?php

namespace common\models;

use Yii;

class VtHistoryViewBase {

    const TYPE_VOD = 'VOD';
    const TYPE_LIVETV = 'LIVETV';
    const TYPE_FILM = 'FILM';
    const HISTORY_VIEW = 'HISTORY';
    const WATCH_CONTINUE = 'CONTINUE';
    const EXPIRED_LIST = 1296000; // 31 ngay
    const EXPIRED_ITEM = 1296000;

    public function attributes() {
        return ['_id', 'id', 'user_id', 'msisdn', 'item_id', 'time', 'content_type', 'duration', 'content', 'request_time', 'parent_id', 'category_id'];
    }

    /**
     * Khoi tao cache
     * @return \yii\caching\Cache
     */
    public static function getConnectCache($key) {
        $redisCache = \Yii::$app->cache;
        $history = $redisCache->get($key);
        return $history;
    }

    public function insertContinue($userId = 0, $msisdn, $video, $time, $parentId = 0) {
        $keyHistoryContinue = self::generaKey($msisdn, $userId, $video['type'], null, self::WATCH_CONTINUE);
        self::insertStr($msisdn, $userId, $video['type'], $keyHistoryContinue, $video['id'], self::EXPIRED_LIST);
    }

    public function insertHistory($userId = 0, $msisdn, $video, $time, $parentId = 0) {
        $keyHistoryItem = self::generaKey($msisdn, $userId, $video['type'], $video['id'], self::HISTORY_VIEW);
        $keyHistoryContinue = self::generaKey($msisdn, $userId, $video['type'], null, self::WATCH_CONTINUE);
//        $keyHistoryType = self::generaKey($msisdn, $userId, $video['type']);
        // Luu du lieu vao type
//        self::insertStr($msisdn, $userId, $video['type'], $keyHistoryType, $video['id']);


        $value = $time . '_' . $video['duration'];
        self::updateCache($keyHistoryItem, $value, self::EXPIRED_ITEM);

        self::insertStr($msisdn, $userId, $video['type'], $keyHistoryContinue, $video['id'], self::EXPIRED_LIST);
    }

    /**
     * Lay danh sach id da xem, sap xep theo thu tu xem moi nhat
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @param $type
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getHistoryByType($userId, $msisdn, $type, $limit = 5, $offset = 0) {
        $result = array();
        $keyHistoryType = self::generaKey($msisdn, $userId, $type);
        $history = self::getConnectCache($keyHistoryType);

        if (!empty($history)) {
            $content = str_replace(" ", ",", $history);
            $content = preg_replace('/(.)\\1+/', '$1', $content);
            if (self::endsWith($content, ",")) {
                $content = substr($content, 0, strlen($content) - 1);
            }
            if (self::startsWith($content, ",")) {
                $content = substr($content, 1, strlen($content));
            }

            $result = explode(",", $content);
            if (count($result) == 0) {
                $result = array($content);
            }
        }

//        array_reverse($result);// sap xep ban ghi moi nhat
        if (!empty($limit)) {
            array_slice($result, $offset, $limit);
        }
        return $result;
    }

    public static function getByCategory($userId, $msisdn, $limit, $offset = 0) {
        return self::getByUser($userId, $msisdn, $limit, $offset);
    }

    public static function getByUser($userId, $msisdn, $limit = null, $offset = 0, $type = null) {

        $result = array();
        if (!empty($type)) {

            $key = self::generaKey($msisdn, $userId, $type, 0, self::WATCH_CONTINUE);
            //var_dump($key);die;
            $history = self::getConnectCache($key);
            if ($history) {
                $content = str_replace(" ", "", $history);
                $result = explode(",", $content);
                if (count($result) == 0) {
                    $result = array($content);
                }
            }
        } else {
            $result = array_merge(
                    self::buildItem($result, $msisdn, $userId, self::TYPE_VOD), self::buildItem($result, $msisdn, $userId, self::TYPE_FILM)
            );
        }


        if (!empty($limit)) {
            array_slice($result, $offset, $limit);
        }
        $items = $result;
        return array_reverse($items);
    }

    public static function getTimeByIds($userId, $msisdn, $ids, $type) {
        $results = [];
        foreach ($ids as $key => $id) {

            $info = self::getByItemId($userId, $msisdn, $type, $id);
            if (!empty($info)) {
                $arr['itemId'] = $id;
                $arr['time'] = $info['time'];
                $arr['duration'] = $info['duration'];
                $results[] = $arr;
            } else {
                $arr['itemId'] = $id;
                $arr['time'] = 0;
                $arr['duration'] = 0;
                $results[] = $arr;
            }
        }
        return $results;
    }

    /**
     * Lay id da xem cua thue bao => update usage
     * @author phumx@viettel.com.vn
     * @param $msisdn
     * @param $type
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByItemId($userId, $msisdn, $type, $itemId) {
        $result = array();
        $key = self::generaKey($msisdn, $userId, $type, $itemId);
        $history = self::getConnectCache($key);

        if ($history) {
            $arr = explode("_", $history);
            if (count($arr) > 0) {
                $result = array(
                    'time' => isset($arr[0]) ? intval($arr[0]) : 0,
                    'duration' => isset($arr[1]) ? intval($arr[1]) : 0
                );
            }
        }
        return $result;
    }

    public static function generaKey($msisdn, $userId, $type, $itemId = 0, $prefix = self::HISTORY_VIEW) {
        if ($itemId > 0) {
            $key = $msisdn . '_' . $userId . '_' . $type . '_' . $itemId;
        } else {
            $key = $msisdn . '_' . $userId . '_' . $type;
        }
        if (!empty($prefix)) {
            $key = $prefix . '_' . $key;
        }

        return $key;
    }

    public static function updateCache($key, $value, $lifeTime = 0) {
        if (!empty($key)) {
            $redisCache = \Yii::$app->cache;
            $redisCache->set($key, $value, $lifeTime);
            return true;
        } else {
            return false;
        }
    }

    public static function insertStr($msisdn, $userId, $type, $key, $itemId, $lifetime = 0) {
        $history = self::getConnectCache($key);
        $space = "";
        $result = "";
        $value = $space . $itemId . $space;
        if ($history) {
            $result = self::formatStr($history, $value);

            $arrStr = count_chars($result, 1);
            $dem = array_key_exists(44, $arrStr) ? $arrStr[44] * 2 - 1 : 0;
            if ($dem >= \Yii::$app->params['historyView.limit']) {
                $firstItem = strchr($result, ",", true);
                $result = str_replace($firstItem . ',', "", $result);
                $keyItem = self::generaKey($msisdn, $userId, $type, trim($firstItem));
                self::deleteCache($keyItem);
            }
            if ($dem == 0) {
                $result = $value;
            }
        } else {
            $result = $value;
        }
        self::updateCache($key, $result, $lifetime);
    }

    public static function deleteCache($key) {
        if (!empty($key)) {
            $redisCache = \Yii::$app->cache;
            $redisCache->delete($key);
            return true;
        } else {
            return false;
        }
    }

    public static function formatStr($str, $value) {
        $search = str_replace($value, ",", $str);
        $result = str_replace(",,", "", $search);
        $result = str_replace("null", "", $result);

        $result = $result . "," . $value; // Loai bo ,,
        if (self::startsWith($result, ",")) {
            $result = substr($result, 1, strlen($result) - 1); // Loai bo , o dau
        }
        if (self::endsWith($result, ",")) {
            $result = substr($result, 0, strlen($result) - 1); // Loai bo , o cuoi
        }
        return $result;
    }

    public static function startsWith($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    private static function buildItem($arr, $msisdn, $userId, $type) {
        $key = self::generaKey($msisdn, $userId, self::TYPE_VOD, 0, self::WATCH_CONTINUE);
        $history = self::getConnectCache($key);

        $result = array();
        if ($history) {
            $content = str_replace(" ", "", $history);
            $items = explode(",", $content);
            if (count($items) > 0) {
                foreach ($items as $key => $item) {
                    //'msisdn', 'itemId', 'time', 'type', 'duration'
                    $arr['msisdn'] = $msisdn;
                    $arr['userId'] = $userId;
                    $arr['itemId'] = $item;
                    $arr['type'] = $type;
                    $info = self::getByItemId($userId, $msisdn, $type, $item);
                    if (!empty($info)) {
                        $arr['time'] = $info['time'];
                        $arr['duration'] = $info['duration'];
                        $result[] = $arr;
                    }
                }
            }
        }

        return $result;
    }

    public static function delete($userId, $msisdn, $itemIds, $type = 'VOD', $lifetime = 0) {
        $keyHistoryType = self::generaKey($msisdn, $userId, $type, 0, self::WATCH_CONTINUE);

        $history = str_replace(" ", "", self::getConnectCache($keyHistoryType));

        $historyArr = explode(',', $history);

        foreach ($itemIds as $id) {
            if (in_array($id, $historyArr)) {
                $keyItem = self::generaKey($msisdn, $userId, $type, trim($id));
                self::deleteCache($keyItem);
            }
        }

        $result = implode(',', array_diff($historyArr, $itemIds));

        self::updateCache($keyHistoryType, $result, $lifetime);
    }

    public static function deleteAll($userId, $msisdn, $type = 'VOD', $lifetime = 0) {
        $keyHistoryType = self::generaKey($msisdn, $userId, $type, 0, self::WATCH_CONTINUE);
        self::deleteCache($keyHistoryType);
    }

}
