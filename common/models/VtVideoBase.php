<?php

namespace common\models;

use cp\models\VtPlaylistItem;
use Yii;
use common\helpers\Utils;
use common\models\VtChannelBase;
use cornernote\linkall\LinkAllBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
//use common\models\LanguageConvert;

class VtVideoBase extends \common\models\db\VtVideoDB
{

    const ACTIVE = 1;
    const IN_ACTIVE = 0;
    const STATUS_JUST_SYNC = -1;
    const STATUS_TEMP = 0;
    const STATUS_DRAFT = 1;
    const STATUS_APPROVE = 2;
    const STATUS_DELETE = 3; #Admin disapprove
    const STATUS_REMOVE = 4; #User delete
    const MODE_PRIVATE = 0;
    const MODE_PUBLIC = 1;
    const CONVERT_STATUS_TEMP = -1;
    const CONVERT_STATUS_DRAFT = 0;
    const CONVERT_STATUS_SUCCESS = 1;
    const CONVERT_STATUS_CONVERTING = 2;
    const CONVERT_STATUS_FAIL_ORG = 3;
    const CONVERT_STATUS_FAIL_CLOUD = 4;
    const CONVERT_STATUS_FAIL_CONVERT = 5;
    const STATUS_FILE_INFO_FAIL = 6;
    const STATUS_SPLIT_FAIL = 7;
    const STATUS_MERGE_FAIL = 8;
    const STATUS_CHECK_FOR_MERGE = 9;
    const STATUS_WAIT_FOR_MERGING = 10;
    const STATUS_IS_MERGING = 11;
    const STATUS_DUR_DEVIATION = 12;
    const TYPE_VOD = 'VOD';
    const TYPE_FILM = 'FILM';
    const VOD_FILTER = 'VOD_FILTER';
    const MUSIC_FILTER = 'MUSIC_FILTER';
    const RECOMMEND_DYNAMIC = 1;
    const RECOMMEND_FIX = 2;

    const OUTSOURCE_DRAFT = 0;
    const OUTSOURCE_APPROVE = 1;
    const OUTSOURCE_DISAPPROVE = 2;
    const OUTSOURCE_REVIEWING = -1;

    const OUTSOURCE_DONT_NEED_APPROVE = 0;
    const OUTSOURCE_NEED_APPROVE = 1;


    public $topic_ids;

    public function getCategory()
    {
        return $this->hasOne(VtGroupCategoryBase::className(), ['id' => 'category_id']);
    }

    public function getFreePrice($id)
    {
        return  self::find()
            ->asArray()
            // ->select('is_free_price')
            ->select('*')
            ->from(self::tableName() . ' v')
            ->where(['v.id' => $id])->all();
    }

    public function getTopics()
    {
        return $this->hasMany(VtGroupTopicBase::className(), ['id' => 'topic_id'])
            ->viaTable(VtVideoTopicBase::tableName(), ['video_id' => 'id']);
    }

    public function getOutsource()
    {
        return $this->hasOne(AuthUser::className(), ['id' => 'outsource_review_by']);
    }


    public function setTopics($value)
    {
        return $value;
    }

    /**
     * Lay chi tiet du lieu theo type
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param string $type
     * @return query
     */
    public static function getDetail($id, $type = "", $isObject = false, $includeDraft = false)
    {
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage = Yii::$app->language;

        $query = self::find()
            ->select('v.*')
            ->from(self::tableName() . ' v')
            ->where([
                'v.id' => $id,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0
            ])//                ->andWhere('v.published_time <= :published_time', [':published_time' => date("Y-m-d H:i:s")])
        ;
        if ($includeDraft) {
            $query->andWhere(['v.status' => [self::STATUS_APPROVE, self::STATUS_DRAFT, self::STATUS_DELETE]]);
        } else {
            $query->andWhere(['v.status' => self::STATUS_APPROVE]);

        }

        if (!empty($type)) {
            $query->andWhere(['v.type' => $type]);
        }

        if ($mainLanguage == $currentLanguage) {
            // neu la mainLanguage
            if ($isObject) {
                // neu la object
                return $query->one();
            } else {
                // neu la mang
                return $query->asArray()->one();
            }
        } else {
            // neu khac main language
            if ($isObject) {
                //neu la object
                /* @var VtVideoBase $temp */
                $temp = $query->one();
                $multilang = $temp['multilang'];
                //var_dump($multilang);die;
                if (is_null($multilang)) {
                    // neu truong multi Language null
                    return $temp;
                }
                // neu multi Language khong null
                return LanguageConvert::convertFieldsToObject($multilang,
                    ['name','description','tag','seo_title','seo_keywords','slug', 'short_desc'],$currentLanguage,$temp);

            } else {
                // neu la mang
                $responData = [];
                $temp = $query->asArray()->one();
                $multilang = $temp['multilang'];
                if (is_null($multilang)) {
                    // neu truong multi Language null
                    return $temp;
                }

                // neu multilan khac null
                $responData=LanguageConvert::arrayExclude($temp,['name','description','tag','seo_title','seo_keywords','slug', 'short_desc']);
                $multiLangFieldPart = LanguageConvert::convertFieldsToArray($multilang,
                    ['name','description','tag','seo_title','seo_keywords','slug', 'short_desc'],$currentLanguage, $temp);

                return array_merge($responData,$multiLangFieldPart);
            }
        }
    }

    /**
     * @param $attributes
     * @param int $limit
     * @param null $offset
     * @return $this
     */
    public static function getVideosByIdsQuery($ids, $limit = 10, $offset = null, $type = self::TYPE_VOD, $filter_time = null, $duration = null, $order = '')
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel u', 'u.id=v.channel_id')
            ->where(['v.id' => $ids])
            ->andWhere('v.type = :type', [':type' => $type])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');


        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * @param $attributes
     * @param int $limit
     * @param null $offset
     * @return $this
     */
    public static function getByIdsQuery($filterType = '', $ids, $limit = 10, $offset = null, $type = self::TYPE_VOD)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel u', 'u.id=v.channel_id')
            ->where([
                'v.id' => $ids,
                'v.type' => 'VOD'
            ])
//                ->andWhere('v.type = :type', [':type' => $type])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                }
//                else {
//                    return $query->limit($limit);
//                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                }
//                else {
//                    return $query->limit($limit);
//                }
                break;
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * @author PhuMX
     * @param $userId
     * @param $limitc
     * @param $offset
     */
    public static function getVideosByUser($filterType = '', $userId, $limit, $offset, $type = 'VOD')
    {

        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->innerJoin('vt_channel u', 'u.id=v.channel_id')
            ->where([
                'v.channel_id' => $userId,
                'v.status' => self::STATUS_APPROVE,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
                'v.type' => $type
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => date('Y-m-d H:i:s')])
            ->orderBy('v.published_time DESC');


        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        //var_dump($filterType);die;//string(0) ""
        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }

        return $query;
        //Xử lý multilang trong file common\modules\v2\libs\VideoObj
    }


    /**
     * @author PhuMX
     * @param $userId
     * @param $limitc
     * @param $offset
     */
    public static function getAllVideosByUser($filterType = '', $userId, $limit, $offset, $type = 'VOD')
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, uf.status as feedback_status, uf.reject_reason as feedback_reject_reason')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel u', 'u.id=v.channel_id')
            ->leftJoin("vt_upload_feedback uf", "uf.video_id = v.id")
            ->where([
                'v.created_by' => $userId,
                'v.status' => [self::STATUS_APPROVE, self::STATUS_DRAFT],
//                'v.status' => [self::STATUS_APPROVE, self::STATUS_DRAFT, self::STATUS_DELETE],
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
                'v.type' => $type
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => date('Y-m-d H:i:s')])
            ->orderBy('v.published_time DESC');


        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }
        return $query;
    }

    /**
     * lisst video user comment
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getAllVideosCommentByUser($filterType = '', $userId, $limit, $offset, $type = 'VOD')
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn, cm.status as comment_status, cm.reason as comment_reject_reason')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->leftJoin("vt_comment cm", "cm.content_id = v.id")
            ->where([
                'v.created_by' => $userId,
                'v.status' => self::STATUS_APPROVE,
//                'v.status' => [self::STATUS_APPROVE, self::STATUS_DRAFT, self::STATUS_DELETE],
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
                'v.type' => $type
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => date('Y-m-d H:i:s')])
            ->orderBy('v.published_time DESC');

        $query->groupBy('content_id');
        $query->having("SUM(cm.content_id) > 1");
        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }

        return $query;
    }
    /**
     * Lay noi dung hot
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getHotVideo($limit = null, $offset = null)
    {
        //Sua dk lay danh sach video trang chu theo hot trong 24h
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where([
                'v.type' => self::TYPE_VOD,
                'v.is_hot' => 1,
                'v.status' => self::STATUS_APPROVE,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
                'u.status' => 1
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');
        //->orderBy('(v.published_time + v.is_hot *1000000) DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    /**
     * Lay noi dung goi y
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getRecommendVideoMixQuery($filterType = '', $limit = null, $offset = null)
    {
        $limit40Percent = ceil($limit * 40 / 100);
        $offset40Percent = ceil($offset * 40 / 100);

        $limit30Percent = ceil($limit * 30 / 100);
        $offset30Percent = ceil($offset * 30 / 100);

        $recommend1 = ArrayHelper::getColumn(self::getRecommendVideo(self::RECOMMEND_DYNAMIC, $limit40Percent, $offset40Percent)->all(), 'id');
        $recommend2 = ArrayHelper::getColumn(self::getRecommendVideo(self::RECOMMEND_FIX, $limit30Percent, $offset30Percent)->all(), 'id');
        $recommend3 = ArrayHelper::getColumn(VtVideoRecommendBase::getByActiveQuery($limit30Percent, $offset30Percent)->all(), 'video_id');


        $ids = [];
        for ($i = 0; $i < count($recommend1); $i++) {

            $j = $i * 3 + 1;

            $ids[$j] = $recommend1[$i];
        }

        for ($i = 0; $i < count($recommend2); $i++) {

            $j = $i * 3 + 2;

            $ids[$j] = $recommend2[$i];
        }

        for ($i = 0; $i < count($recommend3); $i++) {

            $j = $i * 3;

            $ids[$j] = $recommend3[$i];
        }

        ksort($ids);
        $ids = array_slice($ids, 0, $limit);

        if($ids) {
            return VtVideoBase::getByIdsQuery(self::VOD_FILTER, $ids, $limit)
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($ids)) . ')')]);
        } else {
            return VtVideoBase::getByIdsQuery(self::VOD_FILTER, $ids, $limit);
        }


    }


    public static function getRecommendVideo($recommend = 1, $limit = null, $offset = null)
    {

        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id,u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.is_recommend' => $recommend,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.is_no_copyright' => 0
                ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }

//        var_dump($query->createCommand()->rawSql); die;
        return $query;
    }


    /**
     * Lay noi dung goi y
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getRecommendVideoWithUser($filterType = '', $limit = null, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.is_recommend' => 1,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.is_no_copyright' => 0,
                    'u.status' => 1
                ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    /**
     * Lay noi dung goi y
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getNewVideo($filterType = '', $limit = null, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, cl.user_id, cl.bucket as user_bucket, cl.path as user_path, cl.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.is_no_copyright' => 0
                ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    /**
     * Cap nhat truong like_count tang hoac giam dua theo bien $increase
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateLikeCount($id, $increase)
    {
        if ($increase) {
            $rawQuery = "UPDATE vt_video SET like_count = IFNULL(like_count, 0) + 1 WHERE id=:id";
        } else {
            $rawQuery = "UPDATE vt_video SET like_count = IFNULL(like_count, 0) - 1 WHERE id=:id";
        }

        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->execute();
    }

    /**
     * Cap nhat truong like_count tang hoac giam dua theo bien $increase
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateDisLikeCount($id, $increase)
    {
        if ($increase) {
            $rawQuery = "UPDATE vt_video SET dislike_count = IFNULL(dislike_count, 0) + 1 WHERE id=:id";
        } else {
            $rawQuery = "UPDATE vt_video SET dislike_count = IFNULL(dislike_count, 0) - 1 WHERE id=:id";
        }

        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->execute();
    }

    /**
     * Cap nhat truong like_count tang hoac giam dua theo bien $increase
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $increase
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateLikeVsDisLikeCount($id, $like, $dislike)
    {

        $rawQuery = "UPDATE vt_video SET dislike_count = dislike_count + :dislike, like_count = like_count + :like  WHERE id=:id";


        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->bindValue(':like', $like)
            ->bindValue(':dislike', $dislike)
            ->execute();
    }

    /**
     * Query lay video lien quan theo category
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $categoryId
     * @param $limit
     * @param $offset
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRelatedByCategoryQuery($id, $categoryId, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.category_id' => $categoryId,
                    'u.status' => 1
                ])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->andWhere('v.id <> :id', [':id' => $id])
            ->orderBy('v.id DESC'); // thay updated_at = id vi chay qua cham

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }

        return $query;
    }

    public static function getNextRecordByCategoryQuery($currentPublishedTime, $categoryId, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.category_id' => $categoryId,
                    'u.status' => 1
                ])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->andWhere('v.published_time < :current_time', [':current_time' => $currentPublishedTime])
            ->orderBy('v.published_time DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }

        return $query;
    }

    /**
     * Lay video lien quan theo category
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $categoryId
     * @param $limit
     * @param $offset
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRelatedByCategory($id, $categoryId, $limit, $offset)
    {
        $query = self::getRelatedByCategoryQuery($id, $categoryId, $limit, $offset);

        return $query->all();
    }

    public static function getVideosByCategory($categoryId, $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(['v.category_id' => $categoryId])
            ->andWhere('v.type = :type', [':type' => self::TYPE_VOD])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    public static function getVideosFree($filterType = '', $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, cl.user_id, cl.channel_bucket as user_bucket, cl.channel_path as user_path, cl.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
            ->where(['v.price_play' => 0])
            ->andWhere('v.type = :type', [':type' => self::TYPE_VOD])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }

        return $query;
    }

    /**
     * Cap nhat thong tin sau khi upload
     * @author ducda2@viettel.com.vn
     * @param $id
     * @param $name
     * @param $description
     * @param $categoryId
     * @return int
     */
    public static function updateContentById($id, $name = null, $description = null, $categoryId = null, $status = null, $convertStatus = null, $reason = null, $isActive = null)
    {
        $updateArr = [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Yii::$app->user->identity->user_id
        ];

        if (isset($name)) {
            $updateArr = array_merge($updateArr, [
                'name' => $name,
                'slug' => Utils::removeSign($name),
                'name_slug' => Utils::removeSignOnly($name)
            ]);
        }

        if (isset($description)) {
            $updateArr = array_merge($updateArr, [
                'description' => $description,
                'description_slug' => Utils::removeSignOnly($description)
            ]);
        }

        if (isset($categoryId)) {
            $updateArr = array_merge($updateArr, ['category_id' => $categoryId]);
        }

        if (isset($status)) {
            $updateArr = array_merge($updateArr, ['status' => $status]);
        }

        if (isset($convertStatus)) {
            $updateArr = array_merge($updateArr, array('convert_status' => $convertStatus));
        }

        if (isset($reason)) {
            $updateArr = array_merge($updateArr, array('reason' => $reason));
        }

        if (isset($isActive)) {
            $updateArr = array_merge($updateArr, array('is_active' => $isActive));
        }

        return self::updateAll(
            $updateArr, 'id = :id', [
                ':id' => $id
            ]
        );
    }

    public static function updatePricePlayById($id, $pricePlay = null)
    {
        $updateArr = [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Yii::$app->user->identity->user_id
        ];

        if (isset($pricePlay)) {
            $updateArr = array_merge($updateArr, array('price_play' => $pricePlay));
        }

        return self::updateAll(
            $updateArr, 'id = :id', [
                ':id' => $id
            ]
        );
    }

    public function insertVideo($name, $description, $fileBucket, $filePath, $type, $cpId = null, $userId = null, $categoryId = 0, $convertStatus = -1,
                                $bucket = '', $path = '', $mode = self::MODE_PUBLIC, $tags = "", $cpCode = null, $outsourceNeedReview = self::OUTSOURCE_NEED_APPROVE, $status = self::STATUS_DRAFT, $channel_id = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->is_active = self::ACTIVE;
        $this->status = $status;
        $this->type = $type;
        $this->cp_id = $cpId;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        if ($userId) {
            $this->created_by = $userId;
            $this->updated_by = $userId;
        } else {
            $this->created_by = Yii::$app->user->identity->user_id;
            $this->updated_by = Yii::$app->user->identity->user_id;
        }
        $this->category_id = $categoryId;
        $this->file_bucket = $fileBucket;
        $this->file_path = $filePath;
        $this->slug = Utils::removeSign($name);
        $this->name_slug = Utils::removeSignOnly($name);
        $this->description_slug = Utils::removeSignOnly($description);
        $this->published_time = date('Y-m-d H:i:s');

        // -- Thumbnail
        $this->path = $path;
        $this->bucket = $bucket;

        if ($convertStatus == -1) {
            $this->convert_status = self::CONVERT_STATUS_TEMP;
        } else {
            $this->convert_status = $convertStatus;
        }

        $this->mode = $mode;
        $this->tag = $tags;
        $this->cp_id = $cpId;
        $this->cp_code = $cpCode;
        $this->outsource_need_review = $outsourceNeedReview;
        $this->channel_id = $channel_id;
        $this->price_play = 1;
        $this->save(false);
    }

    public static function findOneById($id)
    {
        $query = self::find()
            ->where(['id' => $id]);

        return $query->one();
    }

    /**
     * @author PHUMX
     * Kiem tra xem khach hang() co quyen chinh sua video ma minh da upload nen nua hay khong?
     */
    public static function checkCustomerPermissionEditVideo($userId, $videoId)
    {
        $video = self::find()->asArray()
            ->where(['id' => $videoId, 'created_by' => $userId])
            ->one();
        if (!$video) {
            return false;
        }
        if ($video['status'] == self::STATUS_DRAFT && $video['is_active'] == self::ACTIVE) {
            return true;
        } else {
            return false;
        }
    }

    public static function searchByName($q, $type = 'VOD', $limit = 10)
    {
        $query = self::find()
            ->asArray()
            ->select('id, name as text')
            ->where([
                'is_active' => self::ACTIVE,
                'status' => self::STATUS_APPROVE,
                'type' => $type
            ])
            ->andWhere(['like', 'name', $q])
            ->limit($limit);

        return $query->all();
    }

    public function getVideoStatusString()
    {
        switch ($this->status) {
            case self::STATUS_APPROVE:
                return Yii::t('backend', 'Phê duyệt');
            case self::STATUS_DELETE:
                return Yii::t('backend', 'Xóa');
            case self::STATUS_DRAFT:
                if (in_array($this->convert_status, [self::CONVERT_STATUS_DRAFT, self::CONVERT_STATUS_CONVERTING])) {
                    return Yii::t('backend', 'Chờ convert');
                } else if (in_array($this->convert_status, [self::CONVERT_STATUS_FAIL_CLOUD, self::CONVERT_STATUS_FAIL_CONVERT, self::CONVERT_STATUS_FAIL_ORG])) {
                    return Yii::t('backend', 'Convert lỗi');
                } else {
                    return Yii::t('backend', 'Chờ duyệt');
                }

            case self::STATUS_TEMP:
                return Yii::t('backend', 'Tạm');
            default:
                return false;
        }
    }

    /**
     * @param $videoId
     * @param int $increase
     * @return int
     * @throws Cap nhat so luot xem cua video
     */
    public static function updatePlayTimes($videoId)
    {
        $rawQuery = "UPDATE vt_video SET play_times = IFNULL(play_times, 0) + 1 WHERE id=:id";
        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $videoId)
            ->execute();
    }

    public static function countActiveVideoByUser($userId)
    {

        $query = self::find()
            ->where([
                'is_active' => self::ACTIVE,
                'status' => self::STATUS_APPROVE,
                'created_by' => $userId
            ]);

        return $query->count();
    }

    // dem so luong video trong channel
    public static function countActiveVideoByChannel($channelId)
    {
        $query = self::find()
            ->where([
                'is_active' => self::ACTIVE,
                'status' => self::STATUS_APPROVE,
                'channel_id' => $channelId
            ]);

        return $query->count();
    }

    public static function getDataSynNeedApprove($limit)
    {
        $query = self::find()
            ->where([
                'status' => self::STATUS_JUST_SYNC,
                'convert_status' => self::CONVERT_STATUS_SUCCESS
            ])
            ->andWhere(['not', ['syn_id' => null]])
            ->limit($limit);

        return $query->all();
    }

    public static function getAllVideoByTime($limit, $createdAt) {
        $query = self::find()
            ->where([
                'convert_status' => self::CONVERT_STATUS_SUCCESS
            ])
            ->andWhere(['not', ['syn_id' => null]])
            ->andWhere(['>=', 'created_at', $createdAt])
            ->limit($limit);

        return $query->all();
    }

    //Lay file convert cua video
    public function getConvertVideo($profileId = 2)
    {
        return VtConvertedFileBase::find()
            ->from('vt_converted_file v')
            ->where([
                'v.video_id' => $this->id,
                'v.profile_id' => $profileId
            ])
            ->one();
    }

    /**
     *
     * @param $ids
     * @param int $limit
     * @param null $offset
     * @return $this
     */
    public static function getFilmByIdsQuery($ids, $limit = 5, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, v.id as item_id')
            ->from(self::tableName() . ' v')
            ->where(['v.id' => $ids])
            ->andWhere('v.type = :type', [':type' => self::TYPE_FILM])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()]);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    /**
     * Lay ra toan bo Video theo Ids
     * @author ducda2@viettel.com.vn
     * @param $ids
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllVideosByIds($ids)
    {
        $query = self::find()
            ->asArray()
            ->select('v.id, v.name, v.type, u.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id = v.created_by')
            ->where(['v.id' => $ids]);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query->all();
    }


    public static function getAllVideosWithCategoryByIds($ids)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, c.name as category_name')
            ->from(self::tableName() . ' v')
            ->leftJoin(VtGroupCategoryBase::tableName() . ' c', 'c.id = v.category_id')
            ->where(['v.id' => $ids]);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query->all();
    }

    /**
     * @author PhuMX
     * @param $userId
     * @param $limitc
     * @param $offset
     * @param $order : NEW, MOSTVIEW
     */
    public static function getVideosByChannel($channelId, $limit, $offset, $order = 'NEW')
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id,u.id as channel_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->innerJoin('vt_channel u', 'u.id = v.channel_id')
            ->where(['v.channel_id' => $channelId])
            ->andWhere(['v.status' => self::STATUS_APPROVE])
            ->andWhere(['v.is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->andWhere(['v.type' => 'VOD']);

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        if ($order == 'NEW') {
            $query->orderBy('v.published_time DESC');
        } elseif ($order == 'MOSTVIEW') {
            $query->orderBy('v.play_times DESC');
        } elseif ($order == 'id') {
            $query->orderBy('v.id DESC');
        } elseif ($order == 'IDOLD') {
            $query->orderBy('v.id ASC');
        }else{
            $query->orderBy('v.published_time DESC');
        }

        return $query;
    }

    public static function getVideosByPlaylist($id, $limit = 10, $offset = null, $type = self::TYPE_VOD)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(VtUserPlaylistItemBase::tableName() . ' pi')
            ->leftJoin(self::tableName() . ' v', 'v.id = pi.item_id')
            ->leftJoin('vt_channel u', 'u.id=v.channel_id')
            ->where([
                'v.type' => $type,
                'v.status' => self::STATUS_APPROVE,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
                'pi.playlist_id' => $id
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('pi.id DESC ');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    public static function getMostViewOfChannel($channelId, $limit = 10, $offset = null, $type = self::TYPE_VOD)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->innerJoin('vt_channel u', 'u.id=v.channel_id')
            ->where([
                'v.type' => $type,
                'v.status' => self::STATUS_APPROVE,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
                'v.created_by' => $channelId,
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.play_times DESC');


        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    public static function getNewOfChannel($channelId, $limit = 10, $offset = null, $type = self::TYPE_VOD, $orderDesc = true)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->innerJoin('vt_channel u', 'u.id=v.channel_id')
            ->where([
                'v.type' => $type,
                'v.status' => self::STATUS_APPROVE,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0,
            ])
            ->andWhere('v.channel_id IN ('.$channelId.')')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()]);

        if($orderDesc=='MOSTVIEW'){
            $query->orderBy('v.play_times DESC');
        }
        if($orderDesc=='NEW'){
            $query->orderBy('v.published_time DESC');
        }
        if($orderDesc=='OLD'){
            $query->orderBy('v.published_time ASC');
        }


        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

    public static function updateMedia($id, $name = null, $description = null, $tag = null, $mode, $category, $channel_id)
    {
        $updateArr = [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Yii::$app->user->identity->user_id
        ];

        if (isset($name)) {
            $updateArr = array_merge($updateArr, [
                'name' => $name,
                'slug' => Utils::removeSign($name),
                'name_slug' => Utils::removeSignOnly($name)
            ]);
        }

        if (isset($description)) {
            $updateArr = array_merge($updateArr, [
                'description' => $description,
                'description_slug' => Utils::removeSignOnly($description)
            ]);
        }

        if (isset($category)) {
            $updateArr = array_merge($updateArr, ['category_id' => $category]);
        }

        if (isset($channel_id)) {
            $updateArr = array_merge($updateArr, ['channel_id' => $channel_id]);
        }

        if (isset($tag)) {
            $updateArr = array_merge($updateArr, ['tag' => $tag]);
        }

        if (isset($mode)) {
            $updateArr = array_merge($updateArr, ['mode' => $mode]);
        }

        return self::updateAll(
            $updateArr, 'id = :id', [
                ':id' => $id
            ]
        );
    }

    /**
     * Lay noi dung video ma khach hang upload, sap xep theo luot xem
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getMostViewByCustomer($createdAt = null, $limit = null, $offset = null, $tableName = 'vt_video')
    {

        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from($tableName . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.is_no_copyright' => 0
                ])
            ->andWhere('v.syn_id is null')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.play_times DESC');

        if ($createdAt) {
            $query->andWhere('v.created_at >= :createdAt', [':createdAt' => $createdAt]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * Lay  video moi nhat ma khach hang upload
     * @param null $limit
     * @param null $offset
     * @return $this
     */
    public static function getNewByCustomer($createdAt = null, $limit = null, $offset = null, $tableName = 'vt_video')
    {

        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from($tableName . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.is_no_copyright' => 0
                ])
            ->andWhere('v.syn_id is null')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        if ($createdAt) {
            $query->andWhere('v.created_at >= :createdAt', [':createdAt' => $createdAt]);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    public static function getVideoByCategory($categoryid = null, $type = null, $offset = null, $limit = null)
    {

        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel u', 'u.id=v.created_by')
            ->where(
                [
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.is_no_copyright' => 0
                ]);

        if (!empty($type)) {
            $query->andWhere("v.type = :type", [":type" => $type]);
        }
        if (!empty($categoryid)) {
            $query->andWhere("v.category_id = :category_id ", [':category_id' => $categoryid]);
        }

        $query->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    public static function getVideosByCate($cateId, $limit = 10, $offset = 0, $order = 'NEW', $isGroupChannel = false)
    {

        $query = self::find()
            ->asArray()
            ->select('v.*, cl.user_id, cl.channel_bucket as user_bucket, cl.channel_path as user_path, cl.full_name')            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
            ->where(['v.category_id' => $cateId])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->andWhere('v.type = :type', [':type' => 'VOD']);

        if ($isGroupChannel) {

            $queryTmp = self::find()
                ->asArray()
                ->select('v.*, cl.user_id, cl.channel_bucket as user_bucket, cl.channel_path as user_path, cl.full_name')            ->from(self::tableName() . ' v')
                ->leftJoin('vt_user u', 'u.id=v.created_by')
                ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
                ->where(['v.category_id' => $cateId])
                ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
                ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
                ->andWhere('v.is_no_copyright=0')
                ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
                ->andWhere('v.published_time >= :published_time2', [':published_time2' => date("Y-m-d 00:00:00", time() - 7776000)])
                ->andWhere('v.type = :type', [':type' => 'VOD'])
                ->groupBy('v.created_by')
                ->orderBy("mp DESC")
                ->limit($limit)
                ->offset($offset);

            $arrChannelVideo = $queryTmp->all();
            //Fake empty result
            $query=self::find()->asArray()->where(['id' => 0]);

            if($arrChannelVideo){
                $query = self::find()
                    ->asArray()
                    ->select('v.*, cl.user_id, cl.channel_bucket as user_bucket, cl.channel_path as user_path, cl.full_name')            ->from(self::tableName() . ' v')
                    ->leftJoin('vt_user u', 'u.id=v.created_by')
                    ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
                    ->where(['v.category_id' => $cateId])
                    ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE]);

                foreach ($arrChannelVideo as $tmpVideo) {
                    $strTmpVideo[] = "(v.created_by = " . $tmpVideo['created_by'] . " AND v.published_time = '" . $tmpVideo['mp'] . "' )";
                    $strTmpCreatedBy[] = $tmpVideo['created_by'];
                }
                $queryOR = implode(" OR ", $strTmpVideo);

                if ($queryOR) {
                    $query->andWhere($queryOR);
                }

                $query->groupBy("v.created_by");
                //FAKE CACHE (limit offset for VideoObj)
                $query->limit(1000+$limit+$offset);
                $query->offset(0);
                $query->orderBy("v.published_time DESC");
                //$query->orderBy([new Expression('FIELD (created_by, ' . implode(',', $strTmpCreatedBy) . ')')]);

            }

            return $query;

        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        if ($order == 'NEW') {
            $query->orderBy('v.published_time DESC');
        } elseif ($order == 'MOSTVIEW') {
            $query->orderBy('v.play_times DESC');
        } elseif ($order == 'MOSTTRENDING') {
            $query->andWhere('v.view_time <= :view_time', [':view_time' => Utils::currentCacheTime()])
                ->andWhere('v.view_time >= :bview_time', [':bview_time' => Utils::dateSub(false, "7 day")])
                ->orderBy('v.play_times DESC');
        }


        return $query;
    }

    public static function getRelatedsGAQuery( $categoryId, $limit, $offset,$distinctId=0)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->where(
                [
                    'v.type' => self::TYPE_VOD,
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.category_id' => $categoryId,
                    'u.status' => 1
                ])
            ->andWhere('v.id <> :id', [':id'=>$distinctId])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.play_times DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }

        return $query;
    }

    public static function getVideoIdInCategoryQuery($categoryId, $limit, $offset = 0, $videoTime = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.id')
            ->from(self::tableName() . ' v')
            ->where(
                [
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.category_id' => $categoryId
                ])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->andWhere('v.published_time <= :video_time', [':video_time' => $videoTime])
            ->orderBy('v.published_time DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    public static function getVideoIdInChannelQuery($userId, $limit, $offset = 0, $videoTime = null)
    {

        $query = self::find()
            ->asArray()
            ->select('v.id')
            ->from(self::tableName() . ' v')
            ->where(
                [
                    'v.status' => self::STATUS_APPROVE,
                    'v.is_active' => self::ACTIVE,
                    'v.created_by' => $userId
                ])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->andWhere('v.published_time <= :video_time', [':video_time' => $videoTime])
            ->orderBy('v.published_time DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }
        return $query;
    }

    public static function getVideoRelatedQuery($distinctVideo, $categoryId, $userId, $limit, $offset = 0, $videoTime = null)
    {
        $maxRelatedVideo = (Yii::$app->params['max.related.video']) ? Yii::$app->params['max.related.video'] : 50;
        $arrCateVideo = self::getVideoIdInCategoryQuery($categoryId, $maxRelatedVideo, 0, $videoTime)->all();
        $arrChannelVideo = self::getVideoIdInChannelQuery($userId, $maxRelatedVideo, 0, $videoTime)->all();

        $arrMerger = [];
        $cCate = 0;
        $cUser = 0;

        //Lay video theo ty le 6:4
        for ($i = 0; $i < (count($arrCateVideo) + count($arrChannelVideo)); $i++) {
            if ($i % 10 <= 5 && $arrCateVideo[$cCate]) {
                $arrMerger[$i] = $arrCateVideo[$cCate]["id"];
                $cCate++;
            } else {
                $arrMerger[$i] = $arrChannelVideo[$cUser]["id"];
                $cUser++;
            }
        }
        $arrMerger = array_unique($arrMerger);
        if (($key = array_search($distinctVideo, $arrMerger)) !== false) {
            unset($arrMerger[$key]);
        }
        $arrSlide = array_slice($arrMerger, $offset, $limit);
//        var_dump($arrSlide);die;

        if($arrSlide){
            $query = self::find()
                ->asArray()
                ->select('v.*, vc.user_id, vc.bucket as user_bucket, vc.path as user_path, vc.full_name')
                ->from(self::tableName() . ' v')
               // ->leftJoin('vt_user u', 'u.id=v.created_by')
                ->leftJoin('vt_channel vc', 'vc.id=v.channel_id')
                ->where(['v.id' => $arrSlide])
                ->andWhere('v.type = :type', [':type' => 'VOD'])
                ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
                ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
                ->andWhere('v.is_no_copyright=0')
                ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
                ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($arrSlide)) . ')')]);
        }else{
            $query = self::find()
                ->asArray()
                ->select('v.*, vc.user_id, vc.bucket as user_bucket, vc.path as user_path, vc.full_name')
                ->from(self::tableName() . ' v')
                ->leftJoin('vt_channel vc', 'vc.id=v.channel_id')
                ->where('1=2');

        }
        return $query;
    }

    public static function getVideoHomePage($limit, $isX4 = false)
    {
        //var_dump($isX4);die;//int(4)//bool(true)
        //Lay danh sach de xuat
        $recommendIds = [];
        //$categories = VtGroupCategoryBase::getAllHotCategories(Yii::$app->params['category.hot.limit']);
        //$categoryIds = ArrayHelper::getColumn($categories, 'id');
        //$recommendIds = ArrayHelper::getColumn(VtVideoHotBase::getByCategoryIdsAndActive($categoryIds, $limit, 0), 'video_id');

        //Yii::info("[WAP_HOME_PAGE]VIDEO RECOMMEND:". implode(",", $recommendIds));

        //Lay danh sach video moi
        /*
        $videoNew = self::find()
            ->asArray()
            ->select('v.id')
            ->from(self::tableName() . ' v')
            ->where(['not in', 'v.id',  $recommendIds])
            ->andWhere('v.type = :type', [':type' => 'VOD'])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy("v.published_time DESC")
            ->limit($limit)
            ->all();
        $newIds = ArrayHelper::getColumn($videoNew, 'id');

        Yii::info("[WAP_HOME_PAGE]VIDEO NEW:". implode(",", $newIds));
        */

        $x = ($isX4) ? 4 : 2;
        //var_dump($x);die;//int(4)
        //Lay danh sach video moi
        $videoHot = self::find()
            ->asArray()
            ->select('v.*, cl.user_id, cl.bucket as user_bucket, cl.path as user_path, cl.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
            //->where(['not in', 'v.id',  array_merge($recommendIds, $newIds)])
            ->where(['not in', 'v.id', $recommendIds])
            ->andWhere('v.type = :type', [':type' => 'VOD'])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_hot = 1')
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy(new Expression('rand()'))
            ->limit($limit * $x)
            ->all();

        $hotIds = ArrayHelper::getColumn($videoHot, 'id');
        Yii::info("[WAP_HOME_PAGE]VIDEO HOT:" . implode(",", $hotIds));
        // $arrGet = array_unique(array_merge($hotIds, $recommendIds, $newIds));
        $arrGet = array_unique(array_merge($hotIds, $recommendIds));

        Yii::info("[WAP_HOME_PAGE]VIDEO HOME RETURN:" . implode(",", $arrGet));

        //Set redis cache
        Yii::$app->cache->set("VIDEO_HOMEPAGE_IDS", implode(",", $arrGet));

        $result = self::find()
            ->asArray()
            ->select('v.*, cl.user_id, cl.bucket as user_bucket, cl.path as user_path, cl.full_name')            ->from(self::tableName() . ' v')
            ->leftJoin('vt_user u', 'u.id=v.created_by')
            ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
            ->from(self::tableName() . ' v')
            ->where(['v.id' => $arrGet])
            ->andWhere('v.type = :type', [':type' => 'VOD'])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
//            ->orderBy([new Expression('FIELD (v.id, ' . implode(',', array_filter($arrGet)) . ')')]);
            ->orderBy(new Expression('rand()'));

        return $result;
    }

    public static function getHotVideoWithOutIds($ids, $limit = null, $offset = null)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, u.user_id, u.bucket as user_bucket, u.path as user_path, u.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel u', 'u.id=v.channel_id')
            ->where([
                'v.type' => self::TYPE_VOD,
                'v.is_hot' => 1,
                'v.status' => self::STATUS_APPROVE,
                'v.is_active' => self::ACTIVE,
                'v.is_no_copyright' => 0
            ])
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');
        if ($ids) {
            $query->andWhere(['not in', 'v.id', $ids]);
        }
        if (!empty($offset)) {
            $query->offset($offset);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        return $query;
    }


    public static function getOutsourceReviewQuery($beginTime, $endTime, $status = null)
    {
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' v')
            ->select('v.*, u.msisdn, u.full_name, a.username')
            ->leftJoin(VtUserBase::tableName() . ' u', 'u.id = v.created_by')
            ->leftJoin(AuthUser::tableName() . ' a', 'a.id = v.outsource_review_by')
            ->andWhere([
                'v.outsource_need_review' => self::OUTSOURCE_NEED_APPROVE,
                'v.outsource_status' => [self::OUTSOURCE_APPROVE, self::OUTSOURCE_DISAPPROVE]
            ])
            ->andWhere('outsource_review_at between :beginTime and :endTime', [':beginTime' => $beginTime, ':endTime' => $endTime]);

        if (isset($status) && $status != '') {
            $query->andWhere(['outsource_status' => $status]);
        }

        $query->orderBy('v.outsource_review_at DESC');

        return $query;
    }


    public static function getOutsourceTotal($beginTime, $endTime, $status = null)
    {
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' v')
            ->select('SUM(case when outsource_status = 1 then 1 else 0 end) as approve_count, SUM(case when outsource_status = 2 then 1 else 0 end) as disapprove_count, SUM(case when outsource_status = 1 then duration else 0 end) as approve_duration, SUM(case when outsource_status = 2 then related_id else 0 end) as disapprove_duration')
            ->leftJoin(VtUserBase::tableName() . ' u', 'u.id = v.created_by')
            ->leftJoin(AuthUser::tableName() . ' a', 'a.id = v.outsource_review_by')
            ->andWhere([
                'v.outsource_need_review' => self::OUTSOURCE_NEED_APPROVE,
                'v.outsource_status' => [self::OUTSOURCE_APPROVE, self::OUTSOURCE_DISAPPROVE]
            ])
            ->andWhere('outsource_review_at between :beginTime and :endTime', [':beginTime' => $beginTime, ':endTime' => $endTime]);

        if (isset($status) && $status != '') {
            $query->andWhere(['outsource_status' => $status]);
        }

        return $query->one();
    }

    public static function updateTempToDraft24H($userId){
        self::updateAll(
            ['status'=>1],
            'status = :status and created_by = :created_by and created_at>= :created_at',
            [":status"=>0, ":created_by"=>$userId, ":created_at"=>date("Y-m-d H:i:s", strtotime("-1 day")) ]);
    }

    public static function countVideoByCp($beginTime, $endTime) {
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' v')
            ->select('v.cp_id, v.cp_code,count(v.id) as num')
            ->andWhere('v.created_at between :beginTime and :endTime', [':beginTime' => $beginTime, ':endTime' => $endTime])
            ->andWhere(['v.status' => self::STATUS_DRAFT])
            ->groupBy('v.cp_id')
        ;
        return $query->all();
    }

    public static function getAllStatus()
    {
        return [
            '0' => 'Tạm',
            '1' => 'Chờ phê duyệt',
            '2' => 'Đã phê duyệt',
            '3' => 'Quản trị viên từ chối/xóa',
            '4' =>'Khách hàng tự xóa'
        ];
    }

    public function getStatusName() {
        switch ($this->status) {
            case 0:
                return 'Tạm';
            case 1:
                return 'Chờ phê duyệt';
            case 2:
                return 'Đã phê duyệt';
            case 3:
                return 'Quản trị viên từ chối/xóa';
            case 4:
                return 'Khách hàng tự xóa';
            case null:
                return 'NA';
            default:
                return $this->status;
        }
    }

    public function getHashtags() {
        return $this->hasMany(self::className(), ['id' => 'id_tag'])->viaTable(VtVideoTagBase::className(), ['id_video' => 'id']);
    }

    public static function updateChannelId($id, $channel_id)
    {
        $updateArr = [
            'channel_id' => $channel_id
        ];

        return self::updateAll(
            $updateArr, 'id = :id', [
                ':id' => $id
            ]
        );
    }

    public static function getAllvideo()
    {
        $query = self::find()
            ->asArray()
            ->from(self::tableName() . ' v')
            ->where(['v.channel_id' => null]);

        return $query->all();
    }
    public static function getVideosPaid($filterType = '', $limit, $offset)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*, cl.user_id, cl.channel_bucket as user_bucket, cl.channel_path as user_path, cl.full_name')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_channel cl', 'cl.id=v.channel_id')
            ->where(['<>','v.price_play', 0])
            ->andWhere('v.type = :type', [':type' => self::TYPE_VOD])
            ->andWhere('v.status = :status', [':status' => self::STATUS_APPROVE])
            ->andWhere('v.is_active = :is_active', [':is_active' => self::ACTIVE])
            ->andWhere('v.is_no_copyright=0')
            ->andWhere('v.published_time <= :published_time', [':published_time' => Utils::currentCacheTime()])
            ->orderBy('v.published_time DESC');

        if (!empty($offset)) {
            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        switch ($filterType) {
            case self::MUSIC_FILTER:
                $filterList = trim(VtConfigBase::getConfig('music.category.list'));
                //Neu khong cau hinh the loai thi khong tra ve ket qua
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
            case self::VOD_FILTER:
                $filterList = trim(VtConfigBase::getConfig('video.category.list'));
                if ($filterList) {
                    $query = $query->andWhere(['category_id' => explode(",", $filterList)]);
                } else {
                    return $query->limit(0);
                }
                break;
        }
        return $query;
    }
    public static function triggerComment($id,$status)
    {
        $rawQuery = "UPDATE vt_video SET can_comment=:status WHERE id = :id";
        return Yii::$app->db->createCommand($rawQuery)
            ->bindValue(':id', $id)
            ->bindValue(':status', $status)
            ->execute();

    }
    public static function getVideoByCommentId($id)
    {
        $query = self::find()
            ->asArray()
            ->select('v.*')
            ->from(self::tableName() . ' v')
            ->leftJoin('vt_comment vc', 'v.id=vc.content_id')
            ->where(['vc.id' => $id]);

        return $query->one();
    }
}
