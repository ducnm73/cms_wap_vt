<?php

namespace common\models;

use Yii;

class VtCommentLikeBase extends \common\models\db\VtCommentLikeDB
{

    /**
     * Lay ra thong tin yeu thich cua KH
     * @param $userId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getLike($userId, $commentId,$type)
    {
        $query = self::find()
            ->where([
                'user_id' => $userId,
                'comment_id' => $commentId
            ]);
        if($type != null){
            $query->andWhere(["like" =>$type]);
        }
        return $query->one();
    }

    public function insertLike($userId, $commentId, $contentId, $type,$typelike)
    {
        $this->user_id = $userId;
        $this->comment_id = $commentId;
        $this->content_id = $contentId;
        $this->type = $type;
        $this->like = $typelike;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->save(false);
    }

    /**
     * @param $userId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLikeIdWithUserId($userId, $contentId)
    {
        return self::find()
            ->asArray()
            ->select('comment_id')
            ->where([
                'user_id' => $userId,
                'content_id' => $contentId
            ])
            ->all();
    }
    public static function getLikeCount($commentId,$typelike)
    {
        $query = self::find()
            ->where([
                'like' => $typelike,
                'comment_id' => $commentId
            ])
            ->groupBy(['id'])
            ->count();
        return $query;
    }

}