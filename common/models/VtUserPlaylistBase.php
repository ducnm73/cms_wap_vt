<?php

namespace common\models;

use website\models\VtVideo;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class VtUserPlaylistBase extends \common\models\db\VtUserPlaylistDB {

    const TYPE_USER = 'USER';
    const ACTIVE = 1;
    const IN_ACTIVE = 0;
    const STATUS_APPROVE = 2;
    const STATUS_DELETE = 3;

    public function behaviors() {
        return [
                [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    public function insertItem($msisdn, $userId, $type, $name, $description, $isActive, $status) {
        $this->msisdn = $msisdn;
        $this->user_id = $userId;
        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
        $this->is_active = $isActive;
        $this->status = $status;
        $this->save(false);
    }

    public static function getByUserId($userId) {

        $query = self::find()
                ->asArray()
                ->from('vt_user_playlist up')
                ->where([
            'up.user_id' => $userId
        ]);

        return $query->one();
    }


    public static function getDetail($id, $isObject = false) {
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage=Yii::$app->language;

//        var_dump($num_count);die;
        $query = self::find()
                ->select('v.*, u.full_name as full_name, u.msisdn')
                ->from(self::tableName() . ' v')
                ->leftJoin('vt_user u', 'v.user_id = u.id')
                ->where([
            'v.id' => $id,
            'v.is_active' => self::ACTIVE,
            'v.status' => self::STATUS_APPROVE
        ]);

            if($mainLanguage==$currentLanguage) {
                //return $query->asArray()->one();
                if ($isObject) {
                    // neu la object
                    return $query->one();
                } else {
                    // neu la mang
                    return $query->asArray()->one();
                }
            }
            else{
//                $responData=[];
//                $temp = $query->asArray()->one();
//
//                $multilang = $temp['multilang'];
//                if(is_null($multilang)){
//                    // neu truong multi Language null
//                    return ($isObject)? $query->one(): $query->asArray()->one();
//                }
//
//                $responData['full_name'] = $temp['full_name'];
//                $responData['id'] = $temp['id'];
//                $responData['type'] = $temp['type'];
//                $responData['is_active'] = $temp['is_active'];
//                $responData['status'] = $temp['status'];
//                $responData['user_id'] = $temp['user_id'];
//                $responData['msisdn'] = $temp['msisdn'];
//                $responData['created_at'] = $temp['created_at'];
//                $responData['updated_at'] = $temp['updated_at'];
//                $responData['num_video'] = $temp['num_video'];
//                $responData['bucket'] = $temp['bucket'];
//                $responData['path'] = $temp['path'];
//                $responData['source_display'] = $temp['source_display'];
//                $responData['created_by'] = $temp['created_by'];
//                $responData['updated_by'] = $temp['updated_by'];
//                $responData['play_times'] = $temp['play_times'];
//                $responData['is_finish'] = $temp['is_finish'];
//                //$temp=preg_replace('/\s+/', '',$multilang);
//                $multilang_1 = json_decode($multilang);
//
//                $responData['name'] = LanguageConvert::getLang('name',$multilang_1);
//                $responData['description'] = LanguageConvert::getLang('description',$multilang_1);
//
//                if ($isObject) {
//                    //var_dump((object) $responData);die;
//                    return (object) $responData;
//                } else {
//                    return $responData;
//                }
//            }
//    }
                if ($isObject) {
                    //neu la object
                    /* @var VtVideoBase $temp */
                    $temp = $query->one();
                    $multilang = $temp['multilang'];
                    if (is_null($multilang)) {
                        // neu truong multi Language null
                        return $temp;
                    }
                    // neu multi Language khong null
                    return LanguageConvert::convertFieldsToObject($multilang,
                        ['name','description'],$currentLanguage,$temp);

                } else {
                    // neu la mang
                    $responData = [];
                    $temp = $query->asArray()->one();
                    $multilang = $temp['multilang'];
                    if (is_null($multilang)) {
                        // neu truong multi Language null
                        return $temp;
                    }

                    // neu multilan khac null
                    $responData=LanguageConvert::arrayExclude($temp,['name','description']);
                    $multiLangFieldPart = LanguageConvert::convertFieldsToArray($multilang,
                        ['name','description'],$currentLanguage, $temp);

                    return array_merge($responData,$multiLangFieldPart);
                }
            }
    }

    public static function getDetailWithUser($id, $isObject = false) {
        $mainLanguage = Yii::$app->params['mainLanguage'];
        $currentLanguage=Yii::$app->language;
        $query = self::find()
                ->select('v.*, u.id as user_id, u.bucket as user_bucket, u.path as user_path, u.full_name, u.msisdn')
                ->from(self::tableName() . ' v')
                ->leftJoin('vt_user u', 'u.id = v.user_id')
                ->where([
            'v.id' => $id,
            'v.is_active' => self::ACTIVE,
            'v.status' => self::STATUS_APPROVE
        ]);


        if($mainLanguage==$currentLanguage) {
            //return $query->asArray()->one();
            if ($isObject) {
                // neu la object
                return $query->one();
            } else {
                // neu la mang
                return $query->asArray()->one();
            }
        }
        else{
            if ($isObject) {
                //neu la object
                /* @var VtVideoBase $temp */
                $temp = $query->one();
                $multilang = $temp['multilang'];
                if (is_null($multilang)) {
                    // neu truong multi Language null
                    return $temp;
                }
                // neu multi Language khong null
                return LanguageConvert::convertFieldsToObject($multilang,
                    ['name','description'],$currentLanguage,$temp);

            } else {
                // neu la mang
                $responData = [];
                $temp = $query->asArray()->one();
                $multilang = $temp['multilang'];
                if (is_null($multilang)) {
                    // neu truong multi Language null
                    return $temp;
                }

                // neu multilan khac null
                $responData=LanguageConvert::arrayExclude($temp,['name','description']);
                $multiLangFieldPart = LanguageConvert::convertFieldsToArray($multilang,
                    ['name','description'],$currentLanguage, $temp);

                return array_merge($responData,$multiLangFieldPart);
            }
        }
    }

    public static function updateNumVideo($id) {
        $num_count = VtVideo::getVideosByPlaylist($id)->count();
        $rawQuery = "UPDATE vt_user_playlist SET num_video = :num  WHERE id = :id";


        return Yii::$app->db->createCommand($rawQuery)
                        ->bindValue(':id', $id)
                        ->bindValue(':num', $num_count)
                        ->execute();
    }

}
