<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class VtUserDrmLogBase extends \common\models\db\VtUserDrmLogDB {

    const DELETE = 0;

    const ACTION_CREATE = 1;
    const ACTION_DELETE = 2;


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function insertLog($msisdn, $userId, $deviceId, $networkDeviceId, $entitlementId, $deviceType, $action, $errorCode, $smsPackageId)
    {
        $this->msisdn = $msisdn;
        $this->user_id = $userId;
        $this->device_id = $deviceId;
        $this->network_device_id = $networkDeviceId;
        $this->entitlement_id = $entitlementId;
        $this->device_type = $deviceType;
        $this->action = $action;
        $this->error_code = $errorCode;
        $this->sms_package_id = $smsPackageId;
        return $this->save(false);
    }
}