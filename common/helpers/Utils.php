<?php

namespace common\helpers;

use Yii;
use common\libs\VtHelper;
use yii\helpers\Url;

class Utils {

    const MOBILE_SIMPLE = '0x';
    const MOBILE_GLOBAL = '258x';

    public static function hideMsisdn($msisdn){
        $coverText = "xxxx";
        return strlen($msisdn) > 0 && $msisdn > 0 ? substr_replace(Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL), $coverText, 5, strlen($coverText)) : null;
    }

    public static function hideMsisdnLast($msisdn){
        return strlen($msisdn) > 0 ? substr_replace(Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL), "xxx", -3) : null;
    }

    public static function truncateWords($str, $len, $withDot = true) {
        $strTmp = $str;
        if (strlen($str) > $len) {
            $strTmp = wordwrap($str, $len);
            $strTmp = substr($str, 0, strpos($strTmp, "\n"));

            if ($withDot) {
                $strTmp = $strTmp . "...";
            }
        }

        return $strTmp;
    }

    /**
     * Chuyen cac truong tuong ung sang gia tri ung voi ngon ngu tuong ung
     * @param $object \yii\db\ActiveRecord|array "doi tuong tuong ung"
     * @param $fields
     * @param $lang
     * @param string $multilangFieldName ten truong multilang
     * @author bachpv1
     * @since 02/2019
     * @return array
     */
    public static function convertMultilangFields(&$object, $fields = array(), $lang, $multilangFieldName = 'multilanguage', $thenUnset = true) {
        foreach ($fields as $field) {
            $multilangField = self::getMultilangField(is_object($object) ? $object->$multilangFieldName : $object[$multilangFieldName], $field, $lang);

            if($multilangField !== false) {
                if(is_object($object)) {
                    $object->$field = $multilangField;
                } else {
                    $object[$field] = $multilangField;
                }
            }
        }

        if($thenUnset && !is_object($object)) {
            unset($object[$multilangFieldName]);
        }
    }

    /**
     * Lay gia tri theo ngon ngu va truong tuong ung, neu khong ton tai thi tra ve rong
     * @param $multilang
     * @param $field truong tuong ung
     * @param $lang ngon ngu hien tai
     * @author bachpv1/
     * @since 02/2019
     * @return string
     */
    public static function getMultilangField($multilang, $field, $lang) {
        $jsonArray = json_decode($multilang, true);
        $key = $field . '_' . $lang;

        if($jsonArray && array_key_exists($key, $jsonArray)) {
            return $jsonArray[$key];
        } else {
            return false;
        }
    }

    public static function getMsisdnMocha($uuid) {
        $service_url = 'http://10.120.52.5:8699/privateApi/decrypt/v1/users';
        $curl = curl_init($service_url);
        $curl_post_data = array(
            'username' => 'mocha',
            'password' => 'mocha@%23$2014',
            'uuid' => $uuid
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
//        $decoded = json_decode($curl_response);
        Yii::info("[Mocha API] response = " . $curl_response);
        Yii::info("[Mocha API] jsonRes = " . $curl_response);
        if ($curl_response["errorCode"] == 200) {
            return $curl_response["data"]["msisdn"];
        }
        curl_close($curl);
        return "";
//        $decoded = json_decode($curl_response);
//        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
//            die('error occured: ' . $decoded->response->errormessage);
//        }
//        echo 'response ok!';
//        var_export($decoded->response);
    }

    /**
     * Lay ra so dien thoai theo format
     * @param $msisdn
     * @param $type
     * @return string
     */
    public static function getMobileNumber($msisdn, $type) {
        $spcStr = "/[^0-9]+/";
        //$msisdn = preg_replace($spcStr, '', $msisdn);
        if (strlen($msisdn) < 3)
            return $msisdn;
        if (empty($type))
            $type = self::MOBILE_SIMPLE;
        switch ($type) {
            case self::MOBILE_GLOBAL:
                if ($msisdn[0] == '0') {
					return '258' . substr($msisdn, 1);
				}
                else if (substr($msisdn, 0, 3) != '258') {
                    return '258' . $msisdn;
				}
                else {
                    return $msisdn;
				}
                break;
            case self::MOBILE_SIMPLE:
                if ($msisdn[0] != '0')
                    return '0' . substr($msisdn, 2);
                else
                    return $msisdn;
                break;
            default:
                return $msisdn;
        }
    }

    /**
     * Convert giay sang dang xx gio xx phut xx giay
     * @author ducda2@viettel.com.vn
     * @param int $hours
     * @param string $h
     * @param string $m
     * @param string $s
     * @param string $split
     * @return mixed
     */
    public static function convertSecondToDatetime($hours, $h = ' giờ', $m = ' phút', $s = ' giây', $split = ' ') {

        $hour = floor($hours / 3600);
        $minute = floor(($hours / 60) % 60);
        $second = $hours % 60;

        if (!empty($hour)) {
            if (empty($minute)) {
                return \Yii::t('app', '#hour#hp', array('#hour' => $hour, '#hp' => $split . $h . $split));
            }
            return \Yii::t('app', '#hour#hp#minute#mp', array('#hour' => $hour, '#hp' => $split . $h . $split, '#minute' => $minute, '#mp' => $split . $m));
        } else {
            if (!empty($minute)) {
                if (empty($second)) {
                    return \Yii::t('app', '#minute#mp', array('#minute' => $minute, '#mp' => $split . $m . $split));
                }
                return \Yii::t('app', '#minute#mp#second#sp', array('#minute' => $minute, '#mp' => $split . $m . $split, '#second' => $second, '#sp' => $split . $s));
            } else {
                return \Yii::t('app', '#second#sp', array('#second' => $second, '#sp' => $split . $s));
            }
        }
    }

    /**
     * Ham cong tru thoi gian
     * @param string $cycle
     * @param boolean $isSub
     * @return string
     */
    public static function dateSubOrAdd($cycle, $isSub = false) {
        //Lay thoi gian ngay hien tai
        $today = self::currentCacheTime();
        $sign = "+";
        if ($isSub) {
            $sign = "-";
        }
        $date_minus = strtotime(date("Y-m-d H:i:s", strtotime($today)) . " " . $sign . $cycle);
        $date_minus = strftime("%Y-%m-%d %H:%M:%S", $date_minus);
        return $date_minus;
    }

    /**
     * Lay ra thoi diem hien tai, theo block 5,10,15... phut de phuc vu caching
     * @return string
     */
    public static function currentCacheTime() {
        $minute = intval(date("i"));
        $cacheMinute = floor($minute / 5) * 5;
        if ($cacheMinute < 10) {
            $cacheMinute = '0' . $cacheMinute;
        }
        return date("Y-m-d H:") . $cacheMinute . ":00";
    }

    /**
     * Tao ra 1 chuoi ngau nhien duy nhat GUID (Universally Unique IDentifier)
     * @author huypv5@viettel.com.vn
     * @param string $dir
     * @param int $timestamp
     * @param string $ext
     * @return string
     */
    public static function generateGuid($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                substr($charid, 8, 4) . '-' .
                substr($charid, 12, 4) . '-' .
                substr($charid, 16, 4) . '-' .
                substr($charid, 20, 12);

            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    }

    public static function generateUniqueFileName($ext) {
        return sprintf('%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff)
            ) . '.' . strtolower($ext);
    }

    public static function current_millis() {
        list($usec, $sec) = explode(" ", microtime());
        return round(((float) $usec + (float) $sec) * 1000);
    }

    public static function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime();
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        if ($diff->y)
            return $diff->y . ' '.Yii::t('wap','năm trước');
        if ($diff->m)
            return $diff->m .  ' '.Yii::t('wap','tháng trước');
        if ($diff->w)
            return $diff->w . ' '.Yii::t('wap','tuần trước');
        if ($diff->d)
            return $diff->d . ' '.Yii::t('wap','ngày trước');
        if ($diff->h)
            return $diff->h . ' '.Yii::t('wap','giờ trước');
        if ($diff->i > 1)
            return $diff->i . ' '.Yii::t('wap','phút trước');

        return Yii::t('wap','vừa xong');
    }

    public static function secretMsisdn($msisdn) {

        $simpleMsisdn = self::getMobileNumber($msisdn, self::MOBILE_SIMPLE);

        return substr($simpleMsisdn, 0, -3) . '***';
    }

    private static $hasSign = array(
        "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ",
        "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ",
        "ì", "í", "ị", "ỉ", "ĩ",
        "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ",
        "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
        "ỳ", "ý", "ỵ", "ỷ", "ỹ",
        "đ",
        "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
        "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
        "Ì", "Í", "Ị", "Ỉ", "Ĩ",
        "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
        "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
        "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
        "Đ",
    );
    private static $noSign = array(
        "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
        "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
        "i", "i", "i", "i", "i",
        "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
        "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
        "y", "y", "y", "y", "y",
        "d",
        "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
        "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
        "i", "i", "i", "i", "i",
        "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
        "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
        "y", "y", "y", "y", "y",
        "d");
    private static $noSignOnly = array(
        "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
        "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
        "i", "i", "i", "i", "i",
        "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
        "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
        "y", "y", "y", "y", "y",
        "d",
        "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
        "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
        "I", "I", "I", "I", "I",
        "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
        "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
        "Y", "Y", "Y", "Y", "Y",
        "D");

    public static function removeSignOnly($str) {
        //Sign
        $str = str_replace(self::$hasSign, self::$noSignOnly, $str);
        $str = trim($str);
        return $str;
    }

    public static function removeSign($str) {
        //Sign
        $str = str_replace(self::$hasSign, self::$noSign, $str);
        //Special string
        $spcStr = "/[^A-Za-z0-9]+/";
        $str = preg_replace($spcStr, ' ', $str);
        $str = trim($str);
        //Space
        $str = preg_replace("/( )+/", '-', $str);
        return strtolower($str);
    }

    public static function format_number($number) {
        return number_format($number, 0, ',', '.');
    }

    public static function convertDay($day) {
        $arrTmp = explode(' ', $day);
        if (count($arrTmp) == 2) {
            $day = $arrTmp[1];
        }
        if ($day == 'day')
            return Yii::t('backend', 'Ngày');
        if ($day == 'week')
            return Yii::t('backend', 'Tuần');
        if ($day == 'month')
            return Yii::t('backend', 'Tháng');
    }

    public static function loadFiles() {
        return $_FILES;
    }

    public static function secondToDateString($seconds) {
        $msg = '';

        $t = $seconds / 60;

        if ($t > 0) {
            $h = intval($t / 60);
            $m = intval($t % 60);

            if ($m > 0) {
                $msg = $m . ' ' . Yii::t('backend', 'phút');
            }

            if ($h > 0) {
                if ($msg == '') {
                    $msg = $h . ' ' . Yii::t('backend', 'giờ');
                } else {
                    $msg = $h . ' ' . Yii::t('backend', 'giờ') . ' ' . $msg;
                }
            }
        }

        return $msg;
    }

    public static function generateOtp($length) {
        return self::generateVerifyCode($length, $length);
    }

    public static function generateVerifyCode($minLength = 4, $maxLength = 6, $chars = "0123456789") {

        $length = mt_rand($minLength, $maxLength);
        $size = strlen($chars) - 1;
        $code = '';
        for ($i = 0; $i < $length; ++$i) {
            $code .= $chars[mt_rand(0, $size)];
        }
        return $code;
    }

    public static function getSystemConfig($key, $setCache = true) {
        $cache = \Yii::$app->cache;
        if ($setCache) {
            // try retrieving $data from cache
            $data = $cache->get($key);
            if ($data === false) {
                $data = \Yii::$app->db->createCommand("SELECT config_value FROM vt_config WHERE config_key = :key LIMIT 1")
                    ->bindValue(':key', $key)
                    ->queryOne();

                if (!$data) {
                    $data = \Yii::$app->params[$key];
                } else {
                    $data = $data['config_value'];
                }
                $cache->set($key, $data, MobiTVRedisCache::CACHE_30MINUTE);
                return $data;
            }
            return $data;
        } else {
            $data = \Yii::$app->db->createCommand("SELECT config_value FROM vt_config WHERE config_key = :key  LIMIT 1")
                ->bindValue(':key', $key)
                ->queryOne();
            if (!$data) {
                return \Yii::$app->params[$key];
            } else {
                return $data['config_value'];
            }
        }
    }

    /**
     * Kiem tra so dien thoai hop le
     * @author ducda2@viettel.com.vn
     * @param $msisdn
     * @return mixed
     */
    public static function isValidMsisdn($msisdn) {


        //$msisdn = self::getMobileNumber($msisdn, self::MOBILE_GLOBAL);

        $pattern = \Yii::$app->params['msisdn.regx'];


        return preg_match($pattern, $msisdn);
    }

    /**
     * Chuyen duration  71000 ms  sang string (1:11)
     * @param $duration
     */
    public static function durationToStr($duration) {
        $arrStr = explode(":", gmdate("H:i:s", $duration));

        if ($arrStr[0] > 0) {
            return intval($duration / 3600) . ":" . $arrStr[1] . ":" . $arrStr[2];
        } else {
            return $arrStr[1] . ":" . $arrStr[2];
        }
    }

    public static function generatePath($extension = 'mp4') {
        $now = strtotime('now') . rand(0, 999);
        $fileName = self::generateUniqueFileName($extension);

        $childFolder = date('Y') . '/' . date('m') . '/' . date('d') . "/" . $now . "/" . $fileName;

        return $childFolder;
    }

    /**
     * Tao link redirect banner
     * @param $banner
     * @return mixed
     */
    public static function createLinkBanner($banner) {
        if (strlen($banner['href']) > 0)
            return $banner['href'];

        switch ($banner['type']) {
            case 'TOPIC':
                return Url::to(['default/load-more', 'id' => $banner['item_id'], 'slug' => self::sluggable($banner['name'])]);
            case 'CATEGORY':
                return Url::to(['default/load-more', 'id' => $banner['item_id'], 'slug' => self::sluggable($banner['name'])]);
            case 'VOD':
                return Url::to(['video/get-detail', 'id' => $banner['item_id'], 'slug' => self::sluggable($banner['name'])]);
            case 'FILM':
                if ($banner['video_id']) {
                    return Url::to(['playlist/get-detail', 'id' => $banner['item_id'], 'video_id' => $banner['video_id'], 'slug' => self::sluggable($banner['name'])]);
                } else {
                    return Url::to(['playlist/get-detail', 'id' => $banner['item_id'], 'slug' => self::sluggable($banner['name'])]);
                }
                break;
        }
        return "";
    }

    public static function sluggable($str) {

        $before = array(
            'àáâãäåòóôõöøèéêëðçìíîïùúûüñšž',
            '/[^a-z0-9\s]/',
            array('/\s/', '/--+/', '/---+/')
        );

        $after = array(
            'aaaaaaooooooeeeeeciiiiuuuunsz',
            '',
            '-'
        );

        $str = strtolower($str);
        $str = strtr($str, $before[0], $after[0]);
        $str = preg_replace($before[1], $after[1], $str);
        $str = trim($str);
        $str = preg_replace($before[2], $after[2], $str);

        return $str;
    }

    public static function encodeOutput($string) {
        return htmlentities($string, ENT_COMPAT, 'UTF-8');
    }

    public static function getUrlContent($url) {
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (\Yii::$app->params['proxy']['enable']) {
            curl_setopt($ch, CURLOPT_PROXY, \Yii::$app->params['proxy']['http_proxy']);
        }


        // $output contains the output string
        $response = curl_exec($ch);
        //ghi log
        \Yii::info("{GF}{" . $url . "} " . "{" . json_encode($response) . "}");
        // close curl resource to free up system resources
        curl_close($ch);
        //retry
        if (!$response) {
            // create curl resource
            $ch = curl_init();
            // set url
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // $output contains the output string
            $response = curl_exec($ch);
            //ghi log
            \Yii::info("{GF}Retry{" . $url . "} " . "{" . json_encode($response) . "}");
            // close curl resource to free up system resources
            curl_close($ch);
        }

        return $response;
    }

    public static function convertPlayTimes($count) {
        if ($count < 1000)
            return number_format($count, 0, ',', '.');
        if ($count < 1000000 && $count >= 1000)
            return intval($count / 1000) . "K";
        if ($count >= 1000000)
            return number_format(floor($count / 1000000), 0, ',', '.') . "tr";
    }

    public static function mergeById($array1, $array2, $key, $limit = null) {
        $keyExists = [];
        $result = [];
        foreach ($array1 as $arr) {
            $result[] = $arr;
            $keyExists[] = $arr[$key];
        }

        foreach ($array2 as $arr) {
            if (!in_array($arr[$key], $keyExists)) {
                $result[] = $arr;
                $keyExists[] = $arr[$key];
            }
        }

        if (!empty($limit)) {
            return array_slice($result, 0, $limit);
        } else {
            return $result;
        }
    }

    public static function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public static function dateDiff($datetime) {
        $timeFirst = strtotime('now');
        $timeSecond = strtotime($datetime);
        $differenceInSeconds = $timeSecond - $timeFirst;
        $seconds = 0;
        if ($differenceInSeconds > 0) {
            $now = new \DateTime();
            $nextTime = new \DateTime($datetime);

            $delta = $nextTime->diff($now);
            $seconds = ($delta->s) + ($delta->i * 60) + ($delta->h * 60 * 60) + ($delta->d * 60 * 60 * 24) + ($delta->m * 60 * 60 * 24 * 30) + ($delta->y * 60 * 60 * 24 * 365);
        }

        return $seconds;
    }

    public static function dateSub($add, $day) {
        $today = self::currentCacheTime();
        $sign = "-"; // Cong thoi gian
        if ($add) {
            $sign = "+"; // Tru thoi gian
        }
        $date_minus = strtotime(date("Y-m-d H:i:s", strtotime($today)) . " " . $sign . $day);
        $date = strftime("%Y-%m-%d %H:%M:%S", $date_minus);
        return $date;
    }

    public static function convertEtcToUtc($date)
    {
        $given = new \DateTime($date);
        $given->setTimezone(new \DateTimeZone("UTC"));
        return $given->format("Y-m-d H:i:s");
    }

    public static function convertUtcToEtc($date)
    {
        $given = new \DateTime($date);
        $given->setTimeZone(new \DateTimeZone('GMT+7'));
        return $given->format("Y-m-d H:i:s");
    }

    /**
     * Ham lay parameter va dich cac gia tri theo ngon ngu hien tai
     * @param $paramName
     * @param bool $translate
     */
    public static function getInterlizationParams($paramName, $translate = true, $category) {
        $params = Yii::$app->params[$paramName];


        if($translate) {
            $interlizationParams = [];

            foreach ($params as $key => $value) {

//                $interlizationParams[$key] = Yii::t($category, $value);
                // $value['content'] = Yii::t($category, $value['content']);
                $value = Yii::t($category, $value);
                // var_dump($value['content']);die;
                $interlizationParams[$key] = $value;
            }

            return $interlizationParams;
        } else {
            return $params;
        }
    }

    public static function getInterlizationParamsApp($paramName, $translate = true, $category) {
        $params = Yii::$app->params[$paramName];

//        echo "<pre>";
//        print_r($params); die;
        if($translate) {
            $interlizationParams = [];

            foreach ($params as $key => $value) {
                $value['content'] = Yii::t($category, $value['content']);

                $interlizationParams[$key] = $value;
            }
            return $interlizationParams;
        } else {
            return $params;
        }
    }

    public static function generateFolder() {
        $now = strtotime('now') . rand(0, 999);
        $childFolder = date('Y') . DIRECTORY_SEPARATOR . date('m') . DIRECTORY_SEPARATOR . date('d') . DIRECTORY_SEPARATOR . $now;

        return $childFolder;
    }
}
