<?php
/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 25/6/2016
 * Time: 2:34 PM
 */

namespace common\helpers;

use yii\helpers\StringHelper;

class MobiTVRedisCache extends \yii\redis\Cache
{
    const CACHE_1MINUTE = 60;
    const CACHE_10MINUTE = 600;
    const CACHE_15MINUTE = 900;
    const CACHE_30MINUTE = 1800;
    const CACHE_1HOUR = 3600;
    const CACHE_1DAY = 86400;
    const CACHE_NO_EXPIRE = 0;

    public function buildKey($key)
    {
        if (is_string($key)) {
            // Them ki tu _ vao white list
            $tmpKey = str_replace(["_",".","-","/"], "", $key);
            $key = (ctype_alnum($tmpKey) && StringHelper::byteLength($key) <= 100) ? $key : md5($key);
        } else {
            $key = md5(json_encode($key));
        }

        return $this->keyPrefix . $key;
    }

}