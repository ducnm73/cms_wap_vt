<?php

namespace common\helpers;

class Des
{
//Hàm mã hoá 3DES :
    public static function encrypt($str, $key)
    {
        $key = substr($key, 0, 24);
        $size = mcrypt_get_block_size(MCRYPT_3DES, MCRYPT_MODE_CBC);
        $str = Des::pkcs5Pad($str, $size);

        $data = mcrypt_encrypt(MCRYPT_3DES, $key, $str, MCRYPT_ENCRYPT);
        return bin2hex($data);
    }

//Hàm giải mã 3DES :
    public static function decrypt($str, $key)
    {
        $key = substr($key, 0, 24);
        $str = Des::hextobin($str);
        $str = mcrypt_decrypt(MCRYPT_3DES, $key, $str, MCRYPT_DECRYPT);
        $str = Des::pkcs5Unpad($str);
        return $str;
    }

//Hàm hỗ trợ mã hoá và giải mã :
    public static function pkcs5Pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    public static function pkcs5Unpad($text)
    {
        $pad = ord($text{strlen($text) - 1});
        if ($pad > strlen($text)) return false;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;
        return substr($text, 0, -1 * $pad);
    }

    function hextobin($hexstr)
    {
        $n = strlen($hexstr);
        $sbin = "";
        $i = 0;
        while ($i < $n) {
            $a = substr($hexstr, $i, 2);
            $c = pack("H*", $a);
            if ($i == 0) {
                $sbin = $c;
            } else {
                $sbin .= $c;
            }
            $i += 2;
        }
        return $sbin;
    }

    /*
     * Ma Hoa RSA dung PRIVATE KEY
     * */

    public static function  RSAEncrypt($string)
    {

        //$privateKey = VtHelper::getMtvConfig('API_PRIVATE_KEY');
        $privateKey = '-----BEGIN PRIVATE KEY-----
MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKvR4heUiz61+ze8
B7LVsp822xPGdB7s73fGfTjfm6Bh+hh+uiLSNOFXTM0b2KrigwxF9diq9skHCVUa
EpBzgr+Zx7ljuQxL8FEKEly7MnWClST6kS/YfkfVb2C5xVJD8I6GNDBMsCs9VyrQ
qgLpebMinPzBycvlNQBsmU7q+CH/AgMBAAECgYEAjcOxURVdwlMyQK0iiPpq05TW
Mz3Fp5KEjoW14gwVAsJ0MGu3Nm3L9bUrB/yWy3/y8lhy3N9KmVj16UKG2KqF0ChL
i/7cdRWbLjd3a+fzl46HParokPpOcTUnQbilDx4qsA4I5jDe86yu4UlALRncEQwZ
3tkkYtVxGFSNuMdq9ekCQQDZNsZR7Wr4fVMF5/ymErdfeafHIpdJjUAWcp/n5ojq
6uwaZsJ2yKpSkmM8TOC3ByOg1vsG3QIEj7Qug6CDcwvDAkEAyoAQwbKdOv/y2EtK
mxRuEvp69fqqqvZMEbjy1Xb0Ppm/TBK4Q+WFpMc92itiKtRDe2pm+Vdlhkblag/N
65N5FQJAe3k9hvjU4+gpt6PF5ImjH2zCSNqK8U6P7bK+304W6qMcD232gRtUZpe8
PVzeKGywfg92ptIfAYTGweIk7lQJjQJBAJQ4fQtqk8+1vKk5Ixi0MYXOteYKXpXY
xgFK4OQCt/CCVRV3tkxsWhAsjkd87dPvnp1dWpYztGo+xnmFq/I89akCQC31pi87
zybizzZA09gzuWZNr4ljxQyqaY1m+IVtCOxrTQlh696bVbcVP4D9aQnMBrmJVOHb
gwXYa5mBizQfMVo=
-----END PRIVATE KEY-----';


        openssl_private_encrypt($string, $result, $privateKey);
        return base64_encode($result);

    }

    /*
     * GIAI MA RSA dung PUBLIC KEY
     *
     * */

    public static function  RSADecrypt($string)
    {

        //$pKey = VtHelper::getMtvConfig('API_PUBLIC_KEY');
        $pKey = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCr0eIXlIs+tfs3vAey1bKfNtsT
xnQe7O93xn0435ugYfoYfroi0jThV0zNG9iq4oMMRfXYqvbJBwlVGhKQc4K/mce5
Y7kMS/BRChJcuzJ1gpUk+pEv2H5H1W9gucVSQ/COhjQwTLArPVcq0KoC6XmzIpz8
wcnL5TUAbJlO6vgh/wIDAQAB
-----END PUBLIC KEY-----';


        openssl_public_decrypt(base64_decode($string), $result, $pKey);

        return explode("&", $result);;

    }
}

?>