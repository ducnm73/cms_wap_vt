<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['default/verify-email', 'token' => $user->activation_key]);
?>
Hello <?= $user->first_name ?> <?= $user->last_name ?>,

Follow the link below to reset your password:

<?= $verifyLink ?>
