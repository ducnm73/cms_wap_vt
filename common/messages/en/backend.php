<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'MyClip' => 'Meuclip',
    'Actived' => 'Actived',
    'Bị khóa' => 'Locked',
    'Captcha' => 'Captcha',
    'Chờ duyệt' => 'Pending',
    'Create' => 'Create',
    'Create Menu' => 'Create Menu',
    'Cập nhật' => 'Update',
    'Cập nhật {modelClass}: ' => 'Update {modelClass}',
    'Cập nhật {modelClass}: ' => 'Update {modelClass}',
    'Delete' => '',
    'Found total {total} records' => '',
    'Home' => '',
    'Hot' => '',
    'Inactive' => '',
    'Không duyệt' => 'No confirm',
    'Không kích hoạt' => 'Not activated ',
    'Kích hoạt' => 'Activated',
    'Login' => '',
    'Menus' => '',
    'Mật khẩu cũ' => 'Old password',
    'Mật khẩu gõ lại chưa đúng' => 'The confirm password is not correct.',
    'Mật khẩu phải từ 6-20 ký tự và bao gồm chữ thường, chữ HOA, số và ký tự đặc biệt' => 'Password must be 6-20 characters long, including 1 capital letter and 1 lowercase letter',
    'Page {pager} / {total}' => '',
    'Parent' => '',
    'Password' => '',
    'Reset' => '',
    'Search' => '',
    'Bạn chưa chọn file upload' =>'You have not selected the upload file ',
    'Tạo mới {modelClass}' => 'New {modelClass}',
    'Update' => '',
    'Update Menu' => '',
    'Update {modelClass}: ' => '',
    'User' => '',
    'Username' => '',
    'View' => '',
    'Vt Users' => '',
    'records' => '',
    'Đã duyệt' => 'Validated',
    'Đối tác' => 'Partner',
    'Quản trị' => 'Admin',
    'Menu' => 'Administration',
    'Quản lý nội dung' => 'Content Management',
    'Video' => '',
    'Bộ phim' => 'Film',
    'Chăm sóc khách hàng' => 'Customer care',
    'Quản lý User' => 'User Management',
    'Chủ đề' => 'Topic',
    'Thể loại' => 'Category',
    'Quyền' => 'Permission',
    'Slideshow' => '',
    'Map chuyên mục' => 'Map categories',
    'Map nguồn Crawler' => 'Map source Crawler',
    'Quản lý Khách hàng' => 'Customer management',
    'Quản lý bình luận' => 'Comment management',
    'Báo cáo thống kê' => 'Report statistic',
    'Báo cáo đăng ký' => 'Report statistic',
    'Báo cáo hủy' => 'Report cancel',
    'Báo cáo sử dụng' => 'Report usage',
    'Báo cáo chung' => 'General Report',
    'Chương trình truyền thông' => 'Communications',
    'Báo cáo chia sẻ doanh thu' => 'Revenue sharing report',
    'Import thuê bao khuyến mãi' => 'Import promotion subscription',
    'Quản lý cấu hình' => 'Configuration management',
    'Map nguồn CP' => 'Map source CP',
    'Báo cáo doanh thu theo Video' => 'Report revenue by video',
    'Map nội dung' => 'Map out content',
    'Quản lý playlist' => 'Playlist management',
    'Report KPI' => '',
    'Duyệt thông tin đăng ký nhận tiền' => 'Approve the registration information to receive money',
    'Duyệt SMS khuyến mãi' => 'Approve promotion SMS',
    'Báo cáo user upload' => 'Report user upload',
    'Báo cáo thống kê duyệt bình luận' => 'Report staticstic confirm comment',
    'Link Ngắn' => 'Short link',
    'Quản lý ý kiến của uploader' => 'Manage comment of uploader',
    'Báo cáo thống kê duyệt video' => 'Report statistic video confirm',
    'Báo cáo kênh bán' => 'Report channels sale',
    'Báo cáo chia sẻ User Upload' => 'Report sharing User Upload',
    'Báo cáo chia sẻ doanh thu version 2' => 'Report sharing revenue version 2',
    'Báo cáo Outsource' => 'Report Outsource',
    'Báo cáo nội dung' => 'Report content',
    'Báo cáo vi phạm content' => 'Report content violation',
    'Báo cáo quản lý content' => 'Report content management',
    'Báo cáo content xem nhiều' => 'Report content with high view',
    'Phê duyệt thông tin User Upload' => 'Approve information User Upload',
    'Duyệt thông tin user upload video' => 'Approve information User Upload video',
    'Báo cáo lịch sử tác động' => 'Report history impact',
    'Cấu hình KPI mục tiêu' => 'Configure KPI target',
    'Duyệt thông tin channel' => 'Approve information channel',
    'Video crawler theo CP' => 'Video crawler by CP',

    'Guest' => '',
    'Tiếng Anh' =>'English',
    'Log Out' => '',
    'Sign In' => '',
    'My Profile' => '',
    '@MyClip' => '',
    'Thất bại' => 'Fail',
    'Thành công' => 'Success',
    'STT' => 'ID',
    'Thời gian' => 'Time',
    'Số điện thoại' => 'Phone number',
    'Hoạt động' => 'Active',
    'Giá cước' => 'Postage',
    'Nội dung' => 'Content',
    'Trạng thái' => 'Status',
    'Mã lỗi' => 'Error number',
    'Nguồn' => 'Source',
    'Đầu số' => 'Head numbers',
    'Process ID' => '',
    'Thời gian nhận' => 'Received time',
    'Cú pháp' => 'Syntax',
    'Loại' => 'Kind',
    'ID Video' => '',
    'Tên Video' => 'Video name',
    'IP' => '',
    'User ID' => '',
    'User Agent' => '',
    'Session Id' => '',
    'Cập nhật thành công!' => 'Update successfully',
    'Bạn không có quyền thực hiện giao dịch này' => 'You have not permission to conduct transaction',
    'Không map user Frontend không thể sử dụng tính năng này! Liên hệ Admin để cấp tài khoản' => 'There is no connect user Frontend  to use this function! Please contact Admin to provide account.',
    'Ngày tháng không hợp lệ' => 'Invalid time',
    'Số lượng video upload trong ngày của user không có hợp đồng' => 'Number of users \'  video upload with no contract per day.',
    'Số lượng video upload trong ngày của user có hợp đồng' => 'Number of users video upload have day contract.',
    'Số lượng user không có hợp đồng upload video trong ngày' => 'Number of users haven\'t day contract upload video ',
    'Số lượng user có hợp đồng upload video trong ngày' => 'Number of users have day contract upload video',
    'Video duyệt quá thời gian theo KPI (8h-23h)' => 'Video approved but out of time by KPI (8h-23h)',
    'Tổng video khách hàng upload bị từ chối từ 0h00 đến 23h59' => 'All upload videos of customer was deny from 0h00 to 23h59',
    'Tổng video khách hàng upload được duyệt từ 0h00 đến 23h59' => 'All upload videos of customer was accept from 0h00 to 23h59',
    'Ngày' => 'Day',
    'Đến ngày:' => 'To',
    'Từ ngày:' => 'From',
    'Danh sách user upload' => 'List of user upload',
    'Số thuê bao' => 'Subcribers',
    'Thời gian report' => 'Time of report',
    'Tên video:' => 'Video name',
    'Loại report:' => 'Types of report',
    'Danh sách các tài khoản report' => 'List of account report',
    'Không xác định' => 'Unknown',
    'Thời gian hiển thị trang trên app (<' => 'Display time on app (<',
    'ThoiGianHienThiTrangWeb' => 'ShowtimeOnWeb',
    'Đến ngày' => 'To',
    'Từ ngày' => 'From',
    'giây' => 'seconds',
    'Giờ' => 'Hour',
    'Thời gian hiển thị trang trên web/wap (<' => 'Display time page on web/wap',
    'TiLeTruyCapThanhCongApp' => 'AccessAppSuccessRate',
    'Tỉ lệ truy cập thành công trên app' => 'Access App success rate',
    'TiLeTruyCapThanhCongWeb' => 'AccessWebSuccessRate',
    'Tỉ lệ truy cập thành công' => 'AccessSuccessRate',
    'TocDoUploadTrungBinh' => 'AverageUploadSpeed',
    'Tốc độ upload trung bình' => 'Average upload speed',
    'TiLeUploadLoi' => 'UploadFailRate',
    'Tỉ lệ upload lỗi' => 'Upload fail rate',
    'ThoiGianChoXemVideo' => 'WaitingTimeVideo',
    'Thời gian chờ để bắt đầu xem được video' => 'Waiting time video',
    'TiLeSoLanBiBufferKhiXemVideo' => 'RateTimeToBufferWhenWatchVideo',
    'Tỷ lệ số lần bị buffer khi đang xem video' => 'Rate time to buffer when watch video',
    'TiLeThoiGianBiBufferKhiXemVideo' => 'TimeRateToBufferWhenWatchVideo',
    'TiLeSoLanBufferQua3s' => 'RateTimeToBufferOver3s',
    'ThoiGianEncodeVideo' => 'TimeEncodeVideo',
    'phút' => 'Minute',
    'Thời gian encode video' => 'Time to encode video',
    'TyLeEncodeLoi' => 'EncodefailRate',
    'Thời gian hiển thị trang trên app (giây)' => 'Display time on app (seconds) ',
    'Thời gian hiển thị trang trên web (giây)' => 'Display time on web (seconds)',
    'Tỉ lệ truy cập thành công trên app (%)' => 'Rate of access success on app (%)',
    'Tỉ lệ truy cập thành công trên web (%)' => 'Rate of access success on web (%)',
    'Tốc độ upload trung bình (Kbps)' => 'Average upload speed (Kbps)',
    'Tỷ lệ upload lỗi (%)' => 'Rate of upload fail',
    'Thời gian chờ để bắt đầu xem được video (giây)' => 'Wait time to watch video (seconds)',
    'Tỷ lệ số lần bị buffer khi đang xem video (%)' => 'Rate of number buffer when watch video (%)',
    'Tỷ lệ thời gian bị buffer khi đang xem video (%)' => 'Rate',
    'Tỷ lệ số lần bị buffer vượt quá 3s (%)' => 'Rate of number buffer over 3s (%)',
    'Thời gian encode video (phút)' => 'Time to encode video (mins)',
    'Tỷ lệ encode lỗi (%)' => 'Rate of encode fail (%)',
    'Tên video' => 'Video name',
    'Số lượng video chờ duyệt' => 'Number of video pending',
    'Ngày tạo' => 'Date Created',
    'Mã CP' => 'CP Code',
    'Đến ngày: ' => 'To',
    'Bạn không có quyền truy cập chức năng này!' => 'You do not have permission to access.' ,
    'Loại CP:' => 'Types of CP',
    'Tất cả' => 'All',
    'CP đối soát' => 'CP checking',
    'CP Crawler' => '',
    'Khách hàng upload' => 'Upload customer',
    'CP' => '',
    'Tổng video luỹ kế' => 'Total videos Accumulated',
    'Lũy kế nội dung trong tháng báo cáo' => 'Content Accumulated in monthly report',
    'Bạn không có quyền thực hiện chức năng này' => 'You do not have permission to do',
    'Cập nhật thành công' => 'Update successfully',
    'Định dạng file upload không đúng' => 'File format upload is incorrect',
    'Mật khẩu mới chưa đủ mạnh!' => 'New password is not strong enough',
    'Mật khẩu mới không được trùng với mật khẩu cũ!' => 'New password is not same to old passwword!',
    'Mật khẩu cũ không đúng!' => 'The old password is incorrect!',
    'Duyệt thành công' => 'Approve successfully',
    'Hạ duyệt thành công' => 'Deny Approve successfully',
    'Không tìm thấy bình luận' => 'No comments found',
    'Khóa tài khoản thành công' => 'Lock account successfully',
    'Không tìm thấy thông tin kênh' => 'No information channel found',
    'Thêm mới thành công' => 'Successfully',
    'Chỉ hợp đồng đã duyệt mới được phép gửi' => 'Only the contract accepted can be sent',
    'Gửi hợp đồng thành công' => 'Send contract successfully',
    'Thêm mới thành công!' => 'Successfully',
    'Thất bại:' => 'Fail:',
    'Đã xử lý thành công:' => 'Process successfully:',
    'Không tìm thấy dữ liệu import' => 'No found data import',
    'Gửi tin thành công đến số:' => 'Message was send to: ',
    'Không tìm thấy ý kiến khiếu nại' => 'No complaint found',
    'Không có dữ liệu' => 'No data',
    'Cập nhật thành công !' => 'Update successfully!',
    'Thêm mới thành công !' => 'Add new successfully',
    'Xóa thành công !' => 'Delete successfully',
    'Số lượng video thêm vào playlist vượt quá ' . Yii::$app->params['playlist.limit.item'] . ' !' => 'Number video add to playlist are over' .Yii::$app->params['playlist.limit.item'] . ' !',
    'Lỗi xác thực' => 'Authentication error',
    'Chuyển chờ duyệt thành công!' => 'Transfer pending successfully',
    'Không có quyền thực hiện chức năng này!' => 'No permission to do this function',
    'Duyệt thành công!' => 'Approve successfully',
    'Duyệt không thành công, không có quyền duyệt!' => 'Can not approve, no permission to approve!',
    'Hạ nội dung thành công!' => 'Turn off content successfully',
    'Không có quyền hạ nội dung!' => 'No permission to turn off content',
    'Xử lý thành công!' => 'Processing successfully',
    'Xử lý không thành công do kênh không hợp lệ' => 'Processing failed due to invalid channel',
    'Xử lý không thành công do CP không hợp lệ!' => 'Processing failed due to invalid CP!',
    'Xem lướt thành công' => 'Take a glance',
    'Bucket' => '',
    'Ảnh đại diện' => 'Avatar image',
    'Cấp chuyên mục' => 'Category',
    'Vị trí' => 'Position',
    'Ảnh banner' => 'Banner Image',
    'Mô tả' => 'Description',
    'Tên' => 'Name',
    'ID' => '',
    'Gói cước quảng cáo' => 'Ads packs',
    'Lượt yêu thích' => 'Likes',
    'Đề xuất' => 'Offer',
    'Path' => '',
    'Cập nhật bởi' => 'Update by',
    'Tạo bởi' => 'Created by',
    'Thời gian cập nhật' => 'Update time',
    'Phê duyệt bởi' => 'Approve of',
    'Thứ tự' => 'Order',
    'Lý do' => 'Reason',
    'Giá xem' => 'Pricetag',
    'Thuộc tính' => 'Properties',
    'Thời gian kết thúc' => 'Finish time',
    'Thời gian bắt đầu' => 'Start time',
    'Đường dẫn' => 'Link',
    'Reject Reason' => '',
    'Updated By' => '',
    'Created By' => '',
    'Updated At' => '',
    'Created At' => '',
    'Status' => '',
    'Video ID' => '',
    'Salt' => '',
    'Tên hiển thị' => 'Display name',
    'Email' => '',
    'Oauth ID' => '',
    'Follow Count' => '',
    'Otp' => '',
    'Last Login' => '',
    'Changed Password' => '',
    'Video Count' => '',
    'Is Show Suggest' => '',
    'Source Display' => '',
    'Play Times' => '',
    'Số lượng content' => 'Number of content',
    'Msisdn' => '',
    'Kênh sở hữu' => 'Your channel',
    'Tên playlist' => 'Playlist name',
    'Type' => '',
    'Chọn gói cước' => 'Choose pack of data',
    'Tạo mới' => 'Create New',




    'Miễn phí' => 'Free',
    'Mất phí' => 'Paid',
    'Có' => 'Yes',
    'Không' => 'No',
    'Đăng ký' => 'Sign up',
    'Hủy' => 'Cancel',
    'Lịch sử giao dịch' => 'Exchange history',
    'Lịch sử MT' => 'MT history',
    'Lịch sử MO' => 'MO history',
    'Lịch sử xem' => 'Watching history',
    'Lịch sử truy cập' => 'Access history',
    'Điểm sự kiện' => 'Event',
    'Đăng ký/Hủy' => 'Sign up/ Cancel',
    'Tương tác' => 'Interact',
    'Bình luận' => 'Comment',
    'Xem video' => 'Watch video',
    'Đăng tải video' => 'Upload video',
    'Chia sẻ video' => 'Share video',
    'Yêu thích video' => 'Like video',
    'Theo dõi kênh' => 'Subscribe',
    'Đăng ký dịch vụ' => 'Register service',
    'Không thích video' => 'Unlike video',
    'Tổng điểm' => 'Total points',
    'Xuất Excel' => 'Export exel',
    'Đầu' => 'First',
    'Cuối' => 'Last',
    ' mục.' => 'categories.',
    'Tổng số ' => 'Total ',
    'Tổng số' => 'Total',
    'Tìm kiếm' => 'Search',
    'Lưu' => 'Save',
    'Gửi duyệt' => 'Requires approval',
    'Duyệt' => 'Approved',
    'Xóa' => 'Delete',
    'Tập phim' => 'Episode',
    'Thông tin' => 'Information',
    'Ảnh' => 'Image',
    'Thao tác' => 'Action',
    'Tập phim tạm' => 'Temporary Episode',
    'Xuất bản' => 'Publish',
    'Chờ convert' => 'Wait to convert',
    'Convert lỗi' => 'Convert fail',
    'Convert lại' => 'Convert again',
    'Lượt xem:' => 'Views',
    'Lượt thích:' => 'Likes',
    'Resolution:' => '',
    'CP:' => '',
    'Hôm nay' => 'Today',
    'Tháng trước' => 'Last month',
    'Tháng này' => 'This month',
    '30 ngày gần đây' => 'Last month',
    '7 ngày gần đây' => 'Last week',
    'Hôm qua' => 'Yesterday',
    'Ngày báo cáo' => 'Day report',
    'Vẽ biểu đồ' => 'Chart',
    'Xuất file' => 'Export file',
    'Lọc dữ liệu' => 'Filter data',
    'Khoảng thời gian báo cáo' => 'Time of report',
    'Chọn loại KPI' => 'Types of KPI',
    'Số lượt xem' => 'Views',
    'Chuyên mục' => 'Category',
    'Tên Đối tác' => 'Name of provider',
    'Tất cả chuyên mục' => 'All categories',
    'Chọn loại' => 'Select sort',
    'Xuất file chi tiết' => 'Export detail file',
    'Thời gian encode video (giây)' => 'Encode video time ( seconds)',
    'Tháng báo cáo' => 'Monthly report',
    'Tạo KPI mục tiêu' => 'Create KPI target',
    'Cập nhật KPI mục tiêu' => 'Update KPI target',
    'tháng' => 'Month',
    'Bạn có muốn xóa KPI mục tiêu này không?' => 'Do you want delete KPI target?',
    'Chi tiết KPI mục tiêu tháng ' => 'KPI target deital in month',
    'Mật khẩu mới' => 'New password',
    'Gõ lại mật khẩu mới' => 'Confirm password',
    'Chọn đối tác' => 'Select provider',
    'Tìm kiếm User' => 'Search User',
    'Khóa tài khoản' => 'Lock user',
    '(Không có ảnh)' => '(No image)',
    'Xem cỡ lớn' => 'Show bigger',
    'Ảnh mặt sau CMND:' => 'The second image of ID',
    'Ảnh mặt trước CMND:' => 'The first image of ID',
    'Ngày đăng ký' => 'Date',
    'Tên kênh' => 'Name channel',
    'Chi tiết' => 'Detail',
    'Thông tin user upload video' => 'Information of user upload video',
    'Thời gian thực hiện' => 'Time',
    'Người dùng' => 'User',
    'Lý do không duyệt bình luận:' => 'Reason deny',
    'Lưu lại' => 'Save',
    'Tạo lúc' => 'Create at',
    'Phê duyệt lúc' => 'Approve date',
    'Thời gian phê duyệt' => 'Time of approve',
    'Tài khoản' => 'Account',
    'Duyệt bởi' => 'Approve to',
    'Cập nhật bình luận' => 'Update comment',
    'Tạo' => 'Create',
    'Trạng thái:' => 'Status',
    'Tạo bởi:' => 'Create by',
    'Ảnh CMT mặt sau' => 'The second image of ID',
    'Xem ảnh lớn' => 'Big image',
    'Ảnh CMT mặt trước' => 'The first image of ID',
    'Số CMTND' => 'ID number',
    'Tải mẫu HĐ' => 'Download form contract',
    'Hợp đồng' => 'Contract',
    'Thông tin thể loại' => 'Information',
    'Không Hot' => '',
    'Tìm kiếm kênh' => 'Search',
    'Chuyên mục MyClip' => 'MyClip category',
    'Kênh MyClip' => 'MyClip channel',
    'ID Kho' => 'ID',
    'Crawler Kho' => 'Crawler',
    'Chọn chuyên mục' => 'Select categories',
    'Tên quản trị Kho' => 'Name',
    'IMPORT THUÊ BAO KHUYẾN MÃI' => 'Import promotion message',
    'Trang đầu' => 'First page',
    'Trang cuối' => 'Last page',
    'Tạo link ngắn' => 'Create shorten link',
    'Link ngắn' => 'Shorten link',
    'Tìm kiếm nội dung' => 'Finding content',
    'Mở lại tin nhắn đã hạ' => 're-open message down',
    'Hạ tin nhắn' => 'Message down',
    'Duyệt tin nhắn' => 'Approve message',
    'Yêu cầu duyệt lại tin nhắn' => 'Request approve again message',
    'Kiểm tra tin nháp' => 'Checking draf message',
    'Kiểm tra lại tin đã duyệt' => 'Checking message accepted again',
    'Nội dung tin nhắn đã duyệt' => 'Content message accepted',
    'Mã tin nhắn:' => 'Code message',
    'Nhập tin cần duyệt lại' => 'Enter the message need accept again',
    'Đây là tin nhắn dành cho' => 'This is message to',
    'Tạo mới tin nhắn duyệt' => 'New messange accept',
    'Cập nhật SMS khuyến mãi' => 'Update promotion SMS',
    'Chọn kênh truyền thông' => 'Select communication channel',
    'SMS' => '',
    'APP' => '',
    'Bạn có muốn xem lướt?' => 'Do you want to watch preview?',
    'Thuê bao test' => 'Subcriber test',
    'Thuê bao cài đặt APP' => 'Subcriber with app installed',
    'Chọn tập thuê bao' => 'Select group of subcriber',
    'Kênh truyền thông' => 'Communication channel',
    'Tên chương trình' => 'Channel name',
    'Nội dung chương trình' => 'Program content',
    'Thời gian gửi' => 'Sending time',
    'Báo cáo khiếu nại của uploader' => 'Report complaint of uploader',
    'Nội dung khiếu nại' => 'content of complaints',
    'File đính kèm' => 'Attach files',
    'Tiêu đề:' => 'Title',
    'Mô tả:' => 'Description',
    'Thời gian yêu cầu duyệt:' => 'Time of request',
    'Thời gian admin duyệt:' => 'Time of accepted admin',
    'Tài khoản:' => 'Account',
    'bản ghi.' => 'Record',
    'Xóa banner' => 'Delete banner',
    'Xóa avatar' => 'Delete avatar',
    'Khách hàng' => 'Customer',
    'Thêm mới Khách hàng' => 'New customer',
    'Sắp xếp kênh Hot' => 'Arrange hot channel',
    'Chỉnh sửa Khách hàng' => 'Update information',
    'Ảnh avatar:' => 'Avatar image',
    'Lý do từ chối duyệt:' => 'Reason of refusal:',
    'Mô tả của Channel' => 'Description of channel',
    'Ảnh banner:' => 'Banner image',
    'Lý do từ chối duyệt' => 'Reason of refusal',
    'Tool duyệt thông tin kênh' => 'Tool accept channel information',
    'Ảnh avatar' => 'Avatar image',
    'Cập nhật thông tin kênh' => 'Update channel information',
    'Thêm mới nội dung playlist' => 'New playlist content',
    'Hoàn thành' => 'Finish',
    'Tên nội dung' => 'Content name',
    'Phê duyệt' => 'Approve',
    'Chọn nội dung playlist' => 'Select content of playlist',
    'Playlist' => '',
    '{attribute} không được nhỏ hơn 0' => '{attribute} must be no less than 0',
    'Tạo mới playlist' => 'New playlist',
    '{attribute} phải là số nguyên' =>'{attribute} must be an integer',
    'Lượt xem playlist' => 'Number of playlist\'s view',
    'User tạo' => 'User',
    'Nguồn hiển thị' => 'View of source',
    'Chỉ cho phép các tệp có các phần mở rộng này: jpg, jpeg, png.' => 'Only files with these extensions are allowed: jpg, jpeg, png.',
    'Số lượng content được nhóm' => 'Number of grouded content',
    'Thời gian tạo' => 'Time of create',
    'Thông tin video' => 'Video information',
    'Chọn trạng thái' => 'Status',
    '{attribute} không được để trống'=> '{attribute} cannot be blank',

    // Chưa có file dịch
    'Tuần' => 'Week',
    'sau' => 'after',
    'Không kết nối được tới WSDL' => 'Can not connect to WSDL',
    'Đăng nhập sai quá 5 lần. Tài khoản tạm thời bị khóa 10 phút.' => 'Wrong login more than 5 times, please login again after 10 minutes',
    'Video không hợp lệ' => 'Invalid video',
    'Thuê bao đang bị tạm ngưng sử dụng dịch vụ' => 'Subcriber is stop to',
    'Xác thực bản quyền thất bại' => 'Copyright authentication failed',
    'The requested page does not exist.' => '',
    'A file to illustrate changes by hour, day of each kpi' => '',
    'ThoiGianHienThiTrangApp' => 'DisplayTimePageOnApp',
    'Tỷ lệ encode lỗi ( < ' => 'Rate encode fail ( < ',
    'No data' => '',
    'Content Management Report' => '',
    'Video Crawler By CP' => '',
    'A report about Video Crawler By CP' => '',
    'Chuyển tiền qua VTPAY' => 'Transfers by VTPAY',
    'Tài khoản di động' => 'Phone number',
    'Tài khoản ngân hàng' => 'Bank account number',
    'Can not read file:' => '',
    'CONTENT INVALID' => '',
    'MSISDN INVALID' => '',
    'CSRF INVALID' => '',
    'CSRF TOKEN INVALID' => '',
    'Thao tác theo lô' => 'Act Batch',
    'Dung lượng file tối đa cho phép' => 'Maximun size of file permission',
    'Are you sure you want to delete this item?' => '',
    'Upload a file' => '',
    'Processing dropped files...' => '',
    'Close' => '',
    'No' => '',
    'Yes' => '',
    'Cancel' => '',
    'Ok' => '',
    'Create Report Kpi' => '',
    'Report Kpis' => '',
    'EXPORT EXCEL' => '',
    'First' => '',
    'Last' => '',
    'Loại KPI' => '',
    'Update Report Kpi' => '',
    'REPORT' => '',
    'EXPORT' => '',
    'The above error occurred while the Web server was processing your request.' => '',
    'Please contact us if you think this is a server error. Thank you.' => '',
    'User chưa map' => 'User haven\'t map',
    'Chủ đề chưa map' => 'Topic haven\'t map',
    'Chọn tháng' => 'Select Month',
    'Loading ...' => '',
    'Create Vt Account Infomation' => '',
    'Vt Action Logs' => '',
    'Create Vt Action Log' => '',
    'Update Vt Action Log' => '',
    'Create Vt Comment' => '',
    'Vt Comments' => '',
    'KEY' => '',
    'Create Vt Config' => '',
    'Vt Configs' => '',
    'Quay lại danh sách' => 'Turn back to list',
    'Gửi hợp đồng' => 'Send contract',
    'Bạn có chắc chắn muốn gửi hợp đồng đối soát?' => 'Do you want to send checking contract? ',
    'Create Vt Contract' => '',
    'Vt Contracts' => '',
    'CP CODE' => '',
    'DOWNLOAD' => '',
    'Chọn thể loại cha' => 'Select parent type',
    'Chọn vị trí' => 'Select position',
    'Chọn chuyên mục ...' => 'Select category',
    'Mapping Thể loại & chủ đề' => ' Mapping types & topics',
    'Tìm kiếm kênh ...' => 'Search channel...',
    'Mapping User' => '',
    'Create Vt Promotion Data' => '',
    'Vt Promotion Datas' => '',
    'Update Vt Promotion Data' => '',
    'Vt Short Links' => '',
    'Chọn vị trí hiện thị' => 'Choose position display',
    'Vt Sms Approves' => '',
    'Đang xử lý' => 'In processing',
    'Kết thúc' => 'End',
    'Mobile Client' => '',
    'Create Vt Upload Feedback' => '',
    'Vt Upload Feedbacks' => '',
    'Update Vt Upload Feedback' => '',
    'Waiting for results...' => '',
    'Create Vt User Change Info' => '',
    'Vt User Change Infos' => '',
    'Pause' => '',
    'Continue' => '',
    'Anim pariatur cliche...' => '',
    'Anim pariatur cliche...2' => '',
    'User duyệt từ kho' => 'User approve from',
    '---User duyệt từ kho---' => '---User approve from---',
    'Chọn CP' => 'Select CP',
    'Lưu & Đề nghị duyệt' => 'Save & request approve',
    'Videos' => '',
    'Mapping Crawler' => '',
    'Chưa active' => 'Not active',
    'Chọn Nội Dung' => 'Select Content',
    'Gửi hợp đồng không thành công' => 'Contract submission failed',
    'Chọn' => 'Select',

    'Báo cáo chi tiết Video' => 'Detail video report',
    'Bạn có chắc chắn muốn chuyển outsource các bản ghi đã chọn ?' => 'Are you sure you want to outsource the selected records?',
    'Bạn có chắc chắn muốn cập nhật CP các bản ghi đã chọn ?' => 'Are you sure you want to update the CP selected records?',
    'Bạn có chắc chắn muốn cập nhật các bản ghi đã chọn ?' => 'Are you sure you want to update the selected records?',
    'Bạn có chắc chắn muốn phê duyệt các bản ghi đã chọn ?' => 'Are you sure you want to approve the selected records?',
    'Bạn có chắc chắn muốn từ chối duyệt các bản ghi đã chọn ?' => 'Are you sure you want to refuse to browse the selected records?',
    'CP Code' => 'CP Code',
    'Chuyển outsource' => 'Tranfer outsource',
    'Chưa duyệt' => 'Not approved',
    'Chưa tác động' => 'Not impacted yet',
    'Chọn chủ đề' => 'Choose a topic',
    'Chọn gói cước quảng cáo' => 'Select the advertising package',
    'Chọn ngôn ngữ' => 'Choose the language',
    'Chọn thể loại' => 'Choose a category',
    'Chọn thịnh hành' => 'Choose trending',
    'Chọn trạng thái outsource' => 'Select the outsource status',
    'Cập nhật CP' => 'Update CP',
    'Cập nhật Video' => 'Update video',
    'Cập nhật cấu hình' => 'Update configuration',
    'Cập nhật kênh' => 'Update Channel',
    'Cập nhật tập Phim:' => 'Update Episode:',
    'Danh sách chi tiết' => 'Detail list',
    'Doanh thu' => 'Revenue',
    'Doanh thu (VNĐ)' => 'Revenue(KIP)',
    'Doanh thu gia hạn' => 'Renewal revenue',
    'Doanh thu trừ cước' => 'Fee deducted revenue',
    'Doanh thu đăng ký' => 'Registered revenue',
    'Duyệt lại' => 'Check again',
    'Duyệt lại theo khiếu nại uploader' => 'Review by complaint uploader',
    'File không đạt chất lượng' => 'Unqualified file',
    'File lỗi' => 'File error',
    'giờ' => 'hour',
    'Gói cước' => 'Package',
    'Hạ xuống' => 'Refusal',
    'hủy' => 'cancel',
    'Khiếu nại của uploader' => 'Complaint of uploader',
    'Không có file đính kèm' => 'There are no attachments',
    '(Không xác định)' => '(Unknown)',
    'Kênh' => 'Channel',
    'Loại gói cước' => 'Package type',
    'Loại report' => 'Report type',
    'Lý do không duyệt lại video' => 'Reason not to review videos',
    'Lý do từ chối' => 'Reason for refusal',
    'Lũy kế' => 'Accumulated',
    'Lượt thích' => 'Likes',
    'Lượt xem' => 'Views',
    'Mô tả lỗi' => 'Error description',
    'Nguồn dữ liệu' => 'Data sources',
    'Ngôn ngữ' => 'Language',
    'Người cập nhật:' => 'User update',
    'Người tạo' => 'User create',
    'Người tạo (Kênh)' => 'User create (Channel)',
    'Người tạo:' => 'User create:',
    'Năm báo cáo' => 'Report year',
    'One' => 'One',
    'Phim' => 'Film',
    'Phim tạm' => 'Temporary movie',
    'Phân loại' => 'Classify',
    'No results found.' => 'No results found.',
    'Xóa' =>'Delete',
    'Sửa' =>'Update',
    'Phê duyệt trên Kho' => 'Approved on Warehouse',
    'Quản trị viên' => 'Administrators',
    'References' => 'References',
    'Resolution' => 'Resolution',
    'Số lượng' => 'Amount',
    'Số lượng chờ duyệt' => 'Number of pending approvals',
    'Số lượng duyệt' => 'Number of approve',
    'Số lượng video thêm vào playlist vượt quá ' => 'The number of videos added to the playlist exceeded',
    'Số lượng đăng ký' => 'Number of registrations',
    'Số thuê bao Viettel truy cập' => 'Number of Movitel subscribers accessing',
    'Số thuê bao Viettel xem VOD' => 'Number of Movitel subscribers watching VOD',
    'Số user truy cập' => 'umber of user access',
    'Số user xem VOD' => 'Number of users viewing VOD',
    'Sửa tên' => 'Edit name',
    'Thu gọn' => 'Collapse',
    'Thêm' => 'Add',
    'Thêm mới phim' => 'Add new film',
    'Thêm ngôn ngữ' => 'Add language',
    'Thêm ngôn ngữ khác' => 'Add other language',
    'Thông tin bộ phim' => 'Movie information',
    'Thịnh hành' => 'Trending',
    'Thống kê' => 'Statistical',
    'Thời gian duyệt dưới 5 phút' => 'Approve time is under 5 minutes',
    'Thời gian hẹn giờ' => 'Time timer',
    'Thời gian xuất bản' => 'Publication time',
    'Thời lượng' => 'Time',
    'Tiêu đề' => 'Title',
    'Trạng thái duyệt' => 'Approve status',
    'Trừ cước' => 'Fee charged',
    'trừ cước' => 'fee charged',
    'Trừ cước thành công' => 'Fee charged success',
    'Two' => 'Two',
    'Tác động bởi' => 'Impacted by',
    'Tên tập' => 'Episode name',
    'Tìm kiếm Kênh' => 'Search Channel',
    'Tĩnh' => 'Static',
    'Tạo mới phim' => 'Create a new movie',
    'Tất cả đối tác' => 'All CP',
    'Tổng comment' => 'Total comment',
    'Tổng hợp' => 'Synthetic',
    'Tổng nội dung xem' => 'Total content to watch',
    'Tổng số lượng report' => 'Total number of reports',
    'Tổng số lượng từ chối' => 'The total number of rejection',
    'Tổng số video duyệt' => 'Total number of videos browsed',
    'Tổng số video từ chối' => 'Total number of rejections',
    'Tổng video' => 'Total video',
    'Từ chối duyệt' => 'Deny approve',
    'Tỷ lệ số lần buffer vượt quá 3s' => 'The ratio of the number of buffers exceeds 3s',
    'Upload Video' => 'Upload Video',
    'Upload lỗi' => 'Upload error',
    'Vui lòng chọn trình duyệt hỗ trợ HTML5' => 'Please select a browser that supports HTML5',
    'Xem lướt' => 'Review',
    'Xem lướt bởi' => 'Review by',
    'Xem tất cả' => 'View all',
    'Xuất danh sách' => 'Export list',
    'không có' => 'not available',
    'Tháng' => 'Month',
    'Đa ngôn ngữ' => 'Multilanguage',
    'Đang convert' => 'Converting',
    'đăng ký' => 'register',
    'Động' => 'Dynamic',
    'đã duyệt' => 'Approved',
    'Ảnh mặt sau CMND' => 'Photo on the back of ID card',
    'Ảnh mặt trước CMND' => 'Photo in front of ID card',
    'Họ và tên' => 'First and last name',
    'Số CMND' => 'Identification Card Number',
    'Ngày cấp CMND' => 'Issue date of ID card',
    'Nơi cấp CMND' => 'Place of issue ID',
    'ID kênh' => 'Channel ID',
    'Ngày cập nhật' => 'Update day',
    'Xác nhận' => 'Confirm',
    'SĐT xác nhận' => 'Phone number confirmed',
    'Lý do không duyệt'=> 'Reason not approved',
    'Mã hợp đồng' => 'Contract Code',
    'Ngày cấp CMTND' => 'Issue date of ID card',
    'Nơi cấp CMTND' => 'Place of issue ID',
    'Địa chỉ thường trú' => 'Permanent address',
    'Số thuê bao liên hệ' => 'Subscriber contact number',
    'Hình thức thanh toán' => 'Payments',
    'Mã số thuế' => 'Tax code',
    'Thời điểm tạo' => 'Time created',
    'Mã tài khoản Ngân Hàng' => 'Bank account code',
    'Tên Ngân Hàng' => 'Bank name',
    'Chi nhánh' => 'Branch',
    'Số thuê bao nhận thanh toán' => 'Number of subscribers receiving payment',
    'Cập nhật lúc' => 'Updated at',
    'Nháp' => 'Draft',
    'Từ chối' => 'Refuse',
    'Dự thảo' => 'Draft',
    'Đang chờ duyệt tin(tin cũ vẫn có hiệu lực)' => 'Awaiting browsing news (old news is still valid)',
    'Tin nhắn đã được duyệt' => 'Message approved',
    'Tin nhắn đã bị hạ' => 'The message has been downed',
    'Chi tiết video' => 'Video details',
    'Chuyên mục cha' => 'Parent category',
    'Chuyên mục con' => 'Sub category',
    'mua bộ' => 'buy sets',
    'Gói ngày' => 'Daily package',
    'Gói tuần' => 'Weekly package',
    'Gói tháng' => 'Monthly package',
    'Chuyển đổi' => 'Convert',
    'Chung' => 'General',
    'Chưa kích hoạt' => 'Not activated',
    'Tạm' => 'Draft',
    'Chờ phê duyệt' => 'Awaiting approval',
    'Đã phê duyệt' => 'Approved',
    'Quản trị viên từ chối/xóa' => 'Administrator reject/delete',
    'Khách hàng tự xóa' => 'Customer self-delete',
    'Chọn lý do' => 'Choose a reason',
    'Lỗi định dạng' => 'Format error',
    'Không có ảnh video hoặc chất lượng ảnh kém' => 'No video images or poor image quality',
    'Nội dung khiêu dâm' => 'Sexual content',
    'Vấn đề bản quyền' => 'Copyright issue',
    'Nội dung đã có trên MyClip' => 'Content already on MyClip',
    'Không đạt yêu cầu về chất lượng hình ảnh, âm thanh' => 'Fail to meet the requirements for image and sound quality',
    'Nội dung bạo lực, phản cảm' => 'Violent, repulsive content',
    'Đang duyệt' => 'approving',
    'Xem VOD' => 'View VOD',
    'Chưa sử dụng' => 'Not used yet',
    'Mã khuyến mãi' => 'Promotion code',
    'Thời điểm import' => 'Time of import',
    'Thời điểm hết hạn' => 'Expiry time',
    '---Người tạo---' => '--- Creator ---',
    '(không có)' => '(not available)',
    'Đã xóa' => 'Deleted',
    'Trạng thái khiếu nại' => 'Complaint status',
    'Thời gian xem' => 'Watch time',
    '--- Chọn thể loại ---' => '--- Select a category ---',
    'Khác' => 'Other',
    'Cấp quyền' => 'Authorization',
    'Mật khẩu' => 'Password',
    'Mã xác nhận' => 'Verification code',
    'Top 20' => 'Top 20',
    'Top 50' => 'Top 50',
    'Top 100' => 'Top 100',
    'Top 500' => 'Top 500',

    //update28.3
    'Duyệt hợp đồng' => 'Approve contract',
    'Từ chối hợp đồng' => 'Deny contract',
    'Tải mẫu hợp đồng' => 'Download the contract form',
    'Bạn có chắc chắn DUYỆT HỢP ĐỒNG này' => 'Are you sure APPROVE THIS CONTRACT',
    'Bạn có chắc chắn TỪ CHỐI DUYỆT hợp đồng này' => 'Are you sure  DENY THIS CONTRACT',

    //29.3
    'Kênh hiển thị' => 'Channel display',
    'Phải chọn kênh cần thêm' => 'Must select a channel to add',
    'Người cập nhật' => 'Updated person',
    'Duyệt hợp đồng thành công' => 'Browse successfully contract',
    'Từ chối duyệt hợp đồng thành công' => 'Refuse to approve the contract successfully',
    'Chỉnh sửa thành công' => 'Edit successfully',
    'Chọn tệp' => 'Choose file',
    'Gửi duyệt lại tin nhắn thành công' => 'Request to approve the message successfully',
    'Duyệt tin nhắn thành công' => 'Approve successfully messages',
    'Hạ tin nhắn thành công' => 'Lower successfully messages',
    'Mở lại tin nhắn đã hạ thành công' => 'Reopen lowered messages successfully',
    'Bạn phải chọn 1 lý do từ chối duyệt!' => 'You must choose 1 reason to refuse to approve!',
    'Thông báo' => 'Notification',
    'Yêu cầu duyệt lại tin nhắn thành công' => 'Request to review the message successfully',
    'Hiển thị <b>{begin}-{end}</b> của <b>{totalCount} </b> bản ghi' =>'Showing<b> {begin}-{end}</b> of <b>{totalCount}</b> items',
    'Chỉnh sửa' => 'Edit',
    //11.11 muclip view sms
    'Lượt Xem Mới' => 'New Views',
    'Lượt Xem Lũy Kế' => 'Cumulative Views',
    'Mã nhúng'=>'Embed',
    'kênh' => 'Channel',
    'Báo cáo lượt xem của video theo ngày' => 'Report video views by day',
    'Báo cáo lượt xem của user theo ngày' => 'Report user views by day',
    'Báo cáo lượt xem của kênh theo ngày' => 'Report channel views by day',
    'Báo cáo lượt xem của danh mục theo ngày' => 'Report category views by day',
    'Tên video' => 'Video name',
    'Tên user' => 'User name',
    'Tên category' => 'Category name',
    'Tên channel' => 'Channel name',
    'Tiêu chí' => 'Criterion',
    'Số liệu báo cáo trong ngày ' => 'Data reported for the day ',
    'Tổng view lượt view lũy kế ' => 'Total views accumulated views',
    'Tool duyệt nhanh thông tin kênh' => 'Create channel information',
    'Tạo kênh' => 'Create new channel',
    'Mô tả' => 'Description',
    ' không được để trống' => ' cannot be blank',
    'Vui lòng chọn ảnh {extension}' => 'Please select image with extensions {extension}',
];
