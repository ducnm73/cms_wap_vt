<?php

namespace common\libs;

use common\helpers\S3;
use common\helpers\TimeoutSoapClient;
use common\helpers\Utils;
use common\models\VtConfigBase;
use Yii;

class VtService
{

    const SYSTEM_ERROR = 999;
    const PAYMENT_SUCCESS = 0;

    const PAYMENT_ACTION_BUY = 'BUY';
    const PAYMENT_ACTION_VIEW = 'VIEW';


    /**
     * Goi VAAA nhan dien so dien thoai theo IP
     * @author ducda2@viettel.com.vn
     * @param $ip
     * @return string
     */

    public static function getGprsMsisdn($ip)
    {
		//var_dump($ip);die();//10.255.62.240
        $startTime = Utils::current_millis();
        if (empty($ip)) {
            \Yii::error('{VAAA} Loi lay MSISDN do thieu IP', 'vaaa');
            return '';
        }
		//var_dump($ip);die();//10.255.62.240
        $arrMsisdnService = \Yii::$app->params['service.vaaa'];
		//var_dump($arrMsisdnService);die();//array(5){["username"]=>string(6)"myclip" ["Password"]=>string(12)"myclip!123456" ["wsdl"]=>string(59)"/u01/congphim/apps/myclip-web-lao/common/config/radius.wsdl" ["connectTimeout"]=>int(5) ["readTimeout"]=>int(5)}
        $wsdl = $arrMsisdnService['wsdl'];
		//var_dump($wsdl);die();
        $options = array(
            'connect_timeout' => \Yii::$app->params['service.vaaa']['connectTimeout'],
            'timeout' => \Yii::$app->params['service.vaaa']['readTimeout']
        );
		//var_dump($options);die();
        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
			//var_dump($ip);die();//10.255.62.240
			//var_dump($arrMsisdnService['username']);die();//myclip
			//var_dump($arrMsisdnService['password']);die();//myclip!123456
            $result = $client->__soapCall('getMSISDN', array
						(
							array(
								'username' => $arrMsisdnService['username'], 
								'password' => $arrMsisdnService['password'], 
								'ip' => $ip
								)
						));
			//var_dump($result);die();//object(stdClass)#129(1){["return"]=>object(stdclass)#130(2){["code"]=>int(4)["desc"]=>string(30)""IP does not belong to any pool"}}
        } catch (\Exception $e) {
            \Yii::error('{VAAA} Lay MSISDN gap ngoai le: ' . $e->getMessage(), 'vaaa');
        }
		//var_dump($result);die(xxx);
        if (is_object($result)) {
			//var_dump($result);die();
            if ($result->return->code == 0) {
				//var_dump("123");die();
                $ipPool = $ip . str_repeat(" ", 15 - strlen($ip));
                $msisdn = $result->return->desc . str_repeat(" ", 12 - strlen($result->return->desc));

                \Yii::info('{VAAA}' . $ipPool . '|' . $msisdn . '|' . (Utils::current_millis() - $startTime), 'vaaa');

                return $result->return->desc;
            } else {
                \Yii::info('{VAAA}' . $ip . ' -> ' . $result->return->desc, 'vaaa');
                return '';
            }

        } else {
            \Yii::error('{VAAA}' . $ip . ' -> response no object!', 'vaaa');
            return '';
        }
    }

    /**
     * Goi webservice Recommender system, lay ra noi dung KH co the thich dua theo hanh vi su dung
     * @param $msisdn
     * @param int $limit
     * @param null $type
     * @return mixed
     */
    public static function getRecommendContent($msisdn, $limit = 10, $type = null)
    {
        $url = \Yii::$app->params['service.recommend'] . '?type=' . $type . '&limit=' . $limit . '&user_id=' . $msisdn;

        try {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);

            \Yii::info('{RECOMMEND} msisdn:' . $msisdn . '|limit:' . $limit . '|data:' . $data, 'recommend');

            return json_decode($data);
        } catch (\Exception $e) {
            \Yii::error('{RECOMMEND} Exception: ' . $e->getMessage(), 'recommend');
        }

    }

    public static function registerService($msisdn, $packageid, $source)
    {
        $isConfirm = 0;
        //Kiem tra cau hinh xem co can confirm dang ky hay khong?

        switch (Yii::$app->id) {
            case 'app-wap':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                break;
            case 'app-api':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                break;
            case 'app-web':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                break;
        }

        //Kiem tra source, neu thuoc whitelist thi khong can conffimr (GA)
        $whiteList = VtConfigBase::getConfig('register.whitelist.no.confirm');
        if (strpos(strtoupper($whiteList), strtoupper($source)) !== false) {
            $isConfirm = 0;
        }

        $arrGateway = \Yii::$app->params['gateway'];
        $wsdl = $arrGateway['register.url'];
        $options = array(
            'connect_timeout' => $arrGateway['connectTimeout'],
            'timeout' => $arrGateway['readTimeout']
        );

        \Yii::info('{REGSITER} INPUT : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $isConfirm, 'service');
        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
            $result = $client->__soapCall('register', array(
                array(
                    'username' => $arrGateway['register.username'],
                    'password' => $arrGateway['register.password'],
                    'msisdn' => $msisdn,
                    'packageid' => $packageid,
                    'source' => $source,
                    'is_confirm' => $isConfirm
                )
            ));
        } catch (\Exception $e) {
            \Yii::error('{REGISTER} : ' . $msisdn . "|" . $packageid . "|" . $source . $e->getMessage(), 'service');
        }

        if (is_object($result)) {
            $errorCode = $result->return;
            \Yii::info('{REGISTER} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $errorCode, 'service');
            return $errorCode;
        } else {
            \Yii::error('{REGISTER} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . "PARSE ERROR", 'service');
            return self::SYSTEM_ERROR;
        }

    }

    public static function cancelService($msisdn, $packageid, $source)
    {
        $arrGateway = \Yii::$app->params['gateway'];
        $wsdl = $arrGateway['cancel.url'];
        $options = array(
            'connect_timeout' => $arrGateway['connectTimeout'],
            'timeout' => $arrGateway['readTimeout']
        );
        \Yii::info('{CANCEL} INPUT: ' . $msisdn . "|" . $packageid . "|" . $source, 'service');

        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
            $result = $client->__soapCall('cancel', array(
                array(
                    'username' => $arrGateway['cancel.username'],
                    'password' => $arrGateway['cancel.password'],
                    'msisdn' => $msisdn,
                    'packageid' => $packageid,
                    'source' => $source
                )
            ));
        } catch (\Exception $e) {
            \Yii::error('{CANCEL} : ' . $msisdn . "|" . $packageid . "|" . $source . $e->getMessage(), 'service');
        }

        if (is_object($result)) {
            $errorCode = $result->return;
            \Yii::info('{CANCEL} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $errorCode, 'service');
            return $errorCode;
        } else {
            \Yii::error('{CANCEL} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . "PARSE ERROR", 'service');
            return self::SYSTEM_ERROR;
        }

    }

    public static function paymentService($action, $msisdn, $fee, $itemId, $type, $source, $cp = '', $prefix = '', $subType = '', $command = '', $subService = '')
    {
        $arrGateway = \Yii::$app->params['gateway'];
        $wsdl = $arrGateway['buyretail.url'];
        $options = array(
            'connect_timeout' => $arrGateway['connectTimeout'],
            'timeout' => $arrGateway['readTimeout']
        );

        \Yii::info('{PAYMENT} INPUT : ' . $msisdn . "|" . $itemId . "|" . $type . "|" . $fee . "|" . $source . "|" . $subService, 'service');
        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
            $result = $client->__soapCall('chargeRetail', array(
                array(
                    'username' => $arrGateway['buyretail.username'],
                    'password' => $arrGateway['buyretail.password'],
                    'msisdn' => $msisdn,
                    'amount' => $fee,
                    'action' => $action,
                    'content' => $itemId,
                    'otherinfo' => $source,
                    'subCpName' => $cp,
                    'category' => $prefix,
                    'item' => $subType,
                    'command' => $command,
                    'subService' => $subService
                )
            ));
        } catch (\Exception $e) {
            \Yii::error('{PAYMENT} : ' . $msisdn . "|" . $itemId . "|" . $type . "|" . $fee . "|" . $source . "|" . $cp . "|" . $prefix . "|" . $subType . "|" . $command . "|" . $subService . "|" . $e->getMessage(), 'service');
        }

        if (is_object($result)) {
            $errorCode = $result->return;
            \Yii::info('{PAYMENT} : ' . $msisdn . "|" . $itemId . "|" . $type . "|" . $fee . "|" . $source . "|" . $cp . "|" . $prefix . "|" . $subType . "|" . $command . "|" . $subService . "|" . $errorCode, 'service');
            return $errorCode;
        } else {
            \Yii::error('{PAYMENT} : ' . $msisdn . "|" . $itemId . "|" . $type . "|" . $fee . "|" . $source . "|" . $cp . "|" . $prefix . "|" . $subType . "|" . $command . "|" . $subService . "|" . "PARSE ERROR", 'service');
            return self::SYSTEM_ERROR;
        }

    }


    public static function registerNoRenew($msisdn, $packageid, $source)
    {
        $isConfirm = 0;

        $arrGateway = \Yii::$app->params['gateway'];
        $wsdl = $arrGateway['register.url'];
        $options = array(
            'connect_timeout' => $arrGateway['connectTimeout'],
            'timeout' => $arrGateway['readTimeout']
        );

        \Yii::info('{REGISTER NO RENEW} INPUT : ' . $msisdn . "|" . $packageid . "|" . $source, 'service');
        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
            $result = $client->__soapCall('registerNoRenew', array(
                array(
                    'username' => $arrGateway['register.username'],
                    'password' => $arrGateway['register.password'],
                    'msisdn' => $msisdn,
                    'packageid' => $packageid,
                    'source' => $source,
                    'is_confirm' => $isConfirm
                )
            ));
        } catch (\Exception $e) {
            \Yii::error('{REGISTER NO RENEW} : ' . $msisdn . "|" . $packageid . "|" . $source . $e->getMessage(), 'service');
        }
        if (is_object($result)) {
            $errorCode = $result->return;
            \Yii::info('{REGISTER NO RENEW} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $errorCode, 'service');
            return $errorCode;
        } else {
            \Yii::error('{REGISTER NO RENEW} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . "PARSE ERROR", 'service');
            return self::SYSTEM_ERROR;
        }

    }


    public static function registerNoWaitService($msisdn, $packageid, $source)
    {
        $isConfirm = 0;
        //Kiem tra cau hinh xem co can confirm dang ky hay khong?

        switch (Yii::$app->id) {
            case 'app-wap':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                break;
            case 'app-api':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                break;
            case 'app-web':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                break;
        }

        //Kiem tra source, neu thuoc whitelist thi khong can conffimr (GA)
        $whiteList = VtConfigBase::getConfig('register.whitelist.no.confirm');
        if (strpos(strtoupper($whiteList), strtoupper($source)) !== false) {
            $isConfirm = 0;
        }

        $arrGateway = \Yii::$app->params['gateway'];
        $wsdl = $arrGateway['register.url'];
        $options = array(
            'connect_timeout' => $arrGateway['connectTimeout'],
            'timeout' => $arrGateway['readTimeout']
        );

        \Yii::info('{REGISTER NO WAIT} INPUT : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $isConfirm, 'service');
        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
            $result = $client->__soapCall('registerNoWait', array(
                array(
                    'username' => $arrGateway['register.username'],
                    'password' => $arrGateway['register.password'],
                    'msisdn' => $msisdn,
                    'packageid' => $packageid,
                    'source' => $source,
                    'is_confirm' => $isConfirm
                )
            ));
        } catch (\Exception $e) {
            \Yii::error('{REGISTER NO WAIT} : ' . $msisdn . "|" . $packageid . "|" . $source . $e->getMessage(), 'service');
        }

        if (is_object($result)) {
            $errorCode = $result->return;
            \Yii::info('{REGISTER NO WAIT} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $errorCode, 'service');
            return $errorCode;
        } else {
            \Yii::error('{REGISTER NO WAIT} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . "PARSE ERROR", 'service');
            return self::SYSTEM_ERROR;
        }

    }

    public static function registerServiceWithFee($msisdn, $packageid, $source, $fee)
    {
        $isConfirm = 0;
        //Kiem tra cau hinh xem co can confirm dang ky hay khong?

        switch (Yii::$app->id) {
            case 'app-wap':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                break;
            case 'app-api':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                break;
            case 'app-web':
                $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                break;
            case 'app-backend':
                $isConfirm = 0;
                break;
        }

        //Kiem tra source, neu thuoc whitelist thi khong can conffimr (GA)
        $whiteList = VtConfigBase::getConfig('register.whitelist.no.confirm');
        if (strpos(strtoupper($whiteList), strtoupper($source)) !== false) {
            $isConfirm = 0;
        }

        $arrGateway = \Yii::$app->params['gateway'];
        $wsdl = $arrGateway['register.url'];
        $options = array(
            'connect_timeout' => $arrGateway['connectTimeout'],
            'timeout' => $arrGateway['readTimeout']
        );

        \Yii::info('{REGSITER} INPUT : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $isConfirm, 'service');
        $result = null;
        try {
            $client = new TimeoutSoapClient($wsdl, $options);
            $result = $client->__soapCall('register', array(
                array(
                    'username' => $arrGateway['register.username'],
                    'password' => $arrGateway['register.password'],
                    'msisdn' => $msisdn,
                    'packageid' => $packageid,
                    'source' => $source,
                    'is_confirm' => $isConfirm,
                    'fee' => $fee
                )
            ));
        } catch (\Exception $e) {
            \Yii::error('{REGISTER} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $fee . $e->getMessage(), 'service');
        }

        if (is_object($result)) {
            $errorCode = $result->return;
            \Yii::info('{REGISTER} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $fee . "|" . $errorCode, 'service');
            return $errorCode;
        } else {
            \Yii::error('{REGISTER} : ' . $msisdn . "|" . $packageid . "|" . $source . "|" . $fee . "|" . "PARSE ERROR", 'service');
            return self::SYSTEM_ERROR;
        }

    }

    public static function insertNewContract(
        $name,
        $contractNameB,
        $contractEmailB,
        $contractNo,
        $contractSignDate,
        $startIssueDate,
        $endIssueDate,
        $partnerName,
        $fileContent,
        $fileName
    )
    {
        //Data, connection, auth
        $soapUrl = Yii::$app->params['ws.contracts.url']; // asmx URL of WSDL

        $userCreateId = Yii::$app->params['ws.contract.userCreateId'];
        $username = Yii::$app->params['ws.contract.username'];
        $password = Yii::$app->params['ws.contract.password'];

        // xml post structure
        $xmlStr = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ws='http://WS.htds.viettel.com/'>
           <soapenv:Header/>
           <soapenv:Body>
              <ws:insertContract>
                 <files>
                     <fileContent>
                        $fileContent                     
                    </fileContent>
                    <fileName>
                        $fileName
                    </fileName>
                 </files>
                 <contract>
                    <name>$name</name>
                    <attachFile>1</attachFile>
                    <checkLogo>1</checkLogo>
                    <contractEmailA></contractEmailA>
                    <contractEmailB>$contractEmailB</contractEmailB>
                    <contractId></contractId>
                    <contractNameA></contractNameA>
                    <contractNameB>$contractNameB</contractNameB>
                    <contractNo>$contractNo</contractNo>
                    <contractSignDate>$contractSignDate</contractSignDate>
                    <contractTelA></contractTelA>
                    <contractType>1</contractType>
                    <createDate></createDate>
                    <endIssueDate>$endIssueDate</endIssueDate>
                    <icCycle>1</icCycle>
                    <icMethod>1</icMethod>
                    <icNumber>30</icNumber>
                    <isExternalUnit>1</isExternalUnit>
                    <lockStatus>0</lockStatus>
                    <newAttachFileName></newAttachFileName>
                    <partnerCode></partnerCode>
                    <partnerId></partnerId>
                    <partnerName>$partnerName</partnerName>
                    <paymentMethod>1</paymentMethod>
                    <serviceCode>MYCLIP</serviceCode>
                    <serviceId></serviceId>
                    <serviceName>MYCLIP</serviceName>
                    <startIssueDate>$startIssueDate</startIssueDate>
                    <status>1</status>
                    <userCreateId>$userCreateId</userCreateId>
                 </contract>
                 <authentication>
                    <password>$password</password>
                    <username>$username</username>
                 </authentication>
              </ws:insertContract>
           </soapenv:Body>
        </soapenv:Envelope>";   // data from the form, e.g. some ID number

        Yii::info("SEND CONTRACT: ".$xmlStr, "service");


        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($xmlStr),
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // converting
        $response = curl_exec($ch);
        curl_close($ch);
        // converting
        $start = strpos($response, "<return>");
        $end = strpos($response, "</return>");
        // convertingc to XML
        $parser = simplexml_load_string(substr($response, $start, $end - $start + 9));

        Yii::info( "INSERT CONTRACT RESPONSE: ". substr($response, $start, $end - $start + 9), 'service');
        //Insert hop dong thanh cong
        if (isset($parser->id)) {
            return [
                "errorCode"=>0,
                "contractId"=>$parser->id,
                "errorMessage"=>Yii::t('wap','Thành công')
            ];

        } else if(isset($parser->errorCode)) {
            return [
                "errorCode"=>$parser->errorCode,
                "errorMessage"=>Yii::t('backend', "Gửi hợp đồng không thành công") . ': ' .$parser->errorMessage
            ];
        }else{
            return [
                "errorCode"=>999,
                "errorMessage"=>Yii::t('backend', "Gửi hợp đồng không thành công")
            ];
        }

    }

    public static function drmAddDevice($networkDeviceId, $deviceType, $source) {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout']
        );

        $request = array(
            'username' => $service['username'],
            'password' => $service['password'],
            'networkDeviceId' => $networkDeviceId,
            'deviceType' => $deviceType,
            'entityType' => $service['entityType'],
            'smsPackageId' => $service['smsPackageId'],
            'smsNetworkId' => $service['smsNetworkId'],
            'source' => $source,
            'prefix' => $service['prefix']
        );

        $response = null;
        try {

            $client = new TimeoutSoapClient($service['endPointDrmGateway'], $options);
            $response = $client->__soapCall('addDevice', array($request));
        } catch (\Exception $e) {
            \Yii::error('{DRM} drmAddDevice exception: ' . $e->getMessage(), 'drm');
        }

        if (is_object($response)) {
            \Yii::info('{DRM} drmAddDevice add device: ' . $networkDeviceId . '| Result: ' . $response->result->errorCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return $response->return;
        } else {
            \Yii::error('{DRM} drmAddDevice add device| Result: Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }
}
