<?php
/**
 * Created by PhpStorm.
 * User: bachpv1
 * Date: 2/20/19
 * Time: 09:51
 */

namespace common\libs;


use Yii;
use yii\httpclient\Client;
use yii\httpclient\Exception;

class MpsHelper
{
    const CHARACTERS = 'abcdefghijuklmno0123456789012345';
    const DETECT_URL_KEY = 'detect_url';
    const CHARGE_URL_KEY = 'charge_url';
    const OTP_URL_KEY = 'otp_url';
    const OTP_TYPE_GET = 0;
    const OTP_TYPE_VERIFY = 1;
    const CMD_REGISTER = 'REGISTER';
    const CMD_CANCEL = 'CANCEL';
    const CMD_MOBILE = 'REGISTER';
    const SOURCE_WAP = 'WAP';
    const SOURCE_CLIENT = 'CLIENT';
    const PREFIX_LENGTH = 3;
    const SUBFIX_PAD = '000';
    const BLANK_VALUE = 'BLANK';

    /**
     * Đăng ký
     *
     * @param $subService string Mã gói cước trên hệ thống
     * @param $msisdn string
     * @param $source string
     * @return bool|string Chuỗi trả về có dạng DATA=<chuỗi mã hóa>, sau khi giải mã sẽ được chuỗi:
     * REQ=<mã request>&RES=<mã lỗi>&MOBILE=<số thuê bao>&PRICE=<số tiền>&CMD=CHARGE&SOURCE=<nguồn gọi>
     */
    public static function register($subService, $msisdn, $source = self::SOURCE_CLIENT)
    {
        Yii::info('Goi mps register: SubService=' . $subService . '/msisdn=' . $msisdn . '/source=' . $source);
        $data = [
            'SUB' => $subService,
            'PRICE' => 0,
            'MOBILE' => $msisdn,
            'SOURCE' => $source,
        ];
        $queryData = [
            'SUB' => $subService,
            'CMD' => self::CMD_REGISTER
        ];
        return self::callService(self::CHARGE_URL_KEY, $queryData, $data);
    }

    /**
     * Hủy dịch vụ
     * @param $subService
     * @param $msisdn
     * @param $source
     * @return bool|string Chuỗi trả về có dạng DATA=<chuỗi mã hóa>
     */
    public static function cancel($subService, $msisdn, $source = self::SOURCE_CLIENT)
    {
        Yii::info('Goi mps cancel: SubService=' . $subService . '/msisdn=' . $msisdn . '/source=' . $source);
        $data = [
            'SOURCE' => $source,
            'MOBILE' => $msisdn,
        ];
        $queryData = [
            'SUB' => $subService,
            'CMD' => self::CMD_CANCEL
        ];
        return self::callService(self::CHARGE_URL_KEY, $queryData, $data);
    }

    /**
     * Hàm nhận diện
     *
     * @param string $subService Mã gói cước trên hệ thống
     * @param string $source Nguồn gọi, phải truyền là WAP nếu không sẽ không nhận diện được
     * @return MpsResponse|string
     */
    public static function detectMobile($source = self::SOURCE_CLIENT, $createdUrlOnly = false)
    {
        Yii::info('Goi mps nhan dien thue bao: Source=' . $source);
        $mpscfg = Yii::$app->params['mps'];
        $subService = $mpscfg['default_sub_service'];
        $data = [
//            'SUB' => $subService, // khong can truyen nhung tai lieu mo ta co can
            'SOURCE' => $source,
        ];
        $queryData = [
            'SUB' => $subService,
            'CMD' => self::CMD_MOBILE
        ];
        return self::callService(self::DETECT_URL_KEY, $queryData, $data, $createdUrlOnly);
    }

    /**
     * Gửi tin otp về cho thuê bao
     * @param $msisdn
     * @param string $source
     * @return bool Trả về true nếu gửi thành công, false nếu không thành công
     */
    public static function sendOtp($msisdn, $source = self::SOURCE_CLIENT)
    {
        Yii::info('Goi mps gui ma OTP: msisdn=' . $msisdn . '/source=' . $source);
        $resp = self::otp($msisdn, $source);
        return $resp !== false ? $resp->isSendOtpSuccess() : false;
    }

    /**
     * Xác thực mã otp đã gửi về cho thuê bao
     * @param $msisdn
     * @param $otp
     * @param string $source
     * @return MpsResponse|bool Trả về false nếu có lỗi, trả về object MpsResponse nếu gọi thành công
     */
    public static function verifyOtp($msisdn, $otp, $source = self::SOURCE_CLIENT)
    {
        Yii::info('Goi mps xac thuc ma OTP: msisdn=' . $msisdn . '/otp=' . $otp . '/source=' . $source);
        $resp = self::otp($msisdn, $source, self::OTP_TYPE_VERIFY, $otp);
        return $resp;
    }

    /**
     * @param $msisdn
     * @param $source
     * @param int $type
     * @param string $otp
     * @return MpsResponse|string Trả về mã lỗi 100 nếu gửi thành công tới thuê bao
     */
    private static function otp($msisdn, $source, $type = self::OTP_TYPE_GET, $otp = '')
    {
        $mpscfg = Yii::$app->params['mps'];
        $subService = $mpscfg['default_sub_service'];
        $data = [
            'SOURCE' => $source,
            'OTP' => self::BLANK_VALUE,
            'OTP_TYPE' => $type,
            'MOBILE' => $msisdn,
            'PRICE' => 0
        ];

        if ($type == self::OTP_TYPE_VERIFY) {
            $data['OTP'] = $otp;
        }

        $queryData = [
            'SUB' => $subService,
            'CMD' => self::CMD_MOBILE
        ];
        return self::callService(self::OTP_URL_KEY, $queryData, $data);
    }

    /**
     * @param string $urlKey key chứa url trong cấu hình
     * @param array $queryData Có dạng [key1 => value1, key2 => value2]
     *              Ví dụ ['PRO' => 'PROVIDER', 'SER' => 'SERVICE1']
     * @param array $data Có dạng [key1 => value1, key2 => value2]
     * @return MpsResponse|string
     */
    private static function callService($urlKey, $queryData = array(), $data = array(), $createdUrlOnly = false)
    {
        $mpscfg = Yii::$app->params['mps'];
        $url = $mpscfg[$urlKey];
        $pub_key = 'file://' . $mpscfg['public_key'];
        $pri_key = 'file://' . $mpscfg['private_key'];
        $pub_key_vt = 'file://' . $mpscfg['public_key_vt'];
        $options = [];
        $useCurl = false;
        $queryData = array_merge($queryData, [
            'PRO' => $mpscfg['provider'],
            'SER' => $mpscfg['service'],
        ]);

        if (isset($mpscfg['proxy']) && isset($mpscfg['proxy_port'])) {
            $options = [
                CURLOPT_PROXY => $mpscfg['proxy'],
                CURLOPT_PROXYPORT => $mpscfg['proxy_port'],
            ];
            $useCurl = true;
        }

        $data = array_merge($data, [
            'CATE' => self::BLANK_VALUE,
            'ITEM' => self::BLANK_VALUE,
            'SUB_CP' => self::BLANK_VALUE,
            'CONT' => self::BLANK_VALUE,
            'TYPE' => 'MOBILE',
            'IMEI' => self::BLANK_VALUE,
            'SESS' => self::generateRequestCode($mpscfg['cp_code']),
            'REQ' => self::generateRequestCode($mpscfg['cp_code'])
        ]);
        $result = self::__callMps($url, $pub_key, $pri_key, $pub_key_vt, $queryData, $data, $options, $useCurl, $createdUrlOnly);

        if ($createdUrlOnly === true) {
            return $result;
        }

        if (self::decryptData($result, $decrypted, $pri_key)) {
            //TODO fake
//            $decrypted['res'] = 0;
            $resp = new MpsResponse($urlKey, $decrypted);
            Yii::info('Ma loi mps tra ve: ' . $resp->getCode(), 'service');
            return $resp;
        } else {
            return false;
        }
    }

    /**
     * @param string $url
     * @param array $queryData Mảng các tham số truyền trên url dạng [key1 => value1, key2 => value2]
     * @param array $data Mảng các tham số cần mã hóa
     * @param string $pub_key
     * @param string $pri_key
     * @param array $options
     * @param boolean $useCurl
     * @return boolean|string Trả về false nếu có lỗi, content nếu thành công
     */
    private static function __callMps($url, $pub_key, $pri_key, $pub_key_vt, $queryData = array(), $data = array(), $options = array(), $useCurl = false, $createdUrlOnly = false)
    {
        Yii::info('Goi mps:' . $url . '/Query:' . json_encode($queryData) . '/Data:' . json_encode($data) . '/Option:' . json_encode($options) . '/Curl:' . $useCurl, 'service');
        $url = self::createMpsUrl($url, $pub_key, $pri_key, $pub_key_vt, $queryData, $data);
        Yii::info('Url mps: ' . $url, 'service');

        if ($createdUrlOnly === true) {
            Yii::info('Return url only', 'service');
            return $url;
        }

        $client = new Client();
        $req = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url);

        if ($useCurl) {
            $client->transport = 'yii\httpclient\CurlTransport';
        }

        $req->setOptions($options);

        try {
            $res = $req->send();
        } catch (Exception $e) {
            Yii::error('Goi mps khong thanh cong: ' . $e->getMessage(), 'service');
            return false;
        }

        Yii::info('Content mps tra ve: ' . $res->getContent(), 'service');

        if ($res->isOk) {
            return $res->getContent();
        } else {
            Yii::error('Goi mps khong thanh cong', 'service');
            return false;
        }
    }

    private static function createMpsUrl($baseUrl, $pub_key, $pri_key, $pub_key_vt, $queryData = array(), $data = array())
    {
        $dataTextArray = [];

        foreach ($data as $key => $value) {
            $dataTextArray[] = $key . '=' . $value;
        }

        $dataText = implode('&', $dataTextArray);
        $encryptedData = self::encryptData($dataText, $pub_key, $pri_key, $pub_key_vt);

        if ($encryptedData === false) {
            return $encryptedData;
        }

        $data_encode = $encryptedData['data_encode'];
        $signature_encode = $encryptedData['signature_encode'];
        $mergeQueryData = [
            'DATA' => $data_encode,
            'SIG' => $signature_encode,
        ];

        $mergeQueryData = array_merge($queryData, $mergeQueryData);
        $queryDataTextArray = [];

        foreach ($mergeQueryData as $key => $value) {
            $queryDataTextArray[] = $key . '=' . $value;
        }

        $queryDataText = implode('&', $queryDataTextArray);
        return $baseUrl . '?' . $queryDataText;
    }

    private static function generateRequestCode($cpCode)
    {
        if (strlen($cpCode) > self::PREFIX_LENGTH) {
            $cpCodePre = substr($cpCode, 0, self::PREFIX_LENGTH);
        } else if (strlen($cpCode) < self::PREFIX_LENGTH) {
            $mergePre = self::SUBFIX_PAD . $cpCode;
            $cpCodePre = substr($mergePre, strlen($mergePre) - self::PREFIX_LENGTH, self::PREFIX_LENGTH);
        } else {
            $cpCodePre = $cpCode;
        }

        $result = sprintf('%s%s', $cpCodePre, date('ymdHis') . substr(round(microtime() * 1000), 0, 3));
        return $result;
    }

    private static function encryptData($data, $pub_key, $pri_key, $pub_key_vt)
    {
        Yii::info('Ma hoa du lieu RSA', 'service');
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $aes = new AES(self::CHARACTERS, 'CBC', $iv);
        $encrypted = $aes->encrypt();
        $aeskey = bin2hex($encrypted);
        $data = self::pkcs5_pad($data, 16);
        $value_encrypt_aes = self::encrypt($data, $aeskey);
        Yii::info('Chuoi ma hoa aes: ' . $value_encrypt_aes, 'service');
        $value_with_key = 'value=' . $value_encrypt_aes . '&key=' . $aeskey;
        Yii::info('Chuoi ma hoa cap key/value aes: ' . $value_with_key, 'service');

        if (!openssl_public_encrypt($value_with_key, $data_encrypted, $pub_key_vt)) {
            Yii::error('Ma hoa openssl_public_encrypt khong thanh cong', 'service');
            return false;
        }

        $data_encrypted = base64_encode($data_encrypted);
        $data_encode = urlencode($data_encrypted);
        $signature = '';
        openssl_sign($data_encrypted, $signature, $pri_key, OPENSSL_ALGO_SHA1);
        $signature = base64_encode($signature);
        $signature = urlencode($signature);

        if (openssl_verify($data_encrypted, base64_decode(urldecode($signature)), $pub_key, OPENSSL_ALGO_SHA1) !== 1) {
            Yii::error('Xac thu openssl_verify khong thanh cong', 'service');
            return false;
        }

        Yii::info('Ma hoa thanh cong, cap data/signature: ' . $data_encode . '/' . $signature, 'service');
        return [
            'data_encode' => $data_encode,
            'signature_encode' => $signature,
        ];
    }

    /**
     * Giải mã dữ liệu mã hóa MPS trả về
     * @param $data string Dữ liệu mã hóa MPS trả về
     * @return bool|MpsResponse
     */
    public static function decryptAvailData($data)
    {
        $mpscfg = Yii::$app->params['mps'];
        $pri_key = 'file://' . $mpscfg['private_key'];

        if (self::decryptData($data, $decrypted, $pri_key)) {
            //TODO fake
//            $decrypted['res'] = '0';
//            $decrypted['mobile'] = '51917820004';
            $resp = new MpsResponse(self::DETECT_URL_KEY, $decrypted);
            Yii::info('Ma loi mps tra ve: ' . $resp->getCode(), 'service');
            return $resp;
        } else {
            return false;
        }
    }

    public static function decryptData($data, &$result, $pri_key)
    {
        Yii::info('Giai ma RSA: ' . $data, 'service');
        $data = self::parseResponse($data);

        if ($data === false) {
            return $data;
        }

        if (!openssl_private_decrypt(base64_decode($data), $decrypted, $pri_key)) {
            Yii::error('Giai ma openssl_private_decrypt khong thanh cong', 'service');
            return false;
        }

        Yii::info('Giai ma thanh cong: ' . $decrypted, 'service');
        $decryptedArray = explode('&', $decrypted);
        $valueItem = $decryptedArray[0];
        $value = explode('=', $valueItem)[1];
        $keyItem = $decryptedArray[1];
        $key = explode('=', $keyItem)[1];
        $rsaDecrypted = self::decrypt($value, $key);
        Yii::info('Giai ma aes thanh cong: ' . $rsaDecrypted, 'service');
        $rsaDecryptedArray = explode('&', $rsaDecrypted);

        foreach ($rsaDecryptedArray as $item) {
            $value = explode('=', $item);
            $result[strtolower($value[0])] = $value[1];
        }

        return true;
    }

    private static function encrypt($encrypt, $key)
    {
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, pack("H*", $key), $encrypt, MCRYPT_MODE_ECB, $iv));
        return $encrypted;
    }

    private static function decrypt($decrypt, $key)
    {
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, pack("H*", $key), base64_decode($decrypt), MCRYPT_MODE_ECB, $iv);
        return $decrypted;
    }

    private static function pkcs5_pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    /**
     * Hàm parse dữ liệu trả về từ MPS
     * @param $content string
     * @return bool|string
     */
    private static function parseResponse($content)
    {
        Yii::info('Parse du lieu: ' . $content, 'service');
        preg_match('/^DATA=(?<data>.*)&SIG=(?<signature>.*)$/', $content, $matches);

        if (!isset($matches['data']) || !isset($matches['signature'])) {
            Yii::error('Du lieu parse khong hop le', 'service');
            return false;
        }

        return $matches['data'];
    }
}
