<?php
/**
 * Created by PhpStorm.
 * User: bachpv1
 * Date: 2/27/19
 * Time: 10:58
 */

namespace common\libs;


use Yii;

class MpsResponse
{
    const CMD_REGISTER = 'REGISTER';
    const CMD_CANCEL = 'CANCEL';
    const CMD_CHARGE = 'CHARGE';
    const CMD_MOBILE = 'MOBILE';
    const CMD_MSISDN = 'MSISDN';

    const SUCCESS_CODE = 0;
    const SEND_OTP_SUCCESS_CODE = 100;
    const VERIFY_OTP_INVALID = 415;
    const VERIFY_OTP_NO_REQUEST = 416;
    const MOBILE_NOT_SUB = 204;
    const MOBILE_IS_SUB = 205;

    private $_action;
    private $_req;
    private $_code;
    private $_msisdn;
    private $_price;
    private $_cmd;
    private $_source;
    private $_desc;
    private $_data;
    private $_message;
    private $_sub;
    private $_sub_start_date;
    private $_sub_end_date;
    private $_status;

    public function __construct($action, $data)
    {
        $this->_action = $action;
        $this->_data = $data;
        $this->__parse();
    }

    private function __parse() {
        $this->_req = $this->_data['req'];
        $this->_code = $this->_data['res'];
        $this->_msisdn = $this->_data['mobile'];
        $this->_price = $this->_data['price'];
        $this->_cmd = $this->_data['cmd'];
        $this->_source = $this->_data['client'];
        $this->_desc = $this->_data['desc'];

        if($this->_action == self::CMD_MSISDN) {
            $this->_sub = $this->_data['sub'];
            $this->_status = $this->_data['status'];
            $this->_sub_start_date = $this->_data['sub_start_date'];
            $this->_sub_end_date = $this->_data['sub_end_date'];
        }

        $this->_message = $this->getMpsMessage();
    }

    private function getMpsMessage() {
        $messages = [
            0 => Yii::t('app', 'Đăng ký/Hủy thành công'),
            11 => Yii::t('app', 'Tham số truyền lên MPS không đúng'),
            202 => Yii::t('app', 'Đăng ký/Hủy bị lỗi'),
            406 => Yii::t('app', 'Nhận diện thuê bao không thành công'),
            204 => Yii::t('app', 'Nhận diện thuê bao thành công'),
            205 => Yii::t('app', 'Nhận diện thuê bao thành công, thuê bao đang sử dụng dịch vụ'),
        ];
        return $messages[$this->_code];
    }

    public function isSuccess() {
        if($this->_cmd == self::CMD_MSISDN) {
            return $this->_code == self::MOBILE_IS_SUB || $this->_code == self::MOBILE_NOT_SUB;
        } else {
            return $this->_code == self::SUCCESS_CODE;
        }
    }

    public function isSendOtpSuccess() {
        return $this->_code == self::SEND_OTP_SUCCESS_CODE;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @return mixed
     */
    public function getMsisdn()
    {
        return $this->_msisdn;
    }

    /**
     * @return mixed
     */
    public function getSub()
    {
        return $this->_sub;
    }

    /**
     * @return mixed
     */
    public function getSubStatus()
    {
        return $this->_status;
    }

    /**
     * @return mixed
     */
    public function getRegisterTime()
    {
        return $this->_sub_start_date;
    }

    /**
     * @return mixed
     */
    public function getExpireTime()
    {
        return $this->_sub_end_date;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->_message;
    }

    function __toString()
    {
        return json_encode($this);
    }
}