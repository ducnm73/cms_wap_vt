<?php

namespace common\libs;

use Aws\CloudFront\Exception\Exception;
use common\helpers\Utils;
use yii\imagine\Image;

use Imagine\Gmagick\Imagine;
use Imagine\Image\Box;
use Yii;
use common\helpers\Base64;
use yii\log\FileTarget;
use common\helpers\RemoveSignClass;

class VtHelper
{

    const SIZE_COVER = 'cover';
    const SIZE_VIDEO = 'video';

    const SIZE_VIDEO_WEB_HOME = 'video_web_home';
    const SIZE_VIDEO_WAP_HOME = 'video_wap_home';
    const SIZE_VIDEO_LIST_DETAIL = 'video_list_detail';

    const SIZE_FILM = 'film';
    const SIZE_BANNER = 'banner';
    const SIZE_AVATAR = 'avatar';
    const SIZE_CHANNEL = 'channel';
    const SIZE_CHANNEL2 = 'channel2';
    const SIZE_CHANNEL3 = 'channel3';

    const SIZE_CHANNEL_LOGO_LIST = 'channel_logo_list';
    const SIZE_CHANNEL_LOGO_DETAIL = 'channel_logo_detail';

    const SIZE_CATEGORY = 'category';


    public static function generateCode($length)
    {

        $characters = '23456789ABCDEFGHJKLMNPQRSTVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Ham kiem tra IP co nam trong dai IP cho phep khong
     * Tham khao: http://php.net/manual/en/function.ip2long.php
     * @author NamDT5
     * @created on 17/01/2013
     * @param $ip
     * @param $netAddr
     * @param $netMask
     * @return bool
     */
    public static function ipInNetwork($ip, $netAddr, $netMask)
    {

        if ($netMask <= 0) {
            return false;
        }
        $ipBinaryString = sprintf("%032b", ip2long($ip));
        $netBinaryString = sprintf("%032b", ip2long($netAddr));

        return (substr_compare($ipBinaryString, $netBinaryString, 0, $netMask) === 0);
    }

    /**
     * Tra ve so dien thoai thue bao (Su dung 3G Header va Radius)
     * @created on 10 08, 2012
     * @author DucDA2
     * @return string|unknown
     */
    public static function getMsisdn()
    {
        // MeuClip does not require detecting network phone number
        return '';
        $ip = self::getAgentIp();

        if (self::isV_wapIp($ip)) {
            $msisdn = VtService::getGprsMsisdn($ip);
        } else {
            $msisdn = '';
            Yii::error('{VAAA} IP: ' . $ip . ' is not belong any pool', 'vaaa');
        }
        
        if (!empty($_SERVER['HTTP_MSISDN']) && $_SERVER['HTTP_MSISDN'] != 'unknown') {
            if ($_SERVER['HTTP_MSISDN'] == $msisdn) {
                return self::convert8416XTo843X($msisdn);
            } else {
                Yii::error('{VAAA} IP: ' . $ip . '|{HTTP_MSISDN}: ' . $_SERVER['HTTP_MSISDN'] . '|{VAAA}: ' . $msisdn, 'vaaa');
                return '';
            }
        }
    }

    public static function isV_wapIp($ip)
    {   

        $vInternetRange = Yii::$app->params['ip_pool_v_wap'];

        if (!empty($vInternetRange)) {

            foreach ($vInternetRange as $range) {
                $netArr = explode("/", $range);

                if (self::ipInNetwork($ip, $netArr[0], $netArr[1])) {

                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Kiem tra xem IP co thuoc WAF hay khong?!
     * @param $ip
     * @return bool
     */
    public static function checkWhitelistWAF($ip)
    {

        $poolWAF = Yii::$app->params['ip_pool_waf'];
        if (!empty($poolWAF)) {
            foreach ($poolWAF as $range) {
                $netArr = explode("/", $range);
                if (self::ipInNetwork($ip, $netArr[0], $netArr[1])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Tra ve IP cua thue bao
     * @author ducda2@viettel.com.vn
     * @return string
     */
    public static function getAgentIp()
    {
        //Add WAF header
        $remoteIPWAF = @getenv("WAF_SERVER_IP");
        $forwardIPWAF = @getenv("WAF_FORWARD_FOR_IP");

        Yii::info("WAF " . $remoteIPWAF . "|" . $forwardIPWAF);

        if ($remoteIPWAF && $forwardIPWAF && self::checkWhitelistWAF($remoteIPWAF)) {
            $arrIP = explode(",", $forwardIPWAF);
            Yii::info("WAF PASS|" . $remoteIPWAF . "|" . $forwardIPWAF . "|" . trim($arrIP[0]));
            return trim($arrIP[0]);
        }

        $ipString = @getenv("HTTP_MS_IP");

        if (!empty($ipString)) {
            $addr = explode(",", $ipString);
            return $addr[0];
        } else {
            // if(YII_DEBUG === true) {var_dump($_SERVER); die;}
            return isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        }
    }

    /**
     * Ham kiem tra Ip co nam trong dai ip opera cua viettel khong
     * @author dungld5
     * @created on 17/01/2013
     * @param $ip
     * @return bool
     */
    public static function isV_opreraIp($ip)
    {
        $vInternetRange = Yii::$app->params['ip_opera_mini_v_ip'];

        if (!empty($vInternetRange)) {
            foreach ($vInternetRange as $range) {
                $netArr = explode("/", $range);
                if (self::ipInNetwork($ip, $netArr[0], $netArr[1])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getSize($size)
    {
        switch ($size) {
            case self::SIZE_BANNER:
                $width = 720;
                $height = 405;
                break;
            case self::SIZE_COVER:
                $width = 640;
                $height = 360;
                break;
            case self::SIZE_FILM:
                #$width = 233;
                #$height = 350;
                $width = 192;
                $height = 285;
                break;
            case self::SIZE_AVATAR:
                $width = 180;
                $height = 180;
                break;
            case self::SIZE_VIDEO:
                $width = 320;
                $height = 180;
                break;
            case self::SIZE_VIDEO_WEB_HOME:
                $width = 255;
                $height = 145;
                break;
            case self::SIZE_VIDEO_WAP_HOME:
                $width = 375;
                $height = 210;
                break;
            case self::SIZE_VIDEO_LIST_DETAIL:
                $width = 160;
                $height = 90;
                break;

            case self::SIZE_CHANNEL:
                $width = 750;
                $height = 220;
                break;
            case self::SIZE_CHANNEL2:
                $width = 1080;
                $height = 180;
                break;
            case self::SIZE_CHANNEL3:
                $width = 420;
                $height = 70;
                break;
            case self::SIZE_CHANNEL_LOGO_LIST:
                $width = 32;
                $height = 32;
                break;
            case self::SIZE_CHANNEL_LOGO_DETAIL:
                $width = 80;
                $height = 80;
                break;

            case self::SIZE_CATEGORY:
                $width = 949;
                $height = 157;
                break;

            default:
                $width = null;
                $height = null;
        }

        return [
            'width' => $width,
            'height' => $height
        ];
    }

    /**
     * Generate thumb url dua theo kich co
     * @author ducda2@viettel.com.vn
     * @param $bucket
     * @param $path
     * @param $size
     * @return string
     */
    public static function getThumbUrl($bucket, $path, $size, $isFullUrl = true, $isDemo = false)
    {
        //var_dump($path);die;//string(6) "image1"//string(63) "2018/08/29/11/8d649653/8d649653-2878-4c50-91d2-3bbd85f9b246.jpg"
        $sizeDetail = self::getSize($size);

        $pos = strrpos($path, '.');

        if ($pos !== false) {
            $basename = substr($path, 0, $pos);
            $extension = substr($path, $pos + 1);

            if (!isset($sizeDetail['width']) && !isset($sizeDetail['height'])) {
                $thumbName = $basename . '.' . $extension;
            } elseif (!isset($sizeDetail['width'])) {
                $thumbName = $basename . '_auto_' . $sizeDetail['height'] . '.' . $extension;
            } elseif (!isset($sizeDetail['height'])) {
                $thumbName = $basename . '_' . $sizeDetail['width'] . '_auto.' . $extension;
            } else {
                $thumbName = $basename . '_' . $sizeDetail['width'] . '_' . $sizeDetail['height'] . '.' . $extension;
            }

            if ($isDemo) {
                switch ($size) {
                    case self::SIZE_VIDEO:
                        $thumbName = $basename . '_416_234.' . $extension;
                        break;
                    case self::SIZE_VIDEO_WEB_HOME:
                        $thumbName = $basename . '_255_145.' . $extension;
                        break;
                }
            }

            if ($isFullUrl) {
                if(empty($bucket)){
                    return  Yii::$app->params['local.site'].$thumbName;
                }else{
                    return S3Service::generateWebUrl($bucket, $thumbName);
                }
            } else {
                return $thumbName;
            }
        } else {
            return '';
        }
    }

    /**
     * Ham cong tru thoi gian
     * @param type $isSub
     * @param type $cycle
     * @return type
     */
    public static function dateSubOrAdd($cycle, $isSub = false)
    {
        //Lay thoi gian ngay hien tai
        $today = VtHelper::currentCacheTime();

        $sign = "+"; // Cong thoi gian
        if ($isSub) {
            $sign = "-"; // Tru thoi gian
        }
        $date_minus = strtotime(date("Y-m-d H:i:s", strtotime($today)) . " " . $sign . $cycle);
        $date_minus = strftime("%Y-%m-%d %H:%M:%S", $date_minus);
        return $date_minus;
    }

    /**
     * Lay ra thoi diem hien tai, theo block 5,10,15... phut de phuc vu caching
     * @return type
     */
    public static function currentCacheTime()
    {
        $minute = intval(date("i"));
        $cacheMinute = floor($minute / 5) * 5;
        if ($cacheMinute < 10) {
            $cacheMinute = '0' . $cacheMinute;
        }
        return date("Y-m-d H:") . $cacheMinute . ":00";
    }

    public static function isIPInDetectPool()
    {
        //Kiem tra xem IP co thuoc detect pool khong? (Operamini vs 3G)
        $ip = self::getAgentIp();
        if (self::isV_opreraIp($ip) || self::isV_wapIp($ip)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Convert array dang ['a', 'b', 'c'] thanh ['a'=>'a', 'b'=>'b', 'c'=>'c']
     * @param $items
     * @return array
     */
    public static function getObjectTypeArray($items)
    {
        $arr = [];

        foreach ($items as $item) {
            $arr[$item] = $item;
        }

        return $arr;
    }

    public static function generateAllThumb($bucket, $localPath, $thumbPath, $sizeArr = [])
    {
        if (empty($sizeArr)) {
            $sizeArr = [
                self::SIZE_AVATAR,
                self::SIZE_FILM,
                self::SIZE_BANNER,
                self::SIZE_COVER,
                self::SIZE_VIDEO,
                self::SIZE_VIDEO_LIST_DETAIL,
                self::SIZE_VIDEO_WAP_HOME,
                self::SIZE_VIDEO_WEB_HOME,
                self::SIZE_CHANNEL,
                self::SIZE_CHANNEL2,
                self::SIZE_CHANNEL3,
                self::SIZE_CHANNEL_LOGO_DETAIL,
                self::SIZE_CHANNEL_LOGO_LIST,
                self::SIZE_CATEGORY
            ];
        }

        foreach ($sizeArr as $size) {
            $newFileName = self::getThumbUrl($bucket, $thumbPath, $size, false);
            $sizeDetail = self::getSize($size);
            $percent = 1.3;

            try {
                $storageType = Yii::$app->params['storage.type'];
                
                if($storageType == 1) {
                    $thumbTempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName('jpg');
                    Image::getImagine()->open($localPath)->resize(new Box($sizeDetail['width'] * $percent, $sizeDetail['height'] * $percent))->save($thumbTempPath, ['quality' => 85]);
                    $result = S3Service::putObject($bucket, $thumbTempPath, $newFileName, false);
                    unlink($thumbTempPath);
                } else {
                    $savePath = Yii::$app->params['storage.san.rootPath'] . $newFileName;
                    // $savePath =  $newFileName;
                    Image::getImagine()->open($localPath)->resize(new Box($sizeDetail['width'] * $percent, $sizeDetail['height'] * $percent))->save($savePath, ['quality' => 85]);
                }
            } catch (\Exception $ex) {
                Yii::error('getImagine exception ' . $ex->getMessage());
            }
        }
    }

    /**
     * Upload file anh va gen thumb
     * @param yii\web\UploadedFile $file
     * @param string $dirName duong dan luu anh thumb
     * @param function $cb
     * 
     * @return array|bool Neu thanh cong tra ve mamg bao gom bucket va path, neu khong tra ve false
     */
    public static function uploadAndGenerateThumbs($file, $dirName) {

        if(Yii::$app->params['storage.type'] == 1) {
            $bucket = Yii::$app->params['s3']['static.bucket'];
            $path = Utils::generatePath($file->extension);
            S3Service::putObject($bucket, $file->tempName, $path);
            VtHelper::generateAllThumb($bucket, $file->tempName, $path);
            return [
                'bucket' => $bucket,
                'path' => $path,
            ];
        } else {
            $bucket = '';
            $fileName = Utils::generateUniqueFileName($file->extension);
            $relativePath = $dirName . '/' . $fileName;
            $fullPath = Yii::$app->params['storage.san.rootPath'] . $relativePath;

            if(!is_dir(dirname($fullPath)) && !mkdir(dirname($fullPath), 0700, true)) {
                return false;
            }

            $file->saveAs($fullPath);
            VtHelper::generateAllThumb($bucket, $fullPath, $relativePath);
            return [
                'bucket' => $bucket,
                'path' => $relativePath,
            ];
        }
    }

    /**
     * Generate anh thumb dua theo bucket va path
     * @param string $path
     * @param string $bucket
     * 
     * @return void
     */
    public static function generateThumbs($path, $bucket) {
        if(Yii::$app->params['storage.type'] == 1) {
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($path);
            $result = S3Service::getObject($bucket, $path, $tempPath);

            if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS) {
                return self::generateAllThumb($bucket, $tempPath, $path);
            }
        } else {
            $fullPath = Yii::$app->params['storage.san.rootPath'] . $path;

            if(file_exists($fullPath)) {
                return self::generateAllThumb($bucket, $fullPath, $path);
            }
        }
    }

    public static function generateURL($str, $protocol, $address, $template, $prefix = null, $params = array(), $extension = null)
    {
        $key = 'AABBCCDDEEFFGGHH';

        if (empty($extension)) {
            $extension = substr(strrchr($str, '.'), 1);
        }
        $base64 = new Base64();

        //Tao link ma hoa
        $args = array(
            '%protocol%' => strtolower($protocol),
            '%address%' => $address,
            '%prefix%' => !empty($prefix) ? $prefix . '/' : '',
            '%encoded_string%' => $base64->encode(mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB)),
            '%extension%' => $extension
        );
        //Tao param link ma hoa
        $strParam = '';
        $arr = array();
        foreach ($params as $key => $param) {
            $arr[] = $key . '=' . $param;
        }
        if (!empty($arr)) {
            $strParam = '?' . implode('&', $arr);
        }

        $result = strtr($template, $args) . $strParam;

        Yii::info('{STREAMING} ' . $str . ' ' . $result, 'streaming');

        return $result;
    }

    public static function getMappingArray($items)
    {
        $arr = [];

        foreach ($items as $key => $item) {
            $arr[$key] = $item;
        }
        return $arr;
    }

    public static function getThumbAnimationImg($bucket, $path, $size)
    {
        if ($size == self::SIZE_VIDEO) {
            $sizeDetail["width"] = 350;
            $sizeDetail["height"] = 210;
        } else {
            $sizeDetail["width"] = 255;
            $sizeDetail["height"] = 145;
        }

        $pos = strrpos($path, '.');

        if ($pos !== false) {
            $basename = substr($path, 0, $pos);
            $extension = "webp";

            if (!isset($sizeDetail['width']) && !isset($sizeDetail['height'])) {
                $thumbName = $basename . '.' . $extension;
            } elseif (!isset($sizeDetail['width'])) {
                $thumbName = $basename . '_auto_' . $sizeDetail['height'] . '.' . $extension;
            } elseif (!isset($sizeDetail['height'])) {
                $thumbName = $basename . '_' . $sizeDetail['width'] . '_auto.' . $extension;
            } else {
                $thumbName = $basename . '_' . $sizeDetail['width'] . 'x' . $sizeDetail['height'] . '.' . $extension;
            }

            return S3Service::generateWebUrl($bucket, $thumbName);
        }
    }

    public static function convert8416XTo843X($msisdn)
    {

        $arrShortCode = Yii::$app->params["convert.843x.list"];
        if (count($arrShortCode)) {
            $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
            foreach ($arrShortCode as $from => $to) {
                if (strpos(strval($msisdn), strval($from)) === 0) {
                    return $to . substr($msisdn, strlen($from));
                }
            }
        }
        return $msisdn;
    }
    public static function formatLang($lang, $mutiLang,$key,$langue){
        if($langue=='vi'){
            return $lang;
        }
        $mutiLang = json_encode($mutiLang, true);
        return $mutiLang[$key.'_'.$langue];

    }
    public static function getLang($lang, $mutiLang,$key){
        $langue = Yii::$app->language;

        //return self::formatLang($lang, $mutiLang,$key,$langue);

    }
    public static function getUrl($bucket, $path, $isFull = false)
    {
        if (empty($bucket)) {
			
            $host = Yii::$app->params['stream']['san'];
            return $host . $path;
        } else {
            $host = Yii::$app->params['stream']['s3'][$bucket];
            $fullPath = $bucket . '/' . ltrim($path, '/');
            return $host . $fullPath;
        }
    }

    public static function isJSON($string) {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
	

}
