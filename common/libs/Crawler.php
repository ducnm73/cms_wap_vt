<?php

namespace common\libs;

use backend\models\VtMappingCrawler;
use backend\models\VtMappingTopic;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

class Crawler
{
    const ATTRIBUTE_CLIP = 1;
    const ATTRIBUTE_FILM_PART = 2;
    const ATTRIBUTE_FILM_SERIES = 3;
    const ATTRIBUTE_MUSIC = 4;

    const SUCCESS = '000000';
    const AUTH_FAIL = '000004';

    public static function getAuthorizationCode()
    {
        try {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(Yii::$app->params['crawler']['url.getAuthorizationCode'])
                ->setData(
                    [
                        'client_id' => Yii::$app->params['crawler']['clientId'],
                        'client_secret' => Yii::$app->params['crawler']['clientSecret']
                    ]
                )
                ->setOptions([
                    'timeout' => Yii::$app->params['crawler']['timeout'],
                ])
                ->send();

            if ($response->isOk) {
                Yii::info('getAuthorizationCode: Success ' . $response->getStatusCode(), 'crawler');


                if ($response->data['errorCode'] == self::SUCCESS) {
                    return $response->data['data']['authorization_code'];
                } else {
                    return '';
                }

            } else {
                Yii::error('getAuthorizationCode: Error ' . $response->getStatusCode(), 'crawler');
                return '';
            }

        } catch (\Exception $ex) {
            Yii::error('getAuthorizationCode: Exception ' . $ex->getMessage(), 'crawler');
            return '';
        }

    }


    /**
     * @author ducda2@viettel.com.vn
     * Lay danh sach crawler ids
     */
    public static function getAllCrawlers($name = '')
    {
        try {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(Yii::$app->params['crawler']['url.getCrawler'])
                ->setData(
                    [
                        'authorization_code' => self::getAuthorizationCode(),
                        'name' => $name,
                        'limit' => 10000,
                        'offset' => 0,
                        'type' => 1,
                    ]
                )
                ->setOptions([
                    'timeout' => Yii::$app->params['crawler']['timeout'],
                ])
                ->send();
            if ($response->isOk) {
                Yii::info('getAllAttributes: Success ' . $response->getStatusCode(), 'crawler');
                return $response->data['data'];

            } else {
                Yii::error('getAllAttributes: Error ' . $response->getStatusCode(), 'crawler');
                return [
                    'total' => 0,
                    'items' => []
                ];
            }

        } catch (\Exception $ex) {
            Yii::error('getAllAttributes: Exception ' . $ex->getMessage(), 'crawler');
            return [
                'total' => 0,
                'items' => []
            ];
        }

    }

    /**
     * @author ducda2@viettel.com.vn
     * Lay danh sach attribute theo type
     * @param $type
     */
    public static function getAllAttributes($name = '', $type = 1)
    {
        try {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(Yii::$app->params['crawler']['url.getMediaAttribute'])
                ->setData(
                    [
                        'authorization_code' => self::getAuthorizationCode(),
                        'name' => $name,
                        'limit' => 10000,
                        'type' => $type
                    ]
                )
                ->setOptions([
                    'timeout' => Yii::$app->params['crawler']['timeout'],
                ])
                ->send();

            if ($response->isOk) {
                Yii::info('getAllAttributes: Success ' . $response->getStatusCode(), 'crawler');
                return $response->data['data'];

            } else {
                Yii::error('getAllAttributes: Error ' . $response->getStatusCode(), 'crawler');
                return [
                    'total' => 0,
                    'items' => []
                ];
            }

        } catch (\Exception $ex) {
            Yii::error('getAllAttributes: Exception ' . $ex->getMessage(), 'crawler');
            return [
                'total' => 0,
                'items' => []
            ];
        }

    }


    public static function syncCrawlerIds()
    {
        $crawlers = self::getAllCrawlers();

        if (count($crawlers) > 0) {
            $ids = ArrayHelper::getColumn($crawlers['items'], 'id');
            $localIds = ArrayHelper::getColumn(VtMappingCrawler::getAllCrawler(), 'crawler_id');

            $addIds = array_diff($ids, $localIds);

            foreach ($addIds as $addId) {
                $vtMappingCrawler = new VtMappingCrawler();
                $vtMappingCrawler->crawler_id = $addId;
                $vtMappingCrawler->save();
            }
        }
    }

    public static function syncAttributeIds()
    {
        $attributes = self::getAllAttributes();

        if (count($attributes) > 0) {
            $items = $attributes['items'];
            $ids = [];
            foreach($items as $item){
                if($item['type'] == 1){
                    $ids[] = $item['id'];
                }
            }

            $localIds = ArrayHelper::getColumn(VtMappingTopic::getAllAttribute(), 'crawler_category_id');

            $addIds = array_diff($ids, $localIds);

            foreach ($addIds as $addId) {
                $vtMappingTopic = new VtMappingTopic();
                $vtMappingTopic->crawler_category_id = $addId;
                $vtMappingTopic->save();
            }
        }
    }

}
