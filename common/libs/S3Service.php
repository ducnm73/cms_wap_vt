<?php

namespace common\libs;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use common\helpers\Base64;
use common\helpers\Utils;
use common\libs\VtHelper;
use common\models\VtConvertedFileBase;
use Yii;
use yii\db\Exception;

class S3Service
{
    const SUCCESS = 0;
    const UNSUCCESS = 1;

    const VODCDN = 'VODCDN';
    const OBJCDN = 'OBJCDN';

    /**
     * Generate link streaming ma hoa
     * @param $video
     * @param $type
     * @param $freeData
     * @return string
     */
    public static function generateStreamURL($video, $type = self::VODCDN, $freeData = true, $isAdaptive = true)
    {

        if (!$video) return '';

        if (!isset($video['file_bucket'])) {
            $bucketName = $video['bucket'];
            $path = $video['bucket'] . '/' . ltrim($video['path'], '/');
        } else {
            $bucketName = $video['file_bucket'];
            $path = $video['file_bucket'] . '/' . ltrim($video['file_path'], '/');
        }
        $storageType = Yii::$app->params['storage.type'];
        if ($freeData) {
            $mapping = (!empty($bucketName)) ? Yii::$app->params['s3']['cdn.mapping.freeData'] : Yii::$app->params['san']['san.mapping.freeData'];
        } else {
            $mapping = (!empty($bucketName)) ? Yii::$app->params['s3']['cdn.mapping.lostData'] : Yii::$app->params['san']['san.mapping.lostData'];
        }


        if (empty($bucketName)) { // create url san
            $protocol = Yii::$app->params['san']['san.protocol'];
            $profileDefault = Yii::$app->params['profile.default'];
            $template = Yii::$app->params['san']['san.template'];
            $str = '';
            if ($isAdaptive) {
                // $profile = VtConvertedFileBase::getProfiles($video['id']);
                // $profileFormatArr = array();
                // $path = '';
                // foreach ($profile as $key => $value) {
                // $profileFormatArr[] = $value['name'];
                // if ($value['profile_id'] == $profileDefault) {
                // $path = $value['path'];
                // }
                // }
                // //Start_time|file_path|IP
                // if (count($profileFormatArr) > 1) {
                // $params['adbr'] = 'true';
                // $params['vr'] = implode(',', $profileFormatArr);
                // $extension = 'm3u8';
                // $str = date('ymdHis', time()) . "|" . $path . "|" . VtHelper::getAgentIp();
                // }
                //$profile = VtConvertedFileBase::getConvertFile($video,$profileDefault);
                $fullPath = explode("_", $video['path']);
                //return 'https://bitmovin-a.akamaihd.net/content/playhouse-vr/m3u8s/105560.m3u8';
                //return 'http://10.120.44.11:8083/media1/2019/01/16/1547640299811/41c47e17a261.m3u8';
                //return 'http://myclip.la:8083/media1/2019/01/16/1547606925162/e9cf8094258f.mp4';

                return 'http://myclip.la:8083/' . $fullPath[0] . '.m3u8';
            } else {
                $profile = VtConvertedFileBase::getProfiles($video['id'], $video['profile_id']);
                if ($profile) {
                    $str = date('ymdHis', time()) . "|" . $profile['path'] . "|" . VtHelper::getAgentIp();
                    $filename = basename($profile['path']);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);

                }
            }

            return VtHelper::generateURL($str, $protocol, $mapping, $template, $prefix, $params, $extension);
        }

        $expiredTime = strtotime('now') + Yii::$app->params['s3']['vodcdn.timeout'];

        if (Yii::$app->params['s3']['vodcdn.encrypt']) {

            if (Yii::$app->params['s3']['vodcdn.encryptWithIp']) {
                $ip = VtHelper::getAgentIp();
            } else {
                $ip = '';
            }
            if (Utils::endsWith($path, '.m3u8')) { //Cat bo filename trong truong hop generate link re-stream HLS
                $encryptPath = dirname($path);
            } else {
                $encryptPath = $path;
            }

            $encryptString = md5($ip . ':' . Yii::$app->params['s3']['vodcdn.tokenKey'] . ':' . $expiredTime . ':/' . $encryptPath) . $expiredTime . '/' . $path;

        } else {
            $encryptString = $path;
        }

        $args = array(
            '%domain_name%' => $mapping[$bucketName][$type],
            '%encrypt_string%' => $encryptString,
        );

        $url = strtr(Yii::$app->params['s3']['vodcdn.template'][$type], $args);
        Yii::info('{STREAMING} ' . $url, 'streaming');
        return $url;
    }


    /**
     * Generate link streaming ma hoa
     * @param $video
     * @param $type
     * @return string
     */
    public static function generateStorageURL($video, $type = self::VODCDN)
    {
        $mapping = Yii::$app->params['s3']['cdn.mapping.freeData'];
        $bucketName = $video['file_bucket'];
        $path = $video['file_path'];
        $expiredTime = '';

        if (Yii::$app->params['s3']['vodcdn.encrypt']) {
            $encryptString = md5(VtHelper::getAgentIp() . ':' . Yii::$app->params['s3']['tokenKey'] . ':' . $expiredTime . ':' . $path);
        } else {
            $encryptString = $path;
        }

        $args = array(
            '%domain_name%' => $mapping[$bucketName][$type],
            '%encrypt_string%' => $encryptString,
        );

        return strtr(Yii::$app->params['s3']['vodcdn.template'], $args);
    }

    /**
     * Generate link Web tren CDN: jpg, png...
     * @param $bucketName
     * @param $path
     * @return string
     */
    public static function generateWebUrl($bucketName, $path)
    {
        if (empty($bucketName)) {
            return Yii::$app->params['local.site'] . DIRECTORY_SEPARATOR . $path;
        }

        $mapping = Yii::$app->params['s3']['cdn.mapping.image'];

        $expiredTime = strtotime('now') + Yii::$app->params['s3']['vodcdn.timeout'];

        if (Yii::$app->params['s3']['vodcdn.image.encrypt'] && $bucketName != 'image_mobitv') {
            $path = $bucketName . '/' . ltrim($path, '/');

            if (Yii::$app->params['s3']['vodcdn.encryptWithIp']) {
                $ip = VtHelper::getAgentIp();
            } else {
                $ip = '';
            }

            $encryptString = md5($ip . ':' . Yii::$app->params['s3']['vodcdn.tokenKey'] . ':' . $expiredTime . ':/' . $path) . $expiredTime . '/' . $path;

        } else {
            $path = $bucketName . '/' . ltrim($path, '/');
            $encryptString = $path;
        }

        $args = array(
            '%domain_name%' => isset($mapping[$bucketName]) ? $mapping[$bucketName] : '',
            '%path%' => $encryptString,
        );

        return strtr(Yii::$app->params['s3']['webcdn.template'], $args);
    }

    /**
     * Upload file len S3 Storage
     * @param $bucketName
     * @param $orgPath
     * @param $newPath
     * @return array
     */
    public static function putObject($bucketName, $orgPath, $newPath, $multipart = true, $itemType = 'video')
    {

        $storageType = Yii::$app->params['storage.type'];
        if ($storageType == 0) { // Ko co CDN
            // push local
            //  $san = ($itemType == 'video') ? Yii::$app->params['local.video'] : Yii::$app->params['local.image'] ;
            $filePath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $newPath;
            if (!mkdir(dirname($filePath), 0777, true)) {
                die('Failed to create folders...');
            }
            $status = copy($orgPath, $filePath);
            //unlink($orgPath);
            return array(
                'errorCode' => self::SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
            );
        }
        $sharedConfig = [
            'endpoint' => Yii::$app->params['s3']['services'][$bucketName]['endPoint'],
            'region' => 'us-west-2',
            'version' => 'latest',
            'signature_version' => 'v4',
            'credentials' => array(
                'key' => Yii::$app->params['s3']['services'][$bucketName]['accessKey'],
                'secret' => Yii::$app->params['s3']['services'][$bucketName]['secretKey']
            ),
            'request.options' => [
                'proxy' => Yii::$app->params['s3']['proxy.host']
            ],
            'debug' => false

        ];

        $s3 = S3Client::factory($sharedConfig);

        if (!$multipart) {
            try {
                $result = $s3->putObject(
                    array(
                        'Bucket' => $bucketName,
                        'Key' => $newPath,
                        'SourceFile' => $orgPath,
                        'ContentType' => mime_content_type($orgPath),
                        'PathStyle' => Yii::$app->params['s3']['PathStyle'],
                        'ACL' => 'public-read',
                    )
                );
            } catch (\Exception $e) {
                Yii::error('putObject1:Exception|bucketName:' . '|' . $orgPath . '|' . $newPath . ': ' . $e, 's3');
                $result = null;
            }
        } else {

            $result = $s3->createMultipartUpload(array(
                'Bucket' => $bucketName,
                'Key' => $newPath,
                'ACL' => 'public-read',
                'PathStyle' => true,
                'ContentType' => mime_content_type($orgPath),
            ));
            $uploadId = $result['UploadId'];

            try {

                $file = fopen($orgPath, 'r');
                $parts = array();
                $partNumber = 1;
                while (!feof($file)) {
                    $result = $s3->uploadPart(array(
                        'Bucket' => $bucketName,
                        'Key' => $newPath,
                        'UploadId' => $uploadId,
                        'PartNumber' => $partNumber,
                        'Body' => fread($file, 5 * 1024 * 1024),
                        'PathStyle' => true
                    ));
                    $parts[] = array(
                        'PartNumber' => $partNumber++,
                        'ETag' => $result['ETag'],
                    );

                    Yii::info('putObject2:' . $result . '|bucketName:' . '|' . $orgPath . '|' . $newPath . "|Part " . $partNumber, 's3');
                }
                fclose($file);

            } catch (S3Exception $e) {

                $result = $s3->abortMultipartUpload(array(
                    'Bucket' => $bucketName,
                    'Key' => $newPath,
                    'UploadId' => $uploadId,
                    'PathStyle' => true
                ));

                Yii::error('putObject:Exception|bucketName:' . '|' . $orgPath . '|' . $newPath . ': ' . $e, 's3');
                return array(
                    'errorCode' => self::UNSUCCESS,
                    'message' => Yii::t('wap', 'Thất bại'),
                );
            }

            $result = $s3->completeMultipartUpload(array(
                'Bucket' => $bucketName,
                'Key' => $newPath,
                'UploadId' => $uploadId,
                'Parts' => $parts,
                'PathStyle' => true
            ));
        }


        if (!$result) {
            Yii::error('putObject3:' . $result . '|bucketName:' . '|' . $orgPath . '|' . $newPath, 's3');
            return array(
                'errorCode' => self::UNSUCCESS,
                'message' => Yii::t('wap', 'Thất bại'),
            );
        } else {
            Yii::info('putObject4:' . $result . '|bucketName:' . '|' . $orgPath . '|' . $newPath, 's3');
            return array(
                'errorCode' => self::SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
            );
        }
    }


    /**
     * Upload file len S3 Storage
     * @param $bucketName
     * @param $keyName
     * @param $localPath
     * @return array
     */
    public static function getObject($bucketName, $keyName, $localPath)
    {
        try {
            $sharedConfig = [
                'endpoint' => Yii::$app->params['s3']['services'][$bucketName]['endPoint'],
                'region' => 'us-west-2',
                'version' => 'latest',
                'signature_version' => 'v4',
                'credentials' => array(
                    'key' => Yii::$app->params['s3']['services'][$bucketName]['accessKey'],
                    'secret' => Yii::$app->params['s3']['services'][$bucketName]['secretKey']
                ),
                'http' => [
                    'proxy' => Yii::$app->params['s3']['proxy.host']
                ],
                'debug' => false
            ];
            $s3 = S3Client::factory($sharedConfig);

            $result = $s3->getObject(array(
                'Bucket' => $bucketName,
                'Key' => $keyName,
                'SaveAs' => $localPath,
                'PathStyle' => Yii::$app->params['s3']['PathStyle'],
            ));

            if (!$result) {
                Yii::error('getObject:' . $result . '|bucketName:' . '|' . $bucketName . '|keyName' . $keyName . '|localPath' . $localPath, 's3');
                return array(
                    'errorCode' => self::UNSUCCESS,
                    'message' => Yii::t('wap', 'Thất bại'),
                );
            } else {
                Yii::info('getObject:' . $result . '|bucketName:' . '|' . $bucketName . '|keyName' . $keyName . '|localPath' . $localPath, 's3');
                return array(
                    'errorCode' => self::SUCCESS,
                    'message' => Yii::t('wap', 'Thành công'),
                );
            }
        } catch (S3Exception $e) {
            Yii::error('getObject:Exception|bucketName:' . '|' . $bucketName . '|keyName' . $keyName . '|localPath' . $localPath . ': ' . $e, 's3');
            return array(
                'errorCode' => self::UNSUCCESS,
                'message' => Yii::t('wap', 'Thất bại'),
            );
        }
    }

    /**
     * List danh sach object cua folder
     * @param $bucketName
     * @param $folder
     * @return array
     */
    public static function listObjects($bucketName, $folder)
    {
        try {
            $sharedConfig = [
                'endpoint' => Yii::$app->params['s3']['services'][$bucketName]['endPoint'],
                'region' => 'us-west-2',
                'version' => 'latest',
                'signature_version' => 'v4',
                'credentials' => array(
                    'key' => Yii::$app->params['s3']['services'][$bucketName]['accessKey'],
                    'secret' => Yii::$app->params['s3']['services'][$bucketName]['secretKey']
                ),
                'http' => [
                    'proxy' => Yii::$app->params['s3']['proxy.host']
                ],
                'debug' => false
            ];
            $s3 = S3Client::factory($sharedConfig);

            $result = $s3->listObjects(array(
                // Bucket is required
                'Bucket' => $bucketName,
                'PathStyle' => Yii::$app->params['s3']['PathStyle'],
                'Prefix' => $folder,
            ));
            return array(
                'errorCode' => self::SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
                'data' => $result['Contents']
            );

        } catch (S3Exception $e) {
            Yii::error('listObjects:Exception|bucketName:' . '|' . $bucketName . '|folder' . $folder . ': ' . $e, 's3');
            return array(
                'errorCode' => self::UNSUCCESS,
                'message' => Yii::t('wap', 'Thất bại'),
                'data' => []
            );
        }
    }

    public static function getFileSize($bucketName, $filePath)
    {
        $folder = dirname($filePath);

        $result = S3Service::listObjects($bucketName, $folder);
        foreach ($result['data'] as $object) {
            if ($filePath == $object['Key']) {
                return $object['Size'];
            }
        }

        return 0;
    }

    public static function generateCDNUrl($video)
    {
        if (!$video) return '';


        $bucket = $video['bucket'];
        $file_path = $video['path'];
        $path = '/' . $bucket . '/' . ltrim($file_path, '/');
//        echo "<pre>";
//        print_r($path); die('xx');
        $key = 'AABBCCDD';
        $template = 'http://183.182.100.135/%prefix_path%/_%encoded_string%_.m3u8';
        $base64 = new Base64();
        $str = strtotime('now') . '|' . $path . '|' . VtHelper::getAgentIp();

        $args = array(
            '%encoded_string%' => $base64->encode(mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB)),
            '%prefix_path%' => ltrim(dirname($path), '/'),
        );
        $result = strtr($template, $args);

        return $result;
    }

}
