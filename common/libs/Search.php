<?php

namespace common\libs;

use Solarium\Core\Client\Client;
use yii\base\Exception;

class Search
{
    public static function searchAll($query, $type, $limit = null, $offset = null)
    {

        $client = new Client(\Yii::$app->params['solr.endpoint']);
        $exec = $client->createQuery($client::QUERY_SELECT);

        $exec->addParam('q', $query);
        $exec->addParam('fq', 'type:' . $type);

        if ($limit) {
            $exec->addParam('rows', $limit);
        }

        if ($offset) {
            $exec->addParam('start', $offset);
        }

        $resultSet = $client->execute($exec);

        foreach ($resultSet as $document) {
            $data['full_items'][] = $document;
        }
        $data['total'] = $resultSet->getNumFound();

        return $data;
    }

}
