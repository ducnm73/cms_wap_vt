<?php

namespace common\libs;

use common\helpers\TimeoutSoapClient;
use common\helpers\Utils;
use Yii;

class Drm
{
    const SUCCESS = 0;

    const RESULT_EXIST = 205;

    public static function generateUuid()
    {
        return \Yii::$app->params['service.drm']['prefix'] . date('YmdHis');
    }


    public static function signOn()
    {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout'],
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                )
            )
        );
        $response = null;
        try {
            $client = new TimeoutSoapClient($service['endPointAdminMgmt'], $options);
            $response = $client->__soapCall('signOn', array(array(
                'userName' => $service['username'],
                'password' => $service['password'],
            )));

        } catch (\Exception $e) {
            \Yii::error('{DRM} signOn exception: ' . $e->getMessage(), 'drm');
        }

        if (is_array($response) && key_exists('result', $response)) {
            if ($response['result']->resultCode == self::SUCCESS) {
                \Yii::info('{DRM} signOn ' . $service['username'] . ' successfully| Result: ' . $response['result']->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response['sessionHandle']->handle;
            } else {
                \Yii::info('{DRM} signOn ' . $service['username'] . ' fail| Result: ' . $response['result']->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return '';
            }
        } else {
            \Yii::error('{DRM} Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }

    public static function addDevice($smsDeviceId, $networkDeviceId, $deviceType, $token)
    {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout'],
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                )
            )
        );
        $response = null;
        try {
            $client = new TimeoutSoapClient($service['endPointDeviceMgmt'], $options);
            $response = $client->__soapCall('addDevices', array(
                'deviceList' => [
                    'devices' => [
                        'smsDeviceId' => $smsDeviceId,
                        'smsNetworkId' => $service['smsNetworkId'],
                        'deviceType' => $deviceType,
                        'networkDeviceId' => $networkDeviceId
                    ]
                ],
                'sessionHandle' => [
                    'handle' => $token
                ]
            ));
        } catch (\Exception $e) {
            \Yii::error('{DRM} addDevices exception: ' . $e->getMessage(), 'drm');
        }

        if (is_object($response) && isset($response->result)) {
            if ($response->result->resultCode == self::SUCCESS) {
               \Yii::info('{DRM} addDevices ' . $smsDeviceId . ' successfully| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            } else {
                \Yii::info('{DRM} addDevices ' . $smsDeviceId . ' fail| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            }
        } else {
            \Yii::error('{DRM} addDevices Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }

    public static function removeDevice($smsDeviceId, $networkDeviceId, $deviceType, $token)
    {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout'],
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                )
            )
        );
        $response = null;
        try {
            $client = new TimeoutSoapClient($service['endPointDeviceMgmt'], $options);
            $response = $client->__soapCall('removeDevices', array(
                'deviceList' => [
                    'devices' => [
                        'smsDeviceId' => $smsDeviceId,
                        'smsNetworkId' => $service['smsNetworkId'],
                        'deviceType' => $deviceType,
                        'networkDeviceId' => $networkDeviceId
                    ]
                ],
                'sessionHandle' => [
                    'handle' => $token
                ]
            ));
        } catch (\Exception $e) {
            \Yii::error('{DRM} removeDevice exception: ' . $e->getMessage(), 'drm');
        }

        if (is_object($response) && isset($response->result)) {
            if ($response->result->resultCode == self::SUCCESS) {
                \Yii::info('{DRM} removeDevices ' . $smsDeviceId . ' successfully| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            } else {
                \Yii::info('{DRM} removeDevices ' . $smsDeviceId . ' fail| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            }
        } else {
            \Yii::error('{DRM} removeDevices Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }



    public static function getNetworkDevice($networkDeviceId, $token)
    {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout'],
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                )
            )
        );
        $response = null;
        try {
            $client = new TimeoutSoapClient($service['endPointDeviceMgmt'], $options);
            $response = $client->__soapCall('getNetworkDeviceIdList', array(
                    'networkDeviceIdListQuery' => [
                        'smsNetworkId' => $service['smsNetworkId'],
                        'networkDeviceId' => $networkDeviceId,
                        'deviceCount' => 3
                ],
                'sessionHandle' => [
                    'handle' => $token
                ]
            ));
        } catch (\Exception $e) {
            \Yii::error('{DRM} getNetworkDeviceIdList exception: ' . $e->getMessage(), 'drm');
        }
//        echo "<pre>";print_r($response);die();
        if (is_object($response) && isset($response->result)) {
            if ($response->result->resultCode == self::SUCCESS) {
                \Yii::info('{DRM} getNetworkDeviceIdList ' . $networkDeviceId . ' successfully| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');

                foreach($response->devices as $device){

                    if($device->device->networkDeviceId == $networkDeviceId){
                        return $device->device->smsDeviceId;
                    }

                }
                return '';
            } else {
                \Yii::info('{DRM} getNetworkDeviceIdList ' . $networkDeviceId . ' fail| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return '';
            }
        } else {
            \Yii::error('{DRM} getNetworkDeviceIdList Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }

    public static function addEntitlements($entitlementId, $entityId, $token)
    {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout'],
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                )
            )
        );
        $response = null;
        try {
            $client = new TimeoutSoapClient($service['endPointEntitlementMgmt'], $options);
            $response = $client->__soapCall('addEntitlements', array(
                'entitlementList' => [
                    'entitlements' => [
                        'smsEntitlementId' => $entitlementId,
                        'package' => [
                            'smsPackageId' => $service['smsPackageId']
                        ],
                        'entitledEntity' => [
                            'entityType' => $service['entityType'],
                            'entityId' => $entityId
                        ],
                        'timePeriodRule' => [
                            'startTime' => '',
                            'endTime' => ''
                        ]
                    ]
                ],
                'sessionHandle' => [
                    'handle' => $token
                ]
            ));
        } catch (\Exception $e) {
            \Yii::error('{DRM} addDevices exception: ' . $e->getMessage(), 'drm');
        }

        if (is_object($response) && isset($response->result)) {
            if ($response->result->resultCode == self::SUCCESS) {
                \Yii::info('{DRM} addEntitlements successfully| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            } else {
                \Yii::info('{DRM} addDevices fail| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            }
        } else {
            \Yii::error('{DRM} Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }


    public static function removeEntitlements($entitlementId, $entityId, $token)
    {
        $startTime = Utils::current_millis();

        $service = \Yii::$app->params['service.drm'];

        $options = array(
            'connect_timeout' => $service['connectTimeout'],
            'timeout' => $service['readTimeout'],
            "stream_context" => stream_context_create(
                array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                )
            )
        );
        $response = null;
        try {
            $client = new TimeoutSoapClient($service['endPointEntitlementMgmt'], $options);
            $response = $client->__soapCall('removeEntitlements', array(
                'entitlementList' => [
                    'entitlements' => [
                        'smsEntitlementId' => $entitlementId,
                        'package' => [
                            'smsPackageId' => $service['smsPackageId']
                        ],
                        'entitledEntity' => [
                            'entityType' => $service['entityType'],
                            'entityId' => $entityId
                        ],
                        'timePeriodRule' => [
                            'startTime' => '',
                            'endTime' => ''
                        ]
                    ]
                ],
                'sessionHandle' => [
                    'handle' => $token
                ]
            ));
        } catch (\Exception $e) {
            \Yii::error('{DRM} removeEntitlements exception: ' . $e->getMessage(), 'drm');
        }

        if (is_object($response) && isset($response->result)) {
            if ($response->result->resultCode == self::SUCCESS) {
                \Yii::info('{DRM} removeEntitlements successfully| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            } else {
                \Yii::info('{DRM} removeEntitlements fail| resultCode: ' . $response->result->resultCode . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
                return $response->result->resultCode;
            }
        } else {
            \Yii::error('{DRM} Response no object!' . '| Time: ' . (Utils::current_millis() - $startTime) . 'ms', 'drm');
            return '';
        }
    }

}
