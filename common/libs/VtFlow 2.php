<?php
/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 9/27/2016
 * Time: 5:05 PM
 */

namespace common\libs;

use Yii;

use api\models\VtPromotionData;
use common\helpers\MobiTVRedisCache;
use common\models\VtPromotionDataBase;
use common\models\VtUserDrmBase;
use common\models\VtUserDrmLogBase;

use common\models\LogSubViewBase;
use common\models\VtConvertedFileBase;
use common\models\VtHistoryViewBase;
use common\models\VtUserViewBase;
use common\models\VtVideoBase;
use common\models\VtPlaylistBase;
use backend\models\VtSub;
use common\helpers\Utils;
use common\models\VtBuyPlaylistBase;
use common\models\VtBuyVideoBase;
use common\models\VtConfigBase;
use common\models\VtPackageBase;
use common\modules\v1\libs\ResponseCode;
use yii\db\Exception;


class VtFlow
{
    const FLOW_SUCCESS = 200;
    const FLOW_FAIL = 201;

    /**
     * @author PhuMX
     * @param $msisdn
     * @param $video
     * @param bool $playlistId
     * @param bool $profile
     * @param int $userId
     * @param $authJson
     * @param string $supportType
     * @return array
     */

    public static function viewVideo($msisdn, $video, $playlistId = false, $profile = false, $userId = 0, $authJson, $supportType = S3Service::VODCDN, $acceptLossData = 0, $allowViewFreeDontNeedLogin = null, $distributionId = null, $networkDeviceId = null, $deviceType = '')
    {
        if (!$video) {
            return [
                'errorCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('backend', 'Video không hợp lệ'),
                'urlStreaming' => '',
            ];
        }
        // chuyen type xem ve video
        $video['type'] = 'VOD';

        // Kiem tra xem co cap quyen xem noi dung mien phi khong can dang nhap khong?
        if(!isset($allowViewFreeDontNeedLogin)){
            $allowViewFreeDontNeedLogin = VtConfigBase::getConfig('ALLOW_VIEW_FREE_DONT_NEED_LOGIN', 0);
        }

        $isInPool = VtHelper::isIPInDetectPool();

        $popupObj = array();
        $canView = false;

        // Da kiem tra ben duoi
//        if ($allowViewFreeDontNeedLogin && $video['is_check'] == 1) {
//            if (!$isInPool) {
//                $canView = true;
//                Yii::info('VIEW_FREE_WIFI|' . $msisdn . "|" . $userId . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
//            } else {
//                if (!$msisdn && !$userId && $video['is_check'] == 0) {
//                    return [
//                        'errorCode' => $authJson['responseCode'],
//                        'message' => $authJson['message'],
//                        'urlStreaming' => '',
//                    ];
//                }
//            }
//        }

        //Kiem tra xem KH co quyen xem mien phi noi dung khi dang cho dang ky hay khong?
        if ($userId && Yii::$app->getCache()->get("VIEW_WAIT_REGISTER_" . $userId)) {
            $numVideoHasView = Yii::$app->getCache()->get("NUM_VIEW_WAIT_REGISTER_" . $userId);
            Yii::$app->getCache()->set("NUM_VIEW_WAIT_REGISTER_" . $userId, $numVideoHasView + 1, MobiTVRedisCache::CACHE_1DAY);
            if ($numVideoHasView <= Yii::$app->params['num.video.viewfree.waitregister']) {
                $canView = true;
                Yii::info('VIEW_FREE_WAIT_REGISTER|' . $msisdn . "|" . $userId . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
            }
        };

        // Kiem tra xem khach hang co dang ky goi cuoc video
        $objSub = VtSub::getOneSubByMsisdn($msisdn);
        $today = date("Y-m-d");
        if($objSub){
            // Lay ngay het han goi cuoc
            $ngayhh = $objSub['expired_time'];
        }
        if (!$canView && $video['price_play'] == 0) {
            if(!$userId){
                // trường hợp chưa đăng nhập
                // $ip_address = $_SERVER['REMOTE_ADDR'];
                $ip_address = md5(VtHelper::getAgentIp());
                $quotaFreeNDay = VtConfigBase::getConfig('ALLOW_NO_LOGIN_VIEW_FREE_WATCH_DAY', 3);
                $videoFreeNDay = VtConfigBase::getConfig('NUM_VIEW_VIDEO_FREE_WATCH_DAY', 10);
            }else {
                // trường họp dang nhap
                $ip_address = $userId;
                $quotaFreeNDay = VtConfigBase::getConfig('ALLOW_VIEW_FREE_WATCH_DAY', 30);
                $videoFreeNDay = VtConfigBase::getConfig('NUM_VIEW_VIDEO_FREE_WATCH_DAY', 30);

            }
            // if(YII_DEBUG === true) {var_dump(VtHelper::getAgentIp(), $ip_address); die;}
            // Lay 3 gia tri ngay bat dau, ngay ket thuc, va ngay reset video
            // xoa cache khoi tao ngay moi xem
//            Yii::$app->getCache()->offsetUnset("START_FREE_DATE".$ip_address);
//            Yii::$app->getCache()->offsetUnset("IP".$ip_address);
            // ngay bat dau xem video
            $startDateFree = Yii::$app->getCache()->get("START_FREE_DATE".$ip_address);
            // dia chi ip xem video
            $idIp = Yii::$app->getCache()->get("IP".$ip_address);
            // ngay reset lam mơi video
            $resetVideo = Yii::$app->getCache()->get("RESET_VIDEO".$ip_address);
            // nếu chưa đăng nhập, chưa xem video lần nào thì khởi tạo mới.
            if (($startDateFree == false && $idIp == false) ){
                // Neu da dang nhap, da mua goi cuoc thi ngay bat dau xem free la ngay het han goi cuoc
                if($userId && $objSub) {
                    $settingDate =  date('Y-m-d',strtotime($ngayhh));
                    // khởi tạo ngày xem free cho tai khoan hêt hạn
                    Yii::$app->getCache()->set("START_FREE_DATE".$ip_address, $settingDate);
                } else {
                    // trường hơp chưa có goi dịch vụ, chưa đăng nhập hoặc đã đăng nhập nhưng chưa có dăng ky dịch vu
                    //  Neu khong phai thi ngay bat dau xem free la ngay dau vao xem cho
                    Yii::$app->getCache()->set("START_FREE_DATE".$ip_address, date('Y-m-d'));
                }
                // khởi tạo cache biến ip cho từng máy
                Yii::$app->getCache()->set("IP".$ip_address, $ip_address);
                // khởi tạo ngày rết video. tức là sau 1 ngày reset video về rỗng
                Yii::$app->getCache()->set("RESET_VIDEO".$ip_address, date('Y-m-d'));
                //lấy gia trị ngày xem video cua 1 ip
                $startDateFree = Yii::$app->getCache()->get("START_FREE_DATE".$ip_address);
                // lây dia chi ip của may
                $idIp = Yii::$app->getCache()->get("IP".$ip_address);
                // lay ngya reset video
                $resetVideo = Yii::$app->getCache()->get("RESET_VIDEO".$ip_address);

                Yii::$app->getCache()->set($ip_address,"" );
            }

            // kiem tra ngày hiện tại với ngày reset video. nếu sang 1 ngày mới thi khởi tạo lại reset video và mảng video
            if($today != $resetVideo){
                // neu khac voi ngay reset video thì khởi tạo lại ngày reset video
                Yii::$app->getCache()->set("RESET_VIDEO".$ip_address, date('Y-m-d'));
                // set lai giá trị mảng video đã xem về rông
                Yii::$app->getCache()->set($ip_address,"" );
            }
            // ngày hết hạn xem free video của ip
            $end = date('Y-m-d', strtotime($startDateFree . ' + ' . $quotaFreeNDay . ' days'));

            // mảng giá trị video đã được xem trong ngày
            // khoi tao từ đâu mang video:
            $idStr = Yii::$app->getCache()->get($ip_address);

            $ids = array();
            if (!empty($idStr)) {
                $ids = explode(',', $idStr);
            }

            // kiem tra ngày hienj tại nhỏ hơn ngày hêt hạn thì vẫn được xem video
            if( strtotime($today) < strtotime($end)){
                // kiêm tra số lượng video được xem trong 1 ngày
                if (count($ids) <= (int)$videoFreeNDay-1 || in_array($video['id'], $ids)) {
                    // nếu video nhỏ hơn 10 hoặc video đó có trong mảng thì vẫn được xem video
                    $canView = true;
                    Yii::info('VIEW_FREE|' . $msisdn . "|" . $userId . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
                    // nếu video khong co trong mảng thì insert vao mang
                    if(!in_array($video['id'], $ids))
                    {
                        array_push($ids, $video['id']);
                    }

                    Yii::$app->getCache()->set($ip_address, trim(implode(',', array_unique($ids)), ','));
                } else {
                    // trường hợp video đã xem  > 10 và video không nằm trong mảng video đã xem
                    $canView = false;
                    Yii::info('CHECK FREE QUOTA|' . $msisdn . "|" . $video['id'] . "|FALSE" . "|REMAIN QUOTA: " . ($videoFreeNDay-count($ids)), 'traceview');
                }
            } else {
                // nêu ngay hienj tại đã vượt qua ngày hết hạn xem video
                $canView = false;
                Yii::info('CHECK FREE QUOTA|' . $msisdn . "|" . $video['id'] . "|FALSE" . "|REMAIN QUOTA: " . ($videoFreeNDay-count($ids)), 'traceview');
            }
        }

        // kiêm đã đa đăng ký gói dịch vụ và ngày hiện tại nhỏ hơn ngày hêt hạn xem video trong goi dịch vu
        if ($objSub && strtotime($today) <= strtotime($ngayhh)) {

            // XU LY TRUONG HOP TAM NGHUNG SU DUNG DICH VU (4GPPLAY)
            if ($objSub['is_block'] == "1" && VtConfigBase::getConfig("is_enable_block_viewsub", 0)) {
                $canView = false;
                $popupObj['is_register_sub'] = 0;
                $popupObj['is_buy_video'] = 0;
                $popupObj['is_buy_playlist'] = 0;
                $popupObj['confirm'] = VtConfigBase::getConfig("msg_block_sub", Yii::t('backend', "Thuê bao đang bị tạm ngưng sử dụng dịch vụ"));
                Yii::info('BLOCKVIEW_SUB|' . $msisdn . "|" . $objSub['id'] . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
            } else {
                // Goi CDR doi soat
                if(empty($objSub['distribution_id']) && !strstr($video['cp_code'], "USER_UPLOAD")){
                    self::exportCdr($video, $objSub, Yii::$app->params['app.source']);
                }
                $canView = true;
                Yii::info('VIEW_SUB|' . $msisdn . "|" . $objSub['id'] . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
            }
        }

//          Yii::$app->getCache()->offsetUnset($startDateFree);
//           Yii::$app->getCache()->offsetUnset($ip_address);

        // Co the khach hang chưa dang ky goi cuoc video
        if ($canView == false || !$objSub) {
            Yii::info('CHECK REGISTER SUB|' . $msisdn . "|" . $objSub['id'] . "|FALSE", 'traceview');
            // Kiem tra xem khach hang da mua le video hay chua?
            $objBuyVideo = VtBuyVideoBase::checkBuyVideo($msisdn, $video);
            if ($objBuyVideo) {
                $canView = true;
                Yii::info('VIEW_BUYVIDEO|' . $msisdn . "|" . $objBuyVideo['id'] . "|" . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
            } else {
                Yii::info('CHECK BUY VIDEO|' . $msisdn . "|" . $video['id'] . "|FALSE", 'traceview');
                // Kiem tra xem da mua playlist chua video
                $objBuyPlaylist = VtBuyPlaylistBase::checkBuyPlaylist($msisdn, $video);
                if ($objBuyPlaylist) {
                    $canView = true;
                    Yii::info('VIEW_BUYPLAYLIST|' . $msisdn . "|" . $objBuyPlaylist['id'] . "|" . $video['id'] . "|" . $video['slug'], 'traceview');
                } else {
                    Yii::info('CHECK BUY PLAYLIST|' . $msisdn . "|" . $objBuyPlaylist['id'] . "|FALSE", 'traceview');

                    // Moi dang ky goi cuoc
                    $objPackage = VtPackageBase::getSuggestPackage();
                    $arrReplace = ['#free_times' => $videoFreeNDay, "#free_range" => $quotaFreeNDay, '#videoName' => htmlspecialchars($video['name'], ENT_QUOTES, 'UTF-8'), '#fee' => Utils::format_number($video['price_play']), '#packageName' => htmlspecialchars($objPackage['name'], ENT_QUOTES, 'UTF-8'), '#cycle' => Utils::convertDay($objPackage['charge_range']), '#subPrice' => Utils::format_number($objPackage['fee'])];

                    // Moi neu co goi cuoc
                    if ($objPackage) {
                        $popupObj['is_register_sub'] = 1;
                        $popupObj['confirm_register_sub'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.confirm.registersub']));
                        $popupObj['package_id'] = $objPackage['id'];
                    } else {// Moi khi khong co goi cuoc
                        $popupObj['is_register_sub'] = 0;
                    }

                    if (VtConfigBase::getConfig('ALLOW_BUY_VIDEO') && $video['price_play'] > 0) {
                        $popupObj['video_id'] = $video['id'];
                        $popupObj['is_buy_video'] = 1;
                        $popupObj['confirm_buy_video'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.confirm.buyvideo']));
                    } else {
                        $popupObj['is_buy_video'] = 0;
                    }
                    if (VtConfigBase::getConfig('ALLOW_BUY_PLAYLIST')) {
                        $playlist = VtPlaylistBase::checkVideoInPlaylist($video['id'], $playlistId);
                        if ($playlist && $playlist['price_play'] > 0) {
                            $arrReplace['#playlistName'] = htmlspecialchars($playlist['name'], ENT_QUOTES, 'UTF-8');
                            $arrReplace['#playlistPrice'] = $playlist['price_play'];
                            $popupObj['is_buy_playlist'] = 1;
                            $popupObj['playlist_id'] = $playlistId;
                            $popupObj['confirm_buy_playlist'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.confirm.buyplaylist']));
                        } else {
                            $popupObj['is_buy_playlist'] = 0;
                        }
                    } else {
                        $popupObj['is_buy_playlist'] = 0;
                    }
                    $popupObj['is_register_fast'] = 0;


                    $tmpConfig = $popupObj['is_register_sub'] . $popupObj['is_buy_video'] . $popupObj['is_buy_playlist'];
                    $confirmConfigKey = ($msisdn ? 'popup.confirm_' : 'popup.confirm_not_login_') . $tmpConfig;
                    $popupObj['confirm'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('app', Yii::$app->params[$confirmConfigKey]));

                    //Suggest popup by video
                    if (!empty($video['suggest_package_id'])) {
                        $objPackage = VtPackageBase::getSuggestPackage($video['suggest_package_id']);
                        $arrReplace = ['#videoName' => htmlspecialchars($video['name'], ENT_QUOTES, 'UTF-8'), '#fee' => Utils::format_number($video['price_play']), '#packageName' => htmlspecialchars($objPackage['name'], ENT_QUOTES, 'UTF-8'), '#cycle' => lcfirst(Utils::convertDay($objPackage['charge_range'])), '#subPrice' => Utils::format_number($objPackage['fee'])];
                        $popupObj['is_register_sub'] = 1;
                        $popupObj['confirm'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.suggestion']['confirm']));
                        $popupObj['confirm_register_sub'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.suggestion']['confirm_register_sub']));
                        $popupObj['package_id'] = $objPackage['id'];
                    }

                    //@todo: override subdomain package
                    if (!empty($distributionId)) {
                        $objPackage = VtPackageBase::getDistributionPackage($distributionId);
                        $arrReplace = ['#videoName' => htmlspecialchars($video['name'], ENT_QUOTES, 'UTF-8'), '#fee' => Utils::format_number($video['price_play']), '#packageName' => htmlspecialchars($objPackage['name'], ENT_QUOTES, 'UTF-8'), '#cycle' => lcfirst(Utils::convertDay($objPackage['charge_range'])), '#subPrice' => Utils::format_number($objPackage['fee'])];
                        $popupObj['is_register_sub'] = 1;
                        $popupObj['confirm'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.suggestion']['confirm']));
                        $popupObj['confirm_register_sub'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.suggestion']['confirm_register_sub']));
                        $popupObj['package_id'] = $objPackage['id'];
                    }

                }
            }
        }

        //Kiem tra khuyen mai
        if (!$canView) {
            $promotion = VtPromotionDataBase::checkPromotion($msisdn);
            if (VtPromotionDataBase::checkPromotion($msisdn)) {
                $canView = true;
                Yii::info('VIEW_FREE_PROMOTION|' . $video['id'] . "|" . $profile . "|" . $promotion['code'], 'traceview');
            }
        }

        if (!empty($video['category_id']) && !empty($userId)) {
            $categoryIds = array_filter(explode(',', Yii::$app->getCache()->get("category_list_watch_" . $userId)));

            if (!empty($categoryIds)) {
                $categoryIds = array_diff($categoryIds, [$video['category_id']]);
            }

            array_push($categoryIds, $video['category_id']);
            Yii::$app->getCache()->set("category_list_watch_" . $userId, implode(',', $categoryIds));
        }
        // xem video không cần đăng nhập
        if($video['is_check'] == 1) {
            $canView = true;
        }

        if ($canView || ($acceptLossData == '1' && $video['price_play'] == 0)) {

            if (!empty($networkDeviceId)) {
                $profile = 4;
                $supportType = S3Service::OBJCDN;
                $isDrmSuccess = self::drmHandle($msisdn, $userId, $networkDeviceId, $deviceType);
                if (!$isDrmSuccess) {
                    return [
                        'errorCode' => self::FLOW_FAIL,
                        'message' => Yii::t('backend', 'Xác thực bản quyền thất bại'),
                        'urlStreaming' => '',
                        'popup' => $popupObj,
                        'videoId' => $video['id'],
                    ];
                }
            }

            try {
                $historyView = new VtHistoryViewBase();
                $historyView->insertContinue($userId, $msisdn, $video, 0, $playlistId);

            } catch (\Exception $ex) {
                Yii::error("Elastic Search ERROR:" . $ex);
            }
            Yii::info($msisdn . "|" . $userId . "|" . $video['id'] . "|" . date("YmdHis") . "|" . $profile, 'view');

            $convertFile = VtConvertedFileBase::getConvertFile($video, $profile);

            if (!$convertFile) {
                Yii::info('CONVERT_FILE_NOT_FOUND|' . $video['id'] . "|" . $profile, 'traceview');
                $convertFile = $video;
            } else {
                Yii::info('PLAY_CONVERT_FILE|' . $video['id'] . "|" . $profile, 'traceview');
            }

            if (!empty($networkDeviceId)) {
                $convertFile['bucket'] = 'bucket';
                $convertFile['path'] = '/2017/07/14/15/75236b20/75236b20-8991-4f28-9a70-c1f1aa2067cc_1_.m3u8';
            }

            // Cap nhat luot xem (comment - chuyen sang tien trinh)
//            VtVideoBase::updatePlayTimes($video['id']);
//            if ($playlistId > 0) {
//                VtPlaylistBase::updatePlayTimes($playlistId);
//            }

            //Tu dong hien thi popup moi khi xem
            $currentFreeWatchCount = intval(Yii::$app->cache->get('free_watch_count_' . $msisdn));
            Yii::$app->cache->set('free_watch_count_' . $msisdn, $currentFreeWatchCount + 1);

            //$streamUrl = S3Service::generateStreamURL($convertFile, $supportType, $canView);
            $streamUrl = S3Service::generateCDNUrl($convertFile);
            Yii::info('LOG_VOD|' . $video['type'] . '|' . $video['id'] . '|' . $profile . '|' . $msisdn . '|' . $userId . '|' . str_replace('|', ' ', $video['name']) . '|' . $streamUrl . '|' . $video['category_id'] . '|' . Yii::$app->id . '|' . $video['cp_id'] . '|' . $video['cp_code'], 'log_vod');

            $response = [
                'errorCode' => self::FLOW_SUCCESS,
                'message' => Yii::t('wap', 'Thành công'),
                'urlStreaming' => $streamUrl,
            ];


            if ($popupObj && (($video['suggest_package_id']) && Yii::$app->id == 'app-wap')) {
                $response['popup'] = $popupObj;
            }

            return $response;

        } else {
            if ($video['price_play'] == 0 && $isInPool) {
                // $popupObj['confirm_accept_loss_data'] = Yii::t('web', Yii::$app->params['popup.confirm.accept_loss_data']);
                // $popupObj['accept_loss_data'] = 1;
            }

            $isConfirm = 0;
            //Kiem tra cau hinh xem co can confirm dang ky hay khong?

            switch (Yii::$app->id) {
                case 'app-wap':
                    $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WAP", 0);
                    break;
                case 'app-api':
                    $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_APP", 0);
                    break;
                case 'app-web':
                    $isConfirm = VtConfigBase::getConfig("IS_ON_CONFIRM_SMS_WEB", 0);
                    break;
            }

            $source = Yii::$app->session->get('source');
            //Kiem tra source, neu thuoc whitelist thi khong can conffimr (GA)
            $whiteList = VtConfigBase::getConfig('register.whitelist.no.confirm');
            if (strpos(strtoupper($whiteList), strtoupper($source)) !== false) {
                $isConfirm = 0;
            }
            // Neu mua sub, khong mua le, khong co xem tiep va bat dang ky nhanh(confirm SMS)
            if ($popupObj['is_register_sub'] && !$popupObj['is_buy_video'] && !$popupObj['is_buy_playlist'] && !$popupObj['accept_loss_data'] && $isConfirm) {
                $isRegisterFast = VtConfigBase::getConfig("IS_REGISTER_FAST", 0);
                if ($isRegisterFast) {
                    $popupObj['is_register_fast'] = 1;
                    $popupObj['content_id'] = $video['id'];
                }
            }

            $popupObj['is_confirm_sms'] = $isConfirm;

            return [
                'errorCode' => self::FLOW_FAIL,
                'message' => Yii::t('wap', 'Thất bại'),
                'urlStreaming' => '',
                'popup' => $popupObj
            ];
        }
    }


    public static function caculateQuotaPerDay($userId, $msisdn)
    {
        $userView = VtUserViewBase::getByUser($userId, $msisdn);
        if (!$userView) {
            $userView = new VtUserViewBase();
            $userView->insertLog($userId, $msisdn);
        }

        $redisKey = "user_view_" . date('Ymd') . "_" . (isset($msisdn) ? $msisdn : "") . "_" . $userId;

        $todayQuota = Yii::$app->getCache()->get($redisKey);
        if (!$todayQuota) {
            $todayQuota = $userView->getQuota();
            Yii::$app->getCache()->set($redisKey, $todayQuota, 24 * 60 * 60);
        }
        return $todayQuota;
    }

    /**
     * @param $video
     * @param $sub
     * @param $source
     */
    public static function exportCdr($video, $sub, $source)
    {
        $subType = Yii::$app->params['gateway']['cdr.subType'];
        $command = Yii::$app->params['gateway']['cdr.command'];
        $prefix = Yii::$app->params['gateway']['cdr.prefix'];

        // Check video da duoc xem trong chu ky chua
        $checkView = LogSubViewBase::checkVodWatched($video, $sub);
        if ($checkView == 0) {
            //Neu video chua xem video mien phi lan nao trong chu ky thuc hien ghi log

            $logSubViewIdsKey = "log_sub_view_ids_" . strtotime($sub['last_charge_time']) . "_" . $sub['msisdn'];

            $logSubViewIdsStr = Yii::$app->getCache()->get($logSubViewIdsKey);

            $logSubViewIds = [];
            if (!empty($logSubViewIdsStr)) {
                $logSubViewIds = explode(',', $logSubViewIdsStr);
            }

            if (count($logSubViewIds) < $sub['quota_views']) {
                try {
                    $log = new LogSubViewBase();
                    $log->insertLog($video, $sub, $subType);
                } catch (\Exception $ex) {
                    Yii::error('exportCdr exception ' . $ex->getMessage());
                }

//            if ($isInsertSuccess) {
//                VtService::paymentService(VtService::PAYMENT_ACTION_VIEW, $sub['msisdn'], 0, $video['id'], '', $source, $video['cp_code'], $prefix, $subType, $command, $sub['sub_service_name']);
//            }

                array_push($logSubViewIds, $video['id']);
                $logSubViewIdsStr = implode(',', $logSubViewIds);

                Yii::$app->getCache()->set($logSubViewIdsKey, $logSubViewIdsStr, 30 * 24 * 3600);
            }


        }
    }

    public static function drmHandle($msisdn, $userId, $networkDeviceId, $deviceType)
    {
        $result = false;

        $smsPackageId = \Yii::$app->params['service.drm']['smsPackageId'];

        $userDrms = VtUserDrmBase::getByMsisdnAndUserId($msisdn, $userId);

        foreach ($userDrms as $userDrm) {
            if (!empty($userDrm) && $userDrm->network_device_id == $networkDeviceId) {
                $result = true;
            }
        }

        if ($result == false) {
            $response = VtService::drmAddDevice($networkDeviceId, $deviceType, Yii::$app->id);

            if (!empty($response) && $response->errorCode == Drm::SUCCESS) {
                $userDrm = new VtUserDrmBase();
                $userDrm->insertUserDrm($msisdn, $userId, $response->data->smsDeviceId, $response->data->networkDeviceId, $response->data->smsEntilementId, $deviceType, $smsPackageId);

                $deviceLog = new VtUserDrmLogBase();
                $deviceLog->insertLog($msisdn, $userId, $response->data->smsDeviceId, $response->data->networkDeviceId, $response->data->smsEntilementId, $deviceType, VtUserDrmLogBase::ACTION_CREATE, $response->errorCode, $smsPackageId);

                $result = true;
            } else {
                $deviceLog = new VtUserDrmLogBase();
                $deviceLog->insertLog($msisdn, $userId, '', $networkDeviceId, '', $deviceType, VtUserDrmLogBase::ACTION_CREATE, $response->errorCode, $smsPackageId);
            }
        }

        return $result;
    }

    /**
     * Tra ve popup
     * @param $msisdn
     */
    public static function loadPromotionPopup($msisdn, $isLoadFirst = false)
    {
        if (!$msisdn) {
            return [];
        }

        $promotion = VtPromotionDataBase::checkPromotion($msisdn);
        if ($promotion) {

            // Kiem tra xem khach hang co dang ky goi cuoc video
            $objSub = VtSub::getOneSubByMsisdn($msisdn);
            //Neu da dang ky sub thi khong hien thi popup nua
            if ($objSub) {
                return [];
            }

            $redisCache = Yii::$app->cache;

            $hasInRedis = $redisCache->get('promotion_' . $msisdn);

            //Kiem tra xem da co key trong redis chua
            if ($isLoadFirst) {
                $keyPopup = "promotion.popup.firsttime";
            } else if ($hasInRedis) {
                $keyPopup = $promotion['popup'];
            } else {
                $keyPopup = "promotion.popup.firsttime";
                $redisCache->set('promotion_' . $msisdn, date('Y-m-d H:i:s'));
            }

            if ($keyPopup) {
                $arrReplace = ["#msisdn" => $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_SIMPLE), '#expiredAt' => date("d/m/Y H:i:s", strtotime($promotion['expired_at']))];
                return [
                    'popupId' => $keyPopup,
                    'popupMessage' => str_replace(array_keys($arrReplace), array_values($arrReplace), VtConfigBase::getConfig($keyPopup, ""))
                ];
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
}