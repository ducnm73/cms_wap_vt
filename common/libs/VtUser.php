<?php

namespace common\libs;

use Yii;
use common\modules\v2\libs\ResponseCode;

class VtUser {

    const KEYS_SEARCH = "keys_search";
    const LIMIT = 100;

    public static function insertHistorySearch($keyword) {
        $isAdd = false;
        $count = 0;
        $cookies = Yii::$app->response->cookies;
        $strList = Yii::$app->request->cookies->getValue(self::KEYS_SEARCH);

        $item = array(
            'id' => md5($keyword . time()),
            'name' => $keyword,
            'time' => date('Y-m-d H:i:s'),
        );

        if ($strList) {
            $list = unserialize($strList);

            $count = count($list);
            if ($count < self::LIMIT) {
                $isAdd = true;
                $list[] = $item;
                $decodeStr = serialize($list);
                $cookies->add(new \yii\web\Cookie([
                    'name' => self::KEYS_SEARCH,
                    'value' => $decodeStr,
                ]));
                $count = $count + 1;
            }
        } else { // Chua co thi insert moi 
            $count = 1;
            $isAdd = true;
            $decodeStr = serialize(array($item));

            $cookies->add(new \yii\web\Cookie([
                'name' => self::KEYS_SEARCH,
                'value' => $decodeStr,
            ]));
        }

        $result = array(
            'isAdd' => $count,
            'number' => $isAdd
        );
        return $result;
    }

    public static function getHistorySearch($offset = 0, $limit = self::LIMIT) {
        
        $strList = Yii::$app->request->cookies->getValue(self::KEYS_SEARCH);

        $contents = array();
        if ($strList) {
            $list = unserialize($strList);
            if (count($list) > 0) {
                $contents = array_slice($list, $offset, $limit);
            }
           
        }
        $result = array(
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $contents
        );
        return $result;
    }

    public static function removeHistorySearch($id = "") {
        $cookies = Yii::$app->response->cookies;
        $isDelete = false;
        if (!empty($id)) { // Xoa tat
            $strList = Yii::$app->request->cookies->getValue(self::KEYS_SEARCH);
            if ($strList) {
                $isDelete = true;
                $list = unserialize($strList);
                if (count($list) > 0) {
                    foreach ($list as $key => $value) {
                        if ($id == $value['id']) {
                            unset($list[$key]);
                            break;
                        }
                    }
                }
            }
        } else {
            $cookies->remove(self::KEYS_SEARCH);
            $isDelete = true;
        }
        $result = array(
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'isDetele' => $isDelete,
        );
        return $result;
    }

}
