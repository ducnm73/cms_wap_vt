<?php

return  [
    'mainLanguage'=>'mz',
    'languagepool'=>['vi','en','mz','tz'],
    'cdn.static' => 'http://cdn.myclip.co.mz',
    'cdn.site' => 'http://cdn.myclip.co.mz/',
    'local.video' => 'media1',
    'local.image' => 'image1/thumb', 
    'local.site' => 'http://183.182.100.152:8080/',
    'profile.default' => 4,
    'storage.type' => 1, // 1: S3 storage, 0: SAN Storage
    'storage.san.rootPath' => '/video/',
    'san' =>[
    	 'san.mapping.freeData' => '10.120.44.11:8083', // Link mien phi data
    	 'san.mapping.lostData' => '10.120.44.11:8083',
    	 'san.protocol' =>'http',
         'san.address' =>'10.120.44.11:8083',
         'streaming.template' =>'%protocol%://%address%/%prefix%_%encoded_string%_.%extension%',
    ],
    'elasticsearch-curl' => 'http://10.255.62.27:9302/',
    'elasticsearch' => [
        'host' => '10.255.62.27:9302'
        //'host' => '192.168.146.252:9700'
    ],

    'proxy' => [
        'enable' => false,
        'http_proxy' => 'http://proxy-tct:3128'
    ],

    's3' => [
        'services' => [
            'video1' => [
                'accessKey' => 'cdn-vttvas',
                'secretKey' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
                'endPoint' => 'http://10.60.117.250',
            ],
            'image1' => [
                'accessKey' => 'cdn-vttvas',
                'secretKey' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
                'endPoint' => 'http://10.60.117.250',
            ],
            'video2' => [
                'accessKey' => 'GXWQ39P828R6TGZCOSLO',
                'secretKey' => 'b8ts9ui3P+icUwTlSOqZbp6ByDb7OXvVOOJBPohz',
                'endPoint' => 'http://10.60.117.251',
            ],
            'image2' => [
                'accessKey' => 'GXWQ39P828R6TGZCOSLO',
                'secretKey' => 'b8ts9ui3P+icUwTlSOqZbp6ByDb7OXvVOOJBPohz',
                'endPoint' => 'http://10.60.117.251',
            ],
			's2-image3' => [
                'accessKey' => 'GXWQ39P828R6TGZCOSLO',
                'secretKey' => 'b8ts9ui3P+icUwTlSOqZbp6ByDb7OXvVOOJBPohz',
                'endPoint' => 'http://10.60.117.251',
            ],
	        's2-video3' => [
                'accessKey' => 'GXWQ39P828R6TGZCOSLO',
                'secretKey' => 'b8ts9ui3P+icUwTlSOqZbp6ByDb7OXvVOOJBPohz',
                'endPoint' => 'http://10.60.117.251',
            ],
        ],

        'PathStyle' => true,

        'proxy.host' => '',
        'proxy.username' => null,
        'proxy.password' => null,

        'maxDuration' => 5*60,

        'vodcdn.encrypt' => true,
        'vodcdn.image.encrypt' => false,
        'vodcdn.tokenKey' => 'lkjc34xYMp@@@X3434z',
        'vodcdn.encryptWithIp' => false,
        'vodcdn.timeout' => 180000, #giay

        'vodcdn.template' => [
            'VODCDN' => 'http://%domain_name%/%encrypt_string%/playlist.m3u8',
            'OBJCDN' => 'http://%domain_name%/%encrypt_string%'
        ],
        'webcdn.template' => 'http://%domain_name%/%path%',

        #map Bucket to CDN
        'cdn.mapping.image' => [
            #Link image
            'image_mobitv' => 'cdn.myclip.co.mz/local_files/wap_thumbs',
            'image' => 'static1.myclip.vn',
            'image1' => 'static3.myclip.vn',
            'video1' => 'static3.myclip.vn',
			's2-image3' => 'static3.myclip.vn',
			's2-video3' => 'static3.myclip.vn'
			/*'image' => '197.218.16.21 ',
            'image1' => '197.218.16.21 ',
            'video1' => '197.218.16.21 ',
			's2-image3' => '197.218.16.21 ',
			's2-video3' => '197.218.16.21 '*/

        ],
        'cdn.mapping.freeData' => [
            'video' => [
                'VODCDN' => 'video.clip.viettel.vn',
                'OBJCDN' => ''
            ],
            'video1' => [
                'VODCDN' => 's1.myclip.vn',
                'OBJCDN' => 'static2.myclip.vn'
            ],
            'video_mobitv' => [
                'VODCDN' => '',
                'OBJCDN' => ''
            ],
            'video2' => [
                'VODCDN' => 's1.myclip.vn',
                'OBJCDN' => 'static2.myclip.vn'
            ],
            's2-video3' => [
                'VODCDN' => 's1.myclip.vn',
                'OBJCDN' => 'static2.myclip.vn'
            ],
        ],
        'cdn.mapping.lostData' => [
            'video' => [
                'VODCDN' => 'videovod.47bd8e19.viettel-cdn.vn',
                'OBJCDN' => ''
            ],
            'video1' => [
                'VODCDN' => 's1vod.47bd8e19.viettel-cdn.vn',
                'OBJCDN' => 'static2obj.47bd8e19.viettel-cdn.vn'
            ],
            'video_mobitv' => [
                'VODCDN' => '',
                'OBJCDN' => ''
            ],
            'video2' => [
                'VODCDN' => 's1vod.47bd8e19.viettel-cdn.vn',
                'OBJCDN' => 'static2obj.47bd8e19.viettel-cdn.vn'
            ],
            's2-video3' => [
                'VODCDN' => 's1vod.47bd8e19.viettel-cdn.vn',
                'OBJCDN' => 'static2obj.47bd8e19.viettel-cdn.vn'
            ],
        ],



        'video.bucket' => 's2-video3',
        'static.bucket' => 's2-image3'
    ],

    'service.vaaa' => [
        'username' => 'mobitvttpm',
        'password' => 'ttpm2012mbtv1120',
        'wsdl' => \Yii::getAlias("@configFolder") . '/radius.wsdl',
        'connectTimeout' => 5,
        'readTimeout' => 5,
    ],

    'gateway' => [
        'register.url' => 'http://10.255.63.201:9310/register?wsdl',
        'register.username' => 'mps2311',
        'register.password' => 'Mps2311',

        'cancel.url' => 'http://10.255.63.201:9310/cancel?wsdl',
        'cancel.username' => 'mps2311',
        'cancel.password' => 'Mps2311',

        'buyretail.url' => 'http://10.255.63.201:9310/retail?wsdl',
        'buyretail.username' => 'mps2311',
        'buyretail.password' => 'Mps2311',

        'cdr.subType' => 'MYCLIP',
        'cdr.command' => 'VIEW',
        'cdr.prefix' => 'VOD_WATCH',

        'connectTimeout' => 30,
        'readTimeout' => 30,


    ],

    'crawler' => [
        'url.getCrawler' => 'http://10.60.105.122:8686/v1/sync-crawler/get-crawler',
        'url.getMediaAttribute' => 'http://10.60.105.122:8686/v1/sync-metadata/get-media-attribute',
        'url.getAuthorizationCode' => 'http://10.60.105.122:8686/v1/authorization/get-authorization-code',
        'clientId' => 'm8B5jImSLiVGYxnn',
        'clientSecret' => 'Bcxxav02TLGVhpWId6b-h2WcI3YA9y-I',

//        'authorizationCode' => 'ocokxnqF0VerbNUrSy37OIpYNHkzYQi6',
        'timeout' => 5
    ],

    'recommendation' => [
        'enable' => false,
        'url' => 'http://10.30.9.197:8081/nextsmarty/recommend/myclip',
        'timeout' => 10
    ],

    'ws.contracts.url'=>'http://10.58.71.238:8777/HTDS/InsertContractWs?wsdl',
    'ws.contract.username' => '',
    'ws.contract.password' => '',
    'ws.contract.userCreateId' => '',
	
	'unit_price' => 'MT',
];
