<?php

Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('wap', dirname(dirname(__DIR__)) . '/wap');
Yii::setAlias('wapsite', dirname(dirname(__DIR__)) . '/wapsite');
Yii::setAlias('rest', dirname(dirname(__DIR__)) . '/rest');
Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('cp', dirname(dirname(__DIR__)) . '/cp');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('logs', dirname(dirname(__DIR__)) . '/logs');
Yii::setAlias('website', dirname(dirname(__DIR__)) . '/website');
Yii::setAlias('website_v2', dirname(dirname(__DIR__)) . '/website_v2');
Yii::setAlias('superapi', dirname(dirname(__DIR__)) . '/superapi');