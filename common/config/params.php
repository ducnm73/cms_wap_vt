<?php
# thu muc cau hinh upload file anh feedback
Yii::setAlias('@uploadFolder', realpath(dirname(__FILE__) . '/../../uploads/'));
#thu muc xuat CDR
Yii::setAlias('@cdrFolder', realpath(dirname(__FILE__) . '/../../cdr/'));
#thu muc cau hinh chung
Yii::setAlias('@configFolder', realpath(dirname(__FILE__) . '/../../common/config/'));

#thu muc root
Yii::setAlias('@yiiConsole', realpath(dirname(__FILE__) . '/../../yii'));

return [
    # '/statics/css/3007/'
    //'statics.css.cache.path'=>'/statics/css/3007/',
    # '/statics/js/3007/'
   // 'statics.js.cache.path'=>'/statics/js/3007/',
    'message.for.2010'=>[
        "Phụ Nữ là những loài hoa, Tỏa hương thơm ngát vang xa đất trời",
        "Nhân ngày 20 tháng 10, chúc em có làn da mịn màng, chúc cho môi thắm dịu dàng, chúc cho chân trắng lại càng thêm xa.",
        "Chúc các bạn nữ: Hay ăn chóng béo, Tiền nhiều như kẹo, Tình chặt như keo, Dẻo dai như mèo, Mịn màng trắng trẻo, Sức khỏe như Heo",
        "Ngày 20/10 tôi xin dành tặng bạn lời chúc sức khỏe, vui vẻ, hát hay như chim sẻ, và có nhìu tài lẻ, nói chung là làm gì cũng suôn sẻ.",
        "Hãy gác lại mọi việc, nghỉ ngơi đôi phút và có một ngày 20/10 thật ý nghĩa bên cạnh những người thân yêu bạn nhé!",
        "Nhân ngày phụ nữ Việt Nam, Myclip xin chúc bạn: Luôn tươi như hoa. Mịn màng làn da. Tình cảm thiết tha. Việc nhà nết na. Việc nước xông pha.",

    ],
    'convert.843x.list'=>[
        #"84161"=>"8431",
        #"84162"=>"8432",
    ],
    'expire.time.activate.smartTV' => 2,
    'expire.time.token.smartTV' => 2,
    'category.load.more.limit'=>8,
    'max.channel.related'=>20,
    'random.avatar.channel'=>[
          ["bucket"=>"image1", "path"=>"banner_random/meuclip.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd01.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd02.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd03.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd04.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd05.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd06.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd07.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd08.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd09.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd10.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd11.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd12.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd13.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd14.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd15.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd16.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd17.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd18.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd19.jpg"],
//        ["bucket"=>"image1", "path"=>"banner_random/avatarfd20.jpg"]
    ],
    'random.banner.channel'=>[["bucket"=>"image1", "path"=>"banner_random/1003g.jpg"]],

    'contract.condition.short'=>'<p>Để thực hiện nhận tiền từ đăng tải(upload) video đăng lên MyClip. Cần xác nhận đồng ý với <a  href="http://myclip.vn/dieu-khoan-su-dung">Điều khoản</a> và <a href="http://myclip.vn/quy-che-hoat-dong">Quy chế hoạt  động</a> của dịch vụ MyClip.</p>  <p>Cam kết không vi phạm bản quyền hoặc quyền riêng tư của người khác.</p>',
    'award.month.report'=>["2018-05"=>"Từ 22/05/2018 đến 21/06/2018","2018-06"=>"Từ 22/06/2018 đến 21/07/2018","2018-07"=>"Từ 22/07/2018 đến 22/08/2018" ],
    'award.month.report.range'=>["2018-05"=>"2018-05-22,2018-06-21","2018-06"=>"2018-06-22,2018-07-21","2018-07"=>"2018-07-22,2018-08-22" ],

    'app.domain'=>'http://meuclip.co.mz',
    'award.blacklist.date'=>['2018-06-19', '2018-07-20', '2018-08-19'],
    'myclip.app.facebook.id'=>'355961724747909',
    'event.limit.award.per.page'=>5,
    'event.award.type.month'=>['500_FISRT_REGISTER_PACKAGE_M_MONTH', '2500_FIRST_REGISTER_PACKAGE_D_MONTH', '1000_FIRST_SHARE_MONTH', '500_TOP_COMMENT_MONTH', '500_TOP_LIKE_MONTH', '500_TOP_FOLLOW_MONTH','500_FIRST_UPLOAD_MONTH'],
    'event.award.type.week'=>['10_TOP_COMMENT_WEEK', '10_TOP_LIKE_WEEK', '10_TOP_FOLLOW_WEEK'],
    'video.limit.n.box.home'=>4,
    'app.home.menuchannel.limit'=>15,
    'max.related.video'=>50,
    #Limit so video duoc xem mien phi khi dang doi dang ky
    'num.video.viewfree.waitregister'=>2,
    'app.googleadwords.relate.limit'=>30,
    'app.home.hotbox.limit'=>16,
    'num.max.video.add.later'=>100,
    #Limit so video tra ve o cac box video
    'app.home.hotchannel.limit'=>5,
    'app.page.limit' => 4,
    'app.home.lazy.limit' => 12,
    #Limit so phim tra ve
    'app.page.film.limit' => 6,
    #Limit so video lien quan
    'app.video.relate.limit' => 10,
    #Limit so phim lien quan
    'app.film.relate.limit' => 24,
    #Limit so item moi lan load them
    'app.lazy.item.limit' => 36,
    #Limit so item khi get more content(neu ko truyen limit len)
    'app.showMore.limit' => 10,

    'app.hot.keyword.limit' => 10,

    'app.menu.limit' => 10,

    'app.video.channel.limit' => 10,

    'app.follow.content.limit' => 40,

    'app.category.limit' =>10,
    'category.hot.limit' => 10,
    'video.hot.random.limit' => 30,

    'report.limit' => 12,

    'otp.lock.count' => 5,

    'otp' => [
        'timeout' => 600, #x seconds
        'length' => 6, #x chars
        'msisdnPerDay' => 3, #x times
        'ipPerDay' => 5, #x times
        'failToDestroy' => 3 # so luot check OTP sai thi xoa
    ],
    'register.reg.success' => "0",
    'register.unreg.success' => "0",
    'buy.success' => "0",
    'comment.minlength' => 1,
    'comment.maxlength' => 1000,

    'playlist.name.minlength' => 1,
    'playlist.name.maxlength' => 255,

    'playlist.description.minlength' => 1,
    'playlist.description.maxlength' => 255,

    'channel.name.minlength' => 1,
    'channel.name.maxlength' => 100,

    'channel.description.minlength' => 1,
    'channel.description.maxlength' => 1000,

    'rate.limiter' => 1000, #max TPS of Wapsite & API

    'quota.free.firsttime' => 10,
    'quota.free.before30day' => 10,
    'quota.free.after30day' => 10,
    'quota.free.n.day'=>30,
    'quota.video.free.n.day'=>10,

    'accept_lost_data.timeout' => 3 * 60 * 60,

    'getprofile.facebook' => 'https://graph.facebook.com/me?access_token=',
    'getprofile.google' => 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=',

    'list.package.infoMessage' => 'Xin chào #msisdn, Quý khách đang sử dụng #packageName.',
    //'list.package.infoMessage' =>'Xin chào #msisdn, Quý khách đang sử dụng #packageName.',

    'list.package.confirm' => 'Xem không giới hạn Data 3G các nội dung đặc sắc, hấp dẫn với dịch vụ MyClip, #packageName. Đăng ký ngay #priceđ/#cycle.',
    'list.package.confirm2' => 'Quý khách vừa yêu cầu đăng ký #packageName với giá #price đồng. Bấm ĐỒNG Ý để xác nhận đăng ký',
    'list.package.cancel.confirm' => 'Bạn có muốn Hủy dịch vụ MyClip #packageName?',

    'popup.confirm_000' => 'Không có quyền xem nội dung',
    # Confim mua sub
    'popup.confirm_100' => 'You have run out of free videos (#free_times videos per day) within #free_range days. Please register to continue watching all video via unlimited high speed network bandwidth. Thank you!', // #subPriceđ/#cycle.
    'popup.confirm_not_login_100' => 'You have run out of free videos (#free_times videos per day) within #free_range days. Please login to continue watching video via unlimited high speed network bandwidth. Thank you!',
    # Confim mua sub va video
    'popup.confirm_110' => 'Xem hàng #cycle Phim Hot, Clip đặc sắc hoàn toàn miễn cước 3G/4G tốc độ cao chỉ với #subPriceđ/#cycle hoặc mua lẻ với giá #feeđ',
    # Confim mua sub va playlist
    'popup.confirm_101' => 'Xem hàng #cycle Phim Hot, Clip đặc sắc hoàn toàn miễn cước 3G/4G tốc độ cao chỉ với #subPriceđ/#cycle hoặc mua playlist #playlistName với giá #playlistPriceđ ',
    # Confim mua sub va video va playlist
    'popup.confirm_111' => 'Xem hàng #cycle Phim Hot, Clip đặc sắc hoàn toàn miễn cước 3G/4G tốc độ cao chỉ với #subPriceđ/#cycle hoặc mua lẻ với giá #feeđ hoặc mua playlist #playlistName với giá #playlistPriceđ',
    # Confim mua playlist
    'popup.confirm_001' => 'Để xem #videoName, quý khách vui lòng mua playlist #playlistName với giá #playlistPriceđ ',
    # Confim mua  video va playlist
    'popup.confirm_011' => 'Để xem #videoName, quý khách vui lòng mua lẻ với giá #feeđ hoặc mua playlist #playlistName với giá #playlistPriceđ ',
    # Confim mua  video
    'popup.confirm_010' => 'Để xem #videoName, quý khách vui lòng mua lẻ với giá #feeđ',
    # Confim buoc 2 dang ky
    'popup.confirm.registersub' => 'Have you agree to register #packageName with price #subPriceMT/#cycle (renew every #cycle)',
    # Confim buoc 2 mua  playlist
    'popup.confirm.buyplaylist' => 'Quý khách có đồng ý mua playlist #playlistName với giá #playlistPriceđ',
    # Confim buoc 2 mua  video
    'popup.confirm.buyvideo' => 'Quý khách có đồng ý mua video #videoName với giá #feeđ',

    'popup.confirm.accept_loss_data' => 'Chọn Xem Tiếp sẽ mất phí 3G/4G theo gói Data đang sử dụng',

    'popup.auto.count' => 2,

    'popup.suggestion' => [
        'is_register_sub' => true,
        'confirm' => 'Đăng ký xem hàng #cycle Phim Hot, Clip đặc sắc hoàn toàn miễn cước 3G/4G tốc độ cao chỉ với #subPriceđ/#cycle (gia hạn theo #cycle)',
        'confirm_register_sub' => 'Xác nhận đăng ký #packageName để xem Phim hot, Clip đặc sắc miễn cước 3G/4G (#subPriceđ/#cycle, gia hạn theo #cycle)',
        'default_package_id' => 1
    ],

    'distribution.popup.suggestion' => [
        'is_register_sub' => true,
        'confirm' => 'Quý khách bấm "Đăng ký" để xem toàn bộ Video không giới hạn lưu lượng Data tốc độ cao trên http://#subDomain  Phí DV #subPriceđồng/ngày, gia hạn hàng #cycle',
        'confirm_register_sub' => 'Xác nhận đăng ký #packageName để xem Phim hot, Clip đặc sắc miễn cước 3G/4G (#subPriceđ/#cycle, gia hạn theo #cycle)',
        'default_package_id' => 1
    ],


    'msisdn.regx' => '/^(258)(((87|86|62|71|96|97|98|16[0-9]|3[0-9])\d|86[1-9])\d{6})$/',

    'streaming.vodProfileId' => 6,
    'download.vodProfileId' => 3,

    'refreshToken.timeout' => 1296000, #15 days
    'accessToken.timeout' => 3600, #1 hours
    'userAgent.secretCode' => 'dH3$sYf#dDl',
    'historyView.limit' => 100,


    #so feedback toi da trong ngay
    'feedback.limit' => 10,
    'feedback.max.size' => 5242880,
    'feedback.max.size.label' => 'Kích thước ảnh lỗi quá lớn, tối đa 5M',
    'video.folow.limit' => 4,

    #cau hinh login
    'login.captcha.show.count' => 3,
    'login.lock.count' => 10,

    #deeplink


    'video.vodsubtype.list' => '1123,1028,1118,1037,1031,1034,1025,1097,1040',

    'home.order.list' => '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25',

    'setting.quality' => [
        [
            "name" => '360p',
            "vod_profile_id" => 7,
            "live_profile_id" => 12
        ],
        [
            "name" => '480p',
            "vod_profile_id" => 20,
            "live_profile_id" => 11
        ],
        [
            "name" => '720p',
            "vod_profile_id" => 21,
            "live_profile_id" => 19
        ]
    ],

    'setting.errorType' => [
        [
            'id' => 1,
            'content' => 'Không xem được'
        ],
        [
            'id' => 2,
            'content' => 'Nội dung bạo lực'
        ],
        [
            'id' => 3,
            'content' => 'Chất lượng kém'
        ],
        [
            'id' => 4,
            'content' => 'Load chậm, chập chờn'
        ],
    ],

    'revenue.user.upload.value' => [
        '-1' => 'Tạm tính',
        '0' => 'Chưa thanh toán',
        '1' => 'Đã thanh toán'
    ],

    'object.type' => ['VOD', 'FILM', 'PLAYLIST'],
    'slideshow.object.type' => ['VOD', 'FILM', 'TOPIC','CATEGORY',  'HREF'],
    'slideshow.location' => ['HOME', 'VOD', 'FILM', 'HOME_BANNER'],

    'setting.feedback' => [
        [
            'id' => 6,
            'content' => 'Nội dung khiêu dâm'
        ],
        [
            'id' => 7,
            'content' => 'Vi phạm quyền của tôi'
        ],
        [
            'id' => 8,
            'content' => 'Chất lượng video kém, mờ'
        ],
        [
            'id' => 9,
            'content' => 'Mạng kém, bị loading và chập chờn'
        ],
        [
            'id' => 10,
            'content' => 'Nội dung bạo lực hoặc phản cảm'
        ],
        [
            'id' => 11,
            'content' => 'Nội dung kích động thù địch hoặc lạm dụng'
        ]
    ],

    'type.buy' => ['VOD', 'PLAYLIST'],
    'alias.prefix' => 'Tập ',

    'setting.feedback.all' => [
        [
            'id' => 1,
            'content' => Yii::t('wap','Không xem được')
        ],
        [
            'id' => 2,
            'content' => Yii::t('wap','Nội dung bạo lực')
        ],
        [
            'id' => 3,
            'content' => Yii::t('wap','Chất lượng kém')
        ],
        [
            'id' => 4,
            'content' => Yii::t('wap','Load chậm, chập chờn')
        ],
        [
            'id' => 5,
            'content' => Yii::t('wap','Yêu cầu nội dung')
        ],
        [
            'id' => 6,
            'content' => Yii::t('wap','Nội dung khiêu dâm')
        ],
        [
            'id' => 7,
            'content' => Yii::t('wap','Vi phạm quyền của tôi')
        ],
        [
            'id' => 8,
            'content' => Yii::t('wap','Chất lượng video kém, mờ')
        ],
        [
            'id' => 9,
            'content' => Yii::t('wap','Mạng kém, bị loading và chập chờn')
        ],
        [
            'id' => 10,
            'content' => Yii::t('wap','Nội dung bạo lực hoặc phản cảm')
        ],
        [
            'id' => 11,
            'content' => Yii::t('wap','Nội dung kích động thù địch hoặc lạm dụng')
        ]
    ],
    'setting.comment.decline' => [
        '1' => Yii::t('wap','Tục tĩu, khiêu dâm'),
        '2' => Yii::t('wap','Spam'),
        '3' => Yii::t('wap','Ký tự đặc biệt'),
        '4' => Yii::t('wap','Quảng cáo'),
        '5' => Yii::t('wap','Ngôn ngữ nhạy cảm'),
        '6' => Yii::t('wap','Kích động bạo lực, thù địch, chống phá nhà nước'),
        '7' => Yii::t('wap','Lừa đảo'),
    ],
	/*'sms_short_code' => '15738',
    'mps' => [
        'charge_url' => '',
        'detect_url' => 'http://169.255.187.88/MPS/mobile.html',
        'otp_url' => '',
        //'charge_url' => 'http://169.255.187.88/MPS/charge.html',
        //'otp_url' => 'http://169.255.187.88/MPS/otp.html',
        'private_key' => Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'keys' . DIRECTORY_SEPARATOR . 'PrivateKeyCP.pem',
        'public_key' => Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'keys' . DIRECTORY_SEPARATOR . 'PublicKeyCP.pem',
        'public_key_vt' => Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'keys' . DIRECTORY_SEPARATOR . 'PublicKeyVT.pem',
        //'proxy' => '192.168.193.13',
        //'proxy_port' => '3128',
        'proxy' => '',
        'proxy_port' => '',
        'provider' => 'VTT',
        'service' => 'MCLIP',
        'cp_code' => '123',//ko quan tam
        'default_sub_service' => 'MCLIP_D_1',
    ],*/
    'cache.enabled' => false,
];
