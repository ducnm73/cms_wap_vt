<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=192.168.146.252:3307;dbname=meuclip',
            'username' => 'db_kdqt',
            'password' => 'db_kdqt',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache' => 'false',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => '192.168.146.252',
            'port' => 9311,
            'password' => 'Sxpm@2021',
        ],
        'elasticsearch' => [
            'class' => 'yii\elasticsearch\Connection',
            'connectionTimeout' => 3,
            'dataTimeout' => 3,
            'nodes' => [
                [
                    'http_address' => '10.255.67.170:9212'
                    // 'http_address' => '10.255.67.174:9300'
                ],                                                                                             
            ],
        ],
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://myclip_moz_user@10.255.67.170:8688/mongo_myclip_moz',
            'options' => [
                "username" => "myclip_moz_user",
                "password" => "123456aAbB"
            ]
        ],
    ],
];
