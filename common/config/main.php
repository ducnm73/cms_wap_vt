<?php

return [
    'name'=>'MobiTV',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'httpClient' => [
                # 'transport' => 'yii\httpclient\CurlTransport',
            ],
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '355961724747909',
                    'clientSecret' => '8ce1bbca3d94531ff57063c290ee8837',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'attributeNames' => ['name', 'email', 'first_name', 'last_name', 'picture.type(large)', 'cover'],
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '876981825084-kogupckaaesr4il6kt0p4e2rdn0eeuvl.apps.googleusercontent.com',
                    'clientSecret' => 'KpBq0tSKsqouikHQp131FzTR'
                ],

            ],
        ],
        'formatter' => [
            'dateFormat' => 'dd/MM/yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'EUR',
        ],
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'wap*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'web*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'api*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
//                'api*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@common/messages',
//                ],
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logVars' => [],
                    'logFile' => '@logs/error.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logVars' => [],
                    'logFile' => '@logs/warning.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logVars' => [],
                    'logFile' => '@logs/info.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'logFile' => '@logs/queries.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['service'],
                    'logVars' => [],
                    'logFile' => '@logs/service.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['vaaa'],
                    'logVars' => [],
                    'logFile' => '@logs/vaaa.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['charging'],
                    'logVars' => [],
                    'logFile' => '@logs/charging.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['streaming'],
                    'logVars' => [],
                    'logFile' => '@logs/streaming.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['traceview'],
                    'logVars' => [],
                    'logFile' => '@logs/traceview.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['view'],
                    'logVars' => [],
                    'logFile' => '@logs/view.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['s3'],
                    'logVars' => [],
                    'logFile' => '@logs/s3.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['log_device'],
                    'logVars' => [],
                    'logFile' => '@logs/log_device.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['console'],
                    'logVars' => [],
                    'logFile' => '@logs/console.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['log_vod'],
                    'logVars' => [],
                    'logFile' => '@logs/log_vod.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['ratelimiter'],
                    'logVars' => [],
                    'logFile' => '@logs/ratelimiter.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['recommend'],
                    'logVars' => [],
                    'logFile' => '@logs/recommend.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['maianh'],
                    'levels' => ['info'],
                    'logVars' => [],
                    'logFile' => '@logs/maianh.log',
                    'maxFileSize' => 102400, //100MB
                    'rotateByCopy' => false,
                    'maxLogFiles' => 20
                ]




            ],
        ],
    ],
];
