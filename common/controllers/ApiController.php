<?php

namespace common\controllers;

use backend\models\VtUser;
use common\helpers\Des;
use common\helpers\Mobile_Detect;
use common\helpers\RemoveSignClass;
use common\helpers\Utils;
use common\libs\VtFlow;
use common\libs\VtHelper;
use common\models\MongoDBModel;
use common\models\VtDistributionBase;
use common\models\VtUserBase;
use common\models\VtVideoBase;
use common\modules\v1\libs\ResponseCode;
use cp\models\VtVideo;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

/**
 * Api controller
 */
class ApiController extends Controller
{

    public $language;
    public $datetime;
    public $userId;
    public $msisdn = '';
    public $fullName = '';
    public $authJson = [
        'responseCode' => ResponseCode::FORBIDDEN,
        'message' => 'Xác thực không hợp lệ',
    ];
    public $accessToken = '';
    public $needHiddenFreemiumContent = 0;
    public $needShowPopupRequireInstallNewApp = 0;

    public $avatarImage = 0;
    public $coverImage = 0;
    public $cpId;

    public $loginVia = "";
    public $distributionId = null;
    public $categoryIds = [];
    public $subDomain = null;

    public function initApi()
    {

        Yii::$app->user->enableSession = false;

        $this->datetime = round(microtime(true) * 1000);

        $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
        $this->language = Yii::$app->request->getHeaders()->get('language'); //vi
        // kiem tra xem this->language co nam trong mang khong
        $languagePool=Yii::$app->params['languagepool'];
        // neu trong mang
        if(in_array($this->language,$languagePool)){
            Yii::$app->language=$this->language;
        }else{
            Yii::$app->language=Yii::$app->params['mainLanguage'];
        }


        if ($authHeader === null) {
            $this->authJson = [
                'responseCode' => ResponseCode::UNAUTHORIZED,
                'message' => Yii::t('web','Khách hàng chưa đăng nhập'),
            ];
        } elseif (!preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches)) {
            Yii::info("Authorization 403 - Case1" . $authHeader);
            $this->authJson = [
                'responseCode' => ResponseCode::FORBIDDEN,
                'message' => Yii::t('web','Xác thực không hợp lệ'),
            ];
        } else {

            $accessToken = Des::RSADecrypt($matches[1]);
            Yii::info("TRACK AUTH |" . json_encode($accessToken));

            $this->accessToken = $accessToken;
            $expired_time = $accessToken[0];
            $userId = $accessToken[1];
            $msisdn = $accessToken[2];
            $uaEncrypted = $accessToken[3];
            //Tham so xem co can an noi dung tinh phi khi duyet APP iOS
            $needHiddenFreemiumContent = ($accessToken[4]) ? 1 : 0;
            $needShowPopupRequireInstallNewApp = ($accessToken[4] == 2) ? 1 : 0;

            $this->loginVia = ($accessToken[5]) ? $accessToken[5] : 'NA';

            $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ua';
            $userAgentEncrypted = md5(Yii::$app->params['userAgent.secretCode'] . $userAgent);

            if ((!$userId && !$msisdn) || $uaEncrypted != $userAgentEncrypted || $expired_time < time()) {
                Yii::info("Authorization 403 - Case2 |" . $userId . "|" . $expired_time . "|" . time() . "|" . $userAgentEncrypted . "|" . $uaEncrypted);
                $this->authJson = [
                    'responseCode' => ResponseCode::FORBIDDEN,
                    'message' => Yii::t('web','Xác thực không hợp lệ'),
                ];
            } else {
                //Gan quyen truy cap
                $this->userId = ($userId) ? $userId : 0;
                $this->msisdn = $msisdn;
                $this->needHiddenFreemiumContent = $needHiddenFreemiumContent;
                $this->needShowPopupRequireInstallNewApp = $needShowPopupRequireInstallNewApp;
            }
        }

    }

    public function initWap()
    {

        // hide this line to dissable select language
        $this->language = Yii::$app->session->get('lang', 'lo');
        //var_dump($this->language);die;
        // kiem tra xem this->language co nam trong mang khong
        $languagePool=Yii::$app->params['languagepool'];
        // neu trong mang
        if(in_array($this->language,$languagePool)){
            Yii::$app->language=$this->language;
        }else{
            Yii::$app->language=Yii::$app->params['mainLanguage'];
        }




        // session
        Yii::$app->user->enableSession = true;
        $this->datetime = round(microtime(true) * 1000);
        $session = Yii::$app->session;

        $utmSource = RemoveSignClass::removeSignOnly(Yii::$app->request->get('utm_source', ''));
        if ($utmSource == '') {
            $source = "WAP";
        } else {
            $source = "WAP|" . strtoupper($utmSource);
            $session->set('source', $source);
        }

        //Xu ly so dien thoai
        $msisdnLogin = $session->get('msisdn_login', 0);
        $this->msisdn = $session->get('msisdn', 0);
        $this->userId = $session->get('userId', 0);
        $this->fullName = $session->get('fullName', '');
        $this->avatarImage = $session->get('avatarImage', '');
        $this->coverImage = $session->get('coverImage', '');
        $this->cpId = $session->get('cpId', '');

        $hasBeenLogout = Yii::$app->session->get("has_been_logout", 0);

        if ($msisdnLogin) {

            if ($msisdnLogin != $this->msisdn) {
                $this->msisdn = $msisdnLogin;
                $session->set('msisdn', $this->msisdn);
            }
        } elseif (!$this->msisdn && !$hasBeenLogout) {//Neu chua co sdt va khach hang chua logout

            $this->msisdn = VtHelper::getMsisdn();
            if ($this->msisdn) {
                $session->set('isDetect3G', true);
                $session->set('msisdn', $this->msisdn);
                $objUser = VtUserBase::getByMsisdn($this->msisdn);

                if (!$objUser) {
                    $objUser = new VtUserBase();
                    $objUser->insertAutoLogin($this->msisdn);
                }

//                if ($objUser) {
                $this->userId = $objUser->id;
                $this->fullName = $objUser->full_name;
                $session->set('userId', $objUser->id);
                $session->set('fullName', $objUser->full_name);
                $session->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');
                $session->set('coverImage', ($objUser['channel_bucket'] && $objUser['channel_path']) ? VtHelper::getThumbUrl($objUser['channel_bucket'], $objUser['channel_path'], VtHelper::SIZE_COVER) : '');
                $session->set('cpId', $objUser->cp_id);

                //kiem tra hien thi trang chon chu de
                $session->set('isShowSuggest', ($objUser->is_show_suggest) ? 0 : 1);
//                }
                // Neu khong phai thanh vien thi kiem tra trong redis xem da suggest chon Topic hay chua?
                if (!$this->userId) {
                    $redisCache = Yii::$app->getCache();
                    $isShowSuggest = $redisCache->get("suggest_" . Utils::getMobileNumber($this->msisdn, Utils::MOBILE_GLOBAL));
                    if (!$isShowSuggest) {
                        $session->set('isShowSuggest', 1);
                    }
                    $this->userId = 0;
                }
            }
        }

        //var_dump($session->get('avatarImage'));die;
        //- Neu dang nhap va chua co full name thi lay full name
        if ($this->userId && !$this->fullName) {
            //var_dump(!$this->fullName);die;
            $objUser = VtUserBase::getById($this->userId);
            $session->set('fullName', ($objUser['full_name']) ? $objUser['full_name'] : Utils::hideMsisdnLast($objUser['msisdn']));
            $session->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');
            $session->set('coverImage', ($objUser['channel_bucket'] && $objUser['channel_path']) ? VtHelper::getThumbUrl($objUser['channel_bucket'], $objUser['channel_path'], VtHelper::SIZE_COVER) : '');

            //kiem tra hien thi trang chon chu de
            $session->set('isShowSuggest', ($objUser['is_show_suggest']) ? 0 : 1);
        } else {
            //Neu detect so dien thoai thi lay full name
            //var_dump(!$this->fullName);die;
            if ($this->msisdn && !$this->fullName) {
                //var_dump(!$this->fullName);die;
                $objUser = VtUserBase::getByMsisdn($this->msisdn);
                $session->set('fullName', ($objUser['full_name']) ? $objUser['full_name'] : Utils::hideMsisdnLast( $objUser['msisdn']));
            }
        }


        $objUser = VtUserBase::getById($this->userId);
        $session->set('avatarImage',  $objUser['path'] ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');
        $session->set('themeUserId', $objUser['theme']);
        if ($session->get("needChangePassword", 0) == 1) {
            if (Yii::$app->requestedRoute != "auth/change-password" && Yii::$app->requestedRoute != "auth/captcha") {
                $session->setFlash("success", "Quý khách vui lòng đổi mật khẩu lần đầu tiên đăng nhập");
                $this->redirect("/auth/change-password");
            }
        }

        //Logic detect profile
        $detect = new Mobile_Detect();
        $ip = VtHelper::getAgentIp();
        $ua = $_SERVER['HTTP_USER_AGENT'];
        if (!$session->has('vod_profile_id')) {
            //Default value
            $isPc = 0;
            $deviceId = null;
            $isMp4Only = 0;
            $isOpera = VtHelper::isV_opreraIp($ip);
            $isIOS = 0;
            if ($isOpera) { // Neu thuoc dai ip opera mini
                $vodProfileId = 6;
            } elseif (!$detect->isTablet() && !$detect->isMobile()) {
                //PC
                $isPc = 1;
                $vodProfileId = 6;
            } else {
                $isPc = 0;
                if ($detect->isiOS()) {
                    $isIOS = 1;
                    $vodProfileId = 6;
                } elseif ($detect->isBlackBerryOS() || $detect->isBadaOS() || $detect->isMeeGoOS() || $detect->isMaemoOS() || $detect->isPalmOS()) {
                    if (($detect->version('BlackBerry') > 10) && $detect->isBlackBerryOS()) {
                        $vodProfileId = 6;
                    } else {
                        $vodProfileId = 1;
                    }
                } elseif ($detect->isAndroidOS()) {
                    $version = $detect->version('Android');
                    if ($version >= 4.0) {
                        $vodProfileId = 6;
                    } else if ($version >= 3.0) {
                        $vodProfileId = 2;
                    } elseif ($version >= 2.36) {
                        $vodProfileId = 2;
                    } else {
                        $vodProfileId = 1;
                    }
                }

                if ($detect->isWindowsPhoneOS() || $detect->isWindowsMobileOS()) {
                    $isMp4Only = 1;
                    $vodProfileId = 3;
                }

                if ($detect->isFirefox()) {
                    $vodProfileId = 3;
                    $isMp4Only = 1;
                }
            }

            $session->set('is_mp4_only', $isMp4Only);
            $session->set('is_pc', $isPc);
            $session->set('vod_profile_id', $vodProfileId);

            $session->set('is_iOS', $isIOS);

            Yii::info('LOG_DEVICE|' . session_id() . "|" . $this->msisdn . "|" . $this->userId . "|" . str_replace('|', '_', $source) . "|" . $vodProfileId . "|" . $ua, 'log_device');
            //Neu da co thong tin user thi ko can ghi log update
            if ($this->userId) {
                Yii::$app->session->set("is_write_log_device", 0);
            }
        }

        //Neu dang nhap lai thi insert log
        if ($this->userId) {
            $needWriteLog = Yii::$app->session->get("is_write_log_device", 1);
            if ($needWriteLog) {
                Yii::$app->session->set("is_write_log_device", 0);
                Yii::info('LOG_DEVICE|UPDATE|' . session_id() . "|" . $this->msisdn . "|" . $this->userId . "|" . str_replace('|', '_', $source) . "|" . $session->get('vod_profile_id') . "|" . $ua, 'log_device');
            }
        }

        if (!$this->isValidMsisdn($this->msisdn) && !$this->isValidUser()) {
            $this->authJson = [
                'responseCode' => ResponseCode::UNAUTHORIZED,
                'message' => Yii::t('web','Khách hàng chưa đăng nhập'),
            ];
        }

        //Xu ly share facebook insert event_log
        $shareToken = Yii::$app->request->get('share_token', '');

        if($shareToken){
            $isShare = $session->get("shareToken_".$shareToken);
            if($isShare){
                $objArrVideo= VtVideoBase::getDetail($isShare);
                MongoDBModel::insertEvent(MongoDBModel::SHARE_FACEBOOK,$isShare, $this->userId, $this->msisdn, "",$objArrVideo["created_by"]  );
                $session->remove("shareToken_".$shareToken);
            }
        }

        $this->distributionId = $session->get('distributionId', null);
        $this->subDomain = $session->get('subDomain', null);
        $this->categoryIds = explode(',', $session->get('category_ids', ''));

        if(!isset($this->distributionId)){
            $distribution = VtDistributionBase::getOneBySubDomain($_SERVER['HTTP_HOST']);

            if(!empty($distribution)){
                $this->distributionId = $distribution['id'];
                $session->set('distributionId', $this->distributionId );
                if(!empty($distribution['category_ids'])){
                    $session->set('category_ids', $distribution['category_ids']);
                    $this->categoryIds = explode(',', $distribution['category_ids']);
                }
                $this->subDomain = $distribution['sub_domain'];
                $session->set('subDomain', $this->subDomain );
            }else{
                $session->set('distributionId', 0);
            }
        }


    }

    public function afterAction($action, $result)
    {
        Yii::info("Response time of " . Yii::$app->request->getUrl() . ": " . (round(microtime(true) * 1000) - $this->datetime) . " ms");

        return parent::afterAction($action, $result);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

//        $behaviors['rateLimiter'] = [
//            'class' => Yii\filters\RateLimiter::className(),
//            'user' => new \common\models\User(),
//            'enableRateLimitHeaders' => false
//        ];

        return $behaviors;
    }

    /**
     * Ham check quyen truy cap
     * @author ducda2@viettel.com.vn
     * @return bool
     */
    protected function isValidUser()
    {
        if (empty($this->userId)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Ham check so dien thoai hop le
     * @return bool
     */
    protected function isValidMsisdn()
    {
        if (!Utils::isValidMsisdn($this->msisdn)) {
            return false;
        } else {
            return true;
        }
    }

    protected function getUserTracking()
    {
        return [
            'user_info' => json_encode([
                'user_id_db' => $this->userId,
                'email' => null,
                'name' => null,
//                'phone_number' => $this->msisdn,
                'goi_cuoc_id' => null,
                'age' => null,
                'gender' => null,
                'no_followers' => null,
            ])
        ];
    }

}
