<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/../../common/config/pools.php'),
    require(__DIR__ . '/params.php')
);
$routing = array_merge(require(__DIR__ . '/routes.php'));

return [
    'id' => 'app-superapi',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
        'v2' => [
            'class' => 'api\modules\v2\Module',
        ],

    ],
    'controllerNamespace' => 'superapi\controllers',
    'defaultRoute' => 'v1/default/get-home',
    
    'components' => [
        'user' => [
            'identityClass' => 'superapi\modules\v1\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 3600*24*30,
        ],
        'session' => [
    'name' => 'myvideo-frontend',
    'class' => 'yii\redis\Session',
    'redis' => 'redis', // id of the connection application component
    'timeout' => 3600*24*30
],
        'errorHandler' => [
            'errorAction' => 'v1/default/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => true,
            // Disable r= routes
            'enablePrettyUrl' => true,
            //chi cho phep chay cac pretty url
            //'enableStrictParsing' => true,
            //'suffix' => '.html',
            'rules' => $routing,
        ],
        
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;

                if ($response->data !== null) {
                    $response->statusCode = $response->data->responseCode;
                }
            },
        ],
        'cache' => [
            'class' => 'common\helpers\MobiTVRedisCache',
            'redis' => 'redis' // id of the connection application component
        ],
        
    ],
    'params' => $params,
];
