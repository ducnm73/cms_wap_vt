<?php
/**
 * Created by PhpStorm.
 * User: thuynd
 * Date: 7/24/2018
 * Time: 11:16 AM
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CommentAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/comment.js',
    ];
}
