<?php

namespace backend\models;

use Yii;

class VtSmsMt extends \common\models\VtSmsMtBase {

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Số điện thoại'),
            'message' => Yii::t('app', 'Nội dung'),
            'mo_his_id' => Yii::t('app', 'Mo Id'),
            'sent_time' => Yii::t('app', 'Thời gian gửi'),
            'status' => Yii::t('app', 'Trạng thái'),
            'receive_time' => Yii::t('app', 'Thời gian hoàn tất gửi'),
            'channel' => Yii::t('app', 'Đầu số'),
            'process_id' => Yii::t('app', 'Process ID'),
        ];
    }

}