<?php

namespace backend\models;

use Yii;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use yii\web\UploadedFile;

class VtUser extends \common\models\VtUserBase {
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_hot', 'cp_id'], 'integer'],
            [['status'], 'string'],
            [['msisdn','priority'], 'integer', 'min' => 0, 'tooSmall' => Yii::t('backend','{attribute} không được nhỏ hơn 0'), 'message' =>Yii::t('backend','{attribute} phải là số nguyên')],
            [['last_login', 'is_show_suggest', 'changed_password', 'follow_count', 'video_count'], 'safe'],
            [['password', 'full_name','oauth_id'], 'string', 'max' => 50],
            [['email'],'email'],
            [['salt', 'bucket'], 'string', 'max' => 255],
            [['msisdn', 'status', 'full_name'], 'required', 'message' => Yii::t('backend','{attribute} không được để trống') ],
            [['otp'], 'string', 'max' => 10],
            [['path'], 'safe'],
            [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png', 'wrongExtension' => Yii::t('backend','Chỉ cho phép các tệp có các phần mở rộng này: jpg, jpeg, png.')],
            [['channel_path'], 'safe'],
            [['channel_path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png', 'wrongExtension' => Yii::t('backend','Chỉ cho phép các tệp có các phần mở rộng này: jpg, jpeg, png.')],
        ];
    }

    public function saveWithImage($avatarDelete = '', $bannerDelete = '')
    {
        $file = UploadedFile::getInstance($this, 'path');
        $channelFile = UploadedFile::getInstance($this, 'channel_path');

        if (!empty($file)) {
            /*$bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);

            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            $this->bucket = $bucket;
            $this->path = $path;*/

            $dirName = $this->path ? dirname($this->path) : dirname('media2/images/attribute/' . Utils::generatePath($file->extension));

            $uploadResult = VtHelper::uploadAndGenerateThumbs($file, $dirName);
            $this->path = $uploadResult['path'];

            if(isset($uploadResult['bucket'])) {
                $this->bucket = $uploadResult['bucket'];
            }
        } else {
            if($avatarDelete == "1"){
                $this->path = null;
            } else{
                unset($this->path);
            }
            unset($this->bucket);
        }

        if (!empty($channelFile)) {
            /*$channelBucket = Yii::$app->params['s3']['static.bucket'];

            $channelPath = Utils::generatePath($channelFile->extension);

            S3Service::putObject($channelBucket, $channelFile->tempName, $channelPath);

            VtHelper::generateAllThumb($channelBucket, $channelFile->tempName, $channelPath);

            $this->channel_bucket = $channelBucket;
            $this->channel_path = $channelPath;*/
            $dirName = $this->channel_path ? dirname($this->channel_path) : dirname('media2/images/attribute/' . Utils::generatePath($file->extension));
            $uploadResult = VtHelper::uploadAndGenerateThumbs($channelFile, $dirName);
            $this->channel_path = $uploadResult['path'];

            if(isset($uploadResult['bucket'])) {
                $this->channel_bucket = $uploadResult['bucket'];
            }
        } else {
            if($bannerDelete == "1"){
                $this->channel_path = null;
            } else{
                unset($this->channel_path);
            }
            unset($this->channel_bucket);
        }
        $this->save();
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend','ID'),
            'msisdn' => Yii::t('backend','Số điện thoại'),
            'status' => Yii::t('backend','Trạng thái'),
            'password' => Yii::t('backend','Password'),
            'salt' => Yii::t('backend','Salt'),
            'full_name' => Yii::t('backend','Tên hiển thị'),
            'email' => Yii::t('backend','Email'),
            'oauth_id' => Yii::t('backend','Oauth ID'),
            'follow_count' => Yii::t('backend','Follow Count'),
            'otp' => Yii::t('backend','Otp'),
            'last_login' => Yii::t('backend','Last Login'),
            'changed_password' => Yii::t('backend','Changed Password'),
            'bucket' => Yii::t('backend','Bucket'),
            'path' => Yii::t('backend','Path'),
            'video_count' => Yii::t('backend','Video Count'),
            'is_show_suggest' => Yii::t('backend','Is Show Suggest'),
            'priority' => Yii::t('backend','Thứ tự'),
        ];
    }

    public static function getUser($id){
        $user = self::find()->where(['id' => $id])->one();
        return $user;
    }
}