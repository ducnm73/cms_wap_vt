<?php

namespace backend\models;

use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use Yii;
use yii\web\UploadedFile;

class VtGroupCategory extends \common\models\VtGroupCategoryBase {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['name', 'description', 'type'], 'required'],
                [['name'], 'string', 'max' => 255],
                [['description'], 'string', 'max' => 1000],
                [['is_active', 'is_hot', 'positions', 'parent_id'], 'integer'],
                [['name', 'bucket', 'path', 'avatar_path', 'avatar_bucket'], 'string', 'max' => 255],
                [['type'], 'string', 'max' => 100],
                [['name'], 'unique'],
                [['path','avatar_path'], 'safe'],
                [['path','avatar_path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],
        ];
    }

    public function getMultilangData() {
        $multilangs = json_decode($this->multilang, true);
        $multilangModels = [];

        if(is_array($multilangs)) {
            $langs = [];

            foreach ($multilangs as $key => $value) {
                $lang = substr($key, strripos($key, '_') + 1);

                if(!in_array($lang, $langs)) {
                    $langs[] = $lang;
                }
            }

            foreach ($langs as $lang) {
                $multilangModel = new GroupCategoryMultilang(['lang' => $lang]);
                $multilangModel->name = isset($multilangs["name_$lang"]) ? $multilangs["name_$lang"] : '';
                $multilangModel->description = isset($multilangs["description_$lang"]) ? $multilangs["description_$lang"] : '';
                $multilangModels[] = $multilangModel;
            }
        }

        return $multilangModels;
    }

    public function setMultilangData($datas) {
        $values = [];

        if(is_array($datas)) {
            foreach ($datas as $data) {
                $values['name_' . $data['lang']] = $data['name'];
                $values['description_' . $data['lang']] = $data['description'];
            }

            $values = array_unique($values);
        }

        $this->multilang = empty($values) ? null : json_encode($values);
    }

    public static function searchByName($q, $limit = 10) {
        $query = self::find()
                ->asArray()
                ->select('id, name as text')
                ->where([
                    'is_active' => self::ACTIVE
                ])
                ->andWhere(['like', 'name', $q])
                ->limit($limit);

        return $query->all();
    }

    public function saveWithImage() {
        $avatarFile = UploadedFile::getInstance($this, 'avatar_path');
        $file = UploadedFile::getInstance($this, 'path');
        
        if (!empty($avatarFile)) {
            /*$bucket = Yii::$app->params['s3']['static.bucket'];
            $path = Utils::generatePath();
            S3Service::putObject($bucket, $avatarFile->tempName, $path);
            VtHelper::generateAllThumb($bucket, $avatarFile->tempName, $path);

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->user_id;
            }

            $this->updated_by = Yii::$app->user->identity->user_id;
            $this->avatar_bucket = $bucket;
            $this->avatar_path = $path;*/
            $dirName = $this->avatar_path ? dirname($this->avatar_path) : dirname('media2/images/attribute/' . Utils::generatePath($avatarFile->extension));
            $uploadResult = VtHelper::uploadAndGenerateThumbs($avatarFile, $dirName);
            $this->avatar_path = $uploadResult['path'];

            if(isset($uploadResult['bucket'])) {
                $this->avatar_bucket = $uploadResult['bucket'];
            }
        } else {
            unset($this->avatar_bucket);
            unset($this->avatar_path);
        }


        if (!empty($file)) {
            /*$bucket = Yii::$app->params['s3']['static.bucket'];
            $path = Utils::generatePath($file->extension);
            S3Service::putObject($bucket, $file->tempName, $path);
            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->user_id;
            }

            $this->updated_by = Yii::$app->user->identity->user_id;
            $this->bucket = $bucket;
            $this->path = $path;*/
            $dirName = $this->path ? dirname($this->path) : dirname('media2/images/attribute/' . Utils::generatePath($avatarFile->extension));
            $uploadResult = VtHelper::uploadAndGenerateThumbs($file, $dirName);
            $this->path = $uploadResult['path'];

            if(isset($uploadResult['bucket'])) {
                $this->bucket = $uploadResult['bucket'];
            }
        } else {
            unset($this->bucket);
            unset($this->path);
        }

        $this->save();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend','ID'),
            'name' => Yii::t('backend','Tên'),
            'type' => Yii::t('backend','Loại'),
            'description' => Yii::t('backend','Mô tả'),
            'is_active' => Yii::t('backend','Kích hoạt'),
            'bucket' => Yii::t('backend','Bucket'),
            'path' => Yii::t('backend','Ảnh banner'),
            'positions' => Yii::t('backend','Vị trí'),
            'parent_id' => Yii::t('backend','Cấp chuyên mục'),
            'avatar_path' => Yii::t('backend','Ảnh đại diện'),
            'avatar_bucket' => Yii::t('backend','Bucket'),
        ];
    }

}
