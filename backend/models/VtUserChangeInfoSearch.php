<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use backend\models\VtUserChangeInfo;

/**
 * VtUserChangeInfoSearch represents the model behind the search form about `backend\models\VtUserChangeInfo`.
 */
class VtUserChangeInfoSearch extends VtUserChangeInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','user_id'], 'integer', 'min' => 0],
//            [['id', 'user_id'], 'integer', 'min' => 0],
            [['description','full_name','user_id'], 'trim'],
            [['status'], 'integer'],
            [['full_name', 'bucket', 'path', 'channel_bucket', 'channel_path', 'description', 'reason', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtUserChangeInfo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]]
        ]);
        $query->where(['or', ['<>', 'full_name', null], ['<>', 'full_name', '']]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'full_name', trim($this->full_name)])
            ->andFilterWhere(['like', 'bucket', trim($this->bucket)])
            ->andFilterWhere(['like', 'path', trim($this->path)])
            ->andFilterWhere(['like', 'channel_bucket', trim($this->channel_bucket)])
            ->andFilterWhere(['like', 'channel_path', trim($this->channel_path)])
            ->andFilterWhere(['like', 'description', trim($this->description)])
            ->andFilterWhere(['like', 'reason', trim($this->reason)]);

        return $dataProvider;
    }


    /*
    * @linth
    * created_at: 21/5/2021
    * */

    public function channelGeneralReport($params)
    {

        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (count($params) < 1) {
            return $dataProvider;
        }
        $split = explode(' - ', $this->created_at);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);
        $query1 = self::find()->asArray()
            ->select('count(*) as count')
            ->addSelect(["DATE_FORMAT(vv.updated_at, '%Y-%m-%d') AS date"])
            ->from(VtVideo::tableName() . ' vv')
            ->where(['vv.type' => 'VOD', 'vv.status' => 2, 'vv.created_by' => $this->user_id])
            ->andWhere(['BETWEEN', 'vv.updated_at', $beginDate, $endDate])
            ->groupBy('date');

        // var_dump($query1->createCommand()->getRawSql());die();


        $query2 = self::find()->asArray()
            ->select('count(*) as count')
            ->from(VtVideo::tableName() . ' vv')
            ->addSelect(["DATE_FORMAT(vv.created_at, '%Y-%m-%d') AS date"])
            ->where(['vv.type' => 'VOD', 'vv.status' => 1, 'vv.created_by' => $this->user_id])
            ->andWhere(['BETWEEN', 'vv.created_at', $beginDate, $endDate])
            ->groupBy('date');
        //var_dump($query2->createCommand()->getRawSql());die();


        $query3 = self::find()->asArray()
            ->select('count(*) as count')
            ->addSelect(["DATE_FORMAT(vv.updated_at, '%Y-%m-%d') AS date"])
            ->from(VtVideo::tableName() . ' vv')
            ->where(['vv.type' => 'VOD', 'vv.status' => 3, 'vv.created_by' => $this->user_id])
            ->andWhere(['BETWEEN', 'vv.updated_at', $beginDate, $endDate])
            ->groupBy('date');


        $query4 = self::find()->asArray()
            ->select('follow_count as count')
            ->from(VtUser::tableName())
            ->where(['id' => $this->user_id]);

        $query5 = self::find()->asArray()
            ->select('count(id) as count')
            ->addSelect(["DATE_FORMAT(created_at, '%Y-%m-%d') AS date"])
            ->from(VtUserPlaylist::tableName())
            ->where(['user_id' => $this->user_id])
            ->andWhere(['BETWEEN', 'created_at', $beginDate, $endDate])
            ->groupBy('date');


        $query6 = self::find()->asArray()
            ->select('count(*) as count')
            ->addSelect(["DATE_FORMAT(created_at, '%Y-%m-%d') AS date"])
            ->from(VtFeedBack::tableName())
            ->where(['user_id' => $this->user_id])
            ->andWhere(['BETWEEN', 'created_at', $beginDate, $endDate])
            ->groupBy('date');
        $query7 = self::find()->asArray()
            ->select('count(*) as count')
            ->addSelect(["DATE_FORMAT(vv.updated_at, '%Y-%m-%d') AS date"])
            ->from(VtVideo::tableName() . ' vv')
            ->where(['vv.type' => 'VOD', 'vv.status' => [1, 2, 3], 'vv.created_by' => $this->user_id])
            ->andWhere(['BETWEEN', 'vv.updated_at', $beginDate, $endDate])
            ->groupBy('date');

        $query8 = self::find()->asArray()
            ->select('sum(view_count) as count')
            ->addSelect(["DATE_FORMAT(created_at, '%Y-%m-%d') AS date"])
            ->from(VtUser::tableName())
            ->where(['id'=> $this->user_id])
            ->andWhere(['BETWEEN', 'created_at', $beginDate, $endDate])
            ->groupBy('date');


        if (isset($this->full_name) && $this->full_name != 0) {
            $query1->andWhere(['vv.channel_id' => $this->full_name]);
            $query2->andWhere(['vv.channel_id' => $this->full_name]);
            $query3->andWhere(['vv.channel_id' => $this->full_name]);
            $query7->andWhere(['vv.channel_id' => $this->full_name]);
            $query8->andWhere(['channel_id' =>$this->full_name ]);
            $query4 = self::find()->asArray()
                ->select('follow_count as count')
                ->from(VtChannel::tableName())
                ->where(['user_id' => $this->user_id, 'id' => $this->full_name])
                ->andWhere(['BETWEEN', 'updated_at', $beginDate, $endDate]);

        }

        $videoChanelApprove = $query1->all();

        $videoChanelDraft = $query2->all();

        $videoDisapprove = $query3->all();

        $totalFollowUser = $query4->all();

        $totalPlaylist = $query5->all();

        $totalViolateVideo = $query6->all();
        $totalVideo = $query7->all();
        $totalView = $query8->all();
        // var_dump($videoChanelApprove);die();

        $result = $this->processData($videoChanelApprove, $videoChanelDraft, $videoDisapprove,
            $totalFollowUser, $totalPlaylist, $totalViolateVideo, $totalVideo,$totalView);


        $dataProvider->allModels = $result;

        return $dataProvider;
    }

    private function processData($videoChanelApprove,$videoChanelDraft,$videoDisapprove,
                                 $totalFollowUser,$totalPlaylist,$totalViolateVideo,$totalVideo,$totalView)
    {
        $dates = [];
        // var_dump($data1);die();

        foreach ($videoChanelApprove as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }
        foreach ($videoChanelDraft as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }
        foreach ($videoDisapprove as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }
        foreach ($totalPlaylist as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }
        foreach ($totalViolateVideo as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }
        foreach ($totalVideo as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }
        foreach ($totalView as $i){
            if (!in_array($i['date'], $dates)) {
                $dates[] = $i['date'];
            }
        }

        $totalVideoUploadRow['title'] = 'Tổng Video upload';
        $videoApprovedRow['title'] = 'Tổng Video upload đã được duyệt';
        $videoDraftRow['title'] = 'Tổng Video upload đang chờ duyệt';
        $videoDisapprovedRow['title'] = 'Tổng Video upload đã bị từ chối duyệt';
        $totalFollowRow['title'] = 'Tổng lượt follow';
        $totalPlaylistRow['title'] = 'Tổng Playlist (Chỉ có của User, không có của kênh';
        $totalVideoViolateRow['title'] = 'Tổng video bị cảnh báo vi phạm (Chỉ có của User, không có của kênh)';
        $totalViewRow['title'] = 'Tổng lượt view';
        // var_dump($dates);die();

        foreach ($dates as $date) {
            $totalVideoUploadRow[$date] = 0;
            $videoApprovedRow[$date] = 0;
            $videoDraftRow[$date] = 0;
            $videoDisapprovedRow[$date] = 0;
            $totalFollowRow[$date] = 0;
            $totalPlaylistRow[$date] = 0;
            $totalVideoViolateRow[$date] = 0;
            $totalViewRow[$date] = 0;
            foreach ($videoChanelApprove as $i) {
                if ($i['date'] == $date) {
                    $videoApprovedRow[$date] = $i['count'];

                }
            }
            foreach ($videoChanelDraft as $i) {
                if ($i['date'] == $date) {
                    $videoDraftRow[$date] = $i['count'];

                }
            }
            foreach ($videoDisapprove as $i) {
                if ($i['date'] == $date) {
                    $videoDisapprovedRow[$date] = $i['count'];

                }
            }
            foreach ($totalPlaylist as $i) {
                if ($i['date'] == $date) {
                    $totalPlaylistRow[$date] = $i['count'];
                }
            }
            foreach ($totalViolateVideo as $i) {
                if ($i['date'] == $date) {
                    $totalVideoViolateRow[$date] = $i['count'];
                }
            }

            foreach ($totalFollowUser as $i) {
                $totalFollowRow[$date] = $i['count'];
            }
            foreach ($totalVideo as $i) {
                if ($i['date'] == $date) {
                    $totalVideoUploadRow[$date] = $i['count'];
                }
            }
            foreach ($totalView as $i) {
                if ($i['date'] == $date) {
                    $totalViewRow[$date] = $i['count'];
                }
            }


        }
        $rows[] = $totalVideoUploadRow;
        $rows[] = $videoApprovedRow;
        $rows[] = $videoDraftRow;
        $rows[] = $videoDisapprovedRow;
        $rows[] = $totalFollowRow;
        $rows[] = $totalPlaylistRow;
        $rows[] = $totalVideoViolateRow;
        $rows[] = $totalViewRow;


        return $rows;


    }

    public function reportReasonChannelDisapproved($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (count($params) < 1) {
            return $dataProvider;
        }

        $split = explode(' - ', $this->created_at);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);
        $query = '';
        $command = '';
        if ($this->status == 2) {
            $command = 'Decline';
            $query = self::find()->asArray()
                ->select('count(*) as count, reason')
                ->addSelect(["DATE_FORMAT(updated_at, '%Y-%m-%d') AS date"])
                ->from(VtUserChangeInfo::tableName())
                ->where(['status' => 2, 'user_id' => $this->id])
                ->andWhere(['BETWEEN', 'updated_at', $beginDate, $endDate])
                ->groupBy('reason')
                ->addGroupBy('date');
        } else {
            $command = 'Draft';
            $query = self::find()->asArray()
                ->select('full_name,id')
                ->addSelect(["DATE_FORMAT(created_at, '%Y-%m-%d') AS date"])
                ->from(VtUserChangeInfo::tableName())
                ->where(['status' => 0, 'user_id' => $this->id])
                ->andWhere(['BETWEEN', 'created_at', $beginDate, $endDate]);

        }

        // var_dump($query->createCommand()->getRawSql());die();
        $data = $query->all();
        $result = $this->processDataResonChannelDisapproved($data, $command);
        $dataProvider->allModels = $result;
        return $dataProvider;
    }

    private function processDataResonChannelDisapproved($data, $command)
    {
        $result = [];
        if ($command == 'Decline') {
            $dates = [];
            foreach ($data as $item) {
                if (!in_array($item['date'], $dates)) {
                    $dates[] = $item['date'];
                }
            }
            foreach ($data as $item) {
                $reasonRow = [
                    'titleDecline' => $item['reason'],
                ];
                foreach ($dates as $date) {
                    if ($date == $item['date']) {
                        $dateTitleRow[$date] = $item['reason'];
                        $reasonRow[$date] = $item['count'];
                    } else {
                        $reasonRow[$date] = '';
                    }
                }
                $result[] = $reasonRow;
            }
        } else {

            foreach ($data as $item) {
                $channel = $item['full_name'];

                $item = [
                    'titleDraft' => $channel,
                    'countDraft' => '<a href="/vt-user-change-info/update?id=' . $item['id'] . '">' . 'Chi tiết' . '</a>',
                ];
                $result[] = $item;
            }
        }
        return $result;

    }
}
