<?php

namespace backend\models;

use Yii;

class TargetKpi extends \common\models\TargetKpiBase {
    public static function getTargetKpi($endDate_year, $endDate_month){
        $targetKpi = self::find()->where(['and', ['report_year' => $endDate_year], ['report_month' => $endDate_month]])->all();
        return $targetKpi;
    }
}