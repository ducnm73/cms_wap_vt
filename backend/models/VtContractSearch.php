<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtContract;

/**
 * VtContractSearch represents the model behind the search form about `backend\models\VtContract`.
 */
class VtContractSearch extends VtContract
{
    /**
     * @inheritdoc
     */ 
    public function rules()
    {
        return [
            [['created_by', 'user_id', 'status', 'updated_by'], 'integer'],
            [['id','contract_code','name','id_card_number','msisdn','email'],'trim'],
            [['contract_code', 'name', 'email', 'id_card_number', 'id_card_created_at', 'id_card_created_by', 'bucket', 'id_card_image_frontside', 'id_card_image_backside', 'address', 'msisdn', 'payment_type', 'tax_code', 'created_at', 'account_number', 'bank_name', 'bank_department', 'msisdn_pay', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtContract::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'contract_code', $this->contract_code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'id_card_number', $this->id_card_number])
            ->andFilterWhere(['like', 'id_card_created_at', $this->id_card_created_at])
            ->andFilterWhere(['like', 'id_card_created_by', $this->id_card_created_by])
            ->andFilterWhere(['like', 'bucket', $this->bucket])
            ->andFilterWhere(['like', 'id_card_image_frontside', $this->id_card_image_frontside])
            ->andFilterWhere(['like', 'id_card_image_backside', $this->id_card_image_backside])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'msisdn', $this->msisdn])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'tax_code', $this->tax_code])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_department', $this->bank_department])
            ->andFilterWhere(['like', 'msisdn_pay', $this->msisdn_pay]);

        return $dataProvider;
    }

    public function searchFull($params)
    {
        $query = VtContract::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 999999,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'contract_code', $this->contract_code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'id_card_number', $this->id_card_number])
            ->andFilterWhere(['like', 'id_card_created_at', $this->id_card_created_at])
            ->andFilterWhere(['like', 'id_card_created_by', $this->id_card_created_by])
            ->andFilterWhere(['like', 'bucket', $this->bucket])
            ->andFilterWhere(['like', 'id_card_image_frontside', $this->id_card_image_frontside])
            ->andFilterWhere(['like', 'id_card_image_backside', $this->id_card_image_backside])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'msisdn', $this->msisdn])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'tax_code', $this->tax_code])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_department', $this->bank_department])
            ->andFilterWhere(['like', 'msisdn_pay', $this->msisdn_pay]);

        return $dataProvider;
    }

}






