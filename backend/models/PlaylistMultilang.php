<?php

namespace backend\models;

class PlaylistMultilang extends BaseMultilang {
	public $name;
	public $description;

	public function rules() {
		return [
            [['name', 'lang'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
		];
	}

	public function attributeLabels() {
		$playlist = new VtPlaylist();
		return array_merge($playlist->attributeLabels(), parent::attributeLabels());
	}
}