<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\UserViews;
use yii\data\ArrayDataProvider;

/**
 * UserViewsSearch represents the model behind the search form about `backend\models\UserViews`.
 */
class UserViewsSearch extends UserViews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'date'], 'safe'],
            [['view_count'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserViews::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'view_count' => $this->view_count,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'msisdn', $this->msisdn]);

        return $dataProvider;
    }

/*
 * created_at: 15/7/2021
 * created_by: @linth
 * */
    public function reportPayment($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (!$this->validate()) {

            return $dataProvider;
        }
        if (count($params) < 1) {
            return $dataProvider;
        }
        $split = explode(' - ', $this->date);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);
        $queryThanhtoan = '';
        $queryThanhtoan = self::find()->asArray()
            ->select('msisdn, sum(view_count) as sum')
            ->from(UserViews::tableName())
            ->where(['BETWEEN', 'date', $beginDate, $endDate])
            ->groupBy('msisdn')
            ->all();
        $tongViewUser = self::find()->asArray()
            ->select('sum(view_count) as count')
            ->where(['BETWEEN', 'date', $beginDate, $endDate])
            ->all();
        $total_money_share = LogTransaction::find()->asArray()
            ->select('sum(fee) as fee')
            ->where(['BETWEEN', 'datetime', $beginDate, $endDate])
            ->andWhere(['error_code' => 0, 'action' => 'REG'])
            ->all();
        $result = $this->processDataPayment($queryThanhtoan, $tongViewUser, $total_money_share);
        $dataProvider->allModels = $result;

        return $dataProvider;

    }
    private function processDataPayment($data1, $data2, $data3)
    {
        $money_user = 0;
        if($data3 == null){
            $data3[0]['fee'] = 0;
        }
        $result = [];
        $totalViewUser = $data2[0]['count'];
        foreach ($data1 as $item) {
            $money_user = $data3[0]['fee'] * round((($item['sum']) / $totalViewUser), 3);
            $item = [
                'msisdn' => $item['msisdn'],
                'view_count' => $item['sum'],
                'percent' => round((($item['sum']) / $totalViewUser), 3) . '%',
                'share_money_user_upload' => $data3[0]['fee'],
                'money_userID_view' => $money_user
            ];
            $result[] = $item;
        }
        return $result;
    }

}
