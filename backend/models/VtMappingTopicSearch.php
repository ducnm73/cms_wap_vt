<?php

namespace backend\models;

use common\libs\Crawler;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtVideo;
use yii\helpers\ArrayHelper;

/**
 *
 * VtVideoSearch represents the model behind the search form about `backend\models\VtVideo`.
 */


class VtMappingTopicSearch extends VtMappingTopic
{
    public $topic_name;
    public $category_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'type', 'local_topic_id', 'local_category_id'], 'safe'],
            [['name'],'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->select('v.*, t.name as topic_name, c.name as category_name')
            ->from(self::tableName(). ' v')
            ->leftJoin(VtGroupTopic::tableName() . ' t', 'v.local_topic_id = t.id')
            ->leftJoin(VtGroupCategory::tableName() . ' c', 'v.local_category_id = c.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'v.type' => $this->type,
            'v.local_topic_id' => $this->local_topic_id,
            'v.local_category_id' => $this->local_category_id
        ]);
        $query->andFilterWhere(['like', 'v.name', trim($this->name)]);

        return $dataProvider;
    }
}
