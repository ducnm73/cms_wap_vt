<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TargetKpi;

/**
 * TargetKpiSearch represents the model behind the search form about `backend\models\TargetKpi`.
 */
class TargetKpiSearch extends TargetKpi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'report_year', 'report_month', 'display_time_app', 'display_time_web', 'encode_video_time', 'wait_time'], 'integer'],
            [['success_access_rate_app', 'success_access_rate_web', 'avg_upload_speed', 'error_upload_rate', 'error_encode_rate', 'buffer_rate', 'buffer_duration_rate', 'buffer_over_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TargetKpi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'report_year' => $this->report_year,
            'report_month' => $this->report_month,
            'display_time_app' => $this->display_time_app,
            'display_time_web' => $this->display_time_web,
            'success_access_rate_app' => $this->success_access_rate_app,
            'success_access_rate_web' => $this->success_access_rate_web,
            'avg_upload_speed' => $this->avg_upload_speed,
            'error_upload_rate' => $this->error_upload_rate,
            'encode_video_time' => $this->encode_video_time,
            'error_encode_rate' => $this->error_encode_rate,
            'wait_time' => $this->wait_time,
            'buffer_rate' => $this->buffer_rate,
            'buffer_duration_rate' => $this->buffer_duration_rate,
            'buffer_over_rate' => $this->buffer_over_rate,
        ]);

        return $dataProvider;
    }
}
