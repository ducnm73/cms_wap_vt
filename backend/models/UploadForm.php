<?php
namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $csvFile;

    public function rules()
    {
        $fileSize = 10;//MB
        $realSize =  1024*1024*$fileSize;

        return [
            ['csvFile', 'file', 'skipOnEmpty' => false, 'extensions'=>'csv', 'maxSize'=> $realSize ,'checkExtensionByMimeType' => false, 'tooBig'=> \Yii::t('backend', "Dung lượng file tối đa cho phép") . " " . $fileSize .  " MB"],
        ];
    }

    public function upload()
    {
        $pathFile = 'uploads/' . date("YmdHis")."_".uniqid(). '.' . $this->csvFile->extension;
        if ($this->validate()) {
            $this->csvFile->saveAs($pathFile);
            return $pathFile;
        } else {
            return false;
        }
    }
}