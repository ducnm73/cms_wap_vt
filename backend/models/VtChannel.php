<?php

namespace backend\models;

use common\models\VtChannelBase;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use Yii;
use yii\web\UploadedFile;

class VtChannel extends \common\models\VtChannelBase {
	    
     public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-channel')) {
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'status' => Yii::t('backend', 'Trạng thái'),
            'full_name' => Yii::t('backend', 'Tên kênh'),
            'bucket' => Yii::t('backend', 'Bucket'),
            'path' => Yii::t('backend', 'Path'),
            'channel_bucket' => Yii::t('backend', 'Channel Bucket'),
            'channel_path' => Yii::t('backend', 'Channel Path'),
            'description' => Yii::t('backend', 'Mô tả'),
            'reason' => Yii::t('backend', 'Lý do'),
            'created_at' => Yii::t('backend', 'Tạo lúc'),
            'updated_at' => Yii::t('backend', 'Cập nhật lúc'),
        ];
    }
    public static function getChannel($id){
        $user = self::find()->where(['id' => $id])->one();
        return $user;
    }

    public function saveWithImage($objChannel, $model) {
        $fileBucket = "";
        $storageType = Yii::$app->params['storage.type'];
        $bannerFile = UploadedFile::getInstance($this, 'channel_path');
        $avatarFile = UploadedFile::getInstance($this, 'path');

        if (!empty($avatarFile) && $avatarFile->size > 0) {
            $ext = pathinfo($avatarFile->name, PATHINFO_EXTENSION);
            
            if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                Yii::$app->session->setFlash('error', Yii::t('web','Định dạng file upload không đúng'));

                return false;
            }

            if($storageType == 0){
                $san = Yii::$app->params['local.image'];
                $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                $fileName = Utils::generateUniqueFileName($ext);
                $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;

                if (!mkdir($childFolder, 0777, true)) {
                    return ('Failed to create folders...');
                }

                $fileAvatarPath = 'uploads'. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                $fileAvatarLocalPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR.$folder. DIRECTORY_SEPARATOR.$fileName ;

                move_uploaded_file($avatarFile->tempName, $orgpath);

                VtHelper::generateAllThumb($fileBucket, $fileAvatarLocalPath, $orgpath,[],true);
            } else {
                $fileBucket = Yii::$app->params['s3']['static.bucket'];
                $fileAvatarPath = Utils::generatePath($ext);
                $fileName = pathinfo($avatarFile->name, PATHINFO_FILENAME);

                S3Service::putObject($fileBucket, $avatarFile->tempName, $fileAvatarPath);
                VtHelper::generateAllThumb($fileBucket, $avatarFile->tempName, $fileAvatarPath);
            }

            $objChannel['bucket'] = $fileBucket;
            $objChannel['path'] = $fileAvatarPath;
            $model['bucket'] = $fileBucket;
            $model['path'] = $fileAvatarPath;
        }

        list($width, $height) = getimagesize($avatarFile->tempName);

        if ($width > "180" || $height > "180") {
            Yii::$app->session->setFlash('error', Yii::t('wap','Kích thước ảnh avatar không hợp lệ'));

            return false;
        }
        
        if (!empty($bannerFile) && $bannerFile->size > 0) {
            $ext = pathinfo($bannerFile->name, PATHINFO_EXTENSION);
            
            if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                Yii::$app->session->setFlash('error', Yii::t('web','Định dạng file upload không đúng'));

                return false;
            }
            if ($storageType == 0) {
                $san = Yii::$app->params['local.image'];
                $folder = $san. DIRECTORY_SEPARATOR .Utils::generateFolder();
                $fileName = Utils::generateUniqueFileName($ext);
                $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder ;

                if (!mkdir($childFolder, 0777, true)) {
                    return ('Failed to create folders...');
                }

                $fileBannerPath = 'uploads'. DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                $fileBannerLocalPath = Yii::getAlias('@uploadFolder'). DIRECTORY_SEPARATOR .$folder. DIRECTORY_SEPARATOR .$fileName;
                $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR.$folder. DIRECTORY_SEPARATOR.$fileName ;

                move_uploaded_file($bannerFile->tempName, $orgpath);

                VtHelper::generateAllThumb($fileBucket, $fileBannerLocalPath, $orgpath,[],true);

            } else {
                $fileBucket = Yii::$app->params['s3']['static.bucket'];
                $fileBannerPath = Utils::generatePath($ext);
                $fileName = pathinfo($bannerFile->name, PATHINFO_FILENAME);

                S3Service::putObject($fileBucket, $bannerFile->tempName, $fileBannerPath);
                VtHelper::generateAllThumb($fileBucket, $bannerFile->tempName, $fileBannerPath);
            }

            $objChannel['channel_bucket'] = $fileBucket;
            $objChannel['channel_path'] = $fileBannerPath;
            $model['channel_bucket'] = $fileBucket;
            $model['channel_path'] = $fileBannerPath;
        }

        list($width, $height) = getimagesize($bannerFile->tempName);

        if($width > "1920" || $height > "330") {
            Yii::$app->session->setFlash('error', Yii::t('wap','Kích thước ảnh banner không hợp lệ'));

            return false;
        }
    }
}