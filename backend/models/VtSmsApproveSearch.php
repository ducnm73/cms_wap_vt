<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtSmsApprove;

/**
 * VtSmsApproveSearch represents the model behind the search form about `backend\models\VtSmsApprove`.
 */
class VtSmsApproveSearch extends VtSmsApprove
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'updated_by', 'status'], 'integer'],
            [['sms_code', 'sms_content', 'sms_content_draft', 'note', 'created_at', 'updated_at'], 'safe'],
            [['id'],'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtSmsApprove::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'sms_code', $this->sms_code])
            ->andFilterWhere(['like', 'sms_content', $this->sms_content])
            ->andFilterWhere(['like', 'sms_content_draft', $this->sms_content_draft])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
