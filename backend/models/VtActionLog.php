<?php

namespace backend\models;

use common\models\VtUserBase;
use common\models\VtVideoBase;

class VtActionLog extends \common\models\VtActionLogBase {

    public function getVideo(){
        return $this->hasOne(VtVideoBase::className(), ['id' => 'content_id']);
    }

    public function getUser(){
        return $this->hasOne(AuthUser::className(), ['id' => 'user_id']);
    }
}