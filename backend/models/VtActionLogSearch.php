<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VtActionLogSearch represents the model behind the search form about `backend\models\VtActionLog`.
 */
class VtActionLogSearch extends VtActionLog
{
//    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['module', 'type', 'action', 'source', 'created_at', 'detail', 'ip', 'url', 'user_id', 'content_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtActionLog::find()->joinWith(['user', 'video']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->created_at)) {
//            if(!empty($params["VtAccountInfomationSearch"]["created_at"])){
//                if(strpos( $params["VtAccountInfomationSearch"]["created_at"] ," - " ) !== false){
                    $split = explode(' - ', $this->created_at);

                    $beginDate = trim($split[0]);
                    $endDate = trim($split[1]);

//                    if(checkDateFormat($beginDate) && checkDateFormat($endDate)){
                        $query->andWhere('vt_action_log.created_at between :beginTime and :endTime', [
                            ':beginTime' => $beginDate,
                            ':endTime' => $endDate
                        ]);
//                    }
//                }
//            }
        }

        $query->andFilterWhere([
            'val.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'vt_action_log.module', trim($this->module)])
            ->andFilterWhere(['like', 'vt_action_log.type', trim($this->type)])
            ->andFilterWhere(['like', 'vt_action_log.action', $this->action])
            ->andFilterWhere(['like', 'vt_action_log.source', trim($this->source)])
            ->andFilterWhere(['like', 'vt_action_log.detail', trim($this->detail)])
            ->andFilterWhere(['like', 'vt_action_log.ip', trim($this->ip)])
            ->andFilterWhere(['like', 'vt_action_log.url', trim($this->url)])
            ->andFilterWhere(['like', 'vt_video.name', trim($this->content_id)])
            ->andFilterWhere(['like', 'auth_user.username', trim($this->user_id)]);

        return $dataProvider;
    }

    static function checkDateFormat($dataNeedCheck){
        $dateFormat = '(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})';
        if(preg_match($dateFormat, $dataNeedCheck, $match)){
            return true;
        } else {
            return false;
        }
    }
}
