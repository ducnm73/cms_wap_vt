<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtShortLink;

/**
 * VtShortLinkSearch represents the model behind the search form about `backend\models\VtShortLink`.
 */
class VtShortLinkSearch extends VtShortLink
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active','id'], 'integer'],
//            [['id'], 'integer', 'min' => 0],
            [['short_link', 'full_link', 'other_info'], 'safe'],
            [['id'], 'safe'],
            [['other_info','short_link' ,'full_link','id'],'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtShortLink::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'short_link', $this->short_link])
            ->andFilterWhere(['like', 'full_link', $this->full_link])
            ->andFilterWhere(['like', 'other_info', $this->other_info]);

        return $dataProvider;
    }
}
