<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtPlaylist;
use yii\data\ArrayDataProvider;

/**
 * VtPlaylistSearch represents the model behind the search form about `backend\models\VtPlaylist`.
 */
class VtUserPlaylistSearch extends VtUserPlaylist {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['is_active', 'status', 'user_id', 'msisdn', 'num_video', 'created_by', 'play_times'], 'integer'],
                [['created_at', 'updated_at'], 'safe'],
                [['name', 'bucket', 'path'], 'string', 'max' => 255],
                [['description', 'source_display'], 'string'],
                [['name', 'num_video'], 'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = VtUserPlaylist::find()
                ->from(self::tableName() . ' c')
                ->joinWith('user')
                ->joinWith('auth')
        ;
 


        if (isset($params[$this->formName()])) {
            if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
                if(strpos($params[$this->formName()]['created_at'], ' - ')) {
                    $split = explode(' - ', $params[$this->formName()]['created_at']);
                    $beginDate = trim($split[0]);
                    $endDate = trim($split[1]);
                    $query->andWhere('c.created_at between :beginTime and :endTime', [
                        ':beginTime' => $beginDate,
                        ':endTime' => $endDate
                    ]);
                }
                else {
                    $query->where('0=1');
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->name) {
            $query->andFilterWhere(['like', 'name', $this->name]);
        }

        if ($this->created_at) {
            if(strpos($this->created_at, ' - ')) {
                $split = explode(' - ', $this->created_at);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);
                $query->andWhere('c.created_at between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }
        }

        return $dataProvider;
    }

    public function exportExcel($params) {
        $query = VtUserPlaylist::find()
                ->asArray()
                ->from(self::tableName() . ' c')
                ->joinWith('user')
                ->joinWith('auth');

        if (isset($params[$this->formName()])) {

            if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
                $split = explode(' - ', $params[$this->formName()]['created_at']);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('c.created_at between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }
        }

        $dataProvider = array();
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->num_video) {
            $query->andWhere(['num_video' => $this->num_video]);
        }

        if ($this->play_times) {
            $query->andWhere(['play_times' => $this->play_times]);
        }

        if ($this->user_id) {
            $query->andWhere(['c.user_id' => $this->user_id]);
        }

        if ($this->created_by) {
            $query->andWhere(['created_by' => $this->created_by]);
        }

        if ($this->source_display) {
            $query->andWhere(['source_display' => $this->source_display]);
        }

        if ($this->is_active) {
            $query->andWhere(['is_active' => $this->is_active]);
        }

        if ($this->status) {
            $query->andWhere(['c.status' => $this->status]);
        }

        if ($this->name) {
            $query->andWhere(['like', 'name', $this->name]);
        }

        $dataProvider = $query->all();
        return $dataProvider;
    }


    /*
        * @linth
        * created_at: 10/6/2021
   * Báo cáo playlist tổng hợp
        * */
    public function reportPlaylist($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (!$this->validate()) {

            return $dataProvider;
        }
        if (count($params) < 1) {
            return $dataProvider;
        }
        $split = explode(' - ', $this->created_at);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);

        $query = self::find()->asArray()
            ->select('count(v1.id) as count, v2.full_name')
            ->addSelect(["DATE_FORMAT(v1.created_at, '%Y-%m-%d') AS date"])
            ->from(VtUserPlaylist::tableName() . ' v1')
            ->leftJoin(VtUser::tableName() . ' v2', 'v1.created_by = v2.id')
            ->where(['BETWEEN', 'v1.created_at', $beginDate, $endDate])
            ->groupBy('date');

        if ($this->created_by != null) {
            $query->andWhere(['v1.created_by' => $this->created_by]);
        }
        $playlistCount = $query->all();

        $result = $this->processDataPlaylistReport($playlistCount);
        $dataProvider->allModels = $result;

        return $dataProvider;


    }

    private function processDataPlaylistReport($data)
    {
        $dates = [];
        $result = [];
        foreach ($data as $item) {
            if (!in_array($item['date'], $dates)) {
                $dates[] = $item['date'];
            }
        }
        foreach ($data as $item) {
            $reasonRow = [
                'title' => $item['full_name'],
            ];
            foreach ($dates as $date) {
                if ($date == $item['date']) {
                    $dateTitleRow[$date] = $item['full_name'];
                    $reasonRow[$date] = $item['count'];
                } else {
                    $reasonRow[$date] = '';
                }
            }
            $result[] = $reasonRow;
        }
        return $result;
    }
}
