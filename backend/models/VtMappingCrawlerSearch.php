<?php

namespace backend\models;

use common\libs\Crawler;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 *
 * VtVideoSearch represents the model behind the search form about `backend\models\VtVideo`.
 */


class VtMappingCrawlerSearch extends VtMappingCrawler
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'local_user_id', 'cp_id', 'crawler_id', 'local_category_id'], 'safe'],
            [['crawler_id','name', 'local_user_id','cp_id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->from(self::tableName(). ' v')
            ->joinWith('user')
            ->joinWith('category')
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'v.id' => $this->id,
            'v.cp_id' => $this->cp_id,
            'v.crawler_id' => $this->crawler_id,
            'v.local_category_id' => $this->local_category_id
        ]);

        $query->andFilterWhere(['like', 'v.name', $this->name])
            ->andFilterWhere(['like', 'vt_user.full_name', $this->local_user_id]);


//        echo $query->createCommand()->getRawSql();die;
        return $dataProvider;
    }
}
