<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class SubForm extends Model {

    public $msisdn;
    public $package_id;
    public $reason;
    public $is_free;
    public $is_sms;

    public function rules() {
        return [
            [['package_id', 'is_free', 'is_sms'], 'integer'],
            [['msisdn', 'package_id', 'reason', 'is_free', 'is_sms'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'msisdn' => Yii::t('app', 'Số điện thoại'),
            'package_id' => Yii::t('app', 'Gói cước'),
            'reason' => Yii::t('app', 'Lý do'),
            'is_free' => Yii::t('app', 'Loại'),
            'is_sms' => Yii::t('app', 'Gửi tin tới Khách hàng'),
        ];
    }

}