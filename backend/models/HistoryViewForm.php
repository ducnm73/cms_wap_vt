<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class HistoryViewForm extends Model {

    public $insert_time;
    public $ip;
    public $source;

    public function rules() {
        return [
            [['insert_time', 'ip', 'source'], 'trim'],
            [['insert_time', 'ip', 'source'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'insert_time' => Yii::t('app', 'Thời gian'),
            'ip' => Yii::t('app', 'IP'),
            'source' => Yii::t('app', 'Nguồn'),

        ];
    }

}