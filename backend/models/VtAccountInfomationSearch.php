<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtAccountInfomation;

/**
 * VtAccountInfomationSearch represents the model behind the search form about `backend\models\VtAccountInfomation`.
 */
class VtAccountInfomationSearch extends VtAccountInfomation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by', 'is_confirm','user_id'], 'integer'],
            [['user_id','name','id_card_number','email','msisdn','channelName','created_at'],'trim'],
            [['name', 'email', 'id_card_number', 'id_card_created_at', 'id_card_created_by', 'created_at', 'updated_at', 'id_card_image_frontside', 'id_card_image_backside', 'bucket', 'msisdn', 'reason'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtAccountInfomation::find()->joinWith('user');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'vt_account_infomation.id' => $this->id,
            'vt_account_infomation.status' => $this->status,
//            'created_at' => $this->created_at,
            'vt_account_infomation.created_by' => $this->created_by,
            'vt_account_infomation.user_id' => $this->user_id,
            'vt_account_infomation.updated_at' => $this->updated_at,
            'vt_account_infomation.updated_by' => $this->updated_by,
            'vt_account_infomation.is_confirm' => $this->is_confirm,
        ]);

        $query->andFilterWhere(['like', 'vt_account_infomation.name', trim($this->name)])
            ->andFilterWhere(['like', 'vt_account_infomation.email', trim($this->email)])
            ->andFilterWhere(['like', 'vt_account_infomation.id_card_number', trim($this->id_card_number)])
            ->andFilterWhere(['like', 'vt_account_infomation.id_card_created_at', trim($this->id_card_created_at)])
            ->andFilterWhere(['like', 'vt_account_infomation.id_card_created_by', trim($this->id_card_created_by)])
            ->andFilterWhere(['like', 'vt_account_infomation.id_card_image_frontside', trim($this->id_card_image_frontside)])
            ->andFilterWhere(['like', 'vt_account_infomation.id_card_image_backside', trim($this->id_card_image_backside)])
            ->andFilterWhere(['like', 'vt_account_infomation.bucket', trim($this->bucket)])
            ->andFilterWhere(['like', 'vt_account_infomation.msisdn', trim($this->msisdn)])
            ->andFilterWhere(['like', 'vt_account_infomation.reason', trim($this->reason)])
        ;

        if(!empty($params["VtAccountInfomationSearch"]["channelName"])){
            $this->channelName = $params["VtAccountInfomationSearch"]["channelName"];
            $query->andFilterWhere(['like', 'vt_user.full_name', trim($this->channelName)]);
        }

        if(!empty($params["VtAccountInfomationSearch"]["created_at"])){
            if( strpos( $params["VtAccountInfomationSearch"]["created_at"] ," - " )!==false ){
                $arrDay = preg_split( "/ - /", $params["VtAccountInfomationSearch"]["created_at"] );
                $query->andFilterWhere(['>=', 'date(vt_account_infomation.created_at)', $arrDay[0]]);
                $query->andFilterWhere(['<=', 'date(vt_account_infomation.created_at)', $arrDay[1]]);
            }else{
                $query->andFilterWhere(['=', 'date(vt_account_infomation.created_at)',  $this->created_at]);
            }
        }

        return $dataProvider;
    }
}










