<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 *
 * VtMappingUserSearch represents the model behind the search form about `backend\models\VtVideo`.
 */


class VtMappingUserSearch extends VtMappingUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'email', 'cp_id', 'local_user_id', 'local_category_id', 'local_channel_id'], 'safe'],
            [['id', 'username', 'email', 'cp_id', 'local_user_id', 'local_category_id', 'local_channel_id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->from(self::tableName(). ' v')
            ->joinWith('user')
            ->joinWith('vtChannel')
            ->joinWith('category');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'like','v.id', $this->id])
            ->andFilterWhere(['like','v.cp_id',$this->cp_id])
            ->andFilterWhere(['like','vt_channel.full_name', $this->local_channel_id])
            ->andFilterWhere(['like','v.local_category_id', $this->local_category_id]);

        $query->andFilterWhere(['like', 'v.username', $this->username])
//            ->andFilterWhere(['like', 'v.id', $this->id])
            ->andFilterWhere(['like', 'v.email', $this->email])
            ->andFilterWhere(['like', 'vt_user.full_name', $this->local_user_id])
        ;
        return $dataProvider;
    }
}




