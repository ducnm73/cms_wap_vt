<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtVideoHot;

/**
 * VtVideoHotSearch represents the model behind the search form about `backend\models\VtVideoHot`.
 */
class VtVideoHotSearch extends VtVideoHot
{
    public $video_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'video_id', 'is_active'], 'integer'],
            [['created_at', 'updated_at', 'video_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtVideoHot::find()
            ->from(self::tableName() . ' vh')
            ->joinWith('video')
            ->joinWith('category')
            ->orderBy('vh.id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'video_id' => $this->video_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'vt_video.name', trim($this->video_name)]);

        return $dataProvider;
    }
}
