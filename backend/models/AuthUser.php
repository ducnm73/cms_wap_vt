<?php

namespace backend\models;

use Yii;

class AuthUser extends \common\models\db\AuthUserDB {

    public static function getAllUser() {
        $query = self::find()
                ->asArray();
        return $query->all();
    }

}
