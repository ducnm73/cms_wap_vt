<?php

namespace backend\models;

use Yii;

class VtShortLink extends \common\models\VtShortLinkBase {
    public function rules() {
        return [
            [['short_link', 'route'], 'required','message' => Yii::t('backend','{attribute} không được để trống')],
            [['is_active'], 'integer'],
            [['short_link', 'route', 'other_info'], 'string', 'max' => 255],
            [['params', 'full_link'], 'string', 'max' => 1000],
            [['short_link'], 'unique']
        ];
    }

}