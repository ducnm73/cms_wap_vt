<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtPlaylist;

/**
 * VtPlaylistSearch represents the model behind the search form about `backend\models\VtPlaylist`.
 */
class VtUserPlaylistSearch extends VtUserPlaylist {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['is_active', 'status', 'user_id', 'msisdn', 'num_video', 'created_by', 'play_times'], 'integer'],
                [['created_at', 'updated_at'], 'safe'],
                [['name', 'bucket', 'path'], 'string', 'max' => 255],
//                [['name', 'bucket', 'path'], 'string', 'max' => 255],
                [['description', 'source_display'], 'string'],
                [['name', 'num_video'], 'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = VtUserPlaylist::find()
                ->from(self::tableName() . ' c')
                ->joinWith('user')
                ->joinWith('auth')
        ;
 


        if (isset($params[$this->formName()])) {
            if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
                if(strpos($params[$this->formName()]['created_at'], ' - ')) {
                    $split = explode(' - ', $params[$this->formName()]['created_at']);
                    $beginDate = trim($split[0]);
                    $endDate = trim($split[1]);
                    $query->andWhere('c.created_at between :beginTime and :endTime', [
                        ':beginTime' => $beginDate,
                        ':endTime' => $endDate
                    ]);
                }
                else {
                    $query->where('0=1');
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        if (isset($params[$this->formName()]['name']) && !empty($params[$this->formName()]['name'])) {
            $query->andFilterWhere(['name' => $params[$this->formName()]['name']]);
        }
        if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
            $query->andFilterWhere(['created_at' => $params[$this->formName()]['created_at']]);
        }
        if (isset($params[$this->formName()]['source_display']) && !empty($params[$this->formName()]['source_display'])) {
            $query->andFilterWhere(['source_display' => $params[$this->formName()]['source_display']]);
        }
        if (isset($params[$this->formName()]['play_times']) && !empty($params[$this->formName()]['play_times'])) {
            $query->andFilterWhere(['play_times' => $params[$this->formName()]['play_times']]);
        }
        if (isset($params[$this->formName()]['is_active'])) {
            $query->andFilterWhere(['is_active' => $params[$this->formName()]['is_active']]);
        }
        if (isset($params[$this->formName()]['status']) && !empty($params[$this->formName()]['status'])) {
            $query->andFilterWhere(['c.status' => $params[$this->formName()]['status']]);
        } else {
            $query->andFilterWhere(['between', 'c.status', 1, 2]);
        }
     
//        echo $query->createCommand()->getRawSql();die;
        return $dataProvider;
    }

    public function exportExcel($params) {
        $query = VtUserPlaylist::find()
                ->asArray()
                ->from(self::tableName() . ' c')
//                ->where(['=','c.status', 3])
                ->joinWith('user')
                ->joinWith('auth')
        ;


        if (isset($params[$this->formName()])) {

            if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
                $split = explode(' - ', $params[$this->formName()]['created_at']);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('c.created_at between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }
        }

        $dataProvider = array();

//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
//        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        /*if (isset($params[$this->formName()]['num_video']) && !empty($params[$this->formName()]['num_video'])) {
            $query->andWhere(['num_video' => $params[$this->formName()]['num_video']]);
        }
        if (isset($params[$this->formName()]['play_times']) && !empty($params[$this->formName()]['play_times'])) {
            $query->andWhere(['play_times' => $params[$this->formName()]['play_times']]);
        }
        if (isset($params[$this->formName()]['user_id']) && !empty($params[$this->formName()]['user_id'])) {
            $query->andWhere(['c.user_id' => $params[$this->formName()]['user_id']]);
        }
        if (isset($params[$this->formName()]['created_by']) && !empty($params[$this->formName()]['created_by'])) {
            $query->andWhere(['created_by' => $params[$this->formName()]['created_by']]);
        }
        if (isset($params[$this->formName()]['created_by']) && !empty($params[$this->formName()]['created_by'])) {
            $query->andWhere(['created_by' => $params[$this->formName()]['created_by']]);
        }
        if (isset($params[$this->formName()]['source_display']) && !empty($params[$this->formName()]['source_display'])) {
            $query->andWhere(['source_display' => $params[$this->formName()]['source_display']]);
        }
        if ((isset($params[$this->formName()]['is_active']) && !empty($params[$this->formName()]['is_active']))) {
            $query->andWhere(['is_active' => $params[$this->formName()]['is_active']]);
        }
        if (isset($params[$this->formName()]['status']) && !empty($params[$this->formName()]['status'])) {
            $query->andWhere(['c.status' => $params[$this->formName()]['status']]);
        }
        if (isset($params[$this->formName()]['name']) && !empty($params[$this->formName()]['name'])) {
            $query->andWhere(['like', 'name', $params[$this->formName()]['name']]);
        }*/

        /*if ($this->num_video) {
            $query->andWhere(['num_video' => $this->num_video]);
        }

        if ($this->play_times) {
            $query->andWhere(['play_times' => $this->play_times]);
        }

        if ($this->user_id) {
            $query->andWhere(['c.user_id' => $this->user_id]);
        }

        if ($this->created_by) {
            $query->andWhere(['created_by' => $this->created_by]);
        }

        if ($this->source_display) {
            $query->andWhere(['source_display' => $this->source_display]);
        }

        if ($this->is_active) {
            $query->andWhere(['is_active' => $this->is_active]);
        }

        if ($this->status) {
            $query->andWhere(['c.status' => $this->status]);
        }

        if ($this->name) {
            $query->andWhere(['like', 'name', $this->name]);
        }*/

        $query->andWhere([
            'num_video' => $this->num_video,
            'play_times' => $this->play_times,
            'c.user_id' => $this->user_id,
            'created_by' => $this->created_by,
            'source_display' => $this->source_display,
            'is_active' => $this->is_active,
            'c.status' => $this->status,
        ]);
        $query->andWhere(['like', 'name', $this->name]);
        $dataProvider = $query->all();

        return $dataProvider;
    }



}
