<?php

namespace backend\models;

use Yii;

class VtSmsMtHis extends \common\models\VtSmsMtHisBase {

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msisdn' => Yii::t('app', 'Số điện thoại'),
            'message' => Yii::t('app', 'Nội dung'),
            'mo_his_id' => Yii::t('app', 'Mo His ID'),
            'sent_time' => Yii::t('app', 'Thời gian'),
            'status' => Yii::t('app', 'Status'),
            'receive_time' => Yii::t('app', 'Thời gian gọi thành công'),
            'channel' => Yii::t('app', 'Đầu số'),
            'process_id' => Yii::t('app', 'Process ID'),
        ];
    }


}