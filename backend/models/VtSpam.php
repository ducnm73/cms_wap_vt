<?php

namespace backend\models;

use Yii;

class VtSpam extends \common\models\VtSpamBase {


    public function rules()
    {
        return [
            [['name', 'content', 'send_time', 'channel', 'rule'], 'required'],
            [['is_active', 'is_sent', 'number', 'current_line', 'created_by', 'updated_by', 'item_group_id', 'status', 'channel'], 'integer'],
            [['send_time', 'created_at', 'updated_at'], 'safe'],
            [['name', 'file_path', 'item_type', 'bucket', 'path'], 'string', 'max' => 255],
            [['content'], 'string', 'max' => 500],
            [['rule'], 'string', 'max' => 1000],
            [['item_id'], 'string', 'max' => 2000]
        ];
    }

}