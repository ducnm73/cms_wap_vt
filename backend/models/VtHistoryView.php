<?php

namespace backend\models;

use common\models\VtUserBase;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VtHistoryViewBase;



class VtHistoryView extends VtHistoryViewBase
{
//'_id', 'id', 'user_id', 'msisdn', 'item_id', 'time', 'content_type', 'duration', 'content', 'content_detail', 'request_time'

    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Thuê bao'),
            'msisdn' => Yii::t('app', 'Số điện thọai'),
            'item_id' => Yii::t('app', 'ID nội dung'),
            'time' => Yii::t('app', 'Thời gian xem (giây)'),
            'content_type' => Yii::t('app', 'Loại nội dung'),
            'content' => Yii::t('app', 'Nội dung xem'),
            'request_time' => Yii::t('app', 'Thời điểm xem'),
        ];
    }

    public function getUserName(){
        $objUser = VtUserBase::getById($this->user_id);
        return ($objUser)?$objUser->full_name:$this->user_id;
    }


}
