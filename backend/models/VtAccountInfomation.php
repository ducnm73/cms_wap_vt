<?php

namespace backend\models;
use common\models\VtUserBase;

use Yii;

class VtAccountInfomation extends \common\models\VtAccountInfomationBase {

    public $channelName;
    public $updaterName;

    const WAIT_APPROVE = 1;
    const ACTIVE = 2;
    const DISAPPROVE = 3;
    const BANNED = 4;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
        ];
    }

    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-account-infomation')) {
            return true;
        } else {
            return false;
        }
    }

    public static function getAccInfo($fromTime, $toTime){
        $query = self::find()
            ->where(['>=', 'created_at', $fromTime])
            ->andWhere(['<=', 'created_at', $toTime]);
        return $query;
    }

    public static function getChannelName($channelId){
        $query = self::find()->select('vu.full_name channelName, vac.email, vac.msisdn')->from(['vac' => 'vt_account_infomation'])
            ->leftJoin(['vu' => 'vt_user'], 'vac.user_id = vu.id')->where(['vac.id' => $channelId])->one();
        return $query;
    }

    public static function getUpdaterName($channelId){
        $query = self::find()->select('au.username updaterName')->from(['vac' => 'vt_account_infomation'])
            ->leftJoin(['au' => 'auth_user'], 'vac.updated_by = au.id')->where(['vac.id' => $channelId])->one();
        return $query;
    }

    public function getUser(){
        return $this->hasOne(VtUserBase::className(), ['id' => 'user_id']);
    }
}