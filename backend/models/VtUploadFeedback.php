<?php

namespace backend\models;

use Yii;

class VtUploadFeedback extends \common\models\VtUploadFeedbackBase {
    public $videoId;
    public $videoName;
    public $videoDescription;
    public $uploaderName;
    public $uploaderPhone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'status', 'created_by', 'updated_by'], 'integer'],
            [['updated_at'], 'safe'],
            [['reject_reason'], 'string'],
            [['video_id', 'created_by', 'created_at', 'bucket', 'path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend','ID'),
            'video_id' => Yii::t('backend','Video ID'),
            'status' => Yii::t('backend','Status'),
            'created_at' => Yii::t('backend','Created At'),
            'updated_at' => Yii::t('backend','Updated At'),
            'created_by' => Yii::t('backend','Created By'),
            'updated_by' => Yii::t('backend','Updated By'),
            'reject_reason' => Yii::t('backend','Reject Reason'),
            'bucket' => Yii::t('backend','Bucket'),
            'path' => Yii::t('backend','Path')
        ];
    }
    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-upload-feedback')) {
            return true;
        } else {
            return false;
        }
    }

    public function getVtVideo(){
        return $this->hasOne(VtVideo::className(), ['id' => 'video_id']);
    }

    public function getVtUser(){
        return $this->hasOne(VtUser::className(), ['id' => 'created_by']);
    }

    public static function isExist($video_id){
        $isExist = self::find()
            ->where(['video_id' => $video_id])->exists();
        return $isExist ? true : false;
    }

    public static function getUploadFeedback($videoName, $uploaderName, $status){
        $query = self::find()
            ->select('vv.id videoId, vv.name videoName, vv.description videoDescription, vuf.id, vuf.created_at, vuf.updated_at, vuf.status, 
                                vuf.feedback_content, vu.msisdn uploaderPhone, vu.full_name as uploaderName, vuf.bucket, vuf.path')
            ->from(['vuf' => 'vt_upload_feedback'])
            ->innerJoin(['vv' => 'vt_video'], 'vuf.video_id = vv.id')
            ->innerJoin(['vu' => 'vt_user'], 'vuf.created_by = vu.id');
        if($videoName != ''){
            $query = $query->andWhere(['like', 'lower(vv.name)', strtolower('%'. trim($videoName) . '%'), false]);
        }
        if($uploaderName != ''){
            $query = $query->andWhere(['like', 'lower(vu.full_name)', strtolower('%'. trim($uploaderName) . '%'), false]);
        }
        if($status != ''){
            $query = $query->andWhere(['vuf.status' => $status]);
        }
        return $query;
    }
}