<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\helpers\Utils;

class VtUserPlaylist extends \common\models\VtUserPlaylistBase {

    const CREATE = 1;
    const APPROVE = 2;
    const DRAFF = 3;
    const ACTIVE = 1;
    const DEACTIVE = 0;

    public function rules() {
        return [
                [['user_id','source_display'], 'required', 'message' => Yii::t('backend','{attribute} không được để trống')],
                [['description'], 'string'],
                [['is_active', 'status', 'user_id', 'msisdn', 'num_video', 'created_by', 'updated_by', 'play_times'], 'integer'],
                [['created_at', 'updated_at', 'path'], 'safe'],
                [['type'], 'string', 'max' => 25],
                [['name', 'bucket', 'path', 'source_display'], 'string', 'max' => 255],
                [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],
        ];
    }

    public function getUser() {
        return $this->hasOne(\common\models\VtUserBase::className(), ['id' => 'user_id']);
    }

    public function getAuth() {
        return $this->hasOne(AuthUser::className(), ['id' => 'created_by']);
    }

    public function saveWithImage() {
        $file = UploadedFile::getInstance($this, 'path');

        if (!empty($file)) {
            $bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);
            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->user_id;
            }
            $this->updated_by = Yii::$app->user->identity->user_id;

            $this->bucket = $bucket;
            $this->path = $path;
        } else {
            unset($this->bucket);
            unset($this->path);
        }

        $this->save();
    }

    public static function updateFinish($id, $updatedBy) {

        $rawQuery = "UPDATE vt_user_playlist SET is_finish = 1,updated_by= :updated_by, updated_at= now()  WHERE id = :id";


        return Yii::$app->db->createCommand($rawQuery)
                        ->bindValue(':id', $id)
                        ->bindValue(':updated_by', $updatedBy)
                        ->execute();
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('backend','ID'),
            'type' => Yii::t('backend','Type'),
            'name' => Yii::t('backend','Tên playlist'),
            'description' => Yii::t('backend','Mô tả'),
            'is_active' => Yii::t('backend','Trạng thái'),
            'status' => Yii::t('backend','Status'),
            'user_id' => Yii::t('backend','Kênh sở hữu'),
            'msisdn' => Yii::t('backend','Msisdn'),
            'created_at' => Yii::t('backend','Created At'),
            'updated_at' => Yii::t('backend','Updated At'),
            'bucket' => Yii::t('backend','Bucket'),
            'path' => Yii::t('backend','Path'),
            'num_video' => Yii::t('backend','Số lượng content'),
            'created_by' => Yii::t('backend','Created By'),
            'updated_by' => Yii::t('backend','Updated By'),
            'play_times' => Yii::t('backend','Play Times'),
            'source_display' => Yii::t('backend','Source Display'),
        ];
    }

}
