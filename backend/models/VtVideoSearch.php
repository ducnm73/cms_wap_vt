<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtVideo;
use yii\data\ArrayDataProvider;

/**
 * VtVideoSearch represents the model behind the search form about `backend\models\VtVideo`.
 */
class VtVideoSearch extends VtVideo {

    public $state;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'category_id', 'channel_id', 'attributes', 'is_active', 'price_download', 'price_play', 'play_times', 'status', 'approve_by', 'cp_id', 'created_by', 'updated_by', 'is_no_copyright', 'like_count', 'is_hot', 'is_recommend', 'convert_status', 'duration', 'convert_priority', 'review_by', 'syn_id', 'outsource_status', 'outsource_need_review', 'outsource_review_by'], 'integer'],
                [['name', 'description', 'slug', 'published_time', 'reason', 'type', 'created_at', 'updated_at', 'tag', 'related_id', 'seo_title', 'seo_description', 'seo_keywords', 'bucket', 'path', 'convert_start_time', 'convert_end_time', 'file_path', 'file_bucket', 'resolution', 'syn_id', 'state', 'outsource_review_at'], 'safe'],
                [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = VtVideo::TYPE_VOD) {
//        $state = array_key_exists('state', $params[$this->formName()]) ? $params[$this->formName()]['state'] : '';

        $query = VtVideo::find()
                ->select('v.*')
                ->from(self::tableName() . ' v')
                ->joinWith('user')
                ->joinWith('category')
                ->where(['v.type' => $type])
                ->andWhere('v.status <> 444');//Them trang thai thanh tra

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => Yii::$app->request->get('per-page', 20),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!Yii::$app->user->can('approve-video')) {
            $query->andWhere(['v.created_by' => Yii::$app->user->identity->user_id]);
        }

        switch ($this->state) {
            case 'temp':
                $query->andWhere(['v.status' => VtVideo::STATUS_TEMP]);
                break;
            case 'published':
                $query->andWhere(['v.status' => VtVideo::STATUS_APPROVE]);
                break;
            case 'draft':
                $query->andWhere(
                    [
                        'v.status' => VtVideo::STATUS_DRAFT,
                        'v.convert_status' => VtVideo::CONVERT_STATUS_SUCCESS,
                    ]
                );
                break;
            case 'converting':
                $query->andWhere(
                    [
                        'v.status' => VtVideo::STATUS_DRAFT,
                        'v.convert_status' => [VtVideo::CONVERT_STATUS_CONVERTING, VtVideo::CONVERT_STATUS_DRAFT, VtVideo::STATUS_IS_MERGING]
                    ]
                );
                break;
            case 'error':
                $query->andWhere(
                    [
                        'v.convert_status' => [
                            VtVideo::CONVERT_STATUS_FAIL_CLOUD,
                            VtVideo::CONVERT_STATUS_FAIL_ORG,
                            VtVideo::CONVERT_STATUS_FAIL_CONVERT
                        ]
                    ]
                );
                break;
            case 'deleted':
                $query->andWhere(
                    ['v.status' => VtVideo::STATUS_DELETE]
                );
                break;
            case 'customer-deleted':
                $query->andWhere(
                    ['v.status' => VtVideo::STATUS_REMOVE]
                );
                break;
        }

        if (isset($params[$this->formName()]['updated_by']) && !empty($params[$this->formName()]['updated_by'])) {
            if ($params[$this->formName()]['updated_by'] == 'N/A') {
                $query->andWhere(['v.updated_by' => null]);
            } else {
                $query->andWhere(['v.updated_by' => $params[$this->formName()]['updated_by']]);
            }
        }



        if (isset($params[$this->formName()])) {
            if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
                $split = explode(' - ', $params[$this->formName()]['created_at']);

                if (count($split) < 2) {
                    $split = explode(' / ', $params[$this->formName()]['created_at']);
                }

                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('v.created_at between :createdAtBeginTime and :createdAtEndTime', [
                    ':createdAtBeginTime' => $beginDate,
                    ':createdAtEndTime' => $endDate
                ]);
            }

            if (isset($params[$this->formName()]['published_time']) && !empty($params[$this->formName()]['published_time'])) {
                $split = explode(' - ', $params[$this->formName()]['published_time']);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('v.published_time between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }

            if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
                $split = explode(' - ', $params[$this->formName()]['updated_at']);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('v.updated_at between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }


            if (isset($params[$this->formName()]['attributes'])) {

                $attr = intval($params[$this->formName()]['attributes']);

                if ($attr > 1) {
                    $query->andWhere(['v.attributes' => $params[$this->formName()]['attributes']]);
                } elseif ($attr == 1) {
                    $query->andWhere('v.attributes <= 1 or v.attributes is null');
                }
            }

            if (isset($params[$this->formName()]['syn_id']) && $params[$this->formName()]['syn_id'] !== '') {
                $sourceUpload = $params[$this->formName()]['syn_id'];

                if (intval($sourceUpload) === 0) {
                    $query->andWhere(['v.syn_id' => null]);
                } elseif (intval($sourceUpload) === 1) {
                    $query->andWhere(['not', ['v.syn_id' => null]]);
                }
            }
        }

        if(isset($this->outsource_status) &&$this->outsource_status !== ''){
            $query->andFilterWhere([
                'v.outsource_status' => $this->outsource_status,
                'v.outsource_need_review' => 1
            ]);
        }




        $query->andFilterWhere([
            'v.id' => $this->id,
            'v.category_id' => $this->category_id,
//            'v.attributes' => $this->attributes,
            'v.is_active' => $this->is_active,
            'v.price_download' => $this->price_download,
            'v.price_play' => $this->price_play,
            'v.play_times' => $this->play_times,
            'v.status' => $this->status,
            'v.approve_by' => $this->approve_by,
            'v.cp_id' => $this->cp_id,
            'v.created_by' => $this->created_by,
            'v.is_no_copyright' => $this->is_no_copyright,
            'v.like_count' => $this->like_count,
            'v.is_hot' => $this->is_hot,
            'v.is_recommend' => $this->is_recommend,
            'v.convert_status' => $this->convert_status,
            'v.convert_start_time' => $this->convert_start_time,
            'v.convert_end_time' => $this->convert_end_time,
            'v.duration' => $this->duration,
            'v.convert_priority' => $this->convert_priority,
            'v.review_by' => $this->review_by,
            'v.outsource_review_at' => $this->outsource_review_at,
            'v.outsource_review_by' => $this->outsource_review_by,
        ]);

        $query->andFilterWhere(['like', 'v.name', trim($this->name)])
                ->andFilterWhere(['like', 'v.description', trim($this->description)])
                ->andFilterWhere(['like', 'v.slug', $this->slug])
                ->andFilterWhere(['like', 'v.reason', $this->reason])
                ->andFilterWhere(['like', 'v.type', $this->type])
                ->andFilterWhere(['like', 'v.tag', $this->tag])
                ->andFilterWhere(['like', 'v.related_id', $this->related_id])
                ->andFilterWhere(['like', 'v.seo_title', $this->seo_title])
                ->andFilterWhere(['like', 'v.seo_description', $this->seo_description])
                ->andFilterWhere(['like', 'v.seo_keywords', $this->seo_keywords])
                ->andFilterWhere(['like', 'v.bucket', $this->bucket])
                ->andFilterWhere(['like', 'v.path', $this->path])
                ->andFilterWhere(['like', 'v.file_path', $this->file_path])
                ->andFilterWhere(['like', 'v.file_bucket', $this->file_bucket])
                ->andFilterWhere(['like', 'v.resolution', $this->resolution])
        ;
//        echo $query->createCommand()->getRawSql();
//        die();
        return $dataProvider;
    }

    public function searchPopup($params, $ids = array(), $type = VtVideo::TYPE_VOD, $channelId = '') {

        $query = VtVideo::find()
                ->select('v.*,u.full_name')
                ->from(self::tableName() . ' v')
                ->leftJoin(VtUser::tableName() . ' u', 'u.id= v.created_by')
                ->where(['v.type' => $type,
                    'v.status' => 2,
                    'v.is_active' => 1
//                    'v.status' => [1, 2]
                ])
                ->groupBy(['v.id'])
        ;
//        echo $query->createCommand()->getRawSql();die;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            return $dataProvider;
        }
        if (empty($params[$this->formName()]['name']) && !empty($channelId)) {
            $query->andFilterWhere(['created_by' => $channelId]);
        }
        if (isset($params[$this->formName()]['name']) && !empty($params[$this->formName()]['name'])) {
            $query->andFilterWhere(['like', 'v.name', $params[$this->formName()]['name']]);
        }
        if (isset($params[$this->formName()]['created_by']) && !empty($params[$this->formName()]['created_by'])) {
            $query->andFilterWhere(['created_by' => $params[$this->formName()]['created_by']]);
        }
        if (count($ids) > 0) {

            $query->andFilterWhere(['not in', 'v.id', $ids]);
        }

        return $dataProvider;
    }


    /*
  * @linth
  * created_at: 10/6/2021
  * */

    public function reportLike($params)
    {

        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (!$this->validate()) {

            return $dataProvider;
        }
        if (count($params) < 1) {
            return $dataProvider;
        }
        $queryLike = '';
        $queryLike = self::find()->asArray()
            ->select('id, name, like_count, dislike_count')
            ->from(VtVideo::tableName())
            ->where(['category_id' => $this->category_id, 'is_active' => 1])
            ->groupBy('id');

        if($this->created_by != ''){
            $queryLike->andWhere(['created_by' => $this->created_by]);
        }

        $data = $queryLike->all();
        $result = $this->processDataReportLike($data);
        $dataProvider->allModels = $result;
        return $dataProvider;
    }

    private function processDataReportLike($data){
        $nameVideo = '';
        $likeCount = 0;
        $disLikeCount = 0;
        $result = [];
        foreach ($data as $item){
            $nameVideo = ($item['name'] != null) ?$item['name'] : '' ;
            $likeCount = ($item['like_count'] != null) ?$item['like_count'] : 0   ;
            $disLikeCount =($item['dislike_count'] != null) ? $item['dislike_count'] : 0 ;

            $item = [
                'title' =>$nameVideo ,
                'like' => $likeCount,
                'dis_like' => $disLikeCount,
            ];
            $result[] = $item;
        }

        return $result;
    }

    /**
     * Manage video fees
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchVideoFee($params, $type = VtVideo::TYPE_VOD) {
        $query = VtVideo::find()
                ->select('v.*')
                ->from(self::tableName() . ' v')
                ->joinWith('user')
                ->joinWith('category')
                ->where(['v.type' => $type])
                ->andWhere('v.status <> 444');//Them trang thai thanh tra

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => Yii::$app->request->get('per-page', 20),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!Yii::$app->user->can('approve-video')) {
            $query->andWhere(['v.created_by' => Yii::$app->user->identity->user_id]);
        }

        $query->andWhere(['v.status' => VtVideo::STATUS_APPROVE]);

        if (isset($params[$this->formName()]['updated_by']) && !empty($params[$this->formName()]['updated_by'])) {
            if ($params[$this->formName()]['updated_by'] == 'N/A') {
                $query->andWhere(['v.updated_by' => null]);
            } else {
                $query->andWhere(['v.updated_by' => $params[$this->formName()]['updated_by']]);
            }
        }

        if (isset($params[$this->formName()])) {
            if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
                $split = explode(' - ', $params[$this->formName()]['created_at']);

                if (count($split) < 2) {
                    $split = explode(' / ', $params[$this->formName()]['created_at']);
                }

                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('v.created_at between :createdAtBeginTime and :createdAtEndTime', [
                    ':createdAtBeginTime' => $beginDate,
                    ':createdAtEndTime' => $endDate
                ]);
            }

            if (isset($params[$this->formName()]['published_time']) && !empty($params[$this->formName()]['published_time'])) {
                $split = explode(' - ', $params[$this->formName()]['published_time']);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('v.published_time between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }

            if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
                $split = explode(' - ', $params[$this->formName()]['updated_at']);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('v.updated_at between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
            }


            if (isset($params[$this->formName()]['attributes'])) {

                $attr = intval($params[$this->formName()]['attributes']);

                if ($attr > 1) {
                    $query->andWhere(['v.attributes' => $params[$this->formName()]['attributes']]);
                } elseif ($attr == 1) {
                    $query->andWhere('v.attributes <= 1 or v.attributes is null');
                }
            }

            if (isset($params[$this->formName()]['syn_id']) && $params[$this->formName()]['syn_id'] !== '') {
                $sourceUpload = $params[$this->formName()]['syn_id'];

                if (intval($sourceUpload) === 0) {
                    $query->andWhere(['v.syn_id' => null]);
                } elseif (intval($sourceUpload) === 1) {
                    $query->andWhere(['not', ['v.syn_id' => null]]);
                }
            }
        }

        if(isset($this->outsource_status) &&$this->outsource_status !== ''){
            $query->andFilterWhere([
                'v.outsource_status' => $this->outsource_status,
                'v.outsource_need_review' => 1
            ]);
        }

        $query->andFilterWhere([
            'v.id' => $this->id,
            'v.category_id' => $this->category_id,
            'v.is_active' => $this->is_active,
            'v.price_download' => $this->price_download,
            'v.play_times' => $this->play_times,
            'v.status' => $this->status,
            'v.approve_by' => $this->approve_by,
            'v.cp_id' => $this->cp_id,
            'v.created_by' => $this->created_by,
            'v.channel_id' => $this->channel_id,
            'v.is_no_copyright' => $this->is_no_copyright,
            'v.like_count' => $this->like_count,
            'v.is_hot' => $this->is_hot,
            'v.is_recommend' => $this->is_recommend,
            'v.convert_status' => $this->convert_status,
            'v.convert_start_time' => $this->convert_start_time,
            'v.convert_end_time' => $this->convert_end_time,
            'v.duration' => $this->duration,
            'v.convert_priority' => $this->convert_priority,
            'v.review_by' => $this->review_by,
            'v.outsource_review_at' => $this->outsource_review_at,
            'v.outsource_review_by' => $this->outsource_review_by,
            'v.price_play' => $this->price_play,
        ]);

        $query->andFilterWhere(['like', 'v.name', trim($this->name)])
                ->andFilterWhere(['like', 'v.description', trim($this->description)])
                ->andFilterWhere(['like', 'v.slug', $this->slug])
                ->andFilterWhere(['like', 'v.reason', $this->reason])
                ->andFilterWhere(['like', 'v.type', $this->type])
                ->andFilterWhere(['like', 'v.tag', $this->tag])
                ->andFilterWhere(['like', 'v.related_id', $this->related_id])
                ->andFilterWhere(['like', 'v.seo_title', $this->seo_title])
                ->andFilterWhere(['like', 'v.seo_description', $this->seo_description])
                ->andFilterWhere(['like', 'v.seo_keywords', $this->seo_keywords])
                ->andFilterWhere(['like', 'v.bucket', $this->bucket])
                ->andFilterWhere(['like', 'v.path', $this->path])
                ->andFilterWhere(['like', 'v.file_path', $this->file_path])
                ->andFilterWhere(['like', 'v.file_bucket', $this->file_bucket])
                ->andFilterWhere(['like', 'v.resolution', $this->resolution]);

        return $dataProvider;
    }
}
