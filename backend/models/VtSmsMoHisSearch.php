<?php

namespace backend\models;

use common\helpers\Utils;
use common\libs\VtHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtSmsMoHis;


/**
 * LogTransactionSearch represents the model behind the search form about `backend\models\VtVideo`.
 */
class VtSmsMoHisSearch extends VtSmsMoHis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'receive_time'], 'required'],
            [['command'], 'string'],
            [['receive_time'], 'safe'],
            [['msisdn'], 'string', 'max' => 15],
            [['channel'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $msisdn = null)
    {
        $query = VtSmsMoHis::find();

        $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);

        if(isset($msisdn) && !empty($msisdn)){
            $query->andWhere( 'msisdn = :msisdn or msisdn = :msisdn3x',  [':msisdn' => $msisdn, ':msisdn3x'=>VtHelper::convert8416XTo843X($msisdn)]);
        }

        if (isset($params['VtSmsMoHisSearch']['receive_time']) && !empty($params['VtSmsMoHisSearch']['receive_time'])) {
            $split = explode(' - ', $params['VtSmsMoHisSearch']['receive_time']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere('receive_time between :beginTime and :endTime', [
                ':beginTime' => $beginDate . ' 00:00:00',
                ':endTime' => $endDate . ' 23:59:59'
            ]);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id' => $this->id,
//            'category_id' => $this->category_id,
//        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'resolution', $this->resolution]);

        return $dataProvider;
    }
}
