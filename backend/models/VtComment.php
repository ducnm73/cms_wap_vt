<?php

namespace backend\models;
use common\models\VtCommentBase;

use Yii;

class VtComment extends \common\models\VtCommentBase
{
    public $date;
    public $numOfRecord;
    public $totalComment;
    public $waitingComment;
    public $approvedComment;
    public $declinedComment;
    public $below5MinApproval;


    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-comment')) {
            return true;
        } else {
            return false;
        }
    }

    public function getCreatedByName()
    {
        $objUsername = VtUser::getById($this->user_id);
        return ($objUsername) ? (($objUsername->full_name)?$objUsername->full_name:$objUsername->msisdn) : $this->user_id;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content_id' => Yii::t('app', 'ID Nội dung'),
            'type' => Yii::t('app', 'Loại'),
            'user_id' => Yii::t('app', 'ID Khách hàng'),
            'content' => Yii::t('app', 'Nội dung'),
            'parent_id' => Yii::t('app', 'Bình luận cha'),
            'status' => Yii::t('app', 'Trạng thái'),
            'bad_word_filter' => Yii::t('app', 'Bad Word Filter'),
            'approve_by' => Yii::t('app', 'Phê duyệt bởi'),
            'reason' => Yii::t('app', 'Lý do'),
            'like_count' => Yii::t('app', 'Like Count'),
            'created_at' => Yii::t('app', 'Tạo lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
            'comment' => Yii::t('app', 'Bình luận'),
            'comment_count' => Yii::t('app', 'Số lượng bình luận'),
        ];
    }

    /** list total of record depending on each reason type
     * @param $fromTime, $toTime, $reasonId
     * @return $reasonSummary
     * @author thuynd10
     */
    public static function summaryReason($fromTime, $toTime, $reasonId){
        $reasonSummary = self::find()
            ->select(['count(id) as numOfRecord', 'reason'])
            ->where(['and',
                ['>=', 'created_at', $fromTime],
                ['<=', 'created_at', $toTime],
                ['status' => 2]
            ]);
        if($reasonId != null){
            $reasonSummary->andWhere(['reason' => $reasonId]);
        }
        $reasonSummary = $reasonSummary->groupBy(['reason'])->all();
        return $reasonSummary;
    }

    /** count 'total of Comments', 'number of waiting comments', 'number of approved comments', 'number of declined comments'
     *  'number of comments which are reviewed at a duration of time below 5 minutes'
     * @param $fromTime, $toTime, $reasonId
     * @return $query
     * @author thuynd10
     */
    public static function getCommentData($fromTime, $toTime, $reasonId){
        $date_subquery = self::find()->select(['distinct(date(created_at)) as date'])
            ->where(['and', ['>=', 'created_at', $fromTime], ['<=', 'created_at', $toTime]]);
            if($reasonId != ''){
                $date_subquery = $date_subquery->andWhere(['reason' => $reasonId]);
            }
            $date_subquery = $date_subquery->orderBy(['date' => SORT_DESC]);
        $total_comment_subquery = self::find()->select('date(created_at) as date, count(id) as totalComment')
            ->where(['and', ['>=', 'created_at', $fromTime], ['<=', 'created_at', $toTime]])
            ->groupBy(['date'])->orderBy(['date' => SORT_DESC]);
        $waiting_comment_subquery = self::find()->select('date(created_at) as date, count(id) as waitingComment')
            ->where(['and', ['>=', 'created_at', $fromTime], ['<=', 'created_at', $toTime], ['status' => 0]])
            ->groupBy(['date'])->orderBy(['date' => SORT_DESC]);
        $approved_comment_subquery = self::find()->select('date(created_at) as date, count(id) as approvedComment')
            ->where(['and', ['>=', 'created_at', $fromTime], ['<=', 'created_at', $toTime], ['status' => 1]])
            ->groupBy(['date'])->orderBy(['date' => SORT_DESC]);
        $declined_comment_subquery = self::find()->select('date(created_at) as date, count(id) as declinedComment')
            ->where(['and', ['>=', 'created_at', $fromTime], ['<=', 'created_at', $toTime], ['status' => 2]]);
            if($reasonId != ''){
                $declined_comment_subquery = $declined_comment_subquery->andWhere(['reason' => $reasonId]);
            }
            $declined_comment_subquery = $declined_comment_subquery->groupBy(['date'])->orderBy(['date' => SORT_DESC]);
        $below5MinApproval_subquery = self::find()->select('date(created_at) as date, count(id) as below5MinApproval')
            ->where(['and',
                ['>=', 'created_at', $fromTime],
                ['<=', 'created_at', $toTime],
                ['!=', 'status', 0],
                ['<', 'timediff(updated_at, created_at)', '00:05:00']
            ])
            ->groupBy(['date'])->orderBy(['date' => SORT_DESC]);
        $query = self::find()
            ->select('a.date, b.totalComment, c.waitingComment, d.approvedComment, e.declinedComment, f.below5MinApproval')
            ->from(['a' => $date_subquery])
            ->leftJoin(['b' => $total_comment_subquery], 'a.date = b.date')
            ->leftJoin(['c' => $waiting_comment_subquery], 'a.date = c.date')
            ->leftJoin(['d' => $approved_comment_subquery], 'a.date = d.date')
            ->leftJoin(['e' => $declined_comment_subquery], 'a.date = e.date')
            ->leftJoin(['f' => $below5MinApproval_subquery], 'a.date = f.date')
        ;
        return $query;
    }


    public  function getVideoNameByContentID($contentID){
        $query = VtCommentBase::getById($this->content_id);
        return ($query) ? $query-> name : $this -> content_id;
    }
}