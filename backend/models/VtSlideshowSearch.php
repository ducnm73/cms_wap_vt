<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtSlideshow;

/**
 * VtSlideshowSearch represents the model behind the search form about `backend\models\VtSlideshow`.
 */
class VtSlideshowSearch extends VtSlideshow
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'item_id', 'position', 'is_active'], 'integer'],
            [['type', 'location', 'name', 'href', 'begin_time', 'end_time', 'bucket', 'path'], 'safe'],
            [['name'],'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtSlideshow::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['position' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'item_id' => $this->item_id,
            'position' => $this->position,
            'begin_time' => $this->begin_time,
            'end_time' => $this->end_time,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'href', $this->href])
            ->andFilterWhere(['like', 'bucket', $this->bucket])
            ->andFilterWhere(['like', 'path', $this->path]);

        return $dataProvider;
    }
}
