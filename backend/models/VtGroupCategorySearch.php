<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtGroupCategory;

/**
 * VtGroupCategorySearch represents the model behind the search form about `backend\models\VtGroupCategory`.
 */
class VtGroupCategorySearch extends VtGroupCategory {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'is_active', 'is_hot', 'positions', 'parent_id'], 'integer'],
                [['name', 'type', 'description', 'bucket', 'path'], 'safe'],
                [['name'],'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = VtGroupCategory::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_active' => $this->is_active,
            'is_hot' => $this->is_hot,
            'positions' => $this->positions,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'type', $this->type])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'bucket', $this->bucket])
                ->andFilterWhere(['like', 'path', $this->path]);
        if (isset($this->parent_id) && $this->parent_id == 0 && $this->parent_id != null) {
            $query->andFilterWhere(['>', 'parent_id', '0']);
        } else if (isset($this->parent_id) && $this->parent_id == 1) {
            $query->andWhere("parent_id is null");
        }
        return $dataProvider;
    }

}
