<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtPromotionData;

/**
 * VtPromotionDataSearch represents the model behind the search form about `backend\models\VtPromotionData`.
 */
class VtPromotionDataSearch extends VtPromotionData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['code', 'msisdn', 'created_at', 'expired_at', 'popup'], 'safe'],
            [['msisdn','code','popup'],'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtPromotionData::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id
        ]);
		
		if($this->created_at){
			$query->andFilterWhere([
				'DATE(created_at)' => date("Y-m-d", strtotime($this->created_at)),
			]);
		}
		if($this->expired_at){
			$query->andFilterWhere([
				'DATE(expired_at)' => date("Y-m-d", strtotime($this->expired_at)),
			]);
		}		

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'msisdn', $this->msisdn])
            ->andFilterWhere(['like', 'popup', $this->popup]);

        return $dataProvider;
    }
}







