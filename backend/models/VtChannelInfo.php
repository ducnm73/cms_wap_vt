<?php

namespace backend\models;

use common\models\VtChannelInfoBase;
use Yii;

class VtChannelInfo extends \common\models\VtChannelInfoBase {
    public function rules()
    {
        return [
            [['status'], 'string'],
        ];
    }

    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-channel-info')) {
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'channel_id' => Yii::t('backend', 'Channel ID'),
            'status' => Yii::t('backend', 'Trạng thái'),
            'full_name' => Yii::t('backend', 'Tên kênh'),
            'bucket' => Yii::t('backend', 'Bucket'),
            'path' => Yii::t('backend', 'Path'),
            'channel_bucket' => Yii::t('backend', 'Channel Bucket'),
            'channel_path' => Yii::t('backend', 'Channel Path'),
            'description' => Yii::t('backend', 'Mô tả'),
            'reason' => Yii::t('backend', 'Lý do'),
            'created_at' => Yii::t('backend', 'Tạo lúc'),
            'updated_at' => Yii::t('backend', 'Cập nhật lúc'),
        ];
    }

    public static function getChannel($id){
        $channel = self::find()->where(['id' => $id])->one();
        return $channel;
    }
}