<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use backend\models\LogTransaction;
use backend\models\LogTransactionFail;


/**
 * LogTransactionSearch represents the model behind the search form about `backend\models\VtVideo`.
 */
class LogTransactionSearch extends LogTransaction
{
    public $status;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'msisdn'], 'trim'],
            [['user_id', 'msisdn', 'fee', 'status'], 'integer'],
            [['datetime'], 'safe'],
            [['action', 'error_code', 'content', 'other_info'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $msisdn = null, $userId = null)
    {
        $query = LogTransaction::find();

        $query->andWhere('msisdn = :msisdn', [
            'msisdn' => $msisdn
        ]);


        if (isset($params['LogTransactionSearch']['datetime']) && !empty($params['LogTransactionSearch']['datetime'])) {
            $split = explode(' - ', $params['LogTransactionSearch']['datetime']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere('datetime between :beginTime and :endTime', [
                ':beginTime' => $beginDate . ' 00:00:00',
                ':endTime' => $endDate . ' 23:59:59'
            ]);
        }

        $queryFail = LogTransactionFail::find();

//        if (!empty($msisdn) || !empty($userId)) {
//            $queryFail->andWhere('msisdn = :msisdn or user_id = :user_id', [
//                'msisdn' => $msisdn,
//                'user_id' => $userId
//            ]);
//        } elseif (!empty($msisdn) || empty($userId)) {
//            $queryFail->andWhere('msisdn = :msisdn', [
//                'msisdn' => $msisdn
//            ]);
//        } elseif (empty($msisdn) || !empty($userId)) {
//            $queryFail->andWhere('user_id = :user_id', [
//                'user_id' => $userId
//            ]);
//        }


        $queryFail->andWhere('msisdn = :msisdn', [
            'msisdn' => $msisdn
        ]);

        if (isset($params['LogTransactionSearch']['datetime']) && !empty($params['LogTransactionSearch']['datetime'])) {
            $split = explode(' - ', $params['LogTransactionSearch']['datetime']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $queryFail->andWhere('datetime between :beginTime and :endTime', [
                ':beginTime' => $beginDate . ' 00:00:00',
                ':endTime' => $endDate . ' 23:59:59'
            ]);
        }

        $offset = (isset($params['page']))?20*($params['page']-1):0;
        $queryFail->limit(20);
        $queryFail->offset($offset);
        $queryTmp = $query->union($queryFail);


        $dataProvider = new ActiveDataProvider([
            'query' => $queryTmp,
            'sort' => ['defaultOrder' => ['datetime' => SORT_DESC]]
        ]);


        // var_dump($params);
        // die();

        if (!$this->validate()) {

            return $dataProvider;
        }


        return $dataProvider;
    }


    /*
 * @linth
 * created_at: 21/5/2021
 * */

    public function revenueReport($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (!$this->validate()) {

            return $dataProvider;
        }
        if (count($params) < 1) {
            return $dataProvider;
        }
        $split = explode(' - ', $this->datetime);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);

//        $split = explode('-', $endDate);
//        $day = 1;
//        $month = trim($split[1]);
//        $year = trim($split[0]);
//
//        $timeLuyke = $year.'-'.$month.'-'.$day;
        $queryDoanhthu = self::find()->asArray()
            ->select('sum(fee) as fee, action, content')
            ->addSelect(["DATE_FORMAT(datetime, '%Y-%m-%d') AS date"])
            ->where([
                'action' => ['CHARGE', 'REG'],
                'error_code' => 0,
            ])
            ->andWhere(['BETWEEN', 'datetime', $beginDate, $endDate])
            ->groupBy('action, date, content');
        $queryTongDoanhthu = self::find()->asArray()
            ->select('sum(fee) as fee, action, content')
            ->addSelect(["DATE_FORMAT(datetime, '%Y-%m-%d') AS date"])
            ->where([
                'action' => ['CHARGE', 'REG'],
                'error_code' => 0,
            ])
            ->andWhere(['BETWEEN', 'datetime', $beginDate, $endDate])
            ->groupBy('date, content')
            ->all();
        $doanhthu = $queryDoanhthu->all();
        $result = $this->processDataDoanhthu($doanhthu, $queryTongDoanhthu);
        $dataProvider->allModels = $result;
        return $dataProvider;

    }

    public static function getAllPackage()
    {
        return ['day' => 'day', 'week' => 'week', 'month' => 'month'];
    }

    private function processDataDoanhthu($data, $data2)
    {
        $dates = [];

        foreach ($data as $item) {
            if(!in_array($item['date'], $dates)) {
                $dates[] = $item['date'];
            }
        }

        $packages = [];

        foreach ($data as $item) {
            if(!in_array($item['content'], $packages)) {
                $packages[] = $item['content'];
            }
        }

//        echo '<pre>'; print_r($data2); die;
        $result = [];

        $doanhthuRegTitleRow = [
            'order' => 'I',
            'title' => Yii::t('backend', 'Doanh thu đăng ký mới'),
        ];
        $doanhthuChargeTitleRow = [
            'order' => 'II',
            'title' => Yii::t('backend', 'Doanh thu gia hạn')
        ];
        $doanhthuTongTitleRow = [
            'order' => 'III',
            'title' => Yii::t('backend', 'Doanh thu tổng')
        ];

        foreach ($packages as $package) {
            $doanhthuRegTitleRow[$package] = '';
            $doanhthuChargeTitleRow[$package] = '';
            $doanhthuTongTitleRow[$package] = 0;
        }

        $doanhthuRegRows = [];
        $doanhthuChargeRows = [];
        $doanhthuTotalRows = [];

        $i = 1;
        foreach ($dates as $date) {
            $doanhthuRegRow = [
                'order' => $i,
                'title' => $date,
            ];
            $doanhthuChargeRow = [
                'order' => $i,
                'title' => $date,
            ];
            foreach ($packages as $package) {
                $doanhthuRegRow[$package] = 0;
                $doanhthuChargeRow[$package] = 0;
            }

            foreach ($data as $item) {
                if($item['date'] == $date) {
                    foreach ($packages as $package) {
                        if ($package == $item['content']) {
                            if($item['action'] == 'REG') {
                                $doanhthuRegRow[$package] = $item['fee'];
                            } else if($item['action'] == 'CHARGE') {
                                $doanhthuChargeRow[$package] = $item['fee'];
                            }
                        }
                    }
                }
            }
            $doanhthuRegRows[] = $doanhthuRegRow;
            $doanhthuChargeRows[] = $doanhthuChargeRow;
            $i++;
        }

        $i = 1;
        foreach ($dates as $date) {
            $doanhthuTotalRow = [
                'order' => $i++,
                'title' => $date,
            ];

            foreach ($packages as $package) {
                $doanhthuTotalRow[$package] = 0;
            }

            foreach ($data2 as $item) {
                if($item['date'] == $date) {
                    foreach ($packages as $package) {
                        if ($package == $item['content']) {
                            $doanhthuTotalRow[$package] = $item['fee'];
                        }
                    }
                }
            }

            $doanhthuTotalRows[] = $doanhthuTotalRow;
        }

        $result[] = $doanhthuRegTitleRow;
        $result = array_merge($result, $doanhthuRegRows);
        $result[] = $doanhthuChargeTitleRow;
        $result = array_merge($result, $doanhthuChargeRows);
        $result[] = $doanhthuTongTitleRow;
        $result = array_merge($result, $doanhthuTotalRows);
//        echo '<pre>'; print_r($result); die;
        return $result;
    }

    public function complainReport($params)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (count($params) < 1) {
            return $dataProvider;
        }
        $queryKhieunai = '';
        $queryKhieunai = self::find()->asArray()
            ->select('msisdn, action, fee, content, datetime, other_info')
            ->from(self::tableName())
            ->andWhere(['error_code' => 0])
            ->andWhere(['msisdn' => $this->msisdn]);
        $khieunai = $queryKhieunai->all();
        $result = $this->processDataKhieunai($khieunai);
        $dataProvider->allModels = $result;
        return $dataProvider;

    }

    private function processDataKhieunai($data)
    {
        $sodienthoai = 0;
        $hanhdong = '';
        $fee = 0;
        $time = '';
        $package = '';
        $thietbi = '';
        $result = [];
        foreach ($data as $item) {
            $sodienthoai = $item['msisdn'];
            if ($item['action'] == 'REG')
                $hanhdong = 'Đăng ký';
            else if ($item['action'] == 'UNREG')
                $hanhdong = ' Huỷ đăng ký';
            else
                $hanhdong = 'Gia hạn';
            $fee = $item['fee'];
            if ($item['content'] == 'day')
                $package = 'Gói ngày';
            else if ($item['content'] == 'week')
                $package = 'Gói tuần';
            else
                $package = 'Gói tháng';

            $time = $item['datetime'];
            $thietbi = $item['other_info'];

            $item = [
                'msisdn' => $sodienthoai,
                'action' => $hanhdong,
                'fee' => $fee,
                'content' => $package,
                'datetime' => $time,
                'other_info' => $thietbi,

            ];
            $result[] = $item;
        }

        return $result;

    }
}
