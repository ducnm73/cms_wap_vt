<?php

namespace backend\models;

use DateTime;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtComment;
use yii\data\ArrayDataProvider;

/**
 * VtCommentSearch represents the model behind the search form about `backend\models\VtComment`.
 */
class VtCommentSearch extends VtComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'content_id', 'user_id', 'parent_id', 'status', 'approve_by', 'like_count', 'comment_count'], 'integer'],
            [['type', 'content', 'bad_word_filter', 'reason', 'created_at', 'updated_at', 'comment'], 'safe'],
            [['created_at','updated_at','content','comment','type'], 'trim']
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtComment::find()
                ->from(self::tableName(). ' c')
                ->joinWith('auth_user')
                ->joinWith('user');




        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

            // $split = array();
            if (!empty($this->created_at)) {
                if(strpos($this->created_at, ' - ')) {
                    $split = explode(' - ', $this->created_at);

                    $beginDate = trim($split[0]);
                    $endDate = trim($split[1]);

                    $query->andWhere('c.created_at between :beginTime and :endTime', [
                        ':beginTime' => $beginDate,
                        ':endTime' => $endDate
                    ]);
                } else {
                    $query->where('0=1');
                }
            }

            if (!empty($this->updated_at)) {
                if(strpos($this->updated_at, ' - ')) {
                $split = explode(' - ', $this->updated_at);
                $beginDate = trim($split[0]);
                $endDate = trim($split[1]);

                $query->andWhere('c.updated_at between :beginTime and :endTime', [
                    ':beginTime' => $beginDate,
                    ':endTime' => $endDate
                ]);
                } else {
                    $query->where('0=1');
                }
            }


        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.content_id' => $this->content_id,
            'c.user_id' => $this->user_id,
            'c.parent_id' => $this->parent_id,
            'c.status' => $this->status,
            'c.approve_by' => $this->approve_by,
            'c.like_count' => $this->like_count,
            'c.comment_count' => $this->comment_count,
            'c.reason' => $this->reason,
        ]);

        $query->andFilterWhere(['like', 'c.type', $this->type])
            ->andFilterWhere(['like', 'c.content', $this->content])
            ->andFilterWhere(['like', 'c.bad_word_filter', $this->bad_word_filter])
            ->andFilterWhere(['like', 'c.comment', $this->comment]);

//        var_dump($this->created_at);die;
//        echo $query->createCommand()->getRawSql();die;
        return $dataProvider;
    }



    /*
     * @linth
     * created_at: 21/5/2021
     * */
    public function reportComment ($params){
        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);
        if (!$this->validate()) {

            return $dataProvider;
        }
        if (count($params) < 1) {
            return $dataProvider;
        }
        $split = explode(' - ', $this->created_at);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);
        $queryComment = '';

        $queryComment = self::find()->asArray()
            ->select('count(id) as sum_comment, status as trangthai')
            ->addSelect(["DATE_FORMAT(created_at, '%Y-%m-%d') AS date"])
            ->where(['BETWEEN', 'created_at', $beginDate, $endDate])
            ->andWhere(['content_id' => $this->content_id])
            ->groupBy('date');
        $comment = $queryComment->all();

        $result = $this->processDataComment($comment);
        $dataProvider->allModels = $result;

        return $dataProvider;
    }

    private function processDataComment($data){
        $dates = [];
        foreach ($data as $item) {
            if(!in_array($item['date'], $dates)) {
                $dates[] = $item['date'];
            }
        }
        $totalCommentRow['title'] = 'Tổng lượt comment ';
        $commentDraftRow['title'] = 'Tổng số lượt comment đang chờ duyệt';
        $commentApprovedRow['title'] = 'Tổng số lượt comment đã duyệt';
        $commentDeleteRow['title'] = 'Tổng số lượt comment đã bị từ chối';

        foreach ($dates as $date) {
            $totalCommentRow[$date] = 0;
            $commentDraftRow[$date] = 0;
            $commentApprovedRow[$date] = 0;
            $commentDeleteRow[$date] = 0;

            foreach ($data as $item) {
                if ($item['trangthai'] == 0 && $item['date'] == $date) {
                    $commentDraftRow[$date] += $item['sum_comment'];
                    $totalCommentRow[$date] += $item['sum_comment'];
                } else if ($item['trangthai'] == 1 && $item['date'] == $date) {
                    $commentApprovedRow[$date] += $item['sum_comment'];
                    $totalCommentRow[$date] += $item['sum_comment'];
                } else if ($item['trangthai'] == 2 && $item['date'] == $date) {
                    $commentDeleteRow[$date] += $item['sum_comment'];
                    $totalCommentRow[$date] += $item['sum_comment'];
                }
            }
        }
        $rows[] = $totalCommentRow;
        $rows[] = $commentDraftRow;
        $rows[] = $commentApprovedRow;
        $rows[] = $commentDeleteRow;

        return $rows;
    }

    public function reportReasonCommentDisapprove($params){

        $dataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]);
        $this->load($params);

        if (count($params) < 1) {
            return $dataProvider;
        }

        $split = explode(' - ', $this->created_at);
        $beginDate = trim($split[0]);
        $endDate = trim($split[1]);

        $query = self::find()->asArray()
            ->select('count(id) as count , reason')
            ->addSelect(["DATE_FORMAT(created_at, '%Y-%m-%d') AS date"])
            ->where(['BETWEEN', 'created_at', $beginDate, $endDate])
            ->andWhere(['status' => 2])
            ->groupBy('reason')
            ->addGroupBy('date')
            ->all();
        // var_dump($query->createCommand()->getRawSql());die();
        $result = $this->processDataResonCommentDisapprove($query);
        $dataProvider->allModels = $result;

        return $dataProvider;
    }

    private function processDataResonCommentDisapprove($data){
        $dates = [];
        foreach ($data as $item) {
            if(!in_array($item['date'], $dates)) {
                $dates[] = $item['date'];
            }
        }
        $reson1Row['title'] = 'Tục tĩu, khiêu dâm';
        $reson2Row['title'] = 'Spam';
        $reson3Row['title'] = 'Ký tự đặc biệt';
        $reson4Row['title'] = 'Quảng cáo';
        $reson5Row['title'] = 'Ngôn ngữ nhạy cảm ';
        $reson6Row['title'] = 'Kích động bạo lực, thù địch, chống phá nhà nước';
        $reson7Row['title'] = 'Lừa đảo';
        $reson8Row['title'] = 'Khác';
        foreach ($dates as $date) {
            $reson1Row[$date] = 0;
            $reson2Row[$date] = 0;
            $reson3Row[$date] = 0;
            $reson4Row[$date] = 0;
            $reson5Row[$date] = 0;
            $reson6Row[$date] = 0;
            $reson7Row[$date] = 0;
            $reson8Row[$date] = 0;
            foreach ($data as $item) {
                if($item['date'] == $date) {
                    switch ($item['reason']) {
                        case 1:
                            $reson1Row[$date] = $item['count'];
                            break;
                        case 2:
                            $reson2Row[$date] = $item['count'];
                            break;
                        case 3:
                            $reson3Row[$date] = $item['count'];
                            break;
                        case 4:
                            $reson4Row[$date] = $item['count'];
                            break;
                        case 5:
                            $reson5Row[$date] = $item['count'];
                            break;
                        case 6:
                            $reson6Row[$date] = $item['count'];
                            break;
                        case 7:
                            $reson7Row[$date] = $item['count'];
                            break;
                        default:
                            $reson8Row[$date] = $item['count'];
                            break;
                    }
                }

            }
        }
        $rows[] = $reson1Row;
        $rows[] = $reson2Row;
        $rows[] = $reson3Row;
        $rows[] = $reson4Row;
        $rows[] = $reson5Row;
        $rows[] = $reson6Row;
        $rows[] = $reson7Row;
        $rows[] = $reson8Row;

        return $rows;
    }
}
