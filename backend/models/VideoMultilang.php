<?php

namespace backend\models;

class VideoMultilang extends BaseMultilang {
	public $name;
	public $description;
	public $tag;
	public $seo_title;
	public $seo_description;
	public $seo_keywords;

	public function rules() {
		return [
            [['name', 'lang'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description', 'tag', 'seo_description', 'seo_keywords'], 'string', 'max' => 1000],
            [['seo_title'], 'string', 'max' => 200],
		];
	}

	public function attributeLabels() {
		$video = new VtVideo();
		return array_merge($video->attributeLabels(), parent::attributeLabels());
	}
}