<?php

namespace backend\models;

use Yii;

class VtUserPlaylistItem extends \common\models\VtUserPlaylistItemBase {


    public static function getIdsPlaylist($id) {

        $qs = self::find()
                ->asArray()
                ->select('item_id')
                ->where([
                    'playlist_id' => $id])
                ->all();
        $arr = array();
        if (count($qs) > 0) {
            foreach ($qs as $q) {
                $arr[] = $q["item_id"];
            }
        }
        return $arr;
    }

}
