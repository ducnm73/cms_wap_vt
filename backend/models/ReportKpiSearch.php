<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ReportKpi;

/**
 * ReportKpiSearch represents the model behind the search form about `backend\models\ReportKpi`.
 */
class ReportKpiSearch extends ReportKpi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'report_hour', 'avg_time', 'total_trans', 'failure_trans', 'success_rate', 'failure_rate', 'avg_bandwidth'], 'integer'],
            [['type', 'report_day'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportKpi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'report_hour' => $this->report_hour,
            'avg_time' => $this->avg_time,
            'total_trans' => $this->total_trans,
            'failure_trans' => $this->failure_trans,
            'success_rate' => $this->success_rate,
            'failure_rate' => $this->failure_rate,
            'avg_bandwidth' => $this->avg_bandwidth,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        if($params){
            if(!empty( $params["ReportKpiSearch"]["report_day"])){
                if( strpos( $params["ReportKpiSearch"]["report_day"] ," - " )!==false ){
                    $arrDay = preg_split( "/ - /", $params["ReportKpiSearch"]["report_day"] );
                    $query->andFilterWhere(['>=', 'report_day', $arrDay[0]]);
                    $query->andFilterWhere(['<=', 'report_day', $arrDay[1]]);
                }else{
                    $query->andFilterWhere(['=', 'report_day',  $this->report_day]);
                }
            }
        }

        return $dataProvider;
    }

    public function searchFull($params)
    {
        $query = ReportKpi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 999999,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'report_hour' => $this->report_hour,
            'avg_time' => $this->avg_time,
            'total_trans' => $this->total_trans,
            'failure_trans' => $this->failure_trans,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        if($params){
            if( strpos( $params["ReportKpiSearch"]["report_day"] ," - " )!==false ){
                $arrDay = preg_split( "/ - /", $params["ReportKpiSearch"]["report_day"] );
                $query->andFilterWhere(['>=', 'report_day', $arrDay[0]]);
                $query->andFilterWhere(['<=', 'report_day', $arrDay[1]]);
            }else{
                $query->andFilterWhere(['=', 'report_day',  $this->report_day]);
            }
        }
        $query->limit(999999);
        return $dataProvider;
    }
}
