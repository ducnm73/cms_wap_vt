<?php

namespace backend\models;

use api\models\VtVideoTopic;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtUserBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\VtUserPlaylistItemBase;
use yii\db\Expression;

class VtVideo extends \common\models\VtVideoBase {

    public $snapshot;
    public $date;
    public $totalOfApprovedVideo;
    public $totalOfDeclinedVideo;
    public $overdueApprovedVideoByKPI;
    public $numOfContractedUser;
    public $numOfUncontractedUser;
    public $numOfContractedUploadedVideoInDay;
    public $numOfUncontractedUploadedVideoInDay;
    public $totalDeclinedVideo;
    public $totalVideo;
    public $numOfAcceptedVideo;
    public $numOfDeclinedVideo;
    public $numOfWaitVideo;
    public $createdDate;

    const FORMAT_OLD = 'Không play được';
    const FORMAT_NEW = 'Lỗi định dạng';
    const NO_IMAGE_OLD = 'Không có hình ảnh';
    const NO_IMAGE_NEW = 'Không có ảnh video hoặc chất lượng ảnh kém';
    const INAPP_CONTENT_OLD = 'Nội dung không phù hợp';
    const INAPP_CONTENT_NEW = 'Nội dung khiêu dâm';
    const COPYRIGHT_OLD = 'Vi phạm bản quyền';
    const COPYRIGHT_NEW = 'Vấn đề bản quyền';
    const DUPP_CONTENT_OLD = 'Trùng với nội dung đã có trên dịch vụ';
    const DUPP_CONTENT_NEW = 'Nội dung đã có trên UClip';
    const LOW_QUALITY = 'Không đạt yêu cầu về chất lượng hình ảnh, âm thanh';
    const VIOLENCE_CONTENT = 'Nội dung bạo lực, phản cảm112222';
    const OTHER = 'Khác';

    public function rules() {
        return [
            [['is_active', 'is_no_copyright', 'is_hot', 'is_recommend', 'status', 'convert_priority', 'suggest_package_id', 'review_by', 'price_play', 'category_id', 'created_by','is_check'], 'integer'],
            [['name', 'created_at', 'updated_at', 'file_path'], 'required'],
            [['name', 'slug', 'path', 'file_path', 'file_bucket', 'snapshot'], 'string', 'max' => 255],
            [['description', 'reason', 'seo_description'], 'string'],
            [['type'], 'string', 'max' => 10],
            [['seo_title'], 'string', 'max' => 200],
            [['seo_keywords', 'tag'], 'string', 'max' => 1000],
            [['bucket'], 'string', 'max' => 50],
            [['published_time', 'show_times', 'created_at', 'updated_at', 'convert_start_time', 'convert_end_time'], 'safe'],
            [['path'], 'safe'],
            [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],
            [['convert_priority'],'integer', 'min' => 0],
        ];
    }

    public function getMultilangData() {
        $multilangs = json_decode($this->multilang, true);
        $multilangModels = [];

        if(is_array($multilangs)) {
            $langs = [];

            foreach ($multilangs as $key => $value) {
                $lang = substr($key, strripos($key, '_') + 1);

                if(!in_array($lang, $langs)) {
                    $langs[] = $lang;
                }
            }

            foreach ($langs as $lang) {
                $multilangModel = new VideoMultilang(['lang' => $lang]);
                $multilangModel->name = isset($multilangs["name_$lang"]) ? $multilangs["name_$lang"] : '';
                $multilangModel->description = isset($multilangs["description_$lang"]) ? $multilangs["description_$lang"] : '';
                $multilangModel->tag = isset($multilangs["tag_$lang"]) ? $multilangs["tag_$lang"] : '';
                $multilangModel->seo_title = isset($multilangs["seo_title_$lang"]) ? $multilangs["seo_title_$lang"] : '';
                $multilangModel->seo_description = isset($multilangs["seo_description_$lang"]) ? $multilangs["seo_description_$lang"] : '';
                $multilangModel->seo_keywords = isset($multilangs["seo_keywords_$lang"]) ? $multilangs["seo_keywords_$lang"] : '';
                $multilangModels[] = $multilangModel;
            }
        }

        return $multilangModels;
    }

    public function setMultilangData($datas) {
        $values = [];

        if(is_array($datas)) {
            foreach ($datas as $data) {
                $values['name_' . $data['lang']] = $data['name'];
                $values['description_' . $data['lang']] = $data['description'];
                $values['tag_' . $data['lang']] = $data['tag'];
                $values['seo_title_' . $data['lang']] = $data['seo_title'];
                $values['seo_description_' . $data['lang']] = $data['seo_description'];
                $values['seo_keywords_' . $data['lang']] = $data['seo_keywords'];
            }

            $values = array_unique($values);
        }

        $this->multilang = empty($values) ? null : json_encode($values);
    }

    /*public function getMultilangString() {
        $value = json_decode($this->multilang, true);
        $arr = [];

        foreach ($value as $k => $v) {
            $field = explode('_', $k)[0];
            $lang = explode('_', $k)[1];
            $arr[$field][] = [
                'lang' => $lang,
                'value' => $v,
            ];
        }

        return $arr;
    }

    public function setMultilangString($value) {
        $arr = [];

        foreach ($value as $pk => $pv) {
            foreach ($pv as $v) {
                $arr[$pk . '_' . $v['lang']] = $v['value'];
            }
        }

        $this->multilang = json_encode($arr);
    }*/

    public function getTopics() {
        return $this->hasMany(VtGroupTopic::className(), ['id' => 'topic_id'])
                        ->viaTable(VtVideoTopic::tableName(), ['video_id' => 'id']);
    }

    public function getUser() {
        return $this->hasOne(VtUserBase::className(), ['id' => 'created_by']);
    }

    public function checkApprovePermission() {
        if (Yii::$app->user->can('approve-video')) {
            return true;
        } else {
            return false;
        }
    }

    public function isOwner() {
        if (Yii::$app->user->identity->user_id == $this->created_by) {
            return true;
        } else {
            return false;
        }
    }

    public function isApprove() {
        if ($this->status == self::STATUS_APPROVE) {
            return true;
        } else {
            return false;
        }
    }

    public function isDraft() {
        if ($this->status == self::STATUS_DRAFT) {
            return true;
        } else {
            return false;
        }
    }

    public function isDelete() {
        if ($this->status == self::STATUS_DELETE) {
            return true;
        } else {
            return false;
        }
    }

    public function isConverted() {
        if ($this->convert_status == self::CONVERT_STATUS_SUCCESS) {
            return true;
        } else {
            return false;
        }
    }

    public function saveWithImage($topicIds, $snapshot = false) {
        $file = UploadedFile::getInstance($this, 'path');

        if (!empty($file)) {
            $uploadResult = VtHelper::uploadAndGenerateThumbs($file, dirname($this->file_path));
            $this->path = $uploadResult['path'];

            if(isset($uploadResult['bucket'])) {
                $this->bucket = $uploadResult['bucket'];
            }

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->user_id;
            }
        } else if ($snapshot == false) {
            unset($this->bucket);
            unset($this->path);
        }
        $this->name_slug = Utils::removeSignOnly($this->name);
        $this->description_slug = Utils::removeSignOnly($this->description);
        $this->updated_by = Yii::$app->user->identity->id;

        $this->save(false);


        //Xu ly topics
        if (($actualTopics = VtVideoTopic::find()
                ->andWhere(["video_id" => $this->id])
                ->asArray()
                ->all()) !== null
        ) {
            $actualTopics = ArrayHelper::getColumn($actualTopics, 'topic_id');
            $topicExists = 1;
        }

        if (!empty($topicIds)) { //save the relations
            foreach ($topicIds as $topicId) {

                if (!in_array($topicId, $actualTopics)) {
                    $r = new VtVideoTopic();
                    $r->video_id = $this->id;
                    $r->topic_id = $topicId;
                    $r->save(false);
                }
            }
        }

        if (!empty($topicIds)) {
            $actualTopics = array_diff($actualTopics, $topicIds);
        } else {
            $actualTopics = [];
        }


        if (isset($topicExists) && $topicExists == 1) {

            foreach ($actualTopics as $remove) {
                $r = VtVideoTopic::findOne(
                                [
                                    'topic_id' => $remove,
                                    'video_id' => $this->id
                                ]
                );
                $r->delete();
            }
        }
    }

    public function getCreatedByName() {
        $objUsername = VtUserBase::getById($this->created_by);
        return ($objUsername) ? (($objUsername->full_name) ? $objUsername->full_name : $objUsername->msisdn) : $this->created_by;
    }

    public function getUpdatedByName() {
        $objUsername = VtUserBase::getById($this->updated_by);
        return ($objUsername) ? $objUsername->full_name : $this->updated_by;
    }

    public static function getVideosByPlaylist($id, $limit = 10, $offset = null, $type = self::TYPE_VOD) {

        $query = self::find()
                ->asArray()
                ->select('v.*,u.full_name')
                ->from(VtUserPlaylistItemBase::tableName() . ' pi')
                ->leftJoin(self::tableName() . ' v', 'v.id = pi.item_id')
                ->leftJoin(VtUser::tableName() . ' u', 'u.id = v.created_by')
                ->where([
            'v.type' => $type,
            'v.is_active' => self::ACTIVE,
            'v.status' => self::STATUS_APPROVE,
            'v.is_no_copyright' => 0,
            'pi.playlist_id' => $id
        ]);
//        echo $query->createCommand()->getRawSql();die;
        if (!empty($offset)) {

            $query->offset($offset);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }
        $r = $query->orderBy(['pi.position' => SORT_ASC], ['published_time' => SORT_DESC])
                ->all();
        return $r;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Tên') . ' *',
            'description' => Yii::t('app', 'Mô tả'),
            'slug' => Yii::t('app', 'Slug'),
            'category_id' => Yii::t('app', 'Thể loại'),
            'attributes' => Yii::t('app', 'Thuộc tính'),
            'published_time' => Yii::t('app', 'Thời gian xuất bản'),
            'is_active' => Yii::t('app', 'Kích hoạt'),
            'price_download' => Yii::t('app', 'Giá tải'),
            'price_play' => Yii::t('app', 'Giá xem'),
            'play_times' => Yii::t('app', 'Lượt xem'),
            'status' => Yii::t('app', 'Trạng thái'),
            'reason' => Yii::t('app', 'Lý do'),
            'approve_by' => Yii::t('app', 'Phê duyệt bởi'),
            'type' => Yii::t('app', 'Loại'),
            'cp_id' => Yii::t('app', 'CP'),
            'created_at' => Yii::t('app', 'Thời gian tạo'),
            'updated_at' => Yii::t('app', 'Thời gian cập nhật'),
            'created_by' => Yii::t('app', 'Tạo bởi'),
            'updated_by' => Yii::t('app', 'Cập nhật bởi'),
            'review_by' => Yii::t('app', 'Xem lướt bởi'),
            'tag' => Yii::t('app', 'Tag'),
            'related_id' => Yii::t('app', 'Video liên quan'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'is_no_copyright' => Yii::t('app', 'Không có bản quyền'),
            'like_count' => Yii::t('app', 'Lượt yêu thích'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => 'Ảnh đại diện',
            'is_hot' => Yii::t('app', 'Đề xuất'),
            'is_check' => Yii::t('app', 'No need login'),
            'is_recommend' => Yii::t('app', 'Thịnh hành'),
            'convert_status' => Yii::t('app', 'Trạng thái convert'),
            'convert_start_time' => Yii::t('app', 'Thời gian bắt đầu convert'),
            'convert_end_time' => Yii::t('app', 'Thời gian kết thúc convert'),
            'duration' => Yii::t('app', 'Thời lượng'),
            'file_path' => Yii::t('app', 'File Path'),
            'file_bucket' => Yii::t('app', 'File Bucket'),
            'convert_priority' => Yii::t('app', 'Thứ tự convert'),
            'resolution' => Yii::t('app', 'Độ phân giải'),
            'topics' => Yii::t('backend','Chủ đề'),
            'suggest_package_id' => Yii::t('backend','Gói cước quảng cáo'),
            'show_times' => Yii::t('backend','Thời gian xem')
        ];
    }

    /** Get video name.
     * @param $itemId (video id)
     * @return $this
     * @author thuynd10
     */
    public static function getVideoName($itemId){
        $video = self::find()->select(['name'])->where(['id' => $itemId])->one();
        return $video;
    }

    /** Get number of video which is uploaded by user is approved from 0h00 to 23h59.
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function getTotalOfApprovedVideo($fromTime, $toTime){
        $query = self::find()
            ->select(['date(outsource_review_at) as date', 'count(id) as totalOfApprovedVideo'])
            ->where(['>=', 'outsource_review_at', $fromTime])
            ->andWhere(['<=', 'outsource_review_at', $toTime])
            ->andWhere(['syn_id' => null])
            ->andWhere(['outsource_status' => 1])
            ->andWhere(['outsource_need_review' => 1])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    /** Get number of video which is uploaded by user is declined from 0h00 to 23h59.
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function getTotalOfDeclinedVideo($fromTime, $toTime){
        $query = self::find()
            ->select(['date(outsource_review_at) as date', 'count(id) as totalOfDeclinedVideo'])
            ->where(['>=', 'outsource_review_at', $fromTime])
            ->andWhere(['<=', 'outsource_review_at', $toTime])
            ->andWhere(['syn_id' => null])
            ->andWhere(['outsource_status' => 2])
            ->andWhere(['outsource_need_review' => 1])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    /** Get number of video which is approved overdue by KPI (8h00 to 23h00).
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function sumOfOverdueApprovedVideoByKPI($fromTime, $toTime){
        $query = self::find()
            ->select(['date(created_at) as date', 'count(id) as overdueApprovedVideoByKPI'])
            ->where(['and',
                ['>=', 'created_at', $fromTime],
                ['<=', 'created_at', $toTime],
                ['syn_id' => null],
                ('DATE_ADD(created_at, INTERVAL (duration + 15*60) second) < outsource_review_at'),
                ['outsource_need_review' => 1],
                ['between', 'hour(created_at)', '08', '23']
            ])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    /** Get number of user who is signed contract upload in day.
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function sumOfVideoUploadedByContractedUser($fromTime, $toTime){
        $query = self::find()
            ->select(['date(created_at) as date', 'count(distinct created_by) as numOfContractedUser'])
            ->where(['>=', 'created_at', $fromTime])
            ->andWhere(['<=', 'created_at', $toTime])
            ->andWhere(['status' => 2])
            ->andWhere(['is_active' => 1])
            ->andWhere(['like', 'cp_code', 'USER_UPLOAD'.'%', false])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    /** Get number of user who isn't signed contract upload in day.
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function sumOfVideoUploadedByUncontractedUser($fromTime, $toTime){
        $query = self::find()
            ->select(['date(created_at) as date', 'count(distinct created_by) as numOfUncontractedUser'])
            ->where(['>=', 'created_at', $fromTime])
            ->andWhere(['<=', 'created_at', $toTime])
            ->andWhere(['syn_id' => null])
            ->andWhere(['cp_code' => null])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    /** Get number of video which is uploaded in day by user who is signed contracted.
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function numOfVideoUploadedInDayOfContractedUser($fromTime, $toTime){
        $query = self::find()
            ->select(['date(created_at) as date', 'count(id) as numOfContractedUploadedVideoInDay'])
            ->where(['>=', 'created_at', $fromTime])
            ->andWhere(['<=', 'created_at', $toTime])
            ->andWhere(['like', 'cp_code', 'USER_UPLOAD'.'%', false])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    /** Get number of video which is uploaded in day by user who isn't signed contracted.
     * @param $fromTime, $toTime
     * @return list of number of videos group by day
     * @author thuynd10
     */
    public static function numOfVideoUploadedInDayOfUncontractedUser($fromTime, $toTime){
        $query = self::find()
            ->select(['date(created_at) as date', 'count(id) as numOfUncontractedUploadedVideoInDay'])
            ->where(['>=', 'created_at', $fromTime])
            ->andWhere(['<=', 'created_at', $toTime])
            ->andWhere(['cp_code' => null])
        ;
        $query = $query->groupBy(['date'])->orderBy(['date' => SORT_DESC])->all();
        return $query;
    }

    public static function totalOfDeclinedVideo($fromTime, $toTime, $reasonId){
        $query = self::find()
            ->select(['reason', 'count(id) as totalDeclinedVideo'])
            ->where(['and',
                ['>=', 'admin_review_first_times_at', $fromTime],
                ['<=', 'admin_review_first_times_at', $toTime],
                ['status' => 3]
            ]);
        $query = self::getReasonQuery($query, $reasonId);
        $query = $query->groupBy(['reason'])->all();
        return $query;
    }

    public static function getVideoData($fromTime, $toTime, $reasonId){
        $total_user_video_subquery = self::getVideoUploadedByUserSubquery($fromTime, $toTime, '', 'totalVideo', '');
        $approved_user_video_subquery = self::getVideoUploadedByUserSubquery($fromTime, $toTime, 1, 'numOfAcceptedVideo', '');
        $declined_user_video_subquery = self::getVideoUploadedByUserSubquery($fromTime, $toTime, 2, 'numOfDeclinedVideo', $reasonId);

        $total_cp_video_subquery = self::getVideoUploadedByCPSubquery($fromTime, $toTime, '', 'totalVideo', '');
        $approved_cp_video_subquery = self::getVideoUploadedByCPSubquery($fromTime, $toTime, 2, 'numOfAcceptedVideo', '');
        $declined_cp_video_subquery = self::getVideoUploadedByCPSubquery($fromTime, $toTime, 3, 'numOfDeclinedVideo', $reasonId);

        $queryVideoUploadedByUser = self::find()
            ->select('a.date, a.type, a.totalVideo, b.numOfAcceptedVideo, c.numOfDeclinedVideo')
            ->from(['a' => $total_user_video_subquery])
            ->leftJoin(['b' => $approved_user_video_subquery], 'a.date = b.date')
            ->leftJoin(['c' => $declined_user_video_subquery], 'a.date = c.date');

        $queryVideoUploadedByCP = self::find()
            ->select('a.date, a.type, a.totalVideo, b.numOfAcceptedVideo, c.numOfDeclinedVideo')
            ->from(['a' => $total_cp_video_subquery])
            ->leftJoin(['b' => $approved_cp_video_subquery], 'a.date = b.date')
            ->leftJoin(['c' => $declined_cp_video_subquery], 'a.date = c.date');

        $query = (new \yii\db\Query())->from(['dummy_name' => $queryVideoUploadedByUser->union($queryVideoUploadedByCP)]);
        $query = $query->orderBy(['date' => SORT_DESC, 'type' => SORT_ASC]);
        return $query;
    }

    function getVideoUploadedByUserSubquery($fromTime, $toTime, $outsourceStatus, $aliasColumn, $reasonId){
        $subquery = self::find()->select(['date(vv.outsource_review_at) date', new Expression('2 as type'), 'count(vv.id) as '. $aliasColumn .''])
            ->from(['vv' => 'vt_video'])->leftJoin(['vc' => 'vt_cp'], 'vv.cp_id = vc.id')->where([
            'and',
                ['>=', 'vv.outsource_review_at', $fromTime],
                ['<=', 'vv.outsource_review_at', $toTime],
                ['vv.syn_id' => null],
                ['vv.outsource_need_review' => 1],
                ['or',
                    ['vc.type' => 2],
                    ['vv.cp_id' => null]
                ]
            ]);
        if($outsourceStatus != ''){
            $subquery = $subquery->andWhere(['vv.outsource_status' => $outsourceStatus]);
        }
        $subquery = self::getReasonQuery($subquery, $reasonId);
        $subquery = $subquery->groupBy('date');
        return $subquery;
    }

    function getVideoUploadedByCPSubquery($fromTime, $toTime, $status, $aliasColumn, $reasonId){
        $subquery = self::find()->select(['date(vv.admin_review_first_times_at) date' , new Expression('1 as type'), 'count(vv.id) as '. $aliasColumn .''])
            ->from(['vv' => 'vt_video'])->leftJoin(['vc' => 'vt_cp'], 'vv.cp_id = vc.id')->where([
                'and',
                ['>=', 'vv.admin_review_first_times_at', $fromTime],
                ['<=', 'vv.admin_review_first_times_at', $toTime],
                ['vc.type' => 1],
            ]);
        if($status != ''){
            $subquery = $subquery->andWhere(['vv.status' => $status]);
        }
        $subquery = self::getReasonQuery($subquery, $reasonId);
        $subquery = $subquery->groupBy('date');
        return $subquery;
    }

    function getReasonQuery($subquery, $reasonId){
        if($reasonId != ''){
            if($reasonId == Yii::t('backend','Lỗi định dạng')){
                $subquery = $subquery->andWhere(['reason' => [self::FORMAT_OLD, self::FORMAT_NEW]]);
            } else if($reasonId == Yii::t('backend','Không có ảnh video hoặc chất lượng ảnh kém')) {
                $subquery = $subquery->andWhere(['reason' => [self::NO_IMAGE_OLD, self::NO_IMAGE_NEW]]);
            } else if($reasonId == Yii::t('backend','Nội dung khiêu dâm')) {
                $subquery = $subquery->andWhere(['reason' => [self::INAPP_CONTENT_OLD, self::INAPP_CONTENT_NEW]]);
            } else if($reasonId == Yii::t('backend','Vấn đề bản quyền')) {
                $subquery = $subquery->andWhere(['reason' => [self::COPYRIGHT_OLD, self::COPYRIGHT_NEW]]);
            } else if($reasonId == Yii::t('backend','Nội dung đã có trên UClip')) {
                $subquery = $subquery->andWhere(['reason' => [self::DUPP_CONTENT_OLD, self::DUPP_CONTENT_NEW]]);
            } else if($reasonId == Yii::t('backend','Không đạt yêu cầu về chất lượng hình ảnh, âm thanh')) {
                $subquery = $subquery->andWhere(['reason' => self::LOW_QUALITY]);
            } else if($reasonId == Yii::t('backend','Nội dung bạo lực, phản cảm')) {
                $subquery = $subquery->andWhere(['reason' => self::VIOLENCE_CONTENT]);
            } else if($reasonId == Yii::t('backend','Khác')){
                $subquery = $subquery->andWhere(['or', ['not in', 'reason', [self::FORMAT_OLD, self::FORMAT_NEW, self::NO_IMAGE_OLD,
                    self::NO_IMAGE_NEW, self::INAPP_CONTENT_OLD, self::INAPP_CONTENT_NEW, self::COPYRIGHT_OLD,
                    self::COPYRIGHT_NEW, self::DUPP_CONTENT_OLD, self::DUPP_CONTENT_NEW, self::LOW_QUALITY, self::VIOLENCE_CONTENT]], ['reason' => null]] );
            }
        }
        //var_dump($subquery);die;
        return Yii::t('backend',$subquery);
    }

    public static function countVideoCrawlerByCP($fromDate, $toDate){
        $query = self::find()
            ->select(['vv.cp_code', 'date(vv.created_at) as createdDate', 'count(vv.id) as numOfWaitVideo'])
            ->from(['vv' => 'vt_video'])->leftJoin(['vc' => 'vt_cp'], 'vv.cp_id = vc.id')
            ->where(['and',
                ['>=', 'date(vv.created_at)', $fromDate],
                ['<=', 'date(vv.created_at)', $toDate],
                ['vv.status' => 1],
                ['vc.type' => 1]
            ]);
        $query = $query->groupBy(['vv.cp_code', 'createdDate'])->orderBy(['createdDate' => SORT_ASC]);
        return $query;
    }

    /** Export list of video which was pending for approving.
     * @param $fromTime, $toTime
     * @return list of pending videos
     * @author thuynd10
     */
    public static function countVideoCrawlerByCPDetail($fromDate, $toDate){
        $countVideoSubquery = self::find()
            ->select(['vv.cp_code', 'date(vv.created_at) as createdDate', 'count(vv.id) as numOfWaitVideo'])
            ->from(['vv' => 'vt_video'])->leftJoin(['vc' => 'vt_cp'], 'vv.cp_id = vc.id')
            ->where(['and',
                ['>=', 'date(vv.created_at)', $fromDate],
                ['<=', 'date(vv.created_at)', $toDate],
                ['vv.status' => 1],
                ['vc.type' => 1]
            ])->groupBy(['vv.cp_code', 'createdDate']);
        $detailVideoSubquery = self::find()
            ->select(['vv.cp_code', 'date(vv.created_at) as createdDate', 'vv.name'])
            ->from(['vv' => 'vt_video'])->leftJoin(['vc' => 'vt_cp'], 'vv.cp_id = vc.id')
            ->where(['and',
                ['>=', 'date(vv.created_at)', $fromDate],
                ['<=', 'date(vv.created_at)', $toDate],
                ['vv.status' => 1],
                ['vc.type' => 1]]);
        $query = self::find()->select('a.cp_code, a.createdDate, a.numOfWaitVideo, b.name')
                ->from(['a' => $countVideoSubquery])->leftJoin(['b' => $detailVideoSubquery],
            'a.cp_code = b.cp_code and a.createdDate = b.createdDate')->orderBy(['createdDate' => SORT_ASC])->all();
        return $query;
    }

    public static function getPlayTimesCategory($cate_id) {
        $cache = Yii::$app->cache;
        $key = 'DBCACHE_CATEGORY_PLAYTIMES_' . $cate_id;

        if(!($value = $cache->get($key))) {
            $query = self::find()
                ->select(['SUM(play_times) AS sum'])
                ->asArray()
                ->where([
                    'is_active' => 1, 
                    'status' => 2, 
                    'category_id' => $cate_id,
                ]);
            $sum = $query->one();
            $value = $sum['sum'];
            $cache->set($key, $value, 86400);
        }

        return $value;
    }

    public function getDetailVideoPricePlay($id)
    { 
        $linkDetail = Yii::$app->params['app.domain'] . "video/" . $id . "/" . VtVideo::findOneById($id)['slug'] . "?utm_source=APPSHARE";
        return $linkDetail;
    }
}
