<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtPlaylist;

/**
 * VtPlaylistSearch represents the model behind the search form about `backend\models\VtPlaylist`.
 */
class VtPlaylistSearch extends VtPlaylist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'category_id', 'attributes', 'price_play', 'priority', 'approved_by', 'created_by', 'updated_by', 'is_hot', 'is_recommend', 'like_count'], 'integer'],
            [['type', 'name', 'description', 'reason', 'created_at', 'updated_at', 'bucket', 'path'], 'safe'],
            [['name'],'trim']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $state = Yii::$app->session->get('state');

        $query = VtPlaylist::find()
            ->from(self::tableName(). ' v')
            ->joinWith('category');

        if(!Yii::$app->user->can('approve-film')){
            // $query->andWhere(['v.created_by' => Yii::$app->user->identity->user_id]);
        }

        switch ($state) {
            case 'temp':
                $query->andWhere(['v.status' => VtPlaylist::STATUS_TEMP]);
                break;
            case 'published':
                $query->andWhere(['v.status' => VtPlaylist::STATUS_APPROVE]);
                break;
            case 'draft':
                $query->andWhere(
                    [
                        'v.status' => VtPlaylist::STATUS_DRAFT
                    ]
                );
                break;
            case 'deleted':
                $query->andWhere(
                    ['v.status' => VtPlaylist::STATUS_DELETE]
                );
                break;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'v.id' => $this->id,
            'v.status' => $this->status,
            'v.category_id' => $this->category_id,
            'v.attributes' => $this->attributes,
            'v.price_play' => $this->price_play,
            'v.priority' => $this->priority,
            'v.approved_by' => $this->approved_by,
            'v.created_at' => $this->created_at,
            'v.updated_at' => $this->updated_at,
            'v.created_by' => $this->created_by,
            'v.updated_by' => $this->updated_by,
            'v.is_hot' => $this->is_hot,
            'v.is_recommend' => $this->is_recommend,
            'v.like_count' => $this->like_count,
        ]);

        $query->andFilterWhere(['like', 'v.type', $this->type])
            ->andFilterWhere(['like', 'v.name', $this->name])
            ->andFilterWhere(['like', 'v.description', $this->description])
            ->andFilterWhere(['like', 'v.reason', $this->reason])
            ->andFilterWhere(['like', 'v.bucket', $this->bucket])
            ->andFilterWhere(['like', 'v.path', $this->path]);

        return $dataProvider;
    }
}
