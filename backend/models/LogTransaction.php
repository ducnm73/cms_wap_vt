<?php

namespace backend\models;

use Yii;

class LogTransaction extends \common\models\LogTransactionBase {

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Thuê bao'),
            'msisdn' => Yii::t('app', 'Số điện thoại'),
            'action' => Yii::t('app', 'Hoạt động'),
            'fee' => Yii::t('app', 'Giá cước'),
            'error_code' => Yii::t('app', 'Mã lỗi'),
            'content' => Yii::t('app', 'Nội dung'),
            'datetime' => Yii::t('app', 'Thời gian'),
            'other_info' => Yii::t('app', 'Nguồn'),
        ];
    }


}