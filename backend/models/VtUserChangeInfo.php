<?php

namespace backend\models;

use common\models\VtUserChangeInfoBase;
use Yii;

class VtUserChangeInfo extends \common\models\VtUserChangeInfoBase {
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
        ];
    }

    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-user-change-info')) {
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'status' => Yii::t('backend', 'Trạng thái'),
            'full_name' => Yii::t('backend', 'Tên kênh'),
            'bucket' => Yii::t('backend', 'Bucket'),
            'path' => Yii::t('backend', 'Path'),
            'channel_bucket' => Yii::t('backend', 'Channel Bucket'),
            'channel_path' => Yii::t('backend', 'Channel Path'),
            'description' => Yii::t('backend', 'Mô tả'),
            'reason' => Yii::t('backend', 'Lý do'),
            'created_at' => Yii::t('backend', 'Tạo lúc'),
            'updated_at' => Yii::t('backend', 'Cập nhật lúc'),
        ];
    }

    public  function getUserByUseID($userID){
        $query = VtUserChangeInfoBase::getUserById($this->user_id);
        // return ($query) ? $query-> user_id : $this -> user_id;
        return $query;
    }
}