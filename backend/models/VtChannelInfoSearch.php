<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VtChannelInfo;

/**
 * VtChanneInfoSearch represents the model behind the search form about `backend\models\VtChannelInfo`.
 */
class VtChannelInfoSearch extends VtChannelInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer', 'min' => 0],
//            [['id', 'user_id'], 'integer', 'min' => 0],
            [['description','full_name','channel_id'], 'trim'],
            [['status'], 'integer'],
            [['full_name', 'bucket', 'path', 'channel_bucket', 'channel_path', 'description', 'reason', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtChannelInfo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]]
        ]);
        $query->where(['or', ['<>', 'full_name', null], ['<>', 'full_name', '']]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'channel_id' => $this->channel_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        if($this->status != 3){
            $query->andFilterWhere([
                'status' => $this->status
            ]);
        }

        $query->andFilterWhere(['like', 'full_name', trim($this->full_name)])
            ->andFilterWhere(['like', 'bucket', trim($this->bucket)])
            ->andFilterWhere(['like', 'path', trim($this->path)])
            ->andFilterWhere(['like', 'channel_bucket', trim($this->channel_bucket)])
            ->andFilterWhere(['like', 'channel_path', trim($this->channel_path)])
            ->andFilterWhere(['like', 'description', trim($this->description)])
            ->andFilterWhere(['like', 'reason', trim($this->reason)]);

        return $dataProvider;
    }
}
