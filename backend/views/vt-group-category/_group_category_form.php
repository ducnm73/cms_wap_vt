<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => 255])->label(Yii::t('app', 'Tên') . '*') ?>

<?= $form->field($model, 'type')->dropDownList(\common\libs\VtHelper::getObjectTypeArray(Yii::$app->params['object.type']), ['class' => 'form-control', 'prompt' => '--- ' . Yii::t('backend', 'Chọn thể loại') . ' ---'])->label(Yii::t('app', 'Loại') . '*'); ?>

<?= $form->field($model, 'description')->textarea(['rows' => 6, 'maxlength' => 255])->label(Yii::t('app', 'Mô tả') . '*') ?>

<?= $form->field($model, 'is_active')->checkbox([], false)->label(Yii::t('app', 'Kích hoạt')) ?>

<?= $form->field($model, 'is_hot')->checkbox([], false)->label(Yii::t('app', 'Hot')) ?>

<?php if (!$model->isNewRecord && !empty($model->avatar_path)): ?>
    <div class="form-group">
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->avatar_bucket, $model->avatar_path) ?>"/>
    </div>
<?php endif ?>
<?= $form->field($model, 'avatar_path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>
    <label class="control-label color_red"><?= Yii::t('web', 'Vui lòng chọn ảnh {extension}, kích thước {size}', ['extension' => 'JPG, PNG', 'size' => '180*180px'])?></label>
<?php if (!$model->isNewRecord && !empty($model->path)): ?>
    <div class="form-group">
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->bucket, $model->path) ?>" height="20%"/>
    </div>
<?php endif ?>
<?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh banner')) ?>
    <label class="control-label color_red"><?= Yii::t('web', 'Vui lòng chọn ảnh {extension}, kích thước {size}', ['extension' => 'JPG, PNG', 'size' => '1920*330px'])?></label>
<?= $form->field($model, 'positions')->dropDownList(backend\models\VtGroupCategory::getPosition(), ['class' => 'form-control', 'prompt' => '--- ' . Yii::t('backend', 'Chọn vị trí') . ' ---'])->label(Yii::t('app', 'Vị trí')); ?>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>