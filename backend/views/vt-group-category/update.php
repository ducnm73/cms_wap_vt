<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupCategory */

$this->title = Yii::t('app', 'Cập nhật {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Thể loại'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Cập nhật');
?>
<div class="vt-group-topic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'multilangModels' => $multilangModels,
    ]) ?>

</div>
