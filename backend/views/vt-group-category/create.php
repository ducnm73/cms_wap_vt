<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupCategory */

$this->title = Yii::t('app', 'Tạo mới {modelClass}', [
    'modelClass' => Yii::t('app', 'Thể loại'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Thể loại'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-group-topic-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="vt-group-category-form">
	    <?= $this->render('_group_category_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
