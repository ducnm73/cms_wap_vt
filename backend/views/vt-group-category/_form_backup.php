<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-group-topic-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255])->label(Yii::t('app', 'Tên') . '*') ?>

    <?= $form->field($model, 'type')->dropDownList(\common\libs\VtHelper::getObjectTypeArray(Yii::$app->params['object.type']), ['class' => 'form-control', 'prompt' => "---" . Yii::t('backend', 'Chọn thể loại') . "---"])->label(Yii::t('app', 'Loại') . '*'); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(Yii::t('app', 'Mô tả') . '*') ?>

    <?= $form->field($model, 'is_active')->checkbox([], false)->label(Yii::t('app', 'Kích hoạt')) ?>

    <?= $form->field($model, 'is_hot')->checkbox([], false)->label(Yii::t('app', 'Hot')) ?>


    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(backend\models\VtGroupCategory::getParents(), 'id', 'name'), ['class' => 'form-control', 'prompt' => '--- ' . Yii::t('backend', 'Chọn thể loại cha') . ' ---'])->label(Yii::t('app', 'Thể loại cha')); ?>

    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->avatar_bucket, $model->avatar_path) ?>"/>
    <?php endif ?>
    <?= $form->field($model, 'avatar_path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>
    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->bucket, $model->path) ?>" height="20%"/>
    <?php endif ?>
    <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh banner')) ?>
    <?= $form->field($model, 'positions')->dropDownList(backend\models\VtGroupCategory::getPosition(), ['class' => 'form-control', 'prompt' => '--- ' . Yii::t('backend', 'Chọn vị trí') . ' ---'])->label(Yii::t('app', 'Vị trí')); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
