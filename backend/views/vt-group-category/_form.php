<?php

use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-group-category-form">
    <?= Tabs::widget([
        'options' => ['id' => 'vt-group-category-form-tabs'],
        'items' => [
            [
                'label' => Yii::t('backend','Thông tin thể loại'),
                'encode' => false,
                'content' => $this->render('_group_category_form', ['model' => $model]),
                'active' => true
            ],
            [
                'label' => Yii::t('backend','Đa ngôn ngữ'),
                'encode' => false,
                'content' => $this->render('_multilang_form', ['model' => $model, 'multilangModels' => $multilangModels]),
            ],
        ],
    ]); ?>

</div>