<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtSmsApprove */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-sms-approve-form">

    <?php $form = ActiveForm::begin(); ?>
    <input type="hidden" id="sms_approve_id" value="<?= $model->id ?>">
    <div class="form-group field-vtsmsapprove-sms_code has-success">
        <label class="control-label" for="vtsmsapprove-sms_code"><?= Yii::t('backend','Mã tin nhắn:')?> </label>
        <strong>
            <?php echo $model->sms_code; ?>
        </strong>
    </div>

    <div style="border: solid 1px gray; padding: 20px">
        <div class="form-group field-vtsmsapprove-sms_content has-success">
            <label class="control-label" for="vtsmsapprove-sms_content"><?= Yii::t('backend','Nội dung tin nhắn đã duyệt')?></label>
            <p style="padding: 20px; border: solid 1px #ccc">
                <?php echo $model->sms_content; ?>
            </p>

            <select id="sms_contract_list_main" class="form-control"
                    style="width: 250px; float: left; margin-right: 10px">
                <?php
                foreach (\common\helpers\Utils::getInterlizationParams('sms.approve.test.list', true, 'backend') as $key => $value) {
                    ?>
                    <option value="<?php echo $key ?>"><?php echo $value ?></option>

                <?php }; ?>
            </select>

            <button class="btn" type="button" onclick="checkSendSMS('sms_contract_list_main')"><?= Yii::t('backend','Kiểm tra lại tin đã duyệt')?>

            </button>
        </div>

    </div>


    <br/>
    <div style="border: solid 1px gray; padding: 20px">
        <?= $form->field($model, 'sms_content_draft')->textarea(['maxlength' => 1000, 'placeholder'=>Yii::t('backend','Nhập tin cần duyệt lại')]) ?>
        <select id="sms_contract_list_draft" class="form-control" style="width: 250px; float: left; margin-right: 10px">
            <?php
            foreach (\common\helpers\Utils::getInterlizationParams('sms.approve.test.list', true, 'backend') as $key => $value) {
                ?>
                <option value="<?php echo $key ?>"><?php echo $value ?></option>

            <?php }; ?>
        </select>
        <button class="btn" type="button" onclick="checkSendSMS('sms_contract_list_draft')"><?= Yii::t('backend','Kiểm tra tin nháp')?></button>
    </div>
    <br/><br/>

    <?= $form->field($model, 'note')->textInput(['maxlength' => 255, 'placeholder' => Yii::t('backend','Đây là tin nhắn dành cho').' ...']) ?>


    <div class="form-group field-vtsmsapprove-status has-success">
        <label class="control-label" for="vtsmsapprove-status"><?= Yii::t('backend','Trạng thái:')?>
            <strong>
                <?php
                $statusArr = \common\helpers\Utils::getInterlizationParams('sms.approve.status', true, 'backend');
                if(!is_null($model->status)) {
                    echo ($statusArr[$model->status]) ? $statusArr[$model->status] : $model->status;
                } else {
                    echo Yii::t('backend', 'Không xác định');
                }
                ?>
            </strong>
        </label>

        <div class="help-block"></div>
    </div>
















    <div class="form-group">
        <?php if ($model->status != 3) { ?>
            <button type="submit" name="btn-sms-approve" class="btn btn-success" value="REQUEST_APPROVE_SMS"><?= Yii::t('backend','Yêu cầu duyệt lại tin nhắn')?>

            </button>
        <?php } ?>

        <?php if ($model->status == 1 && $model->isHasPermistionApprove()) { ?>
            <button type="submit" name="btn-sms-approve" class="btn btn-primary" value="APPROVE_SMS"><?= Yii::t('backend','Duyệt tin nhắn')?>
            </button>
        <?php } ?>

        <?php if (($model->status == 1 || $model->status == 2) && $model->isHasPermistionRemove()) { ?>
            <button type="submit" name="btn-sms-approve" class="btn btn-danger" value="REMOVE_SMS"><?= Yii::t('backend','Hạ tin nhắn')?></button>
        <?php } ?>

        <?php if ($model->status == 3 && $model->isHasPermistionOpenSMS()) { ?>
            <button type="submit" name="btn-sms-approve" class="btn btn-warning" value="REOPEN_SMS"><?= Yii::t('backend','Mở lại tin nhắn đã hạ')?>
            </button>
        <?php } ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
