<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtSmsApprove */

$this->title = Yii::t('backend','Cập nhật SMS khuyến mãi').': ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Duyệt SMS khuyến mãi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend','Cập nhật');
?>
<div class="vt-sms-approve-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
