<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtSmsApprove */

$this->title = Yii::t('backend','Tạo mới tin nhắn duyệt');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Duyệt SMS khuyến mãi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-sms-approve-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
