<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;
use backend\models\VtSpam;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtSpamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chương trình truyền thông');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-spam-index">

    <p>
        <?= Html::a(Yii::t('app', 'Tạo mới'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Tên chương trình'),
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function ($model) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['vt-spam/update', 'id' => $model->id]) .'">'
                    . mb_substr(htmlentities($model->name), 0,50,'UTF-8') .((strlen($model->name)>50)?'...':'') .'</a>';
                },
            ],
            [
                'attribute' => 'content',
                'label' => Yii::t('backend','Nội dung chương trình'),
                'headerOptions' => ['style' => 'width:30%'],

            ],
            [
                'attribute' => 'send_time',
                'label' => Yii::t('backend','Thời gian gửi'),
                'filter' => false
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('backend','Trạng thái'),
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'value' => function ($model) {
                    switch ($model->status) {
                        case VtSpam::STATUS_DRAFT:
                            $status = Yii::t('backend', "Tạm");
                            break;
                        case VtSpam::STATUS_IN_PROGRESS:
                            $status = Yii::t('backend', "Đang xử lý");
                            break;
                        case VtSpam::STATUS_FINISH:
                            $status = Yii::t('backend', "Kết thúc");
                            break;
                        default:
                            $status = "";
                            break;
                    };

                    return $status;
                },
                'filter' => false
            ],

            [
                'attribute' => 'channel',
                'label' => Yii::t('backend','Kênh truyền thông'),
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:10%'],
                'value' => function ($model) {
                    switch ($model->channel) {
                        case VtSpam::CHANNEL_ALL:
                            $status = Yii::t('backend', "Tất cả");
                            break;
                        case VtSpam::CHANNEL_APP:
                            $status = Yii::t('backend', "Mobile Client");
                            break;
                        case VtSpam::CHANNEL_SMS:
                            $status = Yii::t('backend', "SMS");
                            break;
                        default:
                            $status = "";
                            break;
                    };

                    return $status;
                },
                'filter' => false
            ],

//            [
//                'attribute' => 'number',
//                'label' => 'Số thuê bao'
//            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
