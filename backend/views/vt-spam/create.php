<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtSpam */

$this->title = Yii::t('app', 'Tạo mới');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chương trình truyền thông'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-spam-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
