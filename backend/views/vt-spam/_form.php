<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use backend\models\VtVideo;
use backend\models\VtPlaylist;
use backend\models\VtSpam;


/* @var $this yii\web\View */
/* @var $model backend\models\VtSpam */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::toRoute(['vt-spam/search']);

$itemName = '';
if (!$model->isNewRecord && !empty($model->item_id)) {

    $video = VtVideo::findOne($model->item_id);
    if($video){
        $itemName = $video->name;
    }

}


$this->registerJs('
    $("#vtspam-item_type").on("select2-selecting", function(e) { 
        alert(this.value);
        $("#vtspam-content").val(this.value);
    });')
?>

<script type="javascript">


</script>

<div class="vt-spam-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'rule')->dropDownList(
            [
                '{"type":1,"packages":[]}' => Yii::t('backend','Thuê bao test'),
                '{"type":2,"packages":[]}' => Yii::t('backend','Thuê bao cài đặt APP')
            ],
            [
                'class' => 'form-control',
                'prompt' => '--- '.Yii::t('backend','Chọn tập thuê bao').' ---'
            ]
        )->label(Yii::t('app', 'Tập thuê bao') . '*'); ?>



    <?= $form->field($model, 'name')->textInput(['maxlength' => 255])->label(Yii::t('app', 'Tên chương trình') . '*'); ?>

    <?= $form->field($model, 'item_type')->dropDownList(
//        \common\libs\VtHelper::getObjectTypeArray(['VOD', 'PLAYLIST']),
        \common\libs\VtHelper::getObjectTypeArray(['VOD']),
        [
            'class' => 'form-control',
            'prompt' => '--- '.Yii::t('backend','Chọn loại').' ---'
        ]
    )->label(Yii::t('app', 'Loại') . '*'); ?>

    <?= $form->field($model, 'item_id')->widget(Select2::classname(), [
        'initValueText' => $itemName,
        'options' => ['placeholder' => Yii::t('backend','Tìm kiếm nội dung').' ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading ...') . "'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term, type:$(\'#vtspam-item_type\').val()}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
        ]

    ])->label(Yii::t('app', 'Nội dung') . '*');
    ?>

    <?= $form->field($model, 'content')->textarea(['maxlength' => 1000])->label(Yii::t('app', 'Nội dung chương trình') . '*'); ?>

    <?= $form->field($model, 'item_group_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'is_active')->checkbox([], false)->label(Yii::t('app', 'Kích hoạt')) ?>

    <?= $form->field($model, 'channel')->dropDownList(
            [
                VtSpam::CHANNEL_ALL => Yii::t('backend','Tất cả'),
                VtSpam::CHANNEL_APP => Yii::t('backend','APP'),
                VtSpam::CHANNEL_SMS => Yii::t('backend','SMS')
            ],
            ['class' => 'form-control', 'prompt' => '--- '.Yii::t('backend','Chọn kênh truyền thông').' ---'])->label(Yii::t('app', 'Kênh truyền thông') . '*'); ?>

    <?= $form->field($model, 'send_time')->widget(DateTimePicker::classname(), [
        'options' => [
            'value' => ($model->send_time) ? $model->send_time : date("Y-m-d H:i:s"),
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'todayHighlight' => true,
            'todayBtn' => true
        ]
    ])->label(Yii::t('app', 'Thời gian gửi') . '*');
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
