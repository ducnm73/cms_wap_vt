<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\libs\S3Service;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\db\VtUserDB;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\VtChannelInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-channel-info-form">
    <!-- <input type="hidden" id="user-change-info-action" value="/vt-channel-info/process"> -->
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => 255])->label(Yii::t('backend', 'Tên channel') . '<span class="text-danger">*</span>') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'maxlength' => 10000])->label(Yii::t('backend', 'Mô tả') . '<span class="text-danger">*</span>') ?>

    <?php $user = VtUserDB::findOne($model->user_id); ?>
    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'initValueText' => $user ? $user->full_name : '',
        'options' => [
            'id' => 'user_id',
            'placeholder' => Yii::t('backend', 'Tên user') . ' ...'
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . "...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/user/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term }; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
        ],
    ])->label(Yii::t('backend', 'Tên user'). '<span class="text-danger">*</span>'); ?>

    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <div>
            <img class="preview-image" src="<?= S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
        </div>
    <?php endif ?>

    <?= $form->field($model, 'channel_path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>
    <label class="control-label color_red"><?= Yii::t('backend', 'Vui lòng chọn ảnh {extension}', ['extension' => 'JPG, PNG', 'size' => '180*180px'])?></label>

    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <div>
            <img class="preview-image" src="<?= S3Service::generateWebUrl($model->channel_bucket, $model->channel_path) ?>"/>
        </div>
    <?php endif ?>

    <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh banner')) ?>
    <label class="control-label color_red"><?= Yii::t('backend', 'Vui lòng chọn ảnh {extension}', ['extension' => 'JPG, PNG', 'size' => '1920*330px'])?></label>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end();?>
</div>
<input type="hidden" id="approve-user-change-info" value="/vt-channel-info/process">