<?php

use yii\helpers\Html;
use backend\assets\UserAsset;
UserAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\VtChannelInfo */

$this->title = Yii::t('backend','Cập nhật thông tin kênh').': ' . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Tool duyệt nhanh thông tin kênh'), 'url' => ['channel-action']];
$this->params['breadcrumbs'][] = Yii::t('backend','Cập nhật thông tin kênh');
?>
<div class="vt-user-change-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_insert', [
        'model' => $model,
    ]) ?>

</div>
