<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\libs\S3Service;

/* @var $this yii\web\View */
/* @var $model backend\models\VtChannelInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-channel-info-form">
<input type="hidden" id="user-change-info-action" value="/vt-channel-info/process">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'channel_id')->textInput(['maxlength' => 11, 'readonly'=> true]) ?>

    <?= $form->field($model, 'status')->textInput(['readonly'=> true, 'value' => $model->getStatusString()]) ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => 255, 'readonly'=> true]) ?>

    <div style="padding-left: 15px;">
        <div class="row">
            <span style="font-size: 14px;"><?= Yii::t('backend','Ảnh avatar:')?> </span>
            <?php if (!$model->isNewRecord && !empty($model->path)): ?>
                <img class="preview-image" src="<?= S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
            <?php else: ?>
                <p><?= Yii::t('backend','(Không có ảnh)')?></p>
            <?php endif ?>
        </div>
        <br>
        <div class="row">
            <span style="font-size: 14px;"><?= Yii::t('backend','Ảnh banner:')?> </span>
            <?php if (!$model->isNewRecord && !empty($model->channel_path)): ?>
                <img class="image-preview-display" style="max-width: 1000px" src="<?= S3Service::generateWebUrl($model->channel_bucket, $model->channel_path) ?>"/>
            <?php else: ?>
                <p><?= Yii::t('backend','(Không có ảnh)')?></p>
            <?php endif ?>
        </div>
    </div>
    <br>
    <div>
        <p><?= Yii::t('backend','Mô tả của Channel')?></p>
        <textarea rows="6" readonly style="width: 100%; resize: none;"><?= $model->description?></textarea>
    </div>

    <div>
        <p><?= Yii::t('backend','Lý do từ chối')?></p>
        <textarea rows="6" readonly style="width: 100%; resize: none;"><?= $model->reason?></textarea>
    </div>

    <?= $form->field($model, 'created_at')->textInput(['readonly'=> true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['readonly'=> true]) ?>

    <div id="processButton">
        <?php
//        if ($model->checkApprovePermission()) {
            $approveBtn = '<button style="width: 115px;" onclick="approveUserChangeInfo(' . $model->id . ', \'approve\')" type="button" class="btn btn-success video-btn-publish">' . Yii::t("backend", "Duyệt") . '</button>';
            $deleteBtn = '<button style="width: 115px;" onclick="declineUserChangeInfo(' . $model->id . ', \'delete\')" type="button" class="btn btn-danger video-btn-publish">' . Yii::t("backend", "Không duyệt") . '</button>';

            switch ($model->status) {
                case 0:
                    echo $approveBtn . $deleteBtn;
                    break;
                case 1:
                    echo $deleteBtn;
                    break;
                case 2:
                    echo $approveBtn;
                    break;
            }
      //  }
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<input type="hidden" id="approve-user-change-info" value="/vt-channel-info/process">
<div id="userChangeInfoModal" class="modal">
    <div class="modal-content">
        <div>
            <span class="close">&times;</span>
            <p><?= Yii::t('backend','Lý do từ chối duyệt:')?></p>
        </div>
        <div>
            <textarea rows="4" id="declineUserChangeInfoReason" style="width: 100%; resize: none;"></textarea>
        </div>
        <input type="hidden" id="hiddenId"/>
        <input type="hidden" id="hiddenAction"/>
        <input type="button" id="reasonDecline" onclick="submitReasonDeclineUserChangeInfo()" value="<?php echo Yii::t('backend', 'Lưu lại')?>" class="btn btn-primary" style="margin: 0 200px;"/>
    </div>
</div>