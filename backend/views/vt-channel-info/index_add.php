<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;
use backend\assets\UserAsset;
UserAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtChannelInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Tool duyệt nhanh thông tin kênh');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-channel-info-index">
    <a style="width: 115px;" href="/vt-channel-info/create-channel" class="btn btn-success"><?= Yii::t("backend", 'Tạo mới') ?></a>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t('backend','ID kênh'),
                'attribute' => 'id',
                'contentOptions' => ['style'=>'width: 120px;'],
            ],
            [
                'label' => Yii::t('backend','Tên channel'),
                'attribute' => 'full_name',
                'contentOptions' => ['style'=>'max-width: 100px; word-break: break-all;'],
            ],
            [
                'label' => Yii::t('backend','Mô tả'),
                'attribute' => 'description',
                'contentOptions' => ['style'=>'max-width: 100px; word-break: break-all;'],
            ],
            [
                'label' => Yii::t('backend','Ảnh avatar'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->channel_bucket , $model->channel_path, VtHelper::SIZE_BANNER), ['class' => 'image-grid-display']);
                }
            ],
            [
                'label' => Yii::t('backend','Ảnh banner'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_AVATAR), ['class' => 'image-grid-display']);
                }
            ],
            // [
            //     'attribute' => 'status',
            //     'format' => 'raw',
            //     'value' => function($model, $key, $index, $widget) {
            //         return $model->getStatusString();
            //     },

            //     'filter' => \common\helpers\Utils::getInterlizationParams('comment.status.dropdown.value', true, 'backend')
            // ],
            [
                'attribute' => Yii::t('backend', 'Thao tác'),
                'format' => 'raw',
                'contentOptions' => ['style'=>'text-align:right; width: 115px;'],
                'value' => function ($model) {
                    $detailBtn = '<a style="width: 115px; background-color: yellowgreen;" href="/vt-channel-info/update-channel?id='. (int)($model->id) .'" target="_blank" class="btn btn-success video-btn-publish">' . Yii::t("backend", "Chi tiết") . '</a>';
                    return $detailBtn;
                },
            ],
        ],
    ]);?>
</div>
<input type="hidden" id="approve-user-change-info" value="/vt-channel-info/process">
<div id="userChangeInfoModal" class="modal">
    <div class="modal-content">
        <div>
            <span class="close">&times;</span>
            <p><?= Yii::t('backend','Lý do từ chối duyệt')?>:</p>
        </div>
        <div>
            <textarea rows="4" id="declineUserChangeInfoReason" style="width: 100%; resize: none;"></textarea>
        </div>
        <input type="hidden" id="hiddenId"/>
        <input type="hidden" id="hiddenAction"/>
        <input type="button" id="reasonDecline" onclick="submitReasonDeclineUserChangeInfoQuick()" value="<?= Yii::t('backend', 'Lưu lại')?>" class="btn btn-primary" style="margin: 0 200px;"/>
    </div>
</div>