<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TargetKpiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="target-kpi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'report_year') ?>

    <?= $form->field($model, 'report_month') ?>

    <?= $form->field($model, 'display_time_app') ?>

    <?= $form->field($model, 'display_time_web') ?>

    <?= $form->field($model, 'success_access_rate_app') ?>

    <?= $form->field($model, 'success_access_rate_web') ?>

    <?= $form->field($model, 'avg_upload_speed') ?>

    <?= $form->field($model, 'error_upload_rate') ?>

    <?= $form->field($model, 'encode_video_time') ?>

    <?= $form->field($model, 'error_encode_rate') ?>

    <?= $form->field($model, 'wait_time') ?>

    <?= $form->field($model, 'buffer_rate') ?>

    <?= $form->field($model, 'buffer_duration_rate') ?>

    <?= $form->field($model, 'buffer_over_rate') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
