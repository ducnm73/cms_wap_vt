<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TargetKpiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Cấu hình KPI mục tiêu');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="target-kpi-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend','Tạo KPI mục tiêu'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t('backend','Năm báo cáo'),
                'attribute' => 'report_year',
            ],
            [
                'label' => Yii::t('backend','Tháng báo cáo'),
                'attribute' => 'report_month',
            ],
            [
                'label' => Yii::t('backend','Thời gian hiển thị trang trên app (giây)'),
                'attribute' => 'display_time_app',
            ],
            [
                'label' => Yii::t('backend','Thời gian hiển thị trang trên web (giây)'),
                'attribute' => 'display_time_web',
            ],
            [
                'label' => Yii::t('backend','Tỉ lệ truy cập thành công trên app (%)'),
                'attribute' => 'success_access_rate_app',
            ],
            [
                'label' => Yii::t('backend','Tỉ lệ truy cập thành công trên web (%)'),
                'attribute' => 'success_access_rate_web',
            ],
            [
                'label' => Yii::t('backend','Tốc độ upload trung bình (Kbps)'),
                'attribute' => 'avg_upload_speed',
            ],
            [
                'label' => Yii::t('backend','Tỷ lệ upload lỗi (%)'),
                'attribute' => 'error_upload_rate',
            ],
            [
                'label' => Yii::t('backend','Thời gian encode video (giây)'),
                'attribute' => 'encode_video_time',
            ],
            [
                'label' => Yii::t('backend','Tỷ lệ encode lỗi (%)'),
                'attribute' => 'error_encode_rate',
            ],
            [
                'label' => Yii::t('backend','Thời gian chờ để bắt đầu xem được video (giây)'),
                'attribute' => 'wait_time',
            ],
            [
                'label' => Yii::t('backend','Tỷ lệ số lần bị buffer khi đang xem video (%)'),
                'attribute' => 'buffer_rate',
            ],
            [
                'label' => Yii::t('backend','Tỷ lệ thời gian bị buffer khi đang xem video (%)'),
                'attribute' => 'buffer_duration_rate',
            ],
            [
                'label' => Yii::t('backend','Tỷ lệ số lần bị buffer vượt quá 3s (%)'),
                'attribute' => 'buffer_over_rate',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
