<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TargetKpi */

$this->title = Yii::t('backend','Cập nhật KPI mục tiêu').': ' . ' '.Yii::t('backend','tháng').' ' . $model->report_month . '/' . $model->report_year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Cấu hình KPI mục tiêu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="target-kpi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
