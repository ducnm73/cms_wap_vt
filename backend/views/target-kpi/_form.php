<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TargetKpi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="target-kpi-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
        $currentYear = date("Y");
        echo $form->field($model, 'report_year')->textInput(['value' => $currentYear, 'readonly'=> true])
    ?>
    <?php
        $listMonth=['1'=>'01', '2'=>'02', '3'=>'03', '4'=>'04', '5'=>'05', '6'=>'06', '7'=>'07', '8'=>'08', '9'=>'09', '10'=>'10', '11'=>'11', '12'=>'12'];
        echo $form->field($model, 'report_month')->dropDownList($listMonth,['prompt'=> Yii::t('backend', 'Chọn tháng')]); ?>

    <?= $form->field($model, 'display_time_app')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'display_time_web')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'success_access_rate_app')->textInput() ?>

    <?= $form->field($model, 'success_access_rate_web')->textInput() ?>

    <?= $form->field($model, 'avg_upload_speed')->textInput() ?>

    <?= $form->field($model, 'error_upload_rate')->textInput() ?>

    <?= $form->field($model, 'encode_video_time')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'error_encode_rate')->textInput() ?>

    <?= $form->field($model, 'wait_time')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'buffer_rate')->textInput() ?>

    <?= $form->field($model, 'buffer_duration_rate')->textInput() ?>

    <?= $form->field($model, 'buffer_over_rate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend','Tạo mới') : Yii::t('backend','Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
