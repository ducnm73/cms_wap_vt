<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TargetKpi */

$this->title = Yii::t('backend','Chi tiết KPI mục tiêu tháng ') . $model->report_month . "/" . $model->report_year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Cấu hình KPI mục tiêu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="target-kpi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend','Bạn có muốn xóa KPI mục tiêu này không?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'report_year',
            'report_month',
            'display_time_app',
            'display_time_web',
            'success_access_rate_app',
            'success_access_rate_web',
            'avg_upload_speed',
            'error_upload_rate',
            'encode_video_time',
            'error_encode_rate',
            'wait_time',
            'buffer_rate',
            'buffer_duration_rate',
            'buffer_over_rate',
        ],
    ]) ?>

</div>
