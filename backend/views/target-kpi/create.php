<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TargetKpi */

$this->title = Yii::t('backend', 'Tạo KPI mục tiêu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Cấu hình KPI mục tiêu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="target-kpi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
