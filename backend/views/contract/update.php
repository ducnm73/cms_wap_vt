<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtContract */

$this->title = 'Update Vt Contract: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vt Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vt-contract-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
