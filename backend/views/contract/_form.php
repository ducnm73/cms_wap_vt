<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtContract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contract_code')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'id_card_number')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'id_card_created_at')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'id_card_created_by')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'id_card_image_frontside')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'id_card_image_backside')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'msisdn')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'payment_type')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'tax_code')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'account_number')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'bank_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'bank_department')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
