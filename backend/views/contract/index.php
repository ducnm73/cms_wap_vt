<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vt Contracts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-contract-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vt Contract', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            [
                'attribute' => 'user_id',
                'label' => 'CP CODE',
                'format' => 'raw',
                'value' => function ($model) {
                    return "USER_UPLOAD_".$model->user_id;
                }
            ],
            'name',
            [
                'attribute' => 'status',
                'filter' => \common\libs\VtHelper::getObjectTypeArray(Yii::$app->params['contract.status'])
            ],
            'email:email',
            'id_card_number',
            // 'id_card_created_at',
            // 'id_card_created_by',
            // 'id_card_image_frontside',
            // 'id_card_image_backside',
            // 'address',
            // 'msisdn',
            // 'payment_type',
            // 'tax_code',
            // 'created_at',
            // 'created_by',
            // 'account_number',
            // 'bank_name',
            // 'bank_department',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
