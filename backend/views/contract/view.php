<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VtContract */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vt Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-contract-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'contract_code',
            'name',
            'email:email',
            'id_card_number',
            'id_card_created_at',
            'id_card_created_by',
            'id_card_image_frontside',
            'id_card_image_backside',
            'address',
            'msisdn',
            'payment_type',
            'tax_code',
            'created_at',
            'created_by',
            'account_number',
            'bank_name',
            'bank_department',
        ],
    ]) ?>

</div>
