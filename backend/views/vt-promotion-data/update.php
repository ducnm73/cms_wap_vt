<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPromotionData */

$this->title = Yii::t('backend', 'Update Vt Promotion Data') . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Promotion Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vt-promotion-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
