<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtPromotionData */

$this->title = Yii::t('backend', 'Create Vt Promotion Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Promotion Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-promotion-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
