<?php

use yii\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend','Import thuê bao khuyến mãi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-promotion-data-index">

    <fieldset class="form-group" style="border: solid 1px #ccc; padding: 10px;">
        <legend><?= Yii::t('backend','IMPORT THUÊ BAO KHUYẾN MÃI')?></legend>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <label for="uploadform-csvfile" class="custom-file-upload" style="padding: 4px; border: 1px solid black;clear: both">
            Upload Image
        </label>
        <input type="file" id="uploadform-csvfile" name="UploadForm[csvFile]" aria-invalid="true" style="display:none;">
        <button class="btn btn-success" style="display: block">IMPORT</button>
        <?php ActiveForm::end() ?>
    </fieldset>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'msisdn',
            [
                'attribute' => 'created_at',
                'value' => 'created_at',

                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'vi',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options'=>['class'=>'form-control']

                ]),
                'format' => 'html',

            ],
            [
                'attribute' => 'expired_at',
                'value' => 'expired_at',
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'expired_at',
                    'language' => 'vi',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options'=>['class'=>'form-control']
                ]),
                'format' => 'html',
            ],
            'code',
            'popup',
        ],
        'pager' => [
            'firstPageLabel' => Yii::t('backend','Trang đầu'),
            'lastPageLabel' => Yii::t('backend','Trang cuối'),
            'maxButtonCount' => 10,
        ],
    ]); ?>

</div>
