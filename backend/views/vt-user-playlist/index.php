<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\VtUserBase;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Quản lý playlist');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-playlist-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]);     ?>

    <p>
        <?=
        Html::a(Yii::t('app', 'Tạo mới {modelClass}', [
                    'modelClass' => Yii::t('app', 'Playlist'),
                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'name',
                'label' => Yii::t('backend','Tên'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['vt-user-playlist/update', 'id' => $model->id]) . '">'
                            . mb_substr(htmlentities($model->name), 0, 50, 'UTF-8') . ((strlen($model->name) > 50) ? '...' : '') . '</a>';
                },
            ],
                [
                'attribute' => 'created_at',
                'label' => Yii::t('backend','Thời gian tạo'),
                'filter' => \kartik\daterange\DateRangePicker::widget([
                    'model' => $searchModel,
                    'name' => 'range_created_at',
                    'attribute' => 'created_at',
                    'value' => date('Y-m-00 H:i:s') . ' - ' . date('Y-m-d H:i:s'),
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'timePicker' => true,
                        'timePicker24Hour' => true,
                        'locale' => ['format' => 'Y-m-d H:i:s']
                    ]
                ])
            ],
                [
                'attribute' => 'num_video',
                'label' => Yii::t('backend','Số lượng content được nhóm'),
            ],
//            [
//                'attribute' => 'user_id',
//                'label' => 'Kênh sở hữu',
//                'value' => function ($model) {
//                    return htmlentities(($model->user_id) ? $model->user->full_name : "(không có)");
//                },
//                'filter' => ArrayHelper::map(VtUserBase ::getAllUser(), 'id', 'full_name')
//            ],
                [
                'attribute' => 'created_by',
                'label' => Yii::t('backend','User tạo'),
                'value' => function ($model) {
                    return ($model->created_by) ? htmlentities($model->auth->username) : Yii::t('backend','không có');
                },
                'filter' => ArrayHelper::map(backend\models\AuthUser::getAllUser(), 'id', 'username')
            ],
                [
                'attribute' => 'source_display',
                'label' => Yii::t('backend','Nguồn hiển thị'),
                'value' => function($model, $key, $index, $widget) {
                    return (array_key_exists($model->source_display, Yii::$app->params['playlist.source.display'])) ? Yii::t('backend', Yii::$app->params['playlist.source.display'][$model->source_display]) : Yii::t('backend','Không xác định');
                },
//                'filter' => Yii::$app->params['playlist.source.display']
                'filter' => [
                    'all' => Yii::t('backend','Tất cả'),
                    'app' => 'App',
                    'web' => 'Web',
                    'wap' => 'Wap'
                ],
            ],
                [
                'attribute' => 'play_times',
                'label' => Yii::t('backend','Lượt xem playlist'),
            ],
                [
                'attribute' => 'is_active',
                'label' => Yii::t('app', 'Kích hoạt'),
                'value' => function($model, $key, $index, $widget) {
                    return ($model->is_active == \backend\models\VtUserPlaylist::ACTIVE) ? Yii::t('backend', 'Kích hoạt') : Yii::t('backend', 'Không kích hoạt');
                },
//                'filter' => Yii::$app->params['active.dropdown.value']
                'filter'  => [
                    '1' => Yii::t('backend','Kích hoạt'),
                    '0' => Yii::t('backend','Không kích hoạt')
                ],
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('app', 'Trạng thái'),
                'value' => function($model, $key, $index, $widget) {
                    return (array_key_exists($model->status, \common\helpers\Utils::getInterlizationParams('playlist.status.dropdown.value', true, 'backend')) ) ? \common\helpers\Utils::getInterlizationParams('playlist.status.dropdown.value', true, 'backend')[$model->status] : Yii::t('backend','Không xác định');
                },
//                'filter' => Yii::$app->params['playlist.status.dropdown.value']
                'filter' =>  Select2::widget([
                    'name' => 'VtUserPlaylistSearch[status]',
                    'model' => $searchModel,
                    'value' => $searchModel->status,
                    'data' => \common\helpers\Utils::getInterlizationParams('playlist.status.dropdown.value', true, 'backend'),//Yii::$app->params['playlist.status.dropdown.value']),
                    'language' => 'vi',
                    'size' => Select2::SMALL,
                    'options' => [
                        'placeholder' => Yii::t('backend','Tất cả'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
            ],
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
                ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
        ],
    ]);
    ?>

    <p>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?=
        Html::submitButton(Yii::t('app', 'Xuất báo cáo {modelClass}', [
                    'modelClass' => Yii::t('app', 'Playlist'),
                ]), ['class' => 'btn btn-success'])
        ?>
        <?php ActiveForm::end(); ?>
    </p>

</div>
