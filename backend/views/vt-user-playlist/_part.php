<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\VtVideo;
?>
<!--<div class="col-md-12" style="overflow-x: auto">-->
<div class="playlist">
    <table class="table table-bordered table-scroll table-striped">
        <thead>
            <tr>
                <th >#</th>
                <th><?= Yii::t('backend','Tên nội dung')?></th>
                <th><?= Yii::t('backend','Kênh sở hữu')?></th>
                <th><?= Yii::t('backend','Trạng thái')?></th>
                <th><?= Yii::t('backend','Thao tác')?></th>
            </tr>
        </thead>
        <tbody id="playlist-part">
            <?php
//        var_dump($parts);
//        die
            ?>
            <?php if (isset($parts)): ?>
                <?php foreach ($parts as $key => $part): ?>
                    <tr class="row-playlist" id="row-playlist-id-<?= $part['id'] ?>" rel="<?= $part['id'] ?>">
                        <th scope="row"><?= $key + 1 ?></th>
                        <td><a href="<?= Url::toRoute(['vt-video/update?id=' . $part['id']]) ?>"  class="part-name" data-type="text" data-pk="<?= $part['id'] ?>"
                               data-url="<?= Url::toRoute(['vt-video/update?id=' . $part['id']]) ?>"
                               data-title="<?= Yii::t('backend','Sửa tên')?>"><?= $part['name'] ?></a></td>
                        <td>
                            <?= (!empty($part['full_name']) ? $part['full_name'] : '('.Yii::t('backend','Không xác định').')') ?>
                        </td>
                        <td>
                            <?php
                            switch ($part['status']) {
                                case VtVideo::STATUS_APPROVE:
                                    echo Yii::t('backend','Phê duyệt');
                                    break;
                                case VtVideo::STATUS_DELETE:
                                    echo Yii::t('backend','Xóa');
                                    break;
                                case VtVideo::STATUS_DRAFT:
                                    if (in_array($part['convert_status'], [VtVideo::CONVERT_STATUS_DRAFT, VtVideo::CONVERT_STATUS_CONVERTING])) {
                                        echo Yii::t('backend','Chờ convert');
                                    } else if (in_array($part['convert_status'], [VtVideo::CONVERT_STATUS_FAIL_CLOUD, VtVideo::CONVERT_STATUS_FAIL_CONVERT, VtVideo::CONVERT_STATUS_FAIL_ORG])) {
                                        echo Yii::t('backend','Convert lỗi');
                                    } else {
                                        echo Yii::t('backend','Chờ duyệt');
                                    }
                                    break;
                                case VtVideo::STATUS_TEMP:
                                    echo Yii::t('backend', 'Tạm');
                                    break;
                                default:
                                    echo Yii::t('backend','Không xác định');
                            }
                            ?>
                        </td>
                        <td class="col-xs-1">
                            <?= Html::button(Yii::t('backend','Xóa'), ['class' => 'btn btn-danger', 'onclick' => 'removeContent("' . $part['id'] . '")']); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>

        </tbody>
    </table>
    <input type="hidden" id="playlistId" value="<?= $model->id ?>" />
    <input type="hidden" id="updatePosition" value="<?= Url::toRoute(['/vt-user-playlist/change']) ?>" />
    <input type="hidden" id="removePlaylist" value="<?= Url::toRoute(['/vt-user-playlist/delete-playlist']) ?>" />
    <input type="hidden" id="addPlaylist" value="<?= Url::toRoute(['/vt-user-playlist/add-playlist']) ?>" />
    <input  type="hidden" id="limitContent" value="<?= Yii::$app->params['playlist.limit.item'] ?>">
    <input  type="hidden" id="finishPlaylist" value="<?= Url::toRoute(['/vt-user-playlist/finish-playlist']) ?>">
</div>

<?php
$this->registerJsFile('/js/playlist.js');
?>
<style>
    .playlist{
        max-height: 500px;
        overflow-y: scroll;
    }
</style>