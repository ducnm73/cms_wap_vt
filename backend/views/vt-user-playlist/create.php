<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */

$this->title = Yii::t('backend','Tạo mới playlist');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Playlist'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-playlist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
