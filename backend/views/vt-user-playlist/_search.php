<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylistSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
Modal::begin([
    'header' => '<h4>'.Yii::t('backend','Chọn nội dung playlist').'</h4>',
    'id' => 'modal',
    'size' => 'modal-lg'
]);
?>
<?php
echo "<div id='modalContent'></div>";
Modal::end();
?>
<script>
    $('#modalButton').click(function () {
        $('#modal').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
        $('.modal-backdrop').hide();
    });
</script>


