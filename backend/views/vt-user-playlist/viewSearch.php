<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\VtUserBase;
use yii\helpers\Url;use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php Pjax::begin(['id' => 'dcm', 'enablePushState' => false, 'timeout' => 10000]); ?>

<div class="playlist-popup">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['action' => Url::toRoute(['vt-user-playlist/view-search', 'id' => $id]), 'method' => 'get', 'options' => ['id' => 'filter-search', 'data-pjax' => true]]); ?>

    <input id="playlistUrl" type="hidden" value="<?= \yii\helpers\Url::toRoute(['vt-user-playlist/view-search']) ?>"/>
    <input id="playlistId" type="hidden" value="<?= $id ?>"/>
    <input id="channelId" type="hidden" value="<?= ($model->user) ? $model->user->id : '' ?>"/>

    <div class="col-md-3">
        <?= Html::activeInput('text', $searchModel,
            'name',
            ['class' => 'form-control', 'placeholder' => '---' . Yii::t('backend', 'Tên Video') . '---']
        ) ?>
    </div>

    <div class="col-md-3">
        <?= Html::button(Yii::t('backend', 'Tìm kiếm'), [
            'id' => 'btn-search',
            'class' => 'btn btn-primary',
            'onclick' => 'searchVideo()'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>


    <?php //\yii\widgets\Pjax::begin(['timeout' => false, 'clientOptions' => ['container' => 'pjax-container'],'id'=>'some_pjax_id']); ?>


    <?= GridView::widget([
//        'id' => 'dcm',
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            return ['id' => 'row-playlist' . $model->id];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend', 'Tên'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['vt-user-playlist/update', 'id' => $model->id]) . '">'
                        . mb_substr(htmlentities($model->name), 0, 50, 'UTF-8') . ((strlen($model->name) > 50) ? '...' : '') . '</a>';
                },
//                'filter' => false
            ],
            [
                'attribute' => 'created_by',
                'label' => Yii::t('backend', 'Kênh sở hữu'),
                'value' => function ($model) {
                    return htmlentities(($model->user) ? $model->user->full_name : '(' . Yii::t('backend', 'không có') . ')');
                },
                'filter' => false
            ],
            [
                'attribute' => 'playlist_id',
                'value' => function ($model) {
                    return $model->id;
                },
                'filter' => false,
                'visible' => false
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{add}', // the default buttons + your custom button
                'buttons' => [
                    'add' => function ($url, $model, $key) { // render your custom button
                        $channel = base64_encode(($model->user) ? $model->user->full_name : Yii::t('backend', "(không có)"));
                        $name = base64_encode($model->name);
                        $status = base64_encode($model->getVideoStatusString());
                        return Html::button(Yii::t('backend', 'Thêm'), ['class' => 'btn btn-primary', 'onclick' => 'addContent("' . $model->id . '","' . $name . '","' . $channel . '","' . $status . '")']);
                    }
                ]
            ]
        ],
    ]);
    ?>

</div>

<?php Pjax::end(); ?>
<?php
//$this->registerJs(
//    '$("document").ready(function(){
//            $("#dcm").on("pjax:end", function() {
//                $.pjax.reload({container:"dcm"});  //Reload GridView
//            });
//        });'
//);
//?>
