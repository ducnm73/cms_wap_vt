<?php

use yii\helpers\Html;
use backend\assets\FineUploaderFilmAsset;
use backend\assets\EditableAsset;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\VtGroupTopic;


FineUploaderFilmAsset::register($this);
EditableAsset::register($this);
?>

    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
    <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
        <div><?php echo Yii::t('backend', 'Upload a file')?></div>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
        <span><?php echo Yii::t('backend', 'Processing dropped files...')?></span>
    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
        <li>
        <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
        <div class="qq-progress-bar-container-selector qq-progress-bar-container">
        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
        </div>
        <div class="qq-thumbnail-wrapper">

        </div>
        <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>

        </div>
        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
        <span class="qq-btn qq-delete-icon" aria-label="<?php echo Yii::t('backend', 'Delete')?>"></span>
        </button>
        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
        <span class="qq-btn qq-pause-icon" aria-label="<?php echo Yii::t('backend', 'Pause')?>"></span>
        </button>
        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
        <span class="qq-btn qq-continue-icon" aria-label="<?php echo Yii::t('backend', 'Continue')?>"></span>
        </button>
        </div>
        </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
        <button type="button" class="qq-cancel-button-selector">Close</button>
        </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
        <button type="button" class="qq-cancel-button-selector"><?= Yii::t('backend', 'No')?></button>
        <button type="button" class="qq-ok-button-selector"><?= Yii::t('backend', 'Yes')?></button>
        </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <input type="text">
        <div class="qq-dialog-buttons">
        <button type="button" class="qq-cancel-button-selector"><?= Yii::t('backend', 'Cancel')?></button>
        </div>
            <button type="button" class="qq-ok-button-selector"><?= Yii::t('backend', 'Ok')?></button>
        </dialog>
        </div>
    </script>

    <input type="hidden" id="update-video-url" value="<?= \yii\helpers\Url::toRoute(['upload/update-film']) ?>"/>
    <input type="hidden" id="upload-url" value="<?= \yii\helpers\Url::toRoute(['upload/index']) ?>"/>
    <input type="hidden" id="upload-type" value="FILM"/>
    <input type="hidden" id="playlist-id" value="<?= $model->id ?>"/>


    <div id="uploader"></div>
    <script>

    </script>

