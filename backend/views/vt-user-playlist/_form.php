<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use common\models\VtUserBase;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */
/* @var $form yii\widgets\ActiveForm */
$url = \yii\helpers\Url::toRoute(['vt-user-playlist/search']);
$itemName = '';
if (!$model->isNewRecord && !empty($model->user_id)) {

    $user = VtUserBase::getById($model->user_id);
    if ($user) {
        $itemName = $user->full_name;
    }
}
?>
<div class="vt-playlist-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'form-playlist-edit', 'enctype' => 'multipart/form-data', 'enableClientScript' => true]]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?=
    $form->field($model, 'user_id')->widget(Select2::classname(), [
        'initValueText' => $itemName,
        'options' => ['placeholder' => Yii::t('backend','Tìm kiếm nội dung').' ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading ...') . "'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
        ]
    ])->label(Yii::t('app', Yii::t('backend','Kênh sở hữu')) . '*');
    ?>

    <?= $form->field($model, 'source_display')->dropDownList(\common\libs\VtHelper::getMappingArray(\common\helpers\Utils::getInterlizationParams('playlist.source.display', true, 'backend')), ['class' => 'form-control'])->label(Yii::t('app', 'Kênh hiển thị ') . '*');

    ?>
    <?php if (!$model->isNewRecord): ?>
        <p>
            <?= Html::button(Yii::t('backend','Thêm mới nội dung playlist'), ['value' => \yii\helpers\Url::toRoute(['vt-user-playlist/view-search', 'id' => $model->id]), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
        </p>
        <?=
        $this->render('_part', [
            'parts' => isset($parts) ? $parts : array(),
            'model' => $model,
        ]);
        ?>
    <?php endif; ?>
    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
    <?php endif ?>
    <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>
    <?= $form->field($model, 'num_video')->hiddenInput()->label(false); ?>

    <div class="form-group">
        <?php if ($model->isNewRecord): ?>
            <?= Html::submitButton(Yii::t('app', 'Lưu'), ['class' => 'btn btn-success', 'id' => 'btn-save', 'name' => 'submitSave', 'value' => 'save']) ?>
            <?= Html::submitButton(Yii::t('app', 'Lưu và thêm mới'), ['class' => 'btn btn-success', 'id' => 'btn-add-save', 'name' => 'submitAddSave', 'value' => 'addSave']) ?>
        <?php else: ?>
            <?= Html::submitButton(Yii::t('app', 'Cập nhật'), ['class' => 'btn btn-success', 'id' => 'btn-update', 'name' => 'submitUpdate', 'value' => 'update']) ?>
            <?php if ($model->is_finish == 0): ?>
                <?= Html::button(Yii::t('backend','Hoàn thành'), ['class' => 'btn btn-primary', 'id' => 'btn-finish', 'onclick' => 'finishContent("' . $model->id . '")']) ?>
            <?php endif ?>
            <?= Html::submitButton(Yii::t('app', 'Xóa'), ['class' => 'btn btn-danger', 'id' => 'btn-remove', 'name' => 'submitDelete', 'value' => 'delete']) ?>
        <?php endif ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?=
    $this->render('_search')
    ?>
</div>


