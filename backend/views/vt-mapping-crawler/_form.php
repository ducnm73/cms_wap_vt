<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use backend\models\VtVideo;
use backend\models\VtPlaylist;
use backend\models\VtGroupCategory;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\VtMappingTopic */
/* @var $form yii\widgets\ActiveForm */

$categoryUrl = \yii\helpers\Url::toRoute(['vt-mapping-crawler/search-category']);
$userUrl = \yii\helpers\Url::toRoute(['/user/search']);
$itemName = '';

if(!empty($model->local_topic_id)){
    $itemName = \backend\models\VtGroupCategory::findOne($model->local_category_id)->name;
}

?>


<div class="vt-mapping-topic-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <?= $form->field($model, 'local_user_id')->widget(Select2::classname(), [
        'initValueText' => $model->getWapUserName(),
        'options' => ['placeholder' => Yii::t('backend','Tìm kiếm kênh').' ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading ...') . "'; }"),
            ],
            'ajax' => [
                'url' => $userUrl,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term }; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }')
        ]

    ])->label(Yii::t('app', 'Kênh UClip'));

    ?>

    <?= $form->field($model, 'local_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\backend\models\VtGroupCategory::getAllActiveCategory(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend', 'Chọn chuyên mục ...')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label(Yii::t('app', 'Chuyên mục'));
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
