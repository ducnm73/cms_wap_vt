<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtMappingTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Map nguồn Crawler');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-slideshow-index" style="word-break: break-word">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'crawler_id',
                'label' => Yii::t('backend','ID Kho'),
                'filter' => false,
            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Crawler Kho'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->name;
                }
            ],
            [
                'attribute' => 'cp_id',
                'label' => Yii::t('backend','CP'),
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->cp) ? $model->cp->name : '';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'name' => 'cp_id',
                    'attribute' => 'cp_id',
                    'data' => ArrayHelper::map(\backend\models\VtCp::getAllCp(), 'id', 'cp_code'),
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Chọn CP'),
                        'multiple' => false,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'showToggleAll' => false,
                ])
            ],
            [
                'attribute' => 'local_user_id',
                'label' => Yii::t('backend','Kênh'),
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->user) ? $model->user->full_name : '';
                },
            ],
            [
                'attribute' => 'local_category_id',
                'label' => Yii::t('backend','Chuyên mục'),
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->category) ? $model->category->name : '';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'name' => 'local_category_id',
                    'attribute' => 'local_category_id',
                    'data' => ArrayHelper::map(\backend\models\VtGroupCategory::getAllActiveCategory(), 'id', 'name'),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Chọn Chuyên mục'),
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'showToggleAll' => false,
                ])
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
