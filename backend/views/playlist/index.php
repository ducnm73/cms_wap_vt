<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\libs\VtHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtPlaylistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Phim');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-playlist-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="float:right">
        <?= Html::a(Yii::t('backend','Thêm mới phim'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <ul class="nav nav-pills">

        <li role="presentation" class="<?= ($state == 'temp')? 'active' : '' ?>"><a href="<?= Url::toRoute(['playlist/index', 'state' => 'temp' ]); ?>"><?= Yii::t('backend','Phim tạm')?></a></li>
        <li role="presentation" class="<?= ($state == 'published')? 'active' : '' ?>"><a href="<?= Url::toRoute(['playlist/index', 'state' => 'published' ]); ?>"><?= Yii::t('backend','Xuất bản')?></a></li>
        <li role="presentation" class="<?= ($state == 'draft')? 'active' : '' ?>"><a href="<?= Url::toRoute(['playlist/index', 'state' => 'draft' ]); ?>"><?= Yii::t('backend','Chờ duyệt')?></a></li>
        <li role="presentation" class="<?= ($state == 'deleted')? 'active' : '' ?>"><a href="<?= Url::toRoute(['playlist/index', 'state' => 'deleted' ]); ?>"><?= Yii::t('backend','Xóa')?></a></li>

    </ul>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Nội dung'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<b>'.Yii::t('backend','Tiêu đề').': </b><a href="' . Url::toRoute(['playlist/update', 'id' => $model->id]) .'">'
                    . Html::encode($model->name)
                    .'</a>'
                    .'<p><b>'.Yii::t('backend','Mô tả').': </b>'
                    . Html::encode($model->description)
                    .'</p>';
                },
            ],

            [
                'attribute' => 'is_active',
                'label' => \Yii::t('app', 'Kích hoạt'),
                'format' => 'raw',
                'value' => function($model, $key, $index, $widget) {
                    return ($model->is_active == \backend\models\VtPlaylist::ACTIVE) ? Yii::t('backend', 'Kích hoạt') : Yii::t('backend', 'Không kích hoạt');
                },
                'filter' => Yii::$app->params['active.dropdown.value']
            ],
            [
                'attribute' => Yii::t('backend','Thông tin'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<p><strong>'.Yii::t('backend','Thể loại').': </strong>'. (($model->category)?htmlentities($model->category->name):'') .'</p>'
                        .'<p><strong>'.Yii::t('backend','Người tạo').': </strong>'. $model->getCreatedByName() .'</p>';
                },
            ],
            [

                'attribute' => 'Image',
                'label' => Yii::t('backend','Ảnh'),
                'format' => 'raw',
                'value' => function ($model) {

                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_FILM), ['class' => 'image-grid-display']);

                }
            ],
        ],
    ]); ?>

</div>
