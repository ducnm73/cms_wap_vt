<?php

use yii\helpers\Url;
use common\libs\S3Service;
?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th><?= Yii::t('backend','Tên tập')?></th>
            <th><?= Yii::t('backend','Video')?></th>
            <th><?= Yii::t('backend','Trạng thái')?></th>

        </tr>
    </thead>
    <tbody id="film-part">

        <?php if (isset($parts)): ?>
            <?php foreach ($parts as $key => $part): ?>
                <tr>
                    <th scope="row"><?= $key + 1 ?></th>
                    <td><a href="#" class="part-name" data-type="text" data-pk="<?= $part['item_id'] ?>"
                           data-url="<?= Url::toRoute(['upload/update-video-editable']) ?>"
                           data-title="<?= Yii::t('backend','Sửa tên')?>"><?= $part->video->name ?></a></td>
                    <td>
                        <video width="320" height="240" controls>
                            <source src="<?= S3Service::generateStreamURL($part->video->getConvertVideo(), S3Service::OBJCDN) ?>" type="video/mp4">
                            <?= Yii::t('backend','Vui lòng chọn trình duyệt hỗ trợ HTML5')?>
                        </video>
                    </td>
                    <td><?= Yii::t('backend', $part->video->getVideoStatusString()) ?></td>
<!--                    --><?php //var_dump($part->video);die; ?>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>

    </tbody>
</table>