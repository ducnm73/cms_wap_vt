<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */

$this->title = Yii::t('backend','Tạo mới phim');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Phim'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-playlist-create">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="vt-playlist-form">
	    <?= $this->render('_playlist_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
