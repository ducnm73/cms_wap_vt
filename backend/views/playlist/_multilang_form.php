<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;

?>

<div class="multilang-form">
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form', 'action' => Url::to('/playlist/update-multilang')]); ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
    <div class="panel panel-default">
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $multilangModels[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'lang',
                    'name',
                    'description',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($multilangModels as $i => $multilangModel): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <!-- <h3 class="panel-title pull-left">Address</h3> -->
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <?= $form->field($multilangModel, "[{$i}]lang")->widget(Select2::className(), [
                                    'data' => Yii::$app->params['all_languages'],
                                    'options' => ['placeholder' => Yii::t('backend','Chọn ngôn ngữ')],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-sm-9">
                                <?= $form->field($multilangModel, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->field($multilangModel, "[{$i}]description")->textArea(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend','Cập nhật'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>