<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\VtGroupTopic;
use backend\models\VtGroupCategory;
use backend\models\VtPackage;


/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-playlist-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(VtGroupCategory::getAllActiveCategory(\common\models\VtGroupCategoryBase::TYPE_FILM), 'id', 'name'),
        ['prompt'=>'--- '.Yii::t('backend','Chọn thể loại').' ---']) ?>

    <?= $form->field($model, 'topics')->checkboxList(ArrayHelper::map(VtGroupTopic::getAllActiveTopic(VtGroupTopic::TYPE_FILM), 'id', 'name'), [
        'class' => 'form-control topic-checkbox-list',
        'prompt'=>'--- '.Yii::t('backend','Chọn chủ đề').' ---'
    ]); ?>

    <?= $form->field($model, 'suggest_package_id')->dropDownList(ArrayHelper::map(VtPackage::getListPackage(), 'id', 'name'), ['class' => 'form-control package-checkbox-list', 'prompt'=>'--- '.Yii::t('backend','Chọn gói cước quảng cáo').' ---']); ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'is_hot')->checkbox() ?>

    <?= $form->field($model, 'is_recommend')->checkbox() ?>

    <?php if(!$model->isNewRecord && !empty($model->path)):?>
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
    <?php endif ?>
    <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', Yii::t('backend','Ảnh đại diện'))) ?>

    <?php if(!$model->isNewRecord):?>
        <?= $this->render('_upload', [
            'model' => $model,
        ]) ?>
    <?php endif;?>
    <?php if(isset($parts)):?>
        <?= $this->render('_part', [
            'parts' => $parts,
            'model' => $model,
        ]) ?>
    <?php endif;?>

    <div class="form-group">
        <?php if(!$model->isApprove() || $model->checkApprovePermission()): ?>
            <?= Html::submitButton(Yii::t('backend','Lưu'), ['class' => 'btn btn-success', 'name' => 'submitSave', 'value' => 'save']) ?>

            <?php if(!$model->isApprove() && !$model->isDraft()): ?>
                <?= Html::submitButton(Yii::t('backend','Gửi duyệt'), ['class' => 'btn btn-primary', 'name' => 'submitApprove', 'value' => 'send_approve']) ?>
            <?php endif;?>

            <?php if(!$model->isApprove()):?>
                <?php if($model->checkApprovePermission()): ?>
                    <?= Html::submitButton(Yii::t('backend','Duyệt'), ['class' => 'btn btn-primary', 'name' => 'submitPublish', 'value' => 'publish']) ?>
                <?php endif;?>
            <?php endif;?>

            <?php if(!$model->isNewRecord):?>
                <?php if(($model->isOwner() || $model->checkApprovePermission()) && !$model->isDelete()): ?>
                    <?= Html::submitButton(Yii::t('backend','Xóa'), ['class' => 'btn btn-danger', 'name' => 'submitDelete', 'value' => 'delete']) ?>
                <?php endif;?>
            <?php endif;?>

        <?php endif;?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
