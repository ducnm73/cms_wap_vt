<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vt Playlists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-playlist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'name',
            'description:ntext',
            'status',
            'category_id',
            'attributes',
            'price_play',
            'reason',
            'priority',
            'approved_by',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'bucket',
            'path',
            'is_hot',
            'is_recommend',
            'like_count',
        ],
    ]) ?>

</div>
