<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */

$this->title = Yii::t('app','Cập nhật phim: ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Cập nhật');
?>
<div class="vt-playlist-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'parts' => $parts,
        'multilangModels' => $multilangModels
    ]) ?>

</div>
