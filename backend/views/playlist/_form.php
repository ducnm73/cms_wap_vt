<?php

use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model backend\models\VtPlaylist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-playlist-form">
    <?= Tabs::widget([
        'options' => ['id' => 'playlist-form-tabs'],
        'items' => [
            [
                'label' => Yii::t('backend','Thông tin bộ phim'),
                'encode' => false,
                'content' => $this->render('_playlist_form', ['model' => $model, 'parts' => $parts]),
                'active' => true
            ],
            [
                'label' => Yii::t('backend','Đa ngôn ngữ'),
                'encode' => false,
                'content' => $this->render('_multilang_form', ['model' => $model, 'multilangModels' => $multilangModels, 'lang' => 'en']),
            ],
        ],
    ]); ?>

</div>