<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtShortLinkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Link Ngắn');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-short-link-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend','Tạo link ngắn'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'short_link',
            'route',
            'params',
            'full_link',
            [
                'attribute' => 'is_active',
                'label' => \Yii::t('app', 'Kích hoạt'),
                'format' => 'raw',
                'value' => function($model, $key, $index, $widget) {
                    return ($model->is_active == \backend\models\VtPlaylist::ACTIVE) ? Yii::t('backend', 'Kích hoạt') : Yii::t('backend', 'Không kích hoạt');
                },
                'filter'  => [
                    '1' => Yii::t('backend','Kích hoạt'),
                    '0' => Yii::t('backend','Không kích hoạt')
                ],
            ],
            'other_info',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}']
        ],
    ]); ?>

</div>
