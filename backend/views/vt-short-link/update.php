<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtShortLink */

$this->title = Yii::t('backend','Cập nhật').': ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Link ngắn'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend','Cập nhật');
?>
<div class="vt-short-link-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
