<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtShortLink */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-short-link-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'short_link')->textInput(['maxlength' => 255, 'placeholder'=>'/abc'])->label(Yii::t('app', 'Short_link') . '*') ?>

    <?= $form->field($model, 'full_link')->textInput(['maxlength' => 1000, 'placeholder'=>'http://meuclip.co.mz/abc']) ?>

    <?= $form->field($model, 'route')->dropDownList(
        \common\helpers\Utils::getInterlizationParams('shortlink.route','true','backend'),
        [
            'class' => 'form-control',
            'prompt' => '--- '.Yii::t('backend','Chọn loại').' ---'
        ]
    )->label(Yii::t('app', 'Loại link') . '*'); ?>

    <?= $form->field($model, 'params')->textInput(['maxlength' => 1000, 'placeholder'=>'id=8888&utm_source=GA&popup=1']) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'other_info')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
