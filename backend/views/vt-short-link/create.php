<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtShortLink */

$this->title = Yii::t('backend','Tạo link ngắn');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Link ngắn'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-short-link-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
