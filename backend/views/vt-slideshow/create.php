<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtSlideshow */

$this->title = Yii::t('app', 'Tạo mới {modelClass}', [
    'modelClass' => 'Slideshow',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Slideshow'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-slideshow-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
