<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtSlideshowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Slideshow');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-slideshow-index">

    <p>
        <?= Html::a(Yii::t('app', 'Tạo mới {modelClass}', [
    'modelClass' => 'Slideshow',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Tên'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['vt-slideshow/update', 'id' => $model->id]) .'">'
                    . mb_substr(htmlentities($model->name), 0,50,'UTF-8') .((strlen($model->name)>50)?'...':'') .'</a>';
                },
            ],
            [
                'attribute' => 'type',
                'filter' => VtHelper::getObjectTypeArray(Yii::$app->params['slideshow.object.type'])
            ],
            [

                'attribute' => 'Image',
                'label' => Yii::t('backend','Ảnh'),
                'format' => 'raw',
                'value' => function ($model) {

                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_COVER), ['class' => 'image-grid-display']);

                }
            ],


#            'item_id',
#            'position',
            [
                'attribute' => 'location',
                'filter' => VtHelper::getObjectTypeArray(Yii::$app->params['slideshow.location'])
            ],
            [
                'attribute' => 'is_active',
                'label' => \Yii::t('app', 'Kích hoạt'),
                'value' => function($model, $key, $index, $widget) {
                    return ($model->is_active == \backend\models\VtSlideshow::ACTIVE) ? Yii::t('backend', 'Kích hoạt') : Yii::t('backend', 'Không kích hoạt');
                },
                'filter' => \common\helpers\Utils::getInterlizationParams('active.dropdown.value', true, 'backend')
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
