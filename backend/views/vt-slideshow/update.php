<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtSlideshow */

$this->title = Yii::t('app', 'Cập nhật {modelClass}: ', [
    'modelClass' => 'Slideshow',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Slideshows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Cập nhật');
?>
<div class="vt-slideshow-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
