<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use backend\models\VtVideo;
use backend\models\VtPlaylist;

/* @var $this yii\web\View */
/* @var $model backend\models\VtSlideshow */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::toRoute(['vt-slideshow/search']);

$itemName = '';
//var_dump($model);die;
if (!$model->isNewRecord && !empty($model->item_id)) {
    if ($model->type == 'VOD') {
        $temp = VtVideo::findOne($model->item_id);
        if(!is_null($temp)) {
            $itemName = $temp->name;
        }
    } else if ($model->type == 'FILM') {
        $temp = VtPlaylist::findOne($model->item_id);
        if(!is_null($temp)) {
            $itemName = $temp->name;
        }
    } else if ($model->type == 'CATEGORY') {
        $temp = \backend\models\VtGroupCategory::findOne($model->item_id);
        if(!is_null($temp)) {
            $itemName = $temp->name;
        }
    } else if ($model->type == 'TOPIC') {
        $temp = \backend\models\VtGroupTopic::findOne($model->item_id);
        if(!is_null($temp)) {
            $itemName = $temp->name;
        }
    }
}

$this->registerJs('
    function toggleItem(value){
        if(value == "VOD" || value == "FILM" || value == "CATEGORY" || value == "TOPIC"  ){
            $(".field-vtslideshow-item_id").show();
            $(".field-vtslideshow-href").hide();
        }else if(value == "HREF"){
            $(".field-vtslideshow-item_id").hide();
            $(".field-vtslideshow-href").show();
        }
    }
    toggleItem($("#vtslideshow-type").val());

    $("#vtslideshow-type").change(function(){
        var value = this.value;
        toggleItem(value);
    });')
?>

<script type="javascript">


</script>

<div class="vt-slideshow-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'type')->dropDownList(
        \common\libs\VtHelper::getObjectTypeArray(Yii::$app->params['slideshow.object.type']),
        [
            'class' => 'form-control',
            'prompt' => '--- '.Yii::t('backend','Chọn loại').' ---'
        ]
    )->label(Yii::t('backend', 'Loại') . '*'); ?>

    <?= $form->field($model, 'item_id')->widget(Select2::classname(), [
        'initValueText' => $itemName,
        'options' => ['placeholder' => Yii::t('backend','Tìm kiếm nội dung').' ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Loading ...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term, type:$(\'#vtslideshow-type\').val()}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }')
        ]

    ])->label(Yii::t('app', 'Nội dung'));
    ?>

    <?= $form->field($model, 'href')->textInput(['maxlength' => 500]) ?>

    <?= $form->field($model, 'is_active')->checkbox([], false)->label(Yii::t('app', 'Kích hoạt')) ?>

    <?= $form->field($model, 'location')->dropDownList(\common\libs\VtHelper::getObjectTypeArray(Yii::$app->params['slideshow.location']), ['class' => 'form-control', 'prompt' => '--- ' . Yii::t('backend', 'Chọn vị trí hiện thị') . ' ---'])->label(Yii::t('app', 'Vị trí') . '*'); ?>

    <?= $form->field($model, 'begin_time')->widget(DateTimePicker::classname(), [
        'options' => [
            'value' => ($model->begin_time) ? $model->begin_time : date("Y-m-d H:i:s"),
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'todayHighlight' => true,
            'todayBtn' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'end_time')->widget(DateTimePicker::classname(), [
        'options' => [
            'value' => ($model->end_time) ? $model->end_time : date("Y-m-d H:i:s"),
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'todayHighlight' => true,
            'todayBtn' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => 10]) ?>

    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
    <?php endif ?>
    <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
