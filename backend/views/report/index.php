<?php
use yii\data\Pagination;

?>

<div>
    <h4 style="text-align: center;"><b>REPORT</b></h4>
    <div>
        <form method="get" action="/report/index">

            <br/>
            From:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'from_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($fromTime)
            ])
            ?>
            To:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'to_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($toTime)
            ])
            ?>

            <input type="submit" name="action" value="REPORT"/>
            <!--            <input type="submit" name="action" value="EXPORT"/>-->

        </form>
    </div>

    <?php if (count($contents) > 0): ?>
        <?php $colspan = count($contents) + 1; ?>
        <?php $rowDate = ''; ?>
        <?php $actions = []; ?>

        <?php foreach ($contents as $key => $content): ?>
            <?php
            foreach ($content as $keyAction => $contentByAction) {
                $actions[] = $keyAction;
                foreach ($contentByAction as $keyBySource => $contentBySource) {
                    ${$keyAction}['source'][] = $keyBySource;
                }
            }
            ?>
        <?php endforeach; ?>


        <?php foreach ($contents as $key => $content): ?>
            <?php
            $rowDate .= '<th class="cell-key">' . $key . "</td>\n";

            foreach (array_unique($actions) as $action) {

                foreach (array_unique(${$action}['source']) as $source) {
                    if (!isset(${$action . $source})) {
                        ${$action . $source} = '';
                    }

                    if (!array_key_exists($action, $content)) {
                        ${$action . $source} .= '<td class="cell-content">0</td>';
                    } else {
                        if (!array_key_exists($source, $content[$action])) {
                            ${$action . $source} .= '<td class="cell-content">0</td>';
                        } else {
                            ${$action . $source} .= '<td class="cell-content">' . Yii::$app->formatter->asInteger($content[$action][$source]['fee']) . "</td>\n";
                        }
                    }
                }
            }
            ?>
        <?php endforeach; ?>

        <div style="width:100%;overflow-x: scroll">
            <table style="table-layout: auto;" class="table-report table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">

                <thead>
                <tr>
                    <th><strong><?php echo Yii::t('backend','Ngày') ?></strong></th>
                    <?php echo $rowDate ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach (array_unique($actions) as $action): ?>
                    <tr>
                        <th colspan="<?php echo $colspan ?>">
                            <strong style="color: blue"><?php echo Yii::t('backend','Thống kê').' ' . Yii::$app->params['mapping.action'][$action] ?></strong>
                        </th>
                    </tr>
                    <?php foreach (array_unique(${$action}['source']) as $source): ?>
                        <tr>
                            <th><?= Yii::t('backend','Doanh thu').' ' . $source ?></th>
                            <?php echo ${$action . $source} ?>
                        </tr>
                    <?php endforeach; ?>

                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>


</div>
