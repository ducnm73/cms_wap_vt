<?php
use yii\data\Pagination;

?>

<div>
    <h4 style="text-align: center;"><b>REPORT</b></h4>
    <div>
        <form method="get" action="/report/unregister">

            <br/>
            From:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'from_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($fromTime)
            ])
            ?>
            To:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'to_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($toTime)
            ])
            ?>

            <input type="submit" name="action" value="REPORT"/>
            <!--            <input type="submit" name="action" value="EXPORT"/>-->

        </form>
    </div>

    <?php if (count($contents) > 0): ?>
        <?php $colspan = count($contents) + 1; ?>
        <?php $rowDate = ''; ?>
        <?php $keyDateArrays = [] ?>
        <?php $keySourceArrays = [] ?>

        <?php foreach ($contents as $key => $content): ?>
            <?php

                foreach ($content as $keyContent => $contentByContent) {
                    $keyDateArrays[] = $keyContent;
                    foreach ($contentByContent as $keyBySource => $contentBySource) {
                        $keySourceArrays[] = $keyBySource;
                    }
                }

                $keyDateArrays = array_unique($keyDateArrays);
                $keySourceArrays = array_unique($keySourceArrays);

            ?>

        <?php endforeach; ?>
        <div style="width:100%;overflow-x: scroll">
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">

                <thead>
                <tr>
                    <th><strong><?php echo Yii::t('backend','Ngày') ?></strong></th>
                    <th><strong><?php echo Yii::t('backend','Loại gói cước') ?></strong></th>
                    <?php foreach($keySourceArrays as $keySource): ?>

                        <th><strong><?php echo Yii::$app->params['mapping.source'][$keySource] ?></strong></th>

                    <?php endforeach;?>
                </tr>
                </thead>
                <tbody>

                    <?php foreach ($contents as $key => $content): ?>

                        <?php $i = 0;?>
                        <?php foreach($keyDateArrays as $keyDate):?>


                            <tr>
                                <?php if($i % 3 == 0):?>

                                    <td rowspan="3"><?php echo $key ?></td>

                                <?php endif; ?>

                                <td><?php echo Yii::$app->params['mapping.content'][$keyDate] ?></td>

                                <?php foreach($keySourceArrays as $keySource): ?>

                                    <td><strong><?php echo array_key_exists($keyDate, $content) ? (array_key_exists($keySource, $content[$keyDate]) ? $content[$keyDate][$keySource]['distinct_msisdn'] : 0) : 0 ?></strong></td>

                                <?php endforeach;?>
                            </tr>

                            <?php $i++;?>
                        <?php endforeach;?>


                    <?php endforeach;?>


                </tbody>
            </table>
        </div>
    <?php endif; ?>


</div>
