<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use backend\models\VtCp;
use kartik\date\DatePicker;
?>

<div>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo chia sẻ doanh thu')?></b></h4>
    <div class="row">

        <form method="get" action="/report/report-cp">

            <div class="col-md-3">

                <?=
                    DatePicker::widget([
                        'name' => 'from_time',
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => date('Y-m-d', strtotime($fromTime)),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                    ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                    DatePicker::widget([
                        'name' => 'to_time',
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => date('Y-m-d', strtotime($toTime)),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                    ]);
                ?>

            </div>

            <div class="col-md-3">


                <?= \kartik\select2\Select2::widget([
                    'name' => 'cp_id',
                    'value' => $cpId, // initial value
                    'data' => ArrayHelper::map(VtCp::getAllCp(), 'id', 'name'),
                    'maintainOrder' => true,
                    'options' => ['placeholder' => Yii::t('backend','Tất cả đối tác').' ...', 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tags' => true,
                        'maximumInputLength' => 10
                    ],
                ]);
                ?>
            </div>

            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>
        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
                <tr>
                    <th><strong>#</strong></th>
                    <th><strong><?= Yii::t('backend','Đối tác') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Lượt xem') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Doanh thu (VNĐ)') ?></strong></th>
                </tr>
            </thead>
            <?php if (count($contentsByCp) > 0): ?>
                <tbody>
                    <?php $i = 0; ?>

                    <?php foreach($contentsByCp as $content):?>
                        <?php if(array_key_exists('view_count', $content) && array_key_exists('revenue', $content)):?>

                            <?php $i++ ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $content['cp'] ?></td>
                                <td class="cell-content"><?= Yii::$app->formatter->asInteger($content['view_count']) ?></td>
                                <td class="cell-content"><?= Yii::$app->formatter->asInteger($content['revenue']) ?></td>
                            </tr>

                        <?php endif; ?>

                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="5" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
