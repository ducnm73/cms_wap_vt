<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use backend\models\VtCp;
use kartik\date\DatePicker;
?>

<div>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo chi tiết Video')?></b></h4>
    <div class="row">
        <form method="get" action="/report/report-cp-video">

            <div class="col-md-3">

                <?=
                DatePicker::widget([
                    'name' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                ]);
                ?>

            </div>

            <div class="col-md-3">


                <?= \kartik\select2\Select2::widget([
                    'name' => 'cp_id',
                    'value' => $cpId, // initial value
                    'data' => ArrayHelper::map(VtCp::getAllCp(), 'id', 'name'),
                    'maintainOrder' => true,
                    'options' => ['placeholder' => Yii::t('backend','Tất cả đối tác').' ...', 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tags' => true,
                        'maximumInputLength' => 10
                    ],
                ]);
                ?>
            </div>

            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>

        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
                <tr>
                    <th width="5%"><strong>#</strong></th>
                    <th width="45%"><strong><?= Yii::t('backend','Nội dung') ?></strong></th>
                    <th width="10%"><strong><?= Yii::t('backend','Loại') ?></strong></th>
                    <th width="10%"><strong><?= Yii::t('backend','Kênh') ?></strong></th>
                    <th width="15%"><strong><?= Yii::t('backend','Lượt xem') ?></strong></th>
                    <th width="15%"><strong><?= Yii::t('backend','Doanh thu (VNĐ)') ?></strong></th>
                </tr>
            </thead>
            <?php if (count($videos) > 0): ?>
                <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($videos as $key => $value):?>
                        <?php $i++ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= (ArrayHelper::keyExists($key, $videoDescriptions)) ? $videoDescriptions[$key]['name']:$key  ?></td>
                            <td><?= (ArrayHelper::keyExists($key, $videoDescriptions)) ? $videoDescriptions[$key]['type']:'' ?></td>
                            <td><?= (ArrayHelper::keyExists($key, $videoDescriptions)) ? $videoDescriptions[$key]['channel']:'' ?></td>
                            <td class="cell-content"><?= $value ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($totalRevenue*$value/$totalView) ?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="5" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
