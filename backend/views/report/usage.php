<?php
use yii\data\Pagination;

?>

<div>
    <h4 style="text-align: center;"><b>REPORT</b></h4>
    <div>
        <form method="get" action="/report/usage">

            <br/>
            From:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'from_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($fromTime)
            ])
            ?>
            To:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'to_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($toTime)
            ])
            ?>

            <input type="submit" name="action" value="REPORT"/>
<!--            <input type="submit" name="action" value="EXPORT"/>-->

        </form>
    </div>

        <?php if(count($contents) > 0):?>
            <?php $colspan = count($contents) + 1;?>
            <?php
                $rowDate = '<th><strong>'.Yii::t('backend','Ngày').'</strong></th>';
                $log_device_distinct_user_id = '<th><strong>'.Yii::t('backend','Số user truy cập').'</strong></th>';
                $log_device_distinct_msisdn = '<th><strong>'.Yii::t('backend','Số thuê bao Unitel truy cập').'</strong></th>';
                $log_vod_distinct_user_id = '<th><strong>'.Yii::t('backend','Số user xem VOD').'</strong></th>';
                $log_vod_distinct_msisdn = '<th><strong>'.Yii::t('backend','Số thuê bao Unitel xem VOD').'</strong></th>';
                $log_vod_distinct_content_id = '<th><strong>'.Yii::t('backend','Tổng nội dung xem').'</strong></th>';
            ?>

            <?php foreach($contents as $key => $content):?>
                <?php
                    $rowDate .= '<th class="cell-key">' . $key . "</th>\n";
                    $log_device_distinct_user_id .= '<td class="cell-content">' . (array_key_exists('log_device_distinct_user_id', $content) ? Yii::$app->formatter->asInteger($content['log_device_distinct_user_id']):'N/A') . "</td>\n";
                    $log_device_distinct_msisdn .= '<td class="cell-content">' . (array_key_exists('log_device_distinct_msisdn', $content) ? Yii::$app->formatter->asInteger($content['log_device_distinct_msisdn']):'N/A') . "</td>\n";
                    $log_vod_distinct_user_id .= '<td class="cell-content">' . (array_key_exists('log_vod_distinct_user_id', $content) ? Yii::$app->formatter->asInteger($content['log_vod_distinct_user_id']):'N/A') . "</td>\n";
                    $log_vod_distinct_msisdn .= '<td class="cell-content">' . (array_key_exists('log_vod_distinct_msisdn', $content) ? Yii::$app->formatter->asInteger($content['log_vod_distinct_msisdn']):'N/A') . "</td>\n";
                    $log_vod_distinct_content_id .= '<td class="cell-content">' . (array_key_exists('log_vod_distinct_content_id', $content) ? Yii::$app->formatter->asInteger($content['log_vod_distinct_content_id']):'N/A') . "</td>\n";
                ?>
            <?php endforeach;?>
        <div style="width:100%;overflow-x: scroll">
                <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid">

                    <tbody>
                        <tr><?php echo $rowDate ?></tr>
                        <tr><?php echo $log_device_distinct_user_id ?></tr>
                        <tr><?php echo $log_device_distinct_msisdn ?></tr>
                        <tr><?php echo $log_vod_distinct_user_id ?></tr>
                        <tr><?php echo $log_vod_distinct_msisdn ?></tr>
                        <tr><?php echo $log_vod_distinct_content_id ?></tr>
                    </tbody>
                </table>
        </div>
        <?php endif;?>


</div>
