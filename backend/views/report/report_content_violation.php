<?php
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use backend\assets\ContentViolationAsset;
ContentViolationAsset::register($this);
?>

<div>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo vi phạm content')?></b></h4>

    <div class="row" >
        <div class="col-md-6">
            <form method="get" id="content-violation-form" action="/report/report-content-violation">
                <div class="row">
                    <div class="col-md-6">
                        <?=
                        DatePicker::widget([
                            'name' => 'from_time',
                            'id' => 'from_time',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'value' => date('Y-m-d', strtotime($fromTime)),
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ],
                            'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                        DatePicker::widget([
                            'name' => 'to_time',
                            'id' => 'to_time',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'value' => date('Y-m-d', strtotime($toTime)),
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ],
                            'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row feebackType">
                    <div class="col-md-6">
                        <?= \kartik\select2\Select2::widget([
                            'name' => 'feedback_type_id',
                            'id' => 'feedback_type_id',
                            'value' => $feedbackTypeId, // initial value
                            'data' => ArrayHelper::map(Yii::$app->params['setting.feedback.all'], 'id', 'content'),
                            'maintainOrder' => true,
                            'options' => ['placeholder' => Yii::t('backend','Loại report'), 'multiple' => false,],
                            'pluginOptions' => [
                                'tags' => true,
                                'maximumInputLength' => 10,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-1">
                        <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6 feedbackGeneral">
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">

                <thead>
                <tr style="background-color: #9db5dc;">
                    <th colspan="2"><strong><?= Yii::t('backend','Tổng hợp')?></strong></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="color: blue"><?=Yii::t('backend','Loại report') ?></td>
                    <td style="color: blue"><?= Yii::t('backend','Tổng số lượng report') ?></td>
                </tr>
                <?php if (count($feedBackGeneral) > 0): ?>
                <?php
                $allFeedback = ArrayHelper::map(Yii::$app->params['setting.feedback.all'], 'id', 'content');
                ?>
                <?php foreach($feedBackGeneral as $fbg):?>
                    <tr>
                        <td><?= $allFeedback[$fbg['request_type']] ?></td>
                        <td><?= $fbg['numOfRecord'] ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
                <?php else:?>
                    <tbody>
                    <tr>
                        <td colspan="2" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <div id="table_area">
        <div style="width:100%" >
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">
                <thead>
                <tr>
                    <th><strong><?= Yii::t('backend','')?>STT</strong></th>
                    <th><strong><?= Yii::t('backend','Loại report') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Tên video') ?></strong></th>
                    <th><strong><?= Yii::t('backend','ID Video') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Tổng số lượng report') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Danh sách chi tiết') ?></strong></th>
                </tr>
                </thead>
                <?php if (count($feedBack) > 0): ?>
                    <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($feedBack as $fb):?>
                        <?php $i++ ?>
                        <?php $k = $pagination->offset + $i ?>
                        <tr>
                            <td><?= $k ?></td>
                            <td><?= $allFeedback[$fb['request_type']] ?></td>
                            <td><?= $fb['videoName'] ?></td>
                            <td><?= $fb['item_id'] ?></td>
                            <td><?= $fb['numOfRecord'] ?></td>
                            <td><?= Html::a(Yii::t('backend','Xuất danh sách'), 'export-list?request_type='. $fb['request_type']
                                    .'&item_id='. $fb['item_id'] .'&from_date='. $fromTime .'&to_time='. $toTime, [ "target"=>"_blank", 'style' => 'color: green;']) ?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                <?php else:?>
                    <tbody>
                    <tr>
                        <td colspan="6" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
        <div class="row">
            <div class="col-md-10">
                <?= LinkPager::widget(['pagination' => $pagination, 'firstPageLabel' => 'First', 'lastPageLabel'  => 'Last']) ?>
            </div>
            <div class="col-md-2">
                <input type="button" id="view-all" class="btn btn-success" style="float: right; margin-top: 10px;" value="<?= Yii::t('backend','Xem tất cả')?>">
            </div>
        </div>
    </div>
</div>