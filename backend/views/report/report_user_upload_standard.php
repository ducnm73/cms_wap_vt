<?php
use kartik\date\DatePicker;
?>

<div>
    <?php
        $this->title = Yii::t('backend','Báo cáo user upload');
    ?>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo user upload')?></b></h4>
    <div class="row">
        <form method="get" id="user-upload-standard-form" action="/report/report-user-upload-standard">
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'from_time',
                    'id' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'readonly' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'id' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'readonly' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                ]);
                ?>

            </div>

            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>
            <div class="col-md-3">
                <button class="btn btn-primary exportUserUploadStandard" style="float: right;">Xuất file</button>
            </div>
        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">
            <thead>
                <tr class="headerTable">
                    <td><?= Yii::t('backend','Ngày')?></td>
                    <td><?= Yii::t('backend','Tổng video khách hàng upload được duyệt từ 0h00 đến 23h59')?></td>
                    <td><?= Yii::t('backend','Tổng video khách hàng upload bị từ chối từ 0h00 đến 23h59')?></td>
                    <td><?= Yii::t('backend','Video duyệt quá thời gian theo KPI (8h-23h)')?></td>
                    <td><?= Yii::t('backend','Số lượng user có hợp đồng upload video trong ngày')?></td>
                    <td><?= Yii::t('backend','Số lượng user không có hợp đồng upload video trong ngày')?></td>
                    <td><?= Yii::t('backend','Số lượng video upload trong ngày của user có hợp đồng')?></td>
                    <td><?= Yii::t('backend','Số lượng video upload trong ngày của user không có hợp đồng')?></td>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($finalArray)) {
                    foreach ($finalArray as $key => $value):
                        ?>
                        <tr>
                            <td style="width: 70px;"><?= $key ?></td>
                            <td class="align-right"><?= $value['totalOfApprovedVideo'] ?></td>
                            <td class="align-right"><?= $value['totalOfDeclinedVideo'] ?></td>
                            <td class="align-right"><?= $value['overdueApprovedVideoByKPI'] ?></td>
                            <td class="align-right"><?= $value['numOfContractedUser'] ?></td>
                            <td class="align-right"><?= $value['numOfUncontractedUser'] ?></td>
                            <td class="align-right"><?= $value['numOfContractedUploadedVideoInDay'] ?></td>
                            <td class="align-right"><?= $value['numOfUncontractedUploadedVideoInDay'] ?></td>
                        </tr>
                    <?php endforeach;
                } else {
                    ?>
                    <tr>
                        <td colspan="8"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>