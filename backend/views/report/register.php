<?php
use yii\data\Pagination;

?>

<div>
    <h4 style="text-align: center;"><b>REPORT</b></h4>
    <div>
        <form method="get" action="/report/register">

            <br/>
            From:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'from_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($fromTime)
            ])
            ?>
            To:
            <?=
            yii\jui\DatePicker::widget([
                'name' => 'to_time',
                'dateFormat' => 'php:Y-m-d',
                'language' => 'en',
                'value' => \yii\helpers\Html::encode($toTime)
            ])
            ?>

            <input type="submit" name="action" value="REPORT"/>
            <!--            <input type="submit" name="action" value="EXPORT"/>-->

        </form>
    </div>

    <?php if (count($contents) > 0): ?>
        <?php $colspan = count($contents) + 2; ?>
        <?php $rowDate = '<th>'.Yii::t('backend','Loại').'</th>'; ?>
        <?php $sources = []; ?>

        <?php foreach ($contents as $key => $content): ?>
            <?php
            foreach ($content as $keySource => $contentBySource) {
                $sources[] = $keySource;
                foreach ($contentBySource as $keyByPackage => $contentByPackage) {
                    ${$keySource}[] = $keyByPackage;

                    foreach($contentByPackage as $keyByPromotion => $contentByPromotion){
                        ${$keySource.$keyByPackage}[] = $keyByPromotion;
                    }
                }
            }
            ?>
        <?php endforeach; ?>

        <?php foreach ($contents as $key => $content): ?>
            <?php
            $rowDate .= '<th>' . $key . "</th>\n";

            foreach (array_unique($sources) as $source) {

                foreach (array_unique(${$source}) as $package) {

                    foreach (array_unique(${$source.$package}) as $promotion) {

                        if (!isset(${$source . $package . $promotion})) {
                            ${$source . $package . $promotion} = '';
                        }

                        if(!isset(${$source . $package . $promotion . 'reg_count'})){
                            ${$source . $package . $promotion . 'reg_count'} = '';
                        }

                        if(!isset(${$source . $package . $promotion . 'reg_fee'})){
                            ${$source . $package . $promotion . 'reg_fee'} = '';
                        }

                        if(!isset(${$source . $package . $promotion . 'charge_fee'})){
                            ${$source . $package . $promotion . 'charge_fee'} = '';
                        }


                        if (!array_key_exists($source, $content)) {
                            ${$source . $package . $promotion . 'reg_count'} .= '<td class="cell-content">0</td>';
                            ${$source . $package . $promotion . 'reg_fee'} .= '<td class="cell-content">0</td>';
                            ${$source . $package . $promotion . 'charge_fee'} .= '<td class="cell-content">0</td>';
                        } else {
                            if(!array_key_exists($package, $content[$source])) {
                                ${$source . $package . $promotion . 'reg_count'} .= '<td class="cell-content">0</td>';
                                ${$source . $package . $promotion . 'reg_fee'} .= '<td class="cell-content">0</td>';
                                ${$source . $package . $promotion . 'charge_fee'} .= '<td class="cell-content">0</td>';
                            }else{
                                if (!array_key_exists($promotion, $content[$source][$package])) {
                                    ${$source . $package . $promotion . 'reg_count'} .= '<td class="cell-content">0</td>';
                                    ${$source . $package . $promotion . 'reg_fee'} .= '<td class="cell-content">0</td>';
                                    ${$source . $package . $promotion . 'charge_fee'} .= '<td class="cell-content">0</td>';
                                } else {
                                    ${$source . $package . $promotion . 'reg_count'} .= '<td class="cell-content">' . Yii::$app->formatter->asInteger($content[$source][$package][$promotion]['reg']['count']) . "</td>\n";
                                    ${$source . $package . $promotion . 'reg_fee'} .= '<td class="cell-content">' . Yii::$app->formatter->asInteger($content[$source][$package][$promotion]['reg']['fee']) . "</td>\n";
                                    ${$source . $package . $promotion . 'charge_fee'} .= '<td class="cell-content">' . Yii::$app->formatter->asInteger($content[$source][$package][$promotion]['charge']['fee']) . "</td>\n";
                                }
                            }
                        }
                    }
                }
            }
            ?>
        <?php endforeach; ?>

        <div style="width:100%;overflow-x: scroll">
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">
            <thead>
            <tr>
                <th><strong><?php echo Yii::t('backend','Ngày') ?></strong></th>
                <?php echo $rowDate ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach (array_unique($sources) as $source): ?>
                <tr>
                    <td colspan="<?php echo $colspan ?>" >
                        <strong><?php echo Yii::t('backend','Thống kê').' ' . Yii::$app->params['mapping.source'][$source] ?></strong>
                    </td>
                </tr>
                <?php foreach (array_unique(${$source}) as $package): ?>

                    <tr>
                        <td colspan="<?php echo $colspan ?>">
                            <strong><?php echo Yii::$app->params['mapping.content'][$package] ?></strong>
                        </td>
                    </tr>

                    <?php foreach(array_unique(${$source.$package}) as $promotion):?>



                        <tr>
                            <th class="cell-key" rowspan="3">
                                <strong><?php echo strtoupper($promotion) ?></strong>
                            </th>

                            <th class="cell-key"><?= Yii::t('backend','Số lượng đăng ký')?></th>
                            <?php echo isset(${$source . $package . $promotion . 'reg_count'})? ${$source . $package . $promotion . 'reg_count'} : 0 ?>
                        </tr>

                        <tr>
                            <th class="cell-key"><?= Yii::t('backend','Doanh thu đăng ký')?></th>
                            <?php echo isset(${$source . $package . $promotion . 'reg_fee'})? ${$source . $package . $promotion . 'reg_fee'} : 0 ?>
                        </tr>

                        <tr>
                            <th class="cell-key"><?= Yii::t('backend','Doanh thu trừ cước')?></th>
                            <?php echo isset(${$source . $package . $promotion . 'charge_fee'})? ${$source . $package . $promotion . 'charge_fee'} : 0?>
                        </tr>



                    <?php endforeach;?>

                <?php endforeach; ?>

            <?php endforeach; ?>
            </tbody>
        </table>
        </div>
    <?php endif; ?>


</div>
