<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div>
    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">
            <thead>
                <tr>
                    <th><strong>STT</strong></th>
                    <th><strong><?= Yii::t('backend','Loại report') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Tên video') ?></strong></th>
                    <th><strong><?= Yii::t('backend','ID Video') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Tổng số lượng report') ?></strong></th>
                    <th><strong><?= Yii::t('backend','Danh sách chi tiết') ?></strong></th>
                </tr>
            </thead>
            <?php if (count($feedBack) > 0): ?>
                <?php
                $allFeedback = ArrayHelper::map(Yii::$app->params['setting.feedback.all'], 'id', 'content');
                ?>
                <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($feedBack as $fb):?>
                    <?php $i++ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $allFeedback[$fb['request_type']] ?></td>
                            <td><?= $fb['videoName'] ?></td>
                            <td><?= $fb['item_id'] ?></td>
                            <td><?= $fb['numOfRecord'] ?></td>
                            <td><?= Html::a(Yii::t('backend','Xuất danh sách'), 'export-list?request_type='. $fb['request_type']
                                    .'&item_id='. $fb['item_id'] .'&from_date='. $fromTime .'&to_time='. $toTime, [ "target"=>"_blank", 'style' => 'color: green;']) ?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="6" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>
    <div>
        <?= Html::a(Yii::t('backend','Thu gọn'), 'javascript:void(0)', ['onclick'=> 'document.getElementById("content-violation-form").submit()'  ,  'class' => 'btn btn-success', 'style' => 'float: right;']) ?>
    </div>
    <div style="clear: both"></div>
</div>