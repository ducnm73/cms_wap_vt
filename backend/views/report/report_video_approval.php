<?php
use kartik\date\DatePicker;
use yii\widgets\LinkPager;
?>
<div>
    <?php
    $this->title = Yii::t('backend','Báo cáo thống kê duyệt video');
    ?>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo thống kê duyệt video')?></b></h4>
    <div class="row" >
        <div class="col-md-6">
            <form method="get" id="video-approval-form" action="/report/report-video-approval">
                <div class="row">
                    <div class="col-md-6">
                        <?=
                        DatePicker::widget([
                            'name' => 'from_time',
                            'id' => 'from_time',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'value' => date('Y-m-d', strtotime($fromTime)),
                            'readonly' => true,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ],
                            'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                        DatePicker::widget([
                            'name' => 'to_time',
                            'id' => 'to_time',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'value' => date('Y-m-d', strtotime($toTime)),
                            'readonly' => true,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ],
                            'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row feebackType">
                    <div class="col-md-6">
                        <?= \kartik\select2\Select2::widget([
                            'name' => 'reason_id',
                            'id' => 'reason_id',
                            'value' => $reasonId, // initial value
                            'data' => Yii::$app->params['reason.unapprove.new'],
                            'maintainOrder' => true,
                            'options' => ['placeholder' => Yii::t('backend','Lý do từ chối'), 'multiple' => false,],
                            'pluginOptions' => [
                                'tags' => true,
                                'maximumInputLength' => 10,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-1">
                        <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6 feedbackGeneral">
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">

                <thead>
                <tr style="background-color: #9db5dc;">
                    <th colspan="2"><strong><?= Yii::t('backend','Tổng hợp')?></strong></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="color: blue"><?= Yii::t('backend','Phân loại') ?></td>
                    <td style="color: blue"><?= Yii::t('backend','Số lượng') ?></td>
                </tr>
                <?php if (count($reasonSummary) > 0): ?>
                <?php foreach($reasonSummary as $key=>$value):?>
                    <tr>
                        <td><?= $key ?></td>
                        <td><?= $value ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
                <?php else:?>
                    <tbody>
                    <tr>
                        <td colspan="2" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <div id="table_area">
        <div style="width:100%" >
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">
                <thead>
                    <tr>
                        <th class="align-center"><?= Yii::t('backend','STT')?></th>
                        <th class="align-center"><strong><?= Yii::t('backend','Ngày')?></strong></th>
                        <th class="align-center"><strong><?= Yii::t('backend','Loại')?></strong></th>
                        <th class="align-center"><strong><?= Yii::t('backend','Tổng video')?></strong></th>
                        <th class="align-center"><strong><?= Yii::t('backend','Tổng số video duyệt')?></strong></th>
                        <th class="align-center"><strong><?= Yii::t('backend','Tổng số video từ chối')?></strong></th>
                    </tr>
                </thead>
                <?php if (count($video) > 0): ?>
                    <tbody>
                    <?php
                    $check=array();
                    $k = 0;
                    foreach($video as $row){
                        $k++;
                        ?><?php
                        if(in_array($row['date'],$check)){
                            $row['date']="";
                        }
                        ?>
                        <tr>
                        <td align="center"><?= $pagination->offset + $k ?></td>
                        <td align="center"><?= $row['date']?></td>
                        <td><?php
                            if($row['type'] == 1){
                            echo "CP Upload";
                            } else if($row['type'] == 2){
                            echo "User Upload";
                            }
                            ?>
                        </td>
                        <td align="right"><?= $row['totalVideo'] != '' ? $row['totalVideo'] : 0 ?></td>
                        <td align="right"><?= $row['numOfAcceptedVideo'] != '' ?  $row['numOfAcceptedVideo'] : 0 ?></td>
                        <td align="right"><?= $row['numOfDeclinedVideo'] != '' ?  $row['numOfDeclinedVideo'] : 0 ?></td>
                        </tr>
                    <?php
                        $check[]=$row['date'];
                    }
                    ?>
                    </tbody>
                <?php else :?>
                    <tbody>
                        <tr>
                            <td colspan="6" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                        </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
        <div class="row">
            <div class="col-md-10">
                <?= LinkPager::widget(['pagination' => $pagination, 'firstPageLabel' => 'First', 'lastPageLabel'  => 'Last']) ?>
            </div>
        </div>
    </div>
</div>