<?php
use kartik\date\DatePicker;
use yii\widgets\LinkPager;
?>
<div>
    <?php
    $this->title = Yii::t('backend','Báo cáo thống kê duyệt bình luận');
    ?>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo thống kê duyệt bình luận')?></b></h4>
    <div class="row" >
        <div class="col-md-6">
            <form method="get" id="comment-approval-form" action="/report/report-comment-approval">
                <div class="row">
                    <div class="col-md-6">
                        <?=
                        DatePicker::widget([
                            'name' => 'from_time',
                            'id' => 'from_time',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'value' => date('Y-m-d', strtotime($fromTime)),
                            'readonly' => true,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ],
                            'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                        DatePicker::widget([
                            'name' => 'to_time',
                            'id' => 'to_time',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'value' => date('Y-m-d', strtotime($toTime)),
                            'readonly' => true,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ],
                            'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row feebackType">
                    <div class="col-md-6">
                        <?= \kartik\select2\Select2::widget([
                            'name' => 'reason_id',
                            'id' => 'reason_id',
                            'value' => $reasonId, // initial value
                            'data' => Yii::$app->params['setting.comment.decline'],
                            'maintainOrder' => true,
                            'options' => ['placeholder' => Yii::t('backend','Lý do từ chối'), 'multiple' => false,],
                            'pluginOptions' => [
                                'tags' => true,
                                'maximumInputLength' => 10,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-1">
                        <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6 feedbackGeneral">
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">

                <thead>
                    <tr style="background-color: #9db5dc;">
                        <th colspan="2"><strong><?= Yii::t('backend','Tổng hợp')?></strong></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="color: blue"><?= Yii::t('backend','Phân loại') ?></td>
                        <td style="color: blue"><?= Yii::t('backend','Số lượng') ?></td>
                    </tr>
                <?php if (count($reasonSummary) > 0): ?>
                    <?php foreach($reasonSummary as $rs):?>
                        <tr>
                            <td><?= $allReason[$rs['reason']] ?></td>
                            <td><?= $rs['numOfRecord'] ?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
                <?php else:?>
                    <tbody>
                        <tr>
                            <td colspan="2" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                        </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <div id="table_area">
        <div style="width:100%" >
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">
                <thead>
                <tr>
                    <th><strong><?= Yii::t('backend','STT')?></strong></th>
                    <th><strong><?= Yii::t('backend','Ngày')?></strong></th>
                    <th><strong><?= Yii::t('backend','Tổng comment')?></strong></th>
                    <th><strong><?= Yii::t('backend','Số lượng chờ duyệt')?></strong></th>
                    <th><strong><?= Yii::t('backend','Số lượng duyệt')?></strong></th>
                    <th><strong><?= Yii::t('backend','Tổng số lượng từ chối')?></strong></th>
                    <th><strong><?= Yii::t('backend','Thời gian duyệt dưới 5 phút')?></strong></th>
                </tr>
                </thead>
                <?php if (count($comment) > 0): ?>
                    <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($comment as $cmt):?>
                        <?php $i++ ?>
                        <?php $no = $pagination->offset + $i ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $cmt['date'] ?></td>
                            <td><?= $cmt['totalComment'] != '' ? $cmt['totalComment'] : 0 ?></td>
                            <td><?= $cmt['waitingComment'] != '' ? $cmt['waitingComment'] : 0 ?></td>
                            <td><?= $cmt['approvedComment'] != '' ? $cmt['approvedComment'] : 0 ?></td>
                            <td><?= $cmt['declinedComment'] != '' ? $cmt['declinedComment'] : 0 ?></td>
                            <td><?= $cmt['below5MinApproval'] != '' ? $cmt['below5MinApproval'] : 0 ?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                <?php else:?>
                    <tbody>
                    <tr>
                        <td colspan="7" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
        <div class="row">
            <div class="col-md-10">
                <?= LinkPager::widget(['pagination' => $pagination, 'firstPageLabel' => 'First', 'lastPageLabel'  => 'Last']) ?>
            </div>
        </div>
    </div>
</div>