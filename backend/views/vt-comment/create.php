<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtComment */

$this->title = Yii::t('backend', 'Create Vt Comment');
$this->params['breadcrumbs'][] = ['label' => 'Vt Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
