<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\assets\CommentAsset;
CommentAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Vt Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-comment-index" style="word-break: break-word">
    <input type="hidden" id="approve-comment-action" value="/vt-comment/approve">
    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    $statusValue = $searchModel->status;
    $comment_status_dropdown_value = Yii::$app->params['comment.status.dropdown.value'];
    for ($i = 0; $i < count($comment_status_dropdown_value); $i++) {
        $comment_status_dropdown_value[$i] = Yii::t('backend', $comment_status_dropdown_value[$i]);
    }

    $setting_comment_decline = Yii::$app->params['setting.comment.decline'];
    for ($i = 1; $i < count($setting_comment_decline) + 1; $i++) {
        $setting_comment_decline[$i] = Yii::t('wap', $setting_comment_decline[$i]);
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            'id',
            [
                'attribute' => 'comment',
                'label' => \Yii::t('app', 'Bình luận'),
                'format' => 'raw',
                'value' => function ($model, $key, $index, $widget) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['vt-comment/update', 'id' => $model->id, 'home-url' => base64_encode(Yii::$app->request->url)]) .'">'. Html::encode($model->comment) .'</a>';
                }
            ],
            'type',
            [
                'attribute' => 'created_at',
                'label' => Yii::t('backend','Tạo lúc'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'filter' => \kartik\daterange\DateRangePicker::widget([
                    'model'=>$searchModel,
                    'name'=>'range_created_at',
                    'attribute' => 'created_at',
                    'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
                    'convertFormat'=>true,
//                    'readonly' => true,
                    'pluginOptions'=>[
                        'opens' => 'left',
                        'alwaysShowCalendars' => true,
                        'timePicker'=>true,
                        'timePicker24Hour' => true,
                        'locale'=>['format'=>'Y-m-d H:i:s']
                    ],
                    'options' => [
                        'class'=>'form-control',
                        'placeholder' => "---" . Yii::t('backend', 'Thời gian tạo') . "---"
                    ],
                ])
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('backend','Phê duyệt lúc'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->updated_at;
                },
                'filter' => \kartik\daterange\DateRangePicker::widget([
                    'model'=>$searchModel,
                    'name'=>'range_updated_at',
                    'attribute' => 'updated_at',
                    'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'timePicker'=>true,
                        'timePicker24Hour' => true,
                        'locale'=>['format'=>'Y-m-d H:i:s']
                    ],
                    'options' => [
                        'class'=>'form-control',
                        'placeholder' => '---'.Yii::t('backend','Thời gian phê duyệt').'---'
                    ]
                ])
            ],
            'content',
            [
                'attribute' => 'status',
                'label' => \Yii::t('app', 'Trạng thái'),
                'format' => 'raw',
                'value' => function ($model, $key, $index, $widget) {
                    if ($model->status == \backend\models\VtComment::WAIT_APPROVE) return Yii::t('backend', 'Chờ duyệt');
                    if ($model->status == \backend\models\VtComment::ACTIVE) return Yii::t('backend', 'Đã duyệt');
                    if ($model->status == \backend\models\VtComment::DISAPPROVE) return Yii::t('backend', 'Không duyệt');
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', $comment_status_dropdown_value,['class'=>'commentStatus form-control','prompt' => '']),
            ],
            [
                'attribute' => 'reason',
                'label' => \Yii::t('app', 'Lý do'),
                'value' => function ($model) {
                    $allReason = Yii::$app->params['setting.comment.decline'];
                    for ($i = 1; $i < count($allReason) + 1; $i++) {
                        $allReason[$i] = Yii::t('wap', $allReason[$i]);
                    }
                    $reason = $model->reason;
                    if($reason != ''){
                        return $allReason[$reason];
                    } else {
                        return "";
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'reason', $setting_comment_decline,['class'=>'form-control','prompt' => '']),
                'visible' => $statusValue == 2 ? true : false,
            ],
            [
                'attribute' => 'user_id',
                'label' => Yii::t('backend','Tài khoản'),
                'format' => 'raw',
                'value' => function ($model) {
                    if (isset($model->user)) {
                        return
                            '<strong>' . ((isset($model->user) && !empty($model->user->full_name)) ? htmlentities($model->user->full_name) : $model->user->msisdn) . '</strong>';
                    } else {
                        return '';
                    }

                },
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $searchModel->getCreatedByName(),
                    'options' => ['placeholder' => "---" . Yii::t('backend','Người tạo') . "---"],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading ...') . "'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term }; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(result) { return result.text; }'),
                        'templateSelection' => new JsExpression('function (result) { return result.text; }')
                    ]
                ])
            ],
            [
                'attribute' => 'approve_by',
                'label' => Yii::t('backend','Duyệt bởi'),
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:15%'],

                'value' => function ($model) {
                    return ($model->auth_user)?$model->auth_user->username:'';
                },
                'filter' => Html::activeDropDownList($searchModel, 'approve_by', ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username'),['class'=>'form-control','prompt' => '']),
            ],
            [
                'attribute' => Yii::t('backend','Thao tác'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->checkApprovePermission()) {
                        $approveBtn = '<button style="width: 115px;" onclick="approveComment(' . $model->id . ', \'approve\')" type="button" class="btn btn-success video-btn-publish">'. Yii::t("backend", "Duyệt") .'</button>';
                        $deleteBtn = '<button style="width: 115px;" onclick="declineComment(' . $model->id . ', \'delete\')" id="declineCommentBtn" type="button" class="btn btn-danger video-btn-publish">'. Yii::t("backend", "Không duyệt") .'</button>';

                        switch ($model->status) {
                            case 0:
                                return $approveBtn . $deleteBtn;
                            case 1:
                                return $deleteBtn;
                            case 2:
                                return $approveBtn;
                        }
                    }
                },
            ],
        ],
    ]); ?>

</div>
<div id="myModal" class="modal fade in">
    <!-- Modal content -->
    <div class="modal-content">
        <div>
            <span class="close">&times;</span>
            <p><?= Yii::t('backend','Lý do không duyệt bình luận:')?></p>
        </div>
        <div>
            <?php
            $allReason = Yii::$app->params['setting.comment.decline'];
            ?>
            <ul class="list-group">
                <?php
                    foreach($allReason as $key=>$value):
                ?>
                    <label for="<?=$key?>" style="width: 500px; font-size: 13px; margin-bottom: -1px;">
                        <li class="list-group-item">
                            <input type="radio" id="<?=$key?>" name="reasonNum" value="<?=$key?>">
                            <?= Yii::t('wap', $value)?>
                        </li>
                    </label>
                <?php endforeach;?>
            </ul>
        </div>
        <input type="hidden" id="hiddenId"/>
        <input type="hidden" id="hiddenAction"/>
        <input type="button" id="reasonDecline" onclick="submitReasonDecline()" value="<?= Yii::t('backend','Lưu lại')?>" class="btn btn-primary" style="margin: 0 200px;"/>
    </div>
    <!-- Trigger/Open The Modal -->
    <button id="myBtn">Open Modal</button>

    <!-- The Modal -->
</div>


<div id="reason" class="modal fade in" role="dialog" tabindex="-1" aria-hidden="true" >
    <div class="modal-dialog" role="document" style="width: 400px" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= Yii::t('backend','Thông báo')?></h4>
            </div>
            <div class="modal-body">
                <p class="modal-text"><?= Yii::t('backend','Bạn phải chọn 1 lý do từ chối duyệt!')?></p>
            </div>
            <div class="modal-footer footer-button" style="border-top: 1px solid #e5e5e5!important;">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('backend','Cancel') ?></button>
            </div>
        </div>
    </div>
</div>
