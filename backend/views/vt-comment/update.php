<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtComment */

$this->title = Yii::t('backend','Cập nhật bình luận').': ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Quản lý bình luận'), 'url' => !empty($homeUrl)? base64_decode($homeUrl):['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend','Cập nhật');
?>
<div class="vt-comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
