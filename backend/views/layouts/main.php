<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\widgets\LayoutMenu;
use backend\helpers\MenuHelper;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\models\LanguageConvert;

use backend\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <!--[if IE 8]>
    <html lang="<?= Yii::$app->language ?>" class="ie8 no-js"> <![endif]-->
    <!--[if IE 9]>
    <html lang="<?= Yii::$app->language ?>" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
    <html lang="<?= Yii::$app->language ?>" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <!--        <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
        <meta http-equiv="X-UA-Compatible" content="IE=11; IE=10; IE=9; IE=8; IE=7; IE=EDGE"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-md page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
    <?php $this->beginBody() ?>
    <!-- BEGIN HEADER -->
    <div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo" style="padding-right: 0;">
                <a href="/">
                    <h4 class="logo-default" style="color: white; font-size: 18px; font-weight: bold;width: 86px">
                        <?= Yii::t('backend','MeuClip')?>
                    </h4>
                </a>
                <div class="page-sidebar sidebar-toggler-container">
                    <div class="sidebar-toggler">
                    </div>
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->

            <!-- Bat dau chon ngon ngu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                            <img alt="" class="img-circle" src="/img/avatar3_small.png"/>
                                <span class="username username-hide-on-mobile">
                                    <?php echo Yii::t('backend','Chọn ngôn ngữ'); ?>
                                </span>
                            <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-default">
                            <!-- <li class="changlanguage">
                                <a url="/site/changlanguage?lang=vi">
                                    <i class="icon-layers"></i>  <?= Yii::t('backend','Việt Nam')?> </a>
                            </li>
                            <li class="divider"></li> -->
                            <li class="changlanguage">
                                <a url="/site/changlanguage?lang=en">
                                    <i class="icon-layers"></i>  <?= Yii::t('backend','Tiếng Anh')?> </a>
                            </li>
                            <li class="divider"></li>
                            <li class="changlanguage">
                                <a url="/site/changlanguage?lang=mz">
                                    <i class="icon-layers"></i>  <?= Yii::t('backend','Portuguese')?> </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- Ket thuc Chon ngon ngu -->

            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                            <img alt="" class="img-circle" src="/img/avatar3_small.png"/>
                            <span class="username username-hide-on-mobile">
                                    <?php echo (!Yii::$app->user->isGuest) ? Html::encode(Yii::$app->user->identity->username) : Yii::t('backend','Guest'); ?>
                                </span>
                            <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-default">
                            <?php if (!\Yii::$app->user->isGuest) { ?>
                                <li>
                                    <a href="/user/change?id=<?php echo Yii::$app->user->identity->id; ?>">
                                        <i class="icon-user"></i>  <?= Yii::t('backend','My Profile')?> </a>
                                </li>
                            <?php } ?>
                            <li class="divider"></li>
                            <li>
                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <a href="/logout">
                                        <i class="icon-key"></i> <?= Yii::t('backend','Log Out')?>
                                    </a>
                                <?php } else { ?>
                                    <a href="/login">
                                        <i class="icon-key"></i>  <?= Yii::t('backend','Sign In')?>
                                    </a>
                                <?php } ?>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN LEFT MENU -->
        <div class="page-sidebar-wrapper">
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <ul class="page-sidebar-menu page-sidebar-menu-closed page-sidebar-menu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true"
                    data-slide-speed="200">
                    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                    <!--                    <li class="sidebar-toggler-wrapper">-->
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <!--                        <div class="sidebar-toggler">-->
                    <!--                        </div>-->
                    <!-- END SIDEBAR TOGGLER BUTTON -->
                    <!--                    </li>-->
                    <?php
                    if (!Yii::$app->user->isGuest) {
                        $callback = function ($menu) {
                            //var_dump($menu['name']);die;
                            //$mainLanguage = Yii::$app->params['mainLanguage'];//lo: dang fix trong common/param_local la LO
                            //$sesionLanguage = Yii::$app->session->get('lang', );
							$sesionLanguage = Yii::$app->session->get('lang', Yii::$app->params['mainLanguage']);
                            //var_dump($sesionLanguage);die;
                            $menu_name = '';
                            //if($menu['name'] == 'Import thuê bao khuyến mãi') die('xxxxxxxxx');
                            if ($sesionLanguage == 'vi') {
                                $menu_name = $menu['name'];
                            }
                            else {
                                $multilang = $menu['multilang'];
                                $multiLangFieldPart = LanguageConvert::convertFieldsToArray($multilang,['name'],$sesionLanguage, $menu);
                                $menu_name = $multiLangFieldPart['name'];
                            }

                            //var_dump();die;
                            if ($menu['route'])
                                return [
                                    'label' => $menu_name,
                                    'url' => [$menu['route']],
                                    'items' => $menu['children'],
                                    'visible' => $menu['is_active'],
                                    'icon' => $menu['icon'],
                                    'parent' => $menu['parent']
                                ];
                            return [
                                'label' => $menu_name,
                                'items' => $menu['children'],
                                'visible' => $menu['is_active'],
                                'icon' => $menu['icon'],
                                'parent' => $menu['parent']
                            ];
                        };
                        //var_dump($callback); die;
                        $items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback);
                        //var_dump($items); die;
                        echo LayoutMenu::widget([
                            'items' => $items,
                        ]);
                    }
                    ?>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END LEFT MENU -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <?=
                    Breadcrumbs::widget([
                        'itemTemplate' => "<li>{link}<i class='fa fa-angle-right'></i></i></li>\n", // template for all links
                        'activeItemTemplate' => "<li>{link}</li>\n", // template for all links
                        'options' => [
                            'class' => 'page-breadcrumb'
                        ],
                        'homeLink' => [
                            'label' => Yii::t('backend', 'Home'),
                            'url' => Yii::$app->homeUrl,
                            'template' => "<li><i class='fa fa-home'></i>{link}<i class='fa fa-angle-right'></i></li>\n",
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                </div>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner">
                <?= Yii::t('backend','@UClip')?>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->
    <?php $this->endBody() ?>
    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
        });
    </script>

    <SCRIPT LANGUAGE="JavaScript">

        var currentLanguage = '<?= Yii::$app->language ?>';
        var arrLang = {
            'en': {
                'Hoàn thành xử lý':'Complete processing',
                'Bạn phải nhập "Lý do từ chối"!':'You must enter "Reason for rejection"!',
                'Bạn phải nhập lý do từ chối!':'You must enter Reason for rejection!',
                '"Lý do từ chối" không vượt quá 255 kí tự!':'"Reason for rejection" does not exceed 255 characters!',
                'Bạn có chắc chắn muốn xóa banner không?':'Are you sure you want to delete the banner?',
                'Bạn có chắc chắn muốn xóa avatar không?':'Are you sure you want to delete the avatar?',
                "Lý do từ chối duyệt lại' không vượt quá 1000 kí tự!":"'Reason for refusing to review' does not exceed 1000 characters!",
                "Bạn có đồng ý không duyệt?":"You refuse to approve?",
                "Bạn có đồng ý phê duyệt lại?":"Do you agree to re-approve?",
                "Bạn có đồng ý convert lại?":"Do you agree to convert?",
                "Bạn có đồng ý xóa?":"Do you agree to delete?",
                "Bạn có đồng ý phê duyệt?":"Do you agree to approve?",
                "Yêu cầu nhập nội dung nháp":"Request draft content",
                "Yêu cầu chọn số thuê bao nhận tin":"Request to select the number of subscribers",
                "Bạn có chắc chắn bỏ trống cấu hình?":"Are you sure you want to empty the configuration?",
                "Bạn có đồng ý hạ nội dung?":"Do you agree to lower the content?",
                "Cập nhật hoàn thành thất bại":"Update completed failed",
                "Xóa video khỏi playlist thất bại":"Delete the video from the failed playlist",
                "Nội dung thêm vào vượt quá ngưỡng":"Additional content exceeds the threshold",
                "cho phép!":"allow!",
                "Sửa tên":"Edit name",
                "Thêm video vào playlist thất bại":"Add video to playlist failed",
                "Bạn có muốn xem lướt?":"Do you want to review",
                "Xóa":"Delete",
                'Giá không được nhỏ hơn 0':"Price play must be no less than 0'",
            },
            'vi': {

            },
            'mz': {
                'Hoàn thành xử lý':'Processamento completo',
                'Bạn phải nhập "Lý do từ chối"!':'Você deve inserir "Motivo da rejeição"!',
                'Bạn phải nhập lý do từ chối!':'Você deve inserir o motivo da rejeição!',
                '"Lý do từ chối" không vượt quá 255 kí tự!':'"Motivo da rejeição "não excede 255 caracteres!',
                'Bạn có chắc chắn muốn xóa banner không?':'Tem certeza de que deseja excluir o banner?',
                'Bạn có chắc chắn muốn xóa avatar không?':'Tem certeza de que deseja excluir o avatar?',
                "'Lý do từ chối duyệt lại' không vượt quá 1000 kí tự!":"'O motivo da recusa de revisão' não excede 1000 caracteres!",
                "Bạn có đồng ý không duyệt?":"Você se recusa a aprovar?",
                "Bạn có đồng ý phê duyệt lại?":"Você concorda em aprovar novamente?",
                "Bạn có đồng ý convert lại?":"Você concorda em converter?",
                "Bạn có đồng ý xóa?":"Você concorda em excluir??",
                "Bạn có đồng ý phê duyệt?":"Você concorda em aprovar?",
                "Yêu cầu nhập nội dung nháp":"Solicitar rascunho de conteúdo",
                "Yêu cầu chọn số thuê bao nhận tin":"Solicitação para selecionar o número de assinantes",
                "Bạn có chắc chắn bỏ trống cấu hình?":"Tem certeza de que deseja esvaziar a configuração?",
                "Bạn có đồng ý hạ nội dung?":"Você concorda em diminuir o conteúdo?",
                "Cập nhật hoàn thành thất bại":"Falha na atualização concluída",
                "Xóa video khỏi playlist thất bại":"Excluir o vídeo da lista de reprodução com falha",
                "Nội dung thêm vào vượt quá ngưỡng":"Conteúdo adicional excede o limite",
                "cho phép!":"permitir!",
                "Sửa tên":"Editar nome",
                "Thêm video vào playlist thất bại":"Falha ao adicionar vídeo à lista de reprodução",
                'Xóa':'Apagar',
                'Phê duyệt': 'aceitar',
                'Bạn có muốn xem lướt?':'Deseja navegar?',
                'Giá không được nhỏ hơn 0':"Price não deve ser menor que 0",
            }
        };
        function action_lang()
        {
            var x = document.getElementsByName('myform');
            alert(x);
            x[0].submit();
        }
        function trans(source) {

            if (typeof arrLang[currentLanguage][source] != 'undefined') {
                return arrLang[currentLanguage][source];
            }
            
            return source;
        }
    </SCRIPT>
    </body>
    </html>
<?php $this->endPage() ?>