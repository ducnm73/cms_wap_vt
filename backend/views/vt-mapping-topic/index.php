<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtMappingTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mapping Thể loại & chủ đề');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-slideshow-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'crawler_category_id',
            ],
            'name',


            [
                'attribute' => 'type',
                'label' => Yii::t('backend','Loại'),
                'format' => 'raw',
                'value' => function ($model) {
                    return array_key_exists($model->type, Yii::$app->params['attribute.type'])? Yii::$app->params['attribute.type'][$model->type]: $model->type;
                },
                'filter' => Yii::$app->params['attribute.type'],

            ],
            [
                'attribute' => 'local_category_id',
                'label' => Yii::t('backend','Thể loại'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->category_name;
                },
                'filter' => ArrayHelper::map(\backend\models\VtGroupCategory::getAllActiveChildCategory(), 'id', 'name_detail')
            ],


            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
