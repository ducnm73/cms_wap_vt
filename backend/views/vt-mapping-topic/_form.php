<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use backend\models\VtVideo;
use backend\models\VtPlaylist;
use backend\models\VtGroupTopic;
use backend\models\VtGroupCategory;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\VtMappingTopic */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::toRoute(['vt-mapping-topic/search']);

$itemName = '';

if(!empty($model->local_topic_id)){
    $itemName = \backend\models\VtGroupTopic::findOne($model->local_topic_id)->name;
}

?>


<div class="vt-mapping-topic-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'local_category_id')->dropDownList(ArrayHelper::map(VtGroupCategory::getAllActiveChildCategory(), 'id', 'name_detail'), [
        'class' => 'form-control',
        'prompt'=>'--- '.Yii::t('backend','Chọn thể loại').' ---'
    ]); ?>






    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
