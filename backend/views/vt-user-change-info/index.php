<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;
use backend\assets\UserAsset;
UserAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtUserChangeInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Tool duyệt thông tin kênh');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-change-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user_id',
                'contentOptions' => ['style'=>'text-align:right; width: 90px;'],
            ],
            [
                'attribute' => 'full_name',
                'contentOptions' => ['style'=>'max-width: 100px; word-break: break-all;'],
            ],
            [
                'attribute' => 'description',
                'contentOptions' => ['style'=>'max-width: 100px; word-break: break-all;'],
            ],
            [
                'label' => Yii::t('backend','Ảnh avatar'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->bucket , $model->path, VtHelper::SIZE_AVATAR), ['class' => 'image-grid-display']);
                }
            ],
            [
                'label' => Yii::t('backend','Ảnh banner'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->channel_bucket , $model->channel_path, VtHelper::SIZE_BANNER), ['class' => 'image-grid-display']);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $widget) {
                    return $model->getStatusString();
                },
                'filter' => \common\helpers\Utils::getInterlizationParams('comment.status.dropdown.value', true, 'backend')
            ],
            [
                'attribute' => Yii::t('backend', 'Thao tác'),
                'format' => 'raw',
                'contentOptions' => ['style'=>'text-align:right; width: 115px;'],
                'value' => function ($model) {
                    if ($model->checkApprovePermission()) {
                        $approveBtn = '<button style="width: 115px;" onclick="approveUserChangeInfoQuick(' . $model->id . ', \'approve\')" type="button" class="btn btn-success video-btn-publish">' . Yii::t("backend", "Duyệt") . '</button>';
                        $deleteBtn = '<button style="width: 115px;" onclick="declineUserChangeInfo(' . $model->id . ', \'delete\')" type="button" class="btn btn-danger video-btn-publish">' . Yii::t("backend", "Không duyệt"). '</button>';
                       // $waitapproveBtn = '<button style="width: 115px;" onclick="wai">'
                        $detailBtn = '<a style="width: 115px; background-color: yellowgreen;" href="/vt-user-change-info/update?id='. $model->id .'" target="_blank" class="btn btn-success video-btn-publish">' . Yii::t("backend", "Chi tiết") . '</a>';

                        switch ($model->status) {
                            case 0:
                                return $approveBtn . $deleteBtn . '<br>'. $detailBtn;
                            case 1:
                                return $deleteBtn . '<br>'. $detailBtn;
                            case 2:
                                return $approveBtn . '<br>'. $detailBtn;
                        }
                    }
                },
            ],
        ],
    ]); ?>
</div>
<input type="hidden" id="approve-user-change-info" value="/vt-user-change-info/process">
<div id="userChangeInfoModal" class="modal">
    <div class="modal-content">
        <div>
            <span class="close">&times;</span>
            <p><?= Yii::t('backend','Lý do từ chối duyệt')?>:</p>
        </div>
        <div>
            <textarea rows="4" id="declineUserChangeInfoReason" style="width: 100%; resize: none;"></textarea>
        </div>
        <input type="hidden" id="hiddenId"/>
        <input type="hidden" id="hiddenAction"/>
        <input type="button" id="reasonDecline" onclick="submitReasonDeclineUserChangeInfoQuick()" value="<?= Yii::t('backend', 'Lưu lại')?>" class="btn btn-primary" style="margin: 0 200px;"/>
    </div>
</div>