<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtUserChangeInfo */

$this->title = Yii::t('backend', 'Create Vt User Change Info');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt User Change Infos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-change-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
