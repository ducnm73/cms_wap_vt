<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\VtGroupTopic;
use backend\models\VtGroupCategory;
use common\helpers\Utils;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-video-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <object class="video_preview" width="300" height="170" allowFullScreen="true" type="application/x-shockwave-flash"
        data="/js/backend_player/player.swf?url=<?= \common\libs\S3Service::generateStreamURL($model) ?>&autoplay=1&volume=80&play=false&wmode=transparent">
    </object>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => 1000]) ?>

    <?php if(!$model->isNewRecord && !empty($model->path)):?>
        <img class="preview-image" src="<?= \common\libs\S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
    <?php endif ?>
    <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', Yii::t('backend','Ảnh đại diện'))) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'is_no_copyright')->checkbox() ?>

    <?= $form->field($model, 'published_time')->widget(DateTimePicker::classname(), [
        'options' => [
            'value' => ($model->published_time) ? $model->published_time : date("Y-m-d H:i:s"),
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'todayHighlight' => true,
        ]
    ]);
    ?>

    <div class="form-group">

        <?php if(!$model->isApprove() || $model->checkApprovePermission()): ?>
            <?= Html::submitButton(Yii::t('backend','Lưu'), ['class' => 'btn btn-success', 'name' => 'submitSave', 'value' => 'save']) ?>

            <?php if(!$model->isApprove()):?>
                <?= Html::submitButton(Yii::t('backend','Gửi duyệt'), ['class' => 'btn btn-primary', 'name' => 'submitApprove', 'value' => 'send_approve']) ?>

                <?php if($model->checkApprovePermission()): ?>
                    <?= Html::submitButton(Yii::t('backend','Duyệt'), ['class' => 'btn btn-primary', 'name' => 'submitPublish', 'value' => 'publish']) ?>
                <?php endif;?>

            <?php endif;?>

            <?php if(!$model->isNewRecord):?>
                <?php if(($model->isOwner() || $model->checkApprovePermission()) && !$model->isDelete()): ?>
                    <?= Html::submitButton(Yii::t('backend','Xóa'), ['class' => 'btn btn-danger', 'name' => 'submitDelete', 'value' => 'delete']) ?>
                <?php endif;?>
            <?php endif;?>

        <?php endif;?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
