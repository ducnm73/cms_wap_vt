<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\assets\EditableAsset;
use backend\assets\FineUploaderVideoAsset;
use backend\models\VtVideo;
use common\libs\VtHelper;


EditableAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtVideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = Yii::t('backend','Tập phim');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-video-index">

    <ul class="nav nav-pills">

        <li role="presentation" class="<?= ($state == 'temp')? 'active' : '' ?>"><a href="<?= Url::toRoute(['film-part/index', 'state' => 'temp' ]); ?>"><?= Yii::t('backend','Tập phim tạm')?></a></li>
        <li role="presentation" class="<?= ($state == 'published')? 'active' : '' ?>"><a href="<?= Url::toRoute(['film-part/index', 'state' => 'published' ]); ?>"><?= Yii::t('backend','Xuất bản')?></a></li>
        <li role="presentation" class="<?= ($state == 'draft')? 'active' : '' ?>"><a href="<?= Url::toRoute(['film-part/index', 'state' => 'draft' ]); ?>"><?= Yii::t('backend','Chờ duyệt')?></a></li>
        <li role="presentation" class="<?= ($state == 'converting')? 'active' : '' ?>"><a href="<?= Url::toRoute(['film-part/index', 'state' => 'converting' ]); ?>"><?= Yii::t('backend','Chờ convert')?></a></li>
        <li role="presentation" class="<?= ($state == 'error')? 'active' : '' ?>"><a href="<?= Url::toRoute(['film-part/index', 'state' => 'error' ]); ?>"><?= Yii::t('backend','Convert lỗi')?></a></li>
        <li role="presentation" class="<?= ($state == 'deleted')? 'active' : '' ?>"><a href="<?= Url::toRoute(['film-part/index', 'state' => 'deleted' ]); ?>"><?= Yii::t('backend','Xóa')?></a></li>

    </ul>

    <input id="approve-action" type="hidden" value="<?php echo Url::toRoute(['film-part/approve']) ?>">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Tên'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="' . Url::toRoute(['film-part/update', 'id' => $model->id]) .'">'.$model->name.'</a>';
                },
            ],
            'description',
            [
                'attribute' => 'is_active',
                'label' => Yii::t('backend','Trạng thái'),
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->is_active == \backend\models\VtVideo::ACTIVE) ? Yii::t('backend','Kích hoạt') : Yii::t('backend','Không kích hoạt');
                },
                'filter' => Yii::$app->params['active.dropdown.value']
            ],
//            'category_id',

            [
                'attribute' => Yii::t('backend','Thông tin'),
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        '<p><strong>'.Yii::t('backend','Lượt xem:').' </strong>'. $model->play_times .'</p>'
                        .'<p><strong>'.Yii::t('backend','Lượt thích:').' </strong>'. $model->like_count .'</p>'
                        .'<p><strong>'.Yii::t('backend','Resolution:').' </strong>'. $model->resolution .'</p>'
                        .'<p><strong>'.Yii::t('backend','CP:').' </strong>'. $model->resolution .'</p>'
                        .'<p><strong>'.Yii::t('backend','Người tạo:').' </strong>'. $model->created_by .'</p>'
                        .'<p><strong>'.Yii::t('backend','Người cập nhật:').' </strong>'. $model->updated_by .'</p>'
                        ;
                },
            ],

            [

                'attribute' => 'Image',
                'label' => Yii::t('backend','Ảnh'),
                'format' => 'raw',
                'value' => function ($model) {

                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_COVER), ['class' => 'image-grid-display']);

                }
            ],


            [
                'attribute' => 'Video',
                'format' => 'raw',
                'value' => function ($model) {
//                    return  '<object class="video_preview" width="300" height="170" allowFullScreen="true" type="application/x-shockwave-flash"'
//                            .    'data="/js/backend_player/player.swf?url='.\common\libs\S3Service::generateStreamURL($model).'autoplay=1&volume=80&play=false&wmode=transparent">'
//                            .'</object>'
//                        ;

                    return  '<object class="video_preview" width="300" height="170" allowFullScreen="true" type="application/x-shockwave-flash"'
                            .    'data="/js/backend_player/player.swf?autoplay=1&volume=80&play=false&wmode=transparent">'
                            .'</object>'
                        ;


                },
            ],
            [
                'attribute' => Yii::t('backend','Thao tác'),
                'format' => 'raw',
                'value' => function ($model) {

                    $state = Yii::$app->session->get('state');
                    $sendApproveBtn = '<button onclick="approveVideo('.$model->id.', \'waiting\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Gửi duyệt').'</button>';
                    $publishBtn = '<button onclick="approveVideo('.$model->id.', \'approve\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Xuất bản').'</button>';
                    $deleteBtn = '<button onclick="approveVideo('.$model->id.', \'delete\')" type="button" class="btn btn-danger video-btn-publish">'.Yii::t('backend','Xóa').'</button>';
                    $reconvertBtn = '<button onclick="approveVideo('.$model->id.', \'reconvert\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Convert lại').'</button>';

                    $btn = '';
                    switch ($state) {
                        case 'temp':
                            $btn = $sendApproveBtn . $deleteBtn;
                            break;

                        case 'published':
                        case 'converting':
                        $btn = $deleteBtn;
                            break;
                        case 'draft':
                            $btn = $publishBtn . $deleteBtn;
                            break;
                        case 'error':
                            $btn = $reconvertBtn;
                            break;
                        case 'deleted':
                            $btn = $publishBtn;
                            break;

                    }
                    return $btn;


                },
            ],


        ],
    ]); ?>


</div>
