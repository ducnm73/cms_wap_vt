<?php

use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="report_comment_disapprove">
    <?php $form = ActiveForm::begin([
        'method' => 'GET',
        'enableClientScript' => false,
    ]); ?>
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <?= $form->field($searchModel, 'created_at')->widget(DateRangePicker::className(), [
                'model' => $searchModel,
                'value' => date('Y-m-00').' - '. date('Y-m-d'),
                'convertFormat' => true,
                'readonly' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'Y-m-d'],
                    'timePicker'=>false,
                    'timePickerIncrement'=>15,
                    'showDropdowns'=>true,
//                    'dateLimit' => [
//                        'days' => 30,
//                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-success" name="form_action">
            <?= Yii::t('backend', 'Search') ?></button>
        <?php if (sizeof($dataProvider->getModels()) > 0) : ?>
            <button type="submit" class="btn btn-primary" value="export" name="form_action">Export Excel</button>
        <?php endif; ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]); ?>
</div>
</div>
