<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use backend\models\VtUser;
use backend\models\VtUserChangeInfo;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$this->params['breadcrumbs'][] = $this->title;
$idUser = 0;
?>

<div class="report_chanel_tonghop">
    <?php $form = ActiveForm::begin([
        'method' => 'GET',
        'enableClientScript' => false,
    ]); ?>
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <?= $form->field($searchModel, 'created_at')->widget(DateRangePicker::className(), [
                'model' => $searchModel,
                'value' => date('Y-m-00') . ' - ' . date('Y-m-d'),
                'convertFormat' => true,
                'readonly' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'Y-m-d'],
                    'timePicker'=>false,
                    'timePickerIncrement'=>15,
                    'showDropdowns'=>true,
//                    'dateLimit' => [
//                        'days' => 30,
//                    ],
                ],
            ]);
            ?>
        </div>

        <div class="col-md-4 col-lg-4">
            <?php $user = VtUser::findOne($searchModel->user_id); ?>
            <?= $form->field($searchModel, 'user_id')->widget(Select2::classname(), [
                'initValueText' => $user ? $user->full_name : '',
                'options' => [
                    'id' => 'user-id',
                    'placeholder' => Yii::t('backend', 'User') . ' ...'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . "...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/vt-user/search']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term }; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),

                ],

            ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-success" name="form_action">
            <?= Yii::t('backend', 'Search') ?></button>
        <?php if (sizeof($dataProvider->getModels()) > 0) : ?>
            <button type="submit" class="btn btn-primary" value="export" name="form_action">Export Excel</button>
        <?php endif; ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]); ?>
</div>
</div>
