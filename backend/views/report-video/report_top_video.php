<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use backend\models\VtCp;
use kartik\date\DatePicker;

?>

<div>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo content xem nhiều')?></b></h4>

    <div class="row">
        <form method="get" action="/report-video/report-top-video">

            <div class="col-md-3">

                <?=
                DatePicker::widget([
                    'name' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                ]);
                ?>

            </div>

            <div class="col-md-3">

                <?= \kartik\select2\Select2::widget([
                    'name' => 'cp_id',
                    'value' => $cpId, // initial value
                    'data' => ArrayHelper::map(VtCp::getAllCp(), 'id', 'name'),
                    'maintainOrder' => true,
                    'options' => ['placeholder' => Yii::t('backend','Tất cả đối tác').' ...', 'multiple' => false],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10,
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>


            <div class="col-md-3">

                <?= \kartik\select2\Select2::widget([
                    'name' => 'category_id',
                    'value' => $categoryId, // initial value
                    'data' => ArrayHelper::map(\backend\models\VtGroupCategory::getAllCategory(), 'id', 'name'),
                    'maintainOrder' => true,
                    'options' => ['placeholder' => Yii::t('backend','Tất cả chuyên mục').' ...', 'multiple' => false],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10,
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-md-3">

                <?= \kartik\select2\Select2::widget([
                    'name' => 'limit',
                    'value' => $limit, // initial value
                    'data' => [20 => Yii::t('backend', 'Top 20'), 50 => Yii::t('backend', 'Top 50'), 100 => Yii::t('backend', 'Top 100'), 500 => Yii::t('backend', 'Top 500')],
                    'maintainOrder' => true,
                    'options' => ['multiple' => false],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10,
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>


            <div class="col-md-3">
                <input type="submit" name="action" value="<?php echo Yii::t('backend', 'REPORT')?>" class="btn btn-primary"/>
            </div>
        </form>

    </div>


    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
                <tr>
                    <th width="3%"><strong><?= Yii::t('backend','STT')?></strong></th>
                    <th width="8%"><strong><?= Yii::t('backend','ID Video')?></strong></th>
                    <th width="50%"><strong><?= Yii::t('backend','Tên Video')?></strong></th>
                    <th width="15%"><strong><?= Yii::t('backend','Tên Đối tác')?></strong></th>
                    <th width="15%"><strong><?= Yii::t('backend','Chuyên mục')?></strong></th>
                    <th width="15%"><strong><?= Yii::t('backend','Số lượt xem')?></strong></th>
                </tr>
            </thead>
            <?php if (count($contents) > 0): ?>
                <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($contents as $key => $value):?>
                        <?php $i++ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $value['key'] ?></td>
                            <td><?= isset($videos[$value['key']]['name']) ? $videos[$value['key']]['name'] : $value['key'] ?></td>
                            <td><?= isset($videos[$value['key']]['cp_code']) ? $videos[$value['key']]['cp_code'] : '' ?></td>
                            <td><?= isset($videos[$value['key']]['category_name']) ? $videos[$value['key']]['category_name'] : '' ?></td>
                            <td><?= Yii::$app->formatter->asInteger($value['doc_count']) ?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="5" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
