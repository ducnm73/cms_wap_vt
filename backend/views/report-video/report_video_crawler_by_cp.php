<?php
use kartik\date\DatePicker;
use yii\widgets\LinkPager;
?>

<div>
    <?php
        $this->title = Yii::t('backend', "Video crawler theo CP");
    ?>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Video crawler theo CP')?></b></h4>
    <div class="row">
        <form method="get" id="video-crawler-by-cp-form" action="/report-video/video-crawler-by-cp">
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'from_time',
                    'id' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'readonly' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'id' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'readonly' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                ]);
                ?>

            </div>

            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" id="exportVideoCrawlerByCPDetailBtn" style="float: right;"><?= Yii::t('backend','Xuất file chi tiết')?></button>
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary" id="exportVideoCrawlerByCPBtn" style="float: right;"><?= Yii::t('backend','Xuất file')?></button>
            </div>

        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">
            <thead>
                <tr class="headerTable">
                    <td class="align-center"><?= Yii::t('backend','STT')?></td>
                    <td class="align-center"><?= Yii::t('backend','Mã CP')?></td>
                    <td class="align-center"><?= Yii::t('backend','Ngày tạo')?></td>
                    <td class="align-center"><?= Yii::t('backend','Số lượng video chờ duyệt')?></td>
                </tr>
            </thead>
            <?php if (count($videoCrawlerByCPs) > 0): ?>
                <tbody>
                <?php $i = 0; ?>
                <?php foreach($videoCrawlerByCPs as $videoCrawlerByCP):?>
                    <?php $i++ ?>
                    <?php $no = $pagination->offset + $i ?>
                    <tr>
                        <td class="align-right"><?= $no ?></td>
                        <td class="align-left"><?= $videoCrawlerByCP['cp_code'] ?></td>
                        <td class="align-center"><?= $videoCrawlerByCP['createdDate'] ?></td>
                        <td class="align-right"><?= $videoCrawlerByCP['numOfWaitVideo'] ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                <tr>
                    <td colspan="4" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>
    <div class="row">
        <div class="col-md-10">
            <?= LinkPager::widget(['pagination' => $pagination, 'firstPageLabel' => Yii::t('backend','First' ), 'lastPageLabel'  => Yii::t('backend', 'Last')]) ?>
        </div>
    </div>
</div>