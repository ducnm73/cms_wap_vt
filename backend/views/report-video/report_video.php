<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use backend\models\VtCp;
use kartik\date\DatePicker;

?>

<div>
    <h4 style="text-align: center;"><b><?= Yii::t('backend','Báo cáo quản lý content')?></b></h4>

    <div class="row">
        <form method="get" action="/report-video/report-video">

            <div class="col-md-3">

                <?=
                DatePicker::widget([
                    'name' => 'from_time',
                    'id' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian bắt đầu')],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'id' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => Yii::t('backend','Thời gian kết thúc')],
                ]);
                ?>

            </div>


            <div class="col-md-3">

                <?= \kartik\select2\Select2::widget([
                    'name' => 'type',
                    'id' => 'type',
                    'value' => $type, // initial value
                    'data' => [
                        '1' => Yii::t('backend','CP đối soát'),
                        '0' => Yii::t('backend','CP Crawler'),
                        '2' => Yii::t('backend','Khách hàng upload')],
                    'maintainOrder' => true,
                    'options' => ['multiple' => false, 'prompt' => Yii::t('backend','Chọn loại')],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10,
                        'allowClear' => true
                    ]
                ]);
                ?>
            </div>




            <div class="col-md-2">
                <input type="submit" name="action" value="<?php Yii::t('backend', 'REPORT')?>" class="btn btn-primary"/>
            </div>
            <div class="col-md-1">
                <input type="button" id="exportContentManagementBtn" name="action" value="<?php echo Yii::t('backend', 'EXPORT')?>" class="btn btn-danger"/>
            </div>
        </form>

    </div>


    <div style="width:100%;overflow: scroll">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
                <tr>
                    <th><strong><?= Yii::t('backend','STT')?></strong></th>
                    <th><strong><?= Yii::t('backend','CP')?></strong></th>
                    <th><strong><?= Yii::t('backend','Tổng video luỹ kế')?></strong></th>
                    <th><strong><?= Yii::t('backend','Lũy kế nội dung trong tháng báo cáo')?></strong></th>

                    <?php
                        $column = '';
                        foreach($dates as $date){
                            $column  .= '<th><strong>' . $date . '</strong></th>';
                        }
                    ?>
                    <?= $column ?>

                </tr>
            </thead>
            <?php if (count($contents) > 0): ?>
                <tbody>
                    <?php $i = 0; ?>

                    <?php foreach($contents as $key => $value):?>


                        <?php $i++ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= isset($cps[$key]) ? $cps[$key] : $key ?></td>
                            <td><?= Yii::$app->formatter->asInteger($value['total'])?></td>
                            <td><?= Yii::$app->formatter->asInteger($value['total_by_range']) ?></td>

                            <?php
                            $column = '';
                            foreach($dates as $date){
                                if(isset($value['range'][$date])){
                                    $column  .= '<td>' . Yii::$app->formatter->asInteger($value['range'][$date]) . '</td>';
                                }else{
                                    $column  .= '<td>' . 0 . '</td>';
                                }
                            }
                            ?>
                            <?= $column ?>



                        </tr>
                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="5" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
