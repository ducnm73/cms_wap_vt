<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtAccountInfomationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-account-infomation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php //$form->field($model, 'id') ?>

    <?php // $form->field($model, 'name') ?>

    <?php // $form->field($model, 'email') ?>

    <?php //->field($model, 'status') ?>

    <?php // $form->field($model, 'id_card_number') ?>

    <?php // echo $form->field($model, 'id_card_created_at') ?>

    <?php // echo $form->field($model, 'id_card_created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'channelName')->textInput(array('placeholder' => Yii::t('backend','Tên kênh')))->label(false) ?>
        </div>
        <div class="col-md-3">
            <?= \kartik\daterange\DateRangePicker::widget([
                'model' => $model,
                'name' => 'created_at',
                'attribute' => 'created_at',
                'value' => date('Y-m-d') . ' - ' . date('Y-m-d'),
                'convertFormat' => true,
                'pluginOptions' => [
                    'timePicker' => false,
                    'timePicker24Hour' => true,
                    'locale' => ['format' => 'Y-m-d']
                ],
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => '---'.Yii::t('backend','Ngày đăng ký').'---'
                ]
            ])
            ?>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?php // Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>


    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'is_confirm') ?>

    <?php // echo $form->field($model, 'id_card_image_frontside') ?>

    <?php // echo $form->field($model, 'id_card_image_backside') ?>

    <?php // echo $form->field($model, 'bucket') ?>

    <?php // echo $form->field($model, 'msisdn') ?>

    <?php // echo $form->field($model, 'reason') ?>

    <?php ActiveForm::end(); ?>

</div>
