<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VtAccountInfomation */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vt Account Infomations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-account-infomation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            'status',
            'id_card_number',
            'id_card_created_at',
            'id_card_created_by',
            'created_at',
            'created_by',
            'user_id',
            'updated_at',
            'updated_by',
            'is_confirm',
            'id_card_image_frontside',
            'id_card_image_backside',
            'bucket',
            'msisdn',
            'reason',
        ],
    ]) ?>

</div>
