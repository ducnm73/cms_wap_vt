<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtAccountInfomationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Duyệt thông tin user upload video');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-account-infomation-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // Html::a('Create Vt Account Infomation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user_id',
                'contentOptions' => ['style'=>'text-align:right;'],
            ],
            [
                'label' => Yii::t('backend','Tên kênh'),
                'value' => 'user.full_name'
            ],
            'name',
            [
                'attribute' => 'id_card_number',
                'contentOptions' => ['style'=>'text-align:right;'],
            ],
            'email:email',
            [
                'attribute' => 'msisdn',
                'contentOptions' => ['style'=>'text-align:right;'],
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('backend','Trạng thái'),
                'contentOptions' => ['style'=>'text-align:center;'],
                'headerOptions' => ['style'=>'text-align:center;'],
                'value' => function ($model) {
                    if ($model->status == \backend\models\VtAccountInfomation::WAIT_APPROVE) return Yii::t('backend', 'Chờ duyệt');
                    if ($model->status == \backend\models\VtAccountInfomation::ACTIVE) return Yii::t('backend', 'Đã duyệt');
                    if ($model->status == \backend\models\VtAccountInfomation::DISAPPROVE) return Yii::t('backend', 'Không duyệt');
                    if ($model->status == \backend\models\VtAccountInfomation::BANNED) return Yii::t('backend', 'Bị khóa');
                },
                'filter' => \common\helpers\Utils::getInterlizationParams('account.information.status.dropdown.value', true, 'backend')
            ],
            [
                'attribute' => Yii::t('backend','Thao tác'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="/vt-account-infomation/update?id='. $model->id .'" target="_blank" style="color: #0F9E5E;">'.Yii::t('backend','Chi tiết').'</a>';
                },
            ],
            // 'id_card_created_at',
            // 'id_card_created_by',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'is_confirm',
            // 'id_card_image_frontside',
            // 'id_card_image_backside',
            // 'bucket',
            // 'reason',
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
