<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtAccountInfomation */

$this->title = Yii::t('backend', 'Create Vt Account Infomation');
$this->params['breadcrumbs'][] = ['label' => 'Vt Account Infomations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-account-infomation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
