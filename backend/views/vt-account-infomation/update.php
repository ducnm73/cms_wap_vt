<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtAccountInfomation */

$this->title = Yii::t('backend','Duyệt thông tin user upload video').':  ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Thông tin user upload video'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vt-account-infomation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'accountInfor' => $accountInfor,
        'updaterName' => $updaterName,
    ]) ?>

</div>
