<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\libs\S3Service;

/* @var $this yii\web\View */
/* @var $model backend\models\VtAccountInfomation */
/* @var $form yii\widgets\ActiveForm */

function getStatus($model) {
    if ($model->status == \backend\models\VtAccountInfomation::WAIT_APPROVE) return Yii::t('backend', 'Chờ duyệt');
    if ($model->status == \backend\models\VtAccountInfomation::ACTIVE) return Yii::t('backend', 'Đã duyệt');
    if ($model->status == \backend\models\VtAccountInfomation::DISAPPROVE) return Yii::t('backend', 'Không duyệt');
}
?>

<div class="vt-account-infomation-form">
    <input type="hidden" id="approve-channel-profile-action" value="/vt-account-infomation/process">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'readonly'=> true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'readonly'=> true]) ?>

    <?= $form->field($model, 'id_card_number')->textInput(['maxlength' => 30, 'readonly'=> true]) ?>

    <?= $form->field($model, 'id_card_created_at')->textInput(['maxlength' => 30, 'readonly'=> true]) ?>

    <?= $form->field($model, 'id_card_created_by')->textInput(['maxlength' => 30, 'readonly'=> true]) ?>

    <?= $form->field($model, 'created_at')->textInput(['readonly'=> true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['value' => $accountInfor->email . ' - ' . $accountInfor->msisdn, 'readonly'=> true]) ?>

    <?= $form->field($model, 'user_id')->textInput(['readonly'=> true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['readonly'=> true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['value' => $updaterName, 'readonly'=> true]) ?>

    <div>
        <div class="row" style="text-align: center">
            <div class="col-md-6">
                <p style="font-size: 14px;"><?= Yii::t('backend','Ảnh mặt trước CMND:')?> </p>
                <?php if (!$model->isNewRecord && !empty($model->id_card_image_frontside)): ?>
                    <?php
                        $idCardFrontside = S3Service::generateWebUrl($model->bucket, $model->id_card_image_frontside);
                    ?>
                    <img class="image-preview" width="500" src="<?= $idCardFrontside; ?>"/>
                    <p><a href="<?= $idCardFrontside; ?>" target="_blank" style="color: #0F9E5E; font-style: italic;"><?= Yii::t('backend','Xem cỡ lớn')?></a></p>
                <?php else: ?>
                    <p><?= Yii::t('backend','(Không có ảnh)')?></p>
                <?php endif ?>
            </div>
            <div class="col-md-6">
                <p style="font-size: 14px;"><?= Yii::t('backend','Ảnh mặt sau CMND:')?> </p>
                <?php if (!$model->isNewRecord && !empty($model->id_card_image_backside)): ?>
                    <?php
                        $idCardBackside = S3Service::generateWebUrl($model->bucket, $model->id_card_image_backside);
                    ?>
                    <img class="image-preview" width="500" src="<?= $idCardBackside; ?>"/>
                    <p><a href="<?= $idCardBackside; ?>" target="_blank" style="color: #0F9E5E; font-style: italic;"><?= Yii::t('backend','Xem cỡ lớn')?></a></p>
                <?php else: ?>
                    <p><?= Yii::t('backend','(Không có ảnh)')?></p>
                <?php endif ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'msisdn')->textInput(['maxlength' => 20, 'readonly'=> true]) ?>

    <?= $form->field($model, 'status')->textInput(['value' => getStatus($model), 'readonly'=> true]) ?>

    <?= $form->field($model, 'reason')->textInput(['maxlength' => 2000]) ?>

    <div id="processButton">
        <?php
        if ($model->checkApprovePermission()) {
            $approveBtn = '<button style="width: 115px;" onclick="processChannelProfile(' . $model->id . ', \'approve\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Duyệt').'</button>';
            $deleteBtn = '<button style="width: 115px;" onclick="processChannelProfile(' . $model->id . ', \'delete\')" type="button" class="btn btn-danger video-btn-publish">'.Yii::t('backend','Không duyệt').'</button>';
            $banBtn = '<button style="width: 130px;" onclick="processChannelProfile(' . $model->id . ', \'ban\')" type="button" class="btn btn-danger video-btn-publish">'.Yii::t('backend','Khóa tài khoản').'</button>';
        }
        switch ($model->status) {
            case 1:
                echo $approveBtn . $deleteBtn . $banBtn;
                break;
            case 2:
                echo $deleteBtn . $banBtn;
                break;
            case 3:
                echo $approveBtn . $banBtn;
                break;
            case 4:
                echo $approveBtn;
                break;
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
