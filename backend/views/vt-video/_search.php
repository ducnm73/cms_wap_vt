<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\VtVideo;
/* @var $this yii\web\View */
/* @var $model backend\models\VtCommentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="col-md-3">
        <?= Html::activeInput('text', $searchModel,
            'name',
            ['class' => 'form-control', 'placeholder' => '---'.Yii::t('backend','Tên Video').'---']
        ) ?>
    </div>



    <div class="col-md-3">
        <?= \kartik\select2\Select2::widget([
            'model' => $searchModel,
            'attribute' => 'created_by',
            'initValueText' => $searchModel->getCreatedByName(),
            'options' => ['placeholder' => '---'.Yii::t('backend','Người tạo (Kênh)').'---'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return '" .  Yii::t('backend', 'Loading ...') . "'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['user/search']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term }; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(result) { return result.text; }'),
                'templateSelection' => new JsExpression('function (result) { return result.text; }')
            ]
        ]) ?>
    </div>



    <div class="col-md-3">
        <?= Html::activeDropDownList($searchModel,
            'syn_id',
            [
                '0' => Yii::t('backend','Khách hàng'),
                '1' => Yii::t('backend','Quản trị viên')
            ],
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Nguồn dữ liệu').'---']
        )?>

    </div>

    <div class="col-md-3">

        <?= \kartik\select2\Select2::widget([
            'model' => $searchModel,
            'name' => 'attributes',
            'attribute' => 'attributes',
            'data' => (['1' => Yii::t('backend','Chưa duyệt')] + ArrayHelper::map(\backend\models\VtMappingUser::getAllUser(), 'id', 'username')),
            'options' => [
                'placeholder' => '---' . Yii::t('app', 'User duyệt từ kho') . '---',
                'multiple' => false,
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'showToggleAll' => false,
        ]) ?>

    </div>



    <div class="col-md-3">
        <?= Html::activeDropDownList($searchModel,
            'is_hot',
            \common\helpers\Utils::getInterlizationParams('boolean.dropdown.value', true, 'backend'),
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Đề xuất').'---']
        )?>
    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList($searchModel,
            'is_recommend',
            [
                VtVideo::RECOMMEND_DYNAMIC => Yii::t('backend','Động'),
                VtVideo::RECOMMEND_FIX => Yii::t('backend','Tĩnh'),
            ],
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Thịnh hành').'---']
        )?>
    </div>




    <div class="col-md-3">
        <?= Html::activeDropDownList(
            $searchModel,
            'updated_by',
            (['N/A' => Yii::t('backend','Chưa tác động')] + ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username')),
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Tác động bởi').'---'])?>
    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList($searchModel,
            'review_by',
            ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username'),
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Xem lướt bởi').'---'])
        ?>
    </div>

    <div class="col-md-3">
        <?= \kartik\daterange\DateRangePicker::widget([
            'model'=>$searchModel,
            'name'=>'range_updated_at',
            'attribute' => 'updated_at',
            'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
            'convertFormat'=>true,
            'pluginOptions'=>[
                'timePicker'=>true,
                'timePicker24Hour' => true,
                'locale'=>['format'=>'Y-m-d H:i:s']
            ],
            'options' => [
                'class'=>'form-control',
                'placeholder' => '---'.Yii::t('backend','Thời gian phê duyệt').'---'
            ]
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= \kartik\daterange\DateRangePicker::widget([
            'model'=>$searchModel,
            'name'=>'range_created_at',
            'attribute' => 'created_at',
            'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
            'convertFormat'=>true,
            'pluginOptions'=>[
                'timePicker'=>true,
                'timePicker24Hour' => true,
                'locale'=>['format'=>'Y-m-d H:i:s']
            ],
            'options' => [
                'class'=>'form-control',
                'placeholder' => '---'.Yii::t('backend','Thời gian tạo').'---'
            ]
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= \kartik\daterange\DateRangePicker::widget([
            'model'=>$searchModel,
            'name'=>'range_published_time',
            'attribute' => 'published_time',
            'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
            'convertFormat'=>true,
            'pluginOptions'=>[
                'timePicker'=>true,
                'timePicker24Hour' => true,
                'locale'=>['format'=>'Y-m-d H:i:s']
            ],
            'options' => [
                'class'=>'form-control',
                'placeholder' => '---'.Yii::t('backend','Thời gian xuất bản').'---'
            ]
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList($searchModel,
            'state',

            \common\helpers\Utils::getInterlizationParams('video.state.dropdown.value', true, 'backend'),
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Trạng thái duyệt').'---']
        )?>

    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList(
            $searchModel,
            'category_id',
            (ArrayHelper::map(\backend\models\VtGroupCategory::getAllCategory(), 'id', 'name')),
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Chọn chuyên mục').'---'])?>
    </div>



    <div class="col-md-3">
        <?= \kartik\select2\Select2::widget([
            'model' => $searchModel,
            'name' => 'cp_id',
            'attribute' => 'cp_id',
            'data' => ArrayHelper::map(\backend\models\VtCp::getAllCp(), 'id', 'cp_code'),
            'options' => [
                'placeholder' => '---' . Yii::t('app', 'Chọn CP') . '---',
                'multiple' => false,
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'showToggleAll' => false,
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList(
            $searchModel,
            'outsource_status',
           \common\helpers\Utils::getInterlizationParams('outsource.status.dropdown.value', true, 'backend'),
            ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Chọn trạng thái outsource').'---'])?>
    </div>

    <div class="col-md-3">
        <?= Html::submitButton(Yii::t('backend','Tìm kiếm'), [
            'class' => 'btn btn-primary'
        ]) ?>
    </div>



    <?php ActiveForm::end(); ?>
</div>