<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */

$this->title = Yii::t('backend','Upload Video');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="vt-video-upload">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_upload', [

    ]) ?>



</div>
