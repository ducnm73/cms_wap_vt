<?php

use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-video-form">
    <?= Tabs::widget([
        'options' => ['id' => 'video-form-tabs'],
        'items' => [
            [
                'label' => Yii::t('backend','Thông tin video'),
                'encode' => false,
                'content' => $this->render('_video_form', ['model' => $model, 'isComplained' => $isComplained, 'feedback' => $feedback,]),
                'active' => true
            ],
            [
                'label' => Yii::t('backend','Đa ngôn ngữ'),
                'encode' => false,
                'content' => $this->render('_multilang_form', ['model' => $model, 'multilangModels' => $multilangModels, 'lang' => 'en']),
            ],
        ],
    ]); ?>

</div>