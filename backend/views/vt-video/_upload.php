<?php

use yii\helpers\Html;

use backend\assets\FineUploaderVideoAsset;
use backend\assets\EditableAsset;
use yii\helpers\ArrayHelper;
use backend\models\VtGroupTopic;
use backend\models\VtGroupCategory;
use common\helpers\Utils;

FineUploaderVideoAsset::register($this);
EditableAsset::register($this);
?>

<script type="text/template" id="qq-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
    </div>
    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
<span class="qq-upload-drop-area-text-selector"></span>
    </div>
    <div class="qq-upload-button-selector qq-upload-button">
    <div><?= Yii::t('backend', 'Upload video file')?></div>
</div>
<span class="qq-drop-processing-selector qq-drop-processing">
    <span><?= Yii::t('backend', 'Processing dropped files...')?></span>
<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
    <li>
    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
    </div>
    <div class="qq-thumbnail-wrapper">

<!--    <object class="video_preview" width="300" height="170" allowFullScreen="true" type="application/x-shockwave-flash"-->
<!--data="/js/backend_player/player.swf?autoplay=1&volume=80&play=false&wmode=transparent">-->
<!--    </object>-->

        <div class="image-uploader"></div>




    </div>
    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>

    <div class="qq-file-info">
    <div class="qq-file-name">
    <form>
    <div class="alert-message">

    </div>

    <input type="hidden" class="video-id">

    <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
    <input type="text" class="form-control video-name" placeholder=<?= Yii::t('backend','Tiêu đề')?>>
    </div>
    <div class="form-group">
        <textarea  class="form-control video-description" placeholder=<?= Yii::t('backend','Mô tả')?>></textarea>
    </div>

    <div class="form-group">
        <?= Html::dropDownList('category_id', null, ArrayHelper::map(VtGroupCategory::getAllActiveCategory(VtGroupCategory::TYPE_VOD), 'id', 'name'), ['class' => 'form-control category-checkbox-list', 'prompt'=>'--- '.Yii::t('backend','Chọn thể loại').' ---']); ?>
    </div>

    <div class="form-group">
        <button type="button" disabled="disabled" class="btn btn-default video-btn"><?= Yii::t('backend', 'Lưu')?></button>
        <button type="button" disabled="disabled" class="btn btn-success video-btn-publish"><?= Yii::t('backend', 'Lưu & Đề nghị duyệt')?></button>
        <span class="video-spinner upload-spinner"></span>
    </div>

    </div>
    <div class="col-xs-6 col-md-6">
        <div class="form-group">

            <?= Html::checkboxList('topic_id', null, ArrayHelper::map(VtGroupTopic::getAllActiveTopic(VtGroupTopic::TYPE_VOD), 'id', 'name'), [
                'class' => 'form-control topic-checkbox-list',
                'prompt'=>'--- '.Yii::t('backend','Chọn chủ đề').' ---',
                'item' =>
                    function ($index, $label, $name, $checked, $value) {

                        return Html::checkbox($name, $checked, [
                            'value' => $value,
                            'label' => Utils::encodeOutput($label),
                            'class' => 'checkbox-item',

                        ]);
                    },

            ]); ?>

        </div>

    </div>
</div>



<script type="text/template" id="image-template">
<div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
<div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
</div>
<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
    <span class="qq-upload-drop-area-text-selector"></span>
</div>
<div class="qq-upload-button-selector qq-upload-button">
    <div><?= Yii::t('backend', 'Upload image file')?></div>
</div>
<span class="qq-drop-processing-selector qq-drop-processing">
    <span><?= Yii::t('backend', 'Processing dropped files...')?></span>
<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <div class="image-preview"></div>

        <ul style="display:none" class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>



                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>

                <div class="qq-file-info">
                    <div class="qq-file-name">


                </div>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                <span class="qq-btn qq-delete-icon" aria-label="<?= Yii::t('backend', 'Delete')?>"></span>
                </button>
                <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                <span class="qq-btn qq-pause-icon" aria-label="<?= Yii::t('backend', 'Pause')?>"></span>
                </button>
                <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                <span class="qq-btn qq-continue-icon" aria-label="<?= Yii::t('backend', 'Continue')?>"></span>
                </button>
                </div>
                </li>
                </ul>

    <dialog class="qq-alert-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector"><?= Yii::t('backend', 'Close')?></button>
    </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector"><?= Yii::t('backend', 'No')?></button>
    <button type="button" class="qq-ok-button-selector"><?= Yii::t('backend', 'Yes')?></button>
    </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <input type="text">
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector"><?= Yii::t('backend', 'Cancel')?></button>
    <button type="button" class="qq-ok-button-selector"><?= Yii::t('backend', 'Ok')?></button>
    </div>
    </dialog>
    </div>
</script>

<input type="hidden" id="update-video-url" value="<?= \yii\helpers\Url::toRoute(['upload/update-video']) ?>"/>
<input type="hidden" id="upload-url" value="<?= \yii\helpers\Url::toRoute(['upload/index']) ?>"/>
<input type="hidden" id="upload-image-url" value="<?= \yii\helpers\Url::toRoute(['upload/upload-image']) ?>"/>
<input type="hidden" id="upload-type" value="VOD"/>
<input type="hidden" id="playlist-id" value=""/>

<div id="uploader"></div>

