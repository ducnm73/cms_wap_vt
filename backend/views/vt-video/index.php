<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\assets\EditableAsset;
use backend\assets\FineUploaderVideoAsset;
use backend\models\VtVideo;
use common\libs\VtHelper;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use backend\models\VtMappingUser;
use common\helpers\Utils;
use mickgeek\actionbar\Widget as ActionBar;
use yii\widgets\Pjax;

EditableAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtVideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('backend', 'Videos');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="vt-video-index">

    <?php  echo $this->render('_search', ['searchModel' => $searchModel, 'state' => $state]); ?>
    <input type="hidden" id="approve-action" value="<?= Url::toRoute('vt-video/approve')?>"

    <div class="row">

                <div class="col-md-2" style="float: right;">

                    <?= ActionBar::widget([
                        'grid' => 'video-grid',
                        'templates' => [
                            '{bulk-actions}' => ['class' => 'col-xs-4', 'label' => Yii::t('backend','Thao tác')],
                        ],
                        'bulkActionsItems' => [
                            'status-approve' => Yii::t('backend','Duyệt'),
                            'status-disapprove' => Yii::t('backend','Từ chối duyệt'),
                            'update-outsource' => Yii::t('backend','Chuyển outsource'),
                            'update-created-by' => Yii::t('backend','Cập nhật kênh'),
                            'update-cp' => Yii::t('backend','Cập nhật CP'),
                        ],
                        'bulkActionsOptions' => [
                            'options' => [
                                'status-approve' => [
                                    'url' => Url::toRoute(['batch-approve', 'action' => 'approve']),
                                    'disabled' => !Yii::$app->user->can('approve-video'),
                                    'data-confirm' => Yii::t('backend','Bạn có chắc chắn muốn phê duyệt các bản ghi đã chọn ?'),
                                ],
                                'status-disapprove' => [
                                    'url' => Url::toRoute(['batch-approve', 'action' => 'delete']),
                                    'disabled' => !Yii::$app->user->can('approve-video'),
                                    'data-confirm' => Yii::t('backend','Bạn có chắc chắn muốn từ chối duyệt các bản ghi đã chọn ?'),
                                ],
                                'update-created-by' => [
                                    'url' => Url::toRoute(['batch-update-created-by']),
                                    'disabled' => !Yii::$app->user->can('approve-video'),
                                    'data-confirm' => Yii::t('backend','Bạn có chắc chắn muốn cập nhật các bản ghi đã chọn ?'),
                                ],
                                'update-outsource' => [
                                    'url' => Url::toRoute(['batch-update-outsource']),
                                    'disabled' => !Yii::$app->user->can('approve-video'),
                                    'data-confirm' => Yii::t('backend','Bạn có chắc chắn muốn chuyển outsource các bản ghi đã chọn ?'),
                                ],
                                'update-cp' => [
                                    'url' => Url::toRoute(['batch-update-cp']),
                                    'disabled' => !Yii::$app->user->can('approve-video'),
                                    'data-confirm' => Yii::t('backend','Bạn có chắc chắn muốn cập nhật CP các bản ghi đã chọn ?'),
                                ],
                            ],
                            'class' => 'form-control',
                            'style' => 'width: 150px'
                        ],
                    ]) ?>
                </div>
                <div class="col-md-3" style="float: right;">
                    <?= \kartik\select2\Select2::widget([
                        'name' => 'created_by',
                        'model' => $searchModel,
                        'options' => ['placeholder' => '---'.Yii::t('backend','Người tạo (Kênh)').'---', 'id' => 'batch_created_by'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading ...') . "'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['user/search']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term }; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(result) { return result.text; }'),
                            'templateSelection' => new JsExpression('function (result) { return result.text; }')
                        ]
                    ]) ?>
                </div>
                <div class="col-md-3"  style="float: right;">
                    <?= \kartik\select2\Select2::widget([
                        'name' => 'cp_id',
                        'model' => $searchModel,
                        'data' => ArrayHelper::map(\backend\models\VtCp::getAllCp(), 'id', 'cp_code'),
                        'options' => [
                            'placeholder' => Yii::t('app', '---Chọn CP---'),
                            'id' => 'batch_cp_id',
                            'multiple' => false,
                        ],
                    ]) ?>
                </div>

    </div>

    <?= \backend\widgets\AwsGridView::widget([
        'id' => 'video-grid',
        'dataProvider' => $dataProvider,
        'summaryOptions' => ['class' => 'summary'],
        //        'filterModel' => $searchModel,
        'columns' => [
            //            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\CheckboxColumn'],
            [
                'attribute' => 'id',
                'label' => Yii::t('backend','ID'),
                'format' => 'raw',
                'headerOptions' => ['width' => '5%'],

                'value' => function ($model) {
                    return $model->id;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_by',
                    'initValueText' => $searchModel->getCreatedByName(),
                    'options' => ['placeholder' => Yii::t('backend','Người tạo').' ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading ...') . "'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::toRoute(['user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term }; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(result) { return result.text; }'),
                        'templateSelection' => new JsExpression('function (result) { return result.text; }')
                    ]
                ])

            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Tên'),
                'format' => 'raw',
                'headerOptions' => ['width' => '30%'],
                'value' => function ($model) {
                    return '<b>'.Yii::t('backend','Tiêu đề').': </b><a href="' . Url::toRoute(['vt-video/update', 'id' => $model->id, 'home-url' => base64_encode(Yii::$app->request->url)]) . '">'
                        . Html::encode($model->name)
                        . '</a>'
                        .'<p><b>'.Yii::t('backend','Mô tả').': </b>' . (!empty($model->description) ? $model->description : 'N/A') .'</p>';
                },
            ],

            [
                'attribute' => Yii::t('backend','Thông tin'),
                'format' => 'raw',
                'headerOptions' => ['width' => '20%'],
                'value' => function ($model) use ($syncUsers) {
                    $reviewUser = Yii::t('backend','Chưa duyệt');
                    if(intval($model->attributes) > 1){
                        if(array_key_exists($model->attributes, $syncUsers)){
                            $reviewUser = $syncUsers[$model->attributes] . ' '.Yii::t('backend','đã duyệt');
                        }else{
                            $reviewUser = Yii::t('backend','Đã duyệt');
                        }
                    }

                    return
                        '<p><strong>'.Yii::t('backend','Thể loại').': </strong>' .(($model->category)?htmlentities($model->category->name):'N/A'). '</p>'
                        . '<p><strong>'.Yii::t('backend','Lượt xem').': </strong>' . $model->play_times . '</p>'
                        . '<p><strong>'.Yii::t('backend','Lượt thích').': </strong>' . $model->like_count . '</p>'
                        . '<p><strong>'.Yii::t('backend','Resolution').': </strong>' . $model->resolution . '</p>'
                        . '<p><strong>'.Yii::t('backend','Thời lượng').': </strong>' . Utils::durationToStr($model->duration) . '</p>'
                        . '<p><strong>'.Yii::t('backend','Phê duyệt trên Kho').': </strong>' . $reviewUser . '</p>';


                },
                'filter' => \kartik\daterange\DateRangePicker::widget([
                    'model'=>$searchModel,
                    'name'=>'range_created_at',
                    'attribute' => 'created_at',
                    'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'timePicker'=>true,
                        'timePicker24Hour' => true,
                        'locale'=>['format'=>'Y-m-d H:i:s']
                    ]
                ])
            ],

            [
                'attribute' => 'published_time',
                'label' => Yii::t('backend','Thời gian xuất bản'),
                'format' => 'raw',
                'headerOptions' => ['width' => '20%'],
                'value' => function ($model) {

                    if($model->status == VtVideo::STATUS_DRAFT){
                        switch ($model->convert_status) {
                            case VtVideo::CONVERT_STATUS_TEMP:
                                $status = Yii::t('backend','Upload lỗi');
                                break;
                            case VtVideo::CONVERT_STATUS_DRAFT:
                                $status = Yii::t('backend','Chờ convert');
                                break;
                            case VtVideo::CONVERT_STATUS_SUCCESS:
                                $status = Yii::t('backend','Chờ duyệt');
                                break;
                            case VtVideo::CONVERT_STATUS_CONVERTING:
                                $status = Yii::t('backend','Đang convert');
                                break;
                            case VtVideo::CONVERT_STATUS_FAIL_ORG:
                                $status = Yii::t('backend','File lỗi');
                                break;
                            case VtVideo::STATUS_FILE_INFO_FAIL:
                                $status = Yii::t('backend','File không đạt chất lượng');
                                break;
                            default:
                                $status = Yii::t('backend','Convert lỗi');
                                break;
                        }
                    }else{
                        $status = ((array_key_exists($model->status, Utils::getInterlizationParams('video.status.dropdown.value', true, 'backend'))? Utils::getInterlizationParams('video.status.dropdown.value', true, 'backend')[$model->status]:''));
                    }

                    return '<p><strong>'.Yii::t('backend','Thời gian tạo').': </strong>' . $model->created_at . '</p>'
                        . '<p><strong>'.Yii::t('backend','Thời gian xuất bản').': </strong>' . $model->published_time . '</p>'
                        . '<p><strong>'.Yii::t('backend','Thời gian hẹn giờ').': </strong>' . $model->show_times . '</p>'
                        . '<p><strong>'.Yii::t('backend','Trạng thái').': </strong>' . $status  . '</p>'
                        . '<p><strong>'.Yii::t('backend','Người tạo').': </strong>' . (($model->user) ? (($model->user->full_name) ? $model->user->full_name : $model->user->msisdn) : '') . '</p>'
                        . '<p><strong>'.Yii::t('backend','CP Code').': </strong>' . (!empty($model->cp_code)? $model->cp_code: 'N/A') . '</p>'
                        ;
                },
                'filter' => [
                    '0' => Yii::t('backend','Khách hàng'),
                    '1' => Yii::t('backend','Quản trị viên')
                ]
            ],

            [
                'attribute' => 'is_hot',
                'label' => Yii::t('backend','Đề xuất'),
                'format' => 'raw',
                'headerOptions' => ['width' => '10%'],
                'contentOptions' => ['style' => 'text-align: center'],
                'value' => function ($model) {
                    $str = '';
                    if ($model->is_hot) $str .= "<img src='/img/icon-hot.png' width='24' height='24' />";
                    return $str;
                },
                'filter' =>\common\helpers\Utils::getInterlizationParams('boolean.dropdown.value', 'true', 'backend'),
            ],
            [
                'attribute' => 'is_recommend',
                'label' => Yii::t('backend','Thịnh hành'),
                'format' => 'raw',
                'headerOptions' => ['width' => '10%'],
                'contentOptions' => ['style' => 'text-align: center'],
                'value' => function ($model) {
                    $str = '';
                    if ($model->is_recommend) $str .= "<img src='/img/icon-focus.png' width='24' height='24' />";
                    return $str;
                },
                'filter'
                => \common\helpers\Utils::getInterlizationParams('boolean.dropdown.value','true','backend'),

            ],
            [
                'attribute' => 'updated_by',
                'label' => Yii::t('backend','Ảnh'),
                'format' => 'raw',
                'headerOptions' => ['width' => '15%'],

                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_COVER), ['class' => 'image-grid-display']);
                },
                //                'filter' => \yii\helpers\ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username'),
                'filter' => Html::activeDropDownList($searchModel, 'updated_by', (['N/A' => Yii::t('backend', 'Chưa tác động')] + ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username')),['class'=>'form-control','prompt' => Yii::t('backend','Tác động bởi')]),

            ],


            //            [
            //                'attribute' => 'attributes',
            //                'label' => 'Phê duyệt trên Kho',
            //                'headerOptions' => ['width' => '10%'],
            //                'format' => 'raw',
            //                'value' => function ($model) use ($syncUsers) {
            //                    if(intval($model->attributes) > 1){
            //                        if(array_key_exists($model->attributes, $syncUsers)){
            //                            return $syncUsers[$model->attributes] . ' đã duyệt';
            //                        }else{
            //                            return 'Đã duyệt';
            //                        }
            //
            //                    }else{
            //                        return  'Chưa duyệt';
            //                    }
            //
            //                },
            //                'filter' => Html::activeDropDownList($searchModel,
            //                    'attributes',
            //                    (['1' => 'Chưa duyệt'] + ArrayHelper::map(VtMappingUser::getAllUser(), 'id', 'username')),
            //                    ['class'=>'form-control','prompt' => 'Tất cả']
            //                ),
            //
            //            ],


            [
                'attribute' => 'review_by',
                'label' => Yii::t('backend','Thao tác'),
                'headerOptions' => ['width' => '10%'],

                'format' => 'raw',
                //                'filter' => \yii\helpers\ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username'),

                'filter' => Html::activeDropDownList($searchModel, 'review_by', ArrayHelper::map(\common\models\AuthUser::getAllUser(), 'id', 'username'),['class'=>'form-control','prompt' => Yii::t('backend','Xem lướt bởi')]),


                'value' => function ($model) {

                    $state = Yii::$app->session->get('state');
                    $sendApproveBtn = '<button style="width: 90px" onclick="approveVideo(' . $model->id . ', \'waiting\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Gửi duyệt').'</button>';
                    $publishBtn = '';
                    $deleteBtn = '';
                    $reconvertBtn = '';
                    $reviewBtn = '';
                    if ($model->checkApprovePermission()) {
                        $publishBtn = '<button style="width: 90px" onclick="approveVideo(' . $model->id . ', \'approve\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Xuất bản').'</button>';
                        $deleteBtn = '<button style="width: 90px" onclick="approveVideo(' . $model->id . ', \'delete\')" type="button" class="btn btn-danger video-btn-publish">'.Yii::t('backend','Xóa').'</button>';
                        $reconvertBtn = '<button style="width: 90px" onclick="approveVideo(' . $model->id . ', \'reconvert\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Convert lại').'</button>';
                        if($model->checkApprovePermission() && empty($model->review_by)){
                            $reviewBtn = '';// '<button style="width: 90px" id="reviewVideo-'. $model->id .'" onclick="approveVideo(' . $model->id . ', \'review\')" type="button" class="btn btn-success video-btn-publish">'.Yii::t('backend','Xem lướt').'</button>';
                        }
                    }
                    switch ($state) {
                        case 'temp':
                            return $sendApproveBtn . $deleteBtn . $reviewBtn;

                        case 'published':
                            return $deleteBtn . $reviewBtn;

                        case 'draft':
                            return $publishBtn . $deleteBtn . $reviewBtn;

                        case 'converting':
                            return $deleteBtn . $reviewBtn;

                        case 'error':
                            return $reconvertBtn . $reviewBtn;

                        case 'deleted':
                            //@todo: check trang thai de tra ve $sendApproveBtn hay $publishBtn
                            return $sendApproveBtn . $reviewBtn;

                    }

                },
            ],


        ],
    ]); ?>


</div>
