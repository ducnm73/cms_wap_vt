<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\VtVideo;
/* @var $this yii\web\View */
/* @var $model backend\models\VtCommentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row container">
    <?php $form = ActiveForm::begin([
        'action' => ['video-fee'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= Html::activeInput('text', $searchModel,
                'name',
                ['class' => 'form-control', 'placeholder' => '---'.Yii::t('backend','Tên Video').'---']
            ) ?>
        </div>

        <div class="col-md-6">
            <?= \kartik\daterange\DateRangePicker::widget([
                'model'=>$searchModel,
                'name'=>'range_created_at',
                'attribute' => 'created_at',
                'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'timePicker'=>true,
                    'timePicker24Hour' => true,
                    'locale'=>['format'=>'Y-m-d H:i:s'],
                ],
                'options' => [
                    'class'=>'form-control',
                    'placeholder' => '---'.Yii::t('backend','Thời gian tạo').'---'
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
    <div class="col-md-3">
        <?= Html::activeDropDownList(
                $searchModel,
                'category_id',
                (ArrayHelper::map(\backend\models\VtGroupCategory::getAllActiveCategory(\common\models\VtGroupCategoryBase::TYPE_VOD), 'id', 'name')),
                ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Chọn chuyên mục').'---'])?>
        </div>

        <div class="col-md-3">
            <?= \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'channel_id',
                'initValueText' => \common\models\db\VtChannelDB::findOne($searchModel->channel_id)->full_name,
                'options' => ['placeholder' => '---'.Yii::t('backend','Kênh').'---'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => false,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return '" .  Yii::t('backend', 'Loading ...') . "'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['user/get-channel-all-by-id']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term }; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                    'templateSelection' => new JsExpression('function (result) { return result.text; }')
                ]
            ]) ?>
        </div>

        <div class="col-md-3">
            <?= Html::activeDropDownList($searchModel,
                'price_play',
                ([0 => Yii::t('backend','Miễn phí')] + [1=>Yii::t('backend','Mất phí')]),
                ['class'=>'form-control','prompt' => '---'.Yii::t('backend','Giá xem').'---'])
            ?>
        </div>
    </div>

    <div class="col-md-12 mb-2">
        <?= Html::submitButton(Yii::t('backend','Tìm kiếm'), [
            'class' => 'btn btn-primary'
        ]) ?>
        <br><br>
    </div>
   
    <?php ActiveForm::end(); ?>
</div>