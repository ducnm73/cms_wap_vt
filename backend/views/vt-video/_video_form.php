<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\VtGroupTopic;
use backend\models\VtGroupCategory;
use backend\models\VtPackage;
use common\helpers\Utils;
use kartik\datetime\DateTimePicker;
use common\models\VtVideoBase;
use common\libs\S3Service;
use yii\bootstrap\BaseHtml;
use backend\models\VtVideo;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    "
    $('span.not-set').text('');
    $( document ).ready(function() {console.log($('#vtvideoviewdaybase-day').val())
    $('#vtvideoviewdaybase-day').on('click', function() { $('button.btn.btn-primary').attr('disabled','disabled'); });
        var id_user = $('#user-id').select2('data')[0].id;
        $('#channel_id_index' ).val(id_user);
        $('#user-id').on('change', function (e) {
            var id_user = $('#user-id').select2('data')[0].id;
            $('#channel_id_index' ).val(id_user);
        });    
    });
    "
);

$jsFunctionUrl = new \yii\web\JsExpression(
    'function(params) { 
            var strUser = document.getElementById("channel_id_index").value;
            return {q:params.term, channel_id: strUser} 
    }'
);

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'video-form']); ?>
<?= $form->errorSummary($model); ?>
<div class="form-group">
    <video id="preview-player" class="video-js vjs-theme-fantasy" controls autoplay>

        <?php $convertedFile = $model->getConvertVideo() ?>
        <?php if ($convertedFile): ?>
            <?php $path = $convertedFile->bucket . '/' . ltrim($convertedFile->path, '/'); ?>

            <?php if ($convertedFile->bucket == 'video2'): ?>
                <source src="http://s2.cloudstorage.com.vn/<?= $path ?>" type="video/mp4">
            <?php else: ?>
                <source src="<?= S3Service::generateCDNUrl($convertedFile) /*S3Service::generateStreamURL($model, S3Service::OBJCDN)*/ ?>"
                        type="application/x-mpegURL">
            <?php endif; ?>


        <?php endif; ?>
        <?= Yii::t('backend', 'Vui lòng chọn trình duyệt hỗ trợ HTML5') ?>
    </video>
    <?php $this->registerJs("var myPlayer = videojs('preview-player');"); ?>
</div>
<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

<?= $form->field($model, 'description')->textarea(['maxlength' => 1000]) ?>

<?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(VtGroupCategory::getMenuByType(VtGroupCategory::TYPE_VOD), 'id', 'name'), ['class' => 'form-control category-checkbox-list', 'prompt' => '--- ' . Yii::t('backend', 'Chọn thể loại') . ' ---']); ?>

<?php $user = \common\models\db\VtUserDB::findOne($model->created_by); ?>
<?= $form->field($model, 'created_by')->widget(Select2::classname(), [
    'initValueText' => $user ? $user->full_name : '',
    'options' => [
        'id' => 'user-id',
        'placeholder' => Yii::t('backend', 'Tên user') . ' ...'
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'language' => [
            'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . "...'; }"),
        ],
        'ajax' => [
            'url' => Url::toRoute(['/user/search']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term }; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(result) { return result.text; }'),
        'templateSelection' => new JsExpression('function (result) { return result.text; }'),
    ],
])->label(Yii::t('backend', 'Tên user'));
?>

<input type="text" id="channel_id_index" style="display: none;">
<?php $userChannel = \common\models\db\VtChannelDB::findOne($model->channel_id); ?>
<?= $form->field($model, 'channel_id')->widget(Select2::classname(), [
    'initValueText' => $userChannel ? $userChannel->full_name : '',
    'options' => [
        'id' => 'channel_id',
        'placeholder' => Yii::t('backend', 'Tên channel') . ' ...'
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => false,
        'language' => [
            'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . "...'; }"),
        ],
        'ajax' => [
            'url' => Url::toRoute(['user/get-channel-all-by-id']),
            'dataType' => 'json',
            'data' => new JsExpression($jsFunctionUrl)
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(result) { return result.text; }'),
        'templateSelection' => new JsExpression('function (result) { return result.text; }'),
    ],
])->label(Yii::t('backend', 'Tên channel'));
?>

<?=
$form->field($model, 'topics')->checkboxList(ArrayHelper::map(VtGroupTopic::getAllActiveTopic(VtGroupTopic::TYPE_VOD), 'id', 'name'), [
    'class' => 'form-control topic-checkbox-list',
    'prompt' => '--- ' . Yii::t('backend', 'Chọn chủ đề') . ' ---'
]);
?>

<?php $price = $model->price_play; ?>

<label for="price_free_vd" style="margin-right: 20px;"><?= Yii::t('backend', 'Video free') ?></label>
<input type="radio" id="price_free_vd" name="pricevdvt" onclick="freevideovt()"
       style="margin-right: 30px" <?php if ($price == 0 || $price == null) {
    echo 'checked';
} ?>>
<label for="price_no_free_vd" style="margin-right: 20px"><?= Yii::t('backend', 'Paid video') ?></label>
<input type="radio" id="price_no_free_vd" name="pricevdvt" onclick="paidvideovt()"
       style="margin-top: 0" <?php if ($price > 0) {
    echo 'checked';
} ?>>

<?php if ($price == 0 || $price == null) {
    echo $form->field($model, 'price_play')->textInput(['maxlength' => 5, 'readonly' => true]);
} else {
    echo $form->field($model, 'price_play')->textInput(['maxlength' => 5]);
} ?>

<?= $form->field($model, 'suggest_package_id')->dropDownList(ArrayHelper::map(VtPackage::getListPackage(), 'id', 'name'), ['class' => 'form-control package-checkbox-list', 'prompt' => '--- ' . Yii::t('backend', 'Chọn gói cước quảng cáo') . ' ---']); ?>


<?= $form->field($model, 'tag')->textInput(['maxlength' => 255]) ?>

<?= $form->field($model, 'seo_title')->textInput(['maxlength' => 200]) ?>

<?= $form->field($model, 'seo_description')->textarea(['maxlength' => 500]) ?>

<?= $form->field($model, 'seo_keywords')->textarea(['maxlength' => 500]) ?>

<?=
$form->field($model, 'published_time')->widget(DateTimePicker::classname(), [
    'options' => [
        'value' => ($model->published_time) ? $model->published_time : date("Y-m-d H:i:s"),
    ],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd hh:ii:ss',
        'todayHighlight' => true,
        'todayBtn' => true,
        'orientation' => 'auto left',
    ]
]);
?>
<?php if (!$model->isNewRecord && !empty($model->syn_id)): ?>
    <?=
    $form->field($model, 'show_times')->widget(DateTimePicker::classname(), [
        'options' => [
            'value' => ($model->show_times) ? $model->show_times : '',
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'todayHighlight' => true,
            'todayBtn' => true,
            'orientation' => 'auto left',
        ]
    ]);
    ?>
<?php endif ?>
<div class="snapshot-choice">
    <?php
    $arr = explode('.', $model->file_path);
    $snapshotPath = $arr[0];

    $radioArr = [];
    for ($i = 1; $i <= 5; $i++) {
        $radioArr[] = (!empty($model->syn_id)) ? $snapshotPath . '_' . $i . '.jpg' : 'snapshot/' . $snapshotPath . '_' . $i . '.jpg';
    }
    ?>


    <?=
    $form->field($model, 'snapshot')
        ->radioList($radioArr, [
                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                    $return = '<label class="modal-radio">';
                    $return .= '<input type="radio" name="' . $name . '" value="' . $label . '">';
                    $return .= '<i></i>';
                    $return .= "<img class='preview-image' src='" . S3Service::generateWebUrl($model->file_bucket, $label) . "'>";
                    $return .= '</label>';

                    return $return;
                }
            ]
        )
        ->label(false);
    ?>

</div>
<div class="clearfix"></div>

<div class="form-group">
    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
        <img class="preview-image" src="<?= S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
    <?php endif ?>
</div>
<?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>

<?= $form->field($model, 'is_active')->checkbox() ?>

<?= $form->field($model, 'is_check')->checkbox() ?>

<?= $form->field($model, 'is_hot')->checkbox() ?>

<?= $form->field($model, 'is_recommend')->dropDownList(
    [
        VtVideo::RECOMMEND_DYNAMIC => Yii::t('backend', 'Động'),
        VtVideo::RECOMMEND_FIX => Yii::t('backend', 'Tĩnh'),
    ],
    ['class' => 'form-control', 'prompt' => '---' . Yii::t('backend', 'Chọn thịnh hành') . '---']
) ?>

<?= $form->field($model, 'is_no_copyright')->checkbox() ?>


<?= $form->field($model, 'convert_priority')->textInput() ?>

<div class="form-group field-vtvideo-reason">
    <label class="control-label" for="vtvideo-reason"><?= Yii::t('backend', 'Lý do từ chối') ?></label>
    <?= BaseHtml::dropDownList('reason-choice', null, \common\helpers\Utils::getInterlizationParams('reason.unapprove', true, 'backend'),
        ['id' => 'reason-choice', 'class' => 'form-control']

    ) ?>
</div>

<?php

$this->registerJs(
    "$('#reason-choice').on('change', function() { if($(this).val() !== '0') {\$('#vtvideo-reason').val($(this).val())}; });"

);

?>

<?= $form->field($model, 'reason')->textarea(['maxlength' => 1000])->label(Yii::t('backend', 'Mô tả lỗi')) ?>

<div class="form-group">
    <?php if (!$model->isApprove() || $model->checkApprovePermission()): ?>
        <?= Html::submitButton(Yii::t('backend', 'Lưu'), ['class' => 'btn btn-success', 'name' => 'submitSave', 'value' => 'save']) ?>

        <?php if (!$model->isApprove() && !$model->isDraft()): ?>
            <?= Html::submitButton(Yii::t('backend', 'Gửi duyệt'), ['class' => 'btn btn-primary', 'name' => 'submitApprove', 'value' => 'send_approve']) ?>
        <?php endif; ?>

        <?php if (!$model->isApprove() && $model->checkApprovePermission() && $model->isConverted()): ?>
            <?= Html::submitButton(Yii::t('backend', 'Duyệt'), ['class' => 'btn btn-warning', 'name' => 'submitPublish', 'value' => 'publish']) ?>
        <?php endif; ?>

        <?php if (!$model->isNewRecord): ?>
            <?php if (($model->isOwner() || $model->checkApprovePermission()) && !$model->isDelete()): ?>
                <?= Html::submitButton(Yii::t('backend', 'Hạ xuống'), ['class' => 'btn btn-danger', 'id' => 'btn-remove-approve', 'name' => 'submitDelete', 'value' => 'delete']) ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
</div>
<?php ActiveForm::end(); ?>

<?php if ($isComplained): ?>
    <div class="form-group" id="reApproveFeedback"
         style="border: 2px solid #3ea7a0; border-radius: 5px; padding: 30px;">
        <h4 style="font-weight: bold; text-align: center;"><?= Yii::t('backend', 'Duyệt lại theo khiếu nại uploader') ?></h4>
        <p style="font-weight: bold;"><?= Yii::t('backend', 'Khiếu nại của uploader') ?></p>
        <input type="hidden" id="approve-video-after-review-action" value="/vt-upload-feedback/approve">
        <p style="resize: none; width: 100%;"><?= Html::encode($feedback->feedback_content) ?></p>
        <p style="font-weight: bold;"><?= Yii::t('backend', 'File đính kèm') ?></p>
        <?php
        if (!empty($feedback->path)):
            $urlImg = S3Service::generateWebUrl($feedback->bucket, $feedback->path);
            ?>
            <a href="<?= $urlImg; ?>" target="_blank"><img class="preview-image" src="<?= $urlImg; ?>"/></a>
        <?php
        else:
            ?> <p><?= Yii::t('backend', 'Không có file đính kèm') ?></p>
        <?php
        endif;
        ?>

        <div style="padding: 10px 0; text-align: center;">
            <button style="width: 115px;" onclick="reApproveVideo(<?= $feedback->id; ?>, 'approve')" type="button"
                    class="btn btn-success video-btn-publish"><?= Yii::t('backend', 'Duyệt lại') ?></button>
            <button style="width: 115px;" onclick="declineVideoAfterReview(<?= $feedback->id; ?>, 'delete')"
                    type="button"
                    class="btn btn-danger video-btn-publish"><?= Yii::t('backend', 'Không duyệt') ?></button>
        </div>
    </div>
<?php endif; ?>
<div id="videoModal" class="modal">
    <div class="modal-content">
        <div>
            <span class="close">&times;</span>
            <p><?= Yii::t('backend', 'Lý do không duyệt lại video') ?>:</p>
        </div>
        <div>
            <textarea rows="4" id="declineVideoReason" style="width: 100%; resize: none;"></textarea>
        </div>
        <input type="hidden" id="hiddenId"/>
        <input type="hidden" id="hiddenAction"/>
        <input type="button" id="reasonDecline" onclick="submitReasonReDeclineVideo()"
               value="<?= Yii::t('backend', 'Lưu lại') ?>" class="btn btn-primary" style="margin: 0 200px;"/>
    </div>
</div>

<script>
    function freevideovt() {
        document.getElementById("vtvideo-price_play").value = 0;
        document.getElementById("vtvideo-price_play").readOnly = true;
    }

    function paidvideovt() {
        document.getElementById("vtvideo-price_play").value = <?= $price ?>;
        document.getElementById("vtvideo-price_play").readOnly = false;
        document.getElementById('vtvideo-price_play').removeAttribute('readonly');
    }
</script>