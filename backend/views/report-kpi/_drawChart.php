<?php

/* @var $this yii\web\View */
/* @var $model backend\models\ReportKpiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style type="text/css">
    .ranges li:last-child { display: none; }
</style>

<div class="report-kpi-search">
    <div class="col-md-2">
        <p><strong> <?= Yii::t('backend','Ngày báo cáo')?> </strong></p>
    </div>
    <div class="col-md-3">
        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 85%">
            <i class="fa fa-calendar"></i>&nbsp;
            <span></span> <i class="fa fa-caret-down"></i>
        </div>

        <script type="text/javascript">
            $(function() {
                var start = moment();
                var end = moment();

                function cb(start, end) {
                    $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                }
                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    format: 'DD/MM/YYYY',
                    name: 'report_day',
                    ranges: {
                        '<?= Yii::t('backend','Hôm nay')?>': [moment(), moment()],
                        '<?= Yii::t('backend','Hôm qua')?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        '<?= Yii::t('backend','7 ngày gần đây')?>': [moment().subtract(6, 'days'), moment()],
                        '<?= Yii::t('backend','30 ngày gần đây')?>': [moment().subtract(29, 'days'), moment()],
                        '<?= Yii::t('backend','Tháng này')?>': [moment().startOf('month'), moment().endOf('month')],
                        '<?= Yii::t('backend','Tháng trước')?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);
                cb(start, end);
            });
        </script>
    </div>
    <div class="col-md-1">
        <button class="btn btn-primary drawChartButton"><?= Yii::t('backend','Vẽ biểu đồ')?></button>
    </div>
    <div class="col-md-6">
        <button class="btn btn-primary exportChartButton" style="float: right;"><?= Yii::t('backend','Xuất file')?></button>
    </div>
</div>