<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ReportKpiSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .input-mini{
        min-width: 200px !important;
    }
</style>
<div class="report-kpi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3">
        <?= Html::activeDropDownList($model,
            'type',
            Yii::$app->params['report.kpi.type'],
            ['class' => 'form-control', 'prompt' => '---'.Yii::t('backend','Chọn loại KPI').'---'])
        ?>
    </div>
    <div class="col-md-3">
        <?= \kartik\daterange\DateRangePicker::widget([
            'model' => $model,
            'name' => 'report_day',
            'attribute' => 'report_day',
            'value' => date('Y-m-d') . ' - ' . date('Y-m-d'),
            'convertFormat' => true,
            'pluginOptions' => [
                'timePicker' => false,
                'timePicker24Hour' => true,
                'locale' => ['format' => 'Y-m-d']
            ],
            'options' => [
                'class' => 'form-control',
                'placeholder' => '---'.Yii::t('backend','Khoảng thời gian báo cáo').'---'
            ]
        ])
        ?>
    </div>

    <div class="col-md-8">
        <p></p>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend','Lọc dữ liệu'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
    $("#reportkpisearch-report_day").prop("readonly", true);
JS;
$this->registerJs($script);
?>