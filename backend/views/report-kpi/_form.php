<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ReportKpi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-kpi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'report_day')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'report_hour')->textInput() ?>

    <?= $form->field($model, 'avg_time')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'total_trans')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'failure_trans')->textInput(['maxlength' => 15]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
