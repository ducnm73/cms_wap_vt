<?php

use backend\assets\CharJsAsset;
use yii\helpers\Html;
use yii\grid\GridView;
CharJsAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReportKpiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Report Kpis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-kpi-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="javascript:void(0)" onclick="showMe('#menu1');hideMe('#menu2')"><?php echo Yii::t('backend', 'Chi tiết')?></a></li>
        <li><a data-toggle="tab" href="javascript:void(0)" onclick="showMe('#menu2');hideMe('#menu1')">Dashboard</a></li>
    </ul>
    <div class="tab-content">
        <div id="menu1" >
            <?php
                if (strpos(Yii::$app->request->url, "?") > 0) {
                    $url = Yii::$app->request->url."&export=excel";
                } else {
                    $url = Yii::$app->request->url."?export=excel";
                }
            ?>

            <form action="/report-kpi/export-excel" method="get" id="form-excel" enctype="multipart/form-data">
                <input type="hidden" name="ReportKpiSearch[type]" value="<?php echo  $searchModel->type ?>">
                <input type="hidden" name="ReportKpiSearch[id]" value="<?php echo  $searchModel->id ?>">
                <input type="hidden" name="ReportKpiSearch[report_day]" value="<?php echo  $searchModel->report_day ?>">
                <input type="hidden" name="ReportKpiSearch[report_hour]" value="<?php echo  $searchModel->report_hour ?>">
                <input type="hidden" name="ReportKpiSearch[avg_time]" value="<?php echo  $searchModel->avg_time ?>">
                <p>
                    <?= Html::a(Yii::t('backend', 'EXPORT EXCEL'), 'javascript:void(0)', [ "target"=>"_blank", 'onclick'=> 'document.getElementById("form-excel").submit()'  ,  'class' => 'btn btn-success', 'style' => 'float: right; margin: 10px;']) ?>
                </p>
            </form>

            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            <div style="clear: both"></div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pager' => [
                    'firstPageLabel' => Yii::t('backend', 'First'),
                    'lastPageLabel'  => Yii::t('backend', 'Last')
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'type',
                        'label' => Yii::t('backend', 'Loại KPI'),
                        'value' => function ($model, $key, $index, $widget) {
                            $model->type = strtoupper($model->type);
                            return (array_key_exists($model->type, Yii::$app->params['report.kpi.type'])) ? Yii::$app->params['report.kpi.type'][$model->type] : Yii::t('backend', "(Không xác định)");
                        },
                        'filter' => Yii::$app->params['report.kpi.type']
                    ],
                    'report_day',
                    'report_hour',
                    'avg_time',
                    'total_trans',
                    'failure_trans',
                    'success_rate',
                    'failure_rate',
                    'avg_bandwidth',
                ],
            ]); ?>
        </div>

        <div id="menu2" style="display: none">
            <?php echo $this->render('_drawChart'); ?>
            <div style="clear: both"></div>
            <div>
                <div class="col-md-6">
                    <canvas id="canvas_pageloadForApp"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_pageload"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_successRate"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_successRateForWeb"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_avgBandwidth"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_failedRate"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_videoEncodingTime"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_encodeFailureRate"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_videoWaitingTime"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_videoBufferRate"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_videoBufferTimeRate"></canvas>
                </div>
                <div class="col-md-6">
                    <canvas id="canvas_videoBufferOver3sRate"></canvas>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
</div>