<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ReportKpi */

$this->title = Yii::t('backend', 'Create Report Kpi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Report Kpis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-kpi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
