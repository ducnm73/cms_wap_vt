<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtUser */

$this->title = Yii::t('backend', 'Tạo mới {modelClass}', [
    'modelClass' => Yii::t('backend', 'User'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
