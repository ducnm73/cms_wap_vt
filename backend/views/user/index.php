<?php

use backend\models\UserSearch;
use yii\grid\GridView;
use common\models\AuthUser;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $searchModel UserSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = Yii::t('backend', 'User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=
        Html::a(Yii::t('backend', Yii::t('backend','Tạo mới').' {modelClass}', [
                    'modelClass' => $this->title,
                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'username',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['user/update', 'id' => $model->id]) .'">'.htmlentities($model->username).'</a>';
                },
            ],

            [
                'attribute' => 'user_id',
                'format' => 'raw', //raw, html
                'value' => function ($model) {
                    return $model->getWapUserName();
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw', //raw, html
                'content' => function($data) {
                    return ($data->status == AuthUser::STATUS_ACTIVE) ? Yii::t('backend', 'Kích hoạt') : Yii::t('backend', 'Không kích hoạt');
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw', //raw, html
                'content' => function($data) {
                    return date('H:i:s d/m/Y', $data->created_at);
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]);
    ?>

</div>
