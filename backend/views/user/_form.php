<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use cp\models\VtCp;

/* @var $this yii\web\View */
/* @var $model backend\models\VtUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $url = \yii\helpers\Url::toRoute(['user/search']);
?>

<div class="vt-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'password_hash')->passwordInput() ?>

    <?= $form->field($model, 're_password')->passwordInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>


    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'initValueText' => $model->getWapUserName(),
        'options' => ['placeholder' => Yii::t('backend','Tìm kiếm User').' ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '". Yii::t('backend', "Loading ...") . "'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term }; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }')
        ]

    ])->label(Yii::t('app', 'Chọn User WAP'));
    ?>

    <?= $form->field($model, 'cp_id')->dropDownList(ArrayHelper::map(VtCp::getAllCp(), 'id', 'name'), ['class' => 'form-control package-checkbox-list', 'prompt'=>'--- '.Yii::t('backend','Chọn đối tác').' ---']); ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
