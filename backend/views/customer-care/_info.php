<?php
use yii\helpers\Url;

?>

<table class="table table-striped table-bordered" style="width: 40%">
    <tr>
        <td width="40%"><?= Yii::t('backend', 'Gói cước') ?></td>
        <td width="60%">

            <?php if($subModel): ?>
                <?= $subModel['package_name'] ?> - <a href="<?= Url::toRoute(['customer-care/sub', 'VtUser[msisdn]' => $model->msisdn ])?>"><?= Yii::t('backend', 'Hủy') ?></a>
            <?php else: ?>
                <?= Yii::t('backend', 'Chưa sử dụng') ?> - <a href="<?= Url::toRoute(['customer-care/sub', 'VtUser[msisdn]' => $model->msisdn ])?>"><?= Yii::t('backend', 'Đăng ký') ?></a>
            <?php endif;?>

        </td>
    </tr>

    <tr>
        <td><?= Yii::t('backend', 'Tên kênh') ?></td>
        <td>

            <?= (isset($model->full_name)) ? $model->full_name : 'N/A'?>

        </td>
    </tr>


</table>


