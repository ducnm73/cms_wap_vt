<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtGroupTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Chăm sóc khách hàng');
$this->params['breadcrumbs'][] = $this->title;

$actionId = Yii::$app->controller->action->id;
?>
<div class="vt-group-topic-index">

    <?= $this->render('_form_search', [
        'model' => $model,
    ]) ?>

    <?php  echo $this->render('_info', ['model' => $model, 'subModel' => $subModel]); ?>


    <?= $this->render('_tab', [
        'model' => $model,
    ]) ?>

    <?php $sms01 = \common\models\VtSmsMessageBase::getConfig('SMS_01');?>

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => [''],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= \kartik\daterange\DateRangePicker::widget([
                'model'=>$searchModel,
                'name'=>'sent_time',
                'attribute' => 'sent_time',
                'value'=> date('Y-m-00').' - '. date('Y-m-d'),
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
                'options' => [
                    'class'=>'form-control',
                    'placeholder' => Yii::t('backend','Thời gian')
                ]
            ]) ?>
        </div>





        <div class="col-md-3">
            <?= Html::submitButton(Yii::t('backend','Tìm kiếm'), [
                'name' => 'search',
                'value' => 'search',
                'class' => 'btn btn-primary'
            ]) ?>
            <?= Html::submitButton(Yii::t('backend','Xuất Excel'), [
                'name' => 'export',
                'value' => 'export',
                'class' => 'btn btn-success'
            ]) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'pager' => [
            'firstPageLabel' => Yii::t('backend','Đầu'),
            'lastPageLabel'  => Yii::t('backend','Cuối')
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['width' => '5%'],
            ],
            [
                'attribute' => 'sent_time',
                'headerOptions' => ['width' => '20%']
            ],
            [
                'attribute' => 'msisdn',
                'headerOptions' => ['width' => '15%'],
            ],
            [
                'attribute' => 'message',
                'headerOptions' => ['width' => '30%'],
                'value' => function ($model) {
                    $sms01 = \common\models\VtSmsMessageBase::getConfig('SMS_01');
                    $arrSMS01 = explode('#password', $sms01);
                    //Giau mat khau cua khach hang
                    if (strpos($model->message, $arrSMS01[0]) === 0 && strpos($model->message, $arrSMS01[1]) > 0) {
                        return $sms01;
                    } else {
                        $smsOTP = \common\models\VtConfigBase::getConfig("OTP_TEMPLATE", '');
                        $arrSMSOTP = explode('%otp%', $smsOTP);
                        //OTP cua khach hang
                        if (strpos($model->message, $arrSMSOTP[0]) === 0) {
                            return $smsOTP;
                        } else {
                            return $model->message;
                        }
                    }
                },
            ],
            [
                'attribute' => 'channel',
                'headerOptions' => ['width' => '10%'],
                'filter' => false
            ],
            [
                'attribute' => 'process_id',
                'headerOptions' => ['width' => '10%'],
                'filter' => false
            ],
        ],
    ]); ?>

</div>
