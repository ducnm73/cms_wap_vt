<?php
use yii\helpers\Url;

$actionId = Yii::$app->controller->action->id;

?>


<ul class="nav nav-pills">
    <li role="presentation" class="<?= ($actionId == 'index')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/index', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Lịch sử giao dịch')?></a></li>
    <li role="presentation" class="<?= ($actionId == 'mt-his')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/mt-his', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Lịch sử MT')?></a></li>
    <li role="presentation" class="<?= ($actionId == 'mo-his')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/mo-his', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Lịch sử MO')?></a></li>
    <li role="presentation" class="<?= ($actionId == 'history-view')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/history-view', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Lịch sử xem')?></a></li>
    <li role="presentation" class="<?= ($actionId == 'history-device')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/history-device', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Lịch sử truy cập')?></a></li>
    <!--<li role="presentation" class="<?= ($actionId == 'event-view')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/event-view', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Điểm sự kiện')?></a></li> -->
    <li role="presentation" class="<?= ($actionId == 'sub')? 'active' : '' ?>"><a href="<?= Url::toRoute(['customer-care/sub', 'VtUser[msisdn]' => $model->msisdn ]); ?>"><?= Yii::t('backend','Đăng ký/Hủy')?></a></li>

</ul>

