<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupTopic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-group-topic-form">
    <?php $form = ActiveForm::begin(['action' => Url::toRoute([\Yii::$app->request->getPathInfo()]), 'method' => 'get']); ?>

    <div class="row show-grid">
        <div class="col-md-4">
            <?= $form->field($model, 'msisdn')->textInput(['maxlength' => 255, 'placeholder' => Yii::t('backend', 'Số điện thoại')])->label(false) ?>
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Tìm kiếm'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
