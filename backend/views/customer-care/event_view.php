<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtGroupTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Chăm sóc khách hàng');
$this->params['breadcrumbs'][] = $this->title;

$actionId = Yii::$app->controller->action->id;
?>

<div class="vt-group-topic-index">

    <?= $this->render('_form_search', [
        'model' => $model,
    ]) ?>

    <?php  echo $this->render('_info', ['model' => $model, 'subModel' => $subModel]); ?>

    <?= $this->render('_tab', [
        'model' => $model,
    ]) ?>


    <table class="table">
        <thead>
        <tr>
            <th><?= Yii::t('backend','Tương tác')?></th>
            <th width="160">Số điểm</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= Yii::t('backend','Bình luận')?></td>
            <td><?php echo  $eventInfo[0]['comment_point'] ?></td>
        </tr>
        <tr>
            <td><?= Yii::t('backend','Xem video')?></td>
            <td><?php echo  $eventInfo[0]['watch_point'] ?></td>
        </tr>
        <tr>
            <td><?= Yii::t('backend','Đăng tải video')?></td>
            <td><?php echo  $eventInfo[0]['upload_point'] ?></td>
        </tr>

        <tr>
            <td><?= Yii::t('backend','Chia sẻ video')?></td>
            <td class="alRight"><?php echo  $eventInfo[0]['share_point'] ?></td>
        </tr>
        <tr>
            <td><?= Yii::t('backend','Yêu thích video')?></td>
            <td class="alRight"><?php echo  $eventInfo[0]['like_point'] ?></td>
        </tr>
        <tr>
            <td><?= Yii::t('backend','Theo dõi kênh')?></td>
            <td><?php echo  $eventInfo[0]['follow_point'] ?></td>
        </tr>
        <tr>
            <td><?= Yii::t('backend','Đăng ký dịch vụ')?></td>
            <td><?php echo  $eventInfo[0]['register_point'] ?></td>
        </tr>
        <tr>
            <td><?= Yii::t('backend','Không thích video')?></td>
            <td><?php echo  $eventInfo[0]['dislike_point'] ?></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td><?= Yii::t('backend','Tổng điểm')?></td>
            <td colspan="2"><?php echo  $eventInfo[0]['total_point'] ?></td>
        </tr>
        </tfoot>

    </table>

</div>
