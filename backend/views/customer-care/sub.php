<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */
/* @var $subModel backend\models\SubForm */

$this->title = Yii::t('app', 'Chăm sóc khách hàng');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-group-topic-index">

    <?= $this->render('_form_search', [
        'model' => $model,
    ]) ?>

    <?php  echo $this->render('_info', ['model' => $model, 'subModel' => $subModel]); ?>


    <?= $this->render('_tab', [
        'model' => $model,
    ]) ?>

    <h1>Đăng ký/Hủy</h1>

    <?= $this->render('_form', [
        'model' => $formModel,
    ]) ?>
</div>
