<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtGroupTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chăm sóc khách hàng');
$this->params['breadcrumbs'][] = $this->title;

$actionId = Yii::$app->controller->action->id;
?>
<div class="vt-group-topic-index">

    <?= $this->render('_form_search', [
        'model' => $model,
    ]) ?>

    <?php  echo $this->render('_info', ['model' => $model, 'subModel' => $subModel]); ?>

    <?= $this->render('_tab', [
        'model' => $model,
    ]) ?>


    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => [''],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= \kartik\daterange\DateRangePicker::widget([
                'model'=>$searchModel,
                'name'=>'insert_time',
                'attribute' => 'insert_time',
                'value'=> date('Y-m-00').' - '. date('Y-m-d'),
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
                'options' => [
                    'class'=>'form-control',
                    'placeholder' => Yii::t('backend','Thời gian')
                ]
            ]) ?>
        </div>

        <div class="col-md-3">
            <?= Html::activeInput('text', $searchModel,
                'ip',
                ['class' => 'form-control', 'placeholder' => 'IP']
            ) ?>
        </div>

        <div class="col-md-3">
            <?= Html::activeDropDownList(
                $searchModel,
                'source',
                [
                    'wap' => 'WAP',
                    'web' => 'WEB',
                    'api' => 'APP',
                ],
                ['class'=>'form-control','prompt' => Yii::t('backend','Nguồn')])?>
        </div>

        <div class="col-md-3">
            <?= Html::submitButton(Yii::t('backend','Tìm kiếm'), [
                'name' => 'search',
                'value' => 'search',
                'class' => 'btn btn-primary'
            ]) ?>
            <?= Html::submitButton(Yii::t('backend','Xuất Excel'), [
                'name' => 'export',
                'value' => 'export',
                'class' => 'btn btn-success'
            ]) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>


    <div id="w2" class="grid-view">
        <div class="summary"><?= Yii::t('backend','Tổng số')?><b><?= Yii::$app->formatter->asInteger($total) ?></b><?= Yii::t('backend',' mục.')?></div>
        <table class="table table-striped table-bordered breakAll">
            <thead>
            <tr>
                <th>#</th>
                <th width="10%"><?= Yii::t('backend','Thời gian')?></th>
                <th width="10%"><?= Yii::t('backend','User ID')?></th>
                <th width="12%"><?= Yii::t('backend','Số điện thoại')?></th>
                <th width="10%"><?= Yii::t('backend','Loại')?></th>
                <th width="10%"><?= Yii::t('backend','ID Video')?></th>
                <th width="25%"><?= Yii::t('backend','Tên Video')?></th>
                <th width="10%"><?= Yii::t('backend','IP')?></th>
                <th width="10%"><?= Yii::t('backend','Nguồn')?></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($pageResults as $key => $result):?>
                    <tr>
                        <td><?= $key + 1?></td>
                        <td><?= $result['insert_time']?></td>
                        <td><?= $result['user_id']?></td>
                        <td><?= $result['msisdn']?></td>
                        <td><?= $result['content_type']?></td>
                        <td><?= $result['content_id']?></td>
                        <td><?= $result['name']?></td>
                        <td><?= $result['remote_addr']?></td>
                        <td><?= $result['source']?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>

        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pager,
            'firstPageLabel' => Yii::t('backend','Đầu'),
            'lastPageLabel' => Yii::t('backend','Cuối')
        ]);
        ?>
    </div>



</div>
