<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\VtGroupTopic;
use backend\models\VtGroupCategory;
use backend\models\VtPackage;
use common\helpers\Utils;
use kartik\datetime\DateTimePicker;
use common\libs\S3Service;
use yii\bootstrap\BaseHtml;
use backend\models\VtVideo;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-video-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'sub-form']); ?>

    <?= $form->field($model, 'msisdn')->textInput() ?>

    <?= $form->field($model, 'package_id')->dropDownList(
            ArrayHelper::map(VtPackage::getAllPackage(), 'id', 'name'),
            ['class' => 'form-control category-checkbox-list', 'prompt' => Yii::t('backend','Chọn gói cước')]
    ); ?>

    <?= $form->field($model, 'is_free')->dropDownList(
        [
            1 => Yii::t('backend','Miễn phí'),
            0 => Yii::t('backend','Mất phí')
        ],
        ['class' => 'form-control category-checkbox-list']
    ); ?>

    <?= $form->field($model, 'is_sms')->dropDownList(
        [
            1 => Yii::t('backend','Có'),
            0 => Yii::t('backend','Không')
        ],
        ['class' => 'form-control category-checkbox-list']
    ); ?>

    <div class="form-group">

        <?= Html::submitButton(Yii::t('backend','Đăng ký'), ['class' => 'btn btn-success', 'name' => 'submitRegister', 'value' => 'register']) ?>
        <?= Html::submitButton(Yii::t('backend','Hủy'), ['class' => 'btn btn-danger', 'name' => 'submitCancel', 'value' => 'cancel']) ?>

    </div>

<?php ActiveForm::end(); ?>

</div>
