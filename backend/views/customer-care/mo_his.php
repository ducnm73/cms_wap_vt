<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtGroupTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chăm sóc khách hàng');
$this->params['breadcrumbs'][] = $this->title;

$actionId = Yii::$app->controller->action->id;
?>
<div class="vt-group-topic-index">

    <?= $this->render('_form_search', [
        'model' => $model,
    ]) ?>

    <?php  echo $this->render('_info', ['model' => $model, 'subModel' => $subModel]); ?>

    <?= $this->render('_tab', [
        'model' => $model,
    ]) ?>

    <?php $sms01 = \common\models\VtSmsMessageBase::getConfig('SMS_01');?>

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => [''],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= \kartik\daterange\DateRangePicker::widget([
                'model'=>$searchModel,
                'name'=>'receive_time',
                'attribute' => 'receive_time',
                'value'=> date('Y-m-00').' - '. date('Y-m-d'),
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
                'options' => [
                    'class'=>'form-control',
                    'placeholder' => Yii::t('backend','Thời gian')
                ]
            ]) ?>
        </div>





        <div class="col-md-3">
            <?= Html::submitButton(Yii::t('backend','Tìm kiếm'), [
                'name' => 'search',
                'value' => 'search',
                'class' => 'btn btn-primary'
            ]) ?>
            <?= Html::submitButton(Yii::t('backend','Xuất Excel'), [
                'name' => 'export',
                'value' => 'export',
                'class' => 'btn btn-success'
            ]) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'pager' => [
            'firstPageLabel' => Yii::t('backend','Đầu'),
            'lastPageLabel'  => Yii::t('backend','Cuối')
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['width' => '5%'],
            ],
            [
                'attribute' => 'receive_time',
                'headerOptions' => ['width' => '20%']
            ],
            [
                'attribute' => 'msisdn',
                'headerOptions' => ['width' => '15%'],
            ],
            [
                'attribute' => 'command',
                'headerOptions' => ['width' => '30%']
            ],
            [
                'attribute' => 'channel',
                'headerOptions' => ['width' => '10%'],
                'filter' => false
            ],

        ],
    ]); ?>

</div>
