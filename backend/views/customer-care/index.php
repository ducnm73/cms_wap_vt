<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtGroupTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Chăm sóc khách hàng');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="vt-group-topic-index">

    <?= $this->render('_form_search', [
        'model' => $model,
    ]) ?>

    <?php  echo $this->render('_info', ['model' => $model, 'subModel' => $subModel]); ?>


    <?= $this->render('_tab', [
        'model' => $model,
    ]) ?>


    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => [''],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= \kartik\daterange\DateRangePicker::widget([
                'model'=>$searchModel,
                'name'=>'datetime',
                'attribute' => 'datetime',
                'value'=> date('Y-m-00').' - '. date('Y-m-d'),
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'locale'=>['format'=>'Y-m-d']
                ],
                'options' => [
                    'class'=>'form-control',
                    'placeholder' => Yii::t('backend','Thời gian')
                ]
            ]) ?>
        </div>



        <div class="col-md-3">
            <?= Html::activeDropDownList(
                $searchModel,
                'action',
                Yii::$app->params['log_transaction.action'],
                ['class'=>'form-control','prompt' => Yii::t('backend','Hoạt động')])?>
        </div>

        <div class="col-md-3">
            <?= Html::activeDropDownList(
                $searchModel,
                'other_info',
                Yii::$app->params['log_transaction.source'],
                ['class'=>'form-control','prompt' => Yii::t('backend','Nguồn')])?>
        </div>

        <div class="col-md-3">
            <?= Html::activeDropDownList(
                $searchModel,
                'status',
                [
                    0 => Yii::t('backend','Thành công'),
                    1 => Yii::t('backend','Thất bại')
                ],
                ['class'=>'form-control','prompt' => Yii::t('backend','Trạng thái')])?>
        </div>

        <div class="col-md-3">
            <?= Html::submitButton(Yii::t('backend','Tìm kiếm'), [
                'name' => 'search',
                'value' => 'search',
                'class' => 'btn btn-primary'
            ]) ?>
            <?= Html::submitButton(Yii::t('backend','Xuất Excel'), [
                'name' => 'export',
                'value' => 'export',
                'class' => 'btn btn-success'
            ]) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>


    <div id="w2" class="grid-view">
        <div class="summary"><?= Yii::t('backend','Tổng số ')?><b><?= Yii::$app->formatter->asInteger($total) ?></b><?= Yii::t('backend',' mục.')?></div>
        <table class="table table-striped table-bordered breakAll">
            <thead>
            <tr>
                <th width="5%">#</th>
                <th width="15%"><?= Yii::t('backend','Thời gian')?></th>
                <th width="12%"><?= Yii::t('backend','Số điện thoại')?></th>
                <th width="15%"><?= Yii::t('backend','Hoạt động')?></th>
                <th width="15%"><?= Yii::t('backend','Giá cước')?></th>
                <th width="15%"><?= Yii::t('backend','Nội dung')?></th>
                <th width="15%"><?= Yii::t('backend','Trạng thái')?></th>
                <th width="15%"><?= Yii::t('backend','Mã lỗi')?></th>
                <th width="15%"><?= Yii::t('backend','Nguồn')?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($pageResults as $key => $result):?>
                <tr>
                    <td><?= $key + 1?></td>
                    <td><?= $result['datetime']?></td>
                    <td><?= $result['msisdn']?></td>
                    <td><?= $result['action']?></td>
                    <td><?= $result['fee']?></td>
                    <td><?= $result['content']?></td>
                    <td><?= $result['status']?></td>
                    <td><?= $result['error_code']?></td>
                    <td><?= $result['other_info']?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pager,
            'firstPageLabel' => Yii::t('backend','Đầu'),
            'lastPageLabel' => Yii::t('backend','Cuối')
        ]);
        ?>
    </div>

</div>
