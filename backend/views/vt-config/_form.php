<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-config-form">

    <?php $form = ActiveForm::begin(['id'=>'form-config']); ?>

    <div class="form-group field-vtconfig-config_key">
        <label class="control-label" ><?php echo Yii::t('backend', 'KEY')?></label>
        <h4><b><?= $model->config_key ?></b></h4>

        <div class="help-block"></div>
    </div>
    <?= $form->field($model, 'config_value')->textarea(['rows' => 6]) ?>

    <div class="form-group field-vtconfig-config_key">
        <label class="control-label" ><?php echo Yii::t('backend', 'Mô tả') ?></label>
        <h4><?= $model->description ?></h4>

        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend','Tạo') : Yii::t('backend','Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
