<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtConfig */

$this->title = Yii::t('backend', 'Create Vt Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-config-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
