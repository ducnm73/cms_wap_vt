<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtConfig */

$this->title = Yii::t('backend','Cập nhật cấu hình').': ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Quản lý cấu hình'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Cập nhật');
?>
<div class="vt-config-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
