<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Quản lý cấu hình');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-config-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'config_key',
            'config_value:ntext',
            [ 'class' => 'yii\grid\ActionColumn', 'template'=>'{update}' ],
        ],
    ]); ?>

</div>
