
<div class="row">

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-value="" data-counter="counterup"></span>
                </div>
                <div class="desc"><?= $crawlerCount ?> <?php echo Yii::t('backend', 'User chưa map')?></div>
            </div>
            <a href="/vt-mapping-crawler/index" class="more"> <?php echo Yii::t('backend', 'Chi tiết')?>
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>



    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-value="" data-counter="counterup"></span>
                </div>
                <div class="desc"><?= $topicCount ?> <?php echo Yii::t('backend', 'Chủ đề chưa map')?></div>
            </div>
            <a href="/vt-mapping-topic/index" class="more"> <?php echo Yii::t('backend', 'Chi tiết')?>
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>



    </div>
</div>