<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtContract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-contract-form">

    <?php $form = ActiveForm::begin(['enableClientValidation'=>false]); ?>

    <?= $form->field($model, 'contract_code')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'disabled'=>'disabled']) ?>

    <div class="form-group field-vtcontract-id_card_number required">
        <label class="control-label" for="vtcontract-id_card_number"><?= Yii::t('backend','Số CMTND')?></label>
        <p class="form-control"><?php echo $model->id_card_number ?></p>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'id_card_created_at')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'id_card_created_by')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <div class="form-group field-vtcontract-id_card_image_frontside required">
        <label class="control-label" for="vtcontract-id_card_image_frontside"><?= Yii::t('backend','Ảnh CMT mặt trước')?></label>
        <img width="200" height="200" src="http://static3.myclip.vn/<?php echo $model->bucket."/".$model->id_card_image_frontside ?>">
        <p>
            <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target="#modalFrontImage">
                <?= Yii::t('backend','Xem ảnh lớn')?>
            </button>
        </p>
    </div>

    <div class="form-group field-vtcontract-id_card_image_backside required">
        <label class="control-label" for="vtcontract-id_card_image_backside"><?= Yii::t('backend','Ảnh CMT mặt sau')?></label>
        <img  width="200" height="200" src="http://static3.myclip.vn/<?php echo $model->bucket."/".$model->id_card_image_backside ?>">
        <p>
            <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target="#modalBackImage">
                <?= Yii::t('backend','Xem ảnh lớn')?>
            </button>
        </p>
    </div>


    <!-- Modal -->
    <div  id="modalFrontImage" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLongTitle" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </button>
                </div>
                <div class="modal-body">
                    <img width="570" style="max-width: 530px" src="http://static3.myclip.vn/<?php echo $model->bucket."/".$model->id_card_image_frontside ?>">
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalBackImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img width="570" style="max-width: 530px" src="http://static3.myclip.vn/<?php echo $model->bucket."/".$model->id_card_image_backside ?>">
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'address')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'msisdn')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'payment_type')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'tax_code')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'created_at')->textInput([ 'disabled'=>'disabled']) ?>

    <div class="form-group field-vtcontract-created_by">
        <label class="control-label" for="vtcontract-created_by"><?= Yii::t('backend','Tạo bởi:')?> </label>
        <a href="http://myclip.vn/channel/<?php echo $model->created_by ?>">
            http://myclip.vn/channel/<?php echo $model->created_by ?>
        </a>
        <div class="help-block"></div>
    </div>
    <?= $form->field($model, 'account_number')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'bank_name')->textInput(['maxlength' => 255, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'bank_department')->textInput(['maxlength' => 255, 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'user_id')->textInput([ 'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'msisdn_pay')->textInput(['maxlength' => 30, 'disabled'=>'disabled']) ?>

    <div class="form-group field-vtcontract-status">
        <label class="control-label" for="vtcontract-status"><?= Yii::t('backend','Trạng thái:')?></label>
        <b>
            <?php

            $statusArr = \common\helpers\Utils::getInterlizationParams('contract.status', true, 'backend');
//                $statusArr = Yii::$app->params['contract.status'];
            echo $statusArr[$model['status'] ];



            ?>
        </b>
        <div class="help-block"></div>
    </div>
    <?= $form->field($model, 'reason')->textarea() ?>

    <div class="form-group">
        <?= Html::resetButton(Yii::t('backend', 'Quay lại danh sách'), ['class' => 'btn', 'onclick'=>"window.location='/vt-contract'"] ) ?>
        <?= Html::submitButton(Yii::t('backend','Duyệt hợp đồng'), ['class' => 'btn btn-primary', 'name'=>'approve', 'onClick' => "return confirm('".Yii::t('backend',"Bạn có chắc chắn DUYỆT HỢP ĐỒNG này")."?')" ]) ?>
        <?= Html::submitButton(Yii::t('backend','Từ chối hợp đồng'), ['class' => 'btn btn-danger', 'name'=>'reject', 'onClick' => "return confirm('".Yii::t('backend',"Bạn có chắc chắn TỪ CHỐI DUYỆT hợp đồng này")."?') " ]) ?>
        <?= Html::submitButton(Yii::t('backend','Tải mẫu hợp đồng'), ['class' => 'btn btn-success', 'name'=>'download' ]) ?>

        <?php /*if($model['status']==2 && !$model['sync_id'] ){ */?><!--
            <?/*= Html::submitButton(Yii::t('backend', 'Gửi hợp đồng'), ['onclick'=>"return confirm('" . Yii::t('backend', 'Bạn có chắc chắn muốn gửi hợp đồng đối soát?') . "')", 'class' => 'btn btn-info', 'name'=>'send-contract' ]) */?>
        --><?php /*} */?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
