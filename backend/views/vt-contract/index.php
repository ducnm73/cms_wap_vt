<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Vt Contracts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-contract-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <form action="/vt-contract/export-excel" method="get" id="form-excel" enctype="multipart/form-data">

        <input type="hidden" name="VtContractSearch[id]" value="<?php echo  $searchModel->id ?>">
        <input type="hidden" name="VtContractSearch[contract_code]" value="<?php echo  $searchModel->contract_code ?>">
        <input type="hidden" name="VtContractSearch[name]" value="<?php echo  $searchModel->name ?>">
        <input type="hidden" name="VtContractSearch[status]" value="<?php echo  $searchModel->status ?>">
        <input type="hidden" name="VtContractSearch[email]" value="<?php echo  $searchModel->email ?>">
        <input type="hidden" name="VtContractSearch[id_card_number]" value="<?php echo  $searchModel->id_card_number ?>">
        <input type="hidden" name="VtContractSearch[msisdn]" value="<?php echo  $searchModel->msisdn ?>">
        <p>
            <?= Html::a(Yii::t('backend', 'EXPORT EXCEL'), 'javascript:void(0)', [ "target"=>"_blank", 'onclick'=> 'document.getElementById("form-excel").submit()'  ,  'class' => 'btn btn-success', 'style' => 'float: right; margin: 10px;']) ?>
        </p>
    </form>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label'=>Yii::t('backend', 'CP CODE'),
                'format'=>'raw',
                'value' => function ($model) {
                    return "<a href='/vt-contract/update/?id=$model->id'>" . "USER_UPLOAD_".$model->user_id ."</a>";
                }
            ],
            'contract_code',
            'name',
            'id_card_number',
            // 'id_card_created_at',
            // 'id_card_created_by',
            // 'bucket',
            // 'id_card_image_frontside',
            // 'id_card_image_backside',
            // 'address',
             'msisdn',
            'email:email',
            // 'payment_type',
            // 'tax_code',
            // 'created_at',
            // 'created_by',
            // 'account_number',
            // 'bank_name',
            // 'bank_department',
            // 'user_id',
            // 'msisdn_pay',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    //$arr = Yii::$app->params['contract.status'];
                    $arr=[
                        0=>Yii::t('backend','Nháp'),
                        1=>Yii::t('backend','Chờ duyệt'),
                        2=>Yii::t('backend','Đã duyệt'),
                        3=>Yii::t('backend','Từ chối')
                    ];

                    return $arr[$model->status];
                },
                //'filter' => Yii::$app->params['contract.status']
                'filter' =>[
                        0=>Yii::t('backend','Nháp'),
                        1=>Yii::t('backend','Chờ duyệt'),
                        2=>Yii::t('backend','Đã duyệt'),
                        3=>Yii::t('backend','Từ chối')
                    ]
            ],
            // 'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
            [
                'attribute' => 'created_at',
                'label'=>Yii::t('backend','Tải mẫu HĐ'),
                'format' => 'raw',
                'value' => function ($model) {
                    return "<a href='javascript:void(0)' class='download-contract' data-content='$model->user_id'>" . Yii::t('backend', 'DOWNLOAD') . "</a>";
                }
            ],
        ],
    ]); ?>

</div>
