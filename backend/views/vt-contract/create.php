<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtContract */

$this->title = Yii::t('backend', 'Create Vt Contract');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Contracts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-contract-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
