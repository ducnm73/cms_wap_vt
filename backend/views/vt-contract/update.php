<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtContract */

    $this->title = Yii::t('backend','Hợp đồng'). ': ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Hợp đồng'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('backend','Cập nhật');
?>

<div class="vt-contract-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
