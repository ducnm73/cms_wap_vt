<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtUploadFeedback */

$this->title = Yii::t('backend', 'Create Vt Upload Feedback');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Upload Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-upload-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
