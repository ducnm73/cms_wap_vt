<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\libs\VtHelper;
use yii\widgets\LinkPager;
use backend\assets\VideoFeedbackAsset;
VideoFeedbackAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtUploadFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Quản lý ý kiến của uploader');
$this->params['breadcrumbs'][] = $this->title;
$allReason = \common\helpers\Utils::getInterlizationParams('video.status.dropdown.value.min', true, 'backend');
?>
<div class="vt-upload-feedback-index">
    <div class="row">
        <form method="get" id="upload-feedback-form" action="/vt-upload-feedback/index">
            <div class="col-md-3">
                <?= Html::textInput('video_name', $videoName, ['id' => 'video_name', 'maxlength'=> 255, 'class'=>'form-control', 'placeholder' => Yii::t('backend','Tên video')]); ?>
            </div>
            <div class="col-md-3">
                <?= Html::textInput('uploader_name', $uploaderName, ['id' => 'uploader_name', 'maxlength'=> 255, 'class'=>'form-control', 'placeholder' => Yii::t('backend','Người tạo')]); ?>
            </div>
            <div class="col-md-3">
                <?= \kartik\select2\Select2::widget([
                    'name' => 'status',
                    'id' => 'status',
                    'value' => $status, // initial value
                    'data' => $allReason,
                    'maintainOrder' => true,
                    'options' => ['placeholder' => Yii::t('backend','Trạng thái khiếu nại'), 'multiple' => false,],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10,
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div>
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>
        </form>
    </div>
    <div id="table_area">
        <div style="width:100%" >
            <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid">
                <thead>
                <tr>
                    <th><strong><?= Yii::t('backend','STT')?></strong></th>
                    <th><strong><?= Yii::t('backend','ID Video')?></strong></th>
                    <th><strong><?= Yii::t('backend','Tên')?></strong></th>
                    <th><strong><?= Yii::t('backend','Thông tin')?></strong></th>
                    <th><strong><?= Yii::t('backend','Nội dung khiếu nại')?></strong></th>
                    <th><strong><?= Yii::t('backend','File đính kèm')?></strong></th>
                    <th><strong><?= Yii::t('backend','Thao tác')?></strong></th>
                </tr>
                </thead>
                <?php if (count($uploadFeedback) > 0):

                    ?>
                    <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($uploadFeedback as $uf):?>
                        <?php $i++ ?>
                        <?php $k = $pagination->offset + $i ?>
                        <tr>
                            <td><?= $k ?></td>
                            <td><?= $uf['videoId'] ?></td>
                            <td>
                                <p><b><?= Yii::t('backend','Tiêu đề:')?></b> <?= $uf['videoName'] ?></p>
                                <p><b><?= Yii::t('backend','Mô tả:')?></b> <?= $uf['videoDescription'] ?></p>
                            </td>
                            <td>
                                <p><b><?= Yii::t('backend','Thời gian yêu cầu duyệt:')?></b> <?= $uf['created_at'] ?></p>
                                <p><b><?= Yii::t('backend','Thời gian admin duyệt:')?></b> <?= $uf['updated_at'] ?></p>
                                <p><b><?= Yii::t('backend','Trạng thái:')?></b> <span style="color: #8a10a0;"><?= $allReason[$uf['status']] ?></span></p>
                                <p><b><?= Yii::t('backend','Tài khoản:')?></b> <?= $uf['uploaderPhone'] ?> - <?= $uf['uploaderName'] ?></p>
                            </td>
                            <td><?= $uf['feedback_content'] ?></td>
                            <td><?= Html::img(VtHelper::getThumbUrl($uf['bucket'], $uf['path'], VtHelper::SIZE_COVER), ['class' => 'image-grid-display']) ?></td>
                            <td>
                                <a href=" <?= Url::toRoute(['vt-video/update', 'id' => $uf['videoId'], 'home-url' => base64_encode(Yii::$app->request->url)]) ?>" target="_blank" style="color: green;">
                                    <?= Yii::t('backend','Chi tiết')?>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                <?php else:?>
                    <tbody>
                    <tr>
                        <td colspan="7" style="text-align: center;"><?= Yii::t('backend','Không có dữ liệu')?></td>
                    </tr>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
        <div class="row">
            <div class="col-md-8">
                <?= LinkPager::widget(['pagination' => $pagination, 'firstPageLabel' => 'First', 'lastPageLabel'  => 'Last']) ?>
            </div>
            <div class="col-md-2" style="margin-top: 10px;">
                Số bản ghi/trang:
                <select id="numOfRecordPerPage">
                    <?php
                        $array = array(20, 50, 100);
                        foreach($array as $number){
                            if($number == $numOfRecord){
                                ?>
                                    <option value="<?= $number?>" selected><?= $number?></option>
                                <?php
                            } else {
                                ?>
                                    <option value="<?= $number?>"><?= $number?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-2" style="margin-top: 10px;">
                <b>Tổng số:</b> <?= $totalUploadFeedback?> <?= Yii::t('backend','bản ghi.')?>
            </div>
        </div>
    </div>
</div>