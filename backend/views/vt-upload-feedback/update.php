<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtUploadFeedback */

$this->title = Yii::t('backend', 'Update Vt Upload Feedback') . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Upload Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="vt-upload-feedback-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
