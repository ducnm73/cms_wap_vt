<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\VtMappingTopic */
/* @var $form yii\widgets\ActiveForm */

$userUrl = \yii\helpers\Url::toRoute(['/user/search']);
$itemName = '';

if(!empty($model->local_topic_id)){
    $itemName = \backend\models\VtGroupCategory::findOne($model->local_category_id)->name;
}

$this->registerJs(
    "
    $('span.not-set').text('');
    $( document ).ready(function() {
        var id_user = $('#local_user_id').select2('data')[0].id;
        $('#channel_id_index' ).val(id_user);
        $('#local_user_id').on('change', function (e) {
            $('.form-group.field-local_channel_id > span').css({
                'pointer-events': 'all',
                'opacity': '1',
             });
            var id_user = $('#local_user_id').select2('data')[0].id;
            $('#channel_id_index' ).val(id_user);
        });    
    });
    "
);

$jsFunctionUrl = new \yii\web\JsExpression(
    'function(params) { 
            var strUser = document.getElementById("channel_id_index").value;
            return {q:params.term, channel_id: strUser, action:"vt-mapping-user"} 
    }'
);

?>
<strong></strong>

<div class="vt-mapping-topic-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['readonly' => true]) ?>

    <?php $user = \common\models\db\VtUserDB::findOne($model->local_user_id); ?>
    <?= $form->field($model, 'local_user_id')->widget(Select2::classname(), [
        'initValueText' => $user ? $user->full_name : '',
        'options' => [
            'id' => 'local_user_id',
            'placeholder' => Yii::t('backend', 'Tên user') . ' ...'
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . "...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/user/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term }; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
        ],
    ])->label(Yii::t('backend', 'Tên user')); ?>

    <input type="text" id="channel_id_index" style="display: none;">
    <?php $userChannel = \common\models\db\VtChannelDB::findOne($model->local_channel_id); ?>
    <?= $form->field($model, 'local_channel_id')->widget(Select2::classname(), [
        'model' => $model,
        'attribute' => 'local_channel_id',
        'initValueText' => $userChannel ? $userChannel->full_name : '',
        'options' => [
            'id' => 'local_channel_id',
            'placeholder' => Yii::t('backend', 'Tên channel') . ' ...'
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => false,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . "...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['user/get-channel-all-by-id']),
                'dataType' => 'json',
                'data' => new JsExpression($jsFunctionUrl)
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
        ],
    ])->label(Yii::t('backend', 'Tên channel')); ?>

    <?= $form->field($model, 'local_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\backend\models\VtGroupCategory::getAllActiveCategory(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend','Chọn chuyên mục').' ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label(Yii::t('backend', 'Chuyên mục'));
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
