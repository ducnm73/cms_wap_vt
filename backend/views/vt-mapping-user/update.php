<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtSlideshow */

$this->title = Yii::t('app', 'Cập nhật {modelClass}: ', [
    'modelClass' => Yii::t('backend', 'Map nguồn CP'),
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mapping User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Cập nhật');
?>
<div class="vt-slideshow-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
