<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtActionLog */

$this->title = Yii::t('backend', 'Create Vt Action Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vt Action Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-action-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
