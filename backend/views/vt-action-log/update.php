<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtActionLog */

$this->title = Yii::t('backend', 'Update Vt Action Log') . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vt Action Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="vt-action-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
