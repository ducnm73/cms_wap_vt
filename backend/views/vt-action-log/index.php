<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtActionLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Báo cáo lịch sử tác động');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .input-mini{
        width: 100% !important;
    }
</style>
<div class="vt-action-log-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'module',
            [
                'label' => Yii::t('backend','Tiêu đề'),
                'attribute' => 'content_id',
                'value' => 'video.name',
                'contentOptions' => ['style'=>'min-width:100px;'],
            ],
            [
                'label' => Yii::t('backend','Loại'),
                'attribute' => 'type',
                'value' => 'type'
            ],
            [
                'label' => Yii::t('backend','Người dùng'),
                'attribute' => 'user_id',
                'value' => 'user.username'
            ],
             'source',
            [
                'attribute' => 'created_at',
                'label' => Yii::t('backend','Thời gian thực hiện'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'filter' => \kartik\daterange\DateRangePicker::widget([
                    'model'=>$searchModel,
                    'name'=>'range_created_at',
                    'attribute' => 'created_at',
                    'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'timePicker'=>true,
                        'timePicker24Hour' => true,
                        'locale'=>['format'=>'Y-m-d H:i:s']
                    ],
                    'options' => [
                        'class'=>'form-control',
                        'placeholder' => '---'.Yii::t('backend','Thời gian thực hiện').'---',
                        'style' => 'min-width: 68px;'
                    ]
                ])
            ],
            [
                'contentOptions' => ['class' => 'text-wrap'],
                'attribute' => 'detail',
                'value' => 'detail',
                'format' => 'ntext'
            ],
             'ip',
            [
                'contentOptions' => ['class' => 'text-wrap', 'style' => 'min-width: 200px;'],
                'attribute' => 'url',
                'value' => 'url',
                'format' => 'url'
            ],
        ],
    ]); ?>

</div>
<?php
$script = <<< JS
    $("#vtactionlogsearch-created_at").prop("readonly", true);
JS;
$this->registerJs($script);
?>
