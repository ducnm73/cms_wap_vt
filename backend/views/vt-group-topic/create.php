<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VtGroupTopic */

$this->title = Yii::t('app', 'Tạo mới {modelClass}', [
    'modelClass' => Yii::t('app', 'Chủ đề'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chủ đề'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-group-topic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
