<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtGroupTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', Yii::t('app', 'Chủ đề'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-group-topic-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Tạo mới {modelClass}', [
            'modelClass' => Yii::t('app', 'Chủ đề'),
            ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('backend','Tên'),
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="' . \yii\helpers\Url::toRoute(['vt-group-topic/update', 'id' => $model->id]) .'">'
                    . mb_substr(htmlentities($model->name), 0,50,'UTF-8') .((strlen($model->name)>50)?'...':'') .'</a>';
                },
            ],
            [
                'attribute' => 'type',
                'filter' => \common\libs\VtHelper::getObjectTypeArray(Yii::$app->params['object.type'])
            ],
            [
                'attribute' => 'is_active',
                'label' => \Yii::t('app', 'Kích hoạt'),
                'value' => function($model, $key, $index, $widget) {
                    return ($model->is_active == \backend\models\VtGroupTopic::ACTIVE) ? Yii::t('backend', 'Kích hoạt') : Yii::t('backend', 'Không kích hoạt');
                },
                'filter' => \common\helpers\Utils::getInterlizationParams('active.dropdown.value', true, 'backend')
            ],


            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
