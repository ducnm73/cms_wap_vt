<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\libs\S3Service;
use cp\models\VtCp;

/* @var $this yii\web\View */
/* @var $model backend\models\VtUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'msisdn')->textInput()->label(Yii::t('backend','Số điện thoại'). '*') ?>


    <?= $form->field($model, 'status')->label(Yii::t('backend','Trạng thái'). '*')->dropDownList(
        \common\helpers\Utils::getInterlizationParams('user.status.dropdown.value', true, 'backend'),
        [
            'class' => 'form-control',
            'prompt' => '--- '.Yii::t('backend','Chọn trạng thái').' ---'
        ]

    )?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => 50]) -> label(Yii::t('backend', 'Tên hiển thị'). '*') ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 50]) ?>

    <div class="row">
        <div class="col-md-6">
            <input type="hidden" id="avatarDelete" name="avatarDelete">
            <?php if (!$model->isNewRecord && !empty($model->path)): ?>
                <div class="col-md-4 avatar-area" style="text-align: center">
                    <?php
                        $urlAvatar = S3Service::generateWebUrl($model->bucket, $model->path);
                    ?>
                    <img class="preview-image" src="<?= $urlAvatar; ?>"/>
                    <br>
                    <a href="<?= $urlAvatar; ?>" target="_blank" style="color: #0F9E5E; font-style: italic;"><?= Yii::t('backend','Xem cỡ lớn')?></a> | <a href="#" onclick="deleteAvatar()" style="color: #9A12B3; font-style: italic;"><?= Yii::t('backend','Xóa avatar')?></a>
                </div>
            <?php endif ?>
            <div class="col-md-8">
                <?= $form->field($model, 'path')->fileInput()->label(Yii::t('app', 'Ảnh đại diện')) ?>
            </div>
        </div>
        <div class="col-md-6">
            <input type="hidden" id="bannerDelete" name="bannerDelete">
            <?php if (!$model->isNewRecord && !empty($model->channel_path)): ?>
                <div class="col-md-4 banner-area" style="text-align: center">
                    <?php
                        $urlBanner = S3Service::generateWebUrl($model->channel_bucket, $model->channel_path);
                    ?>
                    <img class="preview-image" src="<?= $urlBanner; ?>"/>
                    <br>
                    <a href="<?= $urlBanner; ?>" target="_blank" style="color: #0F9E5E; font-style: italic;"><?= Yii::t('backend','Xem cỡ lớn')?></a> | <a href="#" onclick="deleteBanner()" style="color: #9A12B3; font-style: italic;"><?= Yii::t('backend','Xóa banner')?></a>
                </div>
            <?php endif ?>
            <div class="col-md-8">
                <?= $form->field($model, 'channel_path')->fileInput()->label(Yii::t('app', 'Ảnh cover')) ?>
            </div>
        </div>
    </div>


    <?= $form->field($model, 'priority')->textInput(['maxlength' => 50])->label(Yii::t('app', 'Thứ tự')) ?>

    <?= $form->field($model, 'is_hot')->checkbox(['label' => Yii::t('app', 'Hot')]) ?>

    <?= $form->field($model, 'cp_id')->dropDownList(ArrayHelper::map(VtCp::getAllCp(), 'id', 'name'), ['class' => 'form-control package-checkbox-list', 'prompt'=>'--- '.Yii::t('backend','Chọn đối tác').' ---'])->label(Yii::t('backend', 'Đối tác')); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    </div>

    <?php ActiveForm::end(); ?>


</div>
