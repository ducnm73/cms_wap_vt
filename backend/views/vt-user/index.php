<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Khách hàng');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-index">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <?php // echo $this->render('_search', ['model' => $searchModel]);
        $temp = Yii::$app->params['user.status.dropdown.value'];
        for($i = 0; $i < count($temp); $i++) {
            $temp[$i] = Yii::t('backend', $temp[$i]);
        }
    ?>
    <p>
        <?= Html::a(Yii::t('backend','Thêm mới Khách hàng'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('backend','Sắp xếp kênh Hot'), ['index-hot'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summaryOptions' => ['class' => 'summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'filter' => false,
            ],
            'msisdn',
            [
                'attribute' => 'status',
                'label' => \Yii::t('app', 'Trạng thái'),
                'value' => function($model, $key, $index, $widget) {
                    return $model->getStatusString();
                },
                'filter' => $temp
            ],
            'full_name',
            'cp_id',
            'email:email',
            [
                'attribute' => 'Image',
                'label' => Yii::t('backend','Ảnh'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_AVATAR), ['class' => 'image-grid-display']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template'=>'{update}{delete}'],
        ],
    ]); ?>

</div>
