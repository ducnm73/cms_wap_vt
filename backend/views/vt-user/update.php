<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtUser */

$this->title = Yii::t('backend','Chỉnh sửa Khách hàng').': ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Khách hàng'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend','Cập nhật');
?>
<div class="vt-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
