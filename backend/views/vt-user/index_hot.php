<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\libs\VtHelper;
use richardfan\sortable\SortableGridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Sắp xếp kênh Hot');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vt-user-index">


    <?= $this->render('_form_hot', [
        'model' => $model,
    ]) ?>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'sortUrl' => Url::to(['sortItem']),

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'msisdn',
            [
                'attribute' => 'status',
                'label' => \Yii::t('app', 'Trạng thái'),
                'value' => function($model, $key, $index, $widget) {
                    return $model->getStatusString();
                },
                'filter' => Yii::$app->params['user.status.dropdown.value']
            ],
            'full_name',
            'email:email',
            [
                'attribute' => 'Image',
                'label' => Yii::t('backend','Ảnh'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_AVATAR), ['class' => 'image-grid-display']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{update}{delete}'],
        ],
    ]); ?>

</div>
