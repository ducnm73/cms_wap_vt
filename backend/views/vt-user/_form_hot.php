<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\VtUser */
/* @var $form yii\widgets\ActiveForm */



?>

<div class="row">

    <?php $form = ActiveForm::begin(['action' => Url::toRoute(['vt-user/create-hot'])]); ?>

    <div class="col-md-4">

        <?php $userName = empty($model->id) ? '' : \backend\models\VtUser::findOne($model->id)->getCreatedByName();?>

        <?= $form->field($model, 'id')->widget(Select2::classname(), [
            'initValueText' => $userName, // set the initial display text
            'options' => ['placeholder' => Yii::t('backend','Tìm kiếm kênh').' ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Waiting for results...') . "'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['user/search']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(user) { return user.text; }'),
                'templateSelection' => new JsExpression('function (user) { return user.text; }'),
            ],
        ])->label(false);?>
    </div>


    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
