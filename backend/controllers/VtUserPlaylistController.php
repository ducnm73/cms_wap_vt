<?php

namespace backend\controllers;

use backend\models\VtActionLog;
use Yii;
use backend\models\VtUserPlaylistSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\VtUserPlaylist;
use yii\helpers\ArrayHelper;
use common\models\VtActionLogBase;
use backend\helpers\VtExcel;
use backend\models\VtVideoSearch;
use backend\models\VtVideo;

/**
 * VtUserPlaylistController implements the CRUD actions for VtGroupCategory model.
 */
class VtUserPlaylistController extends Controller {
    public  function init()
    {
        //parent::init(); // TODO: Change the autogenerated stub
        //$this->language = Yii::$app->session->get('lang', 'lo');
        $langss = Yii::$app->session->get('lang', 'lo');
        //var_dump($langss);die;
        // kiem tra xem this->language co nam trong mang khong
        $languagePool=Yii::$app->params['languagepool'];
        //var_dump($languagePool);die;
        // neu trong mang
        if(in_array($langss,$languagePool)){
            Yii::$app->language=$langss;
        }else{
            Yii::$app->language=Yii::$app->params['mainLanguage'];
        }
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id == 'index') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all VtUserPlaylistController models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new VtUserPlaylistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post()) {
            $searchModel = new VtUserPlaylistSearch();
            $datas = $searchModel->exportExcel(Yii::$app->request->queryParams);
            if (count($datas) > 0) {
                $reports = array();
                foreach ($datas as $data) {
                    $reports['Tên'][] = $data['name'];
                    $reports['Thời gian tạo'][] = $data['created_at'];
                    $reports['Số lượng content được nhóm'][] = $data['num_video'];
                    $reports['Kênh sở hữu'][] = (isset($data['user'])) ? $data['user']['full_name'] : "";
                    $reports['User tạo'][] = (isset($data['auth'])) ? $data['auth']['username'] : "";
                    $reports['Nguồn hiển thị'][] = $data['source_display'];
                    $reports['Lượt xem playlist'][] = $data['play_times'];
                    $reports['Kích hoạt'][] = $data['is_active'];
                    $reports['Trạng thái'][] = $data['status'];
                }

                VtExcel::exportExcel($reports, 'danh_sach_play_list', Yii::t('web', 'Danh sách playlist'), true);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('backend','Không có dữ liệu'));
            }
        }
        
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        if (Yii::$app->user->identity->user_id > 0) {
            $model = new VtUserPlaylist();
            $model->created_at = date('Y-m-d H:i:s');
            $model->updated_at = date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->status = VtUserPlaylist::CREATE;
                $model->saveWithImage();
                // Luu action log

                VtActionLog::insertLog(VtActionLog::MODULE_USER_PLAYLIST, $model, VtActionLog::TYPE_CREATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

                Yii::$app->session->setFlash('success', Yii::t('backend','Thêm mới thành công !'));
                if (Yii::$app->request->post("submitSave") == "save") {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    return $this->redirect(['create']);
                }
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            Yii::$app->session->setFlash('success', Yii::t('backend','Không map user Frontend không thể sử dụng tính năng này! Liên hệ Admin để cấp tài khoản'));
            return $this->redirect('/');
        }
    }

    public function actionViewSearch() {
        if (Yii::$app->user->identity->user_id > 0) {
            $searchModel = new VtVideoSearch();
            $q = array();
            $dataProvider = $searchModel->searchPopup($q);
            $channelId = Yii::$app->request->get('channel-id');
            $model = $this->findModel(Yii::$app->request->queryParams["id"]);
            if (Yii::$app->request->isAjax) {
                $ids = \backend\models\VtUserPlaylistItem::getIdsPlaylist(Yii::$app->request->queryParams["id"]);

                $dataProvider = $searchModel->searchPopup(Yii::$app->request->queryParams, $ids, VtVideo::TYPE_VOD, $channelId);
                return $this->renderAjax('viewSearch', [
                            'id' => Yii::$app->request->queryParams["id"],
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'model' => $model
                ]);
            } else {
                $dataProvider = $searchModel->searchPopup(Yii::$app->request->queryParams);
                return $this->render('viewSearch', [
                            'id' => Yii::$app->request->queryParams["id"],
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'model' => $model
                ]);
            }
        } else {
            Yii::$app->session->setFlash('success', Yii::t('backend','Không map user Frontend không thể sử dụng tính năng này! Liên hệ Admin để cấp tài khoản'));
            return $this->redirect('/');
        }
    }

    public function actionDelete($id) {
        if (Yii::$app->user->identity->user_id > 0) {
            $model = $this->findModel($id);
            $model->updated_at = date('Y-m-d H:i:s');
            $model->updated_by = Yii::$app->user->id;
            $model->status = \common\models\VtUserPlaylistBase::STATUS_DELETE;
            $model->save();

            VtActionLog::insertLog(VtActionLog::MODULE_USER_PLAYLIST, $model, VtActionLog::TYPE_DELETE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);


            Yii::$app->session->setFlash('success', Yii::t('backend','Xóa thành công !'));
            $this->redirect('/vt-user-playlist/index');
        } else {
            Yii::$app->session->setFlash('success', Yii::t('backend','Không map user Frontend không thể sử dụng tính năng này! Liên hệ Admin để cấp tài khoản'));
            return $this->redirect('/');
        }
    }

    public function actionUpdate($id) {

        if (Yii::$app->user->identity->user_id > 0) {
            $model = $this->findModel($id);
            $model->updated_at = date('Y-m-d H:i:s');
            $model->updated_by = Yii::$app->user->id;
            // Lay danh sach noi dung 
            $contentPlaylist = VtVideo::getVideosByPlaylist($id, '', '', VtVideo::TYPE_VOD);
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if (Yii::$app->request->post("submitUpdate") == "update") {
                    if ($model->num_video > Yii::$app->params['playlist.limit.item']) {
                        Yii::$app->session->setFlash('error', Yii::t('backend','Số lượng video thêm vào playlist vượt quá ' . Yii::$app->params['playlist.limit.item'] . ' !'));
                    } else {
                        $model->status = VtUserPlaylist::APPROVE;
                        $model->saveWithImage();

                        VtActionLog::insertLog(VtActionLog::MODULE_USER_PLAYLIST, $model, VtActionLog::TYPE_UPDATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);


                        Yii::$app->session->setFlash('success', Yii::t('backend','Cập nhật thành công !'));
                    }
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    $model->status = \common\models\VtUserPlaylistBase::STATUS_DELETE;
                    $model->save();

                    VtActionLog::insertLog(VtActionLog::MODULE_USER_PLAYLIST, $model, VtActionLog::TYPE_UPDATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);


                    Yii::$app->session->setFlash('success', Yii::t('backend','Xóa thành công !'));
                    $this->redirect('/vt-user-playlist/index');
                }
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'parts' => $contentPlaylist
                ]);
            }
        } else {
            Yii::$app->session->setFlash('success', Yii::t('backend','Không map user Frontend không thể sử dụng tính năng này! Liên hệ Admin để cấp tài khoản'));
            return $this->redirect('/');
        }
    }

    public function actionSearch() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $q = Yii::$app->request->get('q');
        $out = ['results' => ['id' => '', 'text' => '']];
        $out['results'] = ArrayHelper::toArray(\common\models\VtUserBase::searchByName($q), ['id', 'text' => 'name']);
        return $out;
    }

    protected function findModel($id) {
        if (($model = VtUserPlaylist::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }

    public function actionDeletePlaylist() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $contentId = Yii::$app->request->post('itemId');
        $playlistId = Yii::$app->request->post('playlistId');
        $result = array("errorCode" => 403, "message" => Yii::t('backend','Lỗi xác thực'));
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return $result;
        }
        if (Yii::$app->user->identity->user_id > 0) {
            \backend\models\VtUserPlaylistItem::deletePlaylist($playlistId, $contentId);
            $result = array("errorCode" => 200, "message" => Yii::t('backend','Thành công'));
        }
        return $result;
    }

    public function actionAddPlaylist() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $contentId = Yii::$app->request->post('itemId');
        $playlistId = Yii::$app->request->post('playlistId');
        $result = array("errorCode" => 403, "message" => Yii::t('backend','Lỗi xác thực'));
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return $result;
        }
        if (Yii::$app->user->identity->user_id > 0) {
            $item = new \backend\models\VtUserPlaylistItem();
            $item->insertItem($contentId, $playlistId);
            $result = array("errorCode" => 200, "message" => Yii::t('backend','Thành công'));
        }
        return $result;
    }

    public function actionChange() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $position = Yii::$app->request->post('pos');
        if (Yii::$app->user->identity->user_id > 0) {
            foreach ($position as $pos) {
                $arr = explode(':', $pos);
                \backend\models\VtUserPlaylistItem::updatePos($arr[2], $arr[1], $arr[0]);
            }
        }
        return $this->renderContent("test");
    }

    public function actionFinishPlaylist() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = array("errorCode" => 403, "message" => Yii::t('backend','Lỗi xác thực'));
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return $result;
        }
        $data = Yii::$app->request->post('data');
        $playlistId = Yii::$app->request->post('playlistId');
        if (Yii::$app->user->identity->user_id > 0) {
            $index = 0;
            if(!empty($data)) {
                krsort($data);
                foreach ($data as $value) {
                    \backend\models\VtUserPlaylistItem::updatePos($playlistId, $value, $index);
                    $index++;
                }
            }


            $model = $this->findModel($playlistId);
            $model->is_finish = 1;
            $model->updated_at = date('Y-m-d H:i:s');

            $model->updated_by = Yii::$app->user->identity->user_id;
            $model->save();

            VtActionLog::insertLog(VtActionLog::MODULE_USER_PLAYLIST, $model, VtActionLog::TYPE_UPDATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

            Yii::$app->session->setFlash('success', Yii::t('backend','Cập nhật thành công !'));
            $result = array("errorCode" => 200, "message" => Yii::t('backend','Thành công'));
        }
        return $result;
    }

}
