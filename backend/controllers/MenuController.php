<?php

namespace backend\controllers;

use backend\models\Menu;
use backend\models\MenuSearch;
use mdm\admin\components\MenuHelper;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MenuController implements the CRUD actions for Menu model.
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class MenuController extends Controller {


    public  function init()
    {
        //parent::init(); // TODO: Change the autogenerated stub
        //$this->language = Yii::$app->session->get('lang', 'lo');
        $langss = Yii::$app->session->get('lang', 'lo');
        //var_dump($langss);die;
        // kiem tra xem this->language co nam trong mang khong
        $languagePool=Yii::$app->params['languagepool'];
        //var_dump($languagePool);die;
        // neu trong mang
        if(in_array($langss,$languagePool)){
            Yii::$app->language=$langss;
        }else{
            Yii::$app->language=Yii::$app->params['mainLanguage'];
        }
    }
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MenuSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Menu;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //MenuHelper::invalidate();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->menuParent) {
            $model->parent_name = $model->menuParent->name;
        }
//        echo "<pre>";
//        var_dump(Yii::$app->request->post()); die('xxx');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->is_active = Yii::$app->request->post()['Menu']['is_active'];
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            //MenuHelper::invalidate();
        }
        return $this->render('update', [
                    'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        //MenuHelper::invalidate();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }



}
