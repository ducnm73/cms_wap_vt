<?php

namespace backend\controllers;


use backend\helpers\VtExcel;
use backend\models\LogTransactionSearch;
use backend\models\SubForm;
use backend\models\VtActionLog;
use backend\models\VtHistoryViewSearch;
use backend\models\VtSmsMoHisSearch;
use backend\models\VtSmsMtHisSearch;
use backend\models\VtSub;
use backend\models\VtUser;
use common\helpers\CustomDataProvider;
use common\helpers\Utils;
use common\libs\VtHelper;
use common\libs\VtService;
use common\models\MongoDBModel;
use common\models\VtSmsMessageBase;
use common\models\VtSmsMtBase;
use common\models\VtVideoBase;
use Elasticsearch\ClientBuilder;
use Yii;
use backend\models\VtVideo;
use backend\models\VtVideoSearch;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\HistoryDeviceForm;
use backend\models\HistoryViewForm;


/**
 * VtVideoController implements the CRUD actions for VtVideo model.
 */
class CustomerCareController extends Controller
{
    public  function init()
    {
        //parent::init(); // TODO: Change the autogenerated stub
        //$this->language = Yii::$app->session->get('lang', 'lo');
        $langss = Yii::$app->session->get('lang', 'lo');
        //var_dump($langss);die;
        // kiem tra xem this->language co nam trong mang khong
        $languagePool=Yii::$app->params['languagepool'];
        //var_dump($languagePool);die;
        // neu trong mang
        if(in_array($langss,$languagePool)){
            Yii::$app->language=$langss;
        }else{
            Yii::$app->language=Yii::$app->params['mainLanguage'];
        }
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    protected function handleModel()
    {
        $vtUser = Yii::$app->request->get('VtUser');

        if (isset($vtUser['msisdn'])) {
            $msisdn = $vtUser['msisdn'];
            Yii::$app->session->set('cc_msisdn', $msisdn);
            $model = VtUser::getByMsisdn($msisdn);
        } else {
            $msisdn = Yii::$app->session->get('cc_msisdn');
        }

        if (!isset($model)) {
            $model = new VtUser();
            $model->msisdn = $msisdn;
            $model->id = null;
        }

        return $model;
    }


    /**
     * Lists all VtVideo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->handleModel();

        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }

        $searchModel = new LogTransactionSearch();

        $action = Yii::$app->request->get('action', '');
        $pageSize = Yii::$app->request->get('per-page', 20);
        $total = 0;
        $pageResults = [];

        $searchModel->load(Yii::$app->request->get());
        $searchModel->validate();

        $mustArr = [];

        if (!empty($model->msisdn)) {
            array_push($mustArr, [
                "bool" => [
                    "should" => [
                            ["term" => ["msisdn" => Utils::getMobileNumber($model->msisdn, Utils::MOBILE_GLOBAL) ]],
                            ["term" => ["msisdn" => VtHelper::convert8416XTo843X($model->msisdn)]]
                    ]
                ]
            ]);
        }

        if (!empty($searchModel->datetime)) {
            list($fromTime, $endTime) = explode(' - ', $searchModel->datetime);

            array_push($mustArr, [
                "range" => [
                    "datetime" => [
                        "gte" => trim($fromTime),
                        "lte" => trim($endTime),
                        "format" => "yyyy-MM-dd",
                        "time_zone" => "+07:00"
                    ]
                ]
            ]);
        }

        if (!empty($searchModel->action)) {
            array_push($mustArr, [
                "term" => [
                    "action" => strtolower($searchModel->action)
                ]
            ]);
        }

        if (!empty($searchModel->other_info)) {
            array_push($mustArr, [
                "term" => [
                    "other_info_field_0" => strtolower($searchModel->other_info)
                ]
            ]);
        }

        $index = 'log-transaction*';
		
        if ($searchModel->status == 0) {
            $index = 'log-transaction';
        } elseif ($searchModel->status == 1) {
            $index = 'log-transaction-fail';
        }

        if (!empty(Yii::$app->request->get('export'))) {
            $params = [
                'index' => $index,
                'type' => 'logs',
                'body' => [
                    "size" => 10000,
                    "query" => [
                        "bool" => [
                            "must" => $mustArr
                        ]
                    ],
                    "sort" => [
                        ["datetime" => ["order" => "desc"]]
                    ]
                ]
            ];
        } else {
            $params = [
                'index' => $index,
                'type' => 'logs',
                'body' => [
                    "from" => (Yii::$app->request->get('page', 1) - 1) * $pageSize,
                    "size" => $pageSize,
                    "query" => [
                        "bool" => [
                            "must" => $mustArr
                        ]
                    ],
                    // "sort" => [
                        // ["datetime" => ["order" => "desc"]]
                    // ]
                ]
            ];
        }


        $client = ClientBuilder::create()->setHosts(Yii::$app->params['elasticsearch'])->build();
        $results = $client->search($params);

        if (isset($results['hits'])) {
            $total = $results['hits']['total'];

            $pageResults = [];
            foreach ($results['hits']['hits'] as $key => $result) {
                $item = $result['_source'];

                $arr = [];

                $arr['key'] = $key + 1;
                $arr['datetime'] = isset($item['datetime']) ? \common\helpers\Utils::convertUtcToEtc(date('Y-m-d H:i:s', strtotime($item['datetime']))) : '';
                $arr['msisdn'] = $item['msisdn'];
                $arr['action'] = $item['action'];
                $arr['fee'] = $item['fee'];
                $arr['content'] = $item['content'];
                if ($result['_index'] == 'log-transaction-fail') {
                    $arr['status'] = Yii::t('backend','Thất bại');
                } else {
                    $arr['status'] = Yii::t('backend','Thành công');
                }
                $arr['error_code'] = $item['error_code'];
                $arr['other_info'] = $item['other_info'];

                $pageResults[] = $arr;
            }
        }

        VtActionLog::insertLog(VtActionLog::MODULE_CUSTOMER_CARE, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

        $header = [
            Yii::t('backend','STT'),
            Yii::t('backend','Thời gian'),
            Yii::t('backend','Số điện thoại'),
            Yii::t('backend','Hoạt động'),
            Yii::t('backend','Giá cước'),
            Yii::t('backend','Nội dung'),
            Yii::t('backend','Trạng thái'),
            Yii::t('backend','Mã lỗi'),
            Yii::t('backend','Nguồn')
        ];

        if (!empty(Yii::$app->request->get('export'))) {
            VtExcel::exportRow($header, $pageResults, 'log_transaction');
        }

        $pager = new Pagination([
            'totalCount' => $total
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'model' => $model,
            'subModel' => $subModel,
            'total' => $total,
            'pageResults' => $pageResults,
            'pager' => $pager
        ]);
    }

    public function actionMtHis()
    {
        $model = $this->handleModel();
        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }
        $searchModel = new VtSmsMtHisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $model->msisdn);

        VtActionLog::insertLog(VtActionLog::MODULE_CUSTOMER_CARE, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

        if (!empty(Yii::$app->request->get('export'))) {
            $dataProvider->pagination = [
                'pageSize' => 10000,
            ];

            $header = [
                Yii::t('backend','STT'),
                Yii::t('backend','Thời gian'),
                Yii::t('backend','Số điện thoại'),
                Yii::t('backend','Nội dung'),
                Yii::t('backend','Đầu số'),
                Yii::t('backend','Process ID')
            ];

            $pageResults = [];
            foreach ($dataProvider->getModels() as $key => $item) {
                $arr = [];

                $arr['key'] = $key + 1;
                $arr['sent_time'] = $item->sent_time;
                $arr['msisdn'] = $item->msisdn;
                $arr['message'] = $item->message;
                $arr['channel'] = $item->channel;
                $arr['process_id'] = $item->process_id;

                $pageResults[] = $arr;
            }

            VtExcel::exportRow($header, $pageResults, 'mt_his');
        }


        return $this->render('mt_his', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'subModel' => $subModel
        ]);
    }

    public function actionMoHis()
    {
        $model = $this->handleModel();

        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }

        $searchModel = new VtSmsMoHisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $model->msisdn);

        VtActionLog::insertLog(VtActionLog::MODULE_CUSTOMER_CARE, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

        if (!empty(Yii::$app->request->get('export'))) {
            $dataProvider->pagination = [
                'pageSize' => 10000,
            ];

            $header = [
                Yii::t('backend','STT'),
                Yii::t('backend','Thời gian nhận'),
                Yii::t('backend','Số điện thoại'),
                Yii::t('backend','Cú pháp'),
                Yii::t('backend','Đầu số')
            ];

            $pageResults = [];
            foreach ($dataProvider->getModels() as $key => $item) {
                $arr = [];

                $arr['key'] = $key + 1;
                $arr['receive_time'] = $item->receive_time;
                $arr['msisdn'] = $item->msisdn;
                $arr['command'] = $item->command;
                $arr['channel'] = $item->channel;

                $pageResults[] = $arr;
            }

            VtExcel::exportRow($header, $pageResults, 'mo_his');
        }


        return $this->render('mo_his', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'subModel' => $subModel
        ]);
    }


    public function actionEventView()
    {
        VtActionLog::insertLog(VtActionLog::MODULE_CUSTOMER_CARE, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);
        $model = $this->handleModel();

        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }

        $eventInfo = MongoDBModel::getReportByUserId($model->id, intval($model->msisdn));


        return $this->render('event_view', [
            'model' => $model,
            'subModel' => $subModel,
            'eventInfo' => $eventInfo
        ]);
    }


    public function actionHistoryView()
    {
        $model = $this->handleModel();

        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }

        $pageSize = Yii::$app->request->get('per-page', 20);
        $total = 0;
        $pageResults = [];

        $searchModel = new HistoryViewForm();
        $searchModel->load(Yii::$app->request->get());
        $searchModel->validate();

        $mustArr = [];
        if (!empty($model->msisdn)) {
            array_push($mustArr, [
                "bool" => [
                    "should" => [
                            ["term" => ["msisdn" => Utils::getMobileNumber($model->msisdn, Utils::MOBILE_GLOBAL) ]],
                            ["term" => ["msisdn" => VtHelper::convert8416XTo843X($model->msisdn)]]
                    ]
                ]
            ]);
        }

        if (!empty($searchModel->insert_time)) {
            list($fromTime, $endTime) = explode(' - ', $searchModel->insert_time);

            array_push($mustArr, [
                "range" => [
                    "insert_time" => [
                        "gte" => trim($fromTime),
                        "lte" => trim($endTime),
                        "format" => "yyyy-MM-dd",
                        "time_zone" => "+07:00"
                    ]
                ]
            ]);
        }

        if (!empty($searchModel->ip)) {
            array_push($mustArr, [
                "term" => [
                    "remote_addr" => $searchModel->ip
                ]
            ]);
        }

        if (!empty($searchModel->source)) {
            array_push($mustArr, [
                "term" => [
                    "source" => $searchModel->source
                ]
            ]);
        }

        if (!empty(Yii::$app->request->get('export'))) {
            $params = [
                'index' => 'log-vod*',
                'type' => 'log-vod',
                'body' => [
                    "size" => 10000,
                    "query" => [
                        "bool" => [
                            "must" => $mustArr
                        ]
                    ],
                    "sort" => [
                        ["insert_time" => ["order" => "desc"]]
                    ]
                ]
            ];
        } else {
            $params = [
                'index' => 'log-vod*',
                'type' => 'log-vod',
                'body' => [
                    "from" => (Yii::$app->request->get('page', 1) - 1) * $pageSize,
                    "size" => $pageSize,
                    "query" => [
                        "bool" => [
                            "must" => $mustArr
                        ]
                    ],
                    "sort" => [
                        ["insert_time" => ["order" => "desc"]]
                    ]
                ]
            ];
        }

        $client = ClientBuilder::create()->setHosts(Yii::$app->params['elasticsearch'])->build();
        $results = $client->search($params);


        if (isset($results['hits'])) {
            $total = $results['hits']['total'];

            $pageResults = [];
            foreach ($results['hits']['hits'] as $key => $result) {
                $item = $result['_source'];

                $arr = [];

                $arr['key'] = $key + 1;
                $arr['insert_time'] = isset($item['insert_time']) ? \common\helpers\Utils::convertUtcToEtc(date('Y-m-d H:i:s', strtotime($item['insert_time']))) : '';
                $arr['user_id'] = isset($item['user_id']) ? $item['user_id'] : '';
                $arr['msisdn'] = isset($item['msisdn']) ? $item['msisdn'] : '';
                $arr['content_type'] = $item['content_type'];
                $arr['content_id'] = $item['content_id'];
                $arr['name'] = isset($item['name']) ? $item['name'] : '';
                $arr['remote_addr'] = $item['remote_addr'];

                list($app, $source) = explode('-', isset($item['source']) ? $item['source'] : '-');
                $arr['source'] = isset($source) ? strtoupper($source) : '';

                $pageResults[] = $arr;
            }
        }

        VtActionLog::insertLog(VtActionLog::MODULE_CUSTOMER_CARE, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

        $header = [
            Yii::t('backend','STT'),
            Yii::t('backend','Thời gian'),
            Yii::t('backend','User ID'),
            Yii::t('backend','Số điện thoại'),
            Yii::t('backend','Loại'),
            Yii::t('backend','ID Video'),
            Yii::t('backend','Tên Video'),
            Yii::t('backend','IP'),
            Yii::t('backend','Nguồn')
        ];

        if (!empty(Yii::$app->request->get('export'))) {
            VtExcel::exportRow($header, $pageResults, 'history_view');
        }

        $pager = new Pagination([
            'totalCount' => $total
        ]);

        return $this->render('history_view', [
            'searchModel' => $searchModel,
            'model' => $model,
            'subModel' => $subModel,
            'total' => $total,
            'pageResults' => $pageResults,
            'pager' => $pager
        ]);
    }


    public function actionHistoryDevice()
    {
        $model = $this->handleModel();

        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }

        $searchModel = new HistoryDeviceForm();

        $pageSize = Yii::$app->request->get('per-page', 20);
        $total = 0;
        $pageResults = [];


        $searchModel->load(Yii::$app->request->get());
        $searchModel->validate();

        $mustArr = [];
        if (!empty($model->msisdn)) {
            array_push($mustArr, [
                "bool" => [
                    "should" => [
                            ["term" => ["msisdn" => Utils::getMobileNumber($model->msisdn, Utils::MOBILE_GLOBAL)  ]],
                            ["term" => ["msisdn" => VtHelper::convert8416XTo843X($model->msisdn)]]
                    ]
                ]
            ]);
        }

        if (!empty($searchModel->insert_time)) {
            list($fromTime, $endTime) = explode(' - ', $searchModel->insert_time);

            array_push($mustArr, [
                "range" => [
                    "insert_time" => [
                        "gte" => trim($fromTime),
                        "lte" => trim($endTime),
                        "format" => "yyyy-MM-dd",
                        "time_zone" => "+07:00"
                    ]
                ]
            ]);
        }

        if (!empty($searchModel->ip)) {
            array_push($mustArr, [
                "term" => [
                    "remote_addr" => $searchModel->ip
                ]
            ]);
        }

        if (!empty($searchModel->source)) {
            array_push($mustArr, [
                "term" => [
                    "source" => $searchModel->source
                ]
            ]);
        }

        if (!empty(Yii::$app->request->get('export'))) {
            $params = [
                'index' => 'log-device*',
                'type' => 'log-device',
                'body' => [
                    "size" => 10000,
                    "query" => [
                        "bool" => [
                            "must" => $mustArr
                        ]
                    ],
                    "sort" => [
                        ["insert_time" => ["order" => "desc"]]
                    ]
                ]
            ];
        } else {
            $params = [
                'index' => 'log-device*',
                'type' => 'log-device',
                'body' => [
                    "from" => (Yii::$app->request->get('page', 1) - 1) * $pageSize,
                    "size" => $pageSize,
                    "query" => [
                        "bool" => [
                            "must" => $mustArr
                        ]
                    ],
                    "sort" => [
                        ["insert_time" => ["order" => "desc"]]
                    ]
                ]
            ];
        }

        $client = ClientBuilder::create()->setHosts(Yii::$app->params['elasticsearch'])->build();
        $results = $client->search($params);


        if (isset($results['hits'])) {
            $total = $results['hits']['total'];

//            $pageResults = ArrayHelper::getColumn($results['hits']['hits'], '_source');

            foreach ($results['hits']['hits'] as $key => $result) {
                $item = $result['_source'];

                $arr = [];

                $arr['key'] = $key + 1;
                $arr['insert_time'] = isset($item['insert_time']) ? \common\helpers\Utils::convertUtcToEtc(date('Y-m-d H:i:s', strtotime($item['insert_time']))) : '';
                $arr['user_id'] = isset($item['user_id']) ? $item['user_id'] : '';
                $arr['msisdn'] = isset($item['msisdn']) ? $item['msisdn'] : '';
                $arr['user_agent'] = $item['user_agent'];
                $arr['session_id'] = $item['session_id'];
                $arr['remote_addr'] = $item['remote_addr'];
                $arr['source'] = strtoupper($item['source']);

                $pageResults[] = $arr;
            }
        }

        VtActionLog::insertLog(VtActionLog::MODULE_CUSTOMER_CARE, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);

        $header = [
            Yii::t('backend','STT'),
            Yii::t('backend','Thời gian'),
            Yii::t('backend','User ID'),
            Yii::t('backend','Số điện thoại'),
            Yii::t('backend','User Agent'),
            Yii::t('backend','Session Id'),
            Yii::t('backend','IP'),
            Yii::t('backend','Nguồn')
        ];

        if (!empty(Yii::$app->request->get('export'))) {
            VtExcel::exportRow($header, $pageResults, 'history_device');
        }


        $pager = new Pagination([
            'totalCount' => $total
        ]);

        return $this->render('history_device', [
            'searchModel' => $searchModel,
            'model' => $model,
            'subModel' => $subModel,
            'total' => $total,
            'pageResults' => $pageResults,
            'pager' => $pager
        ]);
    }


    public function actionSub()
    {
        $model = $this->handleModel();

        $subModel = null;
        if (isset($model->msisdn)) {
            $subModel = VtSub::getOneSubByMsisdn($model->msisdn);
        }

        $formModel = new SubForm();
        $formModel->msisdn = $model->msisdn;

        if ($formModel->load(Yii::$app->request->post()) && $formModel->validate()) {
            if (!empty(Yii::$app->request->post('submitRegister'))) {

                $errorCode = VtService::registerServiceWithFee($formModel->msisdn, $formModel->package_id, 'CC', 0);
                $content = VtSmsMessageBase::getConfig("register_sms_" . $formModel->package_id . "_" . $errorCode);
                if ($formModel->is_sms && $errorCode == '0') {

                    $mt = new VtSmsMtBase();
                    if ($content) {
                        $mt->sendSms($formModel->msisdn, $content);
                    }

                }
                Yii::$app->session->setFlash('success', $content);

            } elseif (!empty(Yii::$app->request->post('submitCancel'))) {

                $errorCode = VtService::cancelService($formModel->msisdn, $formModel->package_id, 'BACKEND');

                $content = VtSmsMessageBase::getConfig("cancel_sms_" . $formModel->package_id . "_" . $errorCode, 'System error');
                if ($formModel->is_sms && $errorCode == '0') {

                    $mt = new VtSmsMtBase();
                    if ($content) {
                        $mt->sendSms($formModel->msisdn, $content);
                    }

                }

                Yii::$app->session->setFlash('success', $content);
            }

            return $this->redirect(['sub']);

        }

        return $this->render('sub', [
            'model' => $model,
            'subModel' => $subModel,
            'formModel' => $formModel
        ]);

    }
}