<?php

namespace backend\controllers;

use Yii;
use backend\models\UserViews;
use backend\models\UserViewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserViewsController implements the CRUD actions for UserViews model.
 */
class UserViewsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserViews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserViewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserViews model.
     * @param string $msisdn
     * @param string $date
     * @return mixed
     */
    public function actionView($msisdn, $date)
    {
        return $this->render('view', [
            'model' => $this->findModel($msisdn, $date),
        ]);
    }

    /**
     * Creates a new UserViews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserViews();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'msisdn' => $model->msisdn, 'date' => $model->date]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserViews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $msisdn
     * @param string $date
     * @return mixed
     */
    public function actionUpdate($msisdn, $date)
    {
        $model = $this->findModel($msisdn, $date);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'msisdn' => $model->msisdn, 'date' => $model->date]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserViews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $msisdn
     * @param string $date
     * @return mixed
     */
    public function actionDelete($msisdn, $date)
    {
        $this->findModel($msisdn, $date)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserViews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $msisdn
     * @param string $date
     * @return UserViews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($msisdn, $date)
    {
        if (($model = UserViews::findOne(['msisdn' => $msisdn, 'date' => $date])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
