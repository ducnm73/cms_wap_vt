$(document).ready(function () {
    setTimeout(function() {
        $('#w2-error-0').fadeOut('slow');
    }, 3000);

    $(".download-contract").on("click", function(){
        window.open("/vt-contract/view-contract?userId="+$(this).data('content'));
    });

    $('#btn-remove-approve').on('click', function () {
        var cf = confirm(trans("Bạn có đồng ý hạ nội dung?"));
        if (!cf) {
            return false;
        }
    });

    $("#form-config").on('submit', function () {
        if (!$("#vtconfig-config_value").val() ) {
            var config = confirm(trans("Bạn có chắc chắn bỏ trống cấu hình?"));
            if (config) {
                return true;
            } else {
                return false;
            }
        }
        return true;

    });
    
    $('.changlanguage').on('click', function () {
        url = $(this).find('a').attr('url');
        $.ajax({
            url: url,
            success: function () {
                location.reload();
            }
        });
    });
});

function submitReasonDecline(){
    var id = $('#hiddenId').val();
    var action = $('#hiddenAction').val();
    var selValue = $('input[name=reasonNum]:checked').val();
    // $('#myModal').modal('hide');
    // modal.style.display = "none";
    if(selValue == null) {
        $('#reason').show();
        $('.btn-default').click(function() {
            $('#reason').hide();
        });
    }
    else {
        modal.style.display = "none";
        approveComment(id, action, selValue);
    }
}

function checkSendSMS(id){

    if($("#"+id).val() ==0){
        alert(trans("Yêu cầu chọn số thuê bao nhận tin"));
        return;
    }
    if(id=="sms_contract_list_draft"){

        if( $("#vtsmsapprove-sms_content_draft").val().length<=0 ){
            alert(trans("Yêu cầu nhập nội dung nháp"));
            return;
        }
    }

    $.ajax({
        method: "POST",
        url: "/vt-sms-approve/check-send-sms",
        dataType: 'json',
        data: {
            'id': id,
            'msisdn': $("#"+id).val(),
            "content_id": $("#sms_approve_id").val(),
            "content": $("#vtsmsapprove-sms_content_draft").val(),
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {

            alert(data.message);
        },
        error: function (data) {

        }
    });
}

function approveComment(id, action, reasonDecline) {
    // switch (action) {
    //     case 'approve':
    //         var cf = confirm('Bạn có đồng ý phê duyệt?');
    //         if (!cf) return;
    //         break;
    //     case 'delete':
    //         var cf = confirm('Bạn có đồng ý không duyệt?');
    //         if (!cf) return;
    //         break;
    // }
    $.ajax({
        method: "POST",
        url: $('#approve-comment-action').val(),
        data: {
            'id': id,
            'action': action,
            'reason': reasonDecline,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
                if (response.errorCode == 200) {
                    $('*[data-key=' + id + ']').replaceWith('<tr><td colspan="9" style="text-align:center"><strong>' + trans('Hoàn thành xử lý') + '</strong></td></tr>');

                } else {
                    alert(response.message);
                }
        },
        error: function (data) {
        }
    });
}

function approveVideo(id, action) {

    switch (action) {

        case 'approve':
            var cf = confirm(trans('Bạn có đồng ý phê duyệt?'));
            if (!cf) return;
            break;
        case 'delete':
            var cf = confirm(trans('Bạn có đồng ý xóa?'));
            if (!cf) return;
            break;
        case 'reconvert':
            var cf = confirm(trans('Bạn có đồng ý convert lại?'));
            break;
        case 'review':
            var cf = confirm(trans('Bạn có muốn xem lướt?'));
            if (!cf) return;
            break;
    }

    $.ajax({
        method: "POST",
        url: $('#approve-action').val(),
        data: {
            'id': id,
            'action': action
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.success) {
                if (action == 'review') {
                    $('#reviewVideo-' + id).remove();
                    alert(response.message);
                } else {
                    $('*[data-key=' + id + ']').replaceWith('<tr><td colspan="9" style="text-align:center"><strong>' + trans('Hoàn thành xử lý') + '</strong></td></tr>');
                }
            } else {
                alert(response.message);
            }
        },
        error: function (data) {

        }
    });

}

function searchVideo() {
    $.ajax({
        method: "GET",
        url: $('#playlistUrl').val() + '?id=' + $('#playlistId').val() + '&' + 'VtVideoSearch%5Bname%5D=' + $('#vtvideosearch-name').val() + '&channel-id=' +  $('#channelId').val(),

        success: function (data) {

            $('#modalContent').html(data);

        },
        error: function (data) {
            alert('Error');
        }
    });
}

function showMe(id) {
    $(id).show();
}
function hideMe(id) {
    $(id).hide();
}

$('.exportUserUploadStandard').on('click', function(event){
    event.preventDefault();
    var startDate=  $("#from_time").val();
    var endDate=  $("#to_time").val();
    window.location = "/report/export-user-upload?from_date=" + startDate + "&to_date=" + endDate;
});

function approveVideoAfterReview(id, action, reasonDecline) {
    switch (action) {
        case 'approve':
            var cf = confirm(trans('Bạn có đồng ý phê duyệt lại?'));
            if (!cf) return;
            break;
        case 'delete':
            var cf = confirm(trans('Bạn có đồng ý không duyệt?'));
            if (!cf) return;
            break;
    }
    $.ajax({
        method: "POST",
        url: $('#approve-video-after-review-action').val(),
        data: {
            'id': id,
            'action': action,
            'reason': reasonDecline,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.errorCode == 200) {
                $('*[data-key=' + id + ']').replaceWith('<tr><td colspan="9" style="text-align:center"><strong>' + trans('Hoàn thành xử lý') + '</strong></td></tr>');


            } else {
                alert(response.message);
            }
        },
        error: function (data) {

        }
    });
}

function submitReasonDeclineVideo(){
    var id = $('#hiddenId').val();
    var action = $('#hiddenAction').val();
    var declineReason = $('#declineVideoReason').val();
    approveVideoAfterReview(id, action, declineReason);
    modal.style.display = "none";
}

function submitReasonReDeclineVideo(){
    var action = $('#hiddenAction').val();
    var id = $('#hiddenId').val();
    var declineReason = $('#declineVideoReason').val();
    if(declineReason.length > 1000) {
        alert(trans("'Lý do từ chối duyệt lại' không vượt quá 1000 kí tự!"));
    } else{
        reApproveVideo(id, action, declineReason);
        modal.style.display = "none";
    }
}

function reApproveVideo(id, action, reasonDecline){
    $.ajax({
        method: "POST",
        url: $('#approve-video-after-review-action').val(),
        data: {
            'id': id,
            'action': action,
            'reason': reasonDecline,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.errorCode == 200) {
                $("#reApproveFeedback").css('display', 'none');
            } else {
                alert(response.message);
            }
        },
        error: function (data) {

        }
    });
}

function processChannelProfile(id, action) {
    // switch (action) {
    //
    //     case 'approve':
    //         var cf = confirm('Bạn có đồng ý phê duyệt?');
    //         if (!cf) return;
    //         break;
    //     case 'delete':
    //         var cf = confirm('Bạn có đồng ý không duyệt?');
    //         if (!cf) return;
    //         break;
    // }
    var reason = $('#vtaccountinfomation-reason').val().trim();
    if((action == 'delete' || action == 'ban') && reason == ''){
        alert(trans("Bạn phải nhập lý do từ chối!"));
        return;
    }
    $.ajax({
        method: "POST",
        url: $('#approve-channel-profile-action').val(),
        data: {
            'id': id,
            'action': action,
            'reason': reason,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.errorCode == 200) {
                $("#processButton").replaceWith(response.message);
                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                alert(response.message);
            }
        },
        error: function (data) {

        }
    });
}

function deleteAvatar(){
    var r = confirm(trans("Bạn có chắc chắn muốn xóa avatar không?"));
    if (r == true) {
        $("#avatarDelete").val("1");
        $(".avatar-area").hide();
    }
}

function deleteBanner(){
    var r = confirm(trans("Bạn có chắc chắn muốn xóa banner không?"));
    if (r == true) {
        $("#bannerDelete").val("1");
        $(".banner-area").hide();
    }
}

// function processUserChangeInfo(id, action) {
//     // switch (action) {
//     //
//     //     case 'approve':
//     //         var cf = confirm('Bạn có đồng ý phê duyệt?');
//     //         if (!cf) return;
//     //         break;
//     //     case 'delete':
//     //         var cf = confirm('Bạn có đồng ý không duyệt?');
//     //         if (!cf) return;
//     //         break;
//     // }
//     var reason = $('#vtuserchangeinfo-reason').val().trim();
//     if(action == 'delete' && reason == ''){
//         alert("Bạn phải nhập lý do từ chối!");
//         return;
//     }
//
//     $.ajax({
//         method: "POST",
//         url: $('#user-change-info-action').val(),
//         data: {
//             'id': id,
//             'action': action,
//             'reason': reason,
//             '_csrf': $('meta[name="csrf-token"]').attr("content")
//         },
//         success: function (data) {
//             var response = jQuery.parseJSON(data);
//             if (response.errorCode == 200) {
//                 $("#processButton").replaceWith(response.message);
//                 setTimeout(function() {
//                     location.reload();
//                 }, 2000);
//             } else {
//                 alert(response.message);
//             }
//         },
//         error: function (data) {
//
//         }
//     });
// }

$('#exportContentManagementBtn').on('click', function(){
    var fromDate =  $("#from_time").val();
    var toDate =  $("#to_time").val();
    var type =  $("#type").val();
    window.location = "/report-video/export-content-management?from_date=" + fromDate + "&to_date=" + toDate + "&type=" + type;
});

$('#exportVideoCrawlerByCPBtn').on('click', function(event){
    event.preventDefault();
    var fromDate =  $("#from_time").val();
    var toDate =  $("#to_time").val();
    window.location.href = "/report-video/export-video-crawler-by-cp?from_date=" + fromDate + "&to_date=" + toDate;
});

$('#exportVideoCrawlerByCPDetailBtn').on('click', function(event){
    event.preventDefault();
    var fromDate =  $("#from_time").val();
    var toDate =  $("#to_time").val();
    window.location.href = "/report-video/export-video-crawler-by-cp-detail?from_date=" + fromDate + "&to_date=" + toDate;
});

// -----------------------------------------------------------------------------------------------------------------------------

function approveUserChangeInfoQuick(id, action, reasonDecline) {
    $.ajax({
        method: "POST",
        url: $('#approve-user-change-info').val(),
        data: {
            'id': id,
            'action': action,
            'reason': reasonDecline,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.errorCode == 200) {
                alert(trans('Hoàn thành xử lý'));
                location.reload();
                // $('*[data-key=' + id + ']').replaceWith('<tr><td colspan="8" style="text-align:center"><strong>' + trans('Hoàn thành xử lý') + '</strong></td></tr>');
            } else {
                alert(response.message);
            }
        },
        error: function (data) {

        }
    });
}

function submitReasonDeclineUserChangeInfoQuick(){
    var id = $('#hiddenId').val();
    var action = $('#hiddenAction').val();
    var declineReason = $.trim($('#declineUserChangeInfoReason').val());
    if(declineReason.length == 0) {
        alert(trans('Bạn phải nhập "Lý do từ chối"!'));
    } else if(declineReason.length > 255) {
        alert(trans('"Lý do từ chối" không vượt quá 255 kí tự!'));
    } else{
        approveUserChangeInfoQuick(id, action, declineReason);
        modal.style.display = "none";
    }
}

function submitReasonDeclineUserChangeInfo(){
    var action = $('#hiddenAction').val();
    var id = $('#hiddenId').val();
    var declineReason = $.trim($('#declineUserChangeInfoReason').val());
    if(declineReason.length == 0) {
        alert(trans('Bạn phải nhập "Lý do từ chối"!'));
    } else if(declineReason.length > 255) {
        alert(trans('"Lý do từ chối" không vượt quá 255 kí tự!'));
    } else{
        approveUserChangeInfo(id, action, declineReason);
        modal.style.display = "none";
    }
}

function approveUserChangeInfo(id, action, reasonDecline){
    $.ajax({
        method: "POST",
        url: $('#approve-user-change-info').val(),
        data: {
            'id': id,
            'action': action,
            'reason': reasonDecline,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.errorCode == 200) {
                $("#processButton").replaceWith(response.message);
                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                alert(response.message);
            }
        },
        error: function (data) {

        }
    });
}

//Video fees js
function shareVideoDetail(id) {
    $('#containerShare' + id).show();
}

function copyLinkShare(id) {
    var copyText = document.getElementById("shareWebsite"  + id);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    $('#copy-success'  + id).show();
}

function closeContainerShare(id) {
    $('#containerShare' + id).hide();
    $('#copy-success' + id).hide();
}

function updateVideoPriceplay(id, action) {
    var cf = confirm(trans('Bạn có đồng ý phê duyệt?'));

    if (!cf) {
        setTimeout(function () {
            location.reload();
        }, 500);
        return;
    }

    $.ajax({
        method: "POST",
        url: $('#save-edit-action').val(),
        data: {
            'id': id,
            'action': action,
            'price_play': action,
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.success) {
                setTimeout(function () {
                    location.reload();
                }, 200);
            } else {
                alert(response.message);
            }
        },
        error: function (data) {
        }
    });
}
