// Some options to pass to the uploader are discussed on the next page


$(document).ready(function () {
    $("#playlist-part").sortable({

        stop: function (event, ui) {

            var str = "";
            var playlistId = $('#playlistId').val();
            $(".row-playlist").each(function (index, val) {
                str += "pos[]=" + index + ":" + $(this).attr("rel") + ":" + playlistId + "&";
            });

            url = $('#updatePosition').val();
            $.ajax({
                url: url,
                type: 'post',
                data: str,
                success: function (string) {

                }
            });


        }

    });

});
function removeContent(id) {
    var playlistId = $('#playlistId').val();
    var url = $('#removePlaylist').val();
    var numVideo = $('#vtuserplaylist-num_video').val();
    var value = {
        'itemId': id,
        'playlistId': playlistId,
        '_csrf': $('meta[name="csrf-token"]').attr("content")
    };
    $.ajax({
        url: url,
        type: 'post',
        dataType: "json",
        data: value,
    }).done(function (data) {
        if (data.errorCode == 200) {
            $("#row-playlist-id-" + id).hide();
            $('#vtuserplaylist-num_video').val(parseInt(numVideo) - 1);
        } else {
            alert(trans("Xóa video khỏi playlist thất bại"));
        }
    })
            .fail(function (error) {
                console.error(error);
                return reject(error);
            })
            .always(function () {});
}

var idsChoosen = [];

function addContent(id, name, channel, status) {
    var playlistId = $('#playlistId').val();
    var url = $('#addPlaylist').val();
    var numVideo = $('#vtuserplaylist-num_video').val();
    if(!numVideo) {
        numVideo = 0;
    }

    var limit = $('#limitContent').val();
    var value = {
        'itemId': id,
        'playlistId': playlistId,
        '_csrf': $('meta[name="csrf-token"]').attr("content")
    };
    if ((parseInt(numVideo) + 1) > limit) {
        alert(trans('Nội dung thêm vào vượt quá ngưỡng') + ' ' + limit + ' ' + trans("cho phép!"));
    } else {
        $.ajax({
            url: url,
            type: 'post',
            dataType: "json",
            data: value,
        }).done(function (data) {
            console.log(data);
            if (data.errorCode == 200) {
                // $.pjax.reload({url: url, container: '#dcm', push: false, replace: false, timeout: 8000});
                var html = '<tr class="row-playlist ui-sortable-handle" id="row-playlist-id-' + id + '" rel="' + id + '">'
                        + '<th scope="row">' + (parseInt(numVideo) + 1) + '</th>'
                        + ' <td><a href="/vt-video/update?id=' + id + '" class="part-name" data-type="text" data-pk="' + id + '" data-url="/vt-video/update?id=' + id + '" data-title="' + trans("Sửa tên") + '">' + htmlEntities(Base64.decode(name)) + '</a></td>'
                        + '<td>' + htmlEntities(Base64.decode(channel)) + '</td>'
                        + '<td>' + htmlEntities(Base64.decode(status)) + '</td>'
                        + '<td class="col-xs-1">'
                        + '<button type="button" class="btn btn-danger" onclick="removeContent(' + id + ')">'  + trans("Xóa") + '</button></td>'
                        + '</tr>';
//            console.log(html);
                $("#vtuserplaylist-num_video").val(parseInt(numVideo) + 1);
                $("#playlist-part").append(html);
                $("#row-playlist" + id).hide();
            } else {
                alert("Thêm video vào playlist thất bại");
            }
            $('#filter-search').submit();
        })
                .fail(function (error) {
                    console.error(error);
//                return reject(error);
                })
                .always(function () {});
    }
}
function finishContent(id) {
    var url = $('#finishPlaylist').val();
    var arr = new Array();
    $(".row-playlist").each(function (index, val) {
        arr[index] = $(this).attr("rel");
    });
    var value = {
        'data': arr,
        'playlistId': id,
        '_csrf': $('meta[name="csrf-token"]').attr("content")
    };
    console.log(value);
    $.ajax({
        url: url,
        type: 'post',
        dataType: "json",
        data: value,
    }).done(function (data) {
        console.log(data);
        if (data.errorCode == 200) {
            $("#btn-finish").hide();
            //window.location.href = '/vt-user-playlist/update?id=' + id;
            //alert("Cập nhật hoàn thành thành công");
        } else {
            alert(trans("Cập nhật hoàn thành thất bại"));
        }
    })
        .fail(function (error) {
            console.error(error);
            return reject(error);
        })
        .always(function () {});
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }}

