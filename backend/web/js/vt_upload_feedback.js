$( "#numOfRecordPerPage" ).change(function() {
    var url      = window.location.href;
    var selectedChoice = $('#numOfRecordPerPage').find(":selected").text();
    var url = window.location.href;
    var newUrlObj = '';
    if(url.indexOf('num_of_record') == -1){
        if (url.indexOf('?') > -1){
            url += '&num_of_record=' + selectedChoice;
        } else {
            url += '?num_of_record=' + selectedChoice;
        }
        window.location.href = url;
    } else if(url.indexOf('num_of_record') > -1){
        newUrlObj = new URL(url);
        newUrlObj.searchParams.set('num_of_record', selectedChoice);
        window.location.href = newUrlObj;
    }
});