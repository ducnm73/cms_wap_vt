$('#view-all').on('click', function() {
    var startDate = $('#from_time').val();
    var endDate = $('#to_time').val();
    var feedbackTypeId = $('#feedback_type_id').val();
    $.get('/report/report-content-violation-all', {start_date: startDate, end_date: endDate, feedback_type_id: feedbackTypeId}, function(data){
        $('#table_area').html(data);
    })
});