$('.exportChartButton').on('click', function(){
    var startDate=  $("#reportrange").data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate=  $("#reportrange").data('daterangepicker').endDate.format('YYYY-MM-DD');
    window.location = "/report-kpi/export-chart-excel?start_date=" + startDate + "&end_date=" + endDate;
});

$('.drawChartButton').on('click', function(event){
    event.preventDefault();
    var startDate=  $("#reportrange").data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate=  $("#reportrange").data('daterangepicker').endDate.format('YYYY-MM-DD');

    $.get('/report-kpi/draw-chart', {start_date: startDate, end_date: endDate}, function(data){
        var parsedData = JSON.parse(data);
        var realKPIValue = parsedData[0];
        var thresholdKPIValue = parsedData[1][0];

        var pageloadData = [];
        var pageloadForAppData = [];
        var uploadTypeData = [];
        var successRateForWebData = [];
        var successRateData = [];
        var encodeTypeData = [];
        var bufferRateData = [];
        var bufferTimeRateData = [];
        var bufferOver3sRateData = [];
        for (var i in realKPIValue) {
            if (realKPIValue[i].type == 'pageload_app') {
                pageloadForAppData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'pageload') {
                pageloadData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'upload') {
                uploadTypeData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'access_rate_wap') {
                successRateForWebData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'access_rate') {
                successRateData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'encode') {
                encodeTypeData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'buffer_rate') {
                bufferRateData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'buffer_duration_rate') {
                bufferTimeRateData.push(realKPIValue[i]);
            } else if (realKPIValue[i].type == 'buffer_rate_3s') {
                bufferOver3sRateData.push(realKPIValue[i]);
            }
        }

        var display_time_app = thresholdKPIValue.display_time_app;
        var display_time_web = thresholdKPIValue.display_time_web;
        var success_access_rate_app = thresholdKPIValue.success_access_rate_app;
        var success_access_rate_wap = thresholdKPIValue.success_access_rate_web;
        var avg_upload_speed = thresholdKPIValue.avg_upload_speed;
        var error_upload_rate = thresholdKPIValue.error_upload_rate;
        var encode_video_time = thresholdKPIValue.encode_video_time;
        var error_encode_rate = thresholdKPIValue.error_encode_rate;
        var wait_time = thresholdKPIValue.wait_time;
        var buffer_rate = thresholdKPIValue.buffer_rate;
        var buffer_duration_rate = thresholdKPIValue.buffer_duration_rate;
        var buffer_rate_3s = thresholdKPIValue.buffer_over_rate;

        //DRAW SUCCESS_RATE FOR APP CHART
        drawPageloadForAppChart(pageloadForAppData, display_time_app);
        //DRAW SUCCESS_RATE FOR WEB CHART
        drawPageloadChart(pageloadData, display_time_web);
        //DRAW SUCCESS_RATE FOR APP CHART
        drawSuccessRateChart(successRateData, success_access_rate_app);
        //DRAW SUCCESS_RATE FOR WEB CHART
        drawSuccessRateForWebChart(successRateForWebData, success_access_rate_wap);
        //DRAW BANDWIDTH CHART
        drawBandwidthChart(uploadTypeData, avg_upload_speed);
        //DRAW FAILED_RATE CHART
        drawFailedRateChart(uploadTypeData, error_upload_rate);
        //DRAW 'VIDEO ENCODING TIME' CHART
        drawVideoEncodingTimeChart(encodeTypeData, encode_video_time);
        //DRAW 'VIDEO ENCODING TIME' CHART
        drawEncodeFailureRateChart(encodeTypeData, error_encode_rate);
        //DRAW 'VIDEO ENCODING TIME' CHART
        drawVideoWaitingTimeChart(bufferRateData, wait_time);
        //DRAW 'VIDEO BUFFER RATE' CHART
        drawVideoBufferRateChart(bufferRateData, buffer_rate);
        //DRAW 'VIDEO BUFFER RATE' CHART
        drawVideoBufferTimeRateChart(bufferTimeRateData, buffer_duration_rate);
        //DRAW 'VIDEO BUFFER OVER 3S RATE' CHART
        drawVideoBufferOver3sRateChart(bufferOver3sRateData, buffer_rate_3s);
    });
});

var pageloadForAppChart;
var pageloadChart;
var successRateChart;
var successRateForWebChart;
var avgBandwidthChart;
var failedRateChart;
var videoEncodingTimeChart;
var encodeFailureRateChart;
var videoWaitingTimeChart;
var videoBufferRateChart;
var videoBufferTimeRateChart;
var videoBufferOver3sRateChart;

function drawPageloadForAppChart(pageloadForAppData, display_time_app){
    var timer = [];
    var pageload = [];
    var correctTime = false;
    for( var i in pageloadForAppData){
        if(pageloadForAppData[i].report_hour != undefined){
            timer.push(pageloadForAppData[i].report_hour);
        } else {
            timer.push(pageloadForAppData[i].report_day);
            correctTime = true;
        }
        pageload.push((pageloadForAppData[i].avg_time)/1000);
    }

    var targetValueArr = [];
    var targetValue = display_time_app;
    for (var i = 0; i < pageload.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Thời gian hiển thị trang trên App",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: pageload,
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = 'giây';

    //clear chart
    if(pageloadForAppChart != undefined){
        pageloadForAppChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_pageloadForApp");
    pageloadForAppChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Thời gian hiển thị trang trên App ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom',
                boxHeight: 1,
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yAxisUnit
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawPageloadChart(pageloadData, display_time_web){
    var timer = [];
    var pageload = [];
    var correctTime = false;
    for( var i in pageloadData){
        if(pageloadData[i].report_hour != undefined){
            timer.push(pageloadData[i].report_hour);
        } else {
            timer.push(pageloadData[i].report_day);
            correctTime = true;
        }
        pageload.push((pageloadData[i].avg_time)/1000);
    }

    var targetValueArr = [];
    var targetValue = display_time_web;
    for (var i = 0; i < pageload.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Thời gian hiển thị trang trên web/wap",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: pageload,
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = 'giây';

    //clear chart
    if(pageloadChart != undefined){
        pageloadChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_pageload");
    pageloadChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Thời gian hiển thị trang trên web/wap ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom',
                boxHeight: 1,
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yAxisUnit
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawSuccessRateChart(successRateData, success_access_rate_app){
    var timer = [];
    var successRate = [];
    var correctTime = false;
    for( var i in successRateData){
        if(successRateData[i].report_hour != undefined){
            timer.push(successRateData[i].report_hour);
        } else {
            timer.push(successRateData[i].report_day);
            correctTime = true;
        }
        successRate.push(successRateData[i].success_rate);
    }

    var targetValueArr = [];
    var targetValue = success_access_rate_app;
    for (var i = 0; i < successRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ truy cập thành công trên App",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: successRate,
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(successRateChart != undefined){
        successRateChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_successRate");
    successRateChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ truy cập thành công trên App ( > '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom',
                boxHeight: 1,
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 95,
                        // max: 100,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawSuccessRateForWebChart(successRateForWebData, success_access_rate_wap){
    var timer = [];
    var successRate = [];
    var correctTime = false;
    for( var i in successRateForWebData){
        if(successRateForWebData[i].report_hour != undefined){
            timer.push(successRateForWebData[i].report_hour);
        } else {
            timer.push(successRateForWebData[i].report_day);
            correctTime = true;
        }
        successRate.push(successRateForWebData[i].success_rate);
    }

    var targetValueArr = [];
    var targetValue = success_access_rate_wap;
    for (var i = 0; i < successRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ truy cập thành công trên web/wap",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: successRate,
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(successRateForWebChart != undefined){
        successRateForWebChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_successRateForWeb");
    successRateForWebChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ truy cập thành công trên web/wap ( > '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom',
                boxHeight: 1,
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 95,
                        // max: 100,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawBandwidthChart(uploadTypeData, avg_upload_speed){
    var timer = [];
    var avgBandwidth = [];
    var correctTime = false;
    for( var i in uploadTypeData){
        if(uploadTypeData[i].report_hour != undefined){
            timer.push(uploadTypeData[i].report_hour);
        } else {
            timer.push(uploadTypeData[i].report_day);
            correctTime = true;
        }
        avgBandwidth.push(uploadTypeData[i].avg_bandwidth);
    }

    var targetValueArr = [];
    var targetValue = avg_upload_speed;
    for (var i = 0; i < avgBandwidth.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tốc độ upload trung bình",
                fill: false,
                // lineTension: 0.5,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: avgBandwidth,
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                // lineTension: 0.5,
                // backgroundColor: "rgba(59, 89, 152, 0.75)",
                backgroundColor: "red",
                borderColor: "red",
                // pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                // pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: targetValueArr,
                pointRadius: 0
            }
        ]
    };

    var yAxisUnit = 'Kbps';

    //clear chart
    if(avgBandwidthChart != undefined){
        avgBandwidthChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_avgBandwidth");
    avgBandwidthChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tốc độ upload trung bình ( > '+ targetValue +' '+ yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom',
                // labels: {
                //     fontColor: 'rgb(255, 99, 132)'
                // }
            },
            // annotation: {
            //     annotations: [{
            //         type: 'line',
            //         mode: 'horizontal',
            //         scaleID: 'y-axis-0',
            //         value: 600,
            //         borderColor: 'red',
            //         borderWidth: 2,
            //         label: {
            //             enabled: true,
            //             content: 'Giá trị mục tiêu',
            //             position: "right"
            //         }
            //     }]
            // },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yAxisUnit
                    },
                    // ticks: {
                    //     min: 0,
                    //     max: 1000
                    // }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawFailedRateChart(uploadTypeData, error_upload_rate){
    var timer = [];
    var failedRate = [];
    var correctTime = false;
    for( var i in uploadTypeData){
        if(uploadTypeData[i].report_hour != undefined){
            timer.push(uploadTypeData[i].report_hour);
        } else {
            timer.push(uploadTypeData[i].report_day);
            correctTime = true;
        }
        failedRate.push(uploadTypeData[i].failure_rate);
    }
    var targetValueArr = [];
    var targetValue = error_upload_rate;
    for (var i = 0; i < failedRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ upload lỗi",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: failedRate
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(failedRateChart != undefined){
        failedRateChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_failedRate");
    failedRateChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ upload lỗi ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 0,
                        // max: 5,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawVideoEncodingTimeChart(encodeTypeData, encode_video_time){
    var timer = [];
    var avgTime = [];
    var correctTime = false;
    for( var i in encodeTypeData){
        if(encodeTypeData[i].report_hour != undefined){
            timer.push(encodeTypeData[i].report_hour);
        } else {
            timer.push(encodeTypeData[i].report_day);
            correctTime = true;
        }
        avgTime.push(parseFloat((encodeTypeData[i].avg_time)/(60*1000)).toFixed(2));
    }
    var targetValueArr = [];
    var targetValue = encode_video_time/60;
    for (var i = 0; i < avgTime.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Thời gian encode video",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: avgTime
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = 'phút';

    //clear chart
    if(videoEncodingTimeChart != undefined){
        videoEncodingTimeChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_videoEncodingTime");
    videoEncodingTimeChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Thời gian encode video ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yAxisUnit
                    },
                    // ticks: {
                    //     min: 0,
                    //     max: 20
                    // }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawEncodeFailureRateChart(encodeTypeData, error_encode_rate){
    var timer = [];
    var failureRate = [];
    var correctTime = false;
    for( var i in encodeTypeData){
        if(encodeTypeData[i].report_hour != undefined){
            timer.push(encodeTypeData[i].report_hour);
        } else {
            timer.push(encodeTypeData[i].report_day);
            correctTime = true;
        }
        failureRate.push(encodeTypeData[i].failure_rate);
    }
    var targetValueArr = new Array(failureRate.length);
    var targetValue = error_encode_rate;

    for (var i = 0; i < failureRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ encode lỗi",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: failureRate
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(encodeFailureRateChart != undefined){
        encodeFailureRateChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_encodeFailureRate");
    encodeFailureRateChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ encode lỗi ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 0,
                        // max: 2,
                        // stepSize: 0.2,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawVideoWaitingTimeChart(bufferRateData, wait_time){
    var timer = [];
    var avgTime = [];
    var correctTime = false;
    for( var i in bufferRateData){
        if(bufferRateData[i].report_hour != undefined){
            timer.push(bufferRateData[i].report_hour);
        } else {
            timer.push(bufferRateData[i].report_day);
            correctTime = true;
        }
        avgTime.push((bufferRateData[i].avg_time)/1000);
    }
    var targetValueArr = [];
    var targetValue = wait_time;
    for (var i = 0; i < avgTime.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Thời gian chờ để bắt đầu xem được video",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: avgTime
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = 'giây';

    //clear chart
    if(videoWaitingTimeChart != undefined){
        videoWaitingTimeChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_videoWaitingTime");
    videoWaitingTimeChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Thời gian chờ để bắt đầu xem được video ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yAxisUnit
                    },
                    // ticks: {
                    //     min: 0,
                    //     max: 5
                    // }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawVideoBufferRateChart(bufferRateData, buffer_rate){
    var timer = [];
    var failureRate = [];
    var correctTime = false;
    for( var i in bufferRateData){
        if(bufferRateData[i].report_hour != undefined){
            timer.push(bufferRateData[i].report_hour);
        } else {
            timer.push(bufferRateData[i].report_day);
            correctTime = true;
        }
        failureRate.push(bufferRateData[i].failure_rate);
    }
    var targetValueArr = [];
    var targetValue = buffer_rate;
    for (var i = 0; i < failureRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ số lần bị buffer khi đang xem video",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: failureRate
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(videoBufferRateChart != undefined){
        videoBufferRateChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_videoBufferRate");
    videoBufferRateChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ số lần bị buffer khi đang xem video ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 0,
                        // max: 100,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawVideoBufferTimeRateChart(bufferTimeRateData, buffer_duration_rate){
    var timer = [];
    var failureRate = [];
    var correctTime = false;
    for( var i in bufferTimeRateData){
        if(bufferTimeRateData[i].report_hour != undefined){
            timer.push(bufferTimeRateData[i].report_hour);
        } else {
            timer.push(bufferTimeRateData[i].report_day);
            correctTime = true;
        }
        failureRate.push(bufferTimeRateData[i].failure_rate);
    }
    var targetValueArr = [];
    var targetValue = buffer_duration_rate;
    for (var i = 0; i < failureRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ thời gian bị buffer khi đang xem video",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: failureRate
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(videoBufferTimeRateChart != undefined){
        videoBufferTimeRateChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_videoBufferTimeRate");
    videoBufferTimeRateChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ thời gian bị buffer khi đang xem video ( < '+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 0,
                        // max: 50,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}

function drawVideoBufferOver3sRateChart(bufferOver3sRateData, buffer_rate_3s){
    var timer = [];
    var failureRate = [];
    var correctTime = false;
    for( var i in bufferOver3sRateData){
        if(bufferOver3sRateData[i].report_hour != undefined){
            timer.push(bufferOver3sRateData[i].report_hour);
        } else {
            timer.push(bufferOver3sRateData[i].report_day);
            correctTime = true;
        }
        failureRate.push(bufferOver3sRateData[i].failure_rate);
    }
    var targetValueArr = [];
    var targetValue = buffer_rate_3s;
    for (var i = 0; i < failureRate.length; i++){
        targetValueArr[i] = targetValue;
    }

    var chartdata = {
        labels: timer,
        datasets: [
            {
                label: "Tỷ lệ số lần buffer vượt quá 3s",
                fill: false,
                backgroundColor: "rgba(59, 89, 152, 1)",
                borderColor: "rgba(59, 89, 152, 1)",
                pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                data: failureRate
            },
            {
                label: "Giá trị mục tiêu",
                fill: false,
                backgroundColor: "red",
                borderColor: "red",
                pointRadius: 0,
                data: targetValueArr
            }
        ]
    };

    var yAxisUnit = '%';

    //clear chart
    if(videoBufferOver3sRateChart != undefined){
        videoBufferOver3sRateChart.destroy();
    }

    //draw chart
    var ctx = $("#canvas_videoBufferOver3sRate");
    videoBufferOver3sRateChart = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        responsive: true,
        options: {
            title: {
                display: true,
                text: 'Tỷ lệ số lần buffer vượt quá 3s ('+ targetValue + yAxisUnit +')',
                fontSize: 15,
                fontFamily: "'Palatino Linotype', 'Book Antiqua', 'Palatino', serif",
                fontColor: '#666'
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true
                    },
                    ticks: {
                        // min: 0,
                        // max: 50,
                        callback: function(value, index, values) {
                            return parseFloat(value).toFixed(1) + yAxisUnit;
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: correctTime == false ? 'Giờ' : 'Ngày'
                    },
                    ticks: {
                        autoSkip: false
                    },
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + ' ' + yAxisUnit;
                    }
                }
            }
        }
    });
}