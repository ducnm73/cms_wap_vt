<?php

namespace backend\helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Yii;



class VtExcelCustomer {

    public static function exportExcel($reports, $file_name = 'Report', $sheet_title = 'Report', $auto_size = true, $start_row = 1, $font_size = '12', $font_face = null) {

        //ini_set('max_execution_time', 120);
        //ini_set('memory_limit', -1);
        $char = self::createChar();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        if (!is_null($font_face)) {
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName($font_face);
        }

        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize($font_size);

        $objPHPExcel->getActiveSheet()->setTitle(substr($sheet_title, 0, 30));

        $c = 0;
        $r = $start_row;
        foreach ($reports as $title => $report) {

            $objPHPExcel->getActiveSheet()->setCellValue($char[$c] . $r++, $title);
            foreach ($report as $data) {
                $objPHPExcel->getActiveSheet()->setCellValue($char[$c] . $r++, $data);
            }
            $c++;
            $r = $start_row;
        }

        $styles = array('font' => array('bold' => true));

        for ($i = 0; $i < count($reports); $i++) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($char[$i])->setAutoSize($auto_size);
            $objPHPExcel->getActiveSheet()->getStyle($char[$i] . $start_row)->applyFromArray($styles);
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $file_path = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
        $file_location = $file_path . $file_name . '.xls';

        $objWriter->save($file_location);

        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/octetstream");
        header("Content-Disposition: attachment; filename=" . basename($file_location));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($file_location));

        @readfile($file_location);
        @unlink($file_location);
    }

    /**
     * @param $searchModel DowngradeRegChargeSearch|Model
     * @param $queryParams
     * @param $fields
     * @param $label
     * @param array $extraTexts mảng các text muốn hiển thị dưới tiêu đề
     * @param array $totalAttributes
     * @param mixed $boldRowCallback function trả về true hoặc false để bôi đậm dòng tương ứng
     * @param $fileName
     * @throws ClassNotFoundException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public static function exportToExcel($searchModel, $queryParams, $fields, $label, $extraTexts = array(), $totalAttributes = array(), $boldRowCallback = false, $fileName, $showOrderCol = true, $searchMethod = 'search') {
        if (!class_exists('PHPExcel')) {
            throw new ClassNotFoundException("Class PHPExcel not exist!", 1);
        }

        $dataProvider = $searchModel->$searchMethod($queryParams);
        $sort = $dataProvider->getSort();

        if(isset($queryParams['sort'])) {
            $sort->params = ['sort' => $queryParams['sort']];
        }
        
        $dataProvider->setSort($sort);
        $styleArray = array(
            'font' => array(
                'size' => 14,
                'name' => 'Times NewRoman'
        ));

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
        $objPHPExcel->getProperties()->setTitle($label);
        $objPHPExcel->getProperties()->setDescription($label);
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', mb_strtoupper($label, "UTF-8"));
        $tableStartRow = 2;

        foreach ($extraTexts as $key => $extraText) {
            $objPHPExcel->getActiveSheet()->setCellValue("A$tableStartRow", "$key: $extraText");
            $tableStartRow ++;
        }

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 18
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ]);
        $exportData = $dataProvider;
        $exportData->setPagination(false);

        if ($showOrderCol) {
            $objPHPExcel->getActiveSheet()->setCellValue("A$tableStartRow", 'STT');
            $colIndex = 1;
        } else {
            $colIndex = 0;
        }

        $maxCol = 0;

        for ($i = 0; $i < sizeof($fields); $i ++) {
            if (is_array($fields[$i])) {
                $key = $fields[$i]['attribute'];
            } else {
                $key = $fields[$i];
            }

            if (empty($key)) {
                continue;
            }

            $col = \PHPExcel_Cell::stringFromColumnIndex($colIndex ++);
            $colLabel = isset($fields[$i]['label']) ? $fields[$i]['label'] : $searchModel->getAttributeLabel($key);
            $objPHPExcel->getActiveSheet()->setCellValue($col . $tableStartRow, $colLabel);
            $maxCol = $col;
        }

        // header row
        $objPHPExcel->getActiveSheet()->getStyle("A$tableStartRow:" . $maxCol . $tableStartRow)->applyFromArray(
                [
                    'font' => [
                        'bold' => true
                    ],
                    'alignment' => [
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ]
                ]
        );

        $objPHPExcel->getActiveSheet()->mergeCells('A1:' . $maxCol . '1');
        $row = $tableStartRow;

        foreach ($exportData->models as $item) {
            if ($showOrderCol) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . strval($row + 1), ($row - $tableStartRow + 1));
                $colIndex = 1;
            } else {
                $colIndex = 0;
            }

            for ($i = 0; $i < sizeof($fields); $i ++) {
                if (is_array($fields[$i])) {
                    $k = $fields[$i]['attribute'];
                } else {
                    $k = $fields[$i];
                }

                $col = \PHPExcel_Cell::stringFromColumnIndex($colIndex ++);
                $value = is_array($item) ? (isset($item[$k]) ? $item[$k] : null) : $item->$k;

                if (isset($fields[$i]['callback'])) {
                    $cellVal = call_user_func_array($fields[$i]['callback'], [$item]);
                } else {
                    $cellVal = $value;
                }

                $objPHPExcel->getActiveSheet()->setCellValue($col . strval($row + 1), $cellVal);
            }

            $needToBold = false;
            // set format bold
            if ($boldRowCallback !== false) {
                $needToBold = call_user_func_array($boldRowCallback, [$item]);
            }

            if ($needToBold) {
                $objPHPExcel->getActiveSheet()->getStyle('A' . ($row + 1) . ':' . $maxCol . ($row + 1))->applyFromArray(
                        [
                            'font' => [
                                'bold' => true
                            ],
                        ]
                );
            }

            $row++;
        }

        if (sizeof($totalAttributes) > 0) {
            $row ++;
            $objPHPExcel->getActiveSheet()->setCellValue('A' . strval($row), Yii::t('backend', 'Tổng'));

            foreach ($totalAttributes as $attribute) {
                $colIndex = 0;

                for ($i = 0; $i < sizeof($fields); $i ++) {
                    $colIndex ++;

                    if (is_array($fields[$i])) {
                        $k = $fields[$i]['attribute'];
                    } else {
                        $k = $fields[$i];
                    }

                    if ($attribute == $k) {
                        $col = \PHPExcel_Cell::stringFromColumnIndex($colIndex ++);
                        $cellVal = $dataProvider->query->sum($attribute);
                        $objPHPExcel->getActiveSheet()->setCellValue($col . strval($row), $cellVal);
                    }
                }
            }

            $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':' . $maxCol . $row)->applyFromArray(
                    [
                        'font' => [
                            'bold' => true
                        ],
                    ]
            );
        }

        // cot STT
        if ($showOrderCol) {
            $objPHPExcel->getActiveSheet()->getStyle("A$tableStartRow:A" . $objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray([
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ]
            ]);
            $colIndex = 1;
        } else {
            $colIndex = 0;
        }

        for ($i = 0; $i < sizeof($fields); $i ++) {
            if (is_array($fields[$i])) {
                $key = $fields[$i]['attribute'];
            } else {
                $key = $fields[$i];
            }

            if (empty($key)) {
                continue;
            }

            $col = \PHPExcel_Cell::stringFromColumnIndex($colIndex ++);
            $objPHPExcel->getActiveSheet()->getStyle($col . ($tableStartRow + 1) . ":$col" . $objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray([
                'alignment' => [
                    'horizontal' => isset($fields[$i]['h-align']) ? $fields[$i]['h-align'] : \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,//HORIZONTAL_RIGHT
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ]
            ]);
            $objPHPExcel->getActiveSheet()->getStyle("B" . ($tableStartRow + 1) . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray([
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ]
            ]);
        }

        $objPHPExcel->getActiveSheet()->getRowDimension($tableStartRow)->setRowHeight(40);

        if ($showOrderCol) {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $colIndex = 1;
        } else {
            $colIndex = 0;
        }

        foreach ($fields as $key => $value) {
            $col = \PHPExcel_Cell::stringFromColumnIndex($colIndex ++);
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(array_key_exists('width', $value) ? $value['width'] : 20);
        }

        $objPHPExcel->getActiveSheet()->getStyle('B' . ($tableStartRow + 1) . ':' . $maxCol . $objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray([
            'alignment' => [
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_TOP,
            ]
        ]);
        $objPHPExcel
                ->getActiveSheet()
                ->getStyle("A$tableStartRow:" . $maxCol . $objPHPExcel->getActiveSheet()->getHighestRow())
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
        // Redirect output to a client’s web browser (Excel5)
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '_' . date('d/m/Y') . '.xls"');
        $objWriter->save('php://output');
    }

    /**
     * Tao mang ky tu dung cho Excel Export
     * @return array
     */
    private static function createChar() {
        $c = 0;
        $str_arr = array();
        for ($i = 0; $i < 26; $i++) {
            $str_arr[$c] = chr(65 + $i);
            $c++;
        };
        for ($i = 0; $i < 26; $i++)
            for ($j = 0; $j < 26; $j++) {
                $str_arr[$c] = chr(65 + $i) . chr(65 + $j);
                $c++;
            }
        return $str_arr;
    }

    public static function exportRow($header, $contents, $file_name = 'Report', $sheet_title = 'Report'){

        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setCreator('VAS-VTT')
            ->setLastModifiedBy('VAS-VTT')
            ->setSubject('');

        $sheet_1 = $excel->getSheet(0);

        $column = 'a';
        foreach($header as $item){
            $sheet_1->setCellValue(strtoupper($column) . '1', $item);
            $column++;
        }


        $header = 'A1:I1';
        $sheet_1->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ffff00');

        $startRow = 2;

        foreach($contents as $content){
            $column = 'a';
            foreach($content as $item){

                $sheet_1->setCellValue(strtoupper($column) . $startRow, $item);
                $column++;
            }

            $startRow++;
        }

        $file = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $file_path = Yii::$app->basePath . '/web/';
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $exportTime = '_' . date('His');
        $file_location = $file_path . $file_name . $exportTime . '.xlsx';
        $file->setIncludeCharts(true);
        $file->save($file_location);
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=" . basename($file_location));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($file_location));
        @readfile($file_location);
        @unlink($file_location);
    }
}
