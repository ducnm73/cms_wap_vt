<?php

namespace backend\helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Yii;



class VtExcel {

    public static function exportExcel($reports, $file_name = 'Report', $sheet_title = 'Report', $auto_size = true, $start_row = 1, $font_size = '12', $font_face = null) {

        //ini_set('max_execution_time', 120);
        //ini_set('memory_limit', -1);
        $char = self::createChar();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        if (!is_null($font_face)) {
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName($font_face);
        }

        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize($font_size);

        $objPHPExcel->getActiveSheet()->setTitle(substr($sheet_title, 0, 30));

        $c = 0;
        $r = $start_row;
        foreach ($reports as $title => $report) {

            $objPHPExcel->getActiveSheet()->setCellValue($char[$c] . $r++, $title);
            foreach ($report as $data) {
                $objPHPExcel->getActiveSheet()->setCellValue($char[$c] . $r++, $data);
            }
            $c++;
            $r = $start_row;
        }

        $styles = array('font' => array('bold' => true));

        for ($i = 0; $i < count($reports); $i++) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($char[$i])->setAutoSize($auto_size);
            $objPHPExcel->getActiveSheet()->getStyle($char[$i] . $start_row)->applyFromArray($styles);
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $file_path = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
        $file_location = $file_path . $file_name . '.xls';

        $objWriter->save($file_location);

        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/octetstream");
        header("Content-Disposition: attachment; filename=" . basename($file_location));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($file_location));

        @readfile($file_location);
        @unlink($file_location);
    }


    public static function exportRow($header, $contents, $file_name = 'Report', $sheet_title = 'Report'){

        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setCreator('VAS-VTT')
            ->setLastModifiedBy('VAS-VTT')
            ->setSubject('');

        $sheet_1 = $excel->getSheet(0);

        $column = 'a';
        foreach($header as $item){
            $sheet_1->setCellValue(strtoupper($column) . '1', $item);
            $column++;
        }


        $header = 'A1:I1';
        $sheet_1->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ffff00');

        $startRow = 2;

        foreach($contents as $content){
            $column = 'a';
            foreach($content as $item){

                $sheet_1->setCellValue(strtoupper($column) . $startRow, $item);
                $column++;
            }

            $startRow++;
        }

        $file = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $file_path = Yii::$app->basePath . '/web/';
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $exportTime = '_' . date('His');
        $file_location = $file_path . $file_name . $exportTime . '.xlsx';
        $file->setIncludeCharts(true);
        $file->save($file_location);
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=" . basename($file_location));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($file_location));
        @readfile($file_location);
        @unlink($file_location);


    }


        /**
     * Tao mang ky tu dung cho Excel Export
     * @return array
     */
    private static function createChar() {
        $c = 0;
        $str_arr = array();
        for ($i = 0; $i < 26; $i++) {
            $str_arr[$c] = chr(65 + $i);
            $c++;
        };
        for ($i = 0; $i < 26; $i++)
            for ($j = 0; $j < 26; $j++) {
                $str_arr[$c] = chr(65 + $i) . chr(65 + $j);
                $c++;
            }
        return $str_arr;
    }

}
