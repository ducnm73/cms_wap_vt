cd $HOME/apps/meuclip_www
rsync -R backend/controllers/VtUserController.php ~/apps/meuclip_20210921_backup
rsync -R backend/controllers/VtVideoController.php ~/apps/meuclip_20210921_backup
rsync -R backend/models/LogTransactionSearch.php ~/apps/meuclip_20210921_backup
rsync -R backend/models/VtComment.php ~/apps/meuclip_20210921_backup
rsync -R backend/models/VtCommentSearch.php ~/apps/meuclip_20210921_backup
rsync -R backend/models/VtUserChangeInfoSearch.php ~/apps/meuclip_20210921_backup
rsync -R backend/models/VtUserPlaylistSearch.php ~/apps/meuclip_20210921_backup
rsync -R backend/models/VtVideoSearch.php ~/apps/meuclip_20210921_backup
# rsync -Rr superapi ~/apps/meuclip_20210921_backup
rsync -R common/config/bootstrap.php ~/apps/meuclip_20210921_backup