rsync -R backend/controllers/VtUserController.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/controllers/VtVideoController.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/models/LogTransactionSearch.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/models/VtComment.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/models/VtCommentSearch.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/models/VtUserChangeInfoSearch.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/models/VtUserPlaylistSearch.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R backend/models/VtVideoSearch.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -Rr superapi ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R common/config/bootstrap.php ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode

rsync -R meuclip_20210921_backup.sh ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R meuclip_20210921_upcode.sh ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode
rsync -R meuclip_20210921_restore.sh ~/Projects/Meuclip/Upcode/meuclip_20210921_upcode