<?php
namespace api\modules\v1\controllers;

use common\models\LogPushNotificationSearchBase;
use common\modules\v1\libs\ResponseCode;

class DefaultController extends \common\modules\v1\controllers\DefaultController
{
    public function init()
    {
        parent::initApi();
    }

    public function actionError()
    {
        return [
            'responseCode' => ResponseCode::SYSTEM_ERROR,
            'message' => ResponseCode::getMessage(ResponseCode::SYSTEM_ERROR),
        ];
    }

    public function actionGetNotification()
    {
        $limit = \Yii::$app->request->post('limit', 30);
        $offset = \Yii::$app->request->post('offset', 0);
        $clientId = \Yii::$app->request->post('token', '');
        $dataResponse = array();

        $items = [];

        $logs = LogPushNotificationSearchBase::getLogsByClientId($clientId, $offset, $limit);
        foreach ($logs as $log) {
            if (array_key_exists('_source', $log)) {
                $items[] = [
                    'id' => $log['_source']['item_id'],
                    'aps' => [
                        'alert' => $log['_source']['message'],
                    ],
                    'group_id' => $log['_source']['group_id'],
                    'sent_time' => date("Y-m-d H:i:s", strtotime($log['_source']['sent_time'])),
                    'type' => $log['_source']['type']
                ];
            }
        }

        $dataResponse['notifications'] = $items;
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
            'data' => $dataResponse
        ];
    }
}
