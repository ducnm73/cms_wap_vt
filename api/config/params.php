<?php

return [
    'cache.enabled' => true,
    'app.source' => 'API',
    'app.slideshow.limit' => 5,
    'app.newfeed.limit' => 10,
    'app.focus.limit' => 12,
    'app.home.limit' => 10,
    'app.page.limit' => 4,
    'app.page.film.limit'=>6,
    'app.search.suggestion.limit' => 24,
    'app.search.page.limit' => 24,
    'slideshow.limit'=>5,

    'show.more.link' => 'http://myclip.vn/xem-them/'

];
