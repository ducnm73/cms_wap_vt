<?php

namespace cp\controllers;

use common\models\VtUserBase;
use wap\models\VtUser;
use Yii;
use common\models\AuthUser;
use backend\models\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Change password an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionChange()
    {
        $model = $this->findModel(Yii::$app->user->id);
        $pass = $model->password_hash;
        if ($model->load(Yii::$app->request->post())) {
            $oldPass = trim(Yii::$app->request->post()[$model->formName()]['password_old']);
            if (password_verify($oldPass, $pass)) {
                $passWeak = \Yii::$app->params['pass-weak'];
                $newPass = trim(Yii::$app->request->post()[$model->formName()]['password_hash']);
                if ($oldPass != $newPass) {
                    if (!in_array($newPass, $passWeak)) {
                        $model->is_first_login = 0;
                        if ($model->save()) {
                            \Yii::$app->user->login($model, 1800);
                            return $this->goHome();
                        }
                    } else {
                        $model->addError('password_hash', 'Mật khẩu mới chưa đủ mạnh!');
                    }
                } else {
                    $model->addError('password_hash', 'Mật khẩu mới không được trùng với mật khẩu cũ!');
                }
            } else {
                $model->addError('password_old', 'Mật khẩu cũ không đúng!');
            }
        }
        return $this->render('change', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearch()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $q = Yii::$app->request->get('q');
        $out = ['results' => ['id' => '', 'text' => '']];

        $arr = VtUserBase::find()
            ->asArray()
            ->where("full_name like :name or msisdn like :name", ['name' => "%" . $q . "%"])
            ->all();

        $cc = 0;
        $result = [];
        foreach ($arr as $item) {
            $result[$cc]['id'] = $item['id'];
            $result[$cc]['text'] = $item['id'] . '-' . $item['full_name'];
            $cc++;
        }
        $out['results'] = $result;

        return $out;
    }

}
