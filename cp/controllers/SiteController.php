<?php
namespace cp\controllers;

use cp\models\VtMappingCrawler;
use cp\models\VtMappingTopic;
use common\libs\Crawler;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public $layout = 'default';


    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'transparent' => true,
                'minLength' => 6,
                'maxLength' => 8,
            ],
        ];
    }

    public function actionIndex() {
        $this->layout = 'main';

        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        }
        $this->redirect('login');
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (\Yii::$app->user->identity->is_first_login == 1) {
                $id = \Yii::$app->user->getId();
//                Yii::$app->user->logout();
                return Yii::$app->getResponse()->redirect('/user/change?id=' . $id);
            }
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
