<?php

namespace cp\controllers;

use backend\helpers\VtExcel;
use common\helpers\Utils;use common\models\VtReportSubBase;
use cp\models\VtActionLog;
use cp\models\VtConfig;
use cp\models\VtCp;
use cp\models\VtPackage;
use common\models\VtConfigBase;
use cp\models\VtVideo;
use Elasticsearch\ClientBuilder;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Report controller
 */
class ReportController extends Controller
{

    public function actionReportCp()
    {
        $cpId = Yii::$app->user->identity->cp_id;

        if (empty($cpId)) {
            Yii::$app->session->setFlash('error', 'Quý khách không có quyền truy cập chức năng này!');
            return $this->goHome();
        }

        $fromTime = trim(Yii::$app->request->get('from_time', date('Y-m') . '-' . '01'));
        $toTime = trim(Yii::$app->request->get('to_time', date('Y-m') . '-' . date('d')));

        $packageTypes = ArrayHelper::getColumn(VtPackage::getAllSubViewPackage(), 'type');

        VtActionLog::insertLog(VtActionLog::MODULE_REPORT, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_CP);

        $fromArr = explode('-', $fromTime);
        $toArr = explode('-', $toTime);


        if (empty($fromArr[0]) || empty($fromArr[1]) || empty($fromArr[2]) || empty($toArr[0]) || empty($toArr[1]) || empty($toArr[2])
            || !checkdate($fromArr[1], $fromArr[2], $fromArr[0]) || !checkdate($toArr[1], $toArr[2], $toArr[0])) {

            Yii::$app->session->setFlash('error', 'Ngày tháng không hợp lệ');
            return $this->render('report_cp', [
                'contentsByCp' => [],
                'fromTime' => $fromTime,
                'toTime' => $toTime,
                'cpId' => $cpId
            ]);
        } else {

            $fromTime = date('Y-m-d H:i:s', strtotime(intval($fromArr[0]) . '-' . intval($fromArr[1]) . '-' . intval($fromArr[2]). ' 00:00:00'));
            $toTime = date('Y-m-d H:i:s', strtotime(intval($toArr[0]) . '-' . intval($toArr[1]) . '-' . intval($toArr[2]). ' 23:59:59'));

            $given = new \DateTime($fromTime);
            $given->setTimezone(new \DateTimeZone("UTC"));
            $utcFromTime = $given->format("Y-m-d H:i:s");

            $given = new \DateTime($toTime);
            $given->setTimezone(new \DateTimeZone("UTC"));
            $utcToTime = $given->format("Y-m-d H:i:s");

            $params = [
                'index' => 'log-sub-view',
                'type' => 'logs',
                'body' => [
                    "size" => 0,
                    "query" => [
                        "bool" => [
                            "must" => [
                                [
                                    "range" => [
                                        "datetime" => [
                                            "gte" => $utcFromTime,
                                            "lte" => $utcToTime,
                                            "format" => "yyyy-MM-dd HH:mm:ss"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "aggs" => [
                        "package" => [
                            "terms" => [
                                "field" => "package_type"
                            ],
                            "aggs" => [
                                "cp" => [
                                    "terms" => [
                                        "field" => "cp_id",
                                        "size" => 1000,
                                        "order" => ["_term" => "desc"]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $revenueParams = [
                'index' => 'log-transaction',
                'type' => 'logs',
                'body' => [
                    "size" => 0,
                    "query" => [
                        "bool" => [
                            "must" => [
                                [
                                    "bool" => [
                                        "should" => [
                                            [
                                                "match" => [
                                                    "action" => "REG"
                                                ]
                                            ],
                                            [
                                                "match" => [
                                                    "action" => "CHARGE"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "range" => [
                                        "datetime" => [
                                            "gte" => $utcFromTime,
                                            "lte" => $utcToTime,
                                            "format" => "yyyy-MM-dd HH:mm:ss"
                                        ]
                                    ]
                                ],
                                [
                                    "terms" => [
                                        "content" => $packageTypes
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "aggs" => [
                        "content" => [
                            "terms" => [
                                "field" => "content"
                            ],
                            "aggs" => [
                                "fee" => [
                                    "sum" => [
                                        "field" => "fee"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $allPackages = VtPackage::getAllPackage();
            $packagesMapName = ArrayHelper::map($allPackages, 'sub_service_name', 'name');
            $packagesMapType = ArrayHelper::map($allPackages, 'sub_service_name', 'type');
            $packagesMapCpDataPercentage = ArrayHelper::map($allPackages, 'type', 'cp_data_percentage');

            $cps = ArrayHelper::map(VtCp::getAllCp(), 'id', 'name');

            $client = ClientBuilder::create()->setHosts(Yii::$app->params['elasticsearch'])->build();

            $results = $client->search($params);
            $revenueResults = $client->search($revenueParams);


            $rawContentByPackageTypes = $revenueResults['aggregations']['content']['buckets'];
            $revenuesByPackage = [];
            $totalViewCount = 0;
            $totalFee = 0;
            foreach ($rawContentByPackageTypes as $revenueByPackageType) {

                $keyByPackageType = $revenueByPackageType['key'];

                $revenuesByPackage[$keyByPackageType]['count'] = $revenueByPackageType['doc_count'];
                $revenuesByPackage[$keyByPackageType]['fee'] = $revenueByPackageType['fee']['value'];


                $totalFee += $revenueByPackageType['fee']['value']  -  $revenueByPackageType['doc_count'] * (isset($packagesMapCpDataPercentage[$keyByPackageType])?$packagesMapCpDataPercentage[$keyByPackageType] : 0);
            }

            $contentsByCp = [];
            $rawContentBypackages = $results['aggregations']['package']['buckets'];

            foreach ($rawContentBypackages as $package) {
                $totalViewCount += $package['doc_count'];
                $packageKey = strtoupper($package['key']);

                if (ArrayHelper::keyExists($packageKey, $packagesMapName)) {
                    $packageName = $packagesMapName[$packageKey];
                } else {
                    $packageName = 'Không xác định';
                }

                $contentsByPackage = $package['cp']['buckets'];
                foreach ($contentsByPackage as $contentByPackage) {

                    $keyByCp = $contentByPackage['key'];

                    if (ArrayHelper::keyExists($keyByCp, $cps)) {
                        $cpName = $cps[$keyByCp];
                    } else {
                        $cpName = 'Không xác định';
                    }

                    $contentsByCp[$cpName][$packageName]['view_count'] = (isset($contentsByCp[$cpName][$packageName]['view_count']) ? $contentsByCp[$cpName][$packageName]['view_count'] : 0) + $contentByPackage['doc_count'];



                    //Xoa tat ca du lieu cua CP khac trong truong hop co chon cp_id
                    if (!empty($cpId) && $cpId != $keyByCp) {
                        if (ArrayHelper::keyExists($cpName, $contentsByCp)) {
                            unset($contentsByCp[$cpName]);
                        }
                    }
                }
            }

            foreach($cps as $cp){

                if(array_key_exists($cp, $contentsByCp)){

                    foreach($contentsByCp[$cp] as $content){
                        if(isset($content['view_count'])){
                            $contentsByCp[$cp]['view_count'] = (isset($contentsByCp[$cp]['view_count']) ? $contentsByCp[$cp]['view_count'] : 0) + $content['view_count'];
                        }
                    }
                    $percentage = $contentsByCp[$cp]['view_count'] / $totalViewCount;
                    $contentsByCp[$cp]['revenue'] = $percentage * $totalFee * VtConfigBase::getConfig('CP.REVENUE.PERCENTAGE', 1) / 1.1;
                }
            }

            return $this->render('report_cp', [
                'contentsByCp' => $contentsByCp,
                'fromTime' => $fromTime,
                'toTime' => $toTime,
                'cpId' => $cpId
            ]);
        }

    }


    public function actionReportCpVideo()
    {
        $cpId = Yii::$app->user->identity->cp_id;

        if (empty($cpId)) {
            Yii::$app->session->setFlash('error', 'Quý khách không có quyền truy cập chức năng này!');
            return $this->goHome();
        }

        $fromTime = trim(Yii::$app->request->get('from_time', date('Y-m') . '-' . '01'));
        $toTime = trim(Yii::$app->request->get('to_time', date('Y-m') . '-' . date('d')));

        $packageTypes = ArrayHelper::getColumn(VtPackage::getAllSubViewPackage(), 'type');

        VtActionLog::insertLog(VtActionLog::MODULE_REPORT, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_CP);

        $fromArr = explode('-', $fromTime);
        $toArr = explode('-', $toTime);

        if (empty($fromArr[0]) || empty($fromArr[1]) || empty($fromArr[2]) || empty($toArr[0]) || empty($toArr[1]) || empty($toArr[2])
            || !checkdate($fromArr[1], $fromArr[2], $fromArr[0]) || !checkdate($toArr[1], $toArr[2], $toArr[0])) {

            Yii::$app->session->setFlash('error', 'Ngày tháng không hợp lệ');
//            return $this->render('report_cp', [
//                'contentsByCp' => [],
//                'fromTime' => $fromTime,
//                'toTime' => $toTime,
//                'cpId' => $cpId
//            ]);

            return $this->render('report_cp_video', [
                'contentsByCp' => [],
                'fromTime' => $fromTime,
                'toTime' => $toTime,
                'cpId' => $cpId,
                'videos' => [],
                'totalView' => [],
                'totalRevenue' => 0,
                'videoDescriptions' => []
            ]);

        } else {

            $fromTime = date('Y-m-d H:i:s', strtotime(intval($fromArr[0]) . '-' . intval($fromArr[1]) . '-' . intval($fromArr[2]). ' 00:00:00'));
            $toTime = date('Y-m-d H:i:s', strtotime(intval($toArr[0]) . '-' . intval($toArr[1]) . '-' . intval($toArr[2]). ' 23:59:59'));


            $given = new \DateTime($fromTime);
            $given->setTimezone(new \DateTimeZone("UTC"));
            $utcFromTime = $given->format("Y-m-d H:i:s");

            $given = new \DateTime($toTime);
            $given->setTimezone(new \DateTimeZone("UTC"));
            $utcToTime = $given->format("Y-m-d H:i:s");

            $params = [
                'index' => 'log-sub-view',
                'type' => 'logs',
                'body' => [
                    "size" => 0,
                    "query" => [
                        "bool" => [
                            "must" => [
                                [
                                    "range" => [
                                        "datetime" => [
                                            "gte" => $utcFromTime,
                                            "lte" => $utcToTime,
                                            "format" => "yyyy-MM-dd HH:mm:ss"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "aggs" => [
                        "package" => [
                            "terms" => [
                                "field" => "package_type"
                            ],
                            "aggs" => [
                                "cp" => [
                                    "terms" => [
                                        "field" => "cp_id",
                                        "size" => 1000,
                                        "order" => ["_term" => "desc"]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $revenueParams = [
                'index' => 'log-transaction',
                'type' => 'logs',
                'body' => [
                    "size" => 0,
                    "query" => [
                        "bool" => [
                            "must" => [
                                [
                                    "bool" => [
                                        "should" => [
                                            [
                                                "match" => [
                                                    "action" => "REG"
                                                ]
                                            ],
                                            [
                                                "match" => [
                                                    "action" => "CHARGE"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "range" => [
                                        "datetime" => [
                                            "gte" => $utcFromTime,
                                            "lte" => $utcToTime,
                                            "format" => "yyyy-MM-dd HH:mm:ss"
                                        ]
                                    ]
                                ],
                                [
                                    "range" => [
                                        "fee" => [
                                            "gt" => 0
                                        ]
                                    ]
                                ],
                                [
                                    "terms" => [
                                        "content" => $packageTypes
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "aggs" => [
                        "content" => [
                            "terms" => [
                                "field" => "content"
                            ],
                            "aggs" => [
                                "fee" => [
                                    "sum" => [
                                        "field" => "fee"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $videoParams = [
                'index' => 'log-sub-view',
                'type' => 'logs',
                'body' => [
                    "size" => 0,
                    "query" => [
                        "bool" => [
                            "must" => [
                                [
                                    "range" => [
                                        "datetime" => [
                                            "gte" => $utcFromTime,
                                            "lte" => $utcToTime,
                                            "format" => "yyyy-MM-dd HH:mm:ss"
                                        ]
                                    ]
                                ],
                                [
                                    "term" => [
                                        "cp_id" => $cpId
                                    ]
                                ]

                            ]
                        ]
                    ],
                    "aggs" => [
                        "video" => [
                            "terms" => [
                                "field" => "video_id",
                                "size" => 5000,
                                "order" => ["_count" => "desc"]
                            ]
                        ]
                    ]
                ]
            ];



            $allPackages = VtPackage::getAllPackage();
            $packagesMapName = ArrayHelper::map($allPackages, 'sub_service_name', 'name');
            $packagesMapType = ArrayHelper::map($allPackages, 'sub_service_name', 'type');
            $packagesMapCpDataPercentage = ArrayHelper::map($allPackages, 'type', 'cp_data_percentage');

            $cps = ArrayHelper::map(VtCp::getAllCp(), 'id', 'name');

            $client = ClientBuilder::create()->setHosts(Yii::$app->params['elasticsearch'])->build();

            $results = $client->search($params);
            $revenueResults = $client->search($revenueParams);
            $videoResults = $client->search($videoParams);


            $rawContentByPackageTypes = $revenueResults['aggregations']['content']['buckets'];
            $revenuesByPackage = [];
            $totalViewCount = 0;
            $totalFee = 0;
            foreach ($rawContentByPackageTypes as $revenueByPackageType) {

                $keyByPackageType = $revenueByPackageType['key'];

                $revenuesByPackage[$keyByPackageType]['count'] = $revenueByPackageType['doc_count'];
                $revenuesByPackage[$keyByPackageType]['fee'] = $revenueByPackageType['fee']['value'];


                $totalFee += $revenueByPackageType['fee']['value']  - $revenueByPackageType['doc_count'] * (isset($packagesMapCpDataPercentage[$keyByPackageType]) ? $packagesMapCpDataPercentage[$keyByPackageType] : 0);
            }

            $contentsByCp = [];
            $rawContentBypackages = $results['aggregations']['package']['buckets'];

            foreach ($rawContentBypackages as $package) {
                $totalViewCount += $package['doc_count'];
                $packageKey = strtoupper($package['key']);

                if (ArrayHelper::keyExists($packageKey, $packagesMapName)) {
                    $packageName = $packagesMapName[$packageKey];
                } else {
                    $packageName = 'Không xác định';
                }

                $contentsByPackage = $package['cp']['buckets'];
                foreach ($contentsByPackage as $contentByPackage) {

                    $keyByCp = $contentByPackage['key'];

                    if (ArrayHelper::keyExists($keyByCp, $cps)) {
                        $cpName = $cps[$keyByCp];
                    } else {
                        $cpName = 'Không xác định';
                    }

                    $contentsByCp[$cpName][$packageName]['view_count'] = (isset($contentsByCp[$cpName][$packageName]['view_count']) ? $contentsByCp[$cpName][$packageName]['view_count'] : 0) + $contentByPackage['doc_count'];



                    //Xoa tat ca du lieu cua CP khac trong truong hop co chon cp_id
                    if (!empty($cpId) && $cpId != $keyByCp) {
                        if (ArrayHelper::keyExists($cpName, $contentsByCp)) {
                            unset($contentsByCp[$cpName]);
                        }
                    }
                }
            }

            foreach($cps as $cp){

                if(array_key_exists($cp, $contentsByCp)){

                    foreach($contentsByCp[$cp] as $content){
                        if(isset($content['view_count'])){
                            $contentsByCp[$cp]['view_count'] = (isset($contentsByCp[$cp]['view_count']) ? $contentsByCp[$cp]['view_count'] : 0) + $content['view_count'];
                        }
                    }
                    $percentage = $contentsByCp[$cp]['view_count'] / $totalViewCount;
                    $contentsByCp[$cp]['revenue'] = $percentage * $totalFee * VtConfigBase::getConfig('CP.REVENUE.PERCENTAGE', 1) / 1.1;
                }
            }

            $videos = [];
            $videoIds = [];
            $totalView = 0;
            $rawContentByVideos = $videoResults['aggregations']['video']['buckets'];
            foreach ($rawContentByVideos as $video) {
                $videos[$video['key']] = $video['doc_count'];
                $videoIds[] = $video['key'];
                $totalView += $video['doc_count'];
            }

            $allVideos = VtVideo::getAllVideosByIds($videoIds);

            $videoDescriptions = [];
            foreach ($allVideos as $video) {
                $videoDescriptions[$video['id']]['name'] = $video['name'];
                $videoDescriptions[$video['id']]['type'] = $video['type'];
                $videoDescriptions[$video['id']]['channel'] = $video['full_name'];
            }

            $cpCode = isset($cps[$cpId]) ? $cps[$cpId] : '';

            return $this->render('report_cp_video', [
                'contentsByCp' => $contentsByCp,
                'fromTime' => $fromTime,
                'toTime' => $toTime,
                'cpId' => $cpId,
                'videos' => $videos,
                'totalView' => $totalView,
                'totalRevenue' => isset($contentsByCp[$cpCode]['revenue']) ? $contentsByCp[$cpCode]['revenue'] : 0,
                'videoDescriptions' => $videoDescriptions
            ]);
        }

    }


    public function actionDistribution()
    {
        $cpId = Yii::$app->user->identity->cp_id;

        $packageTypes = ArrayHelper::getColumn(VtPackage::getDistributionPackageByCpId($cpId), 'type');

        $fromTime = trim(Yii::$app->request->get('from_time', date('Y-m') . '-' . '01')). ' 00:00:00';
        $toTime = trim(Yii::$app->request->get('to_time', date('Y-m') . '-' . date('d'))). ' 23:59:59';

        VtActionLog::insertLog(VtActionLog::MODULE_REPORT, null, VtActionLog::TYPE_VIEW, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_CP);

        $reportSubs = VtReportSubBase::getArrByDate($packageTypes, $fromTime, $toTime);

        $allPackages = ArrayHelper::map(VtPackage::getAllDistributionPackage(), 'type', 'name');


        $given = new \DateTime($fromTime);
        $given->setTimezone(new \DateTimeZone("UTC"));
        $utcFromTime = $given->format("Y-m-d H:i:s");

        $given = new \DateTime($toTime);
        $given->setTimezone(new \DateTimeZone("UTC"));
        $utcToTime = $given->format("Y-m-d H:i:s");

        $params = [
            'index' => 'log-transaction*',
            'type' => 'logs',
            'body' => [
                "size" => 0,
                "query" => [
                    "bool" => [
                        "must" => [
                            [
                                "range"  => [
                                    "datetime"  => [
                                        "gte" => $utcFromTime,
                                        "lte" => $utcToTime,
                                        "format" => "yyyy-MM-dd HH:mm:ss"
                                    ]
                                ]
                            ],
                            [
                                "terms" => [
                                    "content" => $packageTypes
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "range" => [
                        "date_histogram"  => [
                            "field"  => "datetime",
                            "format"  => "yyyy-MM-dd",
                            "interval"  => "day",
                            "time_zone" => "+07:00",
                            "order" => [
                                "_key" => "desc"
                            ]
                        ],

                        "aggs" => [
                            "content" => [
                                "terms" => [
                                    "field" => "content"
                                ],
                                "aggs" => [
                                    "reg"  => [
                                        "filter"  => [
                                            "bool" => [
                                                "must" => [
                                                    [
                                                        "term"  => [
                                                            "action"  =>  "reg"
                                                        ]
                                                    ],
                                                    [
                                                        "term" => [
                                                            "error_code" => "0"
                                                        ]
                                                    ],
                                                    [
                                                        "range" => [
                                                            "fee" => [
                                                                "gt" => 0
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        "aggs"  => [
                                            "fee"  => [
                                                "sum"  => [
                                                    "field"  => "fee"
                                                ]
                                            ],
                                            "distinct_msisdn_reg"  => [
                                                "cardinality"  => ["field"  => "msisdn"]
                                            ]
                                        ]
                                    ],
                                    "charge_all"  => [
                                        "filter"  => [
                                            "bool" => [
                                                "must" => [
                                                    [
                                                        "term"  => [
                                                            "action"  =>  "charge"
                                                        ]
                                                    ],
//                                                    [
//                                                        "term" => [
//                                                            "error_code" => "0"
//                                                        ]
//                                                    ],
                                                    [
                                                        "range" => [
                                                            "fee" => [
                                                                "gt" => 0
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        "aggs"  => [
                                            "distinct_msisdn_charge"  => [
                                                "cardinality"  => ["field"  => "msisdn"]
                                            ]
                                        ]
                                    ],
                                    "charge_success"  => [
                                        "filter"  => [
                                            "bool" => [
                                                "must" => [
                                                    [
                                                        "term"  => [
                                                            "action"  =>  "charge"
                                                        ]
                                                    ],
                                                    [
                                                        "term" => [
                                                            "error_code" => "0"
                                                        ]
                                                    ],
                                                    [
                                                        "range" => [
                                                            "fee" => [
                                                                "gt" => 0
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        "aggs"  => [
                                            "fee"  => [
                                                "sum"  => [
                                                    "field"  => "fee"
                                                ]
                                            ],

                                            "distinct_msisdn_charge"  => [
                                                "cardinality"  => ["field"  => "msisdn"]
                                            ]

                                        ]
                                    ],
                                    "unreg"  => [
                                        "filter"  => [
                                            "bool" => [
                                                "must" => [
                                                    [
                                                        "term"  => [
                                                            "action"  =>  "unreg"
                                                        ]
                                                    ],
                                                    [
                                                        "term" => [
                                                            "error_code" => "0"
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        "aggs"  => [
                                            "distinct_msisdn_unreg"  => [
                                                "cardinality"  => ["field"  => "msisdn"]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $client = ClientBuilder::create()->setHosts(Yii::$app->params['elasticsearch'])->build();
        $results = $client->search($params);

        $rawContentsByDate = $results['aggregations']['range']['buckets'];

        $contentsByDate = [];
        foreach ($rawContentsByDate as $rawContentByDate) {
            $rawContentsByAction = $rawContentByDate['content']['buckets'];

            $keyByDate = $rawContentByDate['key_as_string'];

            foreach ($rawContentsByAction as $rawContentByAction) {
                $keyByPackage = $rawContentByAction['key'];

                $contentsByDate[$keyByDate][$keyByPackage]['reg'] = $rawContentByAction['reg']['distinct_msisdn_reg']['value'];
                $contentsByDate[$keyByDate][$keyByPackage]['unreg'] = $rawContentByAction['unreg']['distinct_msisdn_unreg']['value'];
                $contentsByDate[$keyByDate][$keyByPackage]['charge_all'] = $rawContentByAction['charge_all']['distinct_msisdn_charge']['value'];
                $contentsByDate[$keyByDate][$keyByPackage]['charge_success'] = $rawContentByAction['charge_success']['distinct_msisdn_charge']['value'];
                $contentsByDate[$keyByDate][$keyByPackage]['charge_revenue'] = ($rawContentByAction['charge_success']['fee']['value'] - $rawContentByAction['charge_success']['doc_count'] * 1000) * VtConfigBase::getConfig('DISTRIBUTION.REVENUE.PERCENTAGE', 0.28) / 1.1;
                $contentsByDate[$keyByDate][$keyByPackage]['reg_revenue'] = ($rawContentByAction['reg']['fee']['value'] - $rawContentByAction['reg']['doc_count'] * 1000) * VtConfigBase::getConfig('DISTRIBUTION.REVENUE.PERCENTAGE', 0.28) / 1.1;
                $contentsByDate[$keyByDate][$keyByPackage]['active_sub'] = isset($reportSubs[$keyByDate][$keyByPackage]) ? $reportSubs[$keyByDate][$keyByPackage] : 0;
            }
        }

        return $this->render('distribution', [
            'contents' => $contentsByDate,
            'fromTime' => $fromTime,
            'toTime' => $toTime,
            'allPackages' => $allPackages
        ]);

    }


    public function actionReportOutsourceReview()
    {
        set_time_limit(600);
        ini_set('memory_limit', '512M');

        $fromTime = trim(Yii::$app->request->get('from_time', date('Y-m') . '-' . '01')) . ' 00:00:00';
        $toTime = trim(Yii::$app->request->get('to_time', date('Y-m') . '-' . date('d'))) . ' 23:59:59';
        $status = trim(Yii::$app->request->get('status', null));

        $query = VtVideo::getOutsourceReviewQuery($fromTime, $toTime, $status);

        if(!empty(Yii::$app->request->get('export'))) {
            $header = [
                'STT',
                'Ngày duyệt',
                'BTV',
                'ID',
                'Tên clip biên tập',
                'APPROVE',
                'REJECT',
                'Khách hàng',
                'Thời lượng (giây)',
                'Ghi Chú',
                'Doanh thu (VNĐ)'
            ];

            $results = $query->asArray()->all();

            $pageResults = [];
            foreach($results as $key => $model){

                $arr = [];
                $arr['key'] = $key + 1;
                $arr['outsource_review_at'] = date('d-m-Y H:i:s', strtotime($model['outsource_review_at']));
                $arr['username'] = $model['username'];
                $arr['id'] = $model['id'];
                $arr['name'] = $model['name'];
                $arr['approve_status'] = ($model['outsource_status'] == VtVideo::OUTSOURCE_APPROVE) ? 'X' : '';
                $arr['disapprove_status'] = ($model['outsource_status'] == VtVideo::OUTSOURCE_DISAPPROVE) ? 'X' : '';
                $arr['full_name'] = $model['full_name'];

                if($model['outsource_status']  == VtVideo::OUTSOURCE_APPROVE){
                    $arr['duration'] = $model['duration'];
                }elseif($model['outsource_status']  == VtVideo::OUTSOURCE_DISAPPROVE){
                    $arr['duration'] = $model['related_id'];
                }

                $arr['reason'] = $model['reason'];

                if($model['outsource_status']  == VtVideo::OUTSOURCE_APPROVE){
                    $arr['revenue'] = round((VtConfig::getConfig('OUTSOURCE.REVENUE.META_DATA') + (VtConfig::getConfig('OUTSOURCE.REVENUE.PER_MINUTE') * $model['duration'] / 60)));
                }elseif($model['outsource_status']  == VtVideo::OUTSOURCE_DISAPPROVE){
                    $arr['revenue'] = round((VtConfig::getConfig('OUTSOURCE.REVENUE.PER_MINUTE') * $model['related_id'] / 60));
                }

                $pageResults[] = $arr;
            }

            VtExcel::exportRow($header, $pageResults, 'Thống kê duyệt');
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
//            'defaultPageSize' => 5000,
//            'pageSizeLimit' => [1, 5000]
        ]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();

        $total = VtVideo::getOutsourceTotal($fromTime, $toTime, $status);

        return $this->render('outsource_review', [
            'models' => $models,
            'total' => $total,
            'pages' => $pages,
            'fromTime' => $fromTime,
            'toTime' => $toTime,
            'status' => $status
        ]);

    }
}
