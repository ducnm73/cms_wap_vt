<?php

namespace cp\controllers;

use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\MongoDBModel;
use common\models\VtUserBase;
use cp\models\VtActionLog;
use cp\models\VtUser;
use cp\models\VtVideo;
use cp\models\VtVideoSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;


/**
 * VtVideoController implements the CRUD actions for VtVideo model.
 */
class VtVideoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VtVideo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VtVideoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        //@todo: check permission
        $homeUrl = Yii::$app->request->get('home-url', '');
        $model = $this->findModel($id);

        if($model->outsource_need_review != VtVideo::OUTSOURCE_NEED_APPROVE){
            Yii::$app->session->setFlash('error', 'Bạn không có quyền thực hiện chức năng này!');
            return $this->redirect($homeUrl);
        }

        if (!empty(Yii::$app->request->post('submitReviewing'))) {
            $model->outsource_status = VtVideo::OUTSOURCE_REVIEWING;
            $model->outsource_review_by = Yii::$app->user->identity->getId();
            $model->save();
            Yii::$app->session->setFlash('success', 'Chuyển sang trạng thái đang duyệt thành công!');
        }else{

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $topicIds = Yii::$app->request->post('VtVideo')['topics'];

                if (!empty(Yii::$app->request->post('submitApprove'))) {

                    if (!$model->isOutsourceApprove() || $model->isOutsourceDraft()){
                        $model->status = VtVideo::STATUS_APPROVE;
                        $model->outsource_status = VtVideo::OUTSOURCE_APPROVE;
                        $model->outsource_review_by = Yii::$app->user->identity->getId();
                        $model->approve_by = Yii::$app->user->identity->getId();
                        $model->updated_at = date("Y-m-d H:i:s");
                        $model->outsource_review_at = date("Y-m-d H:i:s");
                        //thuynd10 added 20180905_start
                        $model->admin_review_first_times_at = date("Y-m-d H:i:s");
                        //thuynd10 added 20180905_end
                        Yii::$app->session->setFlash('success', 'Phê duyệt thành công!');
                        //- Add event Mongodb
                        $userCreate = VtUserBase::getById($model->created_by);
                        MongoDBModel::insertEvent(MongoDBModel::UPLOAD, $model->id, $model->created_by, ($userCreate)?$userCreate->msisdn:"","",$model->created_by);
                    }

                }else{
                    if ($model->isOutsourceApprove() || $model->isOutsourceDraft()) {
                        $model->is_active = VtVideo::IN_ACTIVE;
                        $model->status = VtVideo::STATUS_DELETE;
                        $model->outsource_status = VtVideo::OUTSOURCE_DISAPPROVE;
                        $model->outsource_review_by = Yii::$app->user->identity->getId();
                        $model->updated_at = date("Y-m-d H:i:s");
                        $model->outsource_review_at = date("Y-m-d H:i:s");
                        //thuynd10 added 20180905_start
                        $model->admin_review_first_times_at = date("Y-m-d H:i:s");
                        //thuynd10 added 20180905_end
                        Yii::$app->session->setFlash('success', 'Từ chối duyệt thành công!');
                    }
                }

                $snapshot = false;
                if(!empty($model->snapshot)){
                    $model->bucket = $model->file_bucket;
                    $model->path = $model->snapshot;

                    $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($model->path);
                    $result = S3Service::getObject($model->bucket, $model->path, $tempPath);

                    VtHelper::generateAllThumb($model->bucket, $tempPath, $model->path);
                    unlink($tempPath);
                    $snapshot = true;
                }

                //Tinh toan so luong Video duoc active cua User
                VtUser::updateVideoCountByUserId($model->created_by);
                $model->saveWithImage($topicIds, $snapshot);

                VtActionLog::insertLog(VtActionLog::MODULE_VIDEO, $model, VtActionLog::TYPE_UPDATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_CP);

            }
        }

        if($model->outsource_status == VtVideo::OUTSOURCE_REVIEWING){
            Yii::$app->session->setFlash('error', 'Video đang được duyệt bởi tài khoản <strong>' . $model->getOutsourceReviewByName() . '</strong>. Vui lòng kiểm tra kĩ trước khi thao tác');
        }



        return $this->render('update', [
            'model' => $model,
            'homeUrl' => $homeUrl
        ]);

    }

    protected function findModel($id)
    {
        if (($model = VtVideo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


//    public function actionApprove()
//    {
//        $id = Yii::$app->request->post('id');
//        $action = Yii::$app->request->post('action');
//
//        $model = $this->findModel($id);
//
//        $status = null;
//        switch ($action) {
//            case 'approve':
//                $status = VtVideo::OUTSOURCE_APPROVE;
//                break;
//            case 'disapprove':
//                $status = VtVideo::OUTSOURCE_DISAPPROVE;
//                break;
//        }
//
//        //Cap nhat thong tin video
////        VtVideo::updateContentById($id, null, null, null, $status, $convertStatus, null, $isActive);
//
//        $model->outsource_status = $status;
//        $model->outsource_review_by = Yii::$app->user->identity->getId();
//        $model->save(false);
//
//        VtActionLog::insertLog(VtActionLog::MODULE_VIDEO, $model, VtActionLog::TYPE_UPDATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);
//
//        return json_encode(array(
//            "success" => true,
//            "message" => 'Thành công'
//        ));
//
//    }


    public function actionBatchUpdateOutsource()
    {
        $ids = Yii::$app->request->post('ids');

        $models = VtVideo::findAll(['id' => $ids]);

        foreach($models as $model){
            if($model->isOutsourceDraft()){
                $model->outsource_status = VtVideo::OUTSOURCE_REVIEWING;
                $model->outsource_review_by = Yii::$app->user->identity->getId();
                $model->save();
            }
            VtActionLog::insertLog(VtActionLog::MODULE_VIDEO, $model, VtActionLog::TYPE_UPDATE, Yii::$app->user->identity->getId(), VtActionLog::SOURCE_BACKEND);
        }

        Yii::$app->session->setFlash('success', 'Chuyển sang trạng thái đang duyệt thành công các bản ghi đã chọn !');

        return $this->redirect(Yii::$app->request->referrer ?: Url::toRoute(['vt-video/index']));
    }
}
