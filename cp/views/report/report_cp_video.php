<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use backend\models\VtCp;
use kartik\date\DatePicker;
?>

<div>
    <h4 style="text-align: center;"><b>Báo cáo chi tiết Video</b></h4>
    <div class="row">
        <form method="get" action="/report/report-cp-video">

            <div class="col-md-3">

                <?=
                DatePicker::widget([
                    'name' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => 'Thời gian bắt đầu'],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => 'Thời gian kết thúc'],
                ]);
                ?>

            </div>


            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>

        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
                <tr>
                    <th width="5%"><strong>#</strong></th>
                    <th width="45%"><strong><?= 'Nội dung' ?></strong></th>
                    <th width="10%"><strong><?= 'Loại' ?></strong></th>
                    <th width="10%"><strong><?= 'Kênh' ?></strong></th>
                    <th width="15%"><strong><?= 'Doanh thu (VNĐ)' ?></strong></th>
                </tr>
            </thead>
            <?php if (count($videos) > 0): ?>
                <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($videos as $key => $value):?>
                        <?php $i++ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= (ArrayHelper::keyExists($key, $videoDescriptions)) ? $videoDescriptions[$key]['name']:$key  ?></td>
                            <td><?= (ArrayHelper::keyExists($key, $videoDescriptions)) ? $videoDescriptions[$key]['type']:'' ?></td>
                            <td><?= (ArrayHelper::keyExists($key, $videoDescriptions)) ? $videoDescriptions[$key]['channel']:'' ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($totalRevenue*$value/$totalView) ?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="5" style="text-align: center;">Không có dữ liệu</td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
