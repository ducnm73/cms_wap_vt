<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use backend\models\VtPackage;
use kartik\date\DatePicker;
?>

<div>
    <h4 style="text-align: center;"><b>Báo cáo chia sẻ doanh thu</b></h4>
    <div class="row">

        <form method="get" action="/report/distribution">

            <div class="col-md-3">

                <?=
                //                yii\jui\DatePicker::widget([
                //                    'name' => 'from_time',
                //                    'dateFormat' => 'php:Y-m-d',
                //                    'language' => 'en',
                //                    'value' => \yii\helpers\Html::encode($fromTime)
                //                ])
                DatePicker::widget([
                    'name' => 'from_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($fromTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => 'Thời gian bắt đầu'],
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                DatePicker::widget([
                    'name' => 'to_time',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'value' => date('Y-m-d', strtotime($toTime)),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => ['placeholder' => 'Thời gian kết thúc'],
                ]);
                ?>

            </div>

            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>
        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
            <tr>
                <th width="5%" class="cell-key"><strong>#</strong></th>
                <th width="10%" class="cell-key"><strong>Ngày</strong></th>
                <th width="15%" class="cell-key"><strong>Gói cước</strong></th>
                <th width="10%" class="cell-key"><strong>Đăng ký</strong></th>
                <th width="10%" class="cell-key"><strong>Hủy</strong></th>
                <th width="10%" class="cell-key"><strong>Lũy kế</strong></th>
                <th width="10%" class="cell-key"><strong>Trừ cước</strong></th>
                <th width="10%" class="cell-key"><strong>Trừ cước thành công</strong></th>
                <th width="10%" class="cell-key"><strong>Doanh thu gia hạn</strong></th>
                <th width="10%" class="cell-key"><strong>Doanh thu đăng ký</strong></th>
            </tr>
            </thead>
            <?php if (count($contents) > 0): ?>
                <tbody>
                <?php $i = 0; ?>
                <?php foreach($contents as $date => $packages):?>
                    <?php $j = 0 ?>
                    <?php foreach($packages as $key => $package):?>
                        <?php $i++ ?>

                        <tr>
                            <td class="cell-key"><?= $i ?></td>
                            <?php if($j == 0): ?>
                                <td class="cell-key" rowspan="<?= count($packages) ?>"><?= $date ?></td>
                            <?php endif;?>
                            <td><?= key_exists($key, $allPackages) ? $allPackages[$key] : $key ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['reg']) ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['unreg']) ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['active_sub']) ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['charge_all']) ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['charge_success']) ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['charge_revenue']) ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($package['reg_revenue']) ?></td>
                        </tr>

                        <?php $j++ ?>
                    <?php endforeach;?>

                <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                    <tr>
                        <td colspan="10" style="text-align: center;">Không có dữ liệu</td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
