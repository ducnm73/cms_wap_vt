<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use cp\models\VtCp;
use kartik\date\DatePicker;
?>

<div>
    <h4 style="text-align: center;"><b>Báo cáo chia sẻ doanh thu</b></h4>
    <div class="row">

        <form method="get" action="/report/report-cp">

            <div class="col-md-3">

                <?=
//                yii\jui\DatePicker::widget([
//                    'name' => 'from_time',
//                    'dateFormat' => 'php:Y-m-d',
//                    'language' => 'en',
//                    'value' => \yii\helpers\Html::encode($fromTime)
//                ])
                    DatePicker::widget([
                        'name' => 'from_time',
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => date('Y-m-d', strtotime($fromTime)),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => ['placeholder' => 'Thời gian bắt đầu'],
                    ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                    DatePicker::widget([
                        'name' => 'to_time',
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => date('Y-m-d', strtotime($toTime)),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => ['placeholder' => 'Thời gian kết thúc'],
                    ]);
                ?>

            </div>

            
            <div class="col-md-3">
                <input type="submit" name="action" value="REPORT" class="btn btn-primary"/>
            </div>

        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
            <tr>
                <th><strong>#</strong></th>
                <th><strong><?= 'Đối tác' ?></strong></th>
                <th><strong><?= 'Doanh thu (VNĐ)' ?></strong></th>
            </tr>
            </thead>
            <?php if (count($contentsByCp) > 0): ?>
                <tbody>
                <?php $i = 0; ?>

                <?php foreach($contentsByCp as $cpId => $content):?>
                    <?php if(array_key_exists('view_count', $content) && array_key_exists('revenue', $content)):?>

                        <?php $i++ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $cpId ?></td>
                            <td class="cell-content"><?= Yii::$app->formatter->asInteger($content['revenue']) ?></td>
                        </tr>

                    <?php endif; ?>

                <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tbody>
                <tr>
                    <td colspan="5" style="text-align: center;">Không có dữ liệu</td>
                </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

</div>
