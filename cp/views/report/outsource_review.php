<?php
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use cp\models\VtCp;
use kartik\date\DatePicker;
use yii\widgets\LinkPager;
use cp\models\VtVideo;
use common\helpers\Utils;
use cp\models\VtConfig;
use yii\helpers\Html;

?>

<div>
    <h4 style="text-align: center;"><b>Báo cáo Kiểm duyệt nội dung đối tác TTXVN</b></h4>
    <div class="row">

        <form method="get" action="/report/report-outsource-review">

            <div class="col-md-3">

                <?=
//                yii\jui\DatePicker::widget([
//                    'name' => 'from_time',
//                    'dateFormat' => 'php:Y-m-d',
//                    'language' => 'en',
//                    'value' => \yii\helpers\Html::encode($fromTime)
//                ])
                    DatePicker::widget([
                        'name' => 'from_time',
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => date('Y-m-d', strtotime($fromTime)),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => ['placeholder' => 'Thời gian bắt đầu'],
                    ]);
                ?>
            </div>
            <div class="col-md-3">
                <?=
                    DatePicker::widget([
                        'name' => 'to_time',
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => date('Y-m-d', strtotime($toTime)),
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => ['placeholder' => 'Thời gian kết thúc'],
                    ]);
                ?>

            </div>



            <div class="col-md-3">
                <?= Html::submitButton('Report', [
                    'name' => 'search',
                    'value' => 'search',
                    'class' => 'btn btn-primary'
                ]) ?>
                <?= Html::submitButton('Xuất Excel', [
                    'name' => 'export',
                    'value' => 'export',
                    'class' => 'btn btn-success'
                ]) ?>
            </div>

        </form>
    </div>

    <div style="width:100%">
        <table style="table-layout: auto;" class="table table-striped table-bordered table-hover dataTable no-footer"
               role="grid">

            <thead>
                <tr>
                    <th width="5%" class="cell-key"><strong>#</strong></th>
                    <th width="10%" class="cell-key"><strong><?= 'Ngày duyệt' ?></strong></th>
                    <th width="10%" class="cell-key"><strong><?= 'BTV' ?></strong></th>
                    <th width="30%" class="cell-key"><strong><?= 'Tên clip biên tập' ?></strong></th>
                    <th width="5%" class="cell-key"><strong><?= 'APPROVE' ?></strong></th>
                    <th width="5%" class="cell-key"><strong><?= 'REJECT' ?></strong></th>
                    <th width="10%" class="cell-key"><strong><?= 'Khách hàng' ?></strong></th>
                    <th width="5%" class="cell-key"><strong><?= 'Thời lượng' ?></strong></th>
                    <th width="15%" class="cell-key"><strong><?= 'Ghi Chú' ?></strong></th>
                    <th width="10%"><strong><?= 'Doanh thu (VNĐ)' ?></strong></th>
                </tr>
            </thead>
            <?php if (count($models) > 0): ?>
                <tbody>
                <?php $i = 0; ?>

                <?php foreach($models as $model):?>

                    <?php $i++ ?>
                    <tr>
                        <td class="cell-key"><?= $i ?></td>
                        <td class="cell-text"><?= date('d-m-Y H:i:s', strtotime($model['outsource_review_at'])) ?></td>
                        <td class="cell-key"><?= $model['username'] ?></td>
                        <td class="cell-text"><?= $model['name'] ?></td>
                        <td class="cell-key"><?= ($model['outsource_status'] == VtVideo::OUTSOURCE_APPROVE) ? 'X' : '' ?></td>
                        <td class="cell-key"><?= ($model['outsource_status'] == VtVideo::OUTSOURCE_DISAPPROVE) ? 'X' : '' ?></td>
                        <td class="cell-key"><?= $model['full_name'] ?></td>
                        <td class="cell-key">
                            <?php if($model['outsource_status']  == VtVideo::OUTSOURCE_APPROVE):?>
                                <?= Utils::durationToStr($model['duration']) ?>
                            <?php elseif($model['outsource_status']  == VtVideo::OUTSOURCE_DISAPPROVE):?>
                                <?= Utils::durationToStr($model['related_id']) ?>
                            <?php endif;?>

                        </td>


                        <td class="cell-text"><?= $model['reason'] ?></td>
                        <td class="cell-content">
                            <?php if($model['outsource_status']  == VtVideo::OUTSOURCE_APPROVE):?>
                                <?= Yii::$app->formatter->asInteger(round(VtConfig::getConfig('OUTSOURCE.REVENUE.META_DATA') + (VtConfig::getConfig('OUTSOURCE.REVENUE.PER_MINUTE') * $model['duration'] / 60))) ?>
                            <?php elseif($model['outsource_status']  == VtVideo::OUTSOURCE_DISAPPROVE):?>
                                <?= Yii::$app->formatter->asInteger(round((VtConfig::getConfig('OUTSOURCE.REVENUE.PER_MINUTE') * $model['related_id'] / 60))) ?>
                            <?php endif;?>

                        </td>
                    </tr>

                <?php endforeach;?>
                </tbody>

                <tfoot>
                <tr>
                    <th width="5%" colspan="4" class="cell-key"><strong>Tổng</strong></th>
                    <th width="5%" class="cell-key"><strong><?= $total['approve_count'] ?></strong></th>
                    <th width="5%" class="cell-key"><strong><?= $total['disapprove_count'] ?></strong></th>
                    <th width="10%" class="cell-key"><strong></strong></th>
                    <th width="5%" class="cell-key"><strong><?= Utils::durationToStr($total['approve_duration'] + $total['disapprove_duration']) ?></strong></th>
                    <th width="15%" class="cell-key"><strong></strong></th>
                    <th width="10%">
                        <strong>
                                <?= Yii::$app->formatter->asInteger(VtConfig::getConfig('OUTSOURCE.REVENUE.META_DATA') * $total['approve_count']
                                    + round(VtConfig::getConfig('OUTSOURCE.REVENUE.PER_MINUTE') * ($total['approve_duration'] + $total['disapprove_duration']) / 60)
                                )
                                ?>
                        </strong>
                    </th>
                </tr>
                </tfoot>

            <?php else:?>
                <tbody>
                <tr>
                    <td colspan="10" style="text-align: center;">Không có dữ liệu</td>
                </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>

    <?= LinkPager::widget(['pagination' => $pages]);?>



</div>
