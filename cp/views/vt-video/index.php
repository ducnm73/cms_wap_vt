<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\libs\VtHelper;

use common\helpers\Utils;
use mickgeek\actionbar\Widget as ActionBar;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\VtVideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;

?>

<input id="approve-action" type="hidden" value="<?php echo Url::toRoute(['vt-video/approve']) ?>">

<div class="vt-video-index">
    <?php  echo $this->render('_search', ['searchModel' => $searchModel]); ?>

    <div class="row">

        <div class="col-md-2"  style="float: right;">

            <?= ActionBar::widget([
                'grid' => 'video-grid',
                'templates' => [
                    '{bulk-actions}' => ['class' => 'col-xs-4', 'label' => 'Thao tác'],
                ],
                'bulkActionsItems' => [
                    'update-outsource' => 'Chuyển đang duyệt'
                ],
                'bulkActionsOptions' => [
                    'options' => [
                        'update-outsource' => [
                            'url' => Url::toRoute(['batch-update-outsource']),
                            'disabled' => !Yii::$app->user->can('approve-video'),
                            'data-confirm' => 'Bạn có chắc chắn muốn chuyển sang trạng thái đang duyệt ?',
                        ],
                    ],
                    'class' => 'form-control',
                    'style' => 'width: 150px'
                ],
            ]) ?>
        </div>

    </div>


    <?php Pjax::begin(['id' => 'vt-video-list']); ?>

        <?= GridView::widget([
            'id' => 'video-grid',
            'dataProvider' => $dataProvider,
    //        'filterModel' => $searchModel,
            'columns' => [
    //            ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'headerOptions' => ['style' => 'width:5%'],
                ],

                [
                    'attribute' => 'id',
                    'label' => '#',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'width:5%'],

                    'value' => function ($model, $key, $index) use ($dataProvider) {
                        $pagination = $dataProvider->getPagination();
                        if ($pagination !== false) {
                            return $pagination->getOffset() + $index + 1;
                        } else {
                            return $index + 1;
                        }
                    }
                ],
                [
                    'attribute' => 'name',
                    'label' => 'Tên',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'width:30%'],
                    'value' => function ($model) {
                        return '<b>Tiêu đề: </b><a href="' . Url::toRoute(['vt-video/update', 'id' => $model->id, 'home-url' => base64_encode(Yii::$app->request->url)]) . '">'
                            . Html::encode($model->name)
                            . '</a>'
                            . '<p><b>Mô tả: </b>' . (!empty($model->description) ? $model->description : 'N/A') .'</p>'
                            . '<p><strong>Người tạo: </strong>' . (($model->user) ? (($model->user->full_name) ? $model->user->full_name : $model->user->msisdn) : '') . '</p>';
                    },
                ],

                [
                    'attribute' => 'Thông tin',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'width:20%'],
                    'value' => function ($model) {
                        return
                            '<p><strong>Thể loại: </strong>' .(($model->category)?htmlentities($model->category->name):'N/A'). '</p>'
                            . '<p><strong>Resolution: </strong>' . $model->resolution . '</p>'
                            . '<p><strong>Thời gian tạo: </strong>' . $model->created_at . '</p>'
                            . '<p><strong>Thời gian duyệt: </strong>' . ((!empty($model->outsource_review_at)) ? $model->outsource_review_at : 'N/A') . '</p>'
                            . '<p><strong>Thời lượng: </strong>' . Utils::durationToStr($model->duration) . '</p>';
                    }
                ],
                [
                    'attribute' => 'Image',
                    'label' => 'Ảnh',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'width:15%'],

                    'value' => function ($model) {
                        return Html::img(VtHelper::getThumbUrl($model->bucket, $model->path, VtHelper::SIZE_COVER), ['class' => 'image-grid-display']);
                    }
                ],

                [
                    'attribute' => 'outsource_status',
                    'label' => 'Trạng thái',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'width:15%'],

                    'value' => function ($model) {

                        $username = !empty($model->outsource) ? $model->outsource->username : '';

                        $action = isset($model->outsource_status) ? Yii::$app->params['outsource.status.dropdown.value'][$model->outsource_status] : '';

                        return !empty($username)? ($username . ' - ' . $action): $action;
                    }
                ],

//                [
//                    'attribute' => 'action',
//                    'label' => 'Thao tác',
//                    'headerOptions' => ['style' => 'width:15%'],
//                    'format' => 'raw',
//                    'value' => function ($model) {
//
//                        $approveBtn = '<button style="width: 90px" onclick="approveVideo(' . $model->id . ', \'approve\')" type="button" class="btn btn-success video-btn-publish">Xuất bản</button>';
//                        $disapproveBtn = '<button style="width: 90px" onclick="approveVideo(' . $model->id . ', \'disapprove\')" type="button" class="btn btn-danger video-btn-publish">Từ chối</button>';
//
//                        switch ($model->outsource_status) {
//                            case VtVideo::OUTSOURCE_APPROVE:
//                                return $disapproveBtn;
//                                break;
//                            case VtVideo::OUTSOURCE_DISAPPROVE:
//                                return $approveBtn;
//                                break;
//                            default:
//                                return $approveBtn . $disapproveBtn;
//                        }
//
//                    },
//                ],



            ],
        ]); ?>
    <?php Pjax::end()?>

</div>
