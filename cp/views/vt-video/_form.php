<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use cp\models\VtGroupTopic;
use cp\models\VtGroupCategory;
use common\libs\S3Service;
use yii\bootstrap\BaseHtml;

/* @var $this yii\web\View */
/* @var $model cp\models\VtVideo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-video-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'video-form']); ?>
    <?= $form->errorSummary($model); ?>
    <video width="320" height="240" controls>

        <?php $convertedFile = $model->getConvertVideo() ?>
        <?php if($convertedFile):?>
            <?php $path = $convertedFile->bucket . '/' . ltrim($convertedFile->path, '/'); ?>
            <source src="http://cdn-vttvas.storebox.vn/<?= $path ?>" type="video/mp4">
        <?php endif;?>
        Vui lòng chọn trình duyệt hỗ trợ HTML5
    </video>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => 1000]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(VtGroupCategory::getAllCategory(VtGroupCategory::TYPE_VOD), 'id', 'name'), ['class' => 'form-control category-checkbox-list', 'prompt' => Yii::t('backend','--- Chọn thể loại ---')]); ?>


    <?=
    $form->field($model, 'topics')->checkboxList(ArrayHelper::map(VtGroupTopic::getAllActiveTopic(VtGroupTopic::TYPE_VOD), 'id', 'name'), [
        'class' => 'form-control topic-checkbox-list',
        'prompt' => '--- Chọn chủ đề ---'
    ]);
    ?>

    <div class="snapshot-choice">
        <?php
        $arr = explode('.', $model->file_path);
        $snapshotPath = $arr[0];

        $radioArr = [];
        for ($i = 1; $i <= 5; $i++) {
            $radioArr[] = (!empty($model->syn_id)) ? $snapshotPath . '_' . $i . '.jpg' : 'snapshot/' . $snapshotPath . '_' . $i . '.jpg';
        }
        ?>


        <?=
                $form->field($model, 'snapshot')
                ->radioList($radioArr, [
                    'item' => function($index, $label, $name, $checked, $value) use ($model) {
                        $return = '<label class="modal-radio">';
                        $return .= '<input type="radio" name="' . $name . '" value="' . $label . '">';
                        $return .= '<i></i>';
                        $return .= "<img class='preview-image' src='" . S3Service::generateWebUrl($model->file_bucket, $label) . "'>";
                        $return .= '</label>';

                        return $return;
                    }
                    ]
                )
                ->label(false);
        ?>

    </div>
    <div class="clearfix"></div>

    <div class="form-group">
    <?php if (!$model->isNewRecord && !empty($model->path)): ?>
            <img class="preview-image" src="<?= S3Service::generateWebUrl($model->bucket, $model->path) ?>"/>
    <?php endif ?>
    </div>

    <div class="form-group field-vtvideo-reason">
        <label class="control-label" for="vtvideo-reason">Lý do từ chối</label>
        <?= BaseHtml::dropDownList('reason-choice', null, Yii::$app->params['reason.unapprove'],
            ['id'=> 'reason-choice', 'class' => 'form-control']

        ) ?>
    </div>

    <?php

    $this->registerJs(
        "$('#reason-choice').on('change', function() { if($(this).val() !== '0') {\$('#vtvideo-reason').val($(this).val())}; });"

    );

    ?>

    <?= $form->field($model, 'reason')->textarea(['maxlength' => 1000])->label('Mô tả lỗi') ?>

    <div class="form-group">

        <?php if ($model->isOutsourceDraft() && $model->outsource_review_by != Yii::$app->user->identity->getId()): ?>
            <?= Html::submitButton('Đánh dấu đang duyệt', ['class' => 'btn blue', 'name' => 'submitReviewing', 'value' => 'submit_reviewing', 'onClick' => "return confirm('Bạn có chắc chắn muốn chuyển video sang trạng thái đang duyệt ?')"]) ?>
        <?php endif;?>

        <?php if (!$model->isOutsourceApprove() || $model->isOutsourceDraft()): ?>
            <?= Html::submitButton('Phê duyệt', ['class' => 'btn btn-success', 'name' => 'submitApprove', 'value' => 'submit_approve', 'onClick' => "return confirm('Bạn có chắc chắn phê duyệt Video này ?')" ]) ?>
        <?php endif;?>

        <?php if ($model->isOutsourceApprove() || $model->isOutsourceDraft()): ?>
            <?= Html::submitButton('Từ chối', ['class' => 'btn btn-danger', 'name' => 'submitDisapprove', 'value' => 'submit_disapprove', 'onClick' => "return confirm('Bạn có chắc chắn từ chối duyệt Video này ?')" ]) ?>
        <?php endif; ?>

    </div>

<?php ActiveForm::end(); ?>

</div>
