<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\VtVideo;
/* @var $this yii\web\View */
/* @var $model backend\models\VtCommentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="col-md-3">
        <?= Html::activeInput('text', $searchModel,
            'name',
            ['class' => 'form-control', 'placeholder' => '---Tên Video---']
        ) ?>
    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList($searchModel,
            'syn_id',
            [
                '0' => 'Khách hàng',
                '1' => 'Quản trị viên'
            ],
            ['class'=>'form-control','prompt' => '---Nguồn dữ liệu---']
        )?>

    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList(
            $searchModel,
            'category_id',
            (ArrayHelper::map(\backend\models\VtGroupCategory::getAllCategory(), 'id', 'name')),
            ['class'=>'form-control','prompt' => '---Chọn chuyên mục---'])?>
    </div>

    <div class="col-md-3">
        <?= Html::activeDropDownList(
            $searchModel,
            'outsource_status',
            Yii::$app->params['outsource.status.dropdown.value'],
            ['class'=>'form-control','prompt' => '---Chọn trạng thái---'])?>
    </div>

    <div class="col-md-3">
        <?= \kartik\daterange\DateRangePicker::widget([
            'model'=>$searchModel,
            'name'=>'range_created_at',
            'attribute' => 'created_at',
            'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
            'convertFormat'=>true,
            'pluginOptions'=>[
                'timePicker'=>true,
                'timePicker24Hour' => true,
                'locale'=>['format'=>'Y-m-d H:i:s']
            ],
            'options' => [
                'class'=>'form-control',
                'placeholder' => '---Thời gian tạo---'
            ]
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= \kartik\daterange\DateRangePicker::widget([
            'model'=>$searchModel,
            'name'=>'range_outsource_review_at',
            'attribute' => 'outsource_review_at',
            'value'=> date('Y-m-00 H:i:s').' - '. date('Y-m-d H:i:s'),
            'convertFormat'=>true,
            'pluginOptions'=>[
                'timePicker'=>true,
                'timePicker24Hour' => true,
                'locale'=>['format'=>'Y-m-d H:i:s']
            ],
            'options' => [
                'class'=>'form-control',
                'placeholder' => '---Thời gian duyệt---'
            ]
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= \kartik\select2\Select2::widget([
            'model' => $searchModel,
            'attribute' => 'created_by',
            'initValueText' => $searchModel->getCreatedByName(),
            'options' => ['placeholder' => '---Người tạo (Kênh)---'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Loading ...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['user/search']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term }; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(result) { return result.text; }'),
                'templateSelection' => new JsExpression('function (result) { return result.text; }')
            ]
        ]) ?>
    </div>

    <div class="col-md-3">
        <?= Html::submitButton('Tìm kiếm', [
            'class' => 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>