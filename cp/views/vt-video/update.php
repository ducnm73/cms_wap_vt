<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VtVideo */

$this->title = 'Cập nhật Video: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Videos', 'url' => !empty($homeUrl)? base64_decode($homeUrl):['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>

<div class="vt-video-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
