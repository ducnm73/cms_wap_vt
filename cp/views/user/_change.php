<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VtUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vt-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'password_old')->passwordInput() ?>

    <?= $form->field($model, 'password_hash')->passwordInput(['value' => ''])->label('Mật khẩu mới') ?>

    <?= $form->field($model, 're_password')->passwordInput()->label('Gõ lại mật khẩu mới') ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
