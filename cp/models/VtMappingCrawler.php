<?php

namespace cp\models;

use Yii;
use yii\helpers\ArrayHelper;

class VtMappingCrawler extends \common\models\VtMappingCrawlerBase
{

    public static function countCrawlerNotMapping()
    {
        $query = self::find()
            ->where([
                'local_user_id' => null,
                'local_category_id' => null
            ]);

        return $query->count();
    }

    public static function getAllCrawler()
    {
        $query = self::find()
            ->asArray();

        return $query->all();


    }

}