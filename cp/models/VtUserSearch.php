<?php

namespace cp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\VtUser;

/**
 * VtUserSearch represents the model behind the search form about `cp\models\VtUser`.
 */
class VtUserSearch extends VtUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'msisdn', 'status', 'follow_count', 'changed_password'], 'integer'],
            [['password', 'salt', 'full_name', 'email', 'oauth_id', 'otp', 'last_login', 'bucket', 'path'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'msisdn' => $this->msisdn,
            'status' => $this->status,
            'follow_count' => $this->follow_count,
            'last_login' => $this->last_login,
            'changed_password' => $this->changed_password,
        ]);

        $query->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'salt', $this->salt])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'oauth_id', $this->oauth_id])
            ->andFilterWhere(['like', 'otp', $this->otp])
            ->andFilterWhere(['like', 'bucket', $this->bucket])
            ->andFilterWhere(['like', 'path', $this->path]);

        return $dataProvider;
    }
}
