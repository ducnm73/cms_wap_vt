<?php

namespace cp\models;

use cp\models\VtHistoryView;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * LogTransactionSearch represents the model behind the search form about `cp\models\VtVideo`.
 */
class VtHistoryViewSearch extends VtHistoryView
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'item_id', 'time', 'content_type', 'duration', 'content', 'request_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $msisdn, $userId)
    {
        $query = VtHistoryView::find();

        if(!empty($msisdn) && !empty($userId)){
            $query->andWhere(['msisdn' => $msisdn])
                ->orWhere(['user_id' => $userId]);
        }elseif(!empty($msisdn) || empty($userId)){
            $query->andWhere(['msisdn' => $msisdn]);
        }elseif(empty($msisdn) || !empty($userId)){
            $query->andWhere(['user_id' => $userId]);
        }

        if (isset($params['VtHistoryViewSearch']['request_time']) && !empty($params['VtHistoryViewSearch']['request_time'])) {

            $split = explode(' - ', $params['VtHistoryViewSearch']['request_time']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->filter([
                'range' => ['request_time' => [
                        'gte' => strtotime($beginDate . ' 00:00:00'),
                        'lte' => strtotime($endDate . ' 23:59:59'),
                    ]
                ]
            ]);

        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['request_time' => SORT_DESC]]
        ]);

//        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id' => $this->id,
//            'category_id' => $this->category_id,
//        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'resolution', $this->resolution]);

        return $dataProvider;
    }
}
