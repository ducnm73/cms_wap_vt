<?php

namespace cp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\VtSmsMtHis;


/**
 * LogTransactionSearch represents the model behind the search form about `cp\models\VtVideo`.
 */
class VtSmsMtHisSearch extends VtSmsMtHis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'sent_time', 'receive_time'], 'required'],
            [['message'], 'string'],
            [['mo_his_id', 'status', 'process_id'], 'integer'],
            [['sent_time', 'receive_time'], 'safe'],
            [['msisdn'], 'string', 'max' => 15],
            [['channel'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $msisdn = null)
    {
        $query = VtSmsMtHis::find();

        if(isset($msisdn) && !empty($msisdn)){
            $query->andWhere(['msisdn' => $msisdn]);
        }

        if (isset($params['VtSmsMtHisSearch']['sent_time']) && !empty($params['VtSmsMtHisSearch']['sent_time'])) {
            $split = explode(' - ', $params['VtSmsMtHisSearch']['sent_time']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere('sent_time between :beginTime and :endTime', [
                ':beginTime' => $beginDate . ' 00:00:00',
                ':endTime' => $endDate . ' 23:59:59'
            ]);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id' => $this->id,
//            'category_id' => $this->category_id,
//        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'resolution', $this->resolution]);

        return $dataProvider;
    }
}
