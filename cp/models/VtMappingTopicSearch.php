<?php

namespace cp\models;

use common\libs\Crawler;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\VtVideo;
use yii\helpers\ArrayHelper;

/**
 *
 * VtVideoSearch represents the model behind the search form about `cp\models\VtVideo`.
 */


class VtMappingTopicSearch extends VtMappingTopic
{
    public $topic_name;
    public $category_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'topic_name', 'category_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ids)
    {
        $query = self::find()
            ->select('v.*, t.name as topic_name, c.name as category_name')
            ->from(self::tableName(). ' v')
            ->leftJoin(VtGroupTopic::tableName() . ' t', 'v.local_topic_id = t.id')
            ->leftJoin(VtGroupCategory::tableName() . ' c', 'v.local_category_id = c.id')
            ->where(['v.crawler_category_id' => $ids]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        return $dataProvider;
    }
}
