<?php

namespace cp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\LogTransaction;
use cp\models\LogTransactionFail;


/**
 * LogTransactionSearch represents the model behind the search form about `cp\models\VtVideo`.
 */
class LogTransactionSearch extends LogTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'msisdn', 'fee'], 'integer'],
            [['datetime'], 'safe'],
            [['action', 'error_code', 'content', 'other_info'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $msisdn = null, $userId = null)
    {
        $query = LogTransaction::find();

        if (!empty($msisdn) || !empty($userId)) {
            $query->andWhere('msisdn = :msisdn or user_id = :user_id', [
                'msisdn' => $msisdn,
                'user_id' => $userId
            ]);
        } elseif (!empty($msisdn) || empty($userId)) {
            $query->andWhere('msisdn = :msisdn', [
                'msisdn' => $msisdn
            ]);
        } elseif (empty($msisdn) || !empty($userId)) {
            $query->andWhere('user_id = :user_id', [
                'user_id' => $userId
            ]);
        }

        if (isset($params['LogTransactionSearch']['datetime']) && !empty($params['LogTransactionSearch']['datetime'])) {
            $split = explode(' - ', $params['LogTransactionSearch']['datetime']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere('datetime between :beginTime and :endTime', [
                ':beginTime' => $beginDate . ' 00:00:00',
                ':endTime' => $endDate . ' 23:59:59'
            ]);
        }

        $queryFail = LogTransactionFail::find();

        if (!empty($msisdn) || !empty($userId)) {
            $queryFail->andWhere('msisdn = :msisdn or user_id = :user_id', [
                'msisdn' => $msisdn,
                'user_id' => $userId
            ]);
        } elseif (!empty($msisdn) || empty($userId)) {
            $queryFail->andWhere('msisdn = :msisdn', [
                'msisdn' => $msisdn
            ]);
        } elseif (empty($msisdn) || !empty($userId)) {
            $queryFail->andWhere('user_id = :user_id', [
                'user_id' => $userId
            ]);
        }

        if (isset($params['LogTransactionSearch']['datetime']) && !empty($params['LogTransactionSearch']['datetime'])) {
            $split = explode(' - ', $params['LogTransactionSearch']['datetime']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $queryFail->andWhere('datetime between :beginTime and :endTime', [
                ':beginTime' => $beginDate . ' 00:00:00',
                ':endTime' => $endDate . ' 23:59:59'
            ]);
        }

        $offset = (isset($params['page']))?20*($params['page']-1):0;
        $queryFail->limit(20);
        $queryFail->offset($offset);
        $queryTmp = $query->union($queryFail);


        $dataProvider = new ActiveDataProvider([
            'query' => $queryTmp,
            'sort' => ['defaultOrder' => ['datetime' => SORT_DESC]]
        ]);


        // var_dump($params);
        // die();

        if (!$this->validate()) {

            return $dataProvider;
        }


        return $dataProvider;
    }
}
