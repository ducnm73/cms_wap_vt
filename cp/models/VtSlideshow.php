<?php

namespace cp\models;

use yii\web\UploadedFile;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use Yii;

class VtSlideshow extends \common\models\VtSlideshowBase
{

    public function saveWithImage()
    {
        $file = UploadedFile::getInstance($this, 'path');

        if (!empty($file)) {
            $bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);

            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->user_id;
            }
            $this->updated_by = Yii::$app->user->identity->user_id;

            $this->bucket = $bucket;
            $this->path = $path;
        } else {
            unset($this->bucket);
            unset($this->path);
        }

        $this->save();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'position', 'is_active', 'updated_by', 'created_by'], 'integer'],
            [['begin_time', 'end_time'], 'safe'],
            [['type'], 'string', 'max' => 10],
            [['location'], 'string', 'max' => 50],
            [['name', 'bucket'], 'string', 'max' => 255],
            [['href'], 'url'],

            [['name', 'location', 'begin_time', 'end_time', 'type', 'position'], 'required'],
            [['path'], 'safe'],
//            [['path'], 'required'],
            [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],

            [['item_id'], 'required', 'when' => function($model) {
                    return in_array($model->type, ['VOD', 'PLAYLIST']);
                }, 'whenClient' => "function (attribute, value) {
                        return ($('#vtslideshow-type').val() == 'VOD' || $('#vtslideshow-type').val() == 'PLAYLIST');
                    }"
            ],
            [['href'], 'required', 'when' => function($model) {
                    return in_array($model->type, ['HREF']);
                }, 'whenClient' => "function (attribute, value) {
                        return ($('#vtslideshow-type').val() == 'HREF');
                    }"
            ],
            [['location'], 'validateLocation'],
            [['type'], 'validateType'],
        ];
    }

    public function validateLocation($attribute, $params)
    {
        if (!in_array($this->$attribute, Yii::$app->params['slideshow.location'])) {
            $this->addError($attribute, Yii::t('app', 'Vị trí hiển thị không hợp lệ".'));
        }
    }

    public function validateType($attribute, $params)
    {
        if (!in_array($this->$attribute, Yii::$app->params['slideshow.object.type'])) {
            $this->addError($attribute, Yii::t('app', 'Loại không hợp lệ".'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'item_id' => 'Nội dung',
            'position' => 'Thứ tự',
            'location' => 'Vị trí',
            'name' => 'Tên',
            'href' => 'Đường dẫn',
            'begin_time' => 'Thời gian bắt đầu',
            'end_time' => 'Thời gian kết thúc',
            'is_active' => 'Kích hoạt',
            'bucket' => 'Bucket',
            'path' => 'Ảnh đại diện',
        ];
    }

}