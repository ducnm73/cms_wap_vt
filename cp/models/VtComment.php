<?php

namespace cp\models;

use Yii;

class VtComment extends \common\models\VtCommentBase
{

    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('vt-comment')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content_id' => Yii::t('app', 'Content ID'),
            'type' => Yii::t('app', 'Loại'),
            'user_id' => Yii::t('app', 'User ID'),
            'content' => Yii::t('app', 'Nội dung'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'status' => Yii::t('app', 'Trạng thái'),
            'bad_word_filter' => Yii::t('app', 'Bad Word Filter'),
            'approve_by' => Yii::t('app', 'Approve By'),
            'reason' => Yii::t('app', 'Reason'),
            'like_count' => Yii::t('app', 'Like Count'),
            'created_at' => Yii::t('app', 'Thời điểm tạo'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'comment' => Yii::t('app', 'Comment'),
            'comment_count' => Yii::t('app', 'Comment Count'),
        ];
    }
}