<?php

namespace cp\models;

use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use Yii;
use yii\web\UploadedFile;

class VtGroupCategory extends \common\models\VtGroupCategoryBase {

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['is_active', 'positions'], 'integer'],
            [['name', 'bucket', 'path'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
            [['name'], 'unique'],
            [['path'], 'safe'],
            [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],
        ];
    }

    public static function searchByName($q, $limit = 10)
    {
        $query = self::find()
            ->asArray()
            ->select('id, name as text')
            ->where([
                'is_active' => self::ACTIVE
            ])
            ->andWhere(['like', 'name', $q])
            ->limit($limit);

        return $query->all();
    }

    public function saveWithImage(){

        $file = UploadedFile::getInstance($this, 'path');

        if(!empty($file)){
            $bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);

            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            if($this->isNewRecord){
                $this->created_by = Yii::$app->user->identity->user_id;
            }
            $this->updated_by = Yii::$app->user->identity->user_id;

            $this->bucket = $bucket;
            $this->path = $path;
        }else{
            unset($this->bucket);
            unset($this->path);
        }

        $this->save();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'type' => 'Loại',
            'description' => 'Mô tả',
            'is_active' => 'Kích hoạt',
            'bucket' => 'Bucket',
            'path' => 'Ảnh đại diện',
            'positions' => 'Vị trí',
        ];
    }

}