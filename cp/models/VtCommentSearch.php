<?php

namespace cp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\VtComment;

/**
 * VtCommentSearch represents the model behind the search form about `cp\models\VtComment`.
 */
class VtCommentSearch extends VtComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'content_id', 'user_id', 'parent_id', 'status', 'approve_by', 'like_count', 'comment_count'], 'integer'],
            [['type', 'content', 'bad_word_filter', 'reason', 'created_at', 'updated_at', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VtComment::find()
                ->from(self::tableName(). ' c')
                ->joinWith('user');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.content_id' => $this->content_id,
            'c.user_id' => $this->user_id,
            'c.parent_id' => $this->parent_id,
            'c.status' => $this->status,
            'c.approve_by' => $this->approve_by,
            'c.like_count' => $this->like_count,
            'c.created_at' => $this->created_at,
            'c.updated_at' => $this->updated_at,
            'c.comment_count' => $this->comment_count,
        ]);

        $query->andFilterWhere(['like', 'c.type', $this->type])
            ->andFilterWhere(['like', 'c.content', $this->content])
            ->andFilterWhere(['like', 'c.bad_word_filter', $this->bad_word_filter])
            ->andFilterWhere(['like', 'c.reason', $this->reason])
            ->andFilterWhere(['like', 'c.comment', $this->comment]);

        return $dataProvider;
    }
}
