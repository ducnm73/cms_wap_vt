<?php

namespace cp\models;

use common\libs\Crawler;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 *
 * VtVideoSearch represents the model behind the search form about `cp\models\VtVideo`.
 */


class VtMappingCrawlerSearch extends VtMappingCrawler
{
    public $name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ids)
    {
        $query = self::find()
            ->select('v.*, t.name')
            ->from(self::tableName(). ' v')
            ->leftJoin(VtGroupCategory::tableName() . ' t', 'v.local_category_id = t.id')
            ->leftJoin(VtUser::tableName(). ' u', 'v.local_user_id = u.id')
            ->where(['v.crawler_id' => $ids]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        return $dataProvider;
    }
}
