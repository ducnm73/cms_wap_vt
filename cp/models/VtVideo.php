<?php

namespace cp\models;

use api\models\VtVideoTopic;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\AuthUser;
use common\models\VtUserBase;
use common\models\VtVideoBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class VtVideo extends \common\models\VtVideoBase
{
    public $snapshot;

    public function rules()
    {
        return [
            [['is_active', 'is_no_copyright', 'is_hot', 'is_recommend', 'status', 'convert_priority', 'suggest_package_id', 'review_by', 'category_id'], 'integer'],
            [['name', 'created_at', 'updated_at', 'file_path', 'file_bucket'], 'required'],
            [['name', 'slug', 'path', 'file_path', 'file_bucket', 'snapshot'], 'string', 'max' => 255],
            [['description', 'reason'], 'string', 'max' => 1000],
            [['type'], 'string', 'max' => 10],
            [['seo_title'], 'string', 'max' => 200],
            [['seo_description', 'seo_keywords', 'tag'], 'string', 'max' => 1000],
            [['bucket'], 'string', 'max' => 50],
            [['published_time', 'created_at', 'updated_at', 'convert_start_time', 'convert_end_time'], 'safe'],
        ];
    }

    public function getUser() {
        return $this->hasOne(VtUserBase::className(), ['id' => 'created_by']);
    }

    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('approve-video')) {
            return true;
        } else {
            return false;
        }
    }

    public function isOwner()
    {
        if (Yii::$app->user->identity->user_id == $this->created_by) {
            return true;
        } else {
            return false;
        }
    }

    public function isApprove() {
        if ($this->status == self::STATUS_APPROVE) {
            return true;
        } else {
            return false;
        }
    }

    public function isDraft() {
        if ($this->status == self::STATUS_DRAFT) {
            return true;
        } else {
            return false;
        }
    }

    public function isOutsourceApprove()
    {
        if ($this->outsource_status == self::OUTSOURCE_APPROVE) {
            return true;
        } else {
            return false;
        }
    }

    public function isOutsourceDraft()
    {
        if ($this->outsource_status == self::OUTSOURCE_DRAFT || $this->outsource_status == self::OUTSOURCE_REVIEWING) {
            return true;
        } else {
            return false;
        }
    }

    public function isDelete()
    {
        if ($this->status == self::STATUS_DELETE) {
            return true;
        } else {
            return false;
        }
    }

    public function isConverted()
    {
        if ($this->convert_status == self::CONVERT_STATUS_SUCCESS) {
            return true;
        } else {
            return false;
        }
    }

    public function saveWithImage($topicIds, $snapshot = false) {
        $file = UploadedFile::getInstance($this, 'path');

        if (!empty($file)) {
            $bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);

            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->user_id;
            }

            $this->bucket = $bucket;
            $this->path = $path;
        } else if ($snapshot == false) {
            unset($this->bucket);
            unset($this->path);
        }
        $this->name_slug = Utils::removeSignOnly($this->name);
        $this->description_slug = Utils::removeSignOnly($this->description);
        $this->updated_by = Yii::$app->user->identity->id;

        $this->save(false);


        //Xu ly topics
        if (($actualTopics = VtVideoTopic::find()
                ->andWhere(["video_id" => $this->id])
                ->asArray()
                ->all()) !== null
        ) {
            $actualTopics = ArrayHelper::getColumn($actualTopics, 'topic_id');
            $topicExists = 1;
        }

        if (!empty($topicIds)) { //save the relations
            foreach ($topicIds as $topicId) {

                if (!in_array($topicId, $actualTopics)) {
                    $r = new VtVideoTopic();
                    $r->video_id = $this->id;
                    $r->topic_id = $topicId;
                    $r->save(false);
                }
            }
        }

        if (!empty($topicIds)) {
            $actualTopics = array_diff($actualTopics, $topicIds);
        } else {
            $actualTopics = [];
        }


        if (isset($topicExists) && $topicExists == 1) {

            foreach ($actualTopics as $remove) {
                $r = VtVideoTopic::findOne(
                    [
                        'topic_id' => $remove,
                        'video_id' => $this->id
                    ]
                );
                $r->delete();
            }
        }
    }



    public function getCreatedByName()
    {
        $objUsername = VtUserBase::getById($this->created_by);
        return ($objUsername) ? $objUsername->full_name : $this->created_by;
    }

    public function getUpdatedByName()
    {
        $objUsername = VtUserBase::getById($this->updated_by);
        return ($objUsername) ? $objUsername->full_name : $this->updated_by;
    }

    public function getOutsourceReviewByName() {
        $user = AuthUser::findOne(['id' => $this->outsource_review_by]);
        return ($user) ? $user->username : $this->outsource_review_by;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Tên'),
            'description' => Yii::t('app', 'Mô tả'),
            'slug' => Yii::t('app', 'Slug'),
            'category_id' => Yii::t('app', 'Thể loại'),
            'attributes' => Yii::t('app', 'Thuộc tính'),
            'published_time' => Yii::t('app', 'Thời gian xuất bản'),
            'is_active' => Yii::t('app', 'Kích hoạt'),
            'price_download' => Yii::t('app', 'Giá tải'),
            'price_play' => Yii::t('app', 'Giá xem'),
            'play_times' => Yii::t('app', 'Lượt xem'),
            'status' => Yii::t('app', 'Trạng thái'),
            'reason' => Yii::t('app', 'Lý do'),
            'approve_by' => Yii::t('app', 'Phê duyệt bởi'),
            'type' => Yii::t('app', 'Loại'),
            'cp_id' => Yii::t('app', 'CP'),
            'created_at' => Yii::t('app', 'Thời gian tạo'),
            'updated_at' => Yii::t('app', 'Thời gian cập nhật'),
            'created_by' => Yii::t('app', 'Tạo bởi'),
            'updated_by' => Yii::t('app', 'Cập nhật bởi'),
            'review_by' => Yii::t('app', 'Xem lướt bởi'),
            'tag' => Yii::t('app', 'Tag'),
            'related_id' => Yii::t('app', 'Video liên quan'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'is_no_copyright' => Yii::t('app', 'Không có bản quyền'),
            'like_count' => Yii::t('app', 'Lượt yêu thích'),
            'bucket' => Yii::t('app', 'Bucket'),
            'path' => 'Ảnh đại diện',
            'is_hot' => Yii::t('app', 'Hot'),
            'is_recommend' => Yii::t('app', 'Đề xuất'),
            'convert_status' => Yii::t('app', 'Trạng thái convert'),
            'convert_start_time' => Yii::t('app', 'Thời gian bắt đầu convert'),
            'convert_end_time' => Yii::t('app', 'Thời gian kết thúc convert'),
            'duration' => Yii::t('app', 'Thời lượng'),
            'file_path' => Yii::t('app', 'File Path'),
            'file_bucket' => Yii::t('app', 'File Bucket'),
            'convert_priority' => Yii::t('app', 'Thứ tự convert'),
            'resolution' => Yii::t('app', 'Độ phân giải'),
            'topics' => 'Chủ đề',
            'suggest_package_id' => 'Gói cước quảng cáo'
        ];
    }

}