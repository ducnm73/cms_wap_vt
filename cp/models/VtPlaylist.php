<?php

namespace cp\models;

use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtUserBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class VtPlaylist extends \common\models\VtPlaylistBase
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'category_id'], 'required'],
            [['description'], 'string', 'max' => 1000],
            [['is_active', 'status', 'category_id', 'attributes', 'price_play', 'priority', 'approved_by', 'created_by', 'updated_by', 'is_hot', 'is_recommend', 'like_count', 'suggest_package_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 25],
            [['name', 'bucket'], 'string', 'max' => 255],
            [['reason'], 'string', 'max' => 500],
            [['path'], 'safe'],
            [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],
//            [['path'], 'checkResolution']
        ];
    }


    public function checkApprovePermission()
    {
        if (Yii::$app->user->can('approve-film')) {
            return true;
        } else {
            return false;
        }
    }

    public function isOwner()
    {
        if (Yii::$app->user->identity->user_id == $this->created_by) {
            return true;
        } else {
            return false;
        }
    }

    public function isApprove()
    {
        if ($this->status == self::STATUS_APPROVE) {
            return true;
        } else {
            return false;
        }
    }

    public function isDraft()
    {
        if ($this->status == self::STATUS_DRAFT) {
            return true;
        } else {
            return false;
        }
    }

    public function isDelete()
    {
        if ($this->status == self::STATUS_DELETE) {
            return true;
        } else {
            return false;
        }
    }


    public function saveWithImage($topicIds)
    {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->created_by = Yii::$app->user->identity->user_id;
        }

        $this->type = self::TYPE_FILM;
        $this->updated_at = date('Y-m-d H:i:s');

        $this->slug = Utils::removeSign($this->name);

        $file = UploadedFile::getInstance($this, 'path');

        if (!empty($file)) {
            $bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);

            VtHelper::generateAllThumb($bucket, $file->tempName, $path);

            $this->bucket = $bucket;
            $this->path = $path;
        } else {
            unset($this->bucket);
            unset($this->path);
        }

        $this->name_slug = Utils::removeSignOnly($this->name);
        $this->description_slug = Utils::removeSignOnly($this->description);
        $this->updated_by = Yii::$app->user->identity->id;

        $this->save();


        //Xu ly topics
        if (($actualTopics = VtPlaylistTopic::find()
                ->andWhere(["playlist_id" => $this->id])
                ->asArray()
                ->all()) !== null
        ) {
            $actualTopics = ArrayHelper::getColumn($actualTopics, 'topic_id');
            $topicExists = 1; // if there is authors relations, we will work it latter
        }

        if (!empty($topicIds)) { //save the relations
            foreach ($topicIds as $topicId) {

                if(!in_array($topicId, $actualTopics)) {
                    $r = new VtVideoTopic();
                    $r->video_id = $this->id;
                    $r->topic_id = $topicId;
                    $r->save(false);
                }
            }
        }

        $actualTopics = array_diff($actualTopics, $topicIds);

        if (isset($topicExists) && $topicExists == 1) { //delete authors tha does not belong anymore to this book

            foreach ($actualTopics as $remove) {
                $r = VtPlaylistTopic::findOne(
                    [
                        'topic_id' => $remove,
                        'playlist_id' => $this->id
                    ]
                );
                $r->delete();
            }
        }
    }

    public function getCreatedByName()
    {
        $objUsername = VtUserBase::getById($this->created_by);
        return ($objUsername) ? $objUsername->full_name : $this->created_by;
    }

    public function getUpdatedByName()
    {
        $objUsername = VtUserBase::getById($this->updated_by);
        return ($objUsername) ? $objUsername->full_name : $this->updated_by;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'name' => 'Tên',
            'description' => 'Mô tả',
            'is_active' => 'Kích hoạt',
            'status' => 'Trạng thái',
            'category_id' => 'Thể loại',
            'attributes' => 'Thuộc tính',
            'price_play' => 'Giá xem',
            'reason' => 'Lý do',
            'priority' => 'Thứ tự',
            'approved_by' => 'Phê duyệt bởi',
            'created_at' => 'Tạo bởi',
            'updated_at' => 'Thời gian cập nhật',
            'created_by' => 'Tạo bởi',
            'updated_by' => 'Cập nhật bởi',
            'bucket' => 'Bucket',
            'path' => 'Path',
            'is_hot' => 'Hot',
            'is_recommend' => 'Đề xuất',
            'like_count' => 'Lượt yêu thích',
            'topics' => 'Chủ đề',
            'suggest_package_id' => 'Gói cước quảng cáo'
        ];
    }

}