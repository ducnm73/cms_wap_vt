<?php

namespace cp\models;

use Yii;
use yii\helpers\ArrayHelper;

class VtMappingTopic extends \common\models\VtMappingTopicBase
{



    public static function countTopicNotMapping()
    {
        $query = self::find()
            ->where(['local_topic_id' => null])
            ->orWhere(['local_category_id' => null]);

        return $query->count();
    }


    public static function getAllAttribute()
    {
        $query = self::find()
            ->asArray();

        return $query->all();
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'crawler_category_id' => Yii::t('app', 'Chuyên mục Crawler'),
            'local_topic_id' => Yii::t('app', 'Chủ đề'),
            'local_category_id' => Yii::t('app', 'Thể loại'),
        ];
    }
}