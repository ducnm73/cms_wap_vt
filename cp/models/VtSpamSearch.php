<?php

namespace cp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\VtSpam;


/**
 * LogTransactionSearch represents the model behind the search form about `cp\models\VtVideo`.
 */
class VtSpamSearch extends VtSpam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'is_sent', 'number', 'current_line', 'created_by', 'updated_by', 'item_id', 'item_group_id', 'status', 'channel'], 'integer'],
            [['send_time', 'created_at', 'updated_at'], 'safe'],
            [['name', 'path', 'item_type'], 'string', 'max' => 255],
            [['content'], 'string', 'max' => 500],
            [['rule'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $msisdn = null)
    {
        $query = VtSpam::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        return $dataProvider;
    }
}
