<?php

namespace cp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use cp\models\VtVideo;

/**
 * VtVideoSearch represents the model behind the search form about `cp\models\VtVideo`.
 */
class VtVideoSearch extends VtVideo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'attributes', 'is_active', 'price_download', 'price_play', 'play_times', 'status', 'approve_by', 'cp_id', 'created_by', 'updated_by', 'is_no_copyright', 'like_count', 'is_hot', 'is_recommend', 'convert_status', 'duration', 'convert_priority', 'review_by', 'syn_id', 'outsource_status', 'outsource_need_review', 'outsource_review_by'], 'integer'],
            [['name', 'description', 'slug', 'published_time', 'reason', 'type', 'created_at', 'updated_at', 'tag', 'related_id', 'seo_title', 'seo_description', 'seo_keywords', 'bucket', 'path', 'convert_start_time', 'convert_end_time', 'file_path', 'file_bucket', 'resolution', 'syn_id', 'outsource_review_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = VtVideo::TYPE_VOD)
    {
        $query = VtVideo::find()
            ->select('v.*')
            ->from(self::tableName(). ' v')
            ->joinWith('user')
            ->joinWith('category')
            ->joinWith('outsource')
            ->where([
                'v.type' => $type,
                'v.outsource_need_review' => VtVideo::OUTSOURCE_NEED_APPROVE,
                'v.convert_status' => VtVideo::CONVERT_STATUS_SUCCESS
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);


        if (!empty($this->created_at)) {
            $split = explode(' - ', $this->created_at);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere('v.created_at between :createdAtBeginTime and :createdAtEndTime', [
                ':createdAtBeginTime' => $beginDate,
                ':createdAtEndTime' => $endDate
            ]);
        }

        if (!empty($this->outsource_review_at)) {
            $split = explode(' - ', $this->outsource_review_at);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere('v.outsource_review_at between :createdAtBeginTime and :createdAtEndTime', [
                ':createdAtBeginTime' => $beginDate,
                ':createdAtEndTime' => $endDate
            ]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'v.id' => $this->id,
            'v.category_id' => $this->category_id,
            'v.attributes' => $this->attributes,
            'v.is_active' => $this->is_active,
            'v.price_download' => $this->price_download,
            'v.price_play' => $this->price_play,
            'v.play_times' => $this->play_times,
            'v.status' => $this->status,
            'v.approve_by' => $this->approve_by,
            'v.cp_id' => $this->cp_id,
            'v.updated_at' => $this->updated_at,
            'v.created_by' => $this->created_by,
            'v.is_no_copyright' => $this->is_no_copyright,
            'v.like_count' => $this->like_count,
            'v.is_hot' => $this->is_hot,
            'v.is_recommend' => $this->is_recommend,
            'v.convert_status' => $this->convert_status,
            'v.convert_start_time' => $this->convert_start_time,
            'v.convert_end_time' => $this->convert_end_time,
            'v.duration' => $this->duration,
            'v.convert_priority' => $this->convert_priority,
            'v.review_by' => $this->review_by,
//            'v.outsource_review_at' => $this->outsource_review_at,
            'v.outsource_review_by' => $this->outsource_review_by,
            'v.outsource_status' => $this->outsource_status,
            'v.outsource_need_review' => $this->outsource_need_review,
        ]);

        $query->andFilterWhere(['like', 'v.name', $this->name])
            ->andFilterWhere(['like', 'v.description', $this->description])
            ->andFilterWhere(['like', 'v.slug', $this->slug])
            ->andFilterWhere(['like', 'v.reason', $this->reason])
            ->andFilterWhere(['like', 'v.type', $this->type])
            ->andFilterWhere(['like', 'v.tag', $this->tag])
            ->andFilterWhere(['like', 'v.related_id', $this->related_id])
            ->andFilterWhere(['like', 'v.seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'v.seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'v.seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'v.bucket', $this->bucket])
            ->andFilterWhere(['like', 'v.path', $this->path])
            ->andFilterWhere(['like', 'v.file_path', $this->file_path])
            ->andFilterWhere(['like', 'v.file_bucket', $this->file_bucket])
            ->andFilterWhere(['like', 'v.resolution', $this->resolution])
        ;

        return $dataProvider;
    }
}
