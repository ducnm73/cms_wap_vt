<?php

namespace cp\models;

use Yii;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use yii\web\UploadedFile;

class VtUser extends \common\models\VtUserBase {
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'status'], 'string'],
            [['last_login', 'is_show_suggest', 'changed_password', 'follow_count', 'video_count'], 'safe'],
            [['password', 'full_name', 'email', 'oauth_id'], 'string', 'max' => 50],
            [['salt', 'bucket'], 'string', 'max' => 255],
            [['msisdn', 'status', 'full_name'], 'required'],
            [['otp'], 'string', 'max' => 10],
            [['path'], 'safe'],
            [['path'], 'file', 'extensions' => 'jpg,jpeg,png', 'mimeTypes' => 'image/jpeg,image/pjpeg,image/png,image/x-png'],
        ];
    }

    public function saveWithImage()
    {
        $file = UploadedFile::getInstance($this, 'path');

        if (!empty($file)) {
            $bucket = Yii::$app->params['s3']['static.bucket'];

            $path = Utils::generatePath($file->extension);

            S3Service::putObject($bucket, $file->tempName, $path);

            VtHelper::generateAllThumb($bucket, $file->tempName, $path);


            $this->bucket = $bucket;
            $this->path = $path;
        } else {
            unset($this->bucket);
            unset($this->path);
        }

        $this->save();
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msisdn' => 'Số điện thoại',
            'status' => 'Trạng thái',
            'password' => 'Password',
            'salt' => 'Salt',
            'full_name' => 'Tên hiển thị',
            'email' => 'Email',
            'oauth_id' => 'Oauth ID',
            'follow_count' => 'Follow Count',
            'otp' => 'Otp',
            'last_login' => 'Last Login',
            'changed_password' => 'Changed Password',
            'bucket' => 'Bucket',
            'path' => 'Path',
            'video_count' => 'Video Count',
            'is_show_suggest' => 'Is Show Suggest',
        ];
    }
}