<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace cp\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EditableAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bootstrap-editable/css/bootstrap-editable.css',

    ];
    public $js = [
        'bootstrap-editable/js/bootstrap-editable.js',
        'js/editable.js'
    ];
    public $depends = [
        'cp\assets\AppAsset',
    ];

}
