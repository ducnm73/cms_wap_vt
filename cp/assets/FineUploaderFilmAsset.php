<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace cp\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FineUploaderFilmAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fine-uploader/fine-uploader/fine-uploader-gallery.min.css',
        'css/upload.css'

    ];
    public $js = [
        'fine-uploader/fine-uploader/fine-uploader.min.js',
        'js/upload-film.js'
    ];
    public $depends = [
        'cp\assets\AppAsset',
    ];

}
