<?php
$config = [
    'language' => 'vi',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'phuml3iFWWmQF337t7p-JZVoJWkr8j7N5f',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii']=[
        'class' =>  'yii\gii\Module',
//        'allowedIPs' => ['127.0.0.1'],
    ];
}

return $config;