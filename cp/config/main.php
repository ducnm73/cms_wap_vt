<?php

use yii\base\ActionEvent;
use yii\base\Controller;
use yii\base\Event;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-cp',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'cp\controllers',
    'bootstrap' => ['log',
        function () {
            Event::on(Controller::className(), Controller::EVENT_AFTER_ACTION, function (ActionEvent $event) {
                Yii::info($event->action->id . '/' . $event->action->controller->id, 'admin-action' );
            });
        },
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'request' => [
            'csrfParam' => '_csrf-cp',
        ],
        'user' => [
            'identityClass' => 'common\models\AuthUser',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-cp', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the cp
            'name' => 'myvideo-cp',
            'class' => 'yii\redis\Session',
            'redis' => 'redis' // id of the connection application component
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => '@logs/admin-action.log',
                    'categories' => ['admin-action'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/site/index',
                'login' => '/site/login',
                'logout' => '/site/logout',
            ]
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'user/change',
            'debug/*'
        ]
    ],
    'params' => $params,
];
