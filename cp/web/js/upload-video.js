function updateVideo(id, isPublish) {

    var allVals = [];
    $('.topic-id-' + id + ':checked').each(function () {
        allVals.push($(this).val());
    });


    $.ajax({
        method: "POST",
        url: $('#update-video-url').val(),
        data: {
            'id': id,
            'name': $('#video-name-' + id).val(),
            'description': $('#video-description-' + id).val(),
            'is_publish': isPublish,
            'category_id': $('.category-id-' + id).val(),
            'topic_ids': JSON.stringify(allVals)
        },
        success: function (data) {
            var dataObj = jQuery.parseJSON(data);

            $('#alert-message-' + id).html("<div class='alert alert-success'>"
                + "<strong>Thông báo!</strong> Lưu dữ liệu thành công."
                + "</div>");

        },
        error: function (data) {
            $('#alert-message-' + id).html("<div class='alert alert-danger'>"
                + "<strong>Thông báo!</strong> Lưu dữ liệu không thành công."
                + "</div>");
        }
    });
}






// Some options to pass to the uploader are discussed on the next page
var uploader = new qq.FineUploader({
    element: document.getElementById("uploader"),
    validation: {
        allowedExtensions: ['mp4', 'flv', 'm4a'],
    },
    request: {
        endpoint: $('#upload-url').val(),
        params: {
            type: $('#upload-type').val(),
            playlist_id: $('#playlist-id').val(),
            '_csrf': yii.getCsrfToken()
        }
    },
    callbacks: {
        onComplete: function (id, name, response) {
            var serverPathToFile = response.filePath,
                fileItem = this.getItemByFileId(id);

            console.log(response);
            if (response.success) {

                var videoNameTxt = qq(fileItem).getByClass("video-name")[0];
                var videoDescriptionTxt = qq(fileItem).getByClass("video-description")[0];
                var videoIdInput = qq(fileItem).getByClass("video-id")[0];
                var submitBtn = qq(fileItem).getByClass("video-btn")[0];
                var submitBtnPublish = qq(fileItem).getByClass("video-btn-publish")[0];
                var alertMessage = qq(fileItem).getByClass("alert-message")[0];
                var videoSpinner = qq(fileItem).getByClass("video-spinner")[0];
                //var videoPreview = qq(fileItem).getByClass("video_preview")[0];

                var imageUploader = qq(fileItem).getByClass("image-uploader")[0];



                var categorySelect = qq(fileItem).getByClass("category-checkbox-list")[0];
                var topicSelect = qq(fileItem).getByClass("checkbox-item");

                videoNameTxt.setAttribute("id", "video-name" + "-" + response.id);
                videoDescriptionTxt.setAttribute("id", "video-description" + "-" + response.id);
                alertMessage.setAttribute("id", "alert-message" + "-" + response.id);
                videoSpinner.remove();

                //videoPreview.setAttribute("data", "/js/cp_player/player.swf?url=" + response.url + "autoplay=1&volume=80&play=false&wmode=transparent");
                videoIdInput.setAttribute("value", response.id);

                var videoName = videoNameTxt.getAttribute("value");
                var videoDescription = videoDescriptionTxt.getAttribute("value");
                if (!videoName) {
                    videoNameTxt.setAttribute("value", response.name);
                }

                if (!videoDescription) {
                    videoDescriptionTxt.setAttribute("value", response.description);
                }

                categorySelect.setAttribute("class", "form-control category-checkbox-list category-id-" + response.id);
                //topicSelect.setAttribute("class", "topic-id-" + response.id);

                for (var i = 0; i < topicSelect.length; i++) {
                    topicSelect[i].setAttribute("class", "topic-id-" + response.id);
                }


                submitBtn.removeAttribute("disabled");
                submitBtnPublish.removeAttribute("disabled");
                submitBtn.setAttribute("onclick", "updateVideo(" + response.id + ", false)");
                submitBtnPublish.setAttribute("onclick", "updateVideo(" + response.id + ", true)");

                imageUploader.setAttribute('id', 'image-uploader-' + response.id);


                var imageUpload = new qq.FineUploader({
                    element: document.getElementById('image-uploader-' + response.id),
                    validation: {
                        allowedExtensions: ['jpg', 'png', 'gif', 'jpeg']
                    },
                    template: 'image-template',

                    request: {
                        endpoint: $('#upload-image-url').val(),
                        params: {
                            id: response.id,
                            '_csrf': yii.getCsrfToken()
                        }
                    },
                    callbacks: {
                        onComplete: function (id, name, response) {
                            console.log(response);

                            var imagePreview = qq(fileItem).getByClass("image-preview")[0];
                            imagePreview.innerHTML = '<img class="image-preview-display" src="'+ response.url +'"/>'


                        }
                    }
                });






            }
        }
    }

})