$(document).ready(function () {

    $('#btn-remove-approve').on('click', function(){
        var cf = confirm("Bạn có đồng ý hạ nội dung?");
        if(!cf){
            return false;
        }
    });



});

function approveComment(id, action) {

    switch (action) {

        case 'approve':
            var cf = confirm('Bạn có đồng ý phê duyệt?');
            if (!cf) return;
            break;
        case 'delete':
            var cf = confirm('Bạn có đồng ý không duyệt?');
            if (!cf) return;
            break;
    }

    $.ajax({
        method: "POST",
        url: $('#approve-comment-action').val(),
        data: {
            'id': id,
            'action': action
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.errorCode==200) {
                $('*[data-key=' + id + ']').replaceWith('<tr><td colspan="8" style="text-align:center"><strong>' +  trans('Hoàn thành xử lý') + '</strong></td></tr>');
            } else {
                alert(response.message);
            }


        },
        error: function (data) {

        }
    });
}


function approveVideo(id, action) {

    switch (action) {

        case 'approve':
            var cf = confirm('Bạn có đồng ý phê duyệt?');
            if (!cf) return;
            break;
        case 'disapprove':
            var cf = confirm('Bạn có đồng ý từ chối duyệt?');
            if (!cf) return;
            break;
    }

    $.ajax({
        method: "POST",
        url: $('#approve-action').val(),
        data: {
            'id': id,
            'action': action
        },
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.success) {
                $.pjax.reload({container:'#vt-video-list'});

            } else {
                alert(response.message);
            }


        },
        error: function (data) {

        }
    });

}


