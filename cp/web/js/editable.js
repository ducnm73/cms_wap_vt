// Some options to pass to the uploader are discussed on the next page

function callEditable(){
    $('.part-name').editable({
        params: function(params) {
            return {
                id: params.pk,
                name: params.value
            }
        },
    });
    $('.part-description').editable({

        params: function(params) {
            return {
                id: params.pk,
                description: params.value
            }
        },
        rows: 5
    });
}


$(document).ready(function() {
    callEditable();
});
