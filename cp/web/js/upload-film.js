// Some options to pass to the uploader are discussed on the next page


$(document).ready(function () {
    $("#film-part").sortable();

});

var uploader = new qq.FineUploader({
    element: document.getElementById("uploader"),
    validation: {
        allowedExtensions: ['mp4', 'flv', 'm4a'],
    },
    request: {
        endpoint: $('#upload-url').val(),
        params: {
            type: $('#upload-type').val(),
            playlist_id: $('#playlist-id').val(),
            '_csrf': yii.getCsrfToken()
        }
    },
    callbacks: {
        onComplete: function (id, name, response) {
            var serverPathToFile = response.filePath,
                fileItem = this.getItemByFileId(id);

            console.log(response);
            if (response.success) {
                fileItem.remove();
                $('#film-part').append(response.content);
                callEditable();
            }
        }
    }

})