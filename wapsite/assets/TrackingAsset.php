<?php

namespace wapsite\assets;

use yii\web\AssetBundle;

/**
 * Tracking asset bundle.
 */
class TrackingAsset extends AssetBundle
{
    public $js = [
        '/js/countly.min.js',
        '/js/myclip.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

}
