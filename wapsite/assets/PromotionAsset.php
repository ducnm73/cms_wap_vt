<?php

namespace wapsite\assets;

use yii\web\AssetBundle;

/**
 * Main wapsite application asset bundle.
 */
class PromotionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/promotion/css/reset.css?t=202102240941',
        '/promotion/css/style.css?t=202102240941',
        '/promotion/css/responsive.css?t=202102240941',
        '/promotion/css/coder-update.css?t=202102240941'
    ];
    public $js = [
        '/promotion/js/jquery-1.12.0.min.js?t=202102240941',
        '/promotion/js/main.js?t=202102240941',
        '/js/myclip.js?t=202102240941',
    ];

}
