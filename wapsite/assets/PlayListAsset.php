<?php

namespace wapsite\assets;

use yii\web\AssetBundle;

/**
 * Main wapsite application asset bundle.
 */
class PlayListAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/styles.css?t=202102241046503',
    ];
    public $js = [
        '/js/myclip.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
