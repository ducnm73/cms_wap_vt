<?php

namespace wapsite\assets;

use yii\web\AssetBundle;

/**
 * Main wapsite application asset bundle.
 */
class MapAccountAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/myclip.css?t=2503',
        'css/styles.css?t=202102241046503',
    ];
    public $js = [
        'js/jquery-3.2.1.min.js?t=2503',
        'js/scripts.js?t=2503',
        'js/myclip.js?t=2503',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
