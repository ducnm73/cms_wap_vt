<?php
namespace wapsite\models;

use common\models\VtSmartTvTokenBase;
use common\models\VtUserBase;
use Yii;
use yii\base\Model;
use common\helpers\Utils;

/**
 * Login form
 */
class ActivateForm extends Model
{
    public $code;
    public $countFail;
    public $countCaptchaShow;
    public $verifyCode; // add this varible to your model class.

    public function __construct(array $config)
    {
        $this->countFail = $config['countFail'];
        parent::__construct($config);
    }

    public function setCountFail($count)
    {
        $this->countFail = $count;
    }

    public function attributeLabels()
    {
        return [
            'code'=> Yii::t('web', 'Mã kích hoạt'),
            'verifyCode'=> Yii::t('wap', 'Mã xác nhận')

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->countFail > $this->countCaptchaShow) {
            return [
                [['code', 'verifyCode'], 'trim'],
                [['code'], 'required'],
                ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha'],
            ];
        } else {
            return [
                [['code'], 'trim'],
                [['code'], 'required'],

            ];
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateCode()
    {
        if (!$this->hasErrors()) {
           /*
            if (!$user || !$user->checkPassword($this->password)) {
                return false;
            } else {
                return true;
            }
           */
        }
        return false;
    }
}
