<?php
namespace wapsite\models;

use Yii;
use yii\base\Model;
use common\helpers\Utils;
use common\models\VtUserBase;

/**
 * Login form
 */
class MapAccountForm extends Model
{
    public $msisdn;
    public $otp; // add this varible to your model class.
    public $verifyCode;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'otp','verifyCode'], 'required'],
            ['msisdn', 'filter', 'filter' => [$this, 'trim']],
            ['msisdn', 'filter', 'filter' => [$this, 'format']],
            ['otp', 'filter', 'filter' => [$this, 'trim']],
            ['msisdn', 'match', 'pattern' => \Yii::$app->params['msisdn.regx']  , 'message' => Yii::t('app', 'Số thuê bao không hợp lệ')],
            ['verifyCode', 'filter', 'filter' => [$this, 'trim']],
            ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha']
        ];
    }

    public function attributeLabels()
    {
        return [
            'msisdn' => Yii::t('app', 'Số thuê bao'),
            'otp' => Yii::t('app', 'Mã OTP'),
            'verifyCode' => Yii::t('app', 'Mã xác nhận'),

        ];
    }

    public function format($msisdn)
    {
        return Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
    }

    public function trim($str)
    {
        return trim($str);
    }

}
