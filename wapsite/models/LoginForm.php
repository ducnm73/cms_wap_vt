<?php
namespace wapsite\models;

use common\models\VtUserBase;
use Yii;
use yii\base\Model;
use common\helpers\Utils;

/**
 * Login form
 */

class LoginForm extends Model
{
    public $username;
    public $password;
    
    private $_user;
    public $countFail;
    public $countCaptchaShow;
    public $verifyCode; // add this varible to your model class.

    public function __construct(array $config)
    {
        $this->countFail = $config['countFail'];
        parent::__construct($config);
    }

    public function setCountFail($count)
    {
        $this->countFail = $count;
    }

    public function attributeLabels()
    {
        return [
            'username'=>Yii::t('app', 'Số thuê bao'),
            'password'=>Yii::t('app', 'Mật khẩu'),
            'verifyCode'=>Yii::t('app', 'Mã xác nhận'),

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->countFail > $this->countCaptchaShow) {
            return [
//                [['username','password', 'verifyCode'], 'trim'],
                [['username'], 'required', 'message'=>Yii::t('wap', 'Số thuê bao không được để trống') ],
                [['password'], 'required', 'message'=>Yii::t('wap', 'Mật khẩu không được để trống') ],
                ['username', 'match', 'pattern' => \Yii::$app->params['msisdn.regx'], 'message'=>Yii::t('wap', 'Tài khoản hoặc mật khẩu đăng nhập không đúng') ],
                ['password', 'validatePassword'],
                ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha','message'=>Yii::t('wap','Mã captcha chưa đúng')],
            ];
        } else {
            return [
//                [['username','password'], 'trim'],
//                [['username', 'password'], 'required'],
                [['username'], 'required', 'message'=>Yii::t('wap', 'Số thuê bao không được để trống') ],
                [['password'], 'required', 'message'=>Yii::t('wap', 'Mật khẩu không được để trống') ],
                ['username', 'match', 'pattern' => \Yii::$app->params['msisdn.regx'] , 'message'=>Yii::t('wap', 'Tài khoản hoặc mật khẩu đăng nhập không đúng') ],
                ['password', 'validatePassword'],

            ];
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->checkPassword($this->password)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = VtUserBase::getByMsisdn(Utils::getMobileNumber($this->username, Utils::MOBILE_GLOBAL));
        }

        return $this->_user;
    }
}
