<?php
namespace wapsite\models;

use Yii;
use yii\base\Model;
use common\helpers\Utils;
use common\models\VtUserBase;

/**
 * Login form
 */
class ChangePasswordForm extends Model
{
    public $username;
    public $password;
    public $newPassword;
    public $repeatPassword;
    private $_user;
    public $verifyCode; // add this varible to your model class.

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $isEmptyPassword = Yii::$app->session->get("isEmptyPassword", 0);
        if ($isEmptyPassword) {
            return [
//                [['newPassword', 'repeatPassword', 'verifyCode'], 'required'],
                [['newPassword'], 'required', 'message' => Yii::t('web', 'New password can not be blank.')],
                [['repeatPassword'], 'required', 'message' => Yii::t('web', 'Repeat password can not be blank.')],
                [['password'], 'required', 'message' => Yii::t('web', 'Password can not be blank.')],
                [['verifyCode'], 'required', 'message' => Yii::t('web', 'Verify Code can not be blank.')],
                [['newPassword', 'repeatPassword', 'verifyCode'], 'filter', 'filter' => 'trim'],
                ['newPassword', 'string', 'min' => 8, 'max' => 128, 'tooShort' => Yii::t('wap','Mật khẩu mới từ 8 đến 128 kí tự.'), 'tooLong' => Yii::t('wap','Mật khẩu mới từ 8 đến 128 kí tự.')],
                ['newPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '!=', 'message' => Yii::t('web','Mật khẩu mới không được trùng mật khẩu cũ.')],
                ['repeatPassword', 'compare', 'compareAttribute' => 'newPassword', 'operator' => '==', 'message' => Yii::t('web','Xác nhận mật khẩu không đúng.')],
                ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha']
            ];
        }
        return [
//            [['password', 'newPassword', 'repeatPassword', 'verifyCode'], 'required'],
            [['newPassword'], 'required', 'message' => Yii::t('web', 'New password can not be blank.')],
            [['repeatPassword'], 'required', 'message' => Yii::t('web', 'Repeat password can not be blank.')],
            [['password'], 'required', 'message' => Yii::t('web', 'Password can not be blank.')],
            [['verifyCode'], 'required', 'message' => Yii::t('web', 'Verify Code can not be blank.')],
            [['newPassword', 'repeatPassword', 'verifyCode'], 'filter', 'filter' => 'trim'],
            ['password', 'validatePassword'],
            ['newPassword', 'string', 'min' => 8, 'max' => 128, 'tooShort' => Yii::t('web','Mật khẩu mới từ 8 đến 128 kí tự.'), 'tooLong' => Yii::t('wap','Mật khẩu mới từ 8 đến 128 kí tự.')],
            ['newPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '!=', 'message' => Yii::t('web','Mật khẩu mới không được trùng mật khẩu cũ.')],
            ['repeatPassword', 'compare', 'compareAttribute' => 'newPassword', 'operator' => '==', 'message' => Yii::t('web','Xác nhận mật khẩu không đúng.')],
            ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha']
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('wap', 'Mật khẩu cũ'),
            'newPassword' => Yii::t('wap', 'Mật khẩu mới'),
            'repeatPassword' => Yii::t('wap', 'Xác nhận mật khẩu'),
            'verifyCode' => Yii::t('wap', 'Mã xác nhận'),

        ];
    }

    /**
     * @return bool
     * @throws sfException
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->checkPassword($this->password)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = VtUserBase::getByMsisdn($this->username);
        }
        return $this->_user;
    }
}
