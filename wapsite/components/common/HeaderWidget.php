<?php

/**
 * Created by PhpStorm.
 * User: KhanhDQ
 * Date: 09/26/2017
 * Time: 2:08 PM
 */

namespace wapsite\components\common;

use common\modules\v2\controllers\AccountController;
use yii\base\Widget;
use yii;

class HeaderWidget extends Widget {

    public $type;
    public $title;

    public function init() {
        parent::init();
    }

    public function run() {

        $referUrl = Yii::$app->request->referrer;
        if (!$referUrl || $referUrl == Yii::$app->request->absoluteUrl) {
            $referUrl = '/';
        }

        $sessionUrl = Yii::$app->session->get("backLink","/");

        if( ($referUrl != $sessionUrl) && $referUrl != '/' && (Yii::$app->request->absoluteUrl != $sessionUrl) ){
            \Yii::$app->session->set("backLink",$referUrl );
        }
        $referUrl= Yii::$app->session->get("backLink","/");

        //Neu la trang goi cuoc thi quay lai la trang ca nhan
        if(\Yii::$app->controller->getRoute()=='default/list-package'){
            $referUrl="/ca-nhan";
        }

        $strId = \Yii::$app->request->get("id");
        $arrId = explode("_", $strId);
        $id = (count($arrId) > 0) ? $arrId[count($arrId) - 1] : 0;
        $route = \Yii::$app->controller->getRoute();

        if(!$id){
            $id = \Yii::$app->request->get("channel_id");
        }

        return $this->render('@wapsite/views/partials/header.twig', ['route' => $route, 'type' => $this->type, 'title' => $this->title, 'referUrl' => $referUrl, 'id' => $id]);
    }

}
