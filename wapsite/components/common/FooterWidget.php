<?php

/**
 * Created by PhpStorm.
 * User: KhanhDQ
 * Date: 09/26/2017
 * Time: 2:08 PM
 */

namespace wapsite\components\common;

use yii\base\Widget;

class FooterWidget extends Widget {

    public $message;

    public function init() {
        parent::init();
    }

    public function run() {
        $route = \Yii::$app->controller->getRoute();
     
        return $this->render('@wapsite/views/partials/footer.twig', ['route' => $route, "popupNoUpload"=>\Yii::$app->params['msg.popup.noupload']]);
    }

}
