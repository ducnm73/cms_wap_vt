<?php

/**
 * Created by JetBrains PhpStorm.
 * User: HUYNC2
 * Date: 10/17/15
 * Time: 9:24 AM
 * To change this template use File | Settings | File Templates.
 */

namespace wapsite\components;

use common\helpers\Utils;
use Imagine\Image\Box;
use Imagine\Imagick\Imagine;
use common\helpers\MusicHelper;
use common\libs\Constant;
use Exception;
use Twig_SimpleFunction;
use wap\models\VtSong;
use wap\models\VtVideo;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtmlPurifier;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\twig\Extension;

class WapExtension extends Extension
{
//    public function init()
//    {
//
//        $Yii = new ETwigViewRendererStaticClassProxy ('Yii');
//        $this->_twig-> addGlobal ('nhntm', $Yii);
//        parent::init();
//    }

    public function getName()
    {
        return "WapExtension";
    }

    public function getFunctions()
    {
        return ArrayHelper::merge(array(
//            new Twig_SimpleFunction('ajax_url', array($this, 'ajax_url')),
//            new Twig_SimpleFunction('prefixJavascript', array($this, 'prefixJavascript'))
        ), parent::getFunctions());
    }

    public function getFilters()
    {
        return ArrayHelper::merge(array(

            new \Twig_SimpleFilter('urlGenerate', array($this, 'urlGenerate')),
            new \Twig_SimpleFilter('dateAgo', array($this, 'dateAgo')),
            new \Twig_SimpleFilter('var_dump', array($this, 'var_dump')),
            new \Twig_SimpleFilter('convertDay', array($this, 'convertDay')),
            new \Twig_SimpleFilter('lang', array($this, 'lang')),


        ), parent::getFilters());
    }

    /*Định nghĩa hàm Yii::t cho twig
    public function lang($mess) {
        switch (Yii::$app->id){
            case "app-wap":
                return Yii::t('wap',$mess);
        }
    }
    Định nghĩa hàm Yii::t cho twig - Viết ở bên trang twig: {{'Trang chủ' | lang }}*/



    public function var_dump($val, $die = false)
    {
        var_dump($val);
        if ($die) {
            die($die);
        }
    }

    /**
     * Generate link trang chi tiet theo loai doi tuong
     * @author ducda2@viettel.com.vn
     * @param $item
     * @return string
     */
    public function urlGenerate($item)
    {
        $type = isset($item['type']) ? $item['type'] : '';

        switch ($type) {
            case 'VOD':
                return Url::to(['video/get-detail', 'id' => $item['id'], 'slug' => $item['slug']]);
            case 'FILM':
                return Url::to(['film/get-detail', 'id' => $item['id'], 'slug' => $item['slug']]);
            case 'LIVETV':
                return Url::to(['channel/get-detail', 'id' => $item['id'], 'slug' => $item['slug']]);
            default:
                return '';
        }

    }

    /**
     * Convert thoi gian sang dang: 4 ngay truoc, vua xong ...
     * @author ducda2@viettel.com.vn
     * @param $value
     * @return string
     */
    public function dateAgo($value){
        return Utils::time_elapsed_string($value);
    }

    public function convertDay($cycle){
        return Utils::convertDay($cycle);
    }
}
