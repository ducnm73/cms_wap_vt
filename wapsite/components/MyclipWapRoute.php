<?php

namespace wapsite\components;

use common\models\VtShortLinkBase;
use yii\base\BootstrapInterface;

class MyclipWapRoute implements BootstrapInterface
{
    public function bootstrap($app)
    {

        $routeShortLink = VtShortLinkBase::getRoutes();

        foreach ($routeShortLink as $row) {

            if ($row->route == "REDIRECT") {
                $routeItem["pattern"] = $row->short_link;
                $routeItem["route"] = "/site/redirect";
                $routeItem["defaults"] = ["link" => $row->full_link];
            } else {
                $routeItem["pattern"] = $row->short_link;
                $routeItem["route"] = $row->route;
                //id=1397998&utm_source=GA1&popup=1
                $defaults = [];
                foreach (explode("&", $row->params) as $p) {
                    $apr = explode("=", $p);
                    $defaults[$apr[0]] = $apr[1];
                };
                $routeItem["defaults"] = $defaults;
            }

            $routeArray[] = $routeItem;
        }

        if ($routeArray) {
            $app->urlManager->addRules($routeArray);// Append new rules to original rules
        }


    }
}

