<?php

/* @app/views/partials/commentBox.twig */
class __TwigTemplate_f66c6936fe2666e4fce09f820a49de1895881c755a366aedad67215fc5fec9f4 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"blk blkBdTop blkComments\">
    <input type=\"hidden\" id=\"commentUrl\" value=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/list-comment"), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"toggleLikeUrl\" value=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/like-video"), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"toggleLikeComment\" value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/like-comment"), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"postCommentUrl\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/comment"), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"contentId\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["contentId"]) ? $context["contentId"] : null), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"commentType\" value=\"";
        // line 7
        if (((isset($context["type"]) ? $context["type"] : null) == "FILM")) {
            echo " PLAYLIST ";
        } else {
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
        }
        echo "\">
    <input type=\"hidden\" id=\"replyCommentId\" value=\"0\">
    <input type=\"hidden\" id=\"commentOffset\" value=\"0\">
    <input type=\"hidden\" id=\"commentLimit\" value=\"20\">
    <div class=\"loadingBoxComment\">
        <h6 class=\"blkTitle\" id=\"titleComment\">";
        // line 12
        echo twig_escape_filter($this->env, Yii::t("wap", "Bình luận"), "html", null, true);
        echo "</h6>
        <section class=\"cmtInput ctn ";
        // line 13
        if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "can_comment", array()) == 0)) {
            echo " hidden";
        }
        echo "\">
            <div class=\"avatar pthld\"><a href=\"javascript:void(0)\"><img src=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["avatarImage"]) ? $context["avatarImage"] : null), "html", null, true);
        echo "\" alt=\"\"></a></div>
            <div class=\"inthld\">
                <textarea name=\"\" id=\"inputComment\" class=\"ipt padding0 iptLg\" placeholder=\"";
        // line 16
        echo twig_escape_filter($this->env, Yii::t("wap", "Thêm bình luận..."), "html", null, true);
        echo "\"></textarea>
            </div>
            <div class=\"btnCommentBox right\" id=\"btnCommentBox\">
";
        // line 20
        echo "                <input type=\"button\" class=\"clearComment\"  value=\"";
        echo twig_escape_filter($this->env, Yii::t("web", "Hủy"), "html", null, true);
        echo "\" onclick=\"return addEvent('comment_video_action', \$('#btnCommentBox').hide());\">

                <a href=\"javascript:void(0)\" class=\"btnComment\" data-type=\"VOD\" data-parentid=\"0\" data-videoid=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["contentId"]) ? $context["contentId"] : null), "html", null, true);
        echo "\"
                   onclick=\"return addEvent('comment_video_action',  {'video_id': '";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "', 'video_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "', 'channel_id': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "', 'channel_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "', 'comment_content ': \$('#inputComment').val() });\">
                    ";
        // line 24
        echo twig_escape_filter($this->env, Yii::t("wap", "Bình luận"), "html", null, true);
        echo "
                </a>
            </div>
        </section> 
        <section class=\"blkBdTop cmtList\" id=\"boxNoComment\" style=\"display:none;\">
            <h6 class=\"blkTitle\">";
        // line 29
        echo twig_escape_filter($this->env, Yii::t("wap", "Chưa có bình luận."), "html", null, true);
        echo "</h6>
        </section>         
        <section class=\"blkBdTop cmtList\" id=\"commentContent\">
        </section>  
        <div class=\"spinner\" id=\"loading-comment\" style=\"display:none;\">
            <div class=\"quarter\">
                <div class=\"circle\"></div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "@app/views/partials/commentBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 29,  95 => 24,  85 => 23,  81 => 22,  75 => 20,  69 => 16,  64 => 14,  58 => 13,  54 => 12,  42 => 7,  38 => 6,  34 => 5,  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <section class="blk blkBdTop blkComments">*/
/*     <input type="hidden" id="commentUrl" value="{{ url('default/list-comment') }}">*/
/*     <input type="hidden" id="toggleLikeUrl" value="{{ url('video/like-video') }}">*/
/*     <input type="hidden" id="toggleLikeComment" value="{{ url('default/like-comment') }}">*/
/*     <input type="hidden" id="postCommentUrl" value="{{ url('default/comment') }}">*/
/*     <input type="hidden" id="contentId" value="{{ contentId }}">*/
/*     <input type="hidden" id="commentType" value="{% if type == 'FILM' %} PLAYLIST {% else %}{{type}}{% endif %}">*/
/*     <input type="hidden" id="replyCommentId" value="0">*/
/*     <input type="hidden" id="commentOffset" value="0">*/
/*     <input type="hidden" id="commentLimit" value="20">*/
/*     <div class="loadingBoxComment">*/
/*         <h6 class="blkTitle" id="titleComment">{{t('wap', 'Bình luận') }}</h6>*/
/*         <section class="cmtInput ctn {% if detail.can_comment == 0 %} hidden{% endif %}">*/
/*             <div class="avatar pthld"><a href="javascript:void(0)"><img src="{{ avatarImage }}" alt=""></a></div>*/
/*             <div class="inthld">*/
/*                 <textarea name="" id="inputComment" class="ipt padding0 iptLg" placeholder="{{t('wap', 'Thêm bình luận...') }}"></textarea>*/
/*             </div>*/
/*             <div class="btnCommentBox right" id="btnCommentBox">*/
/* {#                <a href="javascript:void(0)" class="clearComment">{{t('wap', 'Hủy') }}</a>#}*/
/*                 <input type="button" class="clearComment"  value="{{t('web','Hủy')}}" onclick="return addEvent('comment_video_action', $('#btnCommentBox').hide());">*/
/* */
/*                 <a href="javascript:void(0)" class="btnComment" data-type="VOD" data-parentid="0" data-videoid="{{contentId}}"*/
/*                    onclick="return addEvent('comment_video_action',  {'video_id': '{{ detail.id }}', 'video_name': '{{ detail.name }}', 'channel_id': '{{ detail.owner.id }}', 'channel_name': '{{ detail.owner.name }}', 'comment_content ': $('#inputComment').val() });">*/
/*                     {{t('wap', 'Bình luận') }}*/
/*                 </a>*/
/*             </div>*/
/*         </section> */
/*         <section class="blkBdTop cmtList" id="boxNoComment" style="display:none;">*/
/*             <h6 class="blkTitle">{{t('wap', 'Chưa có bình luận.') }}</h6>*/
/*         </section>         */
/*         <section class="blkBdTop cmtList" id="commentContent">*/
/*         </section>  */
/*         <div class="spinner" id="loading-comment" style="display:none;">*/
/*             <div class="quarter">*/
/*                 <div class="circle"></div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
