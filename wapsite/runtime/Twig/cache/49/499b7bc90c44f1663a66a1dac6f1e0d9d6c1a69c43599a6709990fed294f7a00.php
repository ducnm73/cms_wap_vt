<?php

/* video_channel_follow.twig */
class __TwigTemplate_e0ffc277616ca6ca7d32153346d1f071562db33a5e890a34cad3d5fd7b9c29ee extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/HomeAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "home_asset");
        echo "
";
        // line 3
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "main", "title" => (isset($context["title"]) ? $context["title"] : null))), "method");
        echo "
<main>
    <section class=\"blk blkListVideos blkPadBoth\">
        <input type=\"hidden\" id=\"homeItemLimit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
        echo "\">
        <input type=\"hidden\" id=\"getMoreOffset\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
        echo "\">

        <input type=\"hidden\" id=\"load-more-id\" value=\"video_channel_follow\"/>

        ";
        // line 11
        if ( !twig_test_empty($this->getAttribute($this->getAttribute((isset($context["listFollowChannel"]) ? $context["listFollowChannel"] : null), "data", array()), "content", array()))) {
            // line 12
            echo "            <section class=\"blk blkFollow\">
                <div class=\"ctn\">
                    <a href=\"/danh-sach-theo-doi\" class=\"more\">";
            // line 14
            echo twig_escape_filter($this->env, Yii::t("wap", "Xem tất cả"), "html", null, true);
            echo "</a>
                    <ul id=\"listFollowChannel\">
                        ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["listFollowChannel"]) ? $context["listFollowChannel"] : null), "data", array()), "content", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 17
                echo "                            <li>
                                <div class=\"avatar\">
                                    <a href=\"channel/";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "channel_id", array()), "html", null, true);
                echo "\">
                                        <img src=\"";
                // line 20
                if (($this->getAttribute($context["item"], "avatarImage", array()) == "")) {
                    echo " ../images/dfphoto.png ";
                } else {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "avatarImage", array()), "html", null, true);
                }
                echo "\"/></a>
                                </div>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "                    </ul>
                </div>
            </section>

        ";
        }
        // line 29
        echo "        <div class=\"ctn\">
            ";
        // line 30
        $this->loadTemplate("@app/views/partials/videoBox.twig", "video_channel_follow.twig", 30)->display(array_merge($context, array("data" => $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()), "hasTitle" => true, "hasArrow" => false)));
        // line 31
        echo "            <div id=\"container\">
            </div>
        </div>
    </section>
</main>
";
    }

    public function getTemplateName()
    {
        return "video_channel_follow.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 31,  92 => 30,  89 => 29,  82 => 24,  67 => 20,  63 => 19,  59 => 17,  55 => 16,  50 => 14,  46 => 12,  44 => 11,  37 => 7,  33 => 6,  27 => 3,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/HomeAsset') }}*/
/* {{ register_home_asset() }}*/
/* {{ HeaderWidget.widget({'type':'main','title':title}) | raw }}*/
/* <main>*/
/*     <section class="blk blkListVideos blkPadBoth">*/
/*         <input type="hidden" id="homeItemLimit" value="{{ app.params['app.home.limit'] }}">*/
/*         <input type="hidden" id="getMoreOffset" value="{{ app.params['app.home.limit'] }}">*/
/* */
/*         <input type="hidden" id="load-more-id" value="video_channel_follow"/>*/
/* */
/*         {% if not listFollowChannel.data.content is empty %}*/
/*             <section class="blk blkFollow">*/
/*                 <div class="ctn">*/
/*                     <a href="/danh-sach-theo-doi" class="more">{{t('wap', 'Xem tất cả') }}</a>*/
/*                     <ul id="listFollowChannel">*/
/*                         {% for item in listFollowChannel.data.content %}*/
/*                             <li>*/
/*                                 <div class="avatar">*/
/*                                     <a href="channel/{{ item.channel_id }}">*/
/*                                         <img src="{% if item.avatarImage == '' %} ../images/dfphoto.png {% else %} {{ item.avatarImage }}{% endif %}"/></a>*/
/*                                 </div>*/
/*                             </li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/*             </section>*/
/* */
/*         {% endif %}*/
/*         <div class="ctn">*/
/*             {% include "@app/views/partials/videoBox.twig" with {'data': responseData.data, 'hasTitle': true, 'hasArrow': false} %}*/
/*             <div id="container">*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* </main>*/
/* */
