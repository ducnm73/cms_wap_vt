<?php

/* @app/views/partials/videoOptionModal.twig */
class __TwigTemplate_6db046fce50798924d829396ed593f2838e395c5aedd9ab4a6281e410f0ea7ab extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal modalOptMn\" id=\"videoOpt\">
    <ul>
        <li>
            <a href=\"javascript:void(0);\" class=\"btnSeeLater\"><svg class=\"ico\"><use xlink:href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-later"), "html", null, true);
        echo "\"></use></svg>";
        echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào xem sau"), "html", null, true);
        echo "</a>
        </li>
        <li>
            <a   ";
        // line 7
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "userId"), "method")) {
            echo " href=\"javascript:void(0);\" class=\"modalBtn btnCreatePlaylist\" data-target=\"#createPlaylistModal\" data-opt=\"clearAll\" ";
        } else {
            echo " href=\"/auth/login\" class=\"btnCreatePlaylist\" ";
        }
        echo "  ><svg class=\"ico\"><use xlink:href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-addto"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Tạo danh sách phát"), "html", null, true);
        echo "</a>
        </li>
        <li>
            <a href=\"javascript:void(0);\" class=\"modalBtn btnLoadMyPlaylist\" data-opt=\"clearAll\" data-target=\"#playlistPop\"><svg class=\"ico\"><use xlink:href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-playlist"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào danh sách phát"), "html", null, true);
        echo "</a>
        </li>
";
        // line 15
        echo "        <li>
            <a href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
        echo "</a>
        </li>
    </ul>
</div>
<div class=\"modal modalOptMn\" id=\"playlistOpt\">
    <ul>
        <li>
            <a href=\"javascript:void(0);\" class=\"btnSeeLater\"><svg class=\"ico\"><use xlink:href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-later"), "html", null, true);
        echo "\"></use></svg>";
        echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào xem sau"), "html", null, true);
        echo "</a>
        </li>
        <li>
            <a href=\"javascript:void(0);\" class=\"modalBtn btnCreatePlaylist\" data-target=\"#createPlaylistModal\" data-opt=\"clearAll\"><svg class=\"ico\"><use xlink:href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-addto"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Tạo danh sách phát"), "html", null, true);
        echo "</a>
        </li>
        <li>
            <a href=\"javascript:void(0);\" class=\"modalBtn btnLoadMyPlaylist\" data-opt=\"clearAll\" data-target=\"#playlistPop\"><svg class=\"ico\"><use xlink:href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-playlist"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào danh sách phát"), "html", null, true);
        echo "</a>
        </li>
        <li>
            <a href=\"javascript:void(0);\" id =\"btnDelete\" onclick=\"hideModal()\"><svg class=\"ico\"><use xlink:href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-delete"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Xóa video khỏi playlist"), "html", null, true);
        echo "</a>
        </li>
";
        // line 37
        echo "        <li>
            <a href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
        echo "\"></use></svg> ";
        echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
        echo "</a>
        </li>
    </ul>
</div>
<div class=\"overlay\"></div>
<input type=\"hidden\" id=\"idVideoClickOption\" value=\"\"/>
<input type=\"hidden\" id=\"urlShare\" value=\"\"/>

";
        // line 46
        if ( !$this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "userId"), "method")) {
            echo "  ";
        }
    }

    public function getTemplateName()
    {
        return "@app/views/partials/videoOptionModal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 46,  102 => 38,  99 => 37,  92 => 32,  84 => 29,  76 => 26,  68 => 23,  56 => 16,  53 => 15,  46 => 10,  32 => 7,  24 => 4,  19 => 1,);
    }
}
/* <div class="modal modalOptMn" id="videoOpt">*/
/*     <ul>*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="btnSeeLater"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-later') }}"></use></svg>{{t('wap', 'Thêm vào xem sau') }}</a>*/
/*         </li>*/
/*         <li>*/
/*             <a   {% if app.session.get('userId') %} href="javascript:void(0);" class="modalBtn btnCreatePlaylist" data-target="#createPlaylistModal" data-opt="clearAll" {% else %} href="/auth/login" class="btnCreatePlaylist" {% endif %}  ><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-addto') }}"></use></svg> {{t('wap', 'Tạo danh sách phát') }}</a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="modalBtn btnLoadMyPlaylist" data-opt="clearAll" data-target="#playlistPop"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-playlist') }}"></use></svg> {{t('wap', 'Thêm vào danh sách phát') }}</a>*/
/*         </li>*/
/* {#        <li>#}*/
/* {#            <a href="javascript:void(0);" class="modalBtn" data-opt="clearAll" data-target="#popupShare"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-shareto') }}"></use></svg> {{t('wap', 'Chia sẻ') }}</a>#}*/
/* {#        </li>#}*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*         </li>*/
/*     </ul>*/
/* </div>*/
/* <div class="modal modalOptMn" id="playlistOpt">*/
/*     <ul>*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="btnSeeLater"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-later') }}"></use></svg>{{t('wap', 'Thêm vào xem sau') }}</a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="modalBtn btnCreatePlaylist" data-target="#createPlaylistModal" data-opt="clearAll"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-addto') }}"></use></svg> {{t('wap', 'Tạo danh sách phát') }}</a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="modalBtn btnLoadMyPlaylist" data-opt="clearAll" data-target="#playlistPop"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-playlist') }}"></use></svg> {{t('wap', 'Thêm vào danh sách phát') }}</a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="javascript:void(0);" id ="btnDelete" onclick="hideModal()"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-delete') }}"></use></svg> {{t('wap', 'Xóa video khỏi playlist') }}</a>*/
/*         </li>*/
/* {#        <li>#}*/
/* {#            <a href="javascript:void(0);" class="modalBtn" data-opt="clearAll" data-target="#popupShare"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-shareto') }}"></use></svg> {{t('wap', 'Chia sẻ') }}</a>#}*/
/* {#        </li>#}*/
/*         <li>*/
/*             <a href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*         </li>*/
/*     </ul>*/
/* </div>*/
/* <div class="overlay"></div>*/
/* <input type="hidden" id="idVideoClickOption" value=""/>*/
/* <input type="hidden" id="urlShare" value=""/>*/
/* */
/* {% if not app.session.get('userId') %}  {% endif %}*/
