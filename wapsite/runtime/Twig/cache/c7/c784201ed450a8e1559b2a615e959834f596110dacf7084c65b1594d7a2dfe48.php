<?php

/* mainDetail.twig */
class __TwigTemplate_cebe292cde76f67efda54db1b14ef1bd57f1c5ac8c1ada9bcabde84bd283feef extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginPage", array(), "method"), "html", null, true);
        echo "

<!DOCTYPE html>
<html lang=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()), "html", null, true);
        echo "\">
<head>
    <meta charset=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "charset", array()), "html", null, true);
        echo "\">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>


    ";
        // line 10
        echo $this->getAttribute((isset($context["html"]) ? $context["html"] : null), "csrfMetaTags", array(), "method");
        echo "
    <title>";
        // line 11
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())) ? ($this->getAttribute((isset($context["html"]) ? $context["html"] : null), "encode", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())), "method")) : (Yii::t("wap", "Mạng xã hội video - Myclip"))), "html", null, true);
        echo "</title>
\t<link type=\"image/x-icon\" rel=\"shortcut icon\" href=\"/favicon.ico\">
    ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "head", array(), "method"), "html", null, true);
        echo "
    ";
        // line 14
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "mainDetail.twig", 14)->display(array_merge($context, array("pos" => "head")));
        // line 15
        echo "
</head>
<body class=\"";
        // line 17
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "themeUserId"), "method") == 1)) {
            echo "dark ";
        }
        echo "\">
";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginBody", array(), "method"), "html", null, true);
        echo "
    ";
        // line 19
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "mainDetail.twig", 19)->display(array_merge($context, array("pos" => "body")));
        // line 20
        echo "    <main>
        ";
        // line 21
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "
    ";
        // line 22
        $this->loadTemplate("@app/views/partials/playlistOptionModal.twig", "mainDetail.twig", 22)->display($context);
        // line 23
        echo "    ";
        $this->loadTemplate("@app/views/partials/createPlayListModal.twig", "mainDetail.twig", 23)->display($context);
        // line 24
        echo "    ";
        $this->loadTemplate("@app/views/partials/videoOptionModal.twig", "mainDetail.twig", 24)->display($context);
        // line 25
        echo "    ";
        echo $this->getAttribute((isset($context["Alert"]) ? $context["Alert"] : null), "widget", array(), "method");
        echo "

    ";
        // line 27
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "distributionId"), "method") <= 0)) {
            // line 28
            echo "       ";
            echo $this->getAttribute((isset($context["FooterWidget"]) ? $context["FooterWidget"] : null), "widget", array(), "method");
            echo "
    ";
        }
        // line 30
        echo "\t
    <div id=\"loadMyPlaylist\"></div>
    <input type=\"hidden\" id=\"userId\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "userId"), "method"), "html", null, true);
        echo "\">
";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endBody", array(), "method"), "html", null, true);
        echo "
<!--[if IE]>
<script>svg4everybody();</script>
<!--<![endif]-->
";
        // line 37
        $this->loadTemplate("@app/views/partials/tracking.twig", "mainDetail.twig", 37)->display($context);
        // line 38
        echo $this->getAttribute((isset($context["TrackingWidget"]) ? $context["TrackingWidget"] : null), "widget", array(), "method");
        echo "
 </main>

</body>
</html>
";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endPage", array(), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "mainDetail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 43,  114 => 38,  112 => 37,  105 => 33,  101 => 32,  97 => 30,  91 => 28,  89 => 27,  83 => 25,  80 => 24,  77 => 23,  75 => 22,  71 => 21,  68 => 20,  66 => 19,  62 => 18,  56 => 17,  52 => 15,  50 => 14,  46 => 13,  41 => 11,  37 => 10,  30 => 6,  25 => 4,  19 => 1,);
    }
}
/* {{ this.beginPage() }}*/
/* */
/* <!DOCTYPE html>*/
/* <html lang="{{ app.language }}">*/
/* <head>*/
/*     <meta charset="{{ app.charset }}">*/
/*     <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>*/
/* */
/* */
/*     {{ html.csrfMetaTags() | raw }}*/
/*     <title>{{ (this.title)?html.encode(this.title):t('wap', 'Mạng xã hội video - Myclip') }}</title>*/
/* 	<link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">*/
/*     {{ this.head() }}*/
/*     {% include '@app/views/partials/trackingHead.twig' with {'pos': 'head'} %}*/
/* */
/* </head>*/
/* <body class="{% if app.session.get('themeUserId') == 1 %}dark {% endif %}">*/
/* {{ this.beginBody() }}*/
/*     {% include '@app/views/partials/trackingHead.twig' with {'pos': 'body'} %}*/
/*     <main>*/
/*         {{ content | raw }}*/
/*     {% include '@app/views/partials/playlistOptionModal.twig' %}*/
/*     {% include '@app/views/partials/createPlayListModal.twig' %}*/
/*     {% include '@app/views/partials/videoOptionModal.twig' %}*/
/*     {{ Alert.widget() | raw }}*/
/* */
/*     {% if app.session.get('distributionId') <= 0 %}*/
/*        {{ FooterWidget.widget() | raw }}*/
/*     {% endif %}*/
/* 	*/
/*     <div id="loadMyPlaylist"></div>*/
/*     <input type="hidden" id="userId" value="{{ app.session.get('userId') }}">*/
/* {{ this.endBody() }}*/
/* <!--[if IE]>*/
/* <script>svg4everybody();</script>*/
/* <!--<![endif]-->*/
/* {% include '@app/views/partials/tracking.twig' %}*/
/* {{ TrackingWidget.widget() | raw }}*/
/*  </main>*/
/* */
/* </body>*/
/* </html>*/
/* {{ this.endPage() }}*/
/* */
