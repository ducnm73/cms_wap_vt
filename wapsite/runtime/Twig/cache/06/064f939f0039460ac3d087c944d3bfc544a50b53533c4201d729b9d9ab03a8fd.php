<?php

/* @app/views/partials/shareSocialModal.twig */
class __TwigTemplate_32a83759d6a1c4455326891e6365d59ea324e00f76e7750df3918c48b43f7456 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal modalPopup classShareSocial\" id=\"popupShare\">
    <div class=\"ctn\">
        <h5>";
        // line 3
        echo twig_escape_filter($this->env, Yii::t("wap", "Chia sẻ"), "html", null, true);
        echo "
            <a style=\"float: right; color: black; margin-right: -25px; margin-top: -15px;\" class=\"close\">
                <svg class=\"ico\">
                    <use xlink:href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
        echo "\"></use>
                </svg>
            </a>
        </h5>
        <div class=\"text-center\">
            <div class=\"share\">
                <a href=\"javascript:void(0)\" pref=\"https://facebook.com/sharer/sharer.php?u=";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["shareLink"]) ? $context["shareLink"] : null), "html", null, true);
        echo "\"
                   target=\"_blank\"
                   aria-label=\"Share on Facebook\"
                   class=\"close-popup-share btnFbShare shareMe\"
                   onclick=\"return addEvent('share_video_action', {'video_id': '";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "', 'video_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "', 'channel_id': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "', 'channel_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "', 'share_type': 'Facebook' });\">
                    <img src=\"/images/facebook.png\">
                    <p>Facebook</p>
                </a>

                <a href=\"javascript:void(0)\" pref=\"https://plus.google.com/share?url=";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["shareLink"]) ? $context["shareLink"] : null), "html", null, true);
        echo "\" target=\"_blank\"
                   aria-label=\"Share on Google+\"
                   class=\"close-popup-share btnGgShare shareMe\"
                   onclick=\"return addEvent('share_video_action', {'video_id': '";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "', 'video_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "', 'channel_id': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "', 'channel_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "', 'share_type': 'Google' });\">
                    <img src=\"/images/google-plus.png\">
                    <p>Google+</p>
                </a>
                
            </div>
        </div>
        <!--
        <div class=\"text-center\">
            <p>Áp dụng 1000 lượt chia sẻ facebook đầu tiên. Chi tiết <a style=\"color: blue\" href=\"/event/terms-condition\"> TẠI ĐÂY</a>            </p>
        </div>
        -->
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "@app/views/partials/shareSocialModal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 24,  59 => 21,  45 => 16,  38 => 12,  29 => 6,  23 => 3,  19 => 1,);
    }
}
/* <div class="modal modalPopup classShareSocial" id="popupShare">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'Chia sẻ') }}*/
/*             <a style="float: right; color: black; margin-right: -25px; margin-top: -15px;" class="close">*/
/*                 <svg class="ico">*/
/*                     <use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use>*/
/*                 </svg>*/
/*             </a>*/
/*         </h5>*/
/*         <div class="text-center">*/
/*             <div class="share">*/
/*                 <a href="javascript:void(0)" pref="https://facebook.com/sharer/sharer.php?u={{ shareLink }}"*/
/*                    target="_blank"*/
/*                    aria-label="Share on Facebook"*/
/*                    class="close-popup-share btnFbShare shareMe"*/
/*                    onclick="return addEvent('share_video_action', {'video_id': '{{ detail.id }}', 'video_name': '{{ detail.name }}', 'channel_id': '{{ detail.owner.id }}', 'channel_name': '{{ detail.owner.name }}', 'share_type': 'Facebook' });">*/
/*                     <img src="/images/facebook.png">*/
/*                     <p>Facebook</p>*/
/*                 </a>*/
/* */
/*                 <a href="javascript:void(0)" pref="https://plus.google.com/share?url={{ shareLink }}" target="_blank"*/
/*                    aria-label="Share on Google+"*/
/*                    class="close-popup-share btnGgShare shareMe"*/
/*                    onclick="return addEvent('share_video_action', {'video_id': '{{ detail.id }}', 'video_name': '{{ detail.name }}', 'channel_id': '{{ detail.owner.id }}', 'channel_name': '{{ detail.owner.name }}', 'share_type': 'Google' });">*/
/*                     <img src="/images/google-plus.png">*/
/*                     <p>Google+</p>*/
/*                 </a>*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <!--*/
/*         <div class="text-center">*/
/*             <p>Áp dụng 1000 lượt chia sẻ facebook đầu tiên. Chi tiết <a style="color: blue" href="/event/terms-condition"> TẠI ĐÂY</a>            </p>*/
/*         </div>*/
/*         -->*/
/*     </div>*/
/* </div>*/
