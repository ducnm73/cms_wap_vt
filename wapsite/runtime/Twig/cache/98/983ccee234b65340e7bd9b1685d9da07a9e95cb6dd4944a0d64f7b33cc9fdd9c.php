<?php

/* @app/views/partials/playlistOptionModal.twig */
class __TwigTemplate_b2c8da9f565dc4c580232e403e4d7d0f0b444538c4c253a4c2ca33e93c580a7a extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["type"]) ? $context["type"] : null) == true)) {
            // line 2
            echo "    <div class=\"modal modalOptMn\" id=\"cmtOpt\">
        <ul>
            <li>
                <a href=\"javascript:void(0);\" class=\"btnDelete\"><svg class=\"ico\"><use xlink:href=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-delete"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Xóa playlist"), "html", null, true);
            echo "</a>
            </li>
            <li>
                <a href=\"javascript:void(0);\" class=\"modalBtn btnEdit\"><svg class=\"ico\"><use xlink:href=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-edit"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa playlist"), "html", null, true);
            echo "</a>
            </li>
            <li>
                <a href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
            echo "</a>
            </li>
        </ul>
    </div>
";
        } else {
            // line 16
            echo "    <div class=\"modal modalOptMn\" id=\"playlistOpt\">
        <ul>
            <li>
                <a href=\"javascript:void(0);\" class=\"btnSeeLater\"><svg class=\"ico\"><use xlink:href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-later"), "html", null, true);
            echo "\"></use></svg>";
            echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào xem sau"), "html", null, true);
            echo "</a>
            </li>
            <li>
                <a href=\"javascript:void(0)\" class=\"modalBtn btnCreatePlaylist\" data-target=\"#createPlaylistModal\"><svg class=\"ico\"><use xlink:href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-addto"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Tạo danh sách phát mới"), "html", null, true);
            echo "</a>
            </li>
            <li>
                <a href=\"javascript:void(0)\"class=\"modalBtn btnLoadMyPlaylist\" data-target=\"#playlistPop\" data-opt=\"clearAll\"><svg class=\"ico\"><use xlink:href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-playlist"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào danh sách phát"), "html", null, true);
            echo "</a>
            </li>
            <li>
                <a class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
            echo "</a>
            </li>
        </ul>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@app/views/partials/playlistOptionModal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 28,  73 => 25,  65 => 22,  57 => 19,  52 => 16,  42 => 11,  34 => 8,  26 => 5,  21 => 2,  19 => 1,);
    }
}
/* {% if type == true %}*/
/*     <div class="modal modalOptMn" id="cmtOpt">*/
/*         <ul>*/
/*             <li>*/
/*                 <a href="javascript:void(0);" class="btnDelete"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-delete') }}"></use></svg> {{t('wap', 'Xóa playlist') }}</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="javascript:void(0);" class="modalBtn btnEdit"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-edit') }}"></use></svg> {{t('wap', 'Chỉnh sửa playlist') }}</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*             </li>*/
/*         </ul>*/
/*     </div>*/
/* {% else %}*/
/*     <div class="modal modalOptMn" id="playlistOpt">*/
/*         <ul>*/
/*             <li>*/
/*                 <a href="javascript:void(0);" class="btnSeeLater"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-later') }}"></use></svg>{{t('wap', 'Thêm vào xem sau') }}</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="javascript:void(0)" class="modalBtn btnCreatePlaylist" data-target="#createPlaylistModal"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-addto') }}"></use></svg> {{t('wap', 'Tạo danh sách phát mới') }}</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="javascript:void(0)"class="modalBtn btnLoadMyPlaylist" data-target="#playlistPop" data-opt="clearAll"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-playlist') }}"></use></svg> {{t('wap', 'Thêm vào danh sách phát') }}</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*             </li>*/
/*         </ul>*/
/*     </div>*/
/* {% endif %}*/
/* */
