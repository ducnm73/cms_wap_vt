<?php

/* mainLogin.twig */
class __TwigTemplate_45fc3abde72890e0c933b0f55ffc4d5e23ae6eeca64562b9ba31a417eefa4793 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/AppAsset");
        echo "

";
        // line 3
        $this->env->getExtension('WapExtension')->registerAsset($context, "app_asset");
        echo "
";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginPage", array(), "method"), "html", null, true);
        echo "

<!DOCTYPE html>
<html lang=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()), "html", null, true);
        echo "\">
<head>
    <meta charset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "charset", array()), "html", null, true);
        echo "\">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>

    ";
        // line 12
        echo $this->getAttribute((isset($context["html"]) ? $context["html"] : null), "csrfMetaTags", array(), "method");
        echo "
    <title>";
        // line 13
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())) ? ($this->getAttribute((isset($context["html"]) ? $context["html"] : null), "encode", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())), "method")) : (Yii::t("wap", "Mạng xã hội video - Myclip"))), "html", null, true);
        echo "</title>
\t<link type=\"image/x-icon\" rel=\"shortcut icon\" href=\"/favicon.ico\">
    ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "head", array(), "method"), "html", null, true);
        echo "
    ";
        // line 16
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "mainLogin.twig", 16)->display(array_merge($context, array("pos" => "head")));
        // line 17
        echo "
</head>
<body>

";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginBody", array(), "method"), "html", null, true);
        echo "
";
        // line 22
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "mainLogin.twig", 22)->display(array_merge($context, array("pos" => "body")));
        // line 23
        echo "<main>
    <section class=\"blk blkSignIn\">
        ";
        // line 25
        echo $this->getAttribute((isset($context["Alert"]) ? $context["Alert"] : null), "widget", array(), "method");
        echo "
        ";
        // line 26
        echo $this->getAttribute((isset($context["TrackingWidget"]) ? $context["TrackingWidget"] : null), "widget", array(), "method");
        echo "
        ";
        // line 27
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "
    </section>
</main>

";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endBody", array(), "method"), "html", null, true);
        echo "
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-88258508-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>

";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endPage", array(), "method"), "html", null, true);
    }

    public function getTemplateName()
    {
        return "mainLogin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 45,  91 => 31,  84 => 27,  80 => 26,  76 => 25,  72 => 23,  70 => 22,  66 => 21,  60 => 17,  58 => 16,  54 => 15,  49 => 13,  45 => 12,  39 => 9,  34 => 7,  28 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/AppAsset') }}*/
/* */
/* {{ register_app_asset() }}*/
/* {{ this.beginPage() }}*/
/* */
/* <!DOCTYPE html>*/
/* <html lang="{{ app.language }}">*/
/* <head>*/
/*     <meta charset="{{ app.charset }}">*/
/*     <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>*/
/* */
/*     {{ html.csrfMetaTags() | raw }}*/
/*     <title>{{ (this.title)?html.encode(this.title):t('wap', 'Mạng xã hội video - Myclip') }}</title>*/
/* 	<link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">*/
/*     {{ this.head() }}*/
/*     {% include '@app/views/partials/trackingHead.twig' with {'pos': 'head'} %}*/
/* */
/* </head>*/
/* <body>*/
/* */
/* {{ this.beginBody() }}*/
/* {% include '@app/views/partials/trackingHead.twig' with {'pos': 'body'} %}*/
/* <main>*/
/*     <section class="blk blkSignIn">*/
/*         {{ Alert.widget() | raw }}*/
/*         {{ TrackingWidget.widget() | raw }}*/
/*         {{ content | raw }}*/
/*     </section>*/
/* </main>*/
/* */
/* {{ this.endBody() }}*/
/* <script>*/
/*     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');*/
/* */
/*     ga('create', 'UA-88258508-1', 'auto');*/
/*     ga('send', 'pageview');*/
/* */
/* </script>*/
/* </body>*/
/* </html>*/
/* */
/* {{ this.endPage() }}*/
