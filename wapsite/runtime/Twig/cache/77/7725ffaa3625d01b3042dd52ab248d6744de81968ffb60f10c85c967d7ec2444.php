<?php

/* @app/views/partials/popupCommon.twig */
class __TwigTemplate_6ebd54437ada8db6532efbe3fdb4723a8a20b65e3fcb7ac03d6c9c622f850474 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal modalPopup\" id=\"popupCommon\">
    <div class=\"ctn\">
        <h5>";
        // line 3
        echo twig_escape_filter($this->env, Yii::t("wap", "THÔNG BÁO"), "html", null, true);
        echo "</h5>
        <div class=\"piracy1\">
            <p class=\"title\"></p>      
        </div>
        <div class=\"action alRight\">
            <a class=\"close\" href=\"javascript:void(0)\">";
        // line 8
        echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
        echo "</a>
        </div>
    </div>
</div>
<div class=\"modal modalPopup\" id=\"optionNotification\">
    <div class=\"ctn\">
        <h5>";
        // line 14
        echo twig_escape_filter($this->env, Yii::t("wap", "Nhận thông báo"), "html", null, true);
        echo "</h5>
        <div class=\"ctn\">
            <p class=\"clrGrey\">
            <p><input type=\"radio\" name=\"accept\" value=\"0\"> ";
        // line 17
        echo twig_escape_filter($this->env, Yii::t("wap", "Không nhận thông báo"), "html", null, true);
        echo "</p>
            <p><input type=\"radio\" name=\"accept\" value=\"1\"> ";
        // line 18
        echo twig_escape_filter($this->env, Yii::t("wap", "Thỉnh thoảng"), "html", null, true);
        echo "</p>
            <p><input type=\"radio\" name=\"accept\" value=\"2\"> ";
        // line 19
        echo twig_escape_filter($this->env, Yii::t("wap", "Tất cả"), "html", null, true);
        echo "</p>
            <div class=\"action alRight\">
                <a class=\"ok\" href=\"javascript:void(0)\">";
        // line 21
        echo twig_escape_filter($this->env, Yii::t("wap", "Đồng ý"), "html", null, true);
        echo "</a>
                <a class=\"close\" href=\"javascript:void(0)\">";
        // line 22
        echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
        echo "</a>
            </div>
        </div>
    </div>
</div>

<div class=\"modal modalWC\" id=\"videoOptWorldCup\">
    <div class=\"ctn alCenter\">
        <a class=\"close right\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-close\"></use></svg></a>
        <h1><img src=\"/images/wc_logo.png\" width=\"200\"></h1>
        <p>";
        // line 32
        echo twig_escape_filter($this->env, Yii::t("wap", "Xem Video, Phim, Thể thao 100% Data tốc độ cao trên MyClip (Giá DV 35.000 đồng/tháng, gia hạn hàng tháng)"), "html", null, true);
        echo "</p>
        <p>&nbsp;</p>

        <form id=\"form-wc\" action=\"/account/register-service\"  method=\"post\">
            <input type=\"hidden\" name=\"package_id\" value=\"3\"/>
            <input type=\"hidden\"
                   name=\"";
        // line 38
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
        echo "\"
                   value=\"";
        // line 39
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
        echo "\">
        </form>

        <p><a class=\"btn btnWhite\" onclick=\"document.getElementById('form-wc').submit()\" href=\"javascript:void(0)\">";
        // line 42
        echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký ngay"), "html", null, true);
        echo "</a> </p>

    </div>
    <img src=\"/images/wc_footer.png\">
</div>

<div class=\"modal modalPopup\" id=\"popUp2010\">
    <div class=\"ctn\">
        <a href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-close\"></use></svg></a>
        <img src=\"/images/woman_01.png\">
        <p class=\"text\">";
        // line 52
        echo $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "message.for.2010"), "method");
        echo "</p>
    </div>
</div>



";
    }

    public function getTemplateName()
    {
        return "@app/views/partials/popupCommon.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 52,  95 => 42,  89 => 39,  85 => 38,  76 => 32,  63 => 22,  59 => 21,  54 => 19,  50 => 18,  46 => 17,  40 => 14,  31 => 8,  23 => 3,  19 => 1,);
    }
}
/* <div class="modal modalPopup" id="popupCommon">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'THÔNG BÁO') }}</h5>*/
/*         <div class="piracy1">*/
/*             <p class="title"></p>      */
/*         </div>*/
/*         <div class="action alRight">*/
/*             <a class="close" href="javascript:void(0)">{{ t('wap', 'Đóng') }}</a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="modal modalPopup" id="optionNotification">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'Nhận thông báo') }}</h5>*/
/*         <div class="ctn">*/
/*             <p class="clrGrey">*/
/*             <p><input type="radio" name="accept" value="0"> {{t('wap', 'Không nhận thông báo') }}</p>*/
/*             <p><input type="radio" name="accept" value="1"> {{t('wap', 'Thỉnh thoảng') }}</p>*/
/*             <p><input type="radio" name="accept" value="2"> {{t('wap', 'Tất cả') }}</p>*/
/*             <div class="action alRight">*/
/*                 <a class="ok" href="javascript:void(0)">{{t('wap', 'Đồng ý') }}</a>*/
/*                 <a class="close" href="javascript:void(0)">{{t('wap', 'Đóng') }}</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="modal modalWC" id="videoOptWorldCup">*/
/*     <div class="ctn alCenter">*/
/*         <a class="close right"><svg class="ico"><use xlink:href="/images/defs.svg#ico-close"></use></svg></a>*/
/*         <h1><img src="/images/wc_logo.png" width="200"></h1>*/
/*         <p>{{t('wap', 'Xem Video, Phim, Thể thao 100% Data tốc độ cao trên MyClip (Giá DV 35.000 đồng/tháng, gia hạn hàng tháng)') }}</p>*/
/*         <p>&nbsp;</p>*/
/* */
/*         <form id="form-wc" action="/account/register-service"  method="post">*/
/*             <input type="hidden" name="package_id" value="3"/>*/
/*             <input type="hidden"*/
/*                    name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                    value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*         </form>*/
/* */
/*         <p><a class="btn btnWhite" onclick="document.getElementById('form-wc').submit()" href="javascript:void(0)">{{t('wap', 'Đăng ký ngay') }}</a> </p>*/
/* */
/*     </div>*/
/*     <img src="/images/wc_footer.png">*/
/* </div>*/
/* */
/* <div class="modal modalPopup" id="popUp2010">*/
/*     <div class="ctn">*/
/*         <a href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="/images/defs.svg#ico-close"></use></svg></a>*/
/*         <img src="/images/woman_01.png">*/
/*         <p class="text">{{ app.session.get('message.for.2010')|raw }}</p>*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* */
/* */
