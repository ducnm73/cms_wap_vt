<?php

/* listVideo.twig */
class __TwigTemplate_642012add246dcd93a1f80dd1b453eb4186c127c8425fbb552c4ef3be32ab57d extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ReactAsset");
        echo "
";
        // line 3
        $this->env->getExtension('WapExtension')->registerAsset($context, "react_asset");
        echo "
 ";
        // line 4
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "main", "title" => (isset($context["title"]) ? $context["title"] : null))), "method");
        echo "

<div id=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\">
    <section ";
        // line 7
        if (((isset($context["type"]) ? $context["type"] : null) == "sub")) {
            echo " class=\"blk blkPadBoth blkListVideosVer\" ";
        } else {
            echo "  class=\"blk blkPadBothExt blkListVideosVer\" ";
        }
        echo ">

        ";
        // line 9
        $this->loadTemplate("@app/views/partials/videoSmallBox.twig", "listVideo.twig", 9)->display(array_merge($context, array("data" => $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()), "hasTitle" => false, "hasArrow" => false, "iconMore" => true)));
        // line 10
        echo "
        ";
        // line 11
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()), "content", array())) == 0)) {
            // line 12
            echo "            <p class=\"ctn\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Không có dữ liệu"), "html", null, true);
            echo "</p>
        ";
        }
        // line 14
        echo "
        <div id=\"container\"></div>

        <input type=\"hidden\" id=\"homeItemLimit\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.lazy.limit", array(), "array"), "html", null, true);
        echo "\">
        ";
        // line 18
        if ((isset($context["offset"]) ? $context["offset"] : null)) {
            // line 19
            echo "            <input type=\"hidden\" id=\"getMoreOffset\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["offset"]) ? $context["offset"] : null), "html", null, true);
            echo "\">
        ";
        } else {
            // line 21
            echo "            <input type=\"hidden\" id=\"getMoreOffset\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
            echo "\">
        ";
        }
        // line 23
        echo "        <input type=\"hidden\" id=\"load-more-id\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\"/>
        <input type=\"hidden\" id=\"type\" value=\"VOD\"/>
    </section>
    ";
        // line 26
        if (((isset($context["id"]) ? $context["id"] : null) == "video_history")) {
            // line 27
            echo "        <div class=\"modal modalOptMn\" id=\"specialOpt\">
            <ul>
                <li>
                    <a href=\"javascript:void(0);\" class=\"btnDelete\"><svg class=\"ico\"><use xlink:href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-delete"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Xóa lịch sử xem"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
            echo "</a>
                </li>
            </ul>
        </div>
        <div class=\"overlay\"></div>
    ";
        } elseif ((        // line 38
(isset($context["id"]) ? $context["id"] : null) == "video_watch_later")) {
            // line 39
            echo "        <div class=\"modal modalOptMn\" id=\"specialOpt\">
            <ul>
                <li>
                    <a href=\"javascript:void(0);\" class=\"btnRemoveViewLater\"><svg class=\"ico\"><use xlink:href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-delete"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Xóa khỏi xem sau"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a data-videoid=\"1\" href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
            echo "</a>
                </li>
            </ul>
        </div>
        <div class=\"overlay\"></div>
    ";
        } elseif ((        // line 50
(isset($context["id"]) ? $context["id"] : null) == "video_owner")) {
            // line 51
            echo "        <div class=\"modal modalOptMn\" id=\"specialOpt\">
            <ul>
                <li>
                    <a href=\"javascript:void(0);\" class=\"btnSeeLater\"><svg class=\"ico\"><use xlink:href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-later"), "html", null, true);
            echo "\"></use></svg>";
            echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào xem sau"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"modalBtn btnCreatePlaylist\" data-target=\"#createPlaylistModal\" data-opt=\"clearAll\"><svg class=\"ico\"><use xlink:href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-addto"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Tạo danh sách phát"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"modalBtn btnLoadMyPlaylist\" data-opt=\"clearAll\" data-target=\"#playlistPop\"><svg class=\"ico\"><use xlink:href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-playlist"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào danh sách phát"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"modalBtn btnCreatePlaylist\" onclick=\"openEditVideo()\" ><svg class=\"ico\"><use xlink:href=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-edit"), "html", null, true);
            echo "\"></use></svg>";
            echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"close\" data-opt=\"clearAll\" id=\"delete_my_video_button\"  >
                        <svg class=\"ico\"><use xlink:href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-delete"), "html", null, true);
            echo "\"></use></svg>";
            echo twig_escape_filter($this->env, Yii::t("wap", "Xóa"), "html", null, true);
            echo "
                    </a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
            echo "</a>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"close btnTriggercomment\"><i class=\"fa fa-commenting-o ico\" aria-hidden=\"true\"></i> <span>";
            // line 74
            echo twig_escape_filter($this->env, Yii::t("wap", "Tắt bình luận"), "html", null, true);
            echo "</span></a>
                </li>

            </ul>
        </div>

    
";
            // line 95
            echo "
        </div>
        <div class=\"overlay\"></div>

        <div class=\"modal modalPopup\" id=\"editVideoModal\" style=\"position: absolute\">
            <div class=\"ctn\">
                <h5>";
            // line 101
            echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa video"), "html", null, true);
            echo "</h5>
                    <div class=\"ctn\" style=\"padding:0\">
                        <div class=\"form-group\">
                            <form id=\"edit_video_form\" method=\"post\" enctype=\"multipart/form-data\" action=\"/video/edit-video\">
                                <input type=\"hidden\"
                                       name=\"";
            // line 106
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
            echo "\"
                                       value=\"";
            // line 107
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
            echo "\">
                                <input type=\"hidden\" name=\"id\" id=\"edit_video_id\">
                                <div class=\"row-form\">
                                    <p>";
            // line 110
            echo twig_escape_filter($this->env, Yii::t("wap", "Tên video"), "html", null, true);
            echo "</p>
                                    <input class=\"ipt iptLg\" placeholder=\"";
            // line 111
            echo twig_escape_filter($this->env, Yii::t("wap", "Nhập tên video mới"), "html", null, true);
            echo "\" autofocus type=\"text\" id=\"edit_video_name\" name=\"title\" style=\"width: 100%\"/>
                                </div>
                                <div class=\"row-form\">
                                    <p>";
            // line 114
            echo twig_escape_filter($this->env, Yii::t("wap", "Chọn ảnh đại diện"), "html", null, true);
            echo "</p>
                                    <input type=\"file\" id=\"edit_video_file\" name=\"thumbnail\"/>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class=\"controls\">
                        <a class=\"btn btnBlue ok\" href=\"javascript:void(0)\" id=\"edit_video_button\">";
            // line 122
            echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa"), "html", null, true);
            echo "</a>
                        <a class=\"btn btnOutline close\" href=\"javascript:void(0)\">";
            // line 123
            echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
            echo "</a>
                    </div>
            </div>
        </div>

    ";
        }
        // line 129
        echo "
</div>

";
        // line 132
        if ((isset($context["trackingContent"]) ? $context["trackingContent"] : null)) {
            // line 133
            echo "    ";
            $this->loadTemplate("@app/views/partials/trackingView.twig", "listVideo.twig", 133)->display(array_merge($context, array("trackingContent" => (isset($context["trackingContent"]) ? $context["trackingContent"] : null))));
        }
    }

    public function getTemplateName()
    {
        return "listVideo.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  275 => 133,  273 => 132,  268 => 129,  259 => 123,  255 => 122,  244 => 114,  238 => 111,  234 => 110,  228 => 107,  224 => 106,  216 => 101,  208 => 95,  198 => 74,  190 => 71,  181 => 67,  172 => 63,  164 => 60,  156 => 57,  148 => 54,  143 => 51,  141 => 50,  131 => 45,  123 => 42,  118 => 39,  116 => 38,  106 => 33,  98 => 30,  93 => 27,  91 => 26,  84 => 23,  78 => 21,  72 => 19,  70 => 18,  66 => 17,  61 => 14,  55 => 12,  53 => 11,  50 => 10,  48 => 9,  39 => 7,  35 => 6,  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* */
/* {{ use('wapsite/assets/ReactAsset') }}*/
/* {{ register_react_asset() }}*/
/*  {{ HeaderWidget.widget({'type':'main','title':title}) | raw }}*/
/* */
/* <div id="{{ id }}">*/
/*     <section {% if type=='sub' %} class="blk blkPadBoth blkListVideosVer" {% else %}  class="blk blkPadBothExt blkListVideosVer" {% endif %}>*/
/* */
/*         {% include "@app/views/partials/videoSmallBox.twig" with {'data': responseData.data, 'hasTitle': false, 'hasArrow': false,'iconMore': true} %}*/
/* */
/*         {% if responseData.data.content|length == 0  %}*/
/*             <p class="ctn">{{t('wap', 'Không có dữ liệu') }}</p>*/
/*         {% endif %}*/
/* */
/*         <div id="container"></div>*/
/* */
/*         <input type="hidden" id="homeItemLimit" value="{{ app.params['app.home.lazy.limit'] }}">*/
/*         {% if offset %}*/
/*             <input type="hidden" id="getMoreOffset" value="{{ offset }}">*/
/*         {% else %}*/
/*             <input type="hidden" id="getMoreOffset" value="{{ app.params['app.home.limit'] }}">*/
/*         {% endif %}*/
/*         <input type="hidden" id="load-more-id" value="{{ id }}"/>*/
/*         <input type="hidden" id="type" value="VOD"/>*/
/*     </section>*/
/*     {% if id == 'video_history' %}*/
/*         <div class="modal modalOptMn" id="specialOpt">*/
/*             <ul>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="btnDelete"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-delete') }}"></use></svg> {{t('wap', 'Xóa lịch sử xem') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*                 </li>*/
/*             </ul>*/
/*         </div>*/
/*         <div class="overlay"></div>*/
/*     {% elseif id == 'video_watch_later' %}*/
/*         <div class="modal modalOptMn" id="specialOpt">*/
/*             <ul>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="btnRemoveViewLater"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-delete') }}"></use></svg> {{t('wap', 'Xóa khỏi xem sau') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a data-videoid="1" href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*                 </li>*/
/*             </ul>*/
/*         </div>*/
/*         <div class="overlay"></div>*/
/*     {% elseif id == 'video_owner' %}*/
/*         <div class="modal modalOptMn" id="specialOpt">*/
/*             <ul>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="btnSeeLater"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-later') }}"></use></svg>{{t('wap', 'Thêm vào xem sau') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="modalBtn btnCreatePlaylist" data-target="#createPlaylistModal" data-opt="clearAll"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-addto') }}"></use></svg> {{t('wap', 'Tạo danh sách phát') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="modalBtn btnLoadMyPlaylist" data-opt="clearAll" data-target="#playlistPop"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-playlist') }}"></use></svg> {{t('wap', 'Thêm vào danh sách phát') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="modalBtn btnCreatePlaylist" onclick="openEditVideo()" ><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-edit') }}"></use></svg>{{t('wap', 'Chỉnh sửa') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="close" data-opt="clearAll" id="delete_my_video_button"  >*/
/*                         <svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-delete') }}"></use></svg>{{t('wap', 'Xóa') }}*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="close btnTriggercomment"><i class="fa fa-commenting-o ico" aria-hidden="true"></i> <span>{{t('wap','Tắt bình luận')}}</span></a>*/
/*                 </li>*/
/* */
/*             </ul>*/
/*         </div>*/
/* */
/*     */
/* {#        <ul>#}*/
/* {#            <li>#}*/
/* {#                <a href="javascript:void(0);" class="btnSeeLater"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-later') }}"></use></svg>{{t('wap', 'Thêm vào xem sau') }}</a>#}*/
/* {#            </li>#}*/
/* {#            <li>#}*/
/* {#                <a href="javascript:void(0);" class="modalBtn btnCreatePlaylist" data-target="#createPlaylistModal" data-opt="clearAll"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-addto') }}"></use></svg> {{t('wap', 'Tạo danh sách phát') }}</a>#}*/
/* {#            </li>#}*/
/* {#            <li>#}*/
/* {#                <a href="javascript:void(0);" class="modalBtn btnLoadMyPlaylist" data-opt="clearAll" data-target="#playlistPop"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-playlist') }}"></use></svg> {{t('wap', 'Thêm vào danh sách phát') }}</a>#}*/
/* {#            </li>#}*/
/* {#            <li>#}*/
/* {#                <a href="javascript:void(0);" class="close" data-opt="clearAll" id="delete_my_video_button"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-delete') }}"></use></svg>{{t('wap', 'Xóa') }}</a>#}*/
/* {#            </li>#}*/
/* {#        </ul>#}*/
/* */
/*         </div>*/
/*         <div class="overlay"></div>*/
/* */
/*         <div class="modal modalPopup" id="editVideoModal" style="position: absolute">*/
/*             <div class="ctn">*/
/*                 <h5>{{t('wap', 'Chỉnh sửa video') }}</h5>*/
/*                     <div class="ctn" style="padding:0">*/
/*                         <div class="form-group">*/
/*                             <form id="edit_video_form" method="post" enctype="multipart/form-data" action="/video/edit-video">*/
/*                                 <input type="hidden"*/
/*                                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                                 <input type="hidden" name="id" id="edit_video_id">*/
/*                                 <div class="row-form">*/
/*                                     <p>{{t('wap', 'Tên video') }}</p>*/
/*                                     <input class="ipt iptLg" placeholder="{{t('wap','Nhập tên video mới')}}" autofocus type="text" id="edit_video_name" name="title" style="width: 100%"/>*/
/*                                 </div>*/
/*                                 <div class="row-form">*/
/*                                     <p>{{t('wap', 'Chọn ảnh đại diện') }}</p>*/
/*                                     <input type="file" id="edit_video_file" name="thumbnail"/>*/
/*                                 </div>*/
/*                             </form>*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="controls">*/
/*                         <a class="btn btnBlue ok" href="javascript:void(0)" id="edit_video_button">{{t('wap', 'Chỉnh sửa') }}</a>*/
/*                         <a class="btn btnOutline close" href="javascript:void(0)">{{t('wap', 'Đóng') }}</a>*/
/*                     </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*     {% endif %}*/
/* */
/* </div>*/
/* */
/* {% if trackingContent %}*/
/*     {% include '@app/views/partials/trackingView.twig' with {'trackingContent': trackingContent} %}*/
/* {% endif %}*/
/* */
