<?php

/* @app/views/partials/videoSmallBox.twig */
class __TwigTemplate_74d555a2331645d5e266f16b9a7ebb7224b3a1416f989eee30caeeea7f105ffd extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["hasTitle"]) ? $context["hasTitle"] : null)) {
            // line 2
            echo "    <h6 class=\"blkTitle\">
        ";
            // line 3
            if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "categoryImage", array())) {
                // line 4
                echo "            <div class=\"avatar\">
                <img src=\"";
                // line 5
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "categoryImage", array()), "html", null, true);
                echo "\" alt=\"\">
            </div>
        ";
            }
            // line 8
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "name", array()), "html", null, true);
            echo "
    </h6>
";
        }
        // line 11
        echo "
";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 13
            echo "    ";
            if (($this->getAttribute($context["video"], "status", array()) != 2)) {
                // line 14
                echo "        ";
                $context["detailUrl"] = "javascript:void(0)";
                // line 15
                echo "    ";
            } else {
                // line 16
                echo "        ";
                $context["detailUrl"] = $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array())));
                // line 17
                echo "    ";
            }
            // line 18
            echo "
    <article class=\"videoItem item-";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\">
        <div class=\"ctn\">
            <div class=\"pthld\">
                <span class=\"timing\">";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "duration", array()), "html", null, true);
            echo "</span>
                <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, (isset($context["detailUrl"]) ? $context["detailUrl"] : null), "html", null, true);
            echo "\">
                    <img src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "coverImage", array()), "html", null, true);
            echo "\" onError=\"this.onerror=null;this.src='/images/16x9.png'\" alt=\"\">
                </a>
                ";
            // line 26
            if (($this->getAttribute($context["video"], "price_play", array()) == 0)) {
                // line 27
                echo "                    <div class=\"attach-img\">
                        <img src=\"/images/freeVideo.png\">
                    </div>
                ";
            }
            // line 31
            echo "            </div>
            <div class=\"ctnhld\">
                <h6 class=\"title\">
                    <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, (isset($context["detailUrl"]) ? $context["detailUrl"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "</a>
                </h6>
                <p class=\"txtInfo\">
                    ";
            // line 37
            if (($this->getAttribute($context["video"], "status", array()) == 3)) {
                // line 38
                echo "                        <a href=";
                echo twig_escape_filter($this->env, (isset($context["detailUrl"]) ? $context["detailUrl"] : null), "html", null, true);
                echo ">
                            ";
                // line 39
                echo twig_escape_filter($this->env, Yii::t("wap", "Từ chối duyệt"), "html", null, true);
                echo " ";
                if ($this->getAttribute($context["video"], "reason", array())) {
                    echo " (";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Lý do"), "html", null, true);
                    echo ": ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "reason", array()), "html", null, true);
                    echo ") ";
                }
                // line 40
                echo "                        </a>

                    ";
            } elseif (($this->getAttribute(            // line 42
$context["video"], "status", array()) == 1)) {
                // line 43
                echo "                        <a href=";
                echo twig_escape_filter($this->env, (isset($context["detailUrl"]) ? $context["detailUrl"] : null), "html", null, true);
                echo ">
                            ";
                // line 44
                echo twig_escape_filter($this->env, Yii::t("wap", "Chờ phê duyệt(chưa xem được)"), "html", null, true);
                echo "
                        </a>
                    ";
            } else {
                // line 47
                echo "                      
                        <a href=\"";
                // line 48
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-detail", array("id" => $this->getAttribute($context["video"], "userId", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "userName", array()), "html", null, true);
                echo "</a>
                        <i class=\"dot\"></i>
                        <a href=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
                echo "\">
                            ";
                // line 51
                echo twig_escape_filter($this->env, Yii::t("wap", "{count} lượt xem", array("count" => $this->getAttribute($context["video"], "play_times", array()))), "html", null, true);
                echo "
                        </a>
                        <i class=\"dot\"></i>
                        <a href=\"";
                // line 54
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
                echo "\">
                            ";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "publishedTime", array()), "html", null, true);
                echo "
                        </a>
                    ";
            }
            // line 58
            echo "                </p>
            </div>
            ";
            // line 60
            if (((($this->getAttribute($context["video"], "status", array()) == 2) && ((isset($context["iconMore"]) ? $context["iconMore"] : null) != false)) || ((isset($context["id"]) ? $context["id"] : null) == "video_owner"))) {
                // line 61
                echo "                <a class=\"icoMore optMnBtn\" data-target=\"";
                if ((isset($context["playlist"]) ? $context["playlist"] : null)) {
                    echo " #playlistOpt ";
                } else {
                    echo " #videoOpt";
                }
                echo "\"
                   data-url=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "linkSocial", array()), "html", null, true);
                echo "\"
                   data-id=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
                echo "\"  data-comment=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "can_comment", array()), "html", null, true);
                echo "\" data-status=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "status", array()), "html", null, true);
                echo "\">
                    <svg class=\"ico\">
                        <use xlink:href=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-more"), "html", null, true);
                echo "\"></use>
                    </svg>
                </a>
            ";
            }
            // line 69
            echo "

        </div>
    </article>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        if (((isset($context["hasPage"]) ? $context["hasPage"] : null) && ((isset($context["num"]) ? $context["num"] : null) > twig_length_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array()))))) {
            // line 75
            echo "    <a class=\"btn btnLg\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/get-video-list", array("id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Xem tất cả"), "html", null, true);
            echo "&nbsp;";
            echo twig_escape_filter($this->env, (isset($context["num"]) ? $context["num"] : null), "html", null, true);
            echo "&nbsp;videos</a>
";
        }
    }

    public function getTemplateName()
    {
        return "@app/views/partials/videoSmallBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 75,  216 => 74,  206 => 69,  199 => 65,  190 => 63,  186 => 62,  177 => 61,  175 => 60,  171 => 58,  165 => 55,  161 => 54,  155 => 51,  151 => 50,  144 => 48,  141 => 47,  135 => 44,  130 => 43,  128 => 42,  124 => 40,  114 => 39,  109 => 38,  107 => 37,  99 => 34,  94 => 31,  88 => 27,  86 => 26,  81 => 24,  77 => 23,  73 => 22,  67 => 19,  64 => 18,  61 => 17,  58 => 16,  55 => 15,  52 => 14,  49 => 13,  45 => 12,  42 => 11,  35 => 8,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if hasTitle %}*/
/*     <h6 class="blkTitle">*/
/*         {% if data.categoryImage %}*/
/*             <div class="avatar">*/
/*                 <img src="{{ data.categoryImage }}" alt="">*/
/*             </div>*/
/*         {% endif %}*/
/*         {{ data.name }}*/
/*     </h6>*/
/* {% endif %}*/
/* */
/* {% for video in data.content %}*/
/*     {% if video.status != 2 %}*/
/*         {% set detailUrl = "javascript:void(0)" %}*/
/*     {% else %}*/
/*         {% set detailUrl = url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium }) %}*/
/*     {% endif %}*/
/* */
/*     <article class="videoItem item-{{ video.id }}">*/
/*         <div class="ctn">*/
/*             <div class="pthld">*/
/*                 <span class="timing">{{video.duration}}</span>*/
/*                 <a href="{{ detailUrl }}">*/
/*                     <img src="{{ video.coverImage }}" onError="this.onerror=null;this.src='/images/16x9.png'" alt="">*/
/*                 </a>*/
/*                 {% if(video.price_play == 0) %}*/
/*                     <div class="attach-img">*/
/*                         <img src="/images/freeVideo.png">*/
/*                     </div>*/
/*                 {% endif %}*/
/*             </div>*/
/*             <div class="ctnhld">*/
/*                 <h6 class="title">*/
/*                     <a href="{{ detailUrl }}">{{ video.name }}</a>*/
/*                 </h6>*/
/*                 <p class="txtInfo">*/
/*                     {% if video.status == 3 %}*/
/*                         <a href={{ detailUrl }}>*/
/*                             {{ t('wap', 'Từ chối duyệt') }} {% if video.reason %} ({{ t('wap', 'Lý do') }}: {{ video.reason }}) {% endif %}*/
/*                         </a>*/
/* */
/*                     {% elseif video.status == 1 %}*/
/*                         <a href={{ detailUrl }}>*/
/*                             {{ t('wap', 'Chờ phê duyệt(chưa xem được)') }}*/
/*                         </a>*/
/*                     {% else %}*/
/*                       */
/*                         <a href="{{ url('channel/get-detail',{'id':video.userId }) }}">{{ video.userName }}</a>*/
/*                         <i class="dot"></i>*/
/*                         <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">*/
/*                             {{ t('wap','{count} lượt xem', {'count': video.play_times}) }}*/
/*                         </a>*/
/*                         <i class="dot"></i>*/
/*                         <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">*/
/*                             {{ video.publishedTime }}*/
/*                         </a>*/
/*                     {% endif %}*/
/*                 </p>*/
/*             </div>*/
/*             {% if (video.status == 2 and iconMore != false) or id=='video_owner' %}*/
/*                 <a class="icoMore optMnBtn" data-target="{% if playlist %} #playlistOpt {% else %} #videoOpt{% endif %}"*/
/*                    data-url="{{ video.linkSocial }}"*/
/*                    data-id="{{ video.id }}"  data-comment="{{ video.can_comment }}" data-status="{{ video.status }}">*/
/*                     <svg class="ico">*/
/*                         <use xlink:href="{{ url('images/defs.svg#ico-more') }}"></use>*/
/*                     </svg>*/
/*                 </a>*/
/*             {% endif %}*/
/* */
/* */
/*         </div>*/
/*     </article>*/
/* {% endfor %}*/
/* {% if hasPage and num > data.content|length %}*/
/*     <a class="btn btnLg" href="{{ url('default/get-video-list',{'id': id}) }}">{{ t('wap', 'Xem tất cả') }}&nbsp;{{ num }}&nbsp;videos</a>*/
/* {% endif %}*/
