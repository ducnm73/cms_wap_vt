<?php

/* index.twig */
class __TwigTemplate_6b18edc512f6f7c954eaf35c58eeb684b44547782a057d436ab4346091fddd76 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/HomeAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "home_asset");
        echo "

<div class=\"blkPadBoth\">
    <!--
    <section class=\"blk blkIphoneX blkBdBt\">
        <div class=\"ctn banner\">
            <span class=\"hot\">Hot</span> <span class=\"title\"><a href=\"/event/main-index\">Xem video - Trúng thẻ cào, iPhone X</a></span>
        </div>
    </section>
    -->
    ";
        // line 19
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["block"]) {
            // line 20
            echo "        ";
            // line 21
            echo "        ";
            // line 23
            echo "        ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["block"], "content", array())) > 0)) {
                // line 24
                echo "            ";
                if (($this->getAttribute($context["block"], "type", array()) == "CATEGORY")) {
                    // line 25
                    echo "            ";
                    // line 26
                    echo "                <section class=\"blk blkTopChnls\">
                    <ul class=\"chnlSlider owl-carousel\">
                        ";
                    // line 29
                    echo "                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["block"], "content", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 30
                        echo "                            ";
                        // line 31
                        echo "                            <li class=\"owl-item li-cate\" id=\"li-cate-";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                        echo "\">
                                <div class=\"avatar ";
                        // line 32
                        if ($this->getAttribute($context["category"], "is_event", array())) {
                            echo " highlight-event ";
                        }
                        echo "\">
                                    <a onclick=\"loadHomeCategory('";
                        // line 33
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                        echo "')\">
                                        <img src=\"";
                        // line 34
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "avatarImage", array()), "html", null, true);
                        echo "\" alt=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
                        echo "\">
                                    </a>
                                </div>
                                <p class=\"title ";
                        // line 37
                        if ($this->getAttribute($context["category"], "is_event", array())) {
                            echo " text-highlight-event ";
                        }
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name_short", array()), "html", null, true);
                        echo " </p>
                            </li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 40
                    echo "
                    </ul>
                </section>
            ";
                }
                // line 44
                echo "        ";
            }
            // line 45
            echo "
        ";
            // line 46
            if ((($this->getAttribute($context["block"], "type", array()) != "CATEGORY") && (twig_length_filter($this->env, $this->getAttribute($context["block"], "content", array())) > 0))) {
                // line 47
                echo "            ";
                // line 48
                echo "            <section class=\"blk blkListVideos\">
                <div class=\"ctn\">

                    <input type=\"hidden\" id=\"homeItemLimit\" value=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.lazy.limit", array(), "array"), "html", null, true);
                echo "\">
                    <input type=\"hidden\" id=\"getMoreOffset\" value=\"0\">
                    <input type=\"hidden\" id=\"load-more-id\" value=\"video_hot\"/>

                    <div id=\"root-tab\">
                        ";
                // line 56
                $this->loadTemplate("@app/views/partials/videoBox.twig", "index.twig", 56)->display(array_merge($context, array("data" => $context["block"], "hasTitle" => true, "hasArrow" => false)));
                // line 57
                echo "                    </div>

                    ";
                // line 59
                if ((twig_length_filter($this->env, $this->getAttribute($context["block"], "content", array())) == 0)) {
                    // line 60
                    echo "                        <p class=\"ctn\">";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Không có dữ liệu"), "html", null, true);
                    echo "</p>
                    ";
                }
                // line 62
                echo "
                    <div id=\"container\">
                    </div>
                </div>
            </section>
        ";
            }
            // line 68
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['block'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "
</div>

";
        // line 72
        if ((isset($context["popup"]) ? $context["popup"] : null)) {
            // line 73
            echo "    ";
            $this->loadTemplate("@app/views/partials/popup.twig", "index.twig", 73)->display(array_merge($context, array("type" => "ALL", "popup" => (isset($context["popup"]) ? $context["popup"] : null), "msisdn" => (isset($context["msisdn"]) ? $context["msisdn"] : null))));
        }
        // line 75
        echo "
";
        // line 76
        if ((isset($context["trackingContent"]) ? $context["trackingContent"] : null)) {
            // line 77
            echo "    ";
            $this->loadTemplate("@app/views/partials/trackingView.twig", "index.twig", 77)->display(array_merge($context, array("trackingContent" => (isset($context["trackingContent"]) ? $context["trackingContent"] : null))));
        }
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 77,  192 => 76,  189 => 75,  185 => 73,  183 => 72,  178 => 69,  164 => 68,  156 => 62,  150 => 60,  148 => 59,  144 => 57,  142 => 56,  134 => 51,  129 => 48,  127 => 47,  125 => 46,  122 => 45,  119 => 44,  113 => 40,  100 => 37,  92 => 34,  88 => 33,  82 => 32,  77 => 31,  75 => 30,  70 => 29,  66 => 26,  64 => 25,  61 => 24,  58 => 23,  56 => 21,  54 => 20,  36 => 19,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/HomeAsset') }}*/
/* {{ register_home_asset() }}*/
/* */
/* <div class="blkPadBoth">*/
/*     <!--*/
/*     <section class="blk blkIphoneX blkBdBt">*/
/*         <div class="ctn banner">*/
/*             <span class="hot">Hot</span> <span class="title"><a href="/event/main-index">Xem video - Trúng thẻ cào, iPhone X</a></span>*/
/*         </div>*/
/*     </section>*/
/*     -->*/
/*     {#*/
/*     {{ CHtml::form() }}*/
/*         <div id="langdrop">*/
/*             {{ CHtml::dropDownList('_lang', currentLang, array('en_us' => 'English', 'is_is' => 'Icelandic'), array('submit' => '')) }}*/
/*         </div>*/
/*     {{  CHtml::endForm() }}*/
/*     #}*/
/*     {% for block in responseData.data %}*/
/*         {# {{ responseData.data | var_dump}} #}*/
/*         {# {% set text='line 14 '~responseData.data %}*/
/*         {{ text }}#}*/
/*         {% if block.content|length > 0 %}*/
/*             {% if block.type=='CATEGORY' %}*/
/*             {#{{block.type}}#}*/
/*                 <section class="blk blkTopChnls">*/
/*                     <ul class="chnlSlider owl-carousel">*/
/*                         {#Doan lay du lieu hien thi cac channel#}*/
/*                         {% for category in block.content %}*/
/*                             {# {{ category | var_dump}} #}*/
/*                             <li class="owl-item li-cate" id="li-cate-{{ category.id }}">*/
/*                                 <div class="avatar {% if category.is_event %} highlight-event {% endif %}">*/
/*                                     <a onclick="loadHomeCategory('{{ category.id }}')">*/
/*                                         <img src="{{ category.avatarImage }}" alt="{{ category.name }}">*/
/*                                     </a>*/
/*                                 </div>*/
/*                                 <p class="title {% if category.is_event %} text-highlight-event {% endif %}">{{ category.name_short  }} </p>*/
/*                             </li>*/
/*                         {% endfor %}*/
/* */
/*                     </ul>*/
/*                 </section>*/
/*             {% endif %}*/
/*         {% endif %}*/
/* */
/*         {% if block.type!='CATEGORY' and block.content|length > 0 %}*/
/*             {# {{ block.type ~ block.content|length }} #}*/
/*             <section class="blk blkListVideos">*/
/*                 <div class="ctn">*/
/* */
/*                     <input type="hidden" id="homeItemLimit" value="{{ app.params['app.home.lazy.limit'] }}">*/
/*                     <input type="hidden" id="getMoreOffset" value="0">*/
/*                     <input type="hidden" id="load-more-id" value="video_hot"/>*/
/* */
/*                     <div id="root-tab">*/
/*                         {% include "@app/views/partials/videoBox.twig" with {'data': block, 'hasTitle': true, 'hasArrow': false} %}*/
/*                     </div>*/
/* */
/*                     {% if block.content|length == 0 %}*/
/*                         <p class="ctn">{{t('wap', 'Không có dữ liệu') }}</p>*/
/*                     {% endif %}*/
/* */
/*                     <div id="container">*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/*         {% endif %}*/
/*     {% endfor %}*/
/* */
/* </div>*/
/* */
/* {% if popup %}*/
/*     {% include '@app/views/partials/popup.twig' with {'type': 'ALL', 'popup': popup, 'msisdn': msisdn } %}*/
/* {% endif %}*/
/* */
/* {% if trackingContent %}*/
/*     {% include '@app/views/partials/trackingView.twig' with {'trackingContent': trackingContent} %}*/
/* {% endif %}*/
/* */
