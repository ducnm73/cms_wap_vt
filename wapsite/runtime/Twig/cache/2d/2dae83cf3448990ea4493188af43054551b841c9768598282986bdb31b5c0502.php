<?php

/* playlistModal.twig */
class __TwigTemplate_bc07db8fd64e4d497ce2054be28d13a54afaba8d94198cc7182fdeda45f3ca89 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["playlists"]) ? $context["playlists"] : null), "content", array())) > 0)) {
            // line 2
            echo "<div class=\"modal modalPopup\" id=\"playlistPop\">
    <div class=\"ctn\">
        <h5>";
            // line 4
            echo twig_escape_filter($this->env, Yii::t("wap", "Danh sách playlist"), "html", null, true);
            echo "</h5>
        <div class=\"fixCtn\">
            <ul class=\"listMenu\">
                ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["playlists"]) ? $context["playlists"] : null), "content", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["playlist"]) {
                // line 8
                echo "                <li>
                    <a href=\"javascript:void(0);\" class=\"addVideoPlaylist\" data-playlistid=\"";
                // line 9
                echo twig_escape_filter($this->env, $this->getAttribute($context["playlist"], "id", array()), "html", null, true);
                echo "\" style=\" overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; white-space: unset;\">
                    <svg class=\"ico\"><use xlink:href=\"";
                // line 10
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-playlist"), "html", null, true);
                echo "\"></use></svg>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["playlist"], "name", array()), "html", null, true);
                echo "</a>
                </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['playlist'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "               
            </ul>
        </div>
        <a class=\"close\"><svg class=\"ico\"><use xlink:href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-close"), "html", null, true);
            echo "\"></use></svg> ";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hủy bỏ"), "html", null, true);
            echo "</a>
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "playlistModal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 15,  53 => 12,  42 => 10,  38 => 9,  35 => 8,  31 => 7,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if playlists.content|length > 0 %}*/
/* <div class="modal modalPopup" id="playlistPop">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'Danh sách playlist') }}</h5>*/
/*         <div class="fixCtn">*/
/*             <ul class="listMenu">*/
/*                 {% for playlist in playlists.content %}*/
/*                 <li>*/
/*                     <a href="javascript:void(0);" class="addVideoPlaylist" data-playlistid="{{playlist.id}}" style=" overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; white-space: unset;">*/
/*                     <svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-playlist') }}"></use></svg>{{playlist.name}}</a>*/
/*                 </li>*/
/*                 {% endfor %}               */
/*             </ul>*/
/*         </div>*/
/*         <a class="close"><svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-close') }}"></use></svg> {{t('wap', 'Hủy bỏ') }}</a>*/
/*     </div>*/
/* </div>*/
/* {% endif %}*/
