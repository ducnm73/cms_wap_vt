<?php

/* relatedVideoBox.twig */
class __TwigTemplate_4858c30f248f65c4cf950fc9922d1f266f09f81286f4205b6558cca697fc8b9e extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"blk blkBdTop blkListVideosVer\">
    ";
        // line 2
        if (((isset($context["type"]) ? $context["type"] : null) == "FILM")) {
            // line 3
            echo "        <h6 class=\"blkTitle\">
            ";
            // line 4
            echo twig_escape_filter($this->env, Yii::t("wap", "Phim cùng thể loại"), "html", null, true);
            echo "
        </h6>
        <input type=\"hidden\" id=\"autoPlay\" value=\"0\" />
    ";
        } else {
            // line 8
            echo "        <h6 class=\"blkTitle\">
            ";
            // line 9
            echo twig_escape_filter($this->env, Yii::t("wap", "Xem tiếp"), "html", null, true);
            echo "
            <div class=\"rightCn f1em\">
                ";
            // line 11
            echo twig_escape_filter($this->env, Yii::t("wap", "Tự động phát"), "html", null, true);
            echo "
                <div onclick=\"autoPlayVideoV2()\" class=\"onOff switch\">
                    <svg class=\"ico active\"><use xlink:href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-on"), "html", null, true);
            echo "\"></use></svg>
                    <svg class=\"ico\"><use xlink:href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-off"), "html", null, true);
            echo "\"></use></svg>
                </div>
            </div>
        </h6>
        <input type=\"hidden\" id=\"autoPlay\" value=\"1\" />
    ";
        }
        // line 20
        echo "    ";
        $context["nextVideo"] = "";
        // line 21
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["k"] => $context["video"]) {
            // line 22
            echo "    ";
            if (($context["k"] == 0)) {
                echo " 
        ";
                // line 23
                $context["nextVideo"] = $this->getAttribute($context["video"], "name", array());
                // line 24
                echo "        <input type=\"hidden\" id=\"nextVideo\" value=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
                echo "\" />
    ";
            }
            // line 26
            echo "        <article class=\"videoItem\">
        <div class=\"ctn\">
            <div class=\"pthld\">
\t\t\t<span class=\"timing\">";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "duration", array()), "html", null, true);
            echo "</span>
\t\t\t\t<a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "\">
\t\t\t\t\t<img src=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "coverImage", array()), "html", null, true);
            echo "\" onError=\"this.onerror=null;this.src='/images/16x9.png'\" alt=\"\">
                    ";
            // line 32
            if (($this->getAttribute($context["video"], "price_play", array()) == 0)) {
                // line 33
                echo "                        <div class=\"attach-img\">
                            <img src=\"/images/freeVideo.png\">
                        </div>
                    ";
            }
            // line 37
            echo "\t\t\t\t</a>
            </div>
            <div class=\"ctnhld\">
                <h6 class=\"title\">
                    <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "</a>
                </h6>
                <p class=\"txtInfo\">
                    <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-detail", array("id" => $this->getAttribute($context["video"], "userId", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "userName", array()), "html", null, true);
            echo "</a> <i class=\"dot\"></i>
\t\t\t\t\t<a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "{count} lượt xem", array("count" => $this->getAttribute($context["video"], "play_times", array()))), "html", null, true);
            echo "</a>
                </p>
            </div>
            <a class=\"icoMore optMnBtn\" data-target=\"#videoOpt\" data-id=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\">
                <svg class=\"ico\"><use xlink:href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-more"), "html", null, true);
            echo "\"></use></svg>
            </a>
        </div>
    </article>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['k'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "

</section>

<div class=\"dDownComment\" style=\"text-align: center\">
    <a class=\"trans link-view-more\" id=\"load-more-related-video\">
        <svg class=\"ico\">
            <use xlink:href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-arrow-down-ol"), "html", null, true);
        echo "\"></use>
        </svg>
    </a>
</div>


<div  id=\"modalAutoPlay\" style=\"display:none;\">
    <div class=\"vjs-modal-dialog\" tabindex=\"-1\">
        <div class=\"vjs-modal-dialog-content\" role=\"document\">

            <div class=\"text-center\" onclick=\"window.location=document.getElementById('nextVideo').value\">
                <h5>";
        // line 72
        echo twig_escape_filter($this->env, Yii::t("wap", "Tiếp Theo"), "html", null, true);
        echo "</h5>
                <p><b>";
        // line 73
        echo twig_escape_filter($this->env, (isset($context["nextVideo"]) ? $context["nextVideo"] : null), "html", null, true);
        echo "</b></p>
                <p id=\"countDown\">
                    <span class=\"player-next-s3\"></span>
                    <svg id=\"animated\" width=\"60px\" heigh=\"60px\" viewbox=\"0 0 100 100\">
                        <circle cx=\"50\" cy=\"50\" r=\"45\" fill=\"url(#image)\"/>
                        <path fill=\"none\" stroke-linecap=\"round\" stroke-width=\"5\" stroke=\"#fff\"
                            stroke-dasharray=\"251.2,0\"
                            d=\"M50 10
                               a 40 40 0 0 1 0 80
                               a 40 40 0 0 1 0 -80\">
                            <animate attributeName=\"stroke-dasharray\" from=\"0,251.2\" to=\"251.2,0\" dur=\"5s\"/>           
                        </path>
                    </svg>
                </p>
            </div>

            <div class=\"text-center\" style=\"margin-top: 25px\">
                <p class=\"btn-cancel-next-video\" >
                    <button class=\"a-cancel-next-video\" onclick=\"cancelNextVideo()\" > ";
        // line 91
        echo twig_escape_filter($this->env, Yii::t("wap", "BỎ QUA"), "html", null, true);
        echo " </button>
                </p>
            </div>

        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "relatedVideoBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 91,  182 => 73,  178 => 72,  164 => 61,  155 => 54,  144 => 49,  138 => 48,  130 => 45,  124 => 44,  114 => 41,  108 => 37,  102 => 33,  100 => 32,  96 => 31,  90 => 30,  86 => 29,  81 => 26,  75 => 24,  73 => 23,  68 => 22,  63 => 21,  60 => 20,  51 => 14,  47 => 13,  42 => 11,  37 => 9,  34 => 8,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <section class="blk blkBdTop blkListVideosVer">*/
/*     {% if type == 'FILM' %}*/
/*         <h6 class="blkTitle">*/
/*             {{t('wap', 'Phim cùng thể loại') }}*/
/*         </h6>*/
/*         <input type="hidden" id="autoPlay" value="0" />*/
/*     {% else %}*/
/*         <h6 class="blkTitle">*/
/*             {{t('wap', 'Xem tiếp') }}*/
/*             <div class="rightCn f1em">*/
/*                 {{t('wap', 'Tự động phát') }}*/
/*                 <div onclick="autoPlayVideoV2()" class="onOff switch">*/
/*                     <svg class="ico active"><use xlink:href="{{ url('images/defs.svg#ico-on') }}"></use></svg>*/
/*                     <svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-off') }}"></use></svg>*/
/*                 </div>*/
/*             </div>*/
/*         </h6>*/
/*         <input type="hidden" id="autoPlay" value="1" />*/
/*     {% endif %}*/
/*     {% set nextVideo = '' %}*/
/*     {% for k,video in data %}*/
/*     {% if k == 0%} */
/*         {% set nextVideo = video.name %}*/
/*         <input type="hidden" id="nextVideo" value="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}" />*/
/*     {% endif %}*/
/*         <article class="videoItem">*/
/*         <div class="ctn">*/
/*             <div class="pthld">*/
/* 			<span class="timing">{{video.duration}}</span>*/
/* 				<a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}" title="{{video.name}}">*/
/* 					<img src="{{ video.coverImage }}" onError="this.onerror=null;this.src='/images/16x9.png'" alt="">*/
/*                     {% if(video.price_play == 0) %}*/
/*                         <div class="attach-img">*/
/*                             <img src="/images/freeVideo.png">*/
/*                         </div>*/
/*                     {% endif %}*/
/* 				</a>*/
/*             </div>*/
/*             <div class="ctnhld">*/
/*                 <h6 class="title">*/
/*                     <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}" title="{{video.name}}">{{video.name}}</a>*/
/*                 </h6>*/
/*                 <p class="txtInfo">*/
/*                     <a href="{{ url('channel/get-detail',{'id':video.userId }) }}">{{video.userName}}</a> <i class="dot"></i>*/
/* 					<a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">{{ t('wap','{count} lượt xem', {'count': video.play_times}) }}</a>*/
/*                 </p>*/
/*             </div>*/
/*             <a class="icoMore optMnBtn" data-target="#videoOpt" data-id="{{video.id}}" data-url="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">*/
/*                 <svg class="ico"><use xlink:href="{{ url('images/defs.svg#ico-more') }}"></use></svg>*/
/*             </a>*/
/*         </div>*/
/*     </article>*/
/*     {% endfor %}*/
/* */
/* */
/* </section>*/
/* */
/* <div class="dDownComment" style="text-align: center">*/
/*     <a class="trans link-view-more" id="load-more-related-video">*/
/*         <svg class="ico">*/
/*             <use xlink:href="{{ url('images/defs.svg#ico-arrow-down-ol') }}"></use>*/
/*         </svg>*/
/*     </a>*/
/* </div>*/
/* */
/* */
/* <div  id="modalAutoPlay" style="display:none;">*/
/*     <div class="vjs-modal-dialog" tabindex="-1">*/
/*         <div class="vjs-modal-dialog-content" role="document">*/
/* */
/*             <div class="text-center" onclick="window.location=document.getElementById('nextVideo').value">*/
/*                 <h5>{{t('wap', 'Tiếp Theo') }}</h5>*/
/*                 <p><b>{{nextVideo}}</b></p>*/
/*                 <p id="countDown">*/
/*                     <span class="player-next-s3"></span>*/
/*                     <svg id="animated" width="60px" heigh="60px" viewbox="0 0 100 100">*/
/*                         <circle cx="50" cy="50" r="45" fill="url(#image)"/>*/
/*                         <path fill="none" stroke-linecap="round" stroke-width="5" stroke="#fff"*/
/*                             stroke-dasharray="251.2,0"*/
/*                             d="M50 10*/
/*                                a 40 40 0 0 1 0 80*/
/*                                a 40 40 0 0 1 0 -80">*/
/*                             <animate attributeName="stroke-dasharray" from="0,251.2" to="251.2,0" dur="5s"/>           */
/*                         </path>*/
/*                     </svg>*/
/*                 </p>*/
/*             </div>*/
/* */
/*             <div class="text-center" style="margin-top: 25px">*/
/*                 <p class="btn-cancel-next-video" >*/
/*                     <button class="a-cancel-next-video" onclick="cancelNextVideo()" > {{t('wap', 'BỎ QUA') }} </button>*/
/*                 </p>*/
/*             </div>*/
/* */
/*         </div>*/
/*     </div>*/
/* </div>*/
