<?php

/* comment.twig */
class __TwigTemplate_29a8c1e1aca2d1e952a2bdf6f7cce296a3048e5e8d0cd96772f44c3546d74b80 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((twig_length_filter($this->env, (isset($context["comments"]) ? $context["comments"] : null)) == 0) && ((isset($context["offset"]) ? $context["offset"] : null) == 0))) {
            // line 2
            echo "    <p class=\"ctn\" id=\"blank_comment\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Chưa có bình luận."), "html", null, true);
            echo "</p>
    <input type=\"hidden\" id=\"comment-empty\">
";
        } elseif ((twig_length_filter($this->env,         // line 4
(isset($context["comments"]) ? $context["comments"] : null)) == 0)) {
            // line 5
            echo "    <input type=\"hidden\" id=\"comment-empty\">
";
        }
        // line 7
        echo "
";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comments"]) ? $context["comments"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["parentComment"]) {
            // line 9
            $context["childCommentNum"] = twig_length_filter($this->env, $this->getAttribute($context["parentComment"], "children", array()));
            // line 10
            echo "<article class=\"cmtItem media-cm-video\" id=\"cm_";
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
            echo "\">
    <div class=\"user ctn\">
        <div class=\"avatar pthld left\"><a href=\"javascript:void(0);\"><img src=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "avatarImage", array()), "html", null, true);
            echo "\" alt=\"\"></a></div>
        <div class=\"ctnhld_v1\">
            <div class=\"title comment-textarea-linhtvn\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "comment", array()), "html", null, true);
            echo "</div>
            <p class=\"txtInfo\">
                <a href=\"javascript:void(0);\">";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "full_name", array()), "html", null, true);
            echo "</a> <i class=\"dot\"></i>
                ";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->dateAgo($this->getAttribute($context["parentComment"], "created_at", array())), "html", null, true);
            echo "
            </p>
            <ul class=\"feedback\">
                <li>           
                      <span class=\"col\">
                        <a href=\"javascript:void(0);\" data-like=\"\" data-commentid=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
            echo "\" data-contentId=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "id", array()), "html", null, true);
            echo "\" class=\"btnLikeComment\" title=\"";
            echo twig_escape_filter($this->env, Yii::t("wap", "Thích"), "html", null, true);
            echo "\">
                            <span class=\"likeCommentNumber_";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "like_count", array()), "html", null, true);
            echo "&nbsp;</span>";
            if (($this->getAttribute($context["parentComment"], "like", array()) == 1)) {
                echo "<i id=\"icoLikedComment_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                echo "\" class=\"fa fa-thumbs-up\" aria-hidden=\"true\"></i>";
            } else {
                echo "<i id=\"icoLikedComment_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                echo "\" class=\"fa fa-thumbs-o-up\" aria-hidden=\"true\"></i>";
            }
            // line 24
            echo "                        </a>
                      </span>
                        <span class=\"col\">
                          <a href=\"javascript:void(0);\" data-like=\"\" data-commentid=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
            echo "\" data-contentId=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "id", array()), "html", null, true);
            echo "\" class=\"btnDisLikeComment\" title=\"";
            echo twig_escape_filter($this->env, Yii::t("wap", "Không thích"), "html", null, true);
            echo "\">
                              <span class=\"disLikeCommentNumber_";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "dislike_count", array()), "html", null, true);
            echo "&nbsp;</span>";
            if (($this->getAttribute($context["parentComment"], "like", array()) == 2)) {
                echo "<i id=\"icoDisLikedComment_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                echo "\" class=\"fa fa-thumbs-down\" aria-hidden=\"true\"></i>";
            } else {
                echo "<i id=\"icoDisLikedComment_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                echo "\" class=\"fa fa-thumbs-o-down\" aria-hidden=\"true\"></i>";
            }
            // line 29
            echo "                          </a>
                      </span>
                </li>
                ";
            // line 32
            if (($this->getAttribute($context["parentComment"], "is_comment", array()) > 0)) {
                // line 33
                echo "                    ";
                if (($this->getAttribute($context["parentComment"], "status", array()) == 1)) {
                    // line 34
                    echo "                        <li class=\"answerComment\">
                            <a href=\"javascript:void(0)\" data-id=\"";
                    // line 35
                    echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                    echo "\" class=\"btnReplyComment\" id=\"btnReplyComment_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Trả lời"), "html", null, true);
                    echo "</a>
                        </li>
                    ";
                }
                // line 38
                echo "                ";
            }
            // line 39
            echo "            </ul>
            <div class=\"boxReply\"></div>

            ";
            // line 42
            $context["count"] = 0;
            // line 43
            echo "            ";
            if (((isset($context["childCommentNum"]) ? $context["childCommentNum"] : null) > 0)) {
                // line 44
                echo "            <ul>
                ";
                // line 45
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["parentComment"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["childComment"]) {
                    // line 46
                    echo "                ";
                    $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                    // line 47
                    echo "                <li class=\"";
                    if (((isset($context["count"]) ? $context["count"] : null) > 1)) {
                        echo "cm_group_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                        echo " hidden";
                    }
                    echo "\">
                    <article class=\"cmtItem\">
                        <div class=\"user ctn\">
                            <div class=\"avatar pthld left\"><a href=\"javascript:void(0);\"><img src=\"";
                    // line 50
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "avatarImage", array()), "html", null, true);
                    echo "\" alt=\"\"></a></div>
                            <div class=\"ctnhld\">
                                <div class=\"title\">";
                    // line 52
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "comment", array()), "html", null, true);
                    echo "</div>
                                <p class=\"txtInfo\">
                                    <a href=\"javascript:void(0);\">";
                    // line 54
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "full_name", array()), "html", null, true);
                    echo "</a> <i class=\"dot\"></i>
                                    ";
                    // line 55
                    echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->dateAgo($this->getAttribute($context["childComment"], "created_at", array())), "html", null, true);
                    echo "
                                </p>
                                <ul class=\"feedback\">
                                    <li>
                                          <span class=\"col\">
                                            <a href=\"javascript:void(0);\" data-like=\"\" data-commentid=\"";
                    // line 60
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                    echo "\" data-contentId=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "id", array()), "html", null, true);
                    echo "\" class=\"btnLikeComment\" title=\"";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Thích"), "html", null, true);
                    echo "\">
                                                <span class=\"likeCommentNumber_";
                    // line 61
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "like_count", array()), "html", null, true);
                    echo "&nbsp;</span>";
                    if (($this->getAttribute($context["childComment"], "like", array()) == 1)) {
                        echo "<i id=\"icoLikedComment_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                        echo "\" class=\"fa fa-thumbs-up\" aria-hidden=\"true\"></i>";
                    } else {
                        echo "<i id=\"icoLikedComment_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                        echo "\" class=\"fa fa-thumbs-o-up\" aria-hidden=\"true\"></i>";
                    }
                    // line 62
                    echo "                                            </a>
                                          </span>
                                        <span class=\"col\">
                                              <a href=\"javascript:void(0);\" data-like=\"\" data-commentid=\"";
                    // line 65
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                    echo "\" data-contentId=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["video"]) ? $context["video"] : null), "id", array()), "html", null, true);
                    echo "\" class=\"btnDisLikeComment\" title=\"";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Không thích"), "html", null, true);
                    echo "\">
                                                  <span class=\"disLikeCommentNumber_";
                    // line 66
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "dislike_count", array()), "html", null, true);
                    echo "&nbsp;</span>";
                    if ((($this->getAttribute($context["childComment"], "like", array()) == 0) &&  !(null === $this->getAttribute($context["childComment"], "like", array())))) {
                        echo "<i id=\"icoDisLikedComment_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                        echo "\" class=\"fa fa-thumbs-down\" aria-hidden=\"true\"></i>";
                    } else {
                        echo "<i id=\"icoDisLikedComment_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["childComment"], "id", array()), "html", null, true);
                        echo "\" class=\"fa fa-thumbs-o-down\" aria-hidden=\"true\"></i>";
                    }
                    // line 67
                    echo "                                              </a>
                                          </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </li>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['childComment'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 76
                echo "            </ul>
            ";
            }
            // line 78
            echo "            <!--end sub comment-->
            ";
            // line 79
            if (((isset($context["childCommentNum"]) ? $context["childCommentNum"] : null) > 2)) {
                // line 80
                echo "                <div class=\"showMore\"><a href=\"javascript:void(0);\" data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["parentComment"], "id", array()), "html", null, true);
                echo "\" class=\"viewMoreComment\">";
                echo twig_escape_filter($this->env, Yii::t("wap", "Xem"), "html", null, true);
                echo "&nbsp; ";
                echo twig_escape_filter($this->env, ((isset($context["childCommentNum"]) ? $context["childCommentNum"] : null) - 2), "html", null, true);
                echo "&nbsp; ";
                echo twig_escape_filter($this->env, Yii::t("wap", "câu trả lời trước"), "html", null, true);
                echo "</a> </div>
                <!--sub comment-->
            ";
            }
            // line 83
            echo "        </div>
    </div>
</article>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['parentComment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "comment.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 83,  270 => 80,  268 => 79,  265 => 78,  261 => 76,  247 => 67,  233 => 66,  225 => 65,  220 => 62,  206 => 61,  198 => 60,  190 => 55,  186 => 54,  181 => 52,  176 => 50,  165 => 47,  162 => 46,  158 => 45,  155 => 44,  152 => 43,  150 => 42,  145 => 39,  142 => 38,  132 => 35,  129 => 34,  126 => 33,  124 => 32,  119 => 29,  105 => 28,  97 => 27,  92 => 24,  78 => 23,  70 => 22,  62 => 17,  58 => 16,  53 => 14,  48 => 12,  42 => 10,  40 => 9,  36 => 8,  33 => 7,  29 => 5,  27 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if (comments|length ==0) and offset == 0 %}*/
/*     <p class="ctn" id="blank_comment">{{t('wap', 'Chưa có bình luận.') }}</p>*/
/*     <input type="hidden" id="comment-empty">*/
/* {% elseif (comments|length ==0) %}*/
/*     <input type="hidden" id="comment-empty">*/
/* {% endif %}*/
/* */
/* {% for parentComment in comments %}*/
/* {% set childCommentNum = parentComment.children|length %}*/
/* <article class="cmtItem media-cm-video" id="cm_{{parentComment.id}}">*/
/*     <div class="user ctn">*/
/*         <div class="avatar pthld left"><a href="javascript:void(0);"><img src="{{ parentComment.avatarImage }}" alt=""></a></div>*/
/*         <div class="ctnhld_v1">*/
/*             <div class="title comment-textarea-linhtvn">{{ parentComment.comment }}</div>*/
/*             <p class="txtInfo">*/
/*                 <a href="javascript:void(0);">{{ parentComment.full_name }}</a> <i class="dot"></i>*/
/*                 {{ parentComment.created_at|dateAgo }}*/
/*             </p>*/
/*             <ul class="feedback">*/
/*                 <li>           */
/*                       <span class="col">*/
/*                         <a href="javascript:void(0);" data-like="" data-commentid="{{ parentComment.id }}" data-contentId="{{ video.id }}" class="btnLikeComment" title="{{ t('wap','Thích')}}">*/
/*                             <span class="likeCommentNumber_{{ parentComment.id }}">{{ parentComment.like_count }}&nbsp;</span>{% if (parentComment.like == 1) %}<i id="icoLikedComment_{{ parentComment.id }}" class="fa fa-thumbs-up" aria-hidden="true"></i>{% else %}<i id="icoLikedComment_{{ parentComment.id }}" class="fa fa-thumbs-o-up" aria-hidden="true"></i>{% endif %}*/
/*                         </a>*/
/*                       </span>*/
/*                         <span class="col">*/
/*                           <a href="javascript:void(0);" data-like="" data-commentid="{{ parentComment.id }}" data-contentId="{{ video.id }}" class="btnDisLikeComment" title="{{ t('wap','Không thích')}}">*/
/*                               <span class="disLikeCommentNumber_{{ parentComment.id }}">{{ parentComment.dislike_count }}&nbsp;</span>{% if (parentComment.like == 2) %}<i id="icoDisLikedComment_{{ parentComment.id }}" class="fa fa-thumbs-down" aria-hidden="true"></i>{% else %}<i id="icoDisLikedComment_{{ parentComment.id }}" class="fa fa-thumbs-o-down" aria-hidden="true"></i>{% endif %}*/
/*                           </a>*/
/*                       </span>*/
/*                 </li>*/
/*                 {% if parentComment.is_comment > 0 %}*/
/*                     {% if parentComment.status ==1 %}*/
/*                         <li class="answerComment">*/
/*                             <a href="javascript:void(0)" data-id="{{parentComment.id}}" class="btnReplyComment" id="btnReplyComment_{{parentComment.id}}">{{ t('wap','Trả lời') }}</a>*/
/*                         </li>*/
/*                     {% endif %}*/
/*                 {% endif %}*/
/*             </ul>*/
/*             <div class="boxReply"></div>*/
/* */
/*             {% set count =0 %}*/
/*             {% if childCommentNum > 0 %}*/
/*             <ul>*/
/*                 {% for childComment in parentComment.children %}*/
/*                 {% set count = count +1 %}*/
/*                 <li class="{% if count > 1 %}cm_group_{{ parentComment.id }} hidden{% endif %}">*/
/*                     <article class="cmtItem">*/
/*                         <div class="user ctn">*/
/*                             <div class="avatar pthld left"><a href="javascript:void(0);"><img src="{{ childComment.avatarImage }}" alt=""></a></div>*/
/*                             <div class="ctnhld">*/
/*                                 <div class="title">{{ childComment.comment }}</div>*/
/*                                 <p class="txtInfo">*/
/*                                     <a href="javascript:void(0);">{{ childComment.full_name }}</a> <i class="dot"></i>*/
/*                                     {{ childComment.created_at|dateAgo }}*/
/*                                 </p>*/
/*                                 <ul class="feedback">*/
/*                                     <li>*/
/*                                           <span class="col">*/
/*                                             <a href="javascript:void(0);" data-like="" data-commentid="{{ childComment.id }}" data-contentId="{{ video.id }}" class="btnLikeComment" title="{{ t('wap','Thích')}}">*/
/*                                                 <span class="likeCommentNumber_{{ childComment.id }}">{{ childComment.like_count }}&nbsp;</span>{% if (childComment.like == 1) %}<i id="icoLikedComment_{{ childComment.id }}" class="fa fa-thumbs-up" aria-hidden="true"></i>{% else %}<i id="icoLikedComment_{{ childComment.id }}" class="fa fa-thumbs-o-up" aria-hidden="true"></i>{% endif %}*/
/*                                             </a>*/
/*                                           </span>*/
/*                                         <span class="col">*/
/*                                               <a href="javascript:void(0);" data-like="" data-commentid="{{ childComment.id }}" data-contentId="{{ video.id }}" class="btnDisLikeComment" title="{{ t('wap','Không thích')}}">*/
/*                                                   <span class="disLikeCommentNumber_{{ childComment.id }}">{{ childComment.dislike_count }}&nbsp;</span>{% if (childComment.like == 0 and childComment.like is not null) %}<i id="icoDisLikedComment_{{ childComment.id }}" class="fa fa-thumbs-down" aria-hidden="true"></i>{% else %}<i id="icoDisLikedComment_{{ childComment.id }}" class="fa fa-thumbs-o-down" aria-hidden="true"></i>{% endif %}*/
/*                                               </a>*/
/*                                           </span>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                     </article>*/
/*                 </li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*             {% endif %}*/
/*             <!--end sub comment-->*/
/*             {% if childCommentNum > 2 %}*/
/*                 <div class="showMore"><a href="javascript:void(0);" data-id="{{parentComment.id}}" class="viewMoreComment">{{ t('wap','Xem') }}&nbsp; {{ childCommentNum - 2 }}&nbsp; {{ t('wap','câu trả lời trước') }}</a> </div>*/
/*                 <!--sub comment-->*/
/*             {% endif %}*/
/*         </div>*/
/*     </div>*/
/* </article>*/
/* {% endfor %}*/
/* */
