<?php

/* @app/views/partials/trackingView.twig */
class __TwigTemplate_ac20a668e4fc3420ae025b20a8e95d093cc34fef4169643ccbc134599fe3576d extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "@app/views/partials/trackingView.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }
}
/* {#<script type='text/javascript'>*/
/*         Countly.q.push(['add_event', {*/
/*             "key":"{{ trackingContent.id }}",*/
/*             "segmentation": {*/
/*                 {% for key, value in trackingContent.content %}*/
/*                     {% if key == 'user_info' %}*/
/*                         "{{ key }}": {{ value|raw }},*/
/*                     {% else %}*/
/*                         "{{ key }}": "{{ value }}",*/
/*                     {% endif %}*/
/*                 {% endfor %}*/
/*             }*/
/*         }]);*/
/* </script>#}*/
