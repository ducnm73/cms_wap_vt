<?php

/* listPackage.twig */
class __TwigTemplate_4bd1517350cf21109eda371887d89a27e626ba6397ecb1fe60e42c7197e3bda1 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ListSmallAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "list_small_asset");
        echo "

";
        // line 4
        $context["packages"] = $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array());
        // line 5
        $context["isConfirmSMS"] = $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "isConfirmSMS", array());
        // line 6
        $context["showMessage"] = 0;
        // line 7
        echo "
    ";
        // line 8
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("title" => Yii::t("wap", "Gói cước"), "type" => "sub")), "method");
        echo "

    <section class=\"blk blkPackages blkPadBoth\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packages"]) ? $context["packages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["package"]) {
            // line 12
            echo "            ";
            if ($this->getAttribute($context["package"], "status", array())) {
                // line 13
                echo "                ";
                $context["showMessage"] = 1;
                // line 14
                echo "            ";
            }
            // line 15
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['package'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
        ";
        // line 17
        if ((isset($context["showMessage"]) ? $context["showMessage"] : null)) {
            // line 18
            echo "            <p class=\"ctn\">
                ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "infoMessage", array()), "html", null, true);
            echo "
            </p>
        ";
        } elseif (        // line 21
(isset($context["msisdn"]) ? $context["msisdn"] : null)) {
            // line 22
            echo "            <p class=\"ctn\">
                ";
            // line 23
            echo twig_escape_filter($this->env, Yii::t("wap", "Xin chào {msisdn}", array("msisdn" => (isset($context["msisdn"]) ? $context["msisdn"] : null))), "html", null, true);
            echo ",
            </p>
        ";
        }
        // line 26
        echo "
        ";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["packages"]) ? $context["packages"] : null)) == 0)) {
            // line 28
            echo "            <p class=\"ctn\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Không có gói cước"), "html", null, true);
            echo "</p>
        ";
        }
        // line 30
        echo "
        ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packages"]) ? $context["packages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["package"]) {
            // line 32
            echo "            <article class=\"ctn pkItem\">
                <h5>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "name", array()), "html", null, true);
            echo " ";
            if (($this->getAttribute($context["package"], "status", array()) == 1)) {
                echo "<span class=\"right reged\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-checked\"></use></svg> ";
                echo twig_escape_filter($this->env, Yii::t("wap", "Đã đăng ký"), "html", null, true);
                echo "</span>";
            }
            echo "</h5>
                <p class=\"clrGrey\">";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "description", array()), "html", null, true);
            echo "</p>
                <input type=\"hidden\" class=\"package_id\" value=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
            echo "\"/>
                ";
            // line 36
            if (($this->getAttribute($context["package"], "status", array()) == 0)) {
                // line 37
                echo "                    ";
                if ((isset($context["isConfirmSMS"]) ? $context["isConfirmSMS"] : null)) {
                    // line 38
                    echo "                        <a ";
                    if ((isset($context["userId"]) ? $context["userId"] : null)) {
                        echo "  href=\"javascript:void(0)\"  onclick=\"document.getElementById('";
                        echo twig_escape_filter($this->env, ("f_" . $this->getAttribute($context["package"], "id", array())), "html", null, true);
                        echo "').submit();\" ";
                    } else {
                        echo " class=\"btn btnBlue\" href=\"/auth/login\" ";
                    }
                    echo "  class=\"btn btnBlue\" >";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký"), "html", null, true);
                    echo "</a>
                    ";
                } else {
                    // line 40
                    echo "                        <a ";
                    if ((isset($context["userId"]) ? $context["userId"] : null)) {
                        echo "  href=\"javascript:void(0)\" data-target=\"#package_popup_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                        echo "_step2\" ";
                    } else {
                        echo " class=\"btn btnBlue\" href=\"/auth/login\" ";
                    }
                    echo "  class=\"btn btnBlue modalBtn\" >";
                    echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký"), "html", null, true);
                    echo "</a>
                    ";
                }
                // line 42
                echo "
                ";
            } else {
                // line 44
                echo "                    <a href=\"javascript:void(0)\" class=\"btn btnOutline modalBtn\"
                       data-target=\"#package_popup_";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "_step1\">";
                echo twig_escape_filter($this->env, Yii::t("wap", "Hủy gói"), "html", null, true);
                echo "</a>
                ";
            }
            // line 47
            echo "            </article>
            ";
            // line 48
            if (($this->getAttribute($context["package"], "status", array()) == 0)) {
                // line 49
                echo "                <div class=\"modal modalPopup\" id=\"package_popup_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "_step1\">
                    <div class=\"ctn\">
                        <h5>";
                // line 51
                echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký gói cước"), "html", null, true);
                echo "</h5>
                        <p class=\"clrGrey\"> ";
                // line 52
                echo $this->getAttribute($this->getAttribute($context["package"], "popup", array()), 0, array(), "array");
                echo " </p>
                        <div class=\"action alRight\">
                            <form action=\"/account/register-service\" method=\"post\" id=";
                // line 54
                echo twig_escape_filter($this->env, ("f_" . $this->getAttribute($context["package"], "id", array())), "html", null, true);
                echo " >
                                <input type=\"hidden\" name=\"package_id\" value=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "\"/>
                                <input type=\"hidden\"
                                       name=\"";
                // line 57
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
                echo "\"
                                       value=\"";
                // line 58
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
                echo "\">
                                <a class=\"ok modalBtn\" href=\"javascript:void(0)\" data-target=\"#package_popup_";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "_step2\">";
                echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký"), "html", null, true);
                echo "</a>
                                <a class=\"close\" href=\"javascript:void(0)\">";
                // line 60
                echo twig_escape_filter($this->env, Yii::t("wap", "Để sau"), "html", null, true);
                echo "</a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class=\"modal modalPopup\" id=\"package_popup_";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "_step2\">
                    <div class=\"ctn\">
                        <h5>";
                // line 67
                echo twig_escape_filter($this->env, Yii::t("wap", "Xác nhận mua gói cước"), "html", null, true);
                echo "</h5>
                        <p class=\"clrGrey\">";
                // line 68
                echo $this->getAttribute($this->getAttribute($context["package"], "popup", array()), 1, array(), "array");
                echo "</p>
                        <div class=\"action alRight\">
                            <form action=\"/account/register-service\"  method=\"post\">
                                <input type=\"hidden\" name=\"package_id\" value=\"";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "\"/>
                                <input type=\"hidden\"
                                       name=\"";
                // line 73
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
                echo "\"
                                       value=\"";
                // line 74
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
                echo "\">
                                <a class=\"ok okSubmit\" href=\"javascript:void(0)\" onsubmit=\"return addEvent('goi_cuoc_register_action', {'goi_cuoc_type': '";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "type", array()), "html", null, true);
                echo "', 'goi_cuoc_id': '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "', 'goi_cuoc_price': '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "fee", array()), "html", null, true);
                echo "'});\">";
                echo twig_escape_filter($this->env, Yii::t("wap", "Đồng ý"), "html", null, true);
                echo "</a>
                                <a class=\"close\" href=\"javascript:void(0)\">";
                // line 76
                echo twig_escape_filter($this->env, Yii::t("wap", "Để sau"), "html", null, true);
                echo "</a>
                            </form>
                        </div>
                    </div>
                </div>
            ";
            } else {
                // line 82
                echo "                <div class=\"modal modalPopup\" id=\"package_popup_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "_step1\">
                    <div class=\"ctn\">
                        <h5>";
                // line 84
                echo twig_escape_filter($this->env, Yii::t("wap", "Hủy gói cước"), "html", null, true);
                echo "</h5>
                        <p class=\"clrGrey\">";
                // line 85
                echo $this->getAttribute($this->getAttribute($context["package"], "popup", array()), 0, array(), "array");
                echo "</p>
                        <div class=\"action alRight\">
                            <form action=\"/account/unregister-service\"  method=\"post\">
                                <input type=\"hidden\" name=\"package_id\" value=\"";
                // line 88
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "\"/>
                                <input type=\"hidden\"
                                       name=\"";
                // line 90
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
                echo "\"
                                       value=\"";
                // line 91
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
                echo "\">
                                <a class=\"ok okSubmit\" href=\"javascript:void(0)\" onclick=\"return addEvent('goi_cuoc_cancel_action', {'goi_cuoc_type': '";
                // line 92
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "type", array()), "html", null, true);
                echo "', 'goi_cuoc_id': '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
                echo "', 'goi_cuoc_price': '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "fee", array()), "html", null, true);
                echo "'});\">";
                echo twig_escape_filter($this->env, Yii::t("wap", "Hủy dịch vụ"), "html", null, true);
                echo "</a>
                                <a class=\"close\" href=\"javascript:void(0)\">";
                // line 93
                echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
                echo "</a>
                            </form>
                        </div>
                    </div>
                </div>
            ";
            }
            // line 99
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['package'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "
    </section>





";
    }

    public function getTemplateName()
    {
        return "listPackage.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 100,  318 => 99,  309 => 93,  299 => 92,  295 => 91,  291 => 90,  286 => 88,  280 => 85,  276 => 84,  270 => 82,  261 => 76,  251 => 75,  247 => 74,  243 => 73,  238 => 71,  232 => 68,  228 => 67,  223 => 65,  215 => 60,  209 => 59,  205 => 58,  201 => 57,  196 => 55,  192 => 54,  187 => 52,  183 => 51,  177 => 49,  175 => 48,  172 => 47,  165 => 45,  162 => 44,  158 => 42,  144 => 40,  130 => 38,  127 => 37,  125 => 36,  121 => 35,  117 => 34,  107 => 33,  104 => 32,  100 => 31,  97 => 30,  91 => 28,  89 => 27,  86 => 26,  80 => 23,  77 => 22,  75 => 21,  70 => 19,  67 => 18,  65 => 17,  62 => 16,  56 => 15,  53 => 14,  50 => 13,  47 => 12,  43 => 11,  37 => 8,  34 => 7,  32 => 6,  30 => 5,  28 => 4,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/ListSmallAsset') }}*/
/* {{ register_list_small_asset() }}*/
/* */
/* {% set packages = responseData.data %}*/
/* {% set isConfirmSMS = responseData.isConfirmSMS %}*/
/* {% set showMessage =0 %}*/
/* */
/*     {{ HeaderWidget.widget({'title': t('wap', 'Gói cước') ,'type':'sub'}) | raw }}*/
/* */
/*     <section class="blk blkPackages blkPadBoth">*/
/*         {% for package in packages %}*/
/*             {% if package.status %}*/
/*                 {% set showMessage =1 %}*/
/*             {% endif %}*/
/*         {% endfor %}*/
/* */
/*         {% if showMessage %}*/
/*             <p class="ctn">*/
/*                 {{ responseData.infoMessage }}*/
/*             </p>*/
/*         {% elseif msisdn %}*/
/*             <p class="ctn">*/
/*                 {{ t('wap', 'Xin chào {msisdn}', {'msisdn': msisdn}) }},*/
/*             </p>*/
/*         {% endif %}*/
/* */
/*         {% if(packages|length == 0) %}*/
/*             <p class="ctn">{{ t('wap', 'Không có gói cước') }}</p>*/
/*         {% endif %}*/
/* */
/*         {% for package in packages %}*/
/*             <article class="ctn pkItem">*/
/*                 <h5>{{ package.name }} {% if( package.status == 1) %}<span class="right reged"><svg class="ico"><use xlink:href="/images/defs.svg#ico-checked"></use></svg> {{ t('wap', 'Đã đăng ký') }}</span>{% endif %}</h5>*/
/*                 <p class="clrGrey">{{ package.description }}</p>*/
/*                 <input type="hidden" class="package_id" value="{{ package.id }}"/>*/
/*                 {% if( package.status == 0) %}*/
/*                     {% if isConfirmSMS %}*/
/*                         <a {% if userId %}  href="javascript:void(0)"  onclick="document.getElementById('{{ 'f_'~ package.id }}').submit();" {% else %} class="btn btnBlue" href="/auth/login" {% endif %}  class="btn btnBlue" >{{ t('wap', 'Đăng ký') }}</a>*/
/*                     {% else %}*/
/*                         <a {% if userId %}  href="javascript:void(0)" data-target="#package_popup_{{ package.id }}_step2" {% else %} class="btn btnBlue" href="/auth/login" {% endif %}  class="btn btnBlue modalBtn" >{{ t('wap', 'Đăng ký') }}</a>*/
/*                     {% endif %}*/
/* */
/*                 {% else %}*/
/*                     <a href="javascript:void(0)" class="btn btnOutline modalBtn"*/
/*                        data-target="#package_popup_{{ package.id }}_step1">{{ t('wap', 'Hủy gói') }}</a>*/
/*                 {% endif %}*/
/*             </article>*/
/*             {% if package.status==0 %}*/
/*                 <div class="modal modalPopup" id="package_popup_{{ package.id }}_step1">*/
/*                     <div class="ctn">*/
/*                         <h5>{{ t('wap', 'Đăng ký gói cước') }}</h5>*/
/*                         <p class="clrGrey"> {{ package.popup[0]|raw }} </p>*/
/*                         <div class="action alRight">*/
/*                             <form action="/account/register-service" method="post" id={{ 'f_'~ package.id }} >*/
/*                                 <input type="hidden" name="package_id" value="{{ package.id }}"/>*/
/*                                 <input type="hidden"*/
/*                                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                                 <a class="ok modalBtn" href="javascript:void(0)" data-target="#package_popup_{{ package.id }}_step2">{{ t('wap', 'Đăng ký') }}</a>*/
/*                                 <a class="close" href="javascript:void(0)">{{ t('wap', 'Để sau') }}</a>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="modal modalPopup" id="package_popup_{{ package.id }}_step2">*/
/*                     <div class="ctn">*/
/*                         <h5>{{ t('wap', 'Xác nhận mua gói cước') }}</h5>*/
/*                         <p class="clrGrey">{{ package.popup[1]|raw }}</p>*/
/*                         <div class="action alRight">*/
/*                             <form action="/account/register-service"  method="post">*/
/*                                 <input type="hidden" name="package_id" value="{{ package.id }}"/>*/
/*                                 <input type="hidden"*/
/*                                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                                 <a class="ok okSubmit" href="javascript:void(0)" onsubmit="return addEvent('goi_cuoc_register_action', {'goi_cuoc_type': '{{ package.type }}', 'goi_cuoc_id': '{{ package.id }}', 'goi_cuoc_price': '{{ package.fee }}'});">{{ t('wap', 'Đồng ý') }}</a>*/
/*                                 <a class="close" href="javascript:void(0)">{{ t('wap', 'Để sau') }}</a>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             {% else %}*/
/*                 <div class="modal modalPopup" id="package_popup_{{ package.id }}_step1">*/
/*                     <div class="ctn">*/
/*                         <h5>{{ t('wap', 'Hủy gói cước') }}</h5>*/
/*                         <p class="clrGrey">{{ package.popup[0]|raw }}</p>*/
/*                         <div class="action alRight">*/
/*                             <form action="/account/unregister-service"  method="post">*/
/*                                 <input type="hidden" name="package_id" value="{{ package.id }}"/>*/
/*                                 <input type="hidden"*/
/*                                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                                 <a class="ok okSubmit" href="javascript:void(0)" onclick="return addEvent('goi_cuoc_cancel_action', {'goi_cuoc_type': '{{ package.type }}', 'goi_cuoc_id': '{{ package.id }}', 'goi_cuoc_price': '{{ package.fee }}'});">{{ t('wap', 'Hủy dịch vụ') }}</a>*/
/*                                 <a class="close" href="javascript:void(0)">{{ t('wap', 'Đóng') }}</a>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             {% endif %}*/
/*         {% endfor %}*/
/* */
/*     </section>*/
/* */
/* */
/* */
/* */
/* */
/* */
