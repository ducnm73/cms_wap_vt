<?php

/* detail.twig */
class __TwigTemplate_2009b6773acfccf4048753fd0d2356d96b91dbfbf9ed0f4eec04fa35639724d6 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ListSmallAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "list_small_asset");
        echo "

";
        // line 4
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "sub", "title" => Yii::t("wap", "Cá nhân"))), "method");
        echo "

<div>
    ";
        // line 7
        if ( !(isset($context["userId"]) ? $context["userId"] : null)) {
            // line 8
            echo "        <div class=\"blkPadBoth\">
            <section class=\"blk blkUserInfo blkUserStart\">
                <a href=\"/auth/login\" class=\"btn stBtn\">
                    <svg class=\"ico\">
                        <use xlink:href=\"/images/defs.svg#ico-settings\"></use>
                    </svg>
                </a>
                <div class=\"ctn alCenter\">
                    <div class=\"avatar\"><img src=\"/images/dfphoto.png\" alt=\"\"></div>
                    <p class=\"txtInfo\">";
            // line 17
            echo twig_escape_filter($this->env, Yii::t("wap", "Đăng nhập để trải nghiệm toàn bộ tính năng dịch vụ"), "html", null, true);
            echo "</p>
                    <p><a href=\"/auth/login\" class=\"btn btnBlue\">";
            // line 18
            echo twig_escape_filter($this->env, Yii::t("wap", "Đăng nhập"), "html", null, true);
            echo "</a></p>
                </div>
            </section>
            <section class=\"blk blkUsers\">
                <ul>
                    <li><a href=\"/goi-cuoc\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-wallet\"></use>
                            </svg>
                            ";
            // line 27
            echo twig_escape_filter($this->env, Yii::t("wap", "Gói cước"), "html", null, true);
            echo "</a></li>
                    <li><a href=\"/chinh-sach-rieng-tu\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-lock\"></use>
                            </svg>
                            ";
            // line 32
            echo twig_escape_filter($this->env, Yii::t("wap", "Điều khoản & chính sách bảo mật"), "html", null, true);
            echo "</a></li>
                    <li><a href=\"/lien-he\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-call\"></use>
                            </svg>
                            ";
            // line 37
            echo twig_escape_filter($this->env, Yii::t("wap", "Liên hệ"), "html", null, true);
            echo "</a></li>
                </ul>
            </section>
        </div>
    ";
        } else {
            // line 42
            echo "
        <div class=\"blkPadBoth\">
            <section class=\"blk blkUserInfo\">
                ";
            // line 45
            if ((isset($context["userId"]) ? $context["userId"] : null)) {
                // line 51
                echo "                    <div class=\"ctn\">
                        <div class=\"avatar boxSd\">
                            <a href=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/channel/get-user-detail", array("id" => (isset($context["userId"]) ? $context["userId"] : null))), "html", null, true);
                echo "\">
                                <img src=\"";
                // line 54
                echo twig_escape_filter($this->env, (isset($context["avatarImage"]) ? $context["avatarImage"] : null), "html", null, true);
                echo "\" alt=\"\">
                            </a>
                        </div>
                        <h6 class=\"title\"><a href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/channel/get-user-detail", array("id" => (isset($context["userId"]) ? $context["userId"] : null))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["userName"]) ? $context["userName"] : null), "html", null, true);
                echo "</a>
                            <br />
                            <a class=\"edit_user\" href=\"/user/update/";
                // line 59
                echo twig_escape_filter($this->env, (isset($context["userId"]) ? $context["userId"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa người dùng"), "html", null, true);
                echo "</a>
                        </h6>
                    </div>
                    <div class=\"banner pthld\"><img src=\"";
                // line 62
                echo twig_escape_filter($this->env, (isset($context["coverImage"]) ? $context["coverImage"] : null), "html", null, true);
                echo "\" alt=\"\" onError=\"this.onerror=null;this.src='/images/dfphoto.png'\"></div>
                ";
            }
            // line 64
            echo "            </section>

            <section class=\"blk blkUsers\">
                <ul>

                    <li><a href=\"/goi-cuoc\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-wallet\"></use>
                            </svg>
                            ";
            // line 73
            echo twig_escape_filter($this->env, Yii::t("wap", "Gói cước"), "html", null, true);
            echo "
                        </a>
                    </li>
                    <li><a href=\"/video-cua-toi\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-video\"></use>
                            </svg>
                            ";
            // line 80
            echo twig_escape_filter($this->env, Yii::t("wap", "Video của tôi"), "html", null, true);
            echo "
                        </a>
                    </li>

                    <li><a href=\"/video-da-xem\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-view\"></use>
                            </svg>
                            ";
            // line 88
            echo twig_escape_filter($this->env, Yii::t("wap", "Xem gần đây"), "html", null, true);
            echo "</a>
                    </li>
                    <li><a href=\"/video-xem-sau\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-view-later\"></use>
                            </svg>
                            ";
            // line 94
            echo twig_escape_filter($this->env, Yii::t("wap", "Xem sau"), "html", null, true);
            echo "</a>
                    </li>
                    <li><a href=\"/danh-sach-playlist\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-playlist\"></use>
                            </svg>
                            ";
            // line 100
            echo twig_escape_filter($this->env, Yii::t("wap", "Danh sách phát"), "html", null, true);
            echo "
                        </a>
                    </li>
                    <li><a href=\"/binh-luan\">
                            <i class=\"fa fa-commenting-o ico\" aria-hidden=\"true\"></i>
                            ";
            // line 105
            echo twig_escape_filter($this->env, Yii::t("wap", "Bình luận"), "html", null, true);
            echo "
                        </a>
                    </li>
                    <li><a href=\"javascript:void(0);\" class=\"item theme\" data-theme=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "themeUserId"), "method"), "html", null, true);
            echo "\">
                            <i class=\"fa fa-moon-o ico\" aria-hidden=\"true\"></i>
                            ";
            // line 110
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "themeUserId"), "method") == 1)) {
                echo twig_escape_filter($this->env, Yii::t("web", "Giao diện sáng"), "html", null, true);
            } else {
                echo " ";
                echo twig_escape_filter($this->env, Yii::t("web", "Giao diện tối"), "html", null, true);
            }
            // line 111
            echo "                        </a>
                    </li>
";
            // line 123
            echo "                    <li><a href=\"/auth/logout\" onclick=\"return addEvent('logout_action', {})\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-logout\"></use>
                            </svg>
                            ";
            // line 127
            echo twig_escape_filter($this->env, Yii::t("wap", "Đăng xuất"), "html", null, true);
            echo "</a></li>

                    <li style=\"border: solid 1px #ddd; margin: 5px 0px\"></li>

                    <li><a href=\"/auth/change-password\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-lock\"></use>
                            </svg>
                            ";
            // line 135
            echo twig_escape_filter($this->env, Yii::t("wap", "Đổi mật khẩu"), "html", null, true);
            echo "</a></li>

                    <li><a href=\"/gioi-thieu-dich-vu\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-page\"></use>
                            </svg>
                            ";
            // line 141
            echo twig_escape_filter($this->env, Yii::t("wap", "Giới thiệu"), "html", null, true);
            echo "
                        </a>
                    </li>
                    <li><a href=\"/dieu-khoan-su-dung\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-lock\"></use>
                            </svg>
                            ";
            // line 148
            echo twig_escape_filter($this->env, Yii::t("wap", "Điều khoản & chính sách bảo mật"), "html", null, true);
            echo "
                        </a>
                    </li>
                    <li><a href=\"/lien-he\">
                            <svg class=\"ico\">
                                <use xlink:href=\"/images/defs.svg#ico-call\"></use>
                            </svg>
                            ";
            // line 155
            echo twig_escape_filter($this->env, Yii::t("wap", "Liên hệ"), "html", null, true);
            echo "
                        </a>
                    </li>
                </ul>
            </section>
        </div>
    ";
        }
        // line 162
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 162,  254 => 155,  244 => 148,  234 => 141,  225 => 135,  214 => 127,  208 => 123,  204 => 111,  197 => 110,  192 => 108,  186 => 105,  178 => 100,  169 => 94,  160 => 88,  149 => 80,  139 => 73,  128 => 64,  123 => 62,  115 => 59,  108 => 57,  102 => 54,  98 => 53,  94 => 51,  92 => 45,  87 => 42,  79 => 37,  71 => 32,  63 => 27,  51 => 18,  47 => 17,  36 => 8,  34 => 7,  28 => 4,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/ListSmallAsset') }}*/
/* {{ register_list_small_asset() }}*/
/* */
/* {{ HeaderWidget.widget({'type':'sub','title':t('wap', 'Cá nhân')}) | raw }}*/
/* */
/* <div>*/
/*     {% if not userId %}*/
/*         <div class="blkPadBoth">*/
/*             <section class="blk blkUserInfo blkUserStart">*/
/*                 <a href="/auth/login" class="btn stBtn">*/
/*                     <svg class="ico">*/
/*                         <use xlink:href="/images/defs.svg#ico-settings"></use>*/
/*                     </svg>*/
/*                 </a>*/
/*                 <div class="ctn alCenter">*/
/*                     <div class="avatar"><img src="/images/dfphoto.png" alt=""></div>*/
/*                     <p class="txtInfo">{{t('wap', 'Đăng nhập để trải nghiệm toàn bộ tính năng dịch vụ') }}</p>*/
/*                     <p><a href="/auth/login" class="btn btnBlue">{{t('wap', 'Đăng nhập') }}</a></p>*/
/*                 </div>*/
/*             </section>*/
/*             <section class="blk blkUsers">*/
/*                 <ul>*/
/*                     <li><a href="/goi-cuoc">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-wallet"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Gói cước') }}</a></li>*/
/*                     <li><a href="/chinh-sach-rieng-tu">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-lock"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Điều khoản & chính sách bảo mật') }}</a></li>*/
/*                     <li><a href="/lien-he">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-call"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Liên hệ') }}</a></li>*/
/*                 </ul>*/
/*             </section>*/
/*         </div>*/
/*     {% else %}*/
/* */
/*         <div class="blkPadBoth">*/
/*             <section class="blk blkUserInfo">*/
/*                 {% if userId %}*/
/* {#                    <a href="{{ url('/user/update',{'id':userId}) }}" class="btn stBtn">#}*/
/* {#                        <svg class="ico">#}*/
/* {#                            <use xlink:href="/images/defs.svg#ico-settings"></use>#}*/
/* {#                        </svg>#}*/
/* {#                    </a>#}*/
/*                     <div class="ctn">*/
/*                         <div class="avatar boxSd">*/
/*                             <a href="{{ url('/channel/get-user-detail',{'id':userId}) }}">*/
/*                                 <img src="{{ avatarImage }}" alt="">*/
/*                             </a>*/
/*                         </div>*/
/*                         <h6 class="title"><a href="{{ url('/channel/get-user-detail',{'id':userId}) }}">{{ userName }}</a>*/
/*                             <br />*/
/*                             <a class="edit_user" href="/user/update/{{ userId }}">{{t('wap','Chỉnh sửa người dùng')}}</a>*/
/*                         </h6>*/
/*                     </div>*/
/*                     <div class="banner pthld"><img src="{{ coverImage }}" alt="" onError="this.onerror=null;this.src='/images/dfphoto.png'"></div>*/
/*                 {% endif %}*/
/*             </section>*/
/* */
/*             <section class="blk blkUsers">*/
/*                 <ul>*/
/* */
/*                     <li><a href="/goi-cuoc">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-wallet"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Gói cước') }}*/
/*                         </a>*/
/*                     </li>*/
/*                     <li><a href="/video-cua-toi">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-video"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Video của tôi') }}*/
/*                         </a>*/
/*                     </li>*/
/* */
/*                     <li><a href="/video-da-xem">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-view"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Xem gần đây') }}</a>*/
/*                     </li>*/
/*                     <li><a href="/video-xem-sau">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-view-later"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Xem sau') }}</a>*/
/*                     </li>*/
/*                     <li><a href="/danh-sach-playlist">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-playlist"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Danh sách phát') }}*/
/*                         </a>*/
/*                     </li>*/
/*                     <li><a href="/binh-luan">*/
/*                             <i class="fa fa-commenting-o ico" aria-hidden="true"></i>*/
/*                             {{t('wap', 'Bình luận') }}*/
/*                         </a>*/
/*                     </li>*/
/*                     <li><a href="javascript:void(0);" class="item theme" data-theme="{{ app.session.get('themeUserId') }}">*/
/*                             <i class="fa fa-moon-o ico" aria-hidden="true"></i>*/
/*                             {% if app.session.get('themeUserId') == 1 %}{{t('web','Giao diện sáng')}}{% else %} {{t('web','Giao diện tối')}}{% endif %}*/
/*                         </a>*/
/*                     </li>*/
/* {#                    {% if userId %}#}*/
/* {#                    <li>#}*/
/* {#                        <a href="/lien-ket">#}*/
/* {#                            <svg class="ico">#}*/
/* {#                                <use xlink:href="/images/defs.svg#ico-shareto"></use>#}*/
/* {#                            </svg>#}*/
/* {#                            {{t('wap', 'Liên kết tài khoản') }}#}*/
/* {#                        </a>#}*/
/* {#                    </li>#}*/
/* {#                    {% endif %}#}*/
/*                     <li><a href="/auth/logout" onclick="return addEvent('logout_action', {})">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-logout"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Đăng xuất') }}</a></li>*/
/* */
/*                     <li style="border: solid 1px #ddd; margin: 5px 0px"></li>*/
/* */
/*                     <li><a href="/auth/change-password">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-lock"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Đổi mật khẩu') }}</a></li>*/
/* */
/*                     <li><a href="/gioi-thieu-dich-vu">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-page"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Giới thiệu') }}*/
/*                         </a>*/
/*                     </li>*/
/*                     <li><a href="/dieu-khoan-su-dung">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-lock"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Điều khoản & chính sách bảo mật') }}*/
/*                         </a>*/
/*                     </li>*/
/*                     <li><a href="/lien-he">*/
/*                             <svg class="ico">*/
/*                                 <use xlink:href="/images/defs.svg#ico-call"></use>*/
/*                             </svg>*/
/*                             {{t('wap', 'Liên hệ') }}*/
/*                         </a>*/
/*                     </li>*/
/*                 </ul>*/
/*             </section>*/
/*         </div>*/
/*     {% endif %}*/
/* */
/* </div>*/
