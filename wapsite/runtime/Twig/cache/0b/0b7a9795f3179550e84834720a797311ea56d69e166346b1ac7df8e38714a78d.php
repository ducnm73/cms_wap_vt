<?php

/* header.twig */
class __TwigTemplate_4b3f6f7b6a6bf06509159c1d892ee2aa4dfc07b605ccdf9909321fc5378a90bf extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header>
    ";
        // line 2
        if (((isset($context["type"]) ? $context["type"] : null) == "main")) {
            // line 3
            echo "        <div class=\"inviteBox showed\" id=\"inviteBox\" style=\"display: none;\">
            <a href=\"javascript:void(0)\" class=\"right linkStore\"
               attr-ios=\"https://apps.apple.com/us/app/meuclip/id1552861050?l=vi&amp;mt=8\"
               attr-android=\"https://play.google.com/store/apps/details?id=com.viettel.mov&amp;hl=vi\">
                ";
            // line 7
            echo twig_escape_filter($this->env, Yii::t("wap", "Xem"), "html", null, true);
            echo "
            </a>
            <a class=\"left toggleBtn\"data-target=\"#inviteBox\" onclick=\"hideInviteBox()\" ><svg class=\"ico\">
                    <use xlink:href=\"/images/defs.svg#ico-close\"></use></svg>
            </a>
            <div class=\"appIcon\"><img src=\"/images/appicon_96x96.png\" width=\"64\"></div>
            <div class=\"appInfo\">
                <h6>";
            // line 14
            echo twig_escape_filter($this->env, Yii::t("wap", "MyClip – Xem quay phát video"), "html", null, true);
            echo "</h6>
                <p class=\"txtInfo\">";
            // line 15
            echo twig_escape_filter($this->env, Yii::t("wap", "Movitel SA"), "html", null, true);
            echo "</p>
                <p><img src=\"/images/starts.png\"> </p>
                <p>";
            // line 17
            echo twig_escape_filter($this->env, Yii::t("wap", "Tải về từ Store"), "html", null, true);
            echo "</p>
            </div>
        </div>
        <div class=\"ctn\">
            <div class=\"lMn\">
                ";
            // line 22
            if ((isset($context["title"]) ? $context["title"] : null)) {
                // line 23
                echo "                    <a href=\"javascript:window.history.back();\" style=\"float: left\">
                        <svg class=\"ico\">
                            <use xlink:href=\"/images/defs.svg#ico-back\"></use>
                        </svg>
                    </a>
                    <h5 style=\"float: left; margin-left: 5px; font-size: 18px\">";
                // line 28
                echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
                echo "</h5>
                ";
            } else {
                // line 30
                echo "                    <a href=\"/\">
                        <span class=\"logo\">
                            <img src=\"/images/logo_brandname.png\" height=\"32\"/>
                        </span>
                    </a>
                ";
            }
            // line 36
            echo "            </div>
            <div class=\"rMn\">
                <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "/video-free")), "html", null, true);
            echo "\" title=\"Free videos\" alt=\"\">
                    <img src=\"/images/icon-free.svg\" height=\"20\"/>
                </a>
                ";
            // line 41
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "userId"), "method")) {
                // line 42
                echo "                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/upload-media")), "html", null, true);
                echo "\" title=\"Upload\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-upload\"></use></svg></a>
                    <a href=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/get-notification")), "html", null, true);
                echo "\" title=\"Notification\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-notification\"></use></svg>
                        <span id=\"notification-count\" class=\"style-scope ytd-notification-topbar-button-renderer\">
                    </span>
                    </a>
                ";
            }
            // line 48
            echo "                <a href=\"javascript:void(0)\" title=\"Search\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
                <span class=\"languageCode icoMore\" data-target=\"#languagePop\" onclick=\"showOptMn(\$('#languagePop'));\" >
                    <span class=\"icon-language\">
                        ";
            // line 51
            $this->loadTemplate("@app/views/partials/language.twig", "header.twig", 51)->display($context);
            // line 52
            echo "                    </span>
                </span>
            </div>
            <nav class=\"mainMn pTitle alCenter\">&nbsp;</nav>
        </div>

    ";
        } elseif ((        // line 58
(isset($context["type"]) ? $context["type"] : null) == "channel")) {
            // line 59
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"/\">
                    <span class=\"logo\">
                        <img src=\"/images/logo_brandname.png\" height=\"32\"/>
                    </span>
                </a>
            </div>
            <div class=\"rMn\">
                <a href=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "/video-free")), "html", null, true);
            echo "\" title=\"Free videos\" alt=\"\">
                    <img src=\"/images/icon-free.svg\" height=\"20\"/>
                </a>
                <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/upload-media")), "html", null, true);
            echo "\" title=\"Upload\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-upload\"></use></svg></a>
                <a href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/get-notification")), "html", null, true);
            echo "\" title=\"Notification\" alt=\"\">
                    <svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-notification\"></use></svg>
                    <span id=\"notification-count\" class=\"style-scope ytd-notification-topbar-button-renderer\" >
                    </span>
                </a>
                <a href=\"javascript:void(0)\" title=\"Search\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
                <span class=\"languageCode icoMore\" data-target=\"#languagePop\" onclick=\" showOptMn(\$('#languagePop'));\" >
                    <span class=\"icon-language\">
                        ";
            // line 80
            $this->loadTemplate("@app/views/partials/language.twig", "header.twig", 80)->display($context);
            // line 81
            echo "                    </span>
                </span>
            </div>
            <nav class=\"mainMn pTitle alCenter\"> </nav>
        </div>
        <nav class=\"subTab\" id=\"channelMenu\">
            <ul class=\"chnlSlider owl-carousel\">
                <li class=\"item\"><a ";
            // line 88
            if (((isset($context["route"]) ? $context["route"] : null) == "channel/get-detail")) {
                echo " class=\"active\" ";
            }
            // line 89
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/channel/get-detail", array("id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\" alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Trang chủ"), "html", null, true);
            echo "</a></li>
                <li class=\"item\"><a ";
            // line 90
            if (((isset($context["route"]) ? $context["route"] : null) == "default/get-list-video-filter")) {
                echo " class=\"active\" ";
            }
            // line 91
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/get-list-video-filter", array("id" => ("video_new_of_channel_" . (isset($context["id"]) ? $context["id"] : null)))), "html", null, true);
            echo "\"
                            alt=\"\">";
            // line 92
            echo twig_escape_filter($this->env, Yii::t("wap", "Video"), "html", null, true);
            echo "</a></li>
                <li class=\"item\"><a ";
            // line 93
            if (((isset($context["route"]) ? $context["route"] : null) == "channel/get-about")) {
                echo " class=\"active\" ";
            }
            // line 94
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-about", array("id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\" alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Giới thiệu"), "html", null, true);
            echo "</a></li>
            </ul>
        </nav>
    ";
        } elseif ((        // line 97
(isset($context["type"]) ? $context["type"] : null) == "user")) {
            // line 98
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"/\">
                    <span class=\"logo\">
                        <img src=\"/images/logo_brandname.png\" height=\"32\"/>
                    </span>
                </a>
            </div>
            <div class=\"rMn\">
                <a href=\"";
            // line 107
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "/video-free")), "html", null, true);
            echo "\" title=\"Free videos\" alt=\"\">
                    <img src=\"/images/icon-free.svg\" height=\"20\"/>
                </a>
                <a href=\"";
            // line 110
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/upload-media")), "html", null, true);
            echo "\" title=\"Upload\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-upload\"></use></svg></a>
                <a href=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/get-notification")), "html", null, true);
            echo "\" title=\"Notification\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-notification\"></use></svg>
                    <span id=\"notification-count\" class=\"style-scope ytd-notification-topbar-button-renderer\" >
                    </span>
                </a>
                <a href=\"javascript:void(0)\" title=\"Search\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
                <span class=\"languageCode icoMore\" data-target=\"#languagePop\" onclick=\" showOptMn(\$('#languagePop'));\" >
                    <span class=\"icon-language\">
                        ";
            // line 118
            $this->loadTemplate("@app/views/partials/language.twig", "header.twig", 118)->display($context);
            // line 119
            echo "                    </span>
                </span>
            </div>
            <nav class=\"mainMn pTitle alCenter\"> </nav>
        </div>
        <nav class=\"subTab\" id=\"channelMenu\">
            <ul class=\"chnlSlider owl-carousel\">
                <li class=\"item\"><a ";
            // line 126
            if (((isset($context["route"]) ? $context["route"] : null) == "channel/get-user-detail")) {
                echo " class=\"active\" ";
            }
            // line 127
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/channel/get-user-detail", array("id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\" alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Kênh của tôi"), "html", null, true);
            echo "</a></li>
                <li class=\"item\"><a ";
            // line 128
            if (((isset($context["route"]) ? $context["route"] : null) == "default/get-list-video-filter")) {
                echo " class=\"active\" ";
            }
            // line 129
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/get-list-video-filter", array("id" => ("video_new_of_user_" . (isset($context["id"]) ? $context["id"] : null)), "type" => "user")), "html", null, true);
            echo "\"
                            alt=\"\">";
            // line 130
            echo twig_escape_filter($this->env, Yii::t("wap", "Video"), "html", null, true);
            echo "</a></li>
                <li class=\"item\"><a ";
            // line 131
            if (((isset($context["route"]) ? $context["route"] : null) == "channel/get-list-playlist")) {
                echo " class=\"active\" ";
            }
            // line 132
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-list-playlist", array("id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\"
                            alt=\"\">";
            // line 133
            echo twig_escape_filter($this->env, Yii::t("wap", "Danh sách phát"), "html", null, true);
            echo "</a></li>
                <li class=\"item\"><a ";
            // line 134
            if (((isset($context["route"]) ? $context["route"] : null) == "channel/channel-related")) {
                echo " class=\"active\" ";
            }
            // line 135
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/channel-related", array("channel_id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\"
                            alt=\"\">";
            // line 136
            echo twig_escape_filter($this->env, Yii::t("wap", "Kênh"), "html", null, true);
            echo "</a></li>
                <li class=\"item\"><a ";
            // line 137
            if (((isset($context["route"]) ? $context["route"] : null) == "channel/get-user-about")) {
                echo " class=\"active\" ";
            }
            // line 138
            echo "                            href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-user-about", array("id" => (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\" alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Giới thiệu"), "html", null, true);
            echo "</a></li>
            </ul>
        </nav>
    ";
        } elseif ((        // line 141
(isset($context["type"]) ? $context["type"] : null) == "sub")) {
            // line 142
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"javascript:window.history.back();\">
                    <svg class=\"ico\">
                        <use xlink:href=\"/images/defs.svg#ico-back\"></use>
                    </svg>
                </a>
            </div>
            <div class=\"rMn\">
                <a href=\"";
            // line 151
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "/video-free")), "html", null, true);
            echo "\" title=\"Free videos\" alt=\"\">
                    <img src=\"/images/icon-free.svg\" height=\"20\"/>
                </a>
                <a href=\"";
            // line 154
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/upload-media")), "html", null, true);
            echo "\" title=\"Upload\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-upload\"></use></svg></a>
                <a href=\"javascript:void(0)\" title=\"Search\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
                <span class=\"languageCode icoMore\" data-target=\"#languagePop\" onclick=\" showOptMn(\$('#languagePop'));\" >
                    <span class=\"icon-language\">
                       ";
            // line 158
            $this->loadTemplate("@app/views/partials/language.twig", "header.twig", 158)->display($context);
            // line 159
            echo "                    </span>
                </span>
            </div>
            <!--
            <nav class=\"mainMn pTitle alCenter\">  </nav>
            -->
            <nav class=\"mainMn pTitle\">";
            // line 165
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</nav>
        </div>

    ";
        } elseif ((        // line 168
(isset($context["type"]) ? $context["type"] : null) == "playlist")) {
            // line 169
            echo "        <div class=\"ctn\">
            <div class=\"lMn\"></div>
            <div class=\"rMn\">
                <a href=\"";
            // line 172
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "/video-free")), "html", null, true);
            echo "\" title=\"Free videos\" alt=\"\">
                    <img src=\"/images/icon-free.svg\" height=\"20\"/>
                </a>
                <a href=\"";
            // line 175
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/upload-media")), "html", null, true);
            echo "\" title=\"Upload\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-upload\"></use></svg></a>
                <a href=\"";
            // line 176
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/get-notification")), "html", null, true);
            echo "\" title=\"Notification\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-notification\"></use></svg>
                    <span id=\"notification-count\" class=\"style-scope ytd-notification-topbar-button-renderer\">
                    </span>
                </a>
                <a href=\"javascript:void(0)\" title=\"Search\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
                <span class=\"languageCode icoMore\" data-target=\"#languagePop\" onclick=\" showOptMn(\$('#languagePop'));\" >
                    <span class=\"icon-language\">
                       ";
            // line 183
            $this->loadTemplate("@app/views/partials/language.twig", "header.twig", 183)->display($context);
            // line 184
            echo "                    </span>
                </span>
            </div>
            <nav class=\"mainMn pTitle alCenter\">";
            // line 187
            echo twig_escape_filter($this->env, Yii::t("wap", "Playlist"), "html", null, true);
            echo "</nav>
        </div>
    ";
        } elseif ((        // line 189
(isset($context["type"]) ? $context["type"] : null) == "playlistDelete")) {
            // line 190
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <!--
                <a href=\"javascript:window.history.back();\">
                        <svg class=\"ico\">
                            <use xlink:href=\"/images/defs.svg#ico-back\"></use>
                        </svg>
                </a>
                -->
            </div>
            <div class=\"rMn\">
                <a href=\"javascript:void(0)\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
            </div>
            <nav class=\"mainMn pTitle alCenter\">";
            // line 203
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</nav>
        </div>
    ";
        } elseif ((        // line 205
(isset($context["type"]) ? $context["type"] : null) == "choose_chnls")) {
            // line 206
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"javascript:window.history.back();\">
                    <svg class=\"ico\">
                        <use xlink:href=\"/images/defs.svg#ico-back\"></use>
                    </svg>
                </a>
            </div>
            <div class=\"rMn\">
                <a href=\"javascript:void(0)\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
            </div>
            <nav class=\"mainMn pTitle alCenter\">";
            // line 217
            echo twig_escape_filter($this->env, Yii::t("wap", "Chọn nội dung"), "html", null, true);
            echo "</nav>
        </div>
    ";
        } elseif ((        // line 219
(isset($context["type"]) ? $context["type"] : null) == "channel_update")) {
            // line 220
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"javascript:window.history.back();\">
                    <svg class=\"ico\">
                        <use xlink:href=\"/images/defs.svg#ico-back\"></use>
                    </svg>
                </a>
            </div>

            <div class=\"rMn\">

                <a href=\"javascript:void(0)\" onclick='validateEditChannel()' id=\"upload-button\">
                    <b>";
            // line 232
            echo twig_escape_filter($this->env, Yii::t("wap", "Lưu"), "html", null, true);
            echo "</b>

                </a>
            </div>
            <nav class=\"mainMn pTitle alCenter\">";
            // line 236
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</nav>
        </div>
    ";
        } elseif ((        // line 238
(isset($context["type"]) ? $context["type"] : null) == "detail")) {
            // line 239
            echo "        <header class=\"transparent\">
            <div class=\"ctn\">
                <div class=\"lMn\">
                    <!--
\t\t\t\t<a href=\"";
            // line 243
            echo twig_escape_filter($this->env, (isset($context["referUrl"]) ? $context["referUrl"] : null), "html", null, true);
            echo "\">
                        <svg class=\"ico\">
                            <use xlink:href=\"/images/defs.svg#ico-back\"></use>
                        </svg>
                    </a>
\t\t\t\t-->
                </div>

                <!--
                <div class=\"rMn\">
                    <a class=\"icoMore optMnBtn\" data-target=\"#playerOpt\">
                        <svg class=\"ico\">
                            <use xlink:href=\"/images/defs.svg#ico-more\"></use>
                        </svg>
                    </a>
                </div>
                -->
            </div>
        </header>

    ";
        } elseif ((        // line 263
(isset($context["type"]) ? $context["type"] : null) == "distribution")) {
            // line 264
            echo "        <div class=\"inviteBox showed\" id=\"inviteBox\" style=\"display: none;\">
            <a href=\"javascript:void(0)\" class=\"right linkStore\"
               attr-ios=\"https://itunes.apple.com/vn/app/myclip-clip-hot/id1186215150?l=vi&mt=8\"
               attr-android=\"https://play.google.com/store/apps/details?id=com.viettel.myclip&hl=vi\">
                ";
            // line 268
            echo twig_escape_filter($this->env, Yii::t("wap", "XEM"), "html", null, true);
            echo "
            </a>
            <a class=\"left toggleBtn\"data-target=\"#inviteBox\" onclick=\"hideInviteBox()\" ><svg class=\"ico\">
                    <use xlink:href=\"/images/defs.svg#ico-close\"></use></svg>
            </a>
            <div class=\"appIcon\"><img src=\"/images/appicon_96x96.png\" width=\"64\"></div>
            <div class=\"appInfo\">
                <h6>";
            // line 275
            echo twig_escape_filter($this->env, Yii::t("wap", "MyClip – Xem quay phát video"), "html", null, true);
            echo "</h6>
                <p class=\"txtInfo\">";
            // line 276
            echo twig_escape_filter($this->env, Yii::t("wap", "Movitel SA"), "html", null, true);
            echo "</p>
                <p><img src=\"/images/starts.png\"> </p>
                <p>";
            // line 278
            echo twig_escape_filter($this->env, Yii::t("wap", "Tải về từ Store"), "html", null, true);
            echo "</p>
            </div>
        </div>

        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"/\">
                    <span class=\"logo\">
                        <img src=\"/images/logo_brandname.png\" height=\"32\"/>
                    </span>
                </a>
            </div>
            <div class=\"rMn\">

                <a href=\"javascript:void(0)\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>

            </div>
            <nav class=\"mainMn pTitle alCenter\">&nbsp;</nav>
        </div>

    ";
        } elseif ((        // line 298
(isset($context["type"]) ? $context["type"] : null) == "event")) {
            // line 299
            echo "        <div class=\"ctn\">
            <div class=\"lMn\">
                <a href=\"/\">
                    <span class=\"logo\">
                        <img src=\"/images/logo_brandname.png\" height=\"32\"/>
                    </span>
                </a>
            </div>
            <div class=\"rMn\">
                <a href=\"";
            // line 308
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "/video-free")), "html", null, true);
            echo "\" title=\"Free videos\" alt=\"\">
                    <img src=\"/images/icon-free.svg\" height=\"20\"/>
                </a>
                <a href=\"";
            // line 311
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/upload-media")), "html", null, true);
            echo "\" title=\"Upload\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-upload\"></use></svg></a>
                <a href=\"";
            // line 312
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "account/get-notification")), "html", null, true);
            echo "\" title=\"Notification\" alt=\"\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-notification\"></use></svg>
                    <span id=\"notification-count\" class=\"style-scope ytd-notification-topbar-button-renderer\"></span>
                </a>
                <a href=\"javascript:void(0)\" title=\"Search\" id=\"search-icon\" class=\"modalBtn\" data-target=\"#searchPop\" ><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg></a>
                <span class=\"languageCode icoMore optMnBtn\" data-target=\"#languagePop\" onclick=\" showOptMn(\$('#languagePop'));\" >
                    <span class=\"icon-language\">
                        ";
            // line 318
            $this->loadTemplate("@app/views/partials/language.twig", "header.twig", 318)->display($context);
            // line 319
            echo "                    </span>
                </span>
            </div>
            <nav class=\"mainMn pTitle alCenter\">&nbsp;</nav>
        </div>
        <nav class=\"subTab\">
            <ul class=\"subTabSlider owl-carousel\">
                <li class=\"owl-item\"><a  ";
            // line 326
            if (((isset($context["route"]) ? $context["route"] : null) == "event/main-index")) {
                echo " class=\"active\" ";
            }
            echo " href=\"/event/main-index\" alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Sự kiện"), "html", null, true);
            echo "</a> </li>
                <li class=\"owl-item\"><a href=\"/event/index\" ";
            // line 327
            if (((isset($context["route"]) ? $context["route"] : null) == "event/index")) {
                echo " class=\"active\" ";
            }
            echo " alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Điểm của tôi"), "html", null, true);
            echo "</a> </li>
                <li class=\"owl-item\"><a href=\"/event/guide\" ";
            // line 328
            if (((isset($context["route"]) ? $context["route"] : null) == "event/guide")) {
                echo " class=\"active\" ";
            }
            echo " alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Hướng dẫn"), "html", null, true);
            echo "</a> </li>
                <li class=\"owl-item\"><a href=\"/event/terms-condition\" ";
            // line 329
            if (((isset($context["route"]) ? $context["route"] : null) == "event/terms-condition")) {
                echo " class=\"active\" ";
            }
            echo " alt=\"\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Thể lệ"), "html", null, true);
            echo "</a> </li>
            </ul>
        </nav>
    ";
        }
        // line 333
        echo "
</header>

<input type=\"hidden\" value=\"";
        // line 336
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/search-suggestion"), "html", null, true);
        echo "\" id=\"urlSearch\">

<div class=\"modal modalSearch\" id=\"searchPop\">
    <div class=\"siBox\">
        <div class=\"ctn\">
            <div class=\"lIco\">
                <button type=\"reset\"><svg class=\"ico close\"><use xlink:href=\"/images/defs.svg#ico-back\"></use></svg></button></div>
            <div class=\"rIco\">
                <button class=\"icoClose\" onclick=\"removeKeyword()\" type=\"reset\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-close\"></use></svg></button>
                <a href=\"javascript:void(0)\" class=\"ico-search-header\">
                    <svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-search\"></use></svg>
                </a>
            </div>
            <input class=\"ipt iptLg iptSearch\" id=\"keyword\" type=\"text\" placeholder=\"";
        // line 349
        echo twig_escape_filter($this->env, Yii::t("wap", "Nhập từ khóa"), "html", null, true);
        echo "\" autofocus>
        </div>
    </div>
    <div class=\"blkSearch\">
        <ul id=\"contentSuggess\">
        </ul>
    </div>
</div>

<form class=\"modal modalOptMn\" id=\"languagePop\"  name=\"myform\" action=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "default/changlanguage")), "html", null, true);
        echo "\">
    <div title=\"";
        // line 359
        echo twig_escape_filter($this->env, Yii::t("wap", "Vui lòng chọn ngôn ngữ!"), "html", null, true);
        echo "\">
        <ul>
            <li>
                <a href=\"#\" onclick=\"selectLanguage('en')\">English</a>
            </li>
            <li>
                <a href=\"#\" onclick=\"selectLanguage('mz')\">Portuguese</a>
            </li>
        </ul>
    </div>
</form>
<SCRIPT LANGUAGE=\"JavaScript\">
    function action_lang()
    {
        var x = document.getElementsByName('myform');
        //alert(x);
        x[0].submit();
    }
</SCRIPT>
";
    }

    public function getTemplateName()
    {
        return "header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  655 => 359,  651 => 358,  639 => 349,  623 => 336,  618 => 333,  607 => 329,  599 => 328,  591 => 327,  583 => 326,  574 => 319,  572 => 318,  563 => 312,  559 => 311,  553 => 308,  542 => 299,  540 => 298,  517 => 278,  512 => 276,  508 => 275,  498 => 268,  492 => 264,  490 => 263,  467 => 243,  461 => 239,  459 => 238,  454 => 236,  447 => 232,  433 => 220,  431 => 219,  426 => 217,  413 => 206,  411 => 205,  406 => 203,  391 => 190,  389 => 189,  384 => 187,  379 => 184,  377 => 183,  367 => 176,  363 => 175,  357 => 172,  352 => 169,  350 => 168,  344 => 165,  336 => 159,  334 => 158,  327 => 154,  321 => 151,  310 => 142,  308 => 141,  299 => 138,  295 => 137,  291 => 136,  286 => 135,  282 => 134,  278 => 133,  273 => 132,  269 => 131,  265 => 130,  260 => 129,  256 => 128,  249 => 127,  245 => 126,  236 => 119,  234 => 118,  224 => 111,  220 => 110,  214 => 107,  203 => 98,  201 => 97,  192 => 94,  188 => 93,  184 => 92,  179 => 91,  175 => 90,  168 => 89,  164 => 88,  155 => 81,  153 => 80,  142 => 72,  138 => 71,  132 => 68,  121 => 59,  119 => 58,  111 => 52,  109 => 51,  104 => 48,  96 => 43,  91 => 42,  89 => 41,  83 => 38,  79 => 36,  71 => 30,  66 => 28,  59 => 23,  57 => 22,  49 => 17,  44 => 15,  40 => 14,  30 => 7,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <header>*/
/*     {% if type == 'main' %}*/
/*         <div class="inviteBox showed" id="inviteBox" style="display: none;">*/
/*             <a href="javascript:void(0)" class="right linkStore"*/
/*                attr-ios="https://apps.apple.com/us/app/meuclip/id1552861050?l=vi&amp;mt=8"*/
/*                attr-android="https://play.google.com/store/apps/details?id=com.viettel.mov&amp;hl=vi">*/
/*                 {{t('wap', 'Xem') }}*/
/*             </a>*/
/*             <a class="left toggleBtn"data-target="#inviteBox" onclick="hideInviteBox()" ><svg class="ico">*/
/*                     <use xlink:href="/images/defs.svg#ico-close"></use></svg>*/
/*             </a>*/
/*             <div class="appIcon"><img src="/images/appicon_96x96.png" width="64"></div>*/
/*             <div class="appInfo">*/
/*                 <h6>{{t('wap', 'MyClip – Xem quay phát video') }}</h6>*/
/*                 <p class="txtInfo">{{t('wap', 'Movitel SA') }}</p>*/
/*                 <p><img src="/images/starts.png"> </p>*/
/*                 <p>{{t('wap', 'Tải về từ Store') }}</p>*/
/*             </div>*/
/*         </div>*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 {% if title %}*/
/*                     <a href="javascript:window.history.back();" style="float: left">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="/images/defs.svg#ico-back"></use>*/
/*                         </svg>*/
/*                     </a>*/
/*                     <h5 style="float: left; margin-left: 5px; font-size: 18px">{{ title }}</h5>*/
/*                 {% else %}*/
/*                     <a href="/">*/
/*                         <span class="logo">*/
/*                             <img src="/images/logo_brandname.png" height="32"/>*/
/*                         </span>*/
/*                     </a>*/
/*                 {% endif %}*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="{{ url(['/video-free']) }}" title="Free videos" alt="">*/
/*                     <img src="/images/icon-free.svg" height="20"/>*/
/*                 </a>*/
/*                 {% if app.session.get('userId') %}*/
/*                     <a href="{{ url(['account/upload-media']) }}" title="Upload" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-upload"></use></svg></a>*/
/*                     <a href="{{ url(['account/get-notification']) }}" title="Notification" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-notification"></use></svg>*/
/*                         <span id="notification-count" class="style-scope ytd-notification-topbar-button-renderer">*/
/*                     </span>*/
/*                     </a>*/
/*                 {% endif %}*/
/*                 <a href="javascript:void(0)" title="Search" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*                 <span class="languageCode icoMore" data-target="#languagePop" onclick="showOptMn($('#languagePop'));" >*/
/*                     <span class="icon-language">*/
/*                         {% include '@app/views/partials/language.twig' %}*/
/*                     </span>*/
/*                 </span>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">&nbsp;</nav>*/
/*         </div>*/
/* */
/*     {% elseif type == 'channel' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="/">*/
/*                     <span class="logo">*/
/*                         <img src="/images/logo_brandname.png" height="32"/>*/
/*                     </span>*/
/*                 </a>*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="{{ url(['/video-free']) }}" title="Free videos" alt="">*/
/*                     <img src="/images/icon-free.svg" height="20"/>*/
/*                 </a>*/
/*                 <a href="{{ url(['account/upload-media']) }}" title="Upload" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-upload"></use></svg></a>*/
/*                 <a href="{{ url(['account/get-notification']) }}" title="Notification" alt="">*/
/*                     <svg class="ico"><use xlink:href="/images/defs.svg#ico-notification"></use></svg>*/
/*                     <span id="notification-count" class="style-scope ytd-notification-topbar-button-renderer" >*/
/*                     </span>*/
/*                 </a>*/
/*                 <a href="javascript:void(0)" title="Search" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*                 <span class="languageCode icoMore" data-target="#languagePop" onclick=" showOptMn($('#languagePop'));" >*/
/*                     <span class="icon-language">*/
/*                         {% include '@app/views/partials/language.twig' %}*/
/*                     </span>*/
/*                 </span>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter"> </nav>*/
/*         </div>*/
/*         <nav class="subTab" id="channelMenu">*/
/*             <ul class="chnlSlider owl-carousel">*/
/*                 <li class="item"><a {% if route == "channel/get-detail" %} class="active" {% endif %}*/
/*                             href="{{ url('/channel/get-detail',{'id': id}) }}" alt="">{{ t('wap', 'Trang chủ') }}</a></li>*/
/*                 <li class="item"><a {% if route == "default/get-list-video-filter" %} class="active" {% endif %}*/
/*                             href="{{ url('default/get-list-video-filter',{'id': "video_new_of_channel_"~id}) }}"*/
/*                             alt="">{{ t('wap', 'Video') }}</a></li>*/
/*                 <li class="item"><a {% if route == "channel/get-about" %} class="active" {% endif %}*/
/*                             href="{{ url('channel/get-about',{'id': id}) }}" alt="">{{ t('wap', 'Giới thiệu') }}</a></li>*/
/*             </ul>*/
/*         </nav>*/
/*     {% elseif type == 'user' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="/">*/
/*                     <span class="logo">*/
/*                         <img src="/images/logo_brandname.png" height="32"/>*/
/*                     </span>*/
/*                 </a>*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="{{ url(['/video-free']) }}" title="Free videos" alt="">*/
/*                     <img src="/images/icon-free.svg" height="20"/>*/
/*                 </a>*/
/*                 <a href="{{ url(['account/upload-media']) }}" title="Upload" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-upload"></use></svg></a>*/
/*                 <a href="{{ url(['account/get-notification']) }}" title="Notification" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-notification"></use></svg>*/
/*                     <span id="notification-count" class="style-scope ytd-notification-topbar-button-renderer" >*/
/*                     </span>*/
/*                 </a>*/
/*                 <a href="javascript:void(0)" title="Search" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*                 <span class="languageCode icoMore" data-target="#languagePop" onclick=" showOptMn($('#languagePop'));" >*/
/*                     <span class="icon-language">*/
/*                         {% include '@app/views/partials/language.twig' %}*/
/*                     </span>*/
/*                 </span>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter"> </nav>*/
/*         </div>*/
/*         <nav class="subTab" id="channelMenu">*/
/*             <ul class="chnlSlider owl-carousel">*/
/*                 <li class="item"><a {% if route == "channel/get-user-detail" %} class="active" {% endif %}*/
/*                             href="{{ url('/channel/get-user-detail',{'id': id}) }}" alt="">{{ t('wap', 'Kênh của tôi') }}</a></li>*/
/*                 <li class="item"><a {% if route == "default/get-list-video-filter" %} class="active" {% endif %}*/
/*                             href="{{ url('default/get-list-video-filter',{'id': "video_new_of_user_"~id,'type':'user'}) }}"*/
/*                             alt="">{{ t('wap', 'Video') }}</a></li>*/
/*                 <li class="item"><a {% if route == "channel/get-list-playlist" %} class="active" {% endif %}*/
/*                             href="{{ url('channel/get-list-playlist',{'id': id}) }}"*/
/*                             alt="">{{ t('wap', 'Danh sách phát') }}</a></li>*/
/*                 <li class="item"><a {% if route == "channel/channel-related" %} class="active" {% endif %}*/
/*                             href="{{ url('channel/channel-related',{'channel_id': id}) }}"*/
/*                             alt="">{{ t('wap', 'Kênh') }}</a></li>*/
/*                 <li class="item"><a {% if route == "channel/get-user-about" %} class="active" {% endif %}*/
/*                             href="{{ url('channel/get-user-about',{'id': id}) }}" alt="">{{ t('wap', 'Giới thiệu') }}</a></li>*/
/*             </ul>*/
/*         </nav>*/
/*     {% elseif type == 'sub' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="javascript:window.history.back();">*/
/*                     <svg class="ico">*/
/*                         <use xlink:href="/images/defs.svg#ico-back"></use>*/
/*                     </svg>*/
/*                 </a>*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="{{ url(['/video-free']) }}" title="Free videos" alt="">*/
/*                     <img src="/images/icon-free.svg" height="20"/>*/
/*                 </a>*/
/*                 <a href="{{ url(['account/upload-media']) }}" title="Upload" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-upload"></use></svg></a>*/
/*                 <a href="javascript:void(0)" title="Search" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*                 <span class="languageCode icoMore" data-target="#languagePop" onclick=" showOptMn($('#languagePop'));" >*/
/*                     <span class="icon-language">*/
/*                        {% include '@app/views/partials/language.twig' %}*/
/*                     </span>*/
/*                 </span>*/
/*             </div>*/
/*             <!--*/
/*             <nav class="mainMn pTitle alCenter">  </nav>*/
/*             -->*/
/*             <nav class="mainMn pTitle">{{ title }}</nav>*/
/*         </div>*/
/* */
/*     {% elseif type == 'playlist' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn"></div>*/
/*             <div class="rMn">*/
/*                 <a href="{{ url(['/video-free']) }}" title="Free videos" alt="">*/
/*                     <img src="/images/icon-free.svg" height="20"/>*/
/*                 </a>*/
/*                 <a href="{{ url(['account/upload-media']) }}" title="Upload" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-upload"></use></svg></a>*/
/*                 <a href="{{ url(['account/get-notification']) }}" title="Notification" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-notification"></use></svg>*/
/*                     <span id="notification-count" class="style-scope ytd-notification-topbar-button-renderer">*/
/*                     </span>*/
/*                 </a>*/
/*                 <a href="javascript:void(0)" title="Search" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*                 <span class="languageCode icoMore" data-target="#languagePop" onclick=" showOptMn($('#languagePop'));" >*/
/*                     <span class="icon-language">*/
/*                        {% include '@app/views/partials/language.twig' %}*/
/*                     </span>*/
/*                 </span>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">{{ t('wap', 'Playlist') }}</nav>*/
/*         </div>*/
/*     {% elseif type == 'playlistDelete' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <!--*/
/*                 <a href="javascript:window.history.back();">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="/images/defs.svg#ico-back"></use>*/
/*                         </svg>*/
/*                 </a>*/
/*                 -->*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="javascript:void(0)" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">{{ title }}</nav>*/
/*         </div>*/
/*     {% elseif type == 'choose_chnls' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="javascript:window.history.back();">*/
/*                     <svg class="ico">*/
/*                         <use xlink:href="/images/defs.svg#ico-back"></use>*/
/*                     </svg>*/
/*                 </a>*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="javascript:void(0)" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">{{ t('wap', 'Chọn nội dung') }}</nav>*/
/*         </div>*/
/*     {% elseif type == 'channel_update' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="javascript:window.history.back();">*/
/*                     <svg class="ico">*/
/*                         <use xlink:href="/images/defs.svg#ico-back"></use>*/
/*                     </svg>*/
/*                 </a>*/
/*             </div>*/
/* */
/*             <div class="rMn">*/
/* */
/*                 <a href="javascript:void(0)" onclick='validateEditChannel()' id="upload-button">*/
/*                     <b>{{ t('wap', 'Lưu') }}</b>*/
/* */
/*                 </a>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">{{ title }}</nav>*/
/*         </div>*/
/*     {% elseif type =='detail' %}*/
/*         <header class="transparent">*/
/*             <div class="ctn">*/
/*                 <div class="lMn">*/
/*                     <!--*/
/* 				<a href="{{ referUrl }}">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="/images/defs.svg#ico-back"></use>*/
/*                         </svg>*/
/*                     </a>*/
/* 				-->*/
/*                 </div>*/
/* */
/*                 <!--*/
/*                 <div class="rMn">*/
/*                     <a class="icoMore optMnBtn" data-target="#playerOpt">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="/images/defs.svg#ico-more"></use>*/
/*                         </svg>*/
/*                     </a>*/
/*                 </div>*/
/*                 -->*/
/*             </div>*/
/*         </header>*/
/* */
/*     {% elseif type == 'distribution' %}*/
/*         <div class="inviteBox showed" id="inviteBox" style="display: none;">*/
/*             <a href="javascript:void(0)" class="right linkStore"*/
/*                attr-ios="https://itunes.apple.com/vn/app/myclip-clip-hot/id1186215150?l=vi&mt=8"*/
/*                attr-android="https://play.google.com/store/apps/details?id=com.viettel.myclip&hl=vi">*/
/*                 {{ t('wap', 'XEM') }}*/
/*             </a>*/
/*             <a class="left toggleBtn"data-target="#inviteBox" onclick="hideInviteBox()" ><svg class="ico">*/
/*                     <use xlink:href="/images/defs.svg#ico-close"></use></svg>*/
/*             </a>*/
/*             <div class="appIcon"><img src="/images/appicon_96x96.png" width="64"></div>*/
/*             <div class="appInfo">*/
/*                 <h6>{{t('wap', 'MyClip – Xem quay phát video') }}</h6>*/
/*                 <p class="txtInfo">{{t('wap', 'Movitel SA') }}</p>*/
/*                 <p><img src="/images/starts.png"> </p>*/
/*                 <p>{{t('wap', 'Tải về từ Store') }}</p>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="/">*/
/*                     <span class="logo">*/
/*                         <img src="/images/logo_brandname.png" height="32"/>*/
/*                     </span>*/
/*                 </a>*/
/*             </div>*/
/*             <div class="rMn">*/
/* */
/*                 <a href="javascript:void(0)" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/* */
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">&nbsp;</nav>*/
/*         </div>*/
/* */
/*     {% elseif type == 'event' %}*/
/*         <div class="ctn">*/
/*             <div class="lMn">*/
/*                 <a href="/">*/
/*                     <span class="logo">*/
/*                         <img src="/images/logo_brandname.png" height="32"/>*/
/*                     </span>*/
/*                 </a>*/
/*             </div>*/
/*             <div class="rMn">*/
/*                 <a href="{{ url(['/video-free']) }}" title="Free videos" alt="">*/
/*                     <img src="/images/icon-free.svg" height="20"/>*/
/*                 </a>*/
/*                 <a href="{{ url(['account/upload-media']) }}" title="Upload" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-upload"></use></svg></a>*/
/*                 <a href="{{ url(['account/get-notification']) }}" title="Notification" alt=""><svg class="ico"><use xlink:href="/images/defs.svg#ico-notification"></use></svg>*/
/*                     <span id="notification-count" class="style-scope ytd-notification-topbar-button-renderer"></span>*/
/*                 </a>*/
/*                 <a href="javascript:void(0)" title="Search" id="search-icon" class="modalBtn" data-target="#searchPop" ><svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg></a>*/
/*                 <span class="languageCode icoMore optMnBtn" data-target="#languagePop" onclick=" showOptMn($('#languagePop'));" >*/
/*                     <span class="icon-language">*/
/*                         {% include '@app/views/partials/language.twig' %}*/
/*                     </span>*/
/*                 </span>*/
/*             </div>*/
/*             <nav class="mainMn pTitle alCenter">&nbsp;</nav>*/
/*         </div>*/
/*         <nav class="subTab">*/
/*             <ul class="subTabSlider owl-carousel">*/
/*                 <li class="owl-item"><a  {% if route == "event/main-index" %} class="active" {% endif %} href="/event/main-index" alt="">{{t('wap', 'Sự kiện') }}</a> </li>*/
/*                 <li class="owl-item"><a href="/event/index" {% if route == "event/index" %} class="active" {% endif %} alt="">{{t('wap', 'Điểm của tôi') }}</a> </li>*/
/*                 <li class="owl-item"><a href="/event/guide" {% if route == "event/guide" %} class="active" {% endif %} alt="">{{t('wap', 'Hướng dẫn') }}</a> </li>*/
/*                 <li class="owl-item"><a href="/event/terms-condition" {% if route == "event/terms-condition" %} class="active" {% endif %} alt="">{{t('wap', 'Thể lệ') }}</a> </li>*/
/*             </ul>*/
/*         </nav>*/
/*     {% endif %}*/
/* */
/* </header>*/
/* */
/* <input type="hidden" value="{{ url('default/search-suggestion') }}" id="urlSearch">*/
/* */
/* <div class="modal modalSearch" id="searchPop">*/
/*     <div class="siBox">*/
/*         <div class="ctn">*/
/*             <div class="lIco">*/
/*                 <button type="reset"><svg class="ico close"><use xlink:href="/images/defs.svg#ico-back"></use></svg></button></div>*/
/*             <div class="rIco">*/
/*                 <button class="icoClose" onclick="removeKeyword()" type="reset"><svg class="ico"><use xlink:href="/images/defs.svg#ico-close"></use></svg></button>*/
/*                 <a href="javascript:void(0)" class="ico-search-header">*/
/*                     <svg class="ico"><use xlink:href="/images/defs.svg#ico-search"></use></svg>*/
/*                 </a>*/
/*             </div>*/
/*             <input class="ipt iptLg iptSearch" id="keyword" type="text" placeholder="{{t('wap', 'Nhập từ khóa') }}" autofocus>*/
/*         </div>*/
/*     </div>*/
/*     <div class="blkSearch">*/
/*         <ul id="contentSuggess">*/
/*         </ul>*/
/*     </div>*/
/* </div>*/
/* */
/* <form class="modal modalOptMn" id="languagePop"  name="myform" action="{{ url(['default/changlanguage']) }}">*/
/*     <div title="{{t('wap', 'Vui lòng chọn ngôn ngữ!') }}">*/
/*         <ul>*/
/*             <li>*/
/*                 <a href="#" onclick="selectLanguage('en')">English</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#" onclick="selectLanguage('mz')">Portuguese</a>*/
/*             </li>*/
/*         </ul>*/
/*     </div>*/
/* </form>*/
/* <SCRIPT LANGUAGE="JavaScript">*/
/*     function action_lang()*/
/*     {*/
/*         var x = document.getElementsByName('myform');*/
/*         //alert(x);*/
/*         x[0].submit();*/
/*     }*/
/* </SCRIPT>*/
/* */
