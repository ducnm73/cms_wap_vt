<?php

/* @app/views/account/playlistBox.twig */
class __TwigTemplate_5dcda38a264ad4bc0c1e3c16a0af817b7264f7cf6ca5ce897dbe86c42ef68453 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 2
            echo "
    <article class=\"videoItem playlist-";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\">
        <div class=\"ctn playlist-edit-linhtvn\">
            <div class=\"pthld\">
                <a href=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("playlist/get-video-in-playlist", array("id" => $this->getAttribute($context["video"], "id", array()))), "html", null, true);
            echo "\">
                    <div class=\"plOverlay alCenter\">
                        <p>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "num_video", array()), "html", null, true);
            echo "</p>
                        <svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-playlist\"></use></svg>
                    </div>

                    <img src=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "coverImage", array()), "html", null, true);
            echo "\" onError=\"this.onerror=null;this.src='/images/dfphoto.png'\" alt=\"\">
                </a>
            </div>
            <div class=\"ctnhld\">
                <h6 class=\"title\"><a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("playlist/get-video-in-playlist", array("id" => $this->getAttribute($context["video"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "</a></h6>
                <p class=\"txtInfo\">";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "num_video", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((($this->getAttribute($context["video"], "num_video", array()) > 1)) ? (Yii::t("wap", "videos")) : (Yii::t("wap", "video"))), "html", null, true);
            echo "</p>
            </div>
            <a class=\"icoMore optMnBtn\" data-id=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\" data-target=\"#cmtOpt\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-more\"></use></svg></a>
        </div>
    </article>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "@app/views/account/playlistBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 19,  57 => 17,  51 => 16,  44 => 12,  37 => 8,  32 => 6,  26 => 3,  23 => 2,  19 => 1,);
    }
}
/* {% for video in data.content %}*/
/* */
/*     <article class="videoItem playlist-{{video.id}}">*/
/*         <div class="ctn playlist-edit-linhtvn">*/
/*             <div class="pthld">*/
/*                 <a href="{{ url('playlist/get-video-in-playlist',{'id': video.id})}}">*/
/*                     <div class="plOverlay alCenter">*/
/*                         <p>{{video.num_video}}</p>*/
/*                         <svg class="ico"><use xlink:href="/images/defs.svg#ico-playlist"></use></svg>*/
/*                     </div>*/
/* */
/*                     <img src="{{ video.coverImage }}" onError="this.onerror=null;this.src='/images/dfphoto.png'" alt="">*/
/*                 </a>*/
/*             </div>*/
/*             <div class="ctnhld">*/
/*                 <h6 class="title"><a href="{{ url('playlist/get-video-in-playlist',{'id': video.id})}}">{{video.name}}</a></h6>*/
/*                 <p class="txtInfo">{{video.num_video}} {{ video.num_video > 1 ? t('wap', 'videos') : t('wap', 'video')}}</p>*/
/*             </div>*/
/*             <a class="icoMore optMnBtn" data-id="{{video.id}}" data-target="#cmtOpt"><svg class="ico"><use xlink:href="/images/defs.svg#ico-more"></use></svg></a>*/
/*         </div>*/
/*     </article>*/
/* {% endfor %}*/
