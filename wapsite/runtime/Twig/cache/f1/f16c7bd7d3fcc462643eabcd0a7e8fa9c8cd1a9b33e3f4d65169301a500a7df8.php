<?php

/* list_channel_follow.twig */
class __TwigTemplate_8616c3d8fad341f8fe4b2e0b6adc4412e7eb8362f5f83eedb7ac7d3c81916768 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ListAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "list_asset");
        echo "
";
        // line 3
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "sub", "title" => (isset($context["title"]) ? $context["title"] : null))), "method");
        echo "

<main>
    <section class=\"blk blkFollow blkPadBoth\">
        <input type=\"hidden\" id=\"homeItemLimit\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
        echo "\">
        <input type=\"hidden\" id=\"getMoreOffset\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
        echo "\">
        <input type=\"hidden\" id=\"type\" value=\"CHANNEL\">

        <input type=\"hidden\" id=\"load-more-id\" value=\"list_channel_follow\"/>
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()), "content", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 13
            echo "            <article class=\"userItem\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_id", array()), "html", null, true);
            echo "\">
            <div class=\"ctn\">
            <div class=\"avatar left\">
                <a href=\"channel/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_id", array()), "html", null, true);
            echo "\">
                    ";
            // line 17
            if (($this->getAttribute($context["list"], "avatarImage", array()) == "")) {
                // line 18
                echo "                    <img src=\"../images/dfphoto.png\" alt=\"\">
                    ";
            } else {
                // line 20
                echo "                    <img src=\" ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "avatarImage", array()), "html", null, true);
                echo " \" alt=\"\">
                    ";
            }
            // line 22
            echo "                </a>
            </div>

            <div class=\"ctnhld\">
                <h6 class=\"title\"><a href=\"channel/";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_name", array()), "html", null, true);
            echo "</a></h6>
                <div class=\"txtInfo\">
                    ";
            // line 28
            echo twig_escape_filter($this->env, Yii::t("wap", "{count} video(s)", array("count" => $this->getAttribute($context["list"], "num_video", array()))), "html", null, true);
            echo " <i class=\"dot\"></i>
                    ";
            // line 29
            echo twig_escape_filter($this->env, Yii::t("wap", "{count} lượt theo dõi", array("count" => $this->getAttribute($context["list"], "num_follow", array()))), "html", null, true);
            echo "
                </div>
            </div>

            <a class=\"icoMore btn btnOutlineBlueS right follow-btn-2\" onclick=\"followChannel(";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_id", array()), "html", null, true);
            echo ");return addEvent('follow_channel_action', {'channel_id': '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_id", array()), "html", null, true);
            echo "', 'channel_name': '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "channel_name", array()), "html", null, true);
            echo "', 'follow_type ': '0' });\">
                <svg class=\"ico\">
                    <use xlink:href=\"../images/defs.svg#ico-follow\"></use>
                </svg>
                <span>";
            // line 37
            echo twig_escape_filter($this->env, Yii::t("wap", "Đã theo dõi"), "html", null, true);
            echo "</span>
            </a>

            </div>
            </article>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "
        ";
        // line 44
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()), "content", array())) == 0)) {
            // line 45
            echo "            <p class=\"ctn\">";
            echo twig_escape_filter($this->env, Yii::t("wap", "Không có dữ liệu"), "html", null, true);
            echo "</p>
        ";
        }
        // line 47
        echo "
        <div id=\"container\">
        </div>
    </section>
</main>

";
    }

    public function getTemplateName()
    {
        return "list_channel_follow.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 47,  124 => 45,  122 => 44,  119 => 43,  107 => 37,  96 => 33,  89 => 29,  85 => 28,  78 => 26,  72 => 22,  66 => 20,  62 => 18,  60 => 17,  56 => 16,  49 => 13,  45 => 12,  38 => 8,  34 => 7,  27 => 3,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/ListAsset') }}*/
/* {{ register_list_asset() }}*/
/* {{ HeaderWidget.widget({'type':'sub','title':title}) | raw }}*/
/* */
/* <main>*/
/*     <section class="blk blkFollow blkPadBoth">*/
/*         <input type="hidden" id="homeItemLimit" value="{{ app.params['app.home.limit'] }}">*/
/*         <input type="hidden" id="getMoreOffset" value="{{ app.params['app.home.limit'] }}">*/
/*         <input type="hidden" id="type" value="CHANNEL">*/
/* */
/*         <input type="hidden" id="load-more-id" value="list_channel_follow"/>*/
/*         {% for list in responseData.data.content %}*/
/*             <article class="userItem" id="{{ list.channel_id }}">*/
/*             <div class="ctn">*/
/*             <div class="avatar left">*/
/*                 <a href="channel/{{ list.channel_id }}">*/
/*                     {% if (list.avatarImage == '') %}*/
/*                     <img src="../images/dfphoto.png" alt="">*/
/*                     {% else %}*/
/*                     <img src=" {{ list.avatarImage }} " alt="">*/
/*                     {% endif %}*/
/*                 </a>*/
/*             </div>*/
/* */
/*             <div class="ctnhld">*/
/*                 <h6 class="title"><a href="channel/{{ list.channel_id }}">{{ list.channel_name }}</a></h6>*/
/*                 <div class="txtInfo">*/
/*                     {{ t('wap', '{count} video(s)', {'count': list.num_video}) }} <i class="dot"></i>*/
/*                     {{ t('wap', '{count} lượt theo dõi', {'count': list.num_follow}) }}*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <a class="icoMore btn btnOutlineBlueS right follow-btn-2" onclick="followChannel({{ list.channel_id }});return addEvent('follow_channel_action', {'channel_id': '{{ list.channel_id }}', 'channel_name': '{{ list.channel_name }}', 'follow_type ': '0' });">*/
/*                 <svg class="ico">*/
/*                     <use xlink:href="../images/defs.svg#ico-follow"></use>*/
/*                 </svg>*/
/*                 <span>{{t('wap', 'Đã theo dõi') }}</span>*/
/*             </a>*/
/* */
/*             </div>*/
/*             </article>*/
/*         {% endfor %}*/
/* */
/*         {% if responseData.data.content|length ==0 %}*/
/*             <p class="ctn">{{t('wap', 'Không có dữ liệu') }}</p>*/
/*         {% endif %}*/
/* */
/*         <div id="container">*/
/*         </div>*/
/*     </section>*/
/* </main>*/
/* */
/* */
