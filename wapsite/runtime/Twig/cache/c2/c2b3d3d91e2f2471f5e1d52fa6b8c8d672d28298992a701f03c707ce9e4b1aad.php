<?php

/* @app/views/partials/trackingHead.twig */
class __TwigTemplate_3a49bd1f0aa9c37f70658038c3d8f351ff3e5c1159c38a12f20569f4cc1105b9 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 29
        if (((isset($context["pos"]) ? $context["pos"] : null) == "head")) {
            // line 30
            echo "<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-187937055-1\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-187937055-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WLXSX4Q');</script>
<!-- End Google Tag Manager -->
";
        } elseif ((        // line 47
(isset($context["pos"]) ? $context["pos"] : null) == "body")) {
            // line 48
            echo "<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-WLXSX4Q\"
height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
";
        }
    }

    public function getTemplateName()
    {
        return "@app/views/partials/trackingHead.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 48,  40 => 47,  21 => 30,  19 => 29,);
    }
}
/* {#<script type='text/javascript'>*/
/*     //some default pre init*/
/*     var Countly = Countly || {};*/
/*     Countly.q = Countly.q || [];*/
/*     //provide Countly initialization parameters*/
/*     Countly.app_key = "c3490867509a155f599247196090a42a2a524e38"; //provided by administrator*/
/*     Countly.url = "http://logging.myclip.vn"; //provided by administrator*/
/*     Countly.debug = true;*/
/*     Countly.ignore_bots = true;*/
/* */
/*     (function() {*/
/*         var cly = document.createElement('script');*/
/*         cly.type = 'text/javascript';*/
/*         cly.async = true;*/
/*         //enter url of script here*/
/*         cly.src = '/js/countly.min.js';*/
/*         cly.onload = function(){*/
/*             Countly.init();*/
/*             document.cookie = "device_id=" + Countly.device_id;*/
/*         };*/
/* */
/*         var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(cly, s);*/
/*     })();*/
/* */
/*     Countly.q.push(['track_pageview']);*/
/*     Countly.q.push(['track_sessions']);*/
/* */
/* </script>#}*/
/* {% if pos == 'head' %}*/
/* <!-- Global site tag (gtag.js) - Google Analytics -->*/
/* <script async src="https://www.googletagmanager.com/gtag/js?id=UA-187937055-1"></script>*/
/* <script>*/
/*   window.dataLayer = window.dataLayer || [];*/
/*   function gtag(){dataLayer.push(arguments);}*/
/*   gtag('js', new Date());*/
/* */
/*   gtag('config', 'UA-187937055-1');*/
/* </script>*/
/* */
/* <!-- Google Tag Manager -->*/
/* <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':*/
/* new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],*/
/* j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=*/
/* 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);*/
/* })(window,document,'script','dataLayer','GTM-WLXSX4Q');</script>*/
/* <!-- End Google Tag Manager -->*/
/* {% elseif pos == 'body' %}*/
/* <!-- Google Tag Manager (noscript) -->*/
/* <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WLXSX4Q"*/
/* height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>*/
/* <!-- End Google Tag Manager (noscript) -->*/
/* {% endif %}*/
