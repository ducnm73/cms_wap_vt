<?php

/* @app/views/partials/popup.twig */
class __TwigTemplate_d2824cf99fc5d3cdf674e97ace87a895e2d5bbc7fe05925efd716923f75c3d0c extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["type"]) ? $context["type"] : null) == "ALL")) {
            // line 2
            echo "
    ";
            // line 3
            $context["isRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_register_sub", array());
            // line 4
            echo "    ";
            $context["confirmRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_register_sub", array());
            // line 5
            echo "    ";
            $context["registerSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "package_id", array());
            // line 6
            echo "    ";
            $context["urlRegisterSub"] = $this->env->getExtension('WapExtension')->url("account/register-service");
        }
        // line 8
        echo "

";
        // line 10
        if (((isset($context["type"]) ? $context["type"] : null) == "DISTRIBUTION")) {
            // line 11
            echo "
    ";
            // line 12
            $context["isRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_register_sub", array());
            // line 13
            echo "    ";
            $context["confirmRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_register_sub", array());
            // line 14
            echo "    ";
            $context["registerSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "package_id", array());
            // line 15
            echo "    ";
            $context["urlRegisterSub"] = $this->env->getExtension('WapExtension')->url("account/register-service");
        }
        // line 17
        echo "
";
        // line 18
        if (((isset($context["type"]) ? $context["type"] : null) == "VOD")) {
            // line 19
            echo "    ";
            $context["isBuyVideo"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_buy_video", array());
            // line 20
            echo "    ";
            $context["isRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_register_sub", array());
            // line 21
            echo "    ";
            $context["confirmRetail"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_buy_video", array());
            // line 22
            echo "    ";
            $context["confirmRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_register_sub", array());
            // line 23
            echo "    ";
            $context["confirmAcceptLossData"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_accept_loss_data", array());
            // line 24
            echo "    ";
            $context["idBuyVideo"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "video_id", array());
            // line 25
            echo "    ";
            $context["registerSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "package_id", array());
            // line 26
            echo "    ";
            $context["acceptLossData"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "accept_loss_data", array());
            // line 27
            echo "    ";
            $context["urlBuyRetail"] = $this->env->getExtension('WapExtension')->url("account/buy");
            // line 28
            echo "    ";
            $context["urlRegisterSub"] = $this->env->getExtension('WapExtension')->url("account/register-service");
            // line 29
            echo "    ";
            $context["urlAcceptLossData"] = $this->env->getExtension('WapExtension')->url("account/accept-loss-data");
        }
        // line 31
        echo "
";
        // line 32
        if ((((isset($context["type"]) ? $context["type"] : null) == "FILM") || ((isset($context["type"]) ? $context["type"] : null) == "PLAYLIST"))) {
            // line 33
            echo "    ";
            $context["isBuyVideo"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_buy_video", array());
            // line 34
            echo "    ";
            $context["isBuyPlaylist"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_buy_playlist", array());
            // line 35
            echo "    ";
            $context["isRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_register_sub", array());
            // line 36
            echo "    ";
            $context["confirmRetail"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_buy_video", array());
            // line 37
            echo "    ";
            $context["confirmCategory"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_buy_playlist", array());
            // line 38
            echo "    ";
            $context["confirmRegisterSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_register_sub", array());
            // line 39
            echo "    ";
            $context["confirmAcceptLossData"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_accept_loss_data", array());
            // line 40
            echo "    ";
            $context["idBuyVideo"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "video_id", array());
            // line 41
            echo "    ";
            $context["idBuyCategory"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "playlist_id", array());
            // line 42
            echo "    ";
            $context["registerSub"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "package_id", array());
            // line 43
            echo "    ";
            $context["acceptLossData"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "accept_loss_data", array());
            // line 44
            echo "    ";
            $context["urlBuyRetail"] = $this->env->getExtension('WapExtension')->url("account/buy");
            // line 45
            echo "    ";
            $context["urlBuyCategory"] = $this->env->getExtension('WapExtension')->url("account/buy");
            // line 46
            echo "    ";
            $context["urlRegisterSub"] = $this->env->getExtension('WapExtension')->url("account/register-service");
            // line 47
            echo "    ";
            $context["urlAcceptLossData"] = $this->env->getExtension('WapExtension')->url("account/accept-loss-data");
        }
        // line 49
        echo "
";
        // line 50
        $context["isConfirmSMS"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_confirm_sms", array());
        // line 51
        $context["isRegisterFast"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "is_register_fast", array());
        // line 52
        $context["contentId"] = $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "content_id", array());
        // line 53
        echo "


";
        // line 56
        if (((((isset($context["type"]) ? $context["type"] : null) == "VOD") || ((isset($context["type"]) ? $context["type"] : null) == "FILM")) || ((isset($context["type"]) ? $context["type"] : null) == "PLAYLIST"))) {
            // line 57
            echo "
    <div class=\"overlay-popup\" id=\"popup-step1\" ";
            // line 58
            if ((isset($context["autoDisplay"]) ? $context["autoDisplay"] : null)) {
                echo " style=\"display:block\" ";
            }
            echo ">
        <div class=\"body-popup\">
            <div class=\"wrap\">
                <a onclick=\"\$('#popup-step1').fadeOut();\" class=\"close right popup-close\" style=\"padding-right: 7px; padding-top: 7px;\"><svg class=\"ico\"><use xlink:href=\"/images/defs.svg#ico-close\"></use></svg></a>
                <div class=\"piracy popup-custom\">
                    <h2>";
            // line 63
            echo twig_escape_filter($this->env, Yii::t("wap", "Xin chào {msisdn}", array("msisdn" => (isset($context["msisdn"]) ? $context["msisdn"] : null))), "html", null, true);
            echo "</h2>
                    <p>";
            // line 64
            echo $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm", array());
            echo "</p>
                    <p>";
            // line 65
            echo $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm_accept_loss_data", array());
            echo "</p>
                    <div class=\"list-button\">
                        ";
            // line 67
            if ((isset($context["isBuyVideo"]) ? $context["isBuyVideo"] : null)) {
                // line 68
                echo "                            <div class=\"row-button\">
                                <button class=\"popup-close\"
                                        onclick=\"\$('#popup-step1').fadeOut(); \$('#popupBuyRetail').fadeIn(); \">
                                    ";
                // line 71
                echo twig_escape_filter($this->env, Yii::t("wap", "MUA LẺ"), "html", null, true);
                echo "
                                </button>
                            </div>
                        ";
            }
            // line 75
            echo "                        ";
            if ((isset($context["isBuyPlaylist"]) ? $context["isBuyPlaylist"] : null)) {
                // line 76
                echo "                            <div class=\"row-button\">
                                <button class=\"popup-close\"
                                        onclick=\"\$('#popup-step1').fadeOut(); \$('#popupBuyCategory').fadeIn()\">
                                    ";
                // line 79
                echo twig_escape_filter($this->env, Yii::t("wap", "MUA TRỌN BỘ"), "html", null, true);
                echo "
                                </button>
                            </div>
                        ";
            }
            // line 83
            echo "                        ";
            if ((isset($context["isRegisterSub"]) ? $context["isRegisterSub"] : null)) {
                // line 84
                echo "                            <div class=\"row-button\">

                                ";
                // line 86
                if ((isset($context["isRegisterSub"]) ? $context["isRegisterSub"] : null)) {
                    // line 87
                    echo "                                    ";
                    if ((isset($context["isConfirmSMS"]) ? $context["isConfirmSMS"] : null)) {
                        // line 88
                        echo "                                        <button class=\"popup-close\"
                                                onclick=\"document.getElementById('register-sub-form').submit()\">
                                            ";
                        // line 90
                        echo twig_escape_filter($this->env, Yii::t("wap", "ĐĂNG KÝ"), "html", null, true);
                        echo "
                                        </button>
                                    ";
                    } elseif ( !                    // line 92
(isset($context["msisdn"]) ? $context["msisdn"] : null)) {
                        // line 93
                        echo "                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("auth/login"), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_upper_filter($this->env, Yii::t("wap", "Đăng nhập")), "html", null, true);
                        echo "</a>
                                    ";
                    } else {
                        // line 95
                        echo "                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/list-package"), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, Yii::t("wap", "ĐĂNG KÝ"), "html", null, true);
                        echo "</a>
                                        ";
                        // line 97
                        echo "                                    ";
                    }
                    // line 98
                    echo "                                ";
                } else {
                    // line 99
                    echo "                                    <button class=\"popup-close\"
                                            onclick=\"\$('#popup-step1').fadeOut(); \$('#popupRegisterPackage').fadeIn()\">
                                        ";
                    // line 101
                    echo twig_escape_filter($this->env, Yii::t("wap", "ĐĂNG KÝ"), "html", null, true);
                    echo "
                                    </button>
                                ";
                }
                // line 104
                echo "
                            </div>
                        ";
            }
            // line 107
            echo "
                        ";
            // line 108
            if ((isset($context["acceptLossData"]) ? $context["acceptLossData"] : null)) {
                // line 109
                echo "                            <div class=\"row-button\">
                                <form action=\"";
                // line 110
                echo twig_escape_filter($this->env, (isset($context["urlAcceptLossData"]) ? $context["urlAcceptLossData"] : null), "html", null, true);
                echo "\" method=\"post\" id=\"accept-loss-data-form\">
                                    <input type=\"hidden\"
                                           name=\"";
                // line 112
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
                echo "\"
                                           value=\"";
                // line 113
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
                echo "\">

                                    <button type=\"submit\" class=\"popup-close\">
                                        ";
                // line 116
                echo twig_escape_filter($this->env, Yii::t("wap", "XEM TIẾP"), "html", null, true);
                echo "
                                    </button>
                                </form>
                            </div>
                        ";
            }
            // line 121
            echo "

                    </div>
                </div>
            </div>
        </div>
    </div>
";
        }
        // line 129
        echo "
";
        // line 130
        if (((isset($context["type"]) ? $context["type"] : null) == "ALL")) {
            // line 131
            echo "    <div class=\"overlay-popup\" id=\"popup-step-promotion-1\" style=\"display: block\">
        <div class=\"body-popup\">
            <div class=\"wrap\">
                <div class=\"piracy popup-custom\">
                    <h2>";
            // line 135
            if ((isset($context["msisdn"]) ? $context["msisdn"] : null)) {
                echo twig_escape_filter($this->env, Yii::t("wap", "Xin chào {msisdn}", array("msisdn" => (isset($context["msisdn"]) ? $context["msisdn"] : null))), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký dịch vụ"), "html", null, true);
            }
            echo "</h2>
                    <p>";
            // line 136
            echo $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm", array());
            echo "</p>
                    <div class=\"list-button\">
                        <div class=\"row-button\">
                            ";
            // line 139
            if ((isset($context["isRegisterSub"]) ? $context["isRegisterSub"] : null)) {
                // line 140
                echo "                                <button class=\"popup-close btn-ok\"
                                        onclick=\"\$('#popup-step-promotion-1').fadeOut(); \$('#popupRegisterPackage').fadeIn()\">
                                    ";
                // line 142
                echo twig_escape_filter($this->env, Yii::t("wap", "ĐĂNG KÝ"), "html", null, true);
                echo "
                                </button>
                            ";
            }
            // line 145
            echo "                            <span></span>
                        </div>
                        <div class=\"row-button\">
                            <button class=\"popup-close\" onclick=\"\$('#popup-step-promotion-1').fadeOut();\">
                                ";
            // line 149
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỂ SAU"), "html", null, true);
            echo "
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        }
        // line 158
        echo "
";
        // line 159
        if (((isset($context["type"]) ? $context["type"] : null) == "DISTRIBUTION")) {
            // line 160
            echo "
    <div class=\"overlay-popup\" id=\"popup-step-promotion-1\" style=\"display: block\">
        <div class=\"body-popup\">
            <div class=\"wrap\">
                <div class=\"piracy popup-custom\">


                    <h2>";
            // line 167
            if ((isset($context["msisdn"]) ? $context["msisdn"] : null)) {
                echo twig_escape_filter($this->env, Yii::t("wap", "Xin chào {msisdn}", array("msisdn" => (isset($context["msisdn"]) ? $context["msisdn"] : null))), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký dịch vụ"), "html", null, true);
            }
            echo "</h2>
                    <p class=\"title\">";
            // line 168
            echo $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "confirm", array());
            echo "</p>
                    <div class=\"list-button\" style=\"padding: 10px\">
                            <form action=\"";
            // line 170
            echo twig_escape_filter($this->env, (isset($context["urlRegisterSub"]) ? $context["urlRegisterSub"] : null), "html", null, true);
            echo "\"  method=\"post\">
                                <input type=\"hidden\" name=\"package_id\" value=\"";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["popup"]) ? $context["popup"] : null), "package_id", array()), "html", null, true);
            echo "\"/>
                                <input type=\"hidden\"
                                       name=\"";
            // line 173
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
            echo "\"
                                       value=\"";
            // line 174
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
            echo "\">

                                <button type=\"submit\" class=\"popup-close btn-ok\">";
            // line 176
            echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký"), "html", null, true);
            echo "</button>
                            </form>
                    </div>

                    <div class=\"list-button\" style=\"padding: 10px\">
                        <a href=\"javascript:void(0)\"  onclick=\"\$('#popup-step-promotion-1').fadeOut();\">";
            // line 181
            echo twig_escape_filter($this->env, Yii::t("wap", "Để sau"), "html", null, true);
            echo "</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

";
        }
        // line 190
        echo "

";
        // line 192
        if ((isset($context["isRegisterSub"]) ? $context["isRegisterSub"] : null)) {
            // line 193
            echo "    <div class=\"overlay-popup\" id=\"popupRegisterPackage\">
        <div class=\"body-popup\">
            <div class=\"wrap\">
                <div class=\"piracy popup-custom\">
                    <h2>";
            // line 197
            echo twig_escape_filter($this->env, Yii::t("wap", "Mua gói cước"), "html", null, true);
            echo "</h2>
                    <p class=\"popup-content\">
                        ";
            // line 199
            echo (isset($context["confirmRegisterSub"]) ? $context["confirmRegisterSub"] : null);
            echo "
                    </p>
                    <form action=\"";
            // line 201
            echo twig_escape_filter($this->env, (isset($context["urlRegisterSub"]) ? $context["urlRegisterSub"] : null), "html", null, true);
            echo "\" method=\"post\" id=\"register-sub-form\">
                        <input type=\"hidden\" name=\"package_id\" value=\"";
            // line 202
            echo twig_escape_filter($this->env, (isset($context["registerSub"]) ? $context["registerSub"] : null), "html", null, true);
            echo "\"/>
                        ";
            // line 203
            if ((isset($context["isRegisterFast"]) ? $context["isRegisterFast"] : null)) {
                // line 204
                echo "                            <input type=\"hidden\" name=\"content_id\" value=\"";
                echo twig_escape_filter($this->env, (isset($context["contentId"]) ? $context["contentId"] : null), "html", null, true);
                echo "\"/>
                        ";
            }
            // line 206
            echo "
                        <input type=\"hidden\"
                               name=\"";
            // line 208
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
            echo "\"
                               value=\"";
            // line 209
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
            echo "\">

                        <div class=\"list-button\">
                            <div class=\"row-button\">
                                <button type=\"submit\" class=\"popup-close btn-ok\">
                                    ";
            // line 214
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỒNG Ý"), "html", null, true);
            echo "
                                </button>
                                <span></span>
                            </div>
                            <div class=\"row-button\">
                                <button type=\"button\" onclick=\"\$('#popupRegisterPackage').fadeOut(); \">
                                    ";
            // line 220
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỂ SAU"), "html", null, true);
            echo "
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
";
        }
        // line 230
        echo "
";
        // line 231
        if ((isset($context["isBuyVideo"]) ? $context["isBuyVideo"] : null)) {
            // line 232
            echo "    <div class=\"overlay-popup\" id=\"popupBuyRetail\">
        <div class=\"body-popup\">
            <div class=\"wrap\">
                <div class=\"piracy popup-custom\">
                    <h2>";
            // line 236
            echo twig_escape_filter($this->env, Yii::t("wap", "Mua lẻ"), "html", null, true);
            echo "</h2>
                    <p>";
            // line 237
            echo (isset($context["confirmRetail"]) ? $context["confirmRetail"] : null);
            echo "</p>
                    <form action=\"";
            // line 238
            echo twig_escape_filter($this->env, (isset($context["urlBuyRetail"]) ? $context["urlBuyRetail"] : null), "html", null, true);
            echo "\" method=\"post\" id=\"buy-retail-form\">
                        <input type=\"hidden\" name=\"item_id\" value=\"";
            // line 239
            echo twig_escape_filter($this->env, (isset($context["idBuyVideo"]) ? $context["idBuyVideo"] : null), "html", null, true);
            echo "\"/>
                        <input type=\"hidden\" name=\"type\" value=\"VOD\"/>
                        <input type=\"hidden\"
                               name=\"";
            // line 242
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
            echo "\"
                               value=\"";
            // line 243
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
            echo "\">

                        <div class=\"list-button\">
                            <div class=\"row-button\">
                                <button type=\"submit\" class=\"popup-close btn-ok\">
                                    ";
            // line 248
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỒNG Ý"), "html", null, true);
            echo "
                                </button>
                                <span></span>
                                <button type=\"button\" onclick=\"\$('#popupBuyRetail').fadeOut(); \">
                                    ";
            // line 252
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỂ SAU"), "html", null, true);
            echo "
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
";
        }
        // line 262
        echo "
";
        // line 263
        if ((isset($context["isBuyPlaylist"]) ? $context["isBuyPlaylist"] : null)) {
            // line 264
            echo "    <div class=\"overlay-popup\" id=\"popupBuyCategory\">
        <div class=\"body-popup\">
            <div class=\"wrap\">
                <div class=\"piracy popup-custom\">
                    <h2>";
            // line 268
            echo twig_escape_filter($this->env, Yii::t("wap", "Mua lẻ"), "html", null, true);
            echo "</h2>
                    <p>";
            // line 269
            echo (isset($context["confirmCategory"]) ? $context["confirmCategory"] : null);
            echo "</p>
                    <form action=\"";
            // line 270
            echo twig_escape_filter($this->env, (isset($context["urlBuyCategory"]) ? $context["urlBuyCategory"] : null), "html", null, true);
            echo "\" method=\"post\" id=\"buy-category-form\">
                        <input type=\"hidden\" name=\"item_id\" value=\"";
            // line 271
            echo twig_escape_filter($this->env, (isset($context["idBuyCategory"]) ? $context["idBuyCategory"] : null), "html", null, true);
            echo "\"/>
                        <input type=\"hidden\" name=\"type\" value=\"PLAYLIST\"/>
                        <input type=\"hidden\"
                               name=\"";
            // line 274
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
            echo "\"
                               value=\"";
            // line 275
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
            echo "\">
                        <div class=\"list-button\">
                            <div class=\"row-button\">
                                <button type=\"submit\" class=\"popup-close btn-ok\">
                                    ";
            // line 279
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỒNG Ý"), "html", null, true);
            echo "
                                </button>
                                <span></span>
                                <button type=\"button\" onclick=\"\$('#popupBuyCategory').fadeOut(); \">
                                    ";
            // line 283
            echo twig_escape_filter($this->env, Yii::t("wap", "ĐỂ SAU"), "html", null, true);
            echo "
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
";
        }
        // line 293
        echo "
";
    }

    public function getTemplateName()
    {
        return "@app/views/partials/popup.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  635 => 293,  622 => 283,  615 => 279,  608 => 275,  604 => 274,  598 => 271,  594 => 270,  590 => 269,  586 => 268,  580 => 264,  578 => 263,  575 => 262,  562 => 252,  555 => 248,  547 => 243,  543 => 242,  537 => 239,  533 => 238,  529 => 237,  525 => 236,  519 => 232,  517 => 231,  514 => 230,  501 => 220,  492 => 214,  484 => 209,  480 => 208,  476 => 206,  470 => 204,  468 => 203,  464 => 202,  460 => 201,  455 => 199,  450 => 197,  444 => 193,  442 => 192,  438 => 190,  426 => 181,  418 => 176,  413 => 174,  409 => 173,  404 => 171,  400 => 170,  395 => 168,  387 => 167,  378 => 160,  376 => 159,  373 => 158,  361 => 149,  355 => 145,  349 => 142,  345 => 140,  343 => 139,  337 => 136,  329 => 135,  323 => 131,  321 => 130,  318 => 129,  308 => 121,  300 => 116,  294 => 113,  290 => 112,  285 => 110,  282 => 109,  280 => 108,  277 => 107,  272 => 104,  266 => 101,  262 => 99,  259 => 98,  256 => 97,  249 => 95,  241 => 93,  239 => 92,  234 => 90,  230 => 88,  227 => 87,  225 => 86,  221 => 84,  218 => 83,  211 => 79,  206 => 76,  203 => 75,  196 => 71,  191 => 68,  189 => 67,  184 => 65,  180 => 64,  176 => 63,  166 => 58,  163 => 57,  161 => 56,  156 => 53,  154 => 52,  152 => 51,  150 => 50,  147 => 49,  143 => 47,  140 => 46,  137 => 45,  134 => 44,  131 => 43,  128 => 42,  125 => 41,  122 => 40,  119 => 39,  116 => 38,  113 => 37,  110 => 36,  107 => 35,  104 => 34,  101 => 33,  99 => 32,  96 => 31,  92 => 29,  89 => 28,  86 => 27,  83 => 26,  80 => 25,  77 => 24,  74 => 23,  71 => 22,  68 => 21,  65 => 20,  62 => 19,  60 => 18,  57 => 17,  53 => 15,  50 => 14,  47 => 13,  45 => 12,  42 => 11,  40 => 10,  36 => 8,  32 => 6,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if type=='ALL' %}*/
/* */
/*     {% set isRegisterSub = popup.is_register_sub %}*/
/*     {% set confirmRegisterSub = popup.confirm_register_sub %}*/
/*     {% set registerSub = popup.package_id %}*/
/*     {% set urlRegisterSub = url('account/register-service') %}*/
/* {% endif %}*/
/* */
/* */
/* {% if type=='DISTRIBUTION' %}*/
/* */
/*     {% set isRegisterSub = popup.is_register_sub %}*/
/*     {% set confirmRegisterSub = popup.confirm_register_sub %}*/
/*     {% set registerSub = popup.package_id %}*/
/*     {% set urlRegisterSub = url('account/register-service') %}*/
/* {% endif %}*/
/* */
/* {% if type=='VOD' %}*/
/*     {% set isBuyVideo = popup.is_buy_video %}*/
/*     {% set isRegisterSub = popup.is_register_sub %}*/
/*     {% set confirmRetail = popup.confirm_buy_video %}*/
/*     {% set confirmRegisterSub = popup.confirm_register_sub %}*/
/*     {% set confirmAcceptLossData = popup.confirm_accept_loss_data %}*/
/*     {% set idBuyVideo = popup.video_id %}*/
/*     {% set registerSub = popup.package_id %}*/
/*     {% set acceptLossData = popup.accept_loss_data %}*/
/*     {% set urlBuyRetail = url('account/buy') %}*/
/*     {% set urlRegisterSub = url('account/register-service') %}*/
/*     {% set urlAcceptLossData = url('account/accept-loss-data') %}*/
/* {% endif %}*/
/* */
/* {% if type=='FILM' or type=='PLAYLIST' %}*/
/*     {% set isBuyVideo = popup.is_buy_video %}*/
/*     {% set isBuyPlaylist = popup.is_buy_playlist %}*/
/*     {% set isRegisterSub = popup.is_register_sub %}*/
/*     {% set confirmRetail = popup.confirm_buy_video %}*/
/*     {% set confirmCategory = popup.confirm_buy_playlist %}*/
/*     {% set confirmRegisterSub = popup.confirm_register_sub %}*/
/*     {% set confirmAcceptLossData = popup.confirm_accept_loss_data %}*/
/*     {% set idBuyVideo = popup.video_id %}*/
/*     {% set idBuyCategory = popup.playlist_id %}*/
/*     {% set registerSub = popup.package_id %}*/
/*     {% set acceptLossData = popup.accept_loss_data %}*/
/*     {% set urlBuyRetail = url('account/buy') %}*/
/*     {% set urlBuyCategory = url('account/buy') %}*/
/*     {% set urlRegisterSub = url('account/register-service') %}*/
/*     {% set urlAcceptLossData = url('account/accept-loss-data') %}*/
/* {% endif %}*/
/* */
/* {% set isConfirmSMS = popup.is_confirm_sms %}*/
/* {% set isRegisterFast = popup.is_register_fast %}*/
/* {% set contentId = popup.content_id %}*/
/* */
/* */
/* */
/* {% if type=='VOD' or type=='FILM' or type=='PLAYLIST' %}*/
/* */
/*     <div class="overlay-popup" id="popup-step1" {% if autoDisplay %} style="display:block" {% endif %}>*/
/*         <div class="body-popup">*/
/*             <div class="wrap">*/
/*                 <a onclick="$('#popup-step1').fadeOut();" class="close right popup-close" style="padding-right: 7px; padding-top: 7px;"><svg class="ico"><use xlink:href="/images/defs.svg#ico-close"></use></svg></a>*/
/*                 <div class="piracy popup-custom">*/
/*                     <h2>{{ t('wap', 'Xin chào {msisdn}', {'msisdn': msisdn}) }}</h2>*/
/*                     <p>{{ popup.confirm|raw }}</p>*/
/*                     <p>{{ popup.confirm_accept_loss_data|raw }}</p>*/
/*                     <div class="list-button">*/
/*                         {% if isBuyVideo %}*/
/*                             <div class="row-button">*/
/*                                 <button class="popup-close"*/
/*                                         onclick="$('#popup-step1').fadeOut(); $('#popupBuyRetail').fadeIn(); ">*/
/*                                     {{t('wap', 'MUA LẺ') }}*/
/*                                 </button>*/
/*                             </div>*/
/*                         {% endif %}*/
/*                         {% if isBuyPlaylist %}*/
/*                             <div class="row-button">*/
/*                                 <button class="popup-close"*/
/*                                         onclick="$('#popup-step1').fadeOut(); $('#popupBuyCategory').fadeIn()">*/
/*                                     {{t('wap', 'MUA TRỌN BỘ') }}*/
/*                                 </button>*/
/*                             </div>*/
/*                         {% endif %}*/
/*                         {% if isRegisterSub %}*/
/*                             <div class="row-button">*/
/* */
/*                                 {% if isRegisterSub %}*/
/*                                     {% if isConfirmSMS %}*/
/*                                         <button class="popup-close"*/
/*                                                 onclick="document.getElementById('register-sub-form').submit()">*/
/*                                             {{t('wap', 'ĐĂNG KÝ') }}*/
/*                                         </button>*/
/*                                     {% elseif not msisdn %}*/
/*                                         <a href="{{ url('auth/login') }}">{{ t('wap','Đăng nhập')|upper }}</a>*/
/*                                     {% else %}*/
/*                                         <a href="{{ url('default/list-package') }}">{{t('wap','ĐĂNG KÝ')}}</a>*/
/*                                         {# <button class="popup-close" onclick="$('#popup-step1').fadeOut(); $('#popupRegisterPackage').fadeIn()">{{t('wap', 'ĐĂNG KÝ') }}</button> #}*/
/*                                     {% endif %}*/
/*                                 {% else %}*/
/*                                     <button class="popup-close"*/
/*                                             onclick="$('#popup-step1').fadeOut(); $('#popupRegisterPackage').fadeIn()">*/
/*                                         {{t('wap', 'ĐĂNG KÝ') }}*/
/*                                     </button>*/
/*                                 {% endif %}*/
/* */
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if acceptLossData %}*/
/*                             <div class="row-button">*/
/*                                 <form action="{{ urlAcceptLossData }}" method="post" id="accept-loss-data-form">*/
/*                                     <input type="hidden"*/
/*                                            name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                            value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/* */
/*                                     <button type="submit" class="popup-close">*/
/*                                         {{t('wap', 'XEM TIẾP') }}*/
/*                                     </button>*/
/*                                 </form>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/* */
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endif %}*/
/* */
/* {% if type == 'ALL' %}*/
/*     <div class="overlay-popup" id="popup-step-promotion-1" style="display: block">*/
/*         <div class="body-popup">*/
/*             <div class="wrap">*/
/*                 <div class="piracy popup-custom">*/
/*                     <h2>{% if msisdn %}{{ t('wap', 'Xin chào {msisdn}', {'msisdn': msisdn}) }}{% else %}{{t('wap', 'Đăng ký dịch vụ') }}{% endif %}</h2>*/
/*                     <p>{{ popup.confirm|raw }}</p>*/
/*                     <div class="list-button">*/
/*                         <div class="row-button">*/
/*                             {% if isRegisterSub %}*/
/*                                 <button class="popup-close btn-ok"*/
/*                                         onclick="$('#popup-step-promotion-1').fadeOut(); $('#popupRegisterPackage').fadeIn()">*/
/*                                     {{t('wap', 'ĐĂNG KÝ') }}*/
/*                                 </button>*/
/*                             {% endif %}*/
/*                             <span></span>*/
/*                         </div>*/
/*                         <div class="row-button">*/
/*                             <button class="popup-close" onclick="$('#popup-step-promotion-1').fadeOut();">*/
/*                                 {{t('wap', 'ĐỂ SAU') }}*/
/*                             </button>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endif %}*/
/* */
/* {% if type == 'DISTRIBUTION' %}*/
/* */
/*     <div class="overlay-popup" id="popup-step-promotion-1" style="display: block">*/
/*         <div class="body-popup">*/
/*             <div class="wrap">*/
/*                 <div class="piracy popup-custom">*/
/* */
/* */
/*                     <h2>{% if msisdn %}{{ t('wap', 'Xin chào {msisdn}', {'msisdn': msisdn}) }}{% else %}{{t('wap', 'Đăng ký dịch vụ') }}{% endif %}</h2>*/
/*                     <p class="title">{{ popup.confirm|raw }}</p>*/
/*                     <div class="list-button" style="padding: 10px">*/
/*                             <form action="{{ urlRegisterSub }}"  method="post">*/
/*                                 <input type="hidden" name="package_id" value="{{ popup.package_id }}"/>*/
/*                                 <input type="hidden"*/
/*                                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/* */
/*                                 <button type="submit" class="popup-close btn-ok">{{t('wap', 'Đăng ký') }}</button>*/
/*                             </form>*/
/*                     </div>*/
/* */
/*                     <div class="list-button" style="padding: 10px">*/
/*                         <a href="javascript:void(0)"  onclick="$('#popup-step-promotion-1').fadeOut();">{{t('wap', 'Để sau') }}</a>*/
/*                     </div>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endif %}*/
/* */
/* */
/* {% if isRegisterSub %}*/
/*     <div class="overlay-popup" id="popupRegisterPackage">*/
/*         <div class="body-popup">*/
/*             <div class="wrap">*/
/*                 <div class="piracy popup-custom">*/
/*                     <h2>{{t('wap', 'Mua gói cước') }}</h2>*/
/*                     <p class="popup-content">*/
/*                         {{ confirmRegisterSub|raw }}*/
/*                     </p>*/
/*                     <form action="{{ urlRegisterSub }}" method="post" id="register-sub-form">*/
/*                         <input type="hidden" name="package_id" value="{{ registerSub }}"/>*/
/*                         {% if isRegisterFast %}*/
/*                             <input type="hidden" name="content_id" value="{{ contentId }}"/>*/
/*                         {% endif %}*/
/* */
/*                         <input type="hidden"*/
/*                                name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/* */
/*                         <div class="list-button">*/
/*                             <div class="row-button">*/
/*                                 <button type="submit" class="popup-close btn-ok">*/
/*                                     {{t('wap', 'ĐỒNG Ý') }}*/
/*                                 </button>*/
/*                                 <span></span>*/
/*                             </div>*/
/*                             <div class="row-button">*/
/*                                 <button type="button" onclick="$('#popupRegisterPackage').fadeOut(); ">*/
/*                                     {{t('wap', 'ĐỂ SAU') }}*/
/*                                 </button>*/
/*                             </div>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endif %}*/
/* */
/* {% if isBuyVideo %}*/
/*     <div class="overlay-popup" id="popupBuyRetail">*/
/*         <div class="body-popup">*/
/*             <div class="wrap">*/
/*                 <div class="piracy popup-custom">*/
/*                     <h2>{{t('wap', 'Mua lẻ') }}</h2>*/
/*                     <p>{{ confirmRetail|raw }}</p>*/
/*                     <form action="{{ urlBuyRetail }}" method="post" id="buy-retail-form">*/
/*                         <input type="hidden" name="item_id" value="{{ idBuyVideo }}"/>*/
/*                         <input type="hidden" name="type" value="VOD"/>*/
/*                         <input type="hidden"*/
/*                                name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/* */
/*                         <div class="list-button">*/
/*                             <div class="row-button">*/
/*                                 <button type="submit" class="popup-close btn-ok">*/
/*                                     {{t('wap', 'ĐỒNG Ý') }}*/
/*                                 </button>*/
/*                                 <span></span>*/
/*                                 <button type="button" onclick="$('#popupBuyRetail').fadeOut(); ">*/
/*                                     {{t('wap', 'ĐỂ SAU') }}*/
/*                                 </button>*/
/*                             </div>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endif %}*/
/* */
/* {% if isBuyPlaylist %}*/
/*     <div class="overlay-popup" id="popupBuyCategory">*/
/*         <div class="body-popup">*/
/*             <div class="wrap">*/
/*                 <div class="piracy popup-custom">*/
/*                     <h2>{{t('wap', 'Mua lẻ') }}</h2>*/
/*                     <p>{{ confirmCategory|raw }}</p>*/
/*                     <form action="{{ urlBuyCategory }}" method="post" id="buy-category-form">*/
/*                         <input type="hidden" name="item_id" value="{{ idBuyCategory }}"/>*/
/*                         <input type="hidden" name="type" value="PLAYLIST"/>*/
/*                         <input type="hidden"*/
/*                                name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                         <div class="list-button">*/
/*                             <div class="row-button">*/
/*                                 <button type="submit" class="popup-close btn-ok">*/
/*                                     {{t('wap', 'ĐỒNG Ý') }}*/
/*                                 </button>*/
/*                                 <span></span>*/
/*                                 <button type="button" onclick="$('#popupBuyCategory').fadeOut(); ">*/
/*                                     {{t('wap', 'ĐỂ SAU') }}*/
/*                                 </button>*/
/*                             </div>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endif %}*/
/* */
/* */
