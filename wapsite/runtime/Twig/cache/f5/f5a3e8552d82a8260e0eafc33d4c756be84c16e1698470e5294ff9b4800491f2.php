<?php

/* detail.twig */
class __TwigTemplate_3c30c2501faefffdf8aef9c174387f970b951784e7ee23e56fe4a071c2c41fb1 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/DetailAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "detail_asset");
        echo "

";
        // line 4
        $context["detail"] = $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "detail", array());
        // line 5
        $context["streams"] = $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "streams", array());
        // line 6
        $context["relateds"] = $this->getAttribute($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "relateds", array()), "content", array());
        // line 7
        echo "
";
        // line 8
        $context["isRegisterFast"] = $this->getAttribute($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()), "is_register_fast", array());
        // line 9
        echo "
";
        // line 10
        if ((isset($context["storeUrl"]) ? $context["storeUrl"] : null)) {
            // line 11
            echo "    ";
        }
        // line 23
        echo "
";
        // line 24
        if (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "errorCode", array()) == 200)) {
            // line 25
            echo "    <input type=\"hidden\" id=\"play\" name=\"play\" value=\"true\"/>
    ";
            // line 26
            if (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "urlStreaming", array()) &&  !(isset($context["popupGA"]) ? $context["popupGA"] : null))) {
                // line 27
                echo "        <input type=\"hidden\" id=\"url\" name=\"url\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "urlStreaming", array()), "html", null, true);
                echo "\"/>
    ";
            } else {
                // line 29
                echo "        <input type=\"hidden\" id=\"url\" name=\"url\" value=\"\"/>
    ";
            }
            // line 31
            echo "
    <input type=\"hidden\" id=\"poster\" name=\"poster\" value=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "coverImage", array()), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"play\" value=\"true\"/>
    <input type=\"hidden\" id=\"media_type\" value=\"VOD\"/>
    <input type=\"hidden\" id=\"is_pc\" value=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "is_pc"), "method"), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"is_ios\" value=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "is_ios"), "method"), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"ajaxUrl\" value=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/account/watch-time"), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"playId\" value=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"playPosition\" value=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "currentTime", array()), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"parentId\" value=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
            echo "\"/>
    <input type=\"hidden\" id=\"language\" value=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "getLang", array(0 => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "multilang", array())), "method"), "html", null, true);
            echo "\"/>
";
        }
        // line 43
        if ($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array())) {
            // line 44
            echo "    ";
            $this->loadTemplate("@app/views/partials/popup.twig", "detail.twig", 44)->display(array_merge($context, array("type" => "VOD", "popup" => $this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()), "autoDisplay" => $this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "autoDisplay", array()), "msisdn" => (isset($context["msisdn"]) ? $context["msisdn"] : null))));
        }
        // line 46
        echo "
<div>
    <section id=\"section_detail\" class=\"blk blkVideoDetail\">
        ";
        // line 49
        if ((($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "suggest_package_id", array()) && (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()) && ($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "errorCode", array()) != 200)) || (isset($context["popupGA"]) ? $context["popupGA"] : null))) && ($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "errorCode", array()) != 401))) {
            // line 50
            echo "            <div class=\"container\" style=\"padding: 10px; text-align: center; color: black\">
                ";
            // line 51
            if ($this->getAttribute($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()), "confirm", array())) {
                // line 52
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()), "confirm", array()), "html", null, true);
                echo "
                ";
            } else {
                // line 54
                echo "                    ";
                echo twig_escape_filter($this->env, (isset($context["popupGA"]) ? $context["popupGA"] : null), "html", null, true);
                echo "
                ";
            }
            // line 56
            echo "            </div>
        ";
        }
        // line 58
        echo "
        ";
        // line 59
        if ((isset($context["popupGA"]) ? $context["popupGA"] : null)) {
            // line 60
            echo "            <input type=\"hidden\" id=\"google-adwords\" value=\"1\"/>
        ";
        }
        // line 62
        echo "
        <div class=\"screen pthld\" style=\"height: 100%\">
            ";
        // line 64
        if (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "urlStreaming", array()) &&  !(isset($context["popupGA"]) ? $context["popupGA"] : null))) {
            // line 65
            echo "
                <div id=\"playerBox\" ";
            // line 66
            if (((isset($context["showTime"]) ? $context["showTime"] : null) != null)) {
                echo "style=\"display: none\" ";
            }
            echo ">
                    <video id=\"videoBox\" class=\"video-js  vjs-16-9 youtube\"
                           controls ";
            // line 68
            if ((isset($context["isIOS"]) ? $context["isIOS"] : null)) {
                echo "  webkit-playsinline  playsinline ";
            }
            echo ">
                    </video>
                </div>
                ";
            // line 71
            if (((isset($context["showTime"]) ? $context["showTime"] : null) != null)) {
                // line 72
                echo "
                    <input type=\"hidden\" id=\"idUrlGetStream\"
                           value=\"";
                // line 74
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/video/ajax-get-video-stream", array("id" => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()))), "html", null, true);
                echo "\"/>
                    <input type=\"hidden\" id=\"showTimer\" value=\"";
                // line 75
                echo twig_escape_filter($this->env, (isset($context["showTime"]) ? $context["showTime"] : null), "html", null, true);
                echo "\"/>
                    <div id=\"countdown\" class=\"screen countdown alCenter\">
                        <h5>";
                // line 77
                echo twig_escape_filter($this->env, Yii::t("wap", "Chương trình sẽ bắt đầu sau"), "html", null, true);
                echo "</h5>
                        <div class=\"counter\"></div>
                    </div>
                ";
            }
            // line 81
            echo "            ";
        } else {
            // line 82
            echo "
                ";
            // line 83
            if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "suggest_package_id", array()) && ($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()) || (isset($context["popupGA"]) ? $context["popupGA"] : null)))) {
                // line 84
                echo "                    <div class=\"banner-film\" id=\"suggest-package\">
                        <img src=\"";
                // line 85
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "coverImage", array()), "html", null, true);
                echo "\" width=\"100%\"
                             onError=\"this.onerror=null;this.src='/images/16x9.png';\"/>
                        <div>
                            <a id=\"regSuggestion\"
                               href=\"javascript:void(0)\" ";
                // line 89
                if (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "errorCode", array()) == 401)) {
                    echo " onclick=\"window.location='";
                    echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("auth/login?popup=1"), "html", null, true);
                    echo "'\" ";
                } else {
                    echo " onClick=\"\$('#regSuggestion').fadeOut();\$('#confirmRegSuggestion').fadeIn()\" ";
                }
                // line 90
                echo "                               style=\"position: absolute; left: 10px; bottom: 20px; background: #0081ff; padding: 6px 20px 6px 15px; border-radius: 20px; color: #fff;\">
                                <span style=\"display: inline-block; vertical-align: middle;\">";
                // line 91
                echo twig_escape_filter($this->env, Yii::t("wap", "Đăng ký"), "html", null, true);
                echo "<br>";
                echo twig_escape_filter($this->env, Yii::t("wap", "để xem video"), "html", null, true);
                echo "</span>
                            </a>

                            <form id=\"register-sub-suggestionform\" action=\"";
                // line 94
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("account/register-service"), "html", null, true);
                echo "\"
                                  method=\"post\">
                                <input type=\"hidden\" name=\"package_id\" value=\"";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "suggest_package_id", array()), "html", null, true);
                echo "\"/>
                                <input type=\"hidden\"
                                       name=\"";
                // line 98
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
                echo "\"
                                       value=\"";
                // line 99
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
                echo "\">

                                <a id=\"confirmRegSuggestion\"
                                   href=\"javascript:void(0)\" ";
                // line 102
                if (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "errorCode", array()) == 401)) {
                    echo " onclick=\"window.location='";
                    echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("auth/login?popup=1"), "html", null, true);
                    echo "'\" ";
                } else {
                    echo " onClick=\"\$('#register-sub-suggestionform').submit();\$(this).removeAttr('onClick')\" ";
                }
                // line 103
                echo "                                   style=\"display:none; position: absolute; left: 10px; bottom: 20px; background: #0081ff; padding: 6px 20px 6px 15px; border-radius: 20px; color: #fff;\">
                                    <span style=\"display: inline-block; vertical-align: middle;\">";
                // line 104
                echo twig_escape_filter($this->env, Yii::t("wap", "Đồng ý"), "html", null, true);
                echo " <br/>";
                echo twig_escape_filter($this->env, Yii::t("wap", "để xem video"), "html", null, true);
                echo "</span>
                                </a>
                            </form>
                            ";
                // line 107
                if ((isset($context["popupGA"]) ? $context["popupGA"] : null)) {
                    // line 108
                    echo "                                <a href=\"";
                    echo twig_escape_filter($this->env, (isset($context["detailUrl"]) ? $context["detailUrl"] : null), "html", null, true);
                    echo "\"
                                   style=\"position: absolute; right: 10px; bottom: 20px; background: #222; padding: 10px 20px; border-radius: 16px; color: #fff;\">";
                    // line 109
                    echo twig_escape_filter($this->env, Yii::t("wap", "Bỏ qua"), "html", null, true);
                    echo "</a>
                            ";
                } else {
                    // line 111
                    echo "                                <a href=\"javascript:void(0)\"
                                   onClick=\"\$('#suggest-package').remove();\$('#play-area').show()\"
                                   style=\"position: absolute; right: 10px; bottom: 20px; background: #222; padding: 10px 20px; border-radius: 16px; color: #fff;\">";
                    // line 113
                    echo twig_escape_filter($this->env, Yii::t("wap", "Bỏ qua"), "html", null, true);
                    echo "</a>
                            ";
                }
                // line 115
                echo "
                        </div>
                    </div>
                ";
            } else {
                // line 119
                echo "

                ";
            }
            // line 122
            echo "
                <div class=\"banner-film\"
                     id=\"play-area\" ";
            // line 124
            if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "suggest_package_id", array()) && ($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "popup", array()) || (isset($context["popupGA"]) ? $context["popupGA"] : null)))) {
                echo " style=\"display:none\" ";
            }
            echo ">
                    <div
                            ";
            // line 126
            if (($this->getAttribute((isset($context["streams"]) ? $context["streams"] : null), "errorCode", array()) == 401)) {
                // line 127
                echo "                                onClick=\"window.location='";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("auth/login?popup=1"), "html", null, true);
                echo "'\"
";
                // line 130
                echo "                            ";
            } else {
                // line 131
                echo "                                onClick=\"\$('#popup-step1').fadeIn()\"
                            ";
            }
            // line 132
            echo " >
                        <img src=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "coverImage", array()), "html", null, true);
            echo "\" width=\"100%\"
                             onError=\"this.onerror=null;this.src='/images/16x9.png';\"/>
                        <div class=\"overlay-video youtube video-js\">
                            <button class=\"vjs-big-play-button\" type=\"button\" aria-live=\"polite\" title=\"Play Video\"
                                    aria-disabled=\"false\"><span aria-hidden=\"true\"
                                                                class=\"vjs-icon-placeholder\"></span><span
                                        class=\"vjs-control-text\">";
            // line 139
            echo twig_escape_filter($this->env, Yii::t("wap", "Phát video"), "html", null, true);
            echo "</span></button>
                        </div>
                    </div>
                </div>

            ";
        }
        // line 145
        echo "        </div>
        <div class=\"ctn\">
            <a class=\"right toggleBtn trans\" data-target=\"#videoDesc\" data-ani=\"rotate180\" href=\"#\">
                <svg class=\"ico\">
                    <use xlink:href=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-down-solid"), "html", null, true);
        echo "\"></use>
                </svg>
            </a>
            <h6 class=\"title\">";
        // line 152
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "</h6>
            <div class=\"txtInfo\">
                ";
        // line 154
        echo twig_escape_filter($this->env, Yii::t("wap", "{count} lượt xem", array("count" => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "play_times", array()))), "html", null, true);
        echo "
            </div>
        </div>
        <div class=\"ctn\">
            <ul class=\"btnList list5Items alCenter\">
                <li><a href=\"javascript:void(0)\" data-like=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "isFavourite", array()), "html", null, true);
        echo "\"
                       data-number=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "likeCount", array()), "html", null, true);
        echo "\" data-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\" id=\"linkLike_";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\"
                       class=\"likeBtn\"
                       onclick=\"return addEvent('like_video_action', {'video_id': '";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "', 'video_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "', 'channel_id': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "', 'channel_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "', 'like_type': true });\">
                        <div class=\"onOff\">
                            <svg class=\"ico icoLike ";
        // line 164
        if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "isFavourite", array()) != 1)) {
            echo " active ";
        }
        echo "\">
                                <use xlink:href=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-like"), "html", null, true);
        echo "\"></use>
                            </svg>
                            <svg class=\"ico icoLiked ";
        // line 167
        if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "isFavourite", array()) == 1)) {
            echo " active ";
        }
        echo "\">
                                <use xlink:href=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-liked"), "html", null, true);
        echo "\"></use>
                            </svg>
                        </div>
                        <p class=\"txtInfo\" id=\"numLike_";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "likeCount", array()), "html", null, true);
        echo "</p>
                    </a></li>
                <li><a href=\"javascript:void(0)\" data-dislike=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "isFavourite", array()), "html", null, true);
        echo "\"
                       data-number=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "dislikeCount", array()), "html", null, true);
        echo "\" data-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\"
                       id=\"linkDislike_";
        // line 175
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\" class=\"dislikeBtn\"
                       onclick=\"return addEvent('like_video_action', {'video_id': '";
        // line 176
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "', 'video_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "', 'channel_id': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "', 'channel_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "', 'like_type': false });\">
                        <div class=\"onOff\">
                            <svg class=\"ico icoDislike ";
        // line 178
        if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "isFavourite", array()) !=  -1)) {
            echo " active ";
        }
        echo "\">
                                <use xlink:href=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-dislike"), "html", null, true);
        echo "\"></use>
                            </svg>
                            <svg class=\"ico icoDisliked ";
        // line 181
        if (($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "isFavourite", array()) ==  -1)) {
            echo " active ";
        }
        echo "\">
                                <use xlink:href=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-disliked"), "html", null, true);
        echo "\"></use>
                            </svg>
                        </div>
                        <p class=\"txtInfo\" id=\"numDislike_";
        // line 185
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "dislikeCount", array()), "html", null, true);
        echo "</p>
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 189
        echo "javascript:void(0)";
        echo "\" title=\"";
        echo twig_escape_filter($this->env, Yii::t("web", "Share"), "html", null, true);
        echo "\" id=\"shareVideoDetail\"
                       class=\"txt656565 modalBtn btnShare\" onclick=\"\$('#containerShare').show()\">
                        <svg class=\"ico\">
                            <use xlink:href=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-shareto"), "html", null, true);
        echo "\"></use>
                        </svg>
                        <p class=\"txtInfo\">";
        // line 194
        echo twig_escape_filter($this->env, Yii::t("wap", "Chia sẻ"), "html", null, true);
        echo "</p>
                    </a>
                </li>
                <li>
                    <a class=\"optMnBtn detailVideoBtnAddTo\" data-id=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\"
                       data-url=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "slug" => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "slug", array()))), "html", null, true);
        echo "\"
                       data-target=\"#playlistOpt\">
                        <svg class=\"ico\">
                            <use xlink:href=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-addto"), "html", null, true);
        echo "\"></use>
                        </svg>
                        <p class=\"txtInfo\">";
        // line 204
        echo twig_escape_filter($this->env, Yii::t("wap", "Thêm vào"), "html", null, true);
        echo "</p>
                    </a>
                </li>
                <li>
                    <a class=\"optMnBtn\" data-target=\"#reportOtp\">
                        <svg class=\"ico\">
                            <use xlink:href=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-flag"), "html", null, true);
        echo "\"></use>
                        </svg>
                        <p class=\"txtInfo\">";
        // line 212
        echo twig_escape_filter($this->env, Yii::t("wap", "Báo cáo"), "html", null, true);
        echo "</p>
                    </a>
                </li>
            </ul>
        </div>

        <input type=\"hidden\" id=\"is_follow\" value=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "isFollow", array()), "html", null, true);
        echo "\"/>

        <div class=\"user blkBdTop ctn\">

            <a class=\"";
        // line 222
        echo (($this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "isFollow", array())) ? ("btn right") : ("btn btnOutlineBlueS right"));
        echo "\" href=\"javascript:void(0)\"
               id=\"followVideo\" data-id=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "\"
               onclick=\"return addEvent('follow_video_action', {'video_id': '";
        // line 224
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "', 'video_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "', 'channel_id': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "', 'channel_name': '";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "', 'follow_type ': !!+(1-\$('#is_follow').val()) });\">
                <svg class=\"ico\">
                    <use xlink:href=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-follow"), "html", null, true);
        echo "\"></use>
                </svg>
                ";
        // line 228
        $context["a1"] = Yii::t("wap", "Đã theo dõi");
        // line 229
        echo "                ";
        $context["a2"] = Yii::t("wap", "Theo dõi");
        // line 230
        echo "                <span>";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "isFollow", array())) ? (Yii::t("wap", "Đã theo dõi")) : (Yii::t("wap", "Theo dõi"))), "html", null, true);
        echo "</span>
            </a>

            <div class=\"avatar pthld left\"><a href=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-detail", array("id" => $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()))), "html", null, true);
        echo "\">
                    <img src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "avatarImage", array()), "html", null, true);
        echo "\"
                         onError=\"this.onerror=null;this.src='/images/avatar.gif'\" alt=\"\"></a>
            </div>
            <div class=\"ctnhld\">
                <input type=\"hidden\" id=\"followVideoURL\" value=\"";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("account/follow-video"), "html", null, true);
        echo "\"/>
                <div><a href=\"";
        // line 239
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-detail", array("id" => $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "</a></div>
                <div class=\"txtInfo\" id=\"total_follow\">";
        // line 240
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "followCount", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, Yii::t("wap", "theo dõi"), "html", null, true);
        echo " </div>
            </div>
        </div>

        <div class=\"desc blkBdTop ctn txtInfo toggle\" id=\"videoDesc\">
            <p>";
        // line 245
        echo twig_escape_filter($this->env, Yii::t("wap", "Xuất bản"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "publishedTime", array()), "html", null, true);
        echo "</p>
            <div>";
        // line 246
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "description", array()), "html", null, true);
        echo "</div>
        </div>

    </section>

    ";
        // line 252
        echo "        ";
        // line 253
        echo "    ";
        // line 254
        echo "
    <div id=\"relatedBox\">
    </div>

    ";
        // line 258
        $this->loadTemplate("@app/views/partials/commentBox.twig", "detail.twig", 258)->display(array_merge($context, array("contentId" => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "content" => $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "avatarImage" => (isset($context["avatarImage"]) ? $context["avatarImage"] : null), "type" => "VOD", "detail" => (isset($context["detail"]) ? $context["detail"] : null))));
        // line 259
        echo "    ";
        $this->loadTemplate("@app/views/partials/shareSocialModal.twig", "detail.twig", 259)->display(array_merge($context, array("shareLink" => (isset($context["shareLink"]) ? $context["shareLink"] : null), "detail" => (isset($context["detail"]) ? $context["detail"] : null))));
        // line 260
        echo "</div>

";
        // line 262
        $this->loadTemplate("@app/views/partials/popupCommon.twig", "detail.twig", 262)->display($context);
        // line 263
        echo "
<div class=\"modal modalPopup\" id=\"reportOtp\">
    <div class=\"ctn\">
        <h5>";
        // line 266
        echo twig_escape_filter($this->env, Yii::t("wap", "Báo cáo vi phạm"), "html", null, true);
        echo "</h5>
        ";
        // line 267
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "setting.feedback", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["report"]) {
            // line 268
            echo "            <p>
                <input id=\"rdo_";
            // line 269
            echo twig_escape_filter($this->env, $this->getAttribute($context["report"], "id", array()), "html", null, true);
            echo "\" type=\"radio\" name=\"report\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["report"], "id", array()), "html", null, true);
            echo "\">
                <label for=\"rdo_";
            // line 270
            echo twig_escape_filter($this->env, $this->getAttribute($context["report"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, Yii::t("wap", $this->getAttribute($context["report"], "content", array())), "html", null, true);
            echo "</label>
            </p>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['report'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 273
        echo "        <p>";
        echo twig_escape_filter($this->env, Yii::t("wap", "Nếu bạn là chủ sở hữu video vui lòng"), "html", null, true);
        echo " <a href=\"/lien-he\" style=\"color:blue\">";
        echo twig_escape_filter($this->env, Yii::t("wap", "làm theo hướng dẫn"), "html", null, true);
        echo "</a>
            ";
        // line 274
        echo twig_escape_filter($this->env, Yii::t("wap", "để thông báo về nội dung vi phạm bản quyền."), "html", null, true);
        echo "</p>
        <div class=\"action alRight\">
            <a href=\"javascript:void(0)\" id=\"piracy-cancel\" class=\"close\" href=\"javascript:void(0)\">";
        // line 276
        echo twig_escape_filter($this->env, Yii::t("wap", "HỦY"), "html", null, true);
        echo "</a>
            <span></span>
            <a href=\"javascript:void(0)\" onclick=\"postFeedBack()\" id=\"report-issue\" class=\"ok\">";
        // line 278
        echo twig_escape_filter($this->env, Yii::t("wap", "ĐỒNG Ý"), "html", null, true);
        echo "</a>
            <input type=\"hidden\" id=\"postFeedbackUrl\" value=\"";
        // line 279
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("default/feed-back"), "html", null, true);
        echo "\">
            <input type=\"hidden\" id=\"contentId\" value=\"";
        // line 280
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id", array()), "html", null, true);
        echo "\">
            <input type=\"hidden\" id=\"channelId\" value=\"";
        // line 281
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "id", array()), "html", null, true);
        echo "\">
            <input type=\"hidden\" id=\"channel_name\" value=\"";
        // line 282
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "owner", array()), "name", array()), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"category_id\" value=\"";
        // line 283
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "cate", array()), "id", array()), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"category_name\" value=\"";
        // line 284
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "cate", array()), "name", array()), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"category_parent_id\" value=\"";
        // line 285
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "cate", array()), "parent_id", array()), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"video_name\" value=\"";
        // line 286
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"tag_name\" value=\"";
        // line 287
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "tag", array()), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"typeReport\" value=\"VOD\">
            <input type=\"hidden\" id=\"contentReport\" value=\"";
        // line 289
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name", array()), "html", null, true);
        echo "\">
            <input type=\"hidden\" id=\"related_limit\" value=\"";
        // line 290
        echo twig_escape_filter($this->env, (isset($context["relatedLimit"]) ? $context["relatedLimit"] : null), "html", null, true);
        echo "\"/>
            <input type=\"hidden\" id=\"related_offset\" value=\"0\"/>
        </div>
    </div>
</div>

<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"containerShare\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\" id=\"containerShareLink\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\">";
        // line 300
        echo twig_escape_filter($this->env, Yii::t("web", "Chia sẻ"), "html", null, true);
        echo "</h5>
                <button id=\"closeContainerShare\" type=\"button\" class=\"btn btn-secondary\" style=\"float: right; background-color: white\" >X</button>
            </div>
            <textarea class=\"form-control\" id=\"shareWebsite\" rows=\"3\" style=\"display: inline-table; padding: inherit\">";
        // line 303
        echo (isset($context["shareLink"]) ? $context["shareLink"] : null);
        echo "</textarea>
            <br>
            <div class=\"modal-footer\">
                <button id=\"copyLinkShare\" type=\"button\" class=\"btn btn-primary\">";
        // line 306
        echo twig_escape_filter($this->env, Yii::t("wap", "Copy"), "html", null, true);
        echo "</button>
            </div>
            <div id=\"copy-success-link\" class=\"alert alert-success\">
                <strong>";
        // line 309
        echo twig_escape_filter($this->env, Yii::t("wap", "Link copied to clipboard"), "html", null, true);
        echo "</strong>
            </div>
        </div>
    </div>
</div>

";
        // line 315
        if ($this->getAttribute($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "cate", array()), "is_world_cup_category", array())) {
            // line 316
            echo "    <input type=\"hidden\" id=\"popup-suggest-register-month-package\" value=\"1\">
";
        }
        // line 318
        echo "
";
        // line 319
        if ((isset($context["trackingContent"]) ? $context["trackingContent"] : null)) {
            // line 320
            echo "    ";
            $this->loadTemplate("@app/views/partials/trackingView.twig", "detail.twig", 320)->display(array_merge($context, array("trackingContent" => (isset($context["trackingContent"]) ? $context["trackingContent"] : null))));
        }
    }

    public function getTemplateName()
    {
        return "detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  797 => 320,  795 => 319,  792 => 318,  788 => 316,  786 => 315,  777 => 309,  771 => 306,  765 => 303,  759 => 300,  746 => 290,  742 => 289,  737 => 287,  733 => 286,  729 => 285,  725 => 284,  721 => 283,  717 => 282,  713 => 281,  709 => 280,  705 => 279,  701 => 278,  696 => 276,  691 => 274,  684 => 273,  673 => 270,  667 => 269,  664 => 268,  660 => 267,  656 => 266,  651 => 263,  649 => 262,  645 => 260,  642 => 259,  640 => 258,  634 => 254,  632 => 253,  630 => 252,  622 => 246,  616 => 245,  606 => 240,  600 => 239,  596 => 238,  589 => 234,  585 => 233,  578 => 230,  575 => 229,  573 => 228,  568 => 226,  557 => 224,  553 => 223,  549 => 222,  542 => 218,  533 => 212,  528 => 210,  519 => 204,  514 => 202,  508 => 199,  504 => 198,  497 => 194,  492 => 192,  484 => 189,  475 => 185,  469 => 182,  463 => 181,  458 => 179,  452 => 178,  441 => 176,  437 => 175,  431 => 174,  427 => 173,  420 => 171,  414 => 168,  408 => 167,  403 => 165,  397 => 164,  386 => 162,  377 => 160,  373 => 159,  365 => 154,  360 => 152,  354 => 149,  348 => 145,  339 => 139,  330 => 133,  327 => 132,  323 => 131,  320 => 130,  315 => 127,  313 => 126,  306 => 124,  302 => 122,  297 => 119,  291 => 115,  286 => 113,  282 => 111,  277 => 109,  272 => 108,  270 => 107,  262 => 104,  259 => 103,  251 => 102,  245 => 99,  241 => 98,  236 => 96,  231 => 94,  223 => 91,  220 => 90,  212 => 89,  205 => 85,  202 => 84,  200 => 83,  197 => 82,  194 => 81,  187 => 77,  182 => 75,  178 => 74,  174 => 72,  172 => 71,  164 => 68,  157 => 66,  154 => 65,  152 => 64,  148 => 62,  144 => 60,  142 => 59,  139 => 58,  135 => 56,  129 => 54,  123 => 52,  121 => 51,  118 => 50,  116 => 49,  111 => 46,  107 => 44,  105 => 43,  100 => 41,  96 => 40,  92 => 39,  88 => 38,  84 => 37,  80 => 36,  76 => 35,  70 => 32,  67 => 31,  63 => 29,  57 => 27,  55 => 26,  52 => 25,  50 => 24,  47 => 23,  44 => 11,  42 => 10,  39 => 9,  37 => 8,  34 => 7,  32 => 6,  30 => 5,  28 => 4,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/DetailAsset') }}*/
/* {{ register_detail_asset() }}*/
/* */
/* {% set detail = responseData.detail %}*/
/* {% set streams = responseData.streams %}*/
/* {% set relateds = responseData.relateds.content %}*/
/* */
/* {% set isRegisterFast = streams.popup.is_register_fast %}*/
/* */
/* {% if storeUrl %}*/
/*     {#<script>*/
/*         window.onload = function() {*/
/*             <!-- Deep link URL for existing users with app already installed on their device -->*/
/*             window.location = '{{ storeUrl|raw }}';*/
/*             <!-- Download URL (TUNE link) for new users to download the app -->*/
/* */
/*             {% if appStoreUrl %}*/
/*                 setTimeout("window.location = '{{ appStoreUrl }}';", 1000);*/
/*             {% endif %}*/
/*         }*/
/*     </script>#}*/
/* {% endif %}*/
/* */
/* {% if streams.errorCode == 200 %}*/
/*     <input type="hidden" id="play" name="play" value="true"/>*/
/*     {% if streams.urlStreaming and not popupGA %}*/
/*         <input type="hidden" id="url" name="url" value="{{ streams.urlStreaming }}"/>*/
/*     {% else %}*/
/*         <input type="hidden" id="url" name="url" value=""/>*/
/*     {% endif %}*/
/* */
/*     <input type="hidden" id="poster" name="poster" value="{{ detail.coverImage }}"/>*/
/*     <input type="hidden" id="play" value="true"/>*/
/*     <input type="hidden" id="media_type" value="VOD"/>*/
/*     <input type="hidden" id="is_pc" value="{{ app.session.get('is_pc') }}"/>*/
/*     <input type="hidden" id="is_ios" value="{{ app.session.get('is_ios') }}"/>*/
/*     <input type="hidden" id="ajaxUrl" value="{{ url('/account/watch-time') }}"/>*/
/*     <input type="hidden" id="playId" value="{{ detail.id }}"/>*/
/*     <input type="hidden" id="playPosition" value="{{ responseData.currentTime }}"/>*/
/*     <input type="hidden" id="parentId" value="{{ detail.id }}"/>*/
/*     <input type="hidden" id="language" value="{{ detail.getLang(detail.multilang) }}"/>*/
/* {% endif %}*/
/* {% if streams.popup %}*/
/*     {% include '@app/views/partials/popup.twig' with {'type': 'VOD', 'popup':streams.popup, 'autoDisplay': streams.autoDisplay, 'msisdn': msisdn } %}*/
/* {% endif %}*/
/* */
/* <div>*/
/*     <section id="section_detail" class="blk blkVideoDetail">*/
/*         {% if detail.suggest_package_id and ((streams.popup and streams.errorCode!=200) or popupGA) and streams.errorCode!=401 %}*/
/*             <div class="container" style="padding: 10px; text-align: center; color: black">*/
/*                 {% if  streams.popup.confirm %}*/
/*                     {{ streams.popup.confirm }}*/
/*                 {% else %}*/
/*                     {{ popupGA }}*/
/*                 {% endif %}*/
/*             </div>*/
/*         {% endif %}*/
/* */
/*         {% if popupGA %}*/
/*             <input type="hidden" id="google-adwords" value="1"/>*/
/*         {% endif %}*/
/* */
/*         <div class="screen pthld" style="height: 100%">*/
/*             {% if streams.urlStreaming and not popupGA %}*/
/* */
/*                 <div id="playerBox" {% if showTime != null %}style="display: none" {% endif %}>*/
/*                     <video id="videoBox" class="video-js  vjs-16-9 youtube"*/
/*                            controls {% if isIOS %}  webkit-playsinline  playsinline {% endif %}>*/
/*                     </video>*/
/*                 </div>*/
/*                 {% if showTime != null %}*/
/* */
/*                     <input type="hidden" id="idUrlGetStream"*/
/*                            value="{{ url('/video/ajax-get-video-stream', {'id' : detail.id }) }}"/>*/
/*                     <input type="hidden" id="showTimer" value="{{ showTime }}"/>*/
/*                     <div id="countdown" class="screen countdown alCenter">*/
/*                         <h5>{{t('wap', 'Chương trình sẽ bắt đầu sau') }}</h5>*/
/*                         <div class="counter"></div>*/
/*                     </div>*/
/*                 {% endif %}*/
/*             {% else %}*/
/* */
/*                 {% if detail.suggest_package_id and (streams.popup or popupGA) %}*/
/*                     <div class="banner-film" id="suggest-package">*/
/*                         <img src="{{ detail.coverImage }}" width="100%"*/
/*                              onError="this.onerror=null;this.src='/images/16x9.png';"/>*/
/*                         <div>*/
/*                             <a id="regSuggestion"*/
/*                                href="javascript:void(0)" {% if streams.errorCode==401 %} onclick="window.location='{{ url('auth/login?popup=1') }}'" {% else %} onClick="$('#regSuggestion').fadeOut();$('#confirmRegSuggestion').fadeIn()" {% endif %}*/
/*                                style="position: absolute; left: 10px; bottom: 20px; background: #0081ff; padding: 6px 20px 6px 15px; border-radius: 20px; color: #fff;">*/
/*                                 <span style="display: inline-block; vertical-align: middle;">{{t('wap', 'Đăng ký') }}<br>{{t('wap', 'để xem video') }}</span>*/
/*                             </a>*/
/* */
/*                             <form id="register-sub-suggestionform" action="{{ url('account/register-service') }}"*/
/*                                   method="post">*/
/*                                 <input type="hidden" name="package_id" value="{{ detail.suggest_package_id }}"/>*/
/*                                 <input type="hidden"*/
/*                                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/* */
/*                                 <a id="confirmRegSuggestion"*/
/*                                    href="javascript:void(0)" {% if streams.errorCode==401 %} onclick="window.location='{{ url('auth/login?popup=1') }}'" {% else %} onClick="$('#register-sub-suggestionform').submit();$(this).removeAttr('onClick')" {% endif %}*/
/*                                    style="display:none; position: absolute; left: 10px; bottom: 20px; background: #0081ff; padding: 6px 20px 6px 15px; border-radius: 20px; color: #fff;">*/
/*                                     <span style="display: inline-block; vertical-align: middle;">{{t('wap', 'Đồng ý') }} <br/>{{t('wap', 'để xem video') }}</span>*/
/*                                 </a>*/
/*                             </form>*/
/*                             {% if popupGA %}*/
/*                                 <a href="{{ detailUrl }}"*/
/*                                    style="position: absolute; right: 10px; bottom: 20px; background: #222; padding: 10px 20px; border-radius: 16px; color: #fff;">{{t('wap', 'Bỏ qua') }}</a>*/
/*                             {% else %}*/
/*                                 <a href="javascript:void(0)"*/
/*                                    onClick="$('#suggest-package').remove();$('#play-area').show()"*/
/*                                    style="position: absolute; right: 10px; bottom: 20px; background: #222; padding: 10px 20px; border-radius: 16px; color: #fff;">{{t('wap', 'Bỏ qua') }}</a>*/
/*                             {% endif %}*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                 {% else %}*/
/* */
/* */
/*                 {% endif %}*/
/* */
/*                 <div class="banner-film"*/
/*                      id="play-area" {% if detail.suggest_package_id and (streams.popup or popupGA) %} style="display:none" {% endif %}>*/
/*                     <div*/
/*                             {% if streams.errorCode==401 %}*/
/*                                 onClick="window.location='{{ url('auth/login?popup=1') }}'"*/
/* {#                            {% elseif isRegisterFast %}#}*/
/* {#                                onClick="document.getElementById('register-sub-form').submit()"#}*/
/*                             {% else %}*/
/*                                 onClick="$('#popup-step1').fadeIn()"*/
/*                             {% endif %} >*/
/*                         <img src="{{ detail.coverImage }}" width="100%"*/
/*                              onError="this.onerror=null;this.src='/images/16x9.png';"/>*/
/*                         <div class="overlay-video youtube video-js">*/
/*                             <button class="vjs-big-play-button" type="button" aria-live="polite" title="Play Video"*/
/*                                     aria-disabled="false"><span aria-hidden="true"*/
/*                                                                 class="vjs-icon-placeholder"></span><span*/
/*                                         class="vjs-control-text">{{t('wap', 'Phát video') }}</span></button>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*             {% endif %}*/
/*         </div>*/
/*         <div class="ctn">*/
/*             <a class="right toggleBtn trans" data-target="#videoDesc" data-ani="rotate180" href="#">*/
/*                 <svg class="ico">*/
/*                     <use xlink:href="{{ url('images/defs.svg#ico-down-solid') }}"></use>*/
/*                 </svg>*/
/*             </a>*/
/*             <h6 class="title">{{ detail.name }}</h6>*/
/*             <div class="txtInfo">*/
/*                 {{ t('wap','{count} lượt xem', {'count': detail.play_times}) }}*/
/*             </div>*/
/*         </div>*/
/*         <div class="ctn">*/
/*             <ul class="btnList list5Items alCenter">*/
/*                 <li><a href="javascript:void(0)" data-like="{{ detail.isFavourite }}"*/
/*                        data-number="{{ detail.likeCount }}" data-id="{{ detail.id }}" id="linkLike_{{ detail.id }}"*/
/*                        class="likeBtn"*/
/*                        onclick="return addEvent('like_video_action', {'video_id': '{{ detail.id }}', 'video_name': '{{ detail.name }}', 'channel_id': '{{ detail.owner.id }}', 'channel_name': '{{ detail.owner.name }}', 'like_type': true });">*/
/*                         <div class="onOff">*/
/*                             <svg class="ico icoLike {% if detail.isFavourite != 1 %} active {% endif %}">*/
/*                                 <use xlink:href="{{ url('images/defs.svg#ico-like') }}"></use>*/
/*                             </svg>*/
/*                             <svg class="ico icoLiked {% if detail.isFavourite == 1 %} active {% endif %}">*/
/*                                 <use xlink:href="{{ url('images/defs.svg#ico-liked') }}"></use>*/
/*                             </svg>*/
/*                         </div>*/
/*                         <p class="txtInfo" id="numLike_{{ detail.id }}">{{ detail.likeCount }}</p>*/
/*                     </a></li>*/
/*                 <li><a href="javascript:void(0)" data-dislike="{{ detail.isFavourite }}"*/
/*                        data-number="{{ detail.dislikeCount }}" data-id="{{ detail.id }}"*/
/*                        id="linkDislike_{{ detail.id }}" class="dislikeBtn"*/
/*                        onclick="return addEvent('like_video_action', {'video_id': '{{ detail.id }}', 'video_name': '{{ detail.name }}', 'channel_id': '{{ detail.owner.id }}', 'channel_name': '{{ detail.owner.name }}', 'like_type': false });">*/
/*                         <div class="onOff">*/
/*                             <svg class="ico icoDislike {% if detail.isFavourite != -1 %} active {% endif %}">*/
/*                                 <use xlink:href="{{ url('images/defs.svg#ico-dislike') }}"></use>*/
/*                             </svg>*/
/*                             <svg class="ico icoDisliked {% if detail.isFavourite == -1 %} active {% endif %}">*/
/*                                 <use xlink:href="{{ url('images/defs.svg#ico-disliked') }}"></use>*/
/*                             </svg>*/
/*                         </div>*/
/*                         <p class="txtInfo" id="numDislike_{{ detail.id }}">{{ detail.dislikeCount }}</p>*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ 'javascript:void(0)' }}" title="{{ t('web','Share') }}" id="shareVideoDetail"*/
/*                        class="txt656565 modalBtn btnShare" onclick="$('#containerShare').show()">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="{{ url('images/defs.svg#ico-shareto') }}"></use>*/
/*                         </svg>*/
/*                         <p class="txtInfo">{{ t('wap', 'Chia sẻ') }}</p>*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a class="optMnBtn detailVideoBtnAddTo" data-id="{{ detail.id }}"*/
/*                        data-url="{{ url('video/detail',{'id':detail.id, 'slug':detail.slug}) }}"*/
/*                        data-target="#playlistOpt">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="{{ url('images/defs.svg#ico-addto') }}"></use>*/
/*                         </svg>*/
/*                         <p class="txtInfo">{{t('wap', 'Thêm vào') }}</p>*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a class="optMnBtn" data-target="#reportOtp">*/
/*                         <svg class="ico">*/
/*                             <use xlink:href="{{ url('images/defs.svg#ico-flag') }}"></use>*/
/*                         </svg>*/
/*                         <p class="txtInfo">{{t('wap', 'Báo cáo') }}</p>*/
/*                     </a>*/
/*                 </li>*/
/*             </ul>*/
/*         </div>*/
/* */
/*         <input type="hidden" id="is_follow" value="{{ detail.owner.isFollow }}"/>*/
/* */
/*         <div class="user blkBdTop ctn">*/
/* */
/*             <a class="{{ (detail.owner.isFollow)?"btn right":"btn btnOutlineBlueS right" }}" href="javascript:void(0)"*/
/*                id="followVideo" data-id="{{ detail.owner.id }}"*/
/*                onclick="return addEvent('follow_video_action', {'video_id': '{{ detail.id }}', 'video_name': '{{ detail.name }}', 'channel_id': '{{ detail.owner.id }}', 'channel_name': '{{ detail.owner.name }}', 'follow_type ': !!+(1-$('#is_follow').val()) });">*/
/*                 <svg class="ico">*/
/*                     <use xlink:href="{{ url('images/defs.svg#ico-follow') }}"></use>*/
/*                 </svg>*/
/*                 {% set a1=t('wap', 'Đã theo dõi') %}*/
/*                 {% set a2=t('wap', 'Theo dõi') %}*/
/*                 <span>{{ (detail.owner.isFollow)?t('wap', 'Đã theo dõi'):t('wap', 'Theo dõi') }}</span>*/
/*             </a>*/
/* */
/*             <div class="avatar pthld left"><a href="{{ url('channel/get-detail',{'id':detail.owner.id}) }}">*/
/*                     <img src="{{ detail.owner.avatarImage }}"*/
/*                          onError="this.onerror=null;this.src='/images/avatar.gif'" alt=""></a>*/
/*             </div>*/
/*             <div class="ctnhld">*/
/*                 <input type="hidden" id="followVideoURL" value="{{ url('account/follow-video') }}"/>*/
/*                 <div><a href="{{ url('channel/get-detail',{'id':detail.owner.id}) }}">{{ detail.owner.name }}</a></div>*/
/*                 <div class="txtInfo" id="total_follow">{{ detail.owner.followCount }}{{" "}}{{t('wap', 'theo dõi') }} </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="desc blkBdTop ctn txtInfo toggle" id="videoDesc">*/
/*             <p>{{t('wap', 'Xuất bản') }} {{ detail.publishedTime }}</p>*/
/*             <div>{{ detail.description }}</div>*/
/*         </div>*/
/* */
/*     </section>*/
/* */
/*     {#{% if relateds|length >0 %}#}*/
/*         {#{% include '@app/views/partials/relatedVideoBox.twig' with {'data': relateds} %}#}*/
/*     {#{% endif %}#}*/
/* */
/*     <div id="relatedBox">*/
/*     </div>*/
/* */
/*     {% include '@app/views/partials/commentBox.twig' with {'contentId': detail.id , 'content': detail.name , 'avatarImage': avatarImage,  'type': 'VOD', 'detail': detail} %}*/
/*     {% include '@app/views/partials/shareSocialModal.twig' with {'shareLink': shareLink, 'detail': detail} %}*/
/* </div>*/
/* */
/* {% include '@app/views/partials/popupCommon.twig' %}*/
/* */
/* <div class="modal modalPopup" id="reportOtp">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'Báo cáo vi phạm') }}</h5>*/
/*         {% for report in app.params['setting.feedback'] %}*/
/*             <p>*/
/*                 <input id="rdo_{{ report.id }}" type="radio" name="report" value="{{ report.id }}">*/
/*                 <label for="rdo_{{ report.id }}">{{ t('wap', report.content) }}</label>*/
/*             </p>*/
/*         {% endfor %}*/
/*         <p>{{t('wap', 'Nếu bạn là chủ sở hữu video vui lòng') }} <a href="/lien-he" style="color:blue">{{t('wap', 'làm theo hướng dẫn') }}</a>*/
/*             {{t('wap', 'để thông báo về nội dung vi phạm bản quyền.') }}</p>*/
/*         <div class="action alRight">*/
/*             <a href="javascript:void(0)" id="piracy-cancel" class="close" href="javascript:void(0)">{{t('wap', 'HỦY') }}</a>*/
/*             <span></span>*/
/*             <a href="javascript:void(0)" onclick="postFeedBack()" id="report-issue" class="ok">{{t('wap', 'ĐỒNG Ý') }}</a>*/
/*             <input type="hidden" id="postFeedbackUrl" value="{{ url('default/feed-back') }}">*/
/*             <input type="hidden" id="contentId" value="{{ detail.id }}">*/
/*             <input type="hidden" id="channelId" value="{{ detail.owner.id }}">*/
/*             <input type="hidden" id="channel_name" value="{{ detail.owner.name }}"/>*/
/*             <input type="hidden" id="category_id" value="{{ detail.cate.id }}"/>*/
/*             <input type="hidden" id="category_name" value="{{ detail.cate.name }}"/>*/
/*             <input type="hidden" id="category_parent_id" value="{{ detail.cate.parent_id }}"/>*/
/*             <input type="hidden" id="video_name" value="{{ detail.name }}"/>*/
/*             <input type="hidden" id="tag_name" value="{{ detail.tag }}"/>*/
/*             <input type="hidden" id="typeReport" value="VOD">*/
/*             <input type="hidden" id="contentReport" value="{{ detail.name }}">*/
/*             <input type="hidden" id="related_limit" value="{{ relatedLimit }}"/>*/
/*             <input type="hidden" id="related_offset" value="0"/>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="modal" tabindex="-1" role="dialog" id="containerShare">*/
/*     <div class="modal-dialog" role="document">*/
/*         <div class="modal-content" id="containerShareLink">*/
/*             <div class="modal-header">*/
/*                 <h5 class="modal-title">{{t('web','Chia sẻ')}}</h5>*/
/*                 <button id="closeContainerShare" type="button" class="btn btn-secondary" style="float: right; background-color: white" >X</button>*/
/*             </div>*/
/*             <textarea class="form-control" id="shareWebsite" rows="3" style="display: inline-table; padding: inherit">{{ shareLink | raw }}</textarea>*/
/*             <br>*/
/*             <div class="modal-footer">*/
/*                 <button id="copyLinkShare" type="button" class="btn btn-primary">{{t('wap','Copy')}}</button>*/
/*             </div>*/
/*             <div id="copy-success-link" class="alert alert-success">*/
/*                 <strong>{{t('wap','Link copied to clipboard')}}</strong>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* {% if detail.cate.is_world_cup_category %}*/
/*     <input type="hidden" id="popup-suggest-register-month-package" value="1">*/
/* {% endif %}*/
/* */
/* {% if trackingContent %}*/
/*     {% include '@app/views/partials/trackingView.twig' with {'trackingContent': trackingContent} %}*/
/* {% endif %}*/
/* */
