<?php

/* @app/views/partials/createPlayListModal.twig */
class __TwigTemplate_b355b819797acfaf892a254517ca618e5daed436b9fa8edabf6273589487853b extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal modalPopup\" id=\"createPlaylistModal\">
    <div class=\"ctn\">
        <h5>";
        // line 3
        echo twig_escape_filter($this->env, Yii::t("wap", "Danh sách phát mới"), "html", null, true);
        echo "</h5>
        <form action=\"/playlist/create\" id=\"addPlaylistForm\" method=\"post\">
            <div class=\"ctn\" style=\"padding:0\">
                <div class=\"form-group\">
                    <input class=\"ipt iptLg\" placeholder=\"";
        // line 7
        echo twig_escape_filter($this->env, Yii::t("wap", "Nhập tên danh sách mới"), "html", null, true);
        echo "\" autofocus  name=\"namePlaylist\" rows=\"1\"/>
                    <input type=\"hidden\"  name=\"";
        // line 8
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
        echo "\" value=\"";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
        echo "\">
                </div>
            </div>
            <div class=\"controls\">
                <a class=\"btn btnBlue ok\" href=\"javascript:void(0)\">";
        // line 12
        echo twig_escape_filter($this->env, Yii::t("wap", "Tạo mới"), "html", null, true);
        echo "</a>
                <a class=\"btn btnOutline close\" href=\"javascript:void(0)\">";
        // line 13
        echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
        echo "</a>
            </div>
        </form>
    </div>
</div>


<div class=\"modal modalPopup\" id=\"editPlaylistModal\">
    <div class=\"ctn\">
        <h5>";
        // line 22
        echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa playlist"), "html", null, true);
        echo "</h5>
        <div class=\"ctn\">
            <form action=\"/playlist/update-playlist\" id=\"editPlaylistForm\" name=\"editPlaylistForm\" method=\"post\">
                <input type=\"hidden\" name=\"id\" id =\"idPlaylist\" value=\"\">
                <input type=\"hidden\"
                       name=\"";
        // line 27
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->csrfParam;"), "method");
        echo "\"
                       value=\"";
        // line 28
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "renderDynamic", array(0 => "return \\Yii::\$app->getRequest()->getCsrfToken();"), "method");
        echo "\">
                <p id=\"error_message\"  style=\"color: red\"></p>
                <p>";
        // line 30
        echo twig_escape_filter($this->env, Yii::t("wap", "TÊN"), "html", null, true);
        echo "</p>
                <input type=\"text\" maxlength=\"255\" id = \"editName\" name=\"name\" class=\"ipt padding0 iptLg\">
                <p class=\"mt10\">";
        // line 32
        echo twig_escape_filter($this->env, Yii::t("wap", "MÔ TẢ"), "html", null, true);
        echo "</p>
                <input type=\"text\"  maxlength=\"500\" id = \"editDescription\"  name=\"description\" class=\"ipt padding0 iptLg \">
                </p>
                <div class=\"action alRight\">
                    <a class=\"ok\" href=\"javascript:void(0)\" onclick=\"validateEditPlayslist()\">";
        // line 36
        echo twig_escape_filter($this->env, Yii::t("wap", "Lưu"), "html", null, true);
        echo "</a>
                    <a class=\"close\" href=\"javascript:void(0)\">";
        // line 37
        echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
        echo "</a>
                </div>
            </form>
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "@app/views/partials/createPlayListModal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 37,  88 => 36,  81 => 32,  76 => 30,  71 => 28,  67 => 27,  59 => 22,  47 => 13,  43 => 12,  34 => 8,  30 => 7,  23 => 3,  19 => 1,);
    }
}
/* <div class="modal modalPopup" id="createPlaylistModal">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'Danh sách phát mới') }}</h5>*/
/*         <form action="/playlist/create" id="addPlaylistForm" method="post">*/
/*             <div class="ctn" style="padding:0">*/
/*                 <div class="form-group">*/
/*                     <input class="ipt iptLg" placeholder="{{t('wap', 'Nhập tên danh sách mới') }}" autofocus  name="namePlaylist" rows="1"/>*/
/*                     <input type="hidden"  name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}" value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                 </div>*/
/*             </div>*/
/*             <div class="controls">*/
/*                 <a class="btn btnBlue ok" href="javascript:void(0)">{{t('wap', 'Tạo mới') }}</a>*/
/*                 <a class="btn btnOutline close" href="javascript:void(0)">{{t('wap', 'Đóng') }}</a>*/
/*             </div>*/
/*         </form>*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* <div class="modal modalPopup" id="editPlaylistModal">*/
/*     <div class="ctn">*/
/*         <h5>{{t('wap', 'Chỉnh sửa playlist') }}</h5>*/
/*         <div class="ctn">*/
/*             <form action="/playlist/update-playlist" id="editPlaylistForm" name="editPlaylistForm" method="post">*/
/*                 <input type="hidden" name="id" id ="idPlaylist" value="">*/
/*                 <input type="hidden"*/
/*                        name="{{ this.renderDynamic('return \\Yii::$app->getRequest()->csrfParam;') | raw }}"*/
/*                        value="{{ this.renderDynamic('return \\Yii::$app->getRequest()->getCsrfToken();') | raw }}">*/
/*                 <p id="error_message"  style="color: red"></p>*/
/*                 <p>{{ t('wap','TÊN') }}</p>*/
/*                 <input type="text" maxlength="255" id = "editName" name="name" class="ipt padding0 iptLg">*/
/*                 <p class="mt10">{{ t('wap','MÔ TẢ') }}</p>*/
/*                 <input type="text"  maxlength="500" id = "editDescription"  name="description" class="ipt padding0 iptLg ">*/
/*                 </p>*/
/*                 <div class="action alRight">*/
/*                     <a class="ok" href="javascript:void(0)" onclick="validateEditPlayslist()">{{t('wap', 'Lưu') }}</a>*/
/*                     <a class="close" href="javascript:void(0)">{{t('wap', 'Đóng') }}</a>*/
/*                 </div>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* */
