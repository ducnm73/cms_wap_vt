<?php

/* mainHome.twig */
class __TwigTemplate_5357b2ee2a5dba79a28d0fc889a325f5e9de0c9a4981e2ec14fbd48605a563a9 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginPage", array(), "method"), "html", null, true);
        echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/html1/DTD/xhtml1-strict.dtd\">

<html lang=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "charset", array()), "html", null, true);
        echo "\">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>

        ";
        // line 9
        echo $this->getAttribute((isset($context["html"]) ? $context["html"] : null), "csrfMetaTags", array(), "method");
        echo "
        <title>";
        // line 10
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())) ? ($this->getAttribute((isset($context["html"]) ? $context["html"] : null), "encode", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())), "method")) : (Yii::t("wap", "Mạng xã hội video - Myclip"))), "html", null, true);
        echo "</title>
\t\t<link type=\"image/x-icon\" rel=\"shortcut icon\" href=\"/favicon.ico\">
        ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "head", array(), "method"), "html", null, true);
        echo "
        ";
        // line 13
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "mainHome.twig", 13)->display(array_merge($context, array("pos" => "head")));
        // line 14
        echo "    </head>
    <body class=\"";
        // line 15
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "themeUserId"), "method") == 1)) {
            echo "dark ";
        }
        echo "\">
        ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginBody", array(), "method"), "html", null, true);
        echo "
        ";
        // line 17
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "mainHome.twig", 17)->display(array_merge($context, array("pos" => "body")));
        // line 18
        echo "        ";
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "main")), "method");
        echo "
        <main>
            ";
        // line 20
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "

        ";
        // line 22
        echo $this->getAttribute((isset($context["Alert"]) ? $context["Alert"] : null), "widget", array(), "method");
        echo "
        ";
        // line 23
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "distributionId"), "method") <= 0)) {
            // line 24
            echo "            ";
            echo $this->getAttribute((isset($context["FooterWidget"]) ? $context["FooterWidget"] : null), "widget", array(), "method");
            echo "
        ";
        }
        // line 26
        echo "
        ";
        // line 27
        $this->loadTemplate("@app/views/partials/videoOptionModal.twig", "mainHome.twig", 27)->display($context);
        // line 28
        echo "        ";
        $this->loadTemplate("@app/views/partials/createPlayListModal.twig", "mainHome.twig", 28)->display($context);
        // line 29
        echo "        ";
        $this->loadTemplate("@app/views/partials/shareSocialModal.twig", "mainHome.twig", 29)->display($context);
        // line 30
        echo "        ";
        $this->loadTemplate("@app/views/partials/popupCommon.twig", "mainHome.twig", 30)->display($context);
        // line 31
        echo "        ";
        $this->loadTemplate("@app/views/partials/popupMessage.twig", "mainHome.twig", 31)->display($context);
        // line 32
        echo "
        <div id=\"loadMyPlaylist\"></div>
        <input type=\"hidden\" id=\"userId\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "userId"), "method"), "html", null, true);
        echo "\">
        ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endBody", array(), "method"), "html", null, true);
        echo "
        <!--[if IE]>
        <script>svg4everybody();</script>
        <!--<![endif]-->
        ";
        // line 39
        $this->loadTemplate("@app/views/partials/tracking.twig", "mainHome.twig", 39)->display($context);
        // line 40
        echo "        ";
        echo $this->getAttribute((isset($context["TrackingWidget"]) ? $context["TrackingWidget"] : null), "widget", array(), "method");
        echo "
\t\t </main>
        <form class=\"modal modalOptMn\" id=\"languagePop\"  name=\"myform\" action=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url(array(0 => "default/changlanguage")), "html", null, true);
        echo "\">
            <div title=\"";
        // line 43
        echo twig_escape_filter($this->env, Yii::t("wap", "Vui lòng chọn ngôn ngữ!"), "html", null, true);
        echo "\">
                <ul>
                    ";
        // line 48
        echo "                    <li>
                        <a href=\"#\" onclick=\"selectLanguage('en')\">English</a>
                    </li>
                    <li>
                        <a href=\"#\" onclick=\"selectLanguage('mz')\">Portuguese</a>
                    </li>
                </ul>
            </div>
        </form>



    </body>
</html>
";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endPage", array(), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "mainHome.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 62,  138 => 48,  133 => 43,  129 => 42,  123 => 40,  121 => 39,  114 => 35,  110 => 34,  106 => 32,  103 => 31,  100 => 30,  97 => 29,  94 => 28,  92 => 27,  89 => 26,  83 => 24,  81 => 23,  77 => 22,  72 => 20,  66 => 18,  64 => 17,  60 => 16,  54 => 15,  51 => 14,  49 => 13,  45 => 12,  40 => 10,  36 => 9,  30 => 6,  25 => 4,  19 => 1,);
    }
}
/* {{ this.beginPage() }}*/
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/html1/DTD/xhtml1-strict.dtd">*/
/* */
/* <html lang="{{ app.language }}">*/
/*     <head>*/
/*         <meta charset="{{ app.charset }}">*/
/*         <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>*/
/* */
/*         {{ html.csrfMetaTags() | raw }}*/
/*         <title>{{ (this.title)?html.encode(this.title):t('wap', 'Mạng xã hội video - Myclip') }}</title>*/
/* 		<link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">*/
/*         {{ this.head() }}*/
/*         {% include '@app/views/partials/trackingHead.twig' with {'pos': 'head'} %}*/
/*     </head>*/
/*     <body class="{% if app.session.get('themeUserId') == 1 %}dark {% endif %}">*/
/*         {{ this.beginBody() }}*/
/*         {% include '@app/views/partials/trackingHead.twig' with {'pos': 'body'} %}*/
/*         {{ HeaderWidget.widget({'type':'main'}) | raw }}*/
/*         <main>*/
/*             {{ content | raw }}*/
/* */
/*         {{ Alert.widget() | raw }}*/
/*         {% if app.session.get('distributionId') <= 0 %}*/
/*             {{ FooterWidget.widget() | raw }}*/
/*         {% endif %}*/
/* */
/*         {% include '@app/views/partials/videoOptionModal.twig' %}*/
/*         {% include '@app/views/partials/createPlayListModal.twig' %}*/
/*         {% include '@app/views/partials/shareSocialModal.twig' %}*/
/*         {% include '@app/views/partials/popupCommon.twig' %}*/
/*         {% include '@app/views/partials/popupMessage.twig' %}*/
/* */
/*         <div id="loadMyPlaylist"></div>*/
/*         <input type="hidden" id="userId" value="{{ app.session.get('userId') }}">*/
/*         {{ this.endBody() }}*/
/*         <!--[if IE]>*/
/*         <script>svg4everybody();</script>*/
/*         <!--<![endif]-->*/
/*         {% include '@app/views/partials/tracking.twig' %}*/
/*         {{ TrackingWidget.widget() | raw }}*/
/* 		 </main>*/
/*         <form class="modal modalOptMn" id="languagePop"  name="myform" action="{{ url(['default/changlanguage']) }}">*/
/*             <div title="{{t('wap', 'Vui lòng chọn ngôn ngữ!') }}">*/
/*                 <ul>*/
/*                     {# <li>*/
/*                         <a href="#" onclick="selectLanguage('vi')">Vietnamese</a>*/
/*                     </li> #}*/
/*                     <li>*/
/*                         <a href="#" onclick="selectLanguage('en')">English</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#" onclick="selectLanguage('mz')">Portuguese</a>*/
/*                     </li>*/
/*                 </ul>*/
/*             </div>*/
/*         </form>*/
/* */
/* */
/* */
/*     </body>*/
/* </html>*/
/* {{ this.endPage() }}*/
/* */
