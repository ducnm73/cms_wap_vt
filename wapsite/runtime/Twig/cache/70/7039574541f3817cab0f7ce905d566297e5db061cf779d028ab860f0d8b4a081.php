<?php

/* main.twig */
class __TwigTemplate_3a955acacd684827de239713e66ad731c5841ea6d4b5dbd0b5d5a28e15ea10bc extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginPage", array(), "method"), "html", null, true);
        echo "

<!DOCTYPE html>
<html lang=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "charset", array()), "html", null, true);
        echo "\">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>

        ";
        // line 9
        echo $this->getAttribute((isset($context["html"]) ? $context["html"] : null), "csrfMetaTags", array(), "method");
        echo "
        <title>";
        // line 10
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())) ? ($this->getAttribute((isset($context["html"]) ? $context["html"] : null), "encode", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "title", array())), "method")) : (Yii::t("wap", "Mạng xã hội video - Myclip"))), "html", null, true);
        echo "</title>
\t\t<link type=\"image/x-icon\" rel=\"shortcut icon\" href=\"/favicon.ico\">
        ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "head", array(), "method"), "html", null, true);
        echo "
        ";
        // line 13
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "main.twig", 13)->display(array_merge($context, array("pos" => "head")));
        // line 14
        echo "
    </head>
    <body class=\"";
        // line 16
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "themeUserId"), "method") == 1)) {
            echo "dark ";
        }
        echo "\">

        ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginBody", array(), "method"), "html", null, true);
        echo "
        ";
        // line 19
        $this->loadTemplate("@app/views/partials/trackingHead.twig", "main.twig", 19)->display(array_merge($context, array("pos" => "body")));
        // line 20
        echo "        <main>
            ";
        // line 22
        echo "        ";
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "
        ";
        // line 23
        echo $this->getAttribute((isset($context["Alert"]) ? $context["Alert"] : null), "widget", array(), "method");
        echo "

        ";
        // line 25
        $this->loadTemplate("@app/views/partials/popupMessage.twig", "main.twig", 25)->display($context);
        // line 26
        echo "
        ";
        // line 27
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "distributionId"), "method") <= 0)) {
            // line 28
            echo "            ";
            echo $this->getAttribute((isset($context["FooterWidget"]) ? $context["FooterWidget"] : null), "widget", array(), "method");
            echo "
        ";
        }
        // line 30
        echo "

        ";
        // line 32
        $this->loadTemplate("@app/views/partials/videoOptionModal.twig", "main.twig", 32)->display($context);
        // line 33
        echo "        ";
        $this->loadTemplate("@app/views/partials/playlistOptionModal.twig", "main.twig", 33)->display(array_merge($context, array("type" => true)));
        // line 34
        echo "        ";
        $this->loadTemplate("@app/views/partials/createPlayListModal.twig", "main.twig", 34)->display($context);
        // line 35
        echo "        ";
        $this->loadTemplate("@app/views/partials/shareSocialModal.twig", "main.twig", 35)->display($context);
        // line 36
        echo "        ";
        $this->loadTemplate("@app/views/partials/shareSocialModal.twig", "main.twig", 36)->display($context);
        // line 37
        echo "        ";
        $this->loadTemplate("@app/views/partials/popupCommon.twig", "main.twig", 37)->display($context);
        // line 38
        echo "\t\t
\t\t
        <div id=\"loadMyPlaylist\"></div>
        <input type=\"hidden\" id=\"userId\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "userId"), "method"), "html", null, true);
        echo "\">
        ";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endBody", array(), "method"), "html", null, true);
        echo "
        <!--[if IE]>
        <script>svg4everybody();</script>
        <!--<![endif]-->
        ";
        // line 46
        $this->loadTemplate("@app/views/partials/tracking.twig", "main.twig", 46)->display($context);
        // line 47
        echo "        ";
        echo $this->getAttribute((isset($context["Alert"]) ? $context["Alert"] : null), "widget", array(), "method");
        echo "
        ";
        // line 48
        echo $this->getAttribute((isset($context["TrackingWidget"]) ? $context["TrackingWidget"] : null), "widget", array(), "method");
        echo "
\t\t</main>
\t\t
    </body>
</html>
";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endPage", array(), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 53,  138 => 48,  133 => 47,  131 => 46,  124 => 42,  120 => 41,  115 => 38,  112 => 37,  109 => 36,  106 => 35,  103 => 34,  100 => 33,  98 => 32,  94 => 30,  88 => 28,  86 => 27,  83 => 26,  81 => 25,  76 => 23,  71 => 22,  68 => 20,  66 => 19,  62 => 18,  55 => 16,  51 => 14,  49 => 13,  45 => 12,  40 => 10,  36 => 9,  30 => 6,  25 => 4,  19 => 1,);
    }
}
/* {{ this.beginPage() }}*/
/* */
/* <!DOCTYPE html>*/
/* <html lang="{{ app.language }}">*/
/*     <head>*/
/*         <meta charset="{{ app.charset }}">*/
/*         <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>*/
/* */
/*         {{ html.csrfMetaTags() | raw }}*/
/*         <title>{{ (this.title)?html.encode(this.title):t('wap', 'Mạng xã hội video - Myclip')  }}</title>*/
/* 		<link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">*/
/*         {{ this.head() }}*/
/*         {% include '@app/views/partials/trackingHead.twig' with {'pos': 'head'} %}*/
/* */
/*     </head>*/
/*     <body class="{% if app.session.get('themeUserId') == 1 %}dark {% endif %}">*/
/* */
/*         {{ this.beginBody() }}*/
/*         {% include '@app/views/partials/trackingHead.twig' with {'pos': 'body'} %}*/
/*         <main>*/
/*             {#gọi nội dung chính#}*/
/*         {{ content | raw }}*/
/*         {{ Alert.widget() | raw }}*/
/* */
/*         {% include '@app/views/partials/popupMessage.twig' %}*/
/* */
/*         {% if app.session.get('distributionId') <= 0 %}*/
/*             {{ FooterWidget.widget() | raw }}*/
/*         {% endif %}*/
/* */
/* */
/*         {% include '@app/views/partials/videoOptionModal.twig' %}*/
/*         {% include '@app/views/partials/playlistOptionModal.twig' with {'type': true}%}*/
/*         {% include '@app/views/partials/createPlayListModal.twig' %}*/
/*         {% include '@app/views/partials/shareSocialModal.twig' %}*/
/*         {% include '@app/views/partials/shareSocialModal.twig' %}*/
/*         {% include '@app/views/partials/popupCommon.twig' %}*/
/* 		*/
/* 		*/
/*         <div id="loadMyPlaylist"></div>*/
/*         <input type="hidden" id="userId" value="{{ app.session.get('userId') }}">*/
/*         {{ this.endBody() }}*/
/*         <!--[if IE]>*/
/*         <script>svg4everybody();</script>*/
/*         <!--<![endif]-->*/
/*         {% include '@app/views/partials/tracking.twig' %}*/
/*         {{ Alert.widget() | raw }}*/
/*         {{ TrackingWidget.widget() | raw }}*/
/* 		</main>*/
/* 		*/
/*     </body>*/
/* </html>*/
/* {{ this.endPage() }}*/
/* */
