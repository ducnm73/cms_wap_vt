<?php

/* reportUserUpload.twig */
class __TwigTemplate_6008e1c61e184fb84ffbc36a42fd48c8917e9fe7601ce75020f6b3726ab20a0b extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ListSmallAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "list_small_asset");
        echo "

";
        // line 4
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "sub", "title" => Yii::t("wap", "Doanh thu"))), "method");
        echo "

<main>
    <section class=\"blk blkIphoneX blkIncome blkPadBoth\">
        <div class=\"ctn\">
            <div class=\"box\">
                <div class=\"ctn\">
                    <span>";
        // line 11
        echo twig_escape_filter($this->env, Yii::t("wap", "Thu nhập tháng này"), "html", null, true);
        echo "</span><h5 class=\"right\"><strong>";
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "currentRevenue", array()), 0, ",", "."), "html", null, true);
        echo "<sup>";
        echo twig_escape_filter($this->env, Yii::t("wap", "đ"), "html", null, true);
        echo "</sup></strong></h5>
                </div>
            </div>
            <p class=\"txtInfo\" style=\"margin-top: 5px;\">";
        // line 14
        echo twig_escape_filter($this->env, Yii::t("wap", "Chúng tôi sẽ thực hiện chi trả khi doanh thu đạt ngưỡng 500.000 VNĐ"), "html", null, true);
        echo "</p>
            <p>&nbsp;</p>

            ";
        // line 17
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "histories", array())) > 0)) {
            // line 18
            echo "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                    <thead>
                    <tr>
                        <th width=\"140\">";
            // line 21
            echo twig_escape_filter($this->env, Yii::t("wap", "Thời gian"), "html", null, true);
            echo "</th>
                        <th>";
            // line 22
            echo twig_escape_filter($this->env, Yii::t("wap", "Thu nhập"), "html", null, true);
            echo "</th>
                        <th width=\"120\">";
            // line 23
            echo twig_escape_filter($this->env, Yii::t("wap", "Trạng thái"), "html", null, true);
            echo "</th>
                    </tr>
                    </thead>
                    <tbody id=\"reportUserUpload\">
                        ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "histories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["history"]) {
                // line 28
                echo "                            <tr ";
                if ($this->getAttribute($context["history"], "display_more", array())) {
                    echo " class=\"display-report\" ";
                }
                echo ">
                                <td>";
                // line 29
                echo twig_escape_filter($this->env, Yii::t("wap", "Tháng"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["history"], "month", array()), "html", null, true);
                echo "</td>
                                <td><strong>";
                // line 30
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["history"], "revenue", array()), 0, ",", "."), "html", null, true);
                echo "<sup>";
                echo twig_escape_filter($this->env, Yii::t("wap", "đ"), "html", null, true);
                echo "</sup></strong></td>
                                <td class=\"txtInfo\">";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["history"], "status", array()), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['history'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "                    </tbody>
                </table>
            ";
        }
        // line 37
        echo "        </div>
        <p>&nbsp;</p>
        <input id=\"reportLimit\" type=\"hidden\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["limit"]) ? $context["limit"] : null), "html", null, true);
        echo "\"/>

        ";
        // line 41
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "histories", array())) >= (isset($context["limit"]) ? $context["limit"] : null))) {
            // line 42
            echo "            <p class=\"alCenter\" id=\"showMoreReportUserUpload\">
                <a class=\"trans\" href=\"javascript:void(0)\" onclick=\"loadMoreReport()\">
                    <svg class=\"ico\">
                        <use xlink:href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-arrow-down-ol"), "html", null, true);
            echo "\"></use>
                    </svg>
                </a>
            </p>
        ";
        }
        // line 50
        echo "    </section>
</main>";
    }

    public function getTemplateName()
    {
        return "reportUserUpload.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 50,  129 => 45,  124 => 42,  122 => 41,  117 => 39,  113 => 37,  108 => 34,  99 => 31,  93 => 30,  87 => 29,  80 => 28,  76 => 27,  69 => 23,  65 => 22,  61 => 21,  56 => 18,  54 => 17,  48 => 14,  38 => 11,  28 => 4,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/ListSmallAsset') }}*/
/* {{ register_list_small_asset() }}*/
/* */
/* {{ HeaderWidget.widget({'type':'sub','title':t('wap', 'Doanh thu')}) | raw }}*/
/* */
/* <main>*/
/*     <section class="blk blkIphoneX blkIncome blkPadBoth">*/
/*         <div class="ctn">*/
/*             <div class="box">*/
/*                 <div class="ctn">*/
/*                     <span>{{ t('wap', 'Thu nhập tháng này') }}</span><h5 class="right"><strong>{{ responseData.currentRevenue|number_format(0, ',', '.') }}<sup>{{ t('wap', 'đ') }}</sup></strong></h5>*/
/*                 </div>*/
/*             </div>*/
/*             <p class="txtInfo" style="margin-top: 5px;">{{ t('wap', 'Chúng tôi sẽ thực hiện chi trả khi doanh thu đạt ngưỡng 500.000 VNĐ') }}</p>*/
/*             <p>&nbsp;</p>*/
/* */
/*             {% if responseData.histories|length > 0 %}*/
/*                 <table cellpadding="0" cellspacing="0" width="100%">*/
/*                     <thead>*/
/*                     <tr>*/
/*                         <th width="140">{{ t('wap', 'Thời gian') }}</th>*/
/*                         <th>{{ t('wap', 'Thu nhập') }}</th>*/
/*                         <th width="120">{{ t('wap', 'Trạng thái') }}</th>*/
/*                     </tr>*/
/*                     </thead>*/
/*                     <tbody id="reportUserUpload">*/
/*                         {% for history in responseData.histories %}*/
/*                             <tr {% if history.display_more %} class="display-report" {% endif %}>*/
/*                                 <td>{{ t('wap', 'Tháng') }} {{ history.month }}</td>*/
/*                                 <td><strong>{{ history.revenue|number_format(0, ',', '.') }}<sup>{{ t('wap', 'đ') }}</sup></strong></td>*/
/*                                 <td class="txtInfo">{{ history.status }}</td>*/
/*                             </tr>*/
/*                         {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             {% endif %}*/
/*         </div>*/
/*         <p>&nbsp;</p>*/
/*         <input id="reportLimit" type="hidden" value="{{ limit }}"/>*/
/* */
/*         {% if responseData.histories|length >= limit %}*/
/*             <p class="alCenter" id="showMoreReportUserUpload">*/
/*                 <a class="trans" href="javascript:void(0)" onclick="loadMoreReport()">*/
/*                     <svg class="ico">*/
/*                         <use xlink:href="{{ url('images/defs.svg#ico-arrow-down-ol') }}"></use>*/
/*                     </svg>*/
/*                 </a>*/
/*             </p>*/
/*         {% endif %}*/
/*     </section>*/
/* </main>*/
