<?php

/* @app/views/partials/language.twig */
class __TwigTemplate_a8278e7593a85f39a63a748d13cfe52b00b71d1fd2d10e869f5f7bc0bf36ce0d extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "themeUserId"), "method") == 1)) {
            // line 2
            echo "    ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()) == "vi")) {
                // line 3
                echo "        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/Icon/1x/viIconmdpi.png"), "html", null, true);
                echo "\" style=\"margin-bottom: -5px;\" alt=\"icon-vi\">
    ";
            }
            // line 5
            echo "    ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()) == "en")) {
                // line 6
                echo "        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/Icon/1x/enlconwhite.png"), "html", null, true);
                echo "\" style=\"margin-bottom: -5px;\" alt=\"icon-en\">
    ";
            }
            // line 8
            echo "    ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()) == "mz")) {
                // line 9
                echo "        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/Icon/1x/mzlconwhite.png"), "html", null, true);
                echo "\" style=\"margin-bottom: -5px;\" alt=\"icon-tz\">
    ";
            }
        } else {
            // line 12
            echo "    ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()) == "vi")) {
                // line 13
                echo "        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/Icon/1x/viIconmdpi.png"), "html", null, true);
                echo "\" style=\"margin-bottom: -5px;\" alt=\"icon-vi\">
    ";
            }
            // line 15
            echo "    ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()) == "en")) {
                // line 16
                echo "        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/Icon/1x/enIconmdpi.png"), "html", null, true);
                echo "\" style=\"margin-bottom: -5px;\" alt=\"icon-en\">
    ";
            }
            // line 18
            echo "    ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "language", array()) == "mz")) {
                // line 19
                echo "        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/Icon/1x/mzlconmdpi.png"), "html", null, true);
                echo "\" style=\"margin-bottom: -5px;\" alt=\"icon-tz\">
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@app/views/partials/language.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 19,  67 => 18,  61 => 16,  58 => 15,  52 => 13,  49 => 12,  42 => 9,  39 => 8,  33 => 6,  30 => 5,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {%  if app.session.get('themeUserId') == 1 %}*/
/*     {% if  app.language =='vi' %}*/
/*         <img src="{{ url('images/Icon/1x/viIconmdpi.png')  }}" style="margin-bottom: -5px;" alt="icon-vi">*/
/*     {% endif %}*/
/*     {% if app.language == 'en' %}*/
/*         <img src="{{ url('images/Icon/1x/enlconwhite.png') }}" style="margin-bottom: -5px;" alt="icon-en">*/
/*     {% endif %}*/
/*     {% if app.language == 'mz' %}*/
/*         <img src="{{ url('images/Icon/1x/mzlconwhite.png') }}" style="margin-bottom: -5px;" alt="icon-tz">*/
/*     {% endif %}*/
/* {% else %}*/
/*     {% if  app.language =='vi' %}*/
/*         <img src="{{ url('images/Icon/1x/viIconmdpi.png')  }}" style="margin-bottom: -5px;" alt="icon-vi">*/
/*     {% endif %}*/
/*     {% if app.language == 'en' %}*/
/*         <img src="{{ url('images/Icon/1x/enIconmdpi.png') }}" style="margin-bottom: -5px;" alt="icon-en">*/
/*     {% endif %}*/
/*     {% if app.language == 'mz' %}*/
/*         <img src="{{ url('images/Icon/1x/mzlconmdpi.png') }}" style="margin-bottom: -5px;" alt="icon-tz">*/
/*     {% endif %}*/
/* {% endif %}*/
