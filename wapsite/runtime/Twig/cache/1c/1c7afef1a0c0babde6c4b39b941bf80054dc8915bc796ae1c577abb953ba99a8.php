<?php

/* @app/views/partials/tracking.twig */
class __TwigTemplate_f3629c6b4bbf1ef783aea40db9f6d8ad49fc0989cb0f92c98ea13be987953c33 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 18
        echo "
";
        // line 19
        if ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|GA2") || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|GA3"))) {
            // line 20
            echo "    ";
            // line 33
            echo "
";
        } elseif ((($this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|FB") || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|FB1"))) {
            // line 35
            echo "    ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|TOP")) {
            // line 63
            echo "
    ";
            // line 75
            echo "
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 76
(isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|HOTVIDEO")) {
            // line 77
            echo "
    ";
            // line 89
            echo "
";
        } elseif ((($this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|FB89") || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "source"), "method") == "WAP|FB99"))) {
            // line 91
            echo "
    ";
            // line 120
            echo "
";
        } else {
            // line 122
            echo "
    ";
            // line 135
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@app/views/partials/tracking.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 135,  58 => 122,  54 => 120,  51 => 91,  49 => 90,  46 => 89,  43 => 77,  41 => 76,  38 => 75,  35 => 63,  33 => 62,  31 => 35,  29 => 34,  26 => 33,  24 => 20,  22 => 19,  19 => 18,);
    }
}
/* {#<script>*/
/*     (function (i, s, o, g, r, a, m) {*/
/*         i['GoogleAnalyticsObject'] = r;*/
/*         i[r] = i[r] || function () {*/
/*             (i[r].q = i[r].q || []).push(arguments)*/
/*         }, i[r].l = 1 * new Date();*/
/*         a = s.createElement(o),*/
/*             m = s.getElementsByTagName(o)[0];*/
/*         a.async = 1;*/
/*         a.src = g;*/
/*         m.parentNode.insertBefore(a, m)*/
/*     })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');*/
/* */
/*     ga('create', 'UA-88258508-1', 'auto');*/
/*     ga('send', 'pageview');*/
/* */
/* </script>#}*/
/* */
/* {% if app.session.get('source') == 'WAP|GA2' or app.session.get('source') == 'WAP|GA3' %}*/
/*     {#<!-- SUB DAT -->*/
/*     <script async src="https://www.googletagmanager.com/gtag/js?id=AW-825172423"></script>*/
/*     <script>*/
/*         window.dataLayer = window.dataLayer || [];*/
/* */
/*         function gtag() {*/
/*             dataLayer.push(arguments);*/
/*         }*/
/* */
/*         gtag('js', new Date());*/
/* */
/*         gtag('config', 'AW-825172423');*/
/*     </script>#}*/
/* */
/* {% elseif  app.session.get('source') == 'WAP|FB' or app.session.get('source') == 'WAP|FB1' %}*/
/*     {#<!-- Facebook Pixel Code -->*/
/*     <script>*/
/*         !function (f, b, e, v, n, t, s) {*/
/*             if (f.fbq) return;*/
/*             n = f.fbq = function () {*/
/*                 n.callMethod ?*/
/*                     n.callMethod.apply(n, arguments) : n.queue.push(arguments)*/
/*             };*/
/*             if (!f._fbq) f._fbq = n;*/
/*             n.push = n;*/
/*             n.loaded = !0;*/
/*             n.version = '2.0';*/
/*             n.queue = [];*/
/*             t = b.createElement(e);*/
/*             t.async = !0;*/
/*             t.src = v;*/
/*             s = b.getElementsByTagName(e)[0];*/
/*             s.parentNode.insertBefore(t, s)*/
/*         }(window, document, 'script',*/
/*             'https://connect.facebook.net/en_US/fbevents.js');*/
/*         fbq('init', '1775833932440003');*/
/*         fbq('track', 'PageView');*/
/*     </script>*/
/*     <noscript>*/
/*         <img height="1" width="1" src="https://www.facebook.com/tr?id=1775833932440003&ev=PageView&noscript=1"/>*/
/*     </noscript>*/
/*     <!-- End Facebook Pixel Code -->#}*/
/* {% elseif  app.session.get('source') == 'WAP|TOP' %}*/
/* */
/*     {#<script async src="https://www.googletagmanager.com/gtag/js?id=AW-989984849"></script>*/
/*     <script>*/
/*         window.dataLayer = window.dataLayer || [];*/
/* */
/*         function gtag() {*/
/*             dataLayer.push(arguments);*/
/*         }*/
/* */
/*         gtag('js', new Date());*/
/*         gtag('config', 'AW-989984849');*/
/*     </script>#}*/
/* */
/* {% elseif  app.session.get('source') == 'WAP|HOTVIDEO' %}*/
/* */
/*     {#<script async src="https://www.googletagmanager.com/gtag/js?id=AW-790192412"></script>*/
/*     <script>*/
/*         window.dataLayer = window.dataLayer || [];*/
/* */
/*         function gtag(){*/
/*             dataLayer.push(arguments);*/
/*         }*/
/*         gtag('js', new Date());*/
/*         gtag('config', 'AW-790192412');*/
/* */
/*     </script>#}*/
/* */
/* {% elseif  app.session.get('source') == 'WAP|FB89' or app.session.get('source') == 'WAP|FB99' %}*/
/* */
/*     {#<!-- Facebook Pixel Code -->*/
/*     <script>*/
/*         !function (f, b, e, v, n, t, s) {*/
/*             if (f.fbq) return;*/
/*             n = f.fbq = function () {*/
/*                 n.callMethod ?*/
/*                     n.callMethod.apply(n, arguments) : n.queue.push(arguments)*/
/*             };*/
/*             if (!f._fbq) f._fbq = n;*/
/*             n.push = n;*/
/*             n.loaded = !0;*/
/*             n.version = '2.0';*/
/*             n.queue = [];*/
/*             t = b.createElement(e);*/
/*             t.async = !0;*/
/*             t.src = v;*/
/*             s = b.getElementsByTagName(e)[0];*/
/*             s.parentNode.insertBefore(t, s)*/
/*         }(window, document, 'script',*/
/*             'https://connect.facebook.net/en_US/fbevents.js');*/
/*         fbq('init', '1775833932440003');*/
/*         fbq('track', 'PageView');*/
/*     </script>*/
/*     <noscript>*/
/*         <img height="1" width="1" style="display:none"*/
/*                    src="https://www.facebook.com/tr?id=1775833932440003&ev=PageView&noscript=1"/>*/
/*     </noscript>*/
/*     <!-- End Facebook Pixel Code -->#}*/
/* */
/* {% else %}*/
/* */
/*     {#<!-- MAIN Global site tag (gtag.js) - Google AdWords: 849928907 -->*/
/*     <script async src="https://www.googletagmanager.com/gtag/js?id=AW-849928907"></script>*/
/*     <script>*/
/*         window.dataLayer = window.dataLayer || [];*/
/* */
/*         function gtag() {*/
/*             dataLayer.push(arguments);*/
/*         }*/
/* */
/*         gtag('js', new Date());*/
/*         gtag('config', 'AW-849928907');*/
/*     </script>#}*/
/* */
/* {% endif %}*/
