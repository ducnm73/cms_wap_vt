<?php

/* detailChannel.twig */
class __TwigTemplate_e70afe6d5cfdb9bbb14f80804b116b255abd0cb8bb0f9d1f404de0b92bd2f10f extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ChannelAsset");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->registerAsset($context, "channel_asset");
        echo "
";
        // line 3
        $context["data"] = $this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array());
        // line 4
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "channel", "title" => (isset($context["title"]) ? $context["title"] : null))), "method");
        echo "
<main>

    <div class=\"blkPadBothExt\">
        <section class=\"blk blkChnlInfo\">
            <div class=\"banner pthld\">
                <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "coverImage", array()), "html", null, true);
        echo "\" onError=\"this.onerror=null;this.src='/images/16x9.png'\" alt=\"\">
            </div>
            ";
        // line 12
        $context["status"] = (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "isFollow", array())) ? (0) : (1));
        // line 13
        echo "            <div class=\"ctn\">
                <div class=\"avatar\"><img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "avatarImage", array()), "html", null, true);
        echo "\" onError=\"this.onerror=null;this.src='/images/16x9.png'\" alt=\"\"></div>
                ";
        // line 15
        if (((isset($context["type"]) ? $context["type"] : null) == "channel")) {
            // line 16
            echo "
                <a id=\"aUpdate\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("/channel/update", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["responseData"]) ? $context["responseData"] : null), "data", array()), "detail", array()), "id", array()))), "html", null, true);
            echo "\"
                   class=\"btn btnOutlineBlueS  right\">
                    <span>";
            // line 19
            echo twig_escape_filter($this->env, Yii::t("wap", "Chỉnh sửa kênh"), "html", null, true);
            echo "</span>
                </a>
                ";
        } else {
            // line 22
            echo "                    <a id=\"aFollow\" onclick=\"followChannelAndNotify(";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array()), "html", null, true);
            echo ");return addEvent('follow_channel_action', {'channel_id': '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array()), "html", null, true);
            echo "', 'channel_name': '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "name", array()), "html", null, true);
            echo "', 'follow_type ': !!+(\$('#status_' + ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array()), "html", null, true);
            echo ").val()) });\"
                       class=\"";
            // line 23
            echo (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "isFollow", array())) ? ("btn  right") : ("btn btnOutlineBlueS  right"));
            echo "\">
                        <svg class=\"ico active\"><use xlink:href=\"/images/defs.svg#ico-follow\"></use></svg>
                        <span>";
            // line 25
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "isFollow", array())) ? (Yii::t("wap", "Đã theo dõi")) : (Yii::t("wap", "Theo dõi"))), "html", null, true);
            echo "</span>
                    </a>
                ";
        }
        // line 28
        echo "                <h6 class=\"title\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "name", array()), "html", null, true);
        echo "
                    ";
        // line 29
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "official", array())) {
            // line 30
            echo "                        <img src=\"/images/icon_official.png\" style=\"width: 12px; height: 12px\">
                    ";
        }
        // line 32
        echo "                </h6>

                <div class=\"txtInfo\">

                    <input type=\"hidden\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array()), "html", null, true);
        echo "\" id=\"channel_id\">
                    <input type=\"hidden\" value=\"";
        // line 37
        echo (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "isFollow", array())) ? (0) : (1));
        echo "\" id=\"status_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array()), "html", null, true);
        echo "\">
                    <span id=\"num-follow\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "num_follow", array()), "html", null, true);
        echo " &nbsp;";
        echo twig_escape_filter($this->env, Yii::t("web", "Lượt theo dõi"), "html", null, true);
        echo "</span>
                </div>
            </div>
        </section>

        ";
        // line 43
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "newest_video", array()), "content", array())) > 0)) {
            // line 44
            echo "            <section class=\"blk blkBdTop blkListVideosVer\">
                ";
            // line 45
            $this->loadTemplate("@app/views/partials/videoSmallBox.twig", "detailChannel.twig", 45)->display(array_merge($context, array("hideChannel" => true, "data" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "newest_video", array()), "hasTitle" => true, "hasPage" => true, "num" => $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "num_video", array()), "id" => ("video_new_of_channel_" . $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array())))));
            // line 46
            echo "            </section>
        ";
        }
        // line 48
        echo "
        ";
        // line 49
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "most_view_video", array()), "content", array())) > 0)) {
            // line 50
            echo "            <section class=\"blk blkBdTop blkListVideosVer\">
                ";
            // line 51
            $this->loadTemplate("@app/views/partials/videoSmallBox.twig", "detailChannel.twig", 51)->display(array_merge($context, array("hideChannel" => true, "data" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "most_view_video", array()), "hasTitle" => true, "hasPage" => true, "num" => $this->getAttribute($this->getAttribute(            // line 52
(isset($context["data"]) ? $context["data"] : null), "detail", array()), "num_video", array()), "id" => ("video_most_view_of_channel_" . $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "detail", array()), "id", array())))));
            // line 53
            echo "            </section>
        ";
        }
        // line 55
        echo "
    </div>

    ";
        // line 58
        if ((isset($context["trackingContent"]) ? $context["trackingContent"] : null)) {
            // line 59
            echo "        ";
            $this->loadTemplate("@app/views/partials/trackingView.twig", "detailChannel.twig", 59)->display(array_merge($context, array("trackingContent" => (isset($context["trackingContent"]) ? $context["trackingContent"] : null))));
            // line 60
            echo "    ";
        }
        // line 61
        echo "</main>";
    }

    public function getTemplateName()
    {
        return "detailChannel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 61,  163 => 60,  160 => 59,  158 => 58,  153 => 55,  149 => 53,  147 => 52,  146 => 51,  143 => 50,  141 => 49,  138 => 48,  134 => 46,  132 => 45,  129 => 44,  127 => 43,  117 => 38,  111 => 37,  107 => 36,  101 => 32,  97 => 30,  95 => 29,  90 => 28,  84 => 25,  79 => 23,  68 => 22,  62 => 19,  57 => 17,  54 => 16,  52 => 15,  48 => 14,  45 => 13,  43 => 12,  38 => 10,  29 => 4,  27 => 3,  23 => 2,  19 => 1,);
    }
}
/* {{ use('wapsite/assets/ChannelAsset') }}*/
/* {{ register_channel_asset() }}*/
/* {% set data = responseData.data %}*/
/* {{ HeaderWidget.widget({'type':'channel','title':title}) | raw }}*/
/* <main>*/
/* */
/*     <div class="blkPadBothExt">*/
/*         <section class="blk blkChnlInfo">*/
/*             <div class="banner pthld">*/
/*                 <img src="{{ data.detail.coverImage }}" onError="this.onerror=null;this.src='/images/16x9.png'" alt="">*/
/*             </div>*/
/*             {% set status = data.detail.isFollow ? 0 :1 %}*/
/*             <div class="ctn">*/
/*                 <div class="avatar"><img src="{{ data.detail.avatarImage }}" onError="this.onerror=null;this.src='/images/16x9.png'" alt=""></div>*/
/*                 {% if type == 'channel' %}*/
/* */
/*                 <a id="aUpdate" href="{{ url('/channel/update',{'id': responseData.data.detail.id}) }}"*/
/*                    class="btn btnOutlineBlueS  right">*/
/*                     <span>{{ t('wap', 'Chỉnh sửa kênh') }}</span>*/
/*                 </a>*/
/*                 {% else %}*/
/*                     <a id="aFollow" onclick="followChannelAndNotify({{ data.detail.id }});return addEvent('follow_channel_action', {'channel_id': '{{ data.detail.id }}', 'channel_name': '{{ data.detail.name }}', 'follow_type ': !!+($('#status_' + {{ data.detail.id }}).val()) });"*/
/*                        class="{{ (data.detail.isFollow)?"btn  right":"btn btnOutlineBlueS  right" }}">*/
/*                         <svg class="ico active"><use xlink:href="/images/defs.svg#ico-follow"></use></svg>*/
/*                         <span>{{ (data.detail.isFollow)?t('wap', 'Đã theo dõi') :t('wap', 'Theo dõi') }}</span>*/
/*                     </a>*/
/*                 {% endif %}*/
/*                 <h6 class="title">{{ data.detail.name }}*/
/*                     {% if data.detail.official %}*/
/*                         <img src="/images/icon_official.png" style="width: 12px; height: 12px">*/
/*                     {% endif %}*/
/*                 </h6>*/
/* */
/*                 <div class="txtInfo">*/
/* */
/*                     <input type="hidden" value="{{ data.detail.id }}" id="channel_id">*/
/*                     <input type="hidden" value="{{ (data.detail.isFollow)?0:1 }}" id="status_{{ data.detail.id }}">*/
/*                     <span id="num-follow">{{ data.detail.num_follow }} &nbsp;{{t('web', 'Lượt theo dõi') }}</span>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/* */
/*         {% if data.newest_video.content|length >0 %}*/
/*             <section class="blk blkBdTop blkListVideosVer">*/
/*                 {% include "@app/views/partials/videoSmallBox.twig" with {'hideChannel': true, 'data': data.newest_video, 'hasTitle': true, 'hasPage': true,"num":data.detail.num_video,"id":"video_new_of_channel_"~data.detail.id} %}*/
/*             </section>*/
/*         {% endif %}*/
/* */
/*         {% if data.most_view_video.content|length >0 %}*/
/*             <section class="blk blkBdTop blkListVideosVer">*/
/*                 {% include "@app/views/partials/videoSmallBox.twig" with { 'hideChannel': true, 'data': data.most_view_video, 'hasTitle': true, 'hasPage': true,*/
/*                 "num": data.detail.num_video ,"id":"video_most_view_of_channel_"~data.detail.id} %}*/
/*             </section>*/
/*         {% endif %}*/
/* */
/*     </div>*/
/* */
/*     {% if trackingContent %}*/
/*         {% include '@app/views/partials/trackingView.twig' with {'trackingContent': trackingContent} %}*/
/*     {% endif %}*/
/* </main>*/
