<?php

/* listPlaylist.twig */
class __TwigTemplate_c5f512f5c21ba7b670e708bff343ae665599d0d0f72d9e7fb62f5d09013ff269 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["HeaderWidget"]) ? $context["HeaderWidget"] : null), "widget", array(0 => array("type" => "sub", "title" => "Playlist")), "method");
        echo "
";
        // line 2
        $this->env->getExtension('WapExtension')->addUses("wapsite/assets/ReactAsset");
        echo "
";
        // line 3
        $this->env->getExtension('WapExtension')->registerAsset($context, "react_asset");
        echo "

<main>
    <section class=\"blk blkPadBoth blkListVideosVer \">

        ";
        // line 8
        $this->loadTemplate("@app/views/account/playlistBox.twig", "listPlaylist.twig", 8)->display(array_merge($context, array("data" => $this->getAttribute((isset($context["responseCode"]) ? $context["responseCode"] : null), "data", array()), "id" => (isset($context["idPlaylist"]) ? $context["idPlaylist"] : null))));
        // line 9
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["responseCode"]) ? $context["responseCode"] : null), "data", array()), "content", array())) == 0)) {
            // line 10
            echo "            <div class=\"ctn\">
                <p>";
            // line 11
            echo twig_escape_filter($this->env, Yii::t("wap", "Không có dữ liệu"), "html", null, true);
            echo "</p>
            </div>
        ";
        }
        // line 14
        echo "        <div id=\"container\">
        </div>  
        <input type=\"hidden\" id=\"homeItemLimit\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
        echo "\">
        <input type=\"hidden\" id=\"getMoreOffset\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "params", array()), "app.home.limit", array(), "array"), "html", null, true);
        echo "\">
        <input type=\"hidden\" id=\"load-more-id\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\"/>
        <input type=\"hidden\" id=\"alias\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["idPlaylist"]) ? $context["idPlaylist"] : null), "html", null, true);
        echo "\"/>
        <input type=\"hidden\" id=\"type\" value=\"PLAYLIST\"/>
    </section>
</main>            ";
    }

    public function getTemplateName()
    {
        return "listPlaylist.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 19,  61 => 18,  57 => 17,  53 => 16,  49 => 14,  43 => 11,  40 => 10,  37 => 9,  35 => 8,  27 => 3,  23 => 2,  19 => 1,);
    }
}
/* {{ HeaderWidget.widget({'type': 'sub', 'title': 'Playlist'}) | raw }}*/
/* {{ use('wapsite/assets/ReactAsset') }}*/
/* {{ register_react_asset() }}*/
/* */
/* <main>*/
/*     <section class="blk blkPadBoth blkListVideosVer ">*/
/* */
/*         {% include "@app/views/account/playlistBox.twig" with {'data': responseCode.data,"id":idPlaylist}  %}*/
/*         {% if responseCode.data.content|length ==0   %}*/
/*             <div class="ctn">*/
/*                 <p>{{ t('wap', 'Không có dữ liệu') }}</p>*/
/*             </div>*/
/*         {% endif %}*/
/*         <div id="container">*/
/*         </div>  */
/*         <input type="hidden" id="homeItemLimit" value="{{ app.params['app.home.limit'] }}">*/
/*         <input type="hidden" id="getMoreOffset" value="{{ app.params['app.home.limit'] }}">*/
/*         <input type="hidden" id="load-more-id" value="{{id}}"/>*/
/*         <input type="hidden" id="alias" value="{{idPlaylist}}"/>*/
/*         <input type="hidden" id="type" value="PLAYLIST"/>*/
/*     </section>*/
/* </main>            */
