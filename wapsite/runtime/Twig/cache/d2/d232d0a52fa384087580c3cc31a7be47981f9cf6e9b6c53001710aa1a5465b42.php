<?php

/* @app/views/partials/videoBox.twig */
class __TwigTemplate_88b3370a6f68a24742f549e9f073558b02dd958d14e501688b8a3109a037d5c6 extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $context["cc"] = 0;
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 4
            echo "    ";
            $context["cc"] = ((isset($context["cc"]) ? $context["cc"] : null) + 1);
            // line 5
            echo "    <article class=\"videoItem load-bar\" id=\"video-item-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\" data-content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\">
        ";
            // line 6
            if (($this->getAttribute($context["video"], "price_play", array()) == 0)) {
                // line 7
                echo "            <div class=\"attach-img\">
                <img src=\"/images/freeVideo.png\">
            </div>
        ";
            }
            // line 11
            echo "        <div class=\"pthld\">
            <div class=\"playback\"><span id=\"playback-";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\" style=\"width: 0%\"></span></div>
            <span class=\"timing\">";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "duration", array()), "html", null, true);
            echo "</span>
            <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\">
                <img data-display=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "animationImage", array()), "html", null, true);
            echo "\" data-src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "coverImage", array()), "html", null, true);
            echo "\"  ";
            if (((isset($context["cc"]) ? $context["cc"] : null) <= 3)) {
                echo " src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "coverImage", array()), "html", null, true);
                echo "\"  class=\"animated-image\" ";
            } else {
                echo " class=\"lazy animated-image\" src=\"/images/16x9.gif\" ";
            }
            echo "    onError=\"this.onerror=null;this.src='/images/16x9.gif'\" alt=\"\">
            </a>
        </div>

        <div class=\"ctn\">
            <div class=\"avatar\">
                <a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-detail", array("id" => $this->getAttribute($context["video"], "userId", array()))), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "userName", array()), "html", null, true);
            echo "\">
                    <img data-src=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "userAvatarImage", array()), "html", null, true);
            echo "\" ";
            if (((isset($context["cc"]) ? $context["cc"] : null) <= 3)) {
                echo " src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "userAvatarImage", array()), "html", null, true);
                echo "\" ";
            } else {
                echo " src=\"/images/avatar.gif\" class=\"lazy\" ";
            }
            echo "  onError=\"this.onerror=null;this.src='/images/avatar.gif'\" alt=\"\">
                </a>
            </div>

            <h6 class=\"title\">
                <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\" name=\"urlVideo\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "name", array()), "html", null, true);
            echo "</a>
            </h6>

            <p class=\"txtInfo\">
                <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("channel/get-detail", array("id" => $this->getAttribute($context["video"], "userId", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "userName", array()), "html", null, true);
            echo "</a>
                <i class=\"dot\"></i>
                <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\">
                    ";
            // line 34
            echo twig_escape_filter($this->env, Yii::t("wap", "{count} lượt xem", array("count" => $this->getAttribute($context["video"], "play_times", array()))), "html", null, true);
            echo "
                </a>
                <i class=\"dot\"></i>
                <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\">
                    ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "publishedTime", array()), "html", null, true);
            echo "
                </a>
            </p>

            <a class=\"icoMore optMnBtn\" data-target=\"#videoOpt\" data-id=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["video"], "id", array()), "html", null, true);
            echo "\"
               data-url=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("video/detail", array("id" => $this->getAttribute($context["video"], "id", array()), "slug" => $this->getAttribute($context["video"], "slug", array()), "click_source" => $this->getAttribute($context["video"], "click_source", array()), "click_medium" => $this->getAttribute($context["video"], "click_medium", array()))), "html", null, true);
            echo "\"
               data-opt=\"clearAll\">
                <svg class=\"ico\">
                    <use xlink:href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('WapExtension')->url("images/defs.svg#ico-more"), "html", null, true);
            echo "\"></use>
                </svg>
            </a>
        </div>
    </article>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "@app/views/partials/videoBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 46,  143 => 43,  139 => 42,  132 => 38,  128 => 37,  122 => 34,  118 => 33,  111 => 31,  102 => 27,  86 => 22,  80 => 21,  61 => 15,  57 => 14,  53 => 13,  49 => 12,  46 => 11,  40 => 7,  38 => 6,  31 => 5,  28 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* */
/* {% set cc=0 %}*/
/* {% for video in data.content %}*/
/*     {% set cc= cc+1 %}*/
/*     <article class="videoItem load-bar" id="video-item-{{ video.id }}" data-content="{{ video.id }}">*/
/*         {% if(video.price_play == 0) %}*/
/*             <div class="attach-img">*/
/*                 <img src="/images/freeVideo.png">*/
/*             </div>*/
/*         {% endif %}*/
/*         <div class="pthld">*/
/*             <div class="playback"><span id="playback-{{ video.id }}" style="width: 0%"></span></div>*/
/*             <span class="timing">{{ video.duration }}</span>*/
/*             <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">*/
/*                 <img data-display="{{ video.animationImage }}" data-src="{{ video.coverImage }}"  {% if cc <= 3 %} src="{{ video.coverImage }}"  class="animated-image" {% else %} class="lazy animated-image" src="/images/16x9.gif" {% endif %}    onError="this.onerror=null;this.src='/images/16x9.gif'" alt="">*/
/*             </a>*/
/*         </div>*/
/* */
/*         <div class="ctn">*/
/*             <div class="avatar">*/
/*                 <a href="{{ url('channel/get-detail',{'id':video.userId }) }}" title="{{ video.userName }}">*/
/*                     <img data-src="{{ video.userAvatarImage }}" {% if cc <= 3 %} src="{{ video.userAvatarImage }}" {% else %} src="/images/avatar.gif" class="lazy" {% endif %}  onError="this.onerror=null;this.src='/images/avatar.gif'" alt="">*/
/*                 </a>*/
/*             </div>*/
/* */
/*             <h6 class="title">*/
/*                 <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}" name="urlVideo">{{ video.name }}</a>*/
/*             </h6>*/
/* */
/*             <p class="txtInfo">*/
/*                 <a href="{{ url('channel/get-detail',{'id':video.userId }) }}">{{ video.userName }}</a>*/
/*                 <i class="dot"></i>*/
/*                 <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">*/
/*                     {{ t('wap','{count} lượt xem', {'count': video.play_times}) }}*/
/*                 </a>*/
/*                 <i class="dot"></i>*/
/*                 <a href="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}">*/
/*                     {{ video.publishedTime }}*/
/*                 </a>*/
/*             </p>*/
/* */
/*             <a class="icoMore optMnBtn" data-target="#videoOpt" data-id="{{ video.id }}"*/
/*                data-url="{{ url('video/detail',{'id':video.id, 'slug':video.slug, 'click_source': video.click_source, 'click_medium': video.click_medium}) }}"*/
/*                data-opt="clearAll">*/
/*                 <svg class="ico">*/
/*                     <use xlink:href="{{ url('images/defs.svg#ico-more') }}"></use>*/
/*                 </svg>*/
/*             </a>*/
/*         </div>*/
/*     </article>*/
/* {% endfor %}*/
