<?php

/* @app/views/partials/popupMessage.twig */
class __TwigTemplate_f55445cf26a1d1d68677d5906ce6fdf0ed67d5b6d68658dbebeee1b93c5577ba extends yii\twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal modalPopup\" id=\"popupMessage\" style=\"display: none\">
    <div class=\"ctn\">
        <h5> ";
        // line 3
        echo twig_escape_filter($this->env, Yii::t("wap", "Thông báo"), "html", null, true);
        echo " </h5>
        <p class=\"clrGrey\"></p>
        <div class=\"action alRight\">
            <a class=\"close\" href=\"javascript:void(0)\">";
        // line 6
        echo twig_escape_filter($this->env, Yii::t("wap", "Đóng"), "html", null, true);
        echo "</a>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@app/views/partials/popupMessage.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  23 => 3,  19 => 1,);
    }
}
/* <div class="modal modalPopup" id="popupMessage" style="display: none">*/
/*     <div class="ctn">*/
/*         <h5> {{ t('wap', 'Thông báo') }} </h5>*/
/*         <p class="clrGrey"></p>*/
/*         <div class="action alRight">*/
/*             <a class="close" href="javascript:void(0)">{{ t('wap', 'Đóng') }}</a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
