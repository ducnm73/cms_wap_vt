<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 7/20/2016
 * Time: 10:57 AM
 */
return [
    '' => 'default/index',
    '/' => 'default/theo-doi',
    '/<id:\d+>' => 'default/index-promotion',

    // khanhdq
    'default/get-more-content' => 'default/load-more',
    'theo-doi' => 'default/follow-channel',
    'danh-sach-theo-doi' => 'default/list-follow-channel',
    'user/<id:\d+>' => 'channel/get-user-detail',
    'user/update/<id:\d+>' => 'channel/user-update',
    'channel/<id:\d+>' => 'channel/get-detail',
    'thinh-hanh' => 'default/trending',
    '/<id:\d+>' => 'default/index-promotion',
    // Nhom routing Video
    'upload-media' => 'account/upload-media',
    '/video' => 'default/index',
    '/video/<playlist_id:\d+>/<id:\d+>/<slug>' => 'video/detail',
    '/video/<id:\d+>/<slug>' => 'video/detail',
    '/video/<id:\d+>' => 'video/detail',
    '/nhac' => 'video/home-music',
    // Xem them noi dung tung box
    '/xem-them/<id:\w+>/<slug>' => 'default/load-more',
    '/xem-them/<id:\w+>' => 'default/load-more',
    '/xem-them' => 'default/index',
    '/danh-sach-thanh-vien' => 'account/get-list-member',
    '/goi-cuoc' => 'default/list-package',
    '/goi-cuoc-kenh-ban' => 'default/list-package-distribution',
    '/thanh-vien/<id:\d+>' => 'account/view-profile',
    '/trang-thanh-vien/<id:\d+>' => 'account/view-profile',
    '/trang-thanh-vien' => 'account/get-list-member',
    '/thanh-vien' => 'account/get-list-member',
    '/avatar' => 'account/upload-avatar',
    '/ho-tro' => 'default/support',
    '/de-xuat' => '/default/recommend',
    '/gioi-thieu-dich-vu' => 'default/introduction',
    '/chinh-sach-rieng-tu' => 'default/privacy',
    '/dieu-khoan-su-dung' => 'default/term-condition',
    '/quy-che-hoat-dong' => 'default/rule',
    '/lien-he' => 'default/contact',
    '/timkiem' => 'default/search',
    '/ld' => 'landing-page/index',
    //tuonglv
    'playlist/create' => 'playlist/create',
    'doi-mat-khau' => 'auth/change-password',
    'video/toggle-watch-later' => 'video/toggle-watch-later',
    'danh-sach-playlist' => 'account/list-playlist',
    'ca-nhan' => '/account/detail',
    'thong-bao' => '/account/get-notification',
    '/video-cua-toi' => '/default/my-video',
    '/video-xem-sau' => '/default/video-watch-later',
    '/video-da-xem' => '/default/video-history',
    '/chon-noi-dung' => 'account/get-follow-content',
    '/lien-ket-tai-khoan' => 'account/map-account',
    '/channel/update/<id:\d+>' => '/channel/update',
    '/channel/add' => '/channel/add',
    '/chi-tiet-playlist/<id:\w+>' => 'default/get-video-playlist',
    'trung-thuong-iphone-x' => 'promotion/index',
    '/playlist/xem-toan-bo-playlist/<id:\d+>' => 'playlist/detail',
    '/playlist/chi-tiet-playlist/<id:\d+>' => 'playlist/get-video-in-playlist',
    '/tet' => 'default/tet-holiday',
    [
        'pattern' => 'tet1',
        'route' => 'video/detail',
        'defaults' => ['id' => '1397998', 'utm_source' => 'GA1', 'popup' => 1],
    ],
    [
        'pattern' => 'hot',
        'route' => 'video/ad',
        'defaults' => ['id' => '1854519', 'package_id' => '1', 'utm_source' => 'GA88', 'popup' => 1],
    ],
    [
        'pattern' => 'phim',
        'route' => 'video/detail',
        'defaults' => ['id' => '1393888', 'utm_source' => 'GA1', 'popup' => 1],
    ],
    '/ad/<id:\d+>/<package_id:\d+>' => 'video/ad',
    '/lien-ket' => 'account/check-link-social',
    '/clip-hot-nhat' => 'distribution/index',
    '/Worldcup' => 'default/world-cup',
    '/wcup' => 'default/world-cup',
    '/world-cup' => 'default/world-cup',
    '/doanh-thu' => 'account/report-upload',
    [
        'pattern' => '/chuyen-muc/<id:\d+>/<slug>',
        'route' => 'default/get-video-category',
        'defaults' => ['slug' => 'NA'],
    ],
    'activate'=>'tv/activate',
    'ad-result/<error_code:\d+>'=>'video/after-register',
    '/chinh-sach-dich-vu' => 'default/service-policy',
    '/huong-dan-dich-vu' => 'default/service-instruction',
    'video-free' => 'default/video-free',
    'video-paid' => 'default/video-paid',
    'binh-luan' => 'default/list-comment-user'
];
