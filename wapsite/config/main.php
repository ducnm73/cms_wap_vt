<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        require(__DIR__ . '/../../common/config/params-local.php'),
        require(__DIR__ . '/../../common/config/pools.php'),
        require(__DIR__ . '/params.php')
);

$config = [
    'id' => 'app-wap',
    'basePath' => dirname(__DIR__),
    'language'=>'en',
    //'sourceLanguage'=>'lo',
    'bootstrap' => [
        'log',
        'wapsite\components\MyclipWapRoute'

    ],
    'controllerNamespace' => 'wapsite\controllers',
    'layout' => 'main.twig',
    'components' => [
//        'assetsAutoCompress' =>
//        [
//            'class'                         => '\skeeks\yii2\assetsAuto\AssetsAutoCompressComponent',
//            'enabled'                       => false,
//
//            'readFileTimeout'               => 3,           //Time in seconds for reading each asset file
//
//            'jsCompress'                    => false,        //Enable minification js in html code
//            'jsCompressFlaggedComments'     => false,        //Cut comments during processing js
//
//            'cssCompress'                   => false,        //Enable minification css in html code
//
//            'cssFileCompile'                => false,        //Turning association css files
//            'cssFileRemouteCompile'         => false,       //Trying to get css files to which the specified path as the remote file, skchat him to her.
//            'cssFileCompress'               => true,        //Enable compression and processing before being stored in the css file
//            'cssFileBottom'                 => false,       //Moving down the page css files
//            'cssFileBottomLoadOnJs'         => false,       //Transfer css file down the page and uploading them using js
//
//            'jsFileCompile'                 => true,        //Turning association js files
//            'jsFileRemouteCompile'          => false,       //Trying to get a js files to which the specified path as the remote file, skchat him to her.
//            'jsFileCompress'                => true,        //Enable compression and processing js before saving a file
//            'jsFileCompressFlaggedComments' => true,        //Cut comments during processing js
//
//            'htmlCompress'                  => false,        //Enable compression html
//            'noIncludeJsFilesOnPjax'        => false,        //Do not connect the js files when all pjax requests
//            'htmlCompressOptions'           =>              //options for compressing output result
//                [
//                    'extra' => false,        //use more compact algorithm
//                    'no-comments' => true   //cut all the html comments
//                ],
//        ],
//        'i18n' => [
//            'translations' => [
//                'backend*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@common/messages',
//                    'sourceLanguage' => 'en-US',
////                    'fileMap' => [
////                        'en' => 'en.php',
////                    ],
//                ],
//                'frontend*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@common/messages',
//                    'sourceLanguage' => 'en-US',
////                    'fileMap' => [
////                        'vi' => 'vi.php',
////                    ],
//                ],
//                'vi*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@common/messages',
//                    'sourceLanguage' => 'en-US',
////                    'fileMap' => [
////                        'vi' => 'vi.php',
////                    ],
//                ],
//            ],
//        ],
        'user' => [
            'identityClass' => 'wap\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logFile' => '@logs/wapsite/error.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logFile' => '@logs/wapsite/warning.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => '@logs/wapsite/info.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'logFile' => '@logs/wapsite/queries.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['promotion'],
                    'levels' => ['info'],
                    'logFile' => '@logs/wapsite/promotion.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'default/error',
        ],
        'session' => [
            'name' => 'myvideo-frontend',
            'class' => 'yii\redis\Session',
            'redis' => 'redis' // id of the connection application component
        ],
        'cache' => [
            'class' => 'common\helpers\MobiTVRedisCache',
            'redis' => 'redis' // id of the connection application component
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                    ],
                    'functions' => array(
                        't' => 'Yii::t',
                    ),
                    'globals' => [
                        'html' => '\yii\helpers\Html',
                        'Url' => '\yii\helpers\Url',
                        'Alert' => 'wapsite\widgets\Alert',
                        'HeaderWidget' => 'wapsite\components\common\HeaderWidget',
                        'FooterWidget' => 'wapsite\components\common\FooterWidget',
                        'SubHeaderWidget' => 'wapsite\components\common\SubHeaderWidget',
                        'TrackingWidget' => 'wapsite\components\common\TrackingWidget',
                    ],
                    'uses' => ['yii\bootstrap'],
                    'extensions' => [
                            ['yii2-twig', new \wapsite\components\WapExtension()]
                    ],
                ],
            // ...
            ],
        ],
    ],
    'params' => $params,
];

// Cau hinh route
$config['defaultRoute'] = 'default/index';

$config['components']['urlManager'] = [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    // chi nhung rule trong list moi dc phep truy cap
    'enableStrictParsing' => false,
    'rules' => require(__DIR__ . '/routes.php'),
];
$config['components']['meta'] = [
    'class' => 'wapsite\components\VtMeta',
];


return $config;
