<?php
/**
 * Created by PhpStorm.
 * User: Tuonglv
 * Date: 26/09/2017
 * Time: 10:01 AM
 */

use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$shortcode = '1583';
?>

<header class="transparent">
    <div class="ctn">
        <div class="lMn"><a href="javascript:window.history.back();"><svg class="ico clrWhite"><use xlink:href="/images/defs.svg#ico-back"></use></svg></a></div>
    </div>
</header>

<div class="ctn alCenter">
    <h1 class="logo">
        <a href="/">
            <img src="/images/logo.png">
        </a>
    </h1>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableClientValidation' => false,
        'enableClientScript' => false,
        'errorCssClass' => 'error-class',
        'action' => ['auth/login'],
        'fieldConfig' => [
            'template' => "{input}",
        ],
    ]); ?>

    <h4><?= yii::t('wap','Đăng nhập') ?></h4>
    <?= $form->errorSummary($model, ['header' => '', 'class' => 'txt-warning']); ?>
    <div class="form-group">
        <input class="ipt iptLg" placeholder="<?= yii::t('wap','Nhập số điện thoại') ?>" name="LoginForm[username]">
    </div>
    <div class="form-group">
        <input class="ipt iptLg" placeholder="<?= yii::t('wap','Nhập mật khẩu') ?>" name="LoginForm[password]" type="password">
    </div>

    <?php if ($countLoginFail > $configCaptchaShow): ?>

        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
            'options' => ['placeholder' => yii::t('wap','Mã xác nhận'), 'class' => 'ipt', 'autocomplete' => "off"],
            'captchaAction' => 'auth/captcha',
            'template' => '
                    <div class="form-group captcha">
						{input}
                        <div class="code">
                            <span class="btnRefreshCaptcha" onclick="refreshCaptcha()">
                                {image}
                                <i class="fa fa-repeat" ></i>
                            </span>
                        </div>
                    </div>
                ',
        ]) ?>

    <?php endif; ?>
    <div class="form-group">
        <button class="btn btnLg boxSd"><?= yii::t('wap','Đăng nhập') ?></button>
    </div>
    <p class="forgot">
	<a href="javascript:void(0)" class="modalBtn btnForgot replace-href" data-number="<?= $shortcode ?>" data-body="PW" data-target="#forgot-password"><?= yii::t('wap','Lấy mật khẩu') ?></a></p>
    <?php ActiveForm::end(); ?>
    <div class="overlay"></div>
</div>

<!-- POP UP -->
<div class="modal modalPopup" id="forgot-password">
    <div class="ctn">
        <h4><?= yii::t('wap','Lấy mật khẩu') ?></h4>
        <p> <?= yii::t('wap','Để lấy mật khẩu đăng nhập, quý khách vui lòng soạn') ?> <b>P</b> <?= yii::t('wap','gửi') ?> <b><?= $shortcode ?></b>. <?= yii::t('wap','Trân trọng') ?>!</p>
        <div class="action alRight">
            <a class="close" href="javascript:void(0)"><?= yii::t('wap','Đóng') ?></a>
        </div>
    </div>
</div>
<div class="modal modalPopup" id="register-account">
    <div class="ctn">
        <h4><?= yii::t('wap','Đăng ký tài khoản') ?></h4>
        <p> <?= yii::t('wap','Để đăng ký tài khoản, quý khách vui lòng soạn') ?> <b>MK</b> <?= yii::t('wap','gửi') ?> <b><?= $shortcode ?></b>. <?= yii::t('wap','Trân trọng') ?>!</p>
        <div class="action alRight">
            <a class="close" href="javascript:void(0)"><?= yii::t('wap','Đóng') ?></a>
        </div>
    </div>
</div>

<div class="modal modalPopup" id="3g4g-login">
    <div class="ctn">
        <h4>3G/4G</h4>
        <p><?= yii::t('wap','Mời bạn bật chế độ dữ liệu di động trên máy để tự động đăng nhập') ?></p>
        <div class="action alRight">
            <a class="close" href="javascript:void(0)"><?= yii::t('wap','Đóng') ?></a>
        </div>
    </div>
</div>