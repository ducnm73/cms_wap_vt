<div class="container">
    <div class="not-found">
        <div class="line-top"></div>
        <p>Oooh... <?= Yii::t('wap', 'rất tiếc') ?>  !</p>
        <h1><?php echo $statusCode ?></h1>
        <p><?= Yii::t('wap', 'Trang này không tồn tại') ?>.</p>
        <p><?= Yii::t('wap', 'Vui lòng quay lại') ?> <a href="/"><?=Yii::t('wap','trang chủ')?></a></p>
        <p><br><br><br><br><br></p>
    </div>
</div>
