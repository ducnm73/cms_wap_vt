/**
 * User: KhanhDQ
 * Date: 9/29/2017
 * Time: 09:00 AM
 */


//PhuMX trace KPI
var isPlaying = false;
var pauseTimes = 0;
var seekTimes = 0;
var waitTimes = 0;
var bufferOver3s = 0;

var beforeTime = 0;
var currentTime = 0;

var durationWatching = 0;
var durationBuffering = 0;

var needSendTrace = true;
var firstTimeTrace = true;

var tokenTrace = "GUEST";
var rtimeResizeend;
var timeoutResizeend = false;
var deltaResizeend = 200;
var initdone = false;

var needShowPopupMonthPackage = true;
var waitNextVideo;
var modalNext;

function resizeend() {
    if (new Date() - rtimeResizeend < deltaResizeend) {
        setTimeout(resizeend, deltaResizeend);
    } else {
        //set width, height
        displayVideoResponsive()
    }
}

function displayVideoResponsive() {
    var winWidth = $(window).width();
    var winHeight = $(window).height();
    if (winWidth >= 360) {
        $('.youtube .vjs-time-divider').css('display', 'block');
        $('.youtube .vjs-duration').css('display', 'block');
    } else {
        $('.youtube .vjs-time-divider').css('display', 'none');
        $('.youtube .vjs-duration').css('display', 'none');
    }
}

window.addEventListener("orientationchange", function () {
    rtimeResizeend = new Date();
    if (timeoutResizeend === false) {
        timeoutResizeend = true;
        setTimeout(resizeend, deltaResizeend);
    }
}, false);

window.addEventListener("resize", function () {
    rtimeResizeend = new Date();
    if (timeoutResizeend === false) {
        timeoutResizeend = true;
        setTimeout(resizeend, deltaResizeend);
    }
}, false);


var Video = function () {

    var isFirstLoad = true;
    var lockLoadingComment = false;
    var isAllData = false;
    var scope = {statusLike: null, statusDislike: null, status: null}
    var lockToogleLike = false;
    var lockFollowVideo = false;
    var lockLikeComment = false;
    var player = null;

    toastr.options.showDuration = 1000;
    toastr.options.timeOut = 2000;


    function loadComment(offset) {
        if (isAllData == true) {
            return;
        }
        if (!$("#commentUrl").val()) return;
        $("#loading-comment").show();

        $.ajax({
            method: "GET",
            url: $('#commentUrl').val(),
            data: {
                'type': $('#commentType').val(),
                'content_id': $('#contentId').val(),
                'offset': offset,
                'limit': $("#commentLimit").val()
            }
        }).done(function (data) {
            if (data.length > 0) {
                $("#loading-comment").hide();
                $("#commentContent").append(data);
                $("#commentOffset").val(parseInt($("#commentOffset").val()) + parseInt($("#commentLimit").val()));
            } else {
                if (isFirstLoad == true) {
                    $('#boxNoComment').show();
                }
                isAllData = true;
            }

            if (document.getElementById('comment-empty')) {
                isAllData = true;
                lockLoadingComment = true;
            }

            lockLoadingComment = false;
            $("#loading-comment").hide();
            isFirstLoad = false;
        }).fail(function (jqXHR, textStatus) {
            $("#loading-comment").hide();
            isFirstLoad = false;
        });
    }

    function autoloadComment() {
        $(window).scroll(function (event) {
            if ((($(window).scrollTop() + $(window).height() + 20 >= $(document).height()) || $(window).height() == $(document).height()) && lockLoadingComment == false) {
                console.log('call');
                lockLoadingComment = true;
                loadComment($("#commentOffset").val());
            }
        });
        lockLoadingComment = true;
        loadComment($("#commentOffset").val());
    }

    function toggleLike() {
        $('.likeBtn').click(function () {
            if (lockToogleLike == false) {
                if (scope.statusLike == null) {
                    scope.statusLike = $(this).data('like');
                }
                lockToogleLike = true;
                processLikeVideo($(this).data('id'), 'like');
            }
        });
    }

    function toggleDislike() {
        $('.dislikeBtn').click(function () {
            if (lockToogleLike == false) {
                if (scope.statusDislike == null) {
                    scope.statusDislike = $(this).data('dislike');
                }
                lockToogleLike = true;
                processLikeVideo($(this).data('id'), 'dislike');
            }
        });
    }

    function processLikeVideo(videoId, type) {
        if (type == 'like') {
            if (scope.statusLike == 1) {
                scope.statusLike = 0;
            } else {
                scope.statusLike = 1;
            }
            scope.status = scope.statusLike;
            if (scope.statusDislike == null)
                scope.statusDislike = $('#linkDislike_' + videoId).data('dislike');
        } else {
            if (scope.statusDislike == -1) {
                scope.statusDislike = 0;
            } else {
                scope.statusDislike = -1;
            }
            scope.status = scope.statusDislike;
            if (scope.statusLike == null)
                scope.statusLike = $('#linkLike_' + videoId).data('like');
        }
        $.ajax({
            method: "POST",
            url: $('#toggleLikeUrl').val(),
            data: {
                'id': videoId,
                'status': scope.status,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            }
        }).done(function (obj) {
            processIconLike(videoId, type, obj);
        }).fail(function (jqXHR, textStatus) {
            lockToogleLike = false;
        });
    }

    function processIconLike(videoId, type, obj) {
        if (obj['responseCode'] == '200') {
            var currentLike = parseInt($('#numLike_' + videoId).html());
            var currentDislike = parseInt($('#numDislike_' + videoId).html());
            if (type == 'like') {
                if (scope.statusLike == 1) {
                    $('#numLike_' + videoId).html(currentLike + 1);
                    $('#linkLike_' + videoId + ' svg.icoLiked').addClass("active");
                    $('#linkLike_' + videoId + ' svg.icoLike').removeClass("active");
                } else {
                    if (currentLike >= 1) {
                        $('#numLike_' + videoId).html(currentLike - 1);
                    }
                    $('#linkDislike_' + videoId + ' svg.icoDisliked').removeClass("active");
                    $('#linkDislike_' + videoId + ' svg.icoDislike').addClass("active");
                    $('#linkLike_' + videoId + ' svg.icoLiked').removeClass("active");
                    $('#linkLike_' + videoId + ' svg.icoLike').addClass("active");
                }
                $('#linkLike_' + videoId).attr('data-like', scope.statusLike);
                // remove dislike if exist 
                if (scope.statusDislike == -1) {
                    if (currentDislike >= 1) {
                        $('#numDislike_' + videoId).html(currentDislike - 1);
                    }

                    scope.statusDislike = 0;
                    $('#linkDislike_' + videoId + ' svg.icoDisliked').removeClass("active");
                    $('#linkDislike_' + videoId + ' svg.icoDislike').addClass("active");
                }
            } else {
                if (scope.statusDislike == -1) {
                    $('#numDislike_' + videoId).html(currentDislike + 1);
                    $('#linkDislike_' + videoId + ' svg.icoDisliked').addClass("active");
                    $('#linkDislike_' + videoId + ' svg.icoDislike').removeClass("active");

                    $('#linkLike_' + videoId + ' svg.icoLiked').removeClass("active");
                    $('#linkLike_' + videoId + ' svg.icoLike').addClass("active");
                } else if (scope.statusDislike != -1) {
                    if (currentDislike >= 1) {
                        $('#numDislike_' + videoId).html(currentDislike - 1);
                    }
                }
                $('#linkDislike_' + videoId).attr('data-like', scope.statusLike);
                // remove like if exist 
                if (scope.statusLike == 1) {
                    if (currentLike >= 1) {
                        $('#numLike_' + videoId).html(currentLike - 1);
                    }
                    scope.statusLike = 0;
                    $('#linkLike_' + videoId + ' svg.icoLiked').removeClass("active");
                    $('#linkLike_' + videoId + ' svg.icoLike').addClass("active");
                }
            }
            lockToogleLike = false;
        } else if (obj['responseCode'] == '401' || obj['responseCode'] == '201') {
            window.location = '/auth/login';
        } else if (obj.responseCode == 444) {
            toastr.error(obj.message, {closeButton: true})
        }
    }

    function handleComment() {
        $('#inputComment').focus(function () {
            checkLogin();
            $("#one-time-comment").parent().hide();
            $('.btnCommentBox').show();
        });
        clearComment('#inputComment');
        $('.btnComment').click(function () {
            // var content = $.trim($("#inputComment").val());
            var content = $("#inputComment").val();
            //khong xu ly binh luan rong
            if (content.length == 0) {
                toastr.error(trans("Vui lòng nhập nội dung bình luận")+'', {closeButton: true})
                return;
            }
            if (content.length > 1000) {
                toastr.error(trans("Nội dung bình luận không được lớn hơn 1000 kí tự"), {closeButton: true})
                return;
            }
            postComment(0, content);

        })
    }

    function clearComment(element) {
        $(document).on('click', '.clearComment', function () {

            // if ($(element).val() == "") {
            //     $(this).parent().parent().hide();
            //     $("#inputComment").show();
            // }

            $(element).val('');

        });
    }

    function checkLogin() {
        if ($('#userId').val() == '') {
            window.location = '/auth/login';
        }
    }

    function handleRelpyComment() {
        $(document).on('click', '.btnReplyComment', function () {
            var commentId = $(this).data('id');
            $(".btnCommentBox").hide();
            $("#one-time-comment").parent().show();
            showReplyForm(commentId);
        });
    }

    function showReplyForm(commentId) {
        //alert(commentId + "---------" + currentLanguage);//1381


        $("#one-time-comment").remove();
        $("#cm_" + commentId + " > .ctn > .ctnhld_v1 > .boxReply").append('<div class="inputReply" id="one-time-comment"><textarea name="" id="inputComment_' + commentId + '" class="ipt iptLg mb10" placeholder="'+trans("Nhập bình luận...")+'"></textarea><div id="btnCommentBox_' + commentId + '" class="btnCommentBox right pb10 mt0">' +
            '<a href="javascript:void(0)" class="clearComment" key="Hủy">'+trans("Hủy")+'</a>' +
            '<a href="javascript:void(0)" class="btnComment" id="btnPostComment_' + commentId + '" data-type="VOD" data-parentid="' + commentId + '">'+trans("Bình luận")+'</a></div><div style="clear:both;"></div></div>');

        $('#inputComment_' + commentId).focus(function () {
            checkLogin();
		$('#btnCommentBox_' + commentId).show();
        var y = $(window).scrollTop();  //your current y position on the page
        $(window).scrollTop(y + 30);
		});
        $('#inputComment_' + commentId).focus();
        var content = $('#inputComment_' + commentId).val();
        $('.clearComment').bind('click', clearComment('#inputComment_' + commentId));
        $('#btnPostComment_' + commentId).bind('click', clickReply(commentId));
    }

    function clickReply(commentId) {
        $('#btnPostComment_' + commentId).click(function () {
            var content = $.trim($("#inputComment_" + commentId).val());
            if (content.length > 1000) {
                toastr.error(trans("Nội dung bình luận không được lớn hơn 1000 kí tự")+'...', {closeButton: true})
                return;
            }
            postComment(commentId, content);
        });
    }

    function validateComment(content) {
        if (content.length <= 0) {
            toastr.error(trans("Vui lòng nhập nội dung bình luận")+'...', {closeButton: true})
            return false;
        }
        return true;
    }

    function postComment(parentId, content) {
        if (validateComment(content) == false) return;

        if (parentId == 0) {
            $(".btnComment[data-parentid=" + parentId + "]").parent().hide();
        } else {
            $(".btnComment[data-parentid=" + parentId + "]").parent().parent().hide();
        }


        var commentType = $('#commentType').val();
        var contentId = $('#contentId').val();
        $.ajax({
            method: "POST",
            url: $('#postCommentUrl').val(),
            data: {
                'parent_id': parentId,
                'content_id': contentId,
                'type': commentType,
                'comment': content,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (data) {
            try {
                var objData = $.parseJSON(data);
                if (objData.responseCode == 401) {
                    window.location = '/auth/login'
                } else {
                    toastr.error(objData.message, {closeButton: true})
                }
            } catch (err) {
                $("#content-one-time").val("");
                $("#content").val("");
                if (parentId > 0) {
                    $(data).insertAfter("#cm_" + parentId + " > .user > .ctnhld_v1 > .boxReply");
                    $("#inputComment_" + parentId).val("");
                    $("#inputComment_" + parentId).blur();
                } else {
                    $("#commentContent").prepend(data);
                    $("#inputComment").val("");
                    $("#inputComment").blur();
                }
                $('.ctnhld_v1 .title').addClass('comment-textarea-linhtvn');
                $("#blank_comment").hide();
            }
        }).fail(function (jqXHR, textStatus) {

        });
    }

    function viewMoreComment() {
        $(document).on('click', '.viewMoreComment', function () {
            $(this).parent().hide();
            $('.cm_group_' + $(this).data('id')).removeClass('hidden');
        });
    }

    function followVideo() {
        $('#followVideo').click(function () {

            var status = $("#is_follow").val();
            if (lockFollowVideo == false) {
                lockFollowVideo = true;
                $.ajax({
                    type: "POST",
                    url: '/account/follow-channel',
                    dataType: 'json',
                    data: {
                        'id': $(this).data('id'),
                        'status': (1 - status),
                        '_csrf': $('meta[name="csrf-token"]').attr("content")
                    }
                }).done(function (objData) {

                    if (objData['responseCode'] == '200') {
                        if (objData['data']['isFollow'] == true) {
                            //$('#followVideo').css('color', '#0081FF');
                            $("#is_follow").val(1);
                            $("#total_follow").text(objData['data']['followCount'] + trans("theo dõi"));
                            showValidateMsg(trans('Đăng kí theo dõi kênh thành công'));
                            $("#followVideo span").text(trans("Đã theo dõi"));

                            if (document.getElementById('section_detail') && !document.getElementById('rcmBlock')) {
                                $.ajax({
                                    type: "GET",
                                    url: '/channel/get-channel-recommend',
                                    data: {
                                        '_csrf': $('meta[name="csrf-token"]').attr("content")
                                    },
                                    success: function (data) {
                                        $(data).insertAfter('#section_detail');
                                        if ($('.rcmChnlsSlider').length) {
                                            $('.rcmChnlsSlider').owlCarousel({
                                                nav: false,
                                                dots: false,
                                                loop: false,
                                                responsive: {
                                                    0: {items: 1, autoWidth: true, margin: 12}
                                                }
                                            });
                                        }
                                    }
                                });

                            }

                        } else {
                            $("#followVideo span").text(trans("Theo dõi"));
                            //$('#followVideo').css('color', '#000000');
                            $('#followVideo').blur();
                            $("#is_follow").val(0);
                            $("#total_follow").text(objData['data']['followCount'] + trans("theo dõi"));
                            showValidateMsg(trans('Hủy theo dõi kênh thành công'));
                        }

                    } else if (objData['responseCode'] == '401') {
                        window.location = '/auth/login';
                    } else {
                        toastr.error(objData.message, {closeButton: true})
                    }
                    lockFollowVideo = false;

                }).fail(function (jqXHR, textStatus) {
                    lockFollowVideo = false;
                });
            }
        });
    }


    function clickLikeComment() {
        $(document).on('click', '.btnLikeComment', function () {
            if (lockLikeComment == false) {
                lockLikeComment = true;
                var commentId = $(this).data('commentid');
                var commentType = 1;
                var contentId = $('#contentId').val();
                likeComment(commentType, commentId, contentId);
            }
        });
    }
    function clickDisLikeComment() {
        $(document).on('click', '.btnDisLikeComment', function () {
            if (lockLikeComment == false) {
                lockLikeComment = true;
                var commentId = $(this).data('commentid');
                var commentType = 2;
                var contentId = $('#contentId').val();
                likeComment(commentType, commentId, contentId);
            }
        });
    }

    function likeComment(type, commentId, contentId) {
        $.ajax({
            method: "POST",
            url: $('#toggleLikeComment').val(),
            data: {
                'like': type,
                'comment_id': commentId,
                'content_id': contentId,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (objData) {

            if (objData['responseCode'] == 401) {
                window.location = '/auth/login';
            }
            if (objData['responseCode'] == 200) {
                if (type == 1) {
                    if($('#icoLikedComment_' + commentId).hasClass('fa-thumbs-up')){
                        $('#icoLikedComment_' + commentId).removeClass('fa-thumbs-up');
                        $('#icoLikedComment_' + commentId).addClass('fa-thumbs-o-up');
                    }else{
                        $('#icoLikedComment_' + commentId).addClass('fa-thumbs-up');
                        $('#icoLikedComment_' + commentId).removeClass('fa-thumbs-o-up');
                        $('#icoDisLikedComment_' + commentId).removeClass('fa-thumbs-down');
                        $('#icoDisLikedComment_' + commentId).addClass('fa-thumbs-o-down');
                    }

                }
                if (type == 2) {
                    if($('#icoDisLikedComment_' + commentId).hasClass('fa-thumbs-down')){
                        $('#icoDisLikedComment_' + commentId).removeClass('fa-thumbs-down');
                        $('#icoDisLikedComment_' + commentId).addClass('fa-thumbs-o-down');
                    }else {
                        $('#icoLikedComment_' + commentId).removeClass('fa-thumbs-up');
                        $('#icoLikedComment_' + commentId).addClass('fa-thumbs-o-up');
                        $('#icoDisLikedComment_' + commentId).addClass('fa-thumbs-down');
                        $('#icoDisLikedComment_' + commentId).removeClass('fa-thumbs-o-down');
                    }

                }
                var likeCommentNumber = objData['data']['like_count'];
                $('.likeCommentNumber_' + commentId).html(likeCommentNumber);
                var disLikeCommentNumber = objData['data']['dislike_count'];
                $('.disLikeCommentNumber_' + commentId).html(disLikeCommentNumber);
            }else{
                toastr.error(objData['message'], {closeButton: true});
            }

            lockLikeComment = false;
        }).fail(function (jqXHR, textStatus) {
            lockLikeComment = false;
            toastr.error(objData['message'], {closeButton: true});
        });
    }

    function videoPlayer() {
        if ($('#url').val() && !document.getElementById('countdown')) {
            initPlayer();
        }
    }

    function showTimer() {
        if (document.getElementById('showTimer')) {
            $('.counter').countdown({
                date: $('#showTimer').val(),
                onEnd: function () {
                    reloadPlayerAfterFinishCountdown();
                },
            });
        }
    }

    function reloadPlayerAfterFinishCountdown() {
        $.ajax({
            method: "GET",
            url: $('#idUrlGetStream').val(),
        }).done(function (objData) {
            if (objData.responseCode == 200) {
                $('#url').val(objData.data.urlStreaming);
                $('#countdown').hide();
                $('#playerBox').show();
                initPlayer();
            }
        }).fail(function (jqXHR, textStatus) {

        });
    }


    function initPlayer() {
        //-- START TRACE
        // DO init
        traceKPI();
        // trace interval 10s
        setInterval(function () {
            traceKPI()
        }, 10000);
        var completePosition = 0;
        var startBuffering = 0;

        var streamUrl = $('#url').val();

        var options = {
            html5: {
                hls: {
                    enableLowInitialPlaylist: true
                }
            },
            flash: {
                hls: {
                    enableLowInitialPlaylist: true
                }
            },

            controls: true,
            autoplay: true,
            preload: "auto",
            fluid: true,
            inactivityTimeout: 1000,
            controlBar: {
                playToggle: {},
                currentTimeDisplay: {},
                timeDivider: {},
                durationDisplay: {},
                remainingTimeDisplay: false,
                progressControl: {},
                volumePanel: {},
                captionsButton: false,
                subsCapsButton: false,
                subtitlesButton: false,
                playbackRateMenuButton: false,
                audioTrackButton: false,
                fullscreenToggle: {}
            }
        };


        player = videojs('videoBox', options);
        player.src({
            src: streamUrl,
            type: 'application/x-mpegURL',
        });
        player.poster($('#poster').val());
        var qLevels = [];

        player.qualityLevels().on('addqualitylevel', function (event) {
            var quality = event.qualityLevel;
            if (quality.height !== undefined && $.inArray(quality.height, qLevels) === -1) {
                quality.enabled = true;

                qLevels.push(quality.height);

                if (!$('.quality_ul').length) {
                    var h = '<div class="quality_setting vjs-menu-button vjs-menu-button-popup vjs-control vjs-button">' +
                        '<button class="vjs-menu-button vjs-menu-button-popup vjs-button" type="button" aria-live="polite" aria-disabled="false" title="Quality" aria-haspopup="true" aria-expanded="false">' +
                        '<span aria-hidden="true" class="vjs-icon-placeholder vjs-icon-cog"></span>' +
                        '<span class="vjs-control-text">Quality</span></button>' +
                        '<div class="vjs-menu"><ul class="quality_ul vjs-menu-content" role="menu"></ul></div></div>';

                    $(".vjs-fullscreen-control").before(h);
                } else {
                    $('.quality_ul').empty();
                }

                qLevels.sort(function (a, b) {
                    return b - a
                });
                qLevels.reverse();

                var j = 0;

                $.each(qLevels, function (i, val) {
                    $(".quality_ul").append('<li class="vjs-menu-item" tabindex="' + i + '" role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="false" bitrate="' + val +
                        '"><span class="vjs-menu-item-text">' + val + 'p</span></li>');
                    j = i;
                });

                $(".quality_ul").append('<li class="vjs-menu-item vjs-selected" tabindex="' + (j + 1) + '" role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="true" bitrate="auto">' +
                    '<span class="vjs-menu-item-text">Auto</span></li>');
            }
        });

        player.on('ready', function (event) {
            displayVideoResponsive();
        });

        // -- ADD EVENT TRACE
        player.on(['loadeddata'], function () {
            isPlaying = false;
            console.log("Loadeddata");
            //seek to position
            player.currentTime($("#playPosition").val());
        });

        player.on("canplaythrough", function () {
            if (!initdone) {
                player.currentTime($("#playPosition").val());
                initdone = true;
            }
        });

        player.on('firstplay', function () {
            console.log('firstplay');
        });

        player.on(['waiting'], function () {
            isPlaying = false;
            needSendTrace = true;
            waitTimes++;
            startBuffering = performance.now();
            console.log("Waitting|" + startBuffering);
        });
        player.on("seeking", function (e) {
            console.log("Seeking: " + player.currentTime());
            currentTime = player.currentTime();
            beforeTime = currentTime;
        });
        player.on("seeked", function (e) {
            needSendTrace = true;
            seekTimes++;
            currentTime = player.currentTime();
            beforeTime = currentTime;

            //calc buffer time
            stopBuffer = performance.now();
            if (startBuffering > 0) {
                durationBuffering += stopBuffer - startBuffering;
                if (stopBuffer - startBuffering >= 3000) {
                    bufferOver3s += 1;
                }
            }
            //reset buffer
            startBuffering = 0;
        });

        player.on('timeupdate', function () {
            needSendTrace = true;
            //console.log( player.currentTime() );
            if (isPlaying) {
                beforeTime = currentTime;
                currentTime = player.currentTime();
                if ((currentTime > beforeTime && (currentTime - beforeTime < 5))) {
                    durationWatching += (currentTime - beforeTime);
                }
            }
            // check 10s show popup suggest register package
            if (currentTime >= 10 && needShowPopupMonthPackage) {
                needShowPopupMonthPackage = false;
                //check has popup
                if (document.getElementById('popup-suggest-register-month-package')) {
                    // ajax get has need show popup?
                    $.ajax({
                        method: 'GET',
                        url: '/default/check-show-popup-world-cup',
                        dataType: 'json',
                        data: {
                            'video_id': $('#playId').val(),
                            '_csrf': $('meta[name="csrf-token"]').attr("content")
                        },
                    }).done(function (objData) {
                        if (objData.errorCode == 200) {
                            showPopup($("#videoOptWorldCup"));
                            needShowPopupMonthPackage = false;
                            $('#popup-suggest-register-month-package').remove();
                        }

                    }).fail(function (jqXHR, textStatus) {

                    });
                }
            }

            //console.log("Duration Watching = "+ durationWatching);
        });

        player.on(['pause'], function () {
            isPlaying = false;
            needSendTrace = true;
            currentTime = player.currentTime();
            beforeTime = currentTime;
            pauseTimes++;
            console.log("Pause");

            //calc buffer time
            stopBuffer = performance.now();
            if (startBuffering > 0) {
                durationBuffering += stopBuffer - startBuffering;
            }
            //reset buffer
            startBuffering = 0;
        });

        player.on('playing', function () {
            isPlaying = true;
            needSendTrace = true;
            console.log("Playing");

            //calc buffer time
            stopBuffer = performance.now();
            if (startBuffering > 0) {
                durationBuffering += stopBuffer - startBuffering;
                if (stopBuffer - startBuffering >= 3000) {
                    bufferOver3s += 1;
                }
            }
            //reset buffer
            startBuffering = 0;
            //console.log("Duration Buffering="+durationBuffering);
        });

        player.on('ended', function () {
            isPlaying = false;
            needSendTrace = true;
            completePosition = player.currentTime();
            $.ajax({
                url: $('#ajaxUrl').val(),
                type: 'post',
                data: 'id=' + $('#playId').val() + '&time=' + completePosition + '&type=' + $('#media_type').val(),
                success: function (string) {
                }
            });

            console.log("Ended");
        });

        //player.enableTouchActivity();
        player.on('touchend', function () {
            if (isPlaying) {
                player.pause();
            } else {
                player.play();
            }
        });

        player.ready(function () {
            //seek to position
            player.currentTime($("#playPosition").val());

            var pos = 0;
            setInterval(function () {
                if (player.currentTime() != 0 && player.currentTime() != pos && player.currentTime() != completePosition) {
                    pos = player.currentTime();
                    $.ajax({
                        url: $('#ajaxUrl').val(),
                        type: 'post',
                        data: 'id=' + $('#playId').val() + '&time=' + pos + '&type=' + $('#media_type').val(),
                        success: function (string) {
                        }
                    });
                }
            }, 10000);
            this.hotkeys({
                volumeStep: 0.1,
                seekStep: 5,
                enableModifiersForNumbers: false
            });
        });

        //ADD Button Next-Back
        var Button = videojs.getComponent('Button');
        var MyNextButton = videojs.extend(Button, {
            constructor: function () {
                Button.apply(this, arguments);
                this.addClass('player-next-onscreen');
                /* initialize your button */
            },
            handleClick: function () {
                /* do something on click */
                window.location = $("#nextVideo").val();

            }
        });

        videojs.registerComponent('MyNextButton', MyNextButton);
        player.getChild('BigPlayButton').addChild('MyNextButton', {tabIndex: 2});

        //Add Back button
        if (document.referrer != "" && document.referrer.indexOf("/video/") !== -1) {
            var MyBackButton = videojs.extend(Button, {
                constructor: function () {
                    Button.apply(this, arguments);
                    this.addClass('player-back-onscreen');
                },
                handleClick: function () {
                    if (document.referrer.indexOf("/video/") !== -1) {
                        window.history.back();
                    }
                }
            });

            videojs.registerComponent('MyBackButton', MyBackButton);
            player.getChild('BigPlayButton').addChild('MyBackButton', {});
        }


        /*
        player.muted = true;
         // muted is a boolean attribute, so, any value turns it on, using the name of the attribute is an often used convention
        player.setAttribute('muted', 'muted');
        player.autoplay = true;
        player.setAttribute('autoplay', 'autoplay');

        player.play();
        */
    }

    function eventClickResolution() {

        $("body").on("click", ".quality_ul li", function () {
            $(".quality_ul li").removeClass("vjs-selected");
            $(".quality_ul li").prop("aria-checked", "false");

            $(this).addClass("vjs-selected");
            $(this).prop("aria-checked", "true");

            var val = $(this).attr("bitrate");

            var qualityLevels = player.qualityLevels();

            for (var i = 0; i < qualityLevels.length; i++) {
                qualityLevels[i].enabled = (val == "auto" || (val != "auto" && qualityLevels[i].height == val));
            }
            $('.quality_setting .vjs-menu-button-popup').attr('aria-expanded', 'false');
            $('.quality_setting .vjs-menu').removeClass('vjs-lock-showing');
        });

        $("body").on("click", ".quality_setting .vjs-menu-button-popup", function () {
            var isShowed = $(this).attr('aria-expanded');
            if (typeof isShowed !== typeof undefined && isShowed !== false) {
                isShowed = !(isShowed.toUpperCase() === 'TRUE');
                $(this).attr('aria-expanded', isShowed);
                if (isShowed) {
                    $('.quality_setting .vjs-menu').addClass('vjs-lock-showing');
                } else {
                    $('.quality_setting .vjs-menu').removeClass('vjs-lock-showing');
                }
            }
        });
    }

    function autoPlayVideo() {

        var isAutoPlay = getCookie("isAutoPlay");
        if (isAutoPlay == "") {
            setCookie("isAutoPlay", 1, 7);
            isAutoPlay = 1;
        }
        if (isAutoPlay == 1) {
            $(".switch svg:first-child").addClass("active");
            $(".switch svg:last-child").removeClass("active");
            $("#autoPlay").val(1);
        } else {
            $(".switch svg:first-child").removeClass("active");
            $(".switch svg:last-child").addClass("active");
            $("#autoPlay").val(0);
        }

        $('.switch').click(function (e) {

            if ($('#autoPlay').val() == 1) {
                $('#autoPlay').val(0);
                setCookie("isAutoPlay", 0, 7);
                isAutoPlay = 0;
            } else {
                $('#autoPlay').val(1);
                setCookie("isAutoPlay", 1, 7);
                isAutoPlay = 1;
            }

            if (isAutoPlay == 1) {
                $(".switch svg:first-child").addClass("active");
                $(".switch svg:last-child").removeClass("active");
            } else {
                $(".switch svg:first-child").removeClass("active");
                $(".switch svg:last-child").addClass("active");
            }
        });
    }

    function callBackEndVideo() {
        if (player) {
            player.on('ended', function () {
                if ($('#autoPlay').val() == 1 && $('#nextVideo').val()) {
                    $(".vjs-big-play-button ").css("display", "none");
                    var el = videojs.createEl('ModalDialog', {
                        innerHTML: $('#modalAutoPlay').html()
                    })
                    modalNext = player.addChild('ModalDialog', {
                        'el': el
                    })
                    modalNext.open();
                    modelNext.on('modalclose', function() {
                        cancelNextVideo();
                    });
                    waitNextVideo = setInterval(function () {
                        window.location.href = $('#nextVideo').val();
                    }, 5000);
                }
            });
        }
    }

    function traceKPI() {
        if (needSendTrace) {
            if (firstTimeTrace) {
                firstTimeTrace = false;
                $.ajax({
                    method: "GET",
                    url: '/kpi/init',
                    data: {
                        'video_id': $('#playId').val(),
                        'play_url': $('#url').val(),
                        'os_type': 'WAPSITE',
                        '_csrf': $('meta[name="csrf-token"]').attr("content")
                    },
                }).done(function (objData) {
                    if (objData.responseCode == '200') {
                        tokenTrace = objData.token
                    }
                }).fail(function (jqXHR, textStatus) {
                });
            } else {
                if (tokenTrace) {
                    $.ajax({
                        method: "GET",
                        url: '/kpi/trace',
                        data: {
                            'token': tokenTrace,
                            'duration_watching': durationWatching,
                            'pause_times': pauseTimes,
                            'seek_times': seekTimes,
                            'wait_times': waitTimes,
                            'duration_buffer': (durationBuffering / 1000),
                            'current_time': currentTime,
                            'buffer_times_over_3s': bufferOver3s,
                            '_csrf': $('meta[name="csrf-token"]').attr("content")
                        },
                    }).done(function (objData) {
                    }).fail(function (jqXHR, textStatus) {
                    });
                }
            }

            needSendTrace = 0;
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            lockLoadingComment = false;
            isAllData = false;
            autoloadComment();
            viewMoreComment();
            toggleLike();
            toggleDislike();
            handleComment();
            followVideo();
            clickLikeComment();
            clickDisLikeComment();
            handleRelpyComment();
            videoPlayer();
            autoPlayVideo();
            callBackEndVideo();
            eventClickResolution();
            showTimer();
        },
        likeComment: function (commentId) {
            if (lockLikeComment == false) {
                lockLikeComment = true;
                var commentType = $('#commentType').val();
                var contentId = $('#contentId').val();
                return likeComment(commentType, commentId, contentId);
            }
        },
        showReplyForm: function (commentId) {
            return showReplyForm(commentId);
        },
        initComment: function () {
            loadComment(0);
        }
    };
}();

$(document).ready(function () {
    Video.init();
    $('.loadingBoxComment').show();


    // $('#auto-play-video').click(function() {
    //     console.log("AAAAAAAAAAA");
    // });
});

function autoPlayVideoV2() {
    var isAutoPlay = getCookie("isAutoPlay");
    if (isAutoPlay == "") {
        setCookie("isAutoPlay", 1, 7);
        isAutoPlay = 1;
    }
    if (isAutoPlay == 1) {
        $(".switch svg:first-child").addClass("active");
        $(".switch svg:last-child").removeClass("active");
        setCookie("isAutoPlay", 0, 7);
        $("#autoPlay").val(1);
    } else {
        $(".switch svg:first-child").removeClass("active");
        $(".switch svg:last-child").addClass("active");
        setCookie("isAutoPlay", 1, 7);
        $("#autoPlay").val(0);
    }
}

function cancelNextVideo() {
    clearInterval(waitNextVideo);
    modalNext.close();
}