var $body = $('body');
var $header = $('header');
var $footer = $('footer');
var $overlay = $('.overlay');
var $crrModal;
var lastPoint = 0;
var isDisScroll = false;

$(function () {
    var currentPoint = 0;

    if (document.getElementById('datepicker_fromdate')) {
        var evenStartDate = $("#event_start_date").val();

        var dateFormat = "dd/mm/yy",
            from = $("#datepicker_fromdate")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: dateFormat
                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                }).attr('readonly', 'readonly'),
            to = $("#datepicker_todate").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
                .on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));

                }).attr('readonly', 'readonly');
        var nowT = new Date();
        var dayT = ("0" + nowT.getDate()).slice(-2);
        var monthT = ("0" + (nowT.getMonth() + 1)).slice(-2);
        var todayT = (dayT) + "/" + (monthT) + "/" + nowT.getFullYear();

        //to.datepicker("option", "maxDate", $.datepicker.parseDate(dateFormat, todayT));
        from.datepicker("option", "maxDate", $.datepicker.parseDate(dateFormat, todayT));
        if (evenStartDate) {
            to.datepicker("option", "minDate", $.datepicker.parseDate("yy-mm-dd", evenStartDate));
            from.datepicker("option", "minDate", $.datepicker.parseDate("yy-mm-dd", evenStartDate));
        }

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
    }

    $("#form-award-week").on('submit', function () {

        if ($("#datepicker_fromdate").val().length && $("#datepicker_todate").val().length) {
            return true;
        }

        showValidateMsg("Yêu cầu nhập Từ ngày-Đến ngày", 'error');
        return false;
    });

    $("#form-award-month").on('submit', function () {
        if ($("#month_report").val().length) {
            return true;
        }
        showValidateMsg("Yêu cầu chọn Tháng", 'error');
        return false;
    });

    if ($('.rcmChnlsSlider').length) {
        $('.rcmChnlsSlider').owlCarousel({
            nav: false,
            dots: false,
            loop: false,
            responsive: {
                0: {items: 1, autoWidth: true, margin: 12}
            }
        });
    }

    //handle touch swiped
    $(window).scroll(function () {
        $("#inviteBox").slideUp(200);
        $("main:first").removeClass('padTop');

        if (isDisScroll == false) {
            var scrollY = $(window).scrollTop();
            //Scroll down
            if (lastPoint < scrollY && (scrollY > 40)) {
                $header.animate({'marginTop': '-51px'}, 50);
                $footer.animate({'marginBottom': '-50px'}, 50);
            } else {
                $header.animate({'marginTop': '0px'}, 50);
                $footer.animate({'marginBottom': '0px'}, 50);
            }

            if (Math.abs(lastPoint - scrollY) > 10) {
                lastPoint = scrollY;
            }

            //Lock 2 second for scroll animate
            isDisScroll = true;
            setTimeout(function () {
                isDisScroll = false;

            }, 50)
        }

    });

    $body.on('click  touch', '.optMnBtn', function () {
        var target = $(this).data('target');
        //alert(target);
        //PHUMX
        if (document.getElementById('specialOpt')) {
            target = '#specialOpt';
        }
        showOptMn($(target));
    });

    $body.on('click touch', '.overlay', function () {
        $('.modal').hide();
        $('.overlay').hide();
        hideModal();
    });

    // modal popup
    $body.on('click touch', '.modalBtn', function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        var opt = $(this).data('opt');

        if (opt == "clearAll")
            hideModal();

        showPopup($(target));
    });

    //slider for chapters

    if ($('.chnlSlider').length) {
        $('.chnlSlider').owlCarousel({
            nav: false,
            dots: false,
            loop: false,
            responsive: {
                0: {items: 5, autoWidth: true, margin: 5}
            }
        });
    }

    // toggle menu
    $('main').on('click touch', '.toggleBtn', function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        var animation = $(this).data('ani');

        if ($(target).hasClass('showed')) {
            $(target).removeClass('showed')
            $(target).slideUp(200);
        } else {
            $(target).addClass('showed')
            $(target).slideDown(200);
        }

        if (animation == "rotate180") {
            if ($(this).hasClass('rotate180')) {
                $(this).removeClass('rotate180')
            } else {
                $(this).addClass('rotate180')
            }
        }
    });

    // choose channels
    /*
    $('.blkChooseChnls').find('.div-choose').on('click touch', function () {
        if ($(this).hasClass('chose')) {
            $(this).removeClass('chose');
        } else {
            $(this).addClass('chose');
        }
    });
*/

    //onOff button
    $('.onOff').on('click touch', function (e) {
        var target = $(this).data('target');
//        showOptMn($(target));
        e.preventDefault();
//        alert('on/off click');
        $(this).find('.ico').each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        })
    });
    //slider for subtab menu
    if ($('.subTabSlider').length) {
        $('.subTabSlider').owlCarousel({
            nav: false,
            dots: false,
            loop: false,
            responsive: {
                0: {autoWidth: true, margin: 0}
            }
        });
    }

    //slider for chapters
    if ($('.chapterList').length) {
        $('.chapterList').owlCarousel({
            nav: true,
            dots: false,
            loop: false,
            navText: [
                "<svg class='ico'><use xlink:href='/images/defs.svg#ico-next'></use></svg>",
                "<svg class='ico'><use xlink:href='/images/defs.svg#ico-prev'></use></svg>"
            ],
            responsive: {
                0: {items: 5, margin: 0}
            }
        });
    }


});

function showOptMn($target) {
    $crrModal = $target;

    $overlay.fadeIn(200);
    $target.slideDown(200);
    $overlay.on('click touch', function (e) {
        e.preventDefault();
        hideModal();
    });
    $target.find('.close').on('click touch', function (e) {
        e.preventDefault();
        hideModal();
    });

    if($target.attr('id')=='viewOptFilter'){

    }else{
        // $body.addClass('disScroll');
    }
}

function showPopup($target) {
    $crrModal = $target;

    $overlay.fadeIn(200);
    $target.fadeIn(200);
    $overlay.on('click touch', function (e) {
        e.preventDefault();
        hideModal();
    });
    $target.find('.close').on('click touch', function (e) {
        e.preventDefault();
        hideModal();
    });
    $body.addClass('disScroll');
}

//hide modal
function hideModal() {
    $overlay.fadeOut(200);
    if ($crrModal) {
        $crrModal.hide(200);
    }

    $body.removeClass('disScroll');
}

function openDatePicker(id) {
    $(id).datepicker('show');
}