/**
 * User: KhanhDQ
 * Date: 9/29/2017
 * Time: 09:00 AM
 */
//PhuMX trace KPI
var isPlaying = false;
var pauseTimes = 0;
var seekTimes = 0;
var waitTimes = 0;
var bufferOver3s = 0;

var beforeTime = 0;
var currentTime = 0;

var durationWatching = 0;
var durationBuffering = 0;
var startBuffering = 0;

var needSendTrace = true;
var firstTimeTrace = true;

var tokenTrace = "GUEST";
var waitNextVideo;

var modalNext;

var Video = function () {
    var isFirstLoad = true;
    var lockLoadingComment = false;
    var isAllData = false;
    var scope = {statusLike: null, statusDislike: null, status: null}
    var lockToogleLike = false;
    var lockFollowVideo = false;
    var lockLikeComment = false;
    var player = null;
    var status = null;

    toastr.options.showDuration = 1000;
    toastr.options.timeOut = 2000;

    function loadCommentChannel(offset,status) {
        if (isAllData == true) {
            return;
        }
        $("#loading-comment").show();
        $.ajax({
            method: "GET",
            url: $('#commentUrl').val(),
            data: {
                'status': status,
                'offset': offset,
                'limit': $("#commentLimit").val()
            }
        }).done(function (data) {
            if (data.length > 0) {
                $("#loading-comment").hide();
                $("#commentContent").append(data);
                $("#commentOffset").val(parseInt($("#commentOffset").val()) + parseInt($("#commentLimit").val()));
            } else {
                if (isFirstLoad == true) {
                    $('#boxNoComment').show();
                }
                isAllData = true;
            }

            if (document.getElementById('comment-empty')) {
                isAllData = true;
                lockLoadingComment = true;
            }

            lockLoadingComment = false;
            $("#loading-comment").hide();
            $('.btnLikeComment').bind('click', clickLikeComment());
            isFirstLoad = false;
        }).fail(function (jqXHR, textStatus) {
            $("#loading-comment").hide();
            isFirstLoad = false;
        });
    }

    function autoloadComment() {
        if($('#canLoad').val() == 1){
            $(window).scroll(function (event) {
                if ((($(window).scrollTop() + $(window).height() + 20 >= $('.blkCommentsUser').height()) || $(window).height() == $(document).height()) && lockLoadingComment == false) {
                    lockLoadingComment = true;
                    loadCommentChannel($("#commentOffset").val(),$("#commentStatus").val());
                }
            });
            if (lockLoadingComment == false) {
                lockLoadingComment = true;
                loadCommentChannel($("#commentOffset").val(),$("#commentStatus").val());
            }
        }
    }

    function toggleLike() {
        $('.likeBtn').click(function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            if (lockToogleLike == false) {
                if (scope.statusLike == null) {
                    scope.statusLike = $(this).data('like');
                }
                lockToogleLike = true;
                processLikeVideo($(this).data('id'), 'like');
            }
        });
    }

    function toggleDislike() {
        $('.dislikeBtn').click(function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            if (lockToogleLike == false) {
                if (scope.statusDislike == null) {
                    scope.statusDislike = $(this).data('dislike');
                }
                lockToogleLike = true;
                processLikeVideo($(this).data('id'), 'dislike');
            }
        });
    }


    function handleComment() {
        $('.btnClearComment').click(function () {
            $('#inputComment').val("");
        });
        $(document).on('click', '.btnClearReply', function () {

            if ($('#inputComment_' + $(this).data('parentid')).val() == "") {
                $(".boxReply_"+$(this).data('parentid')).hide();
            }

            $('#inputComment_' + $(this).data('parentid')).val("");
        });
        $('#inputComment').focus(function () {
            $("#control_comment").show();
            checkLogin();
            // $("#one-time-comment").parent().hide();
            $('.btnCommentBox').show();
        });

        $('#btnSubmitComment').click(function () {
            var content = $.trim($("#inputComment").val());
            $("#control_comment").hide();
            postComment(0, content);
        });
    }

    function checkLogin() {
        if ($('#userId').val() == '') {
            window.location = '/auth/login';
        }
    }

    function handleRelpyComment() {
        $(document).on('click', '.btnReplyComment', function () {
            var commentId = $(this).data('id');
            $('.boxReply_' + commentId ).show();
            // $("#control_comment").hide();
            // showReplyForm(commentId);
        });
    }

    function validateComment(content) {
        if (content.length <= 0) {
            toastr.error(trans("Vui lòng nhập nội dung bình luận"), {closeButton: true})
            return false;
        }
        return true;
    }

    function postComment(commentId,contentId, content) {
        if (validateComment(content) == false)
            return;
        var commentType = $('#commentType').val();
        $.ajax({
            method: "POST",
            url: $('#postCommentUrl').val(),
            data: {
                'parent_id': commentId,
                'content_id': contentId,
                'type': commentType,
                'comment': content,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (data) {
            try {
                var objData = $.parseJSON(data);
                if (objData.responseCode == 401) {
                    window.location = '/auth/login'
                } else {
                    toastr.error(objData.message, {closeButton: true})
                }
            } catch (err) {
                $("#content-one-time").val("");
                $("#content").val("");
                if (commentId > 0) {
                    $(data).insertAfter("#cm_" + commentId + " > .media-body > .boxReply_"+commentId);
                    $(".boxReply_"+commentId).hide();
                    $("#inputComment_" + commentId).val("");
                    $("#inputComment_" + commentId).blur();
                } else {
                    $("#commentContent").prepend(data);
                    $("#inputComment").val("");
                    $("#inputComment").blur();
                    $('#boxNoComment').hide();
                }
                $('.txt-singer').addClass('comment-textarea-linhtvn');
                // $('.child .media-body').addClass('comment-textarea-linhtvn');
                $("#blank_comment").hide();
            }
        }).fail(function (jqXHR, textStatus) {

        });
    }

    function showReplyForm(commentId) {
        // $("#one-time-comment").remove();
        $(".boxReply_" + commentId +" .media").show();
        $('#inputComment_' + commentId).focus(function () {
            checkLogin();
        });
    }

    function postReplyComment() {
        $(document).on('click', '.btnPostReplyComment', function () {
            var pCommentId = $(this).data('parentid');
            var content = $.trim($("#inputComment_" + pCommentId).val());
            var contentId = $(this).data('contentid');
            postComment(pCommentId,contentId, content);
        });
    }

    function viewMoreComment() {
        $(document).on('click', '.viewMoreComment', function () {
            $(this).parent().hide();
            $('.cm_group_' + $(this).data('id')).removeClass('hidden');
        });
    }


    function clickLikeComment() {
        $(document).on('click', '.btnLikeComment', function () {
            if (lockLikeComment == false) {
                lockLikeComment = true;
                var commentId = $(this).data('commentid');
                var commentType = 1;
                var contentId = $(this).data('contentid');
                likeComment(commentType, commentId, contentId);
            }
        });
    }
    function clickDisLikeComment() {
        $(document).on('click', '.btnDisLikeComment', function () {
            if (lockLikeComment == false) {
                lockLikeComment = true;
                var commentId = $(this).data('commentid');
                var commentType = 2;
                var contentId = $(this).data('contentid');
                likeComment(commentType, commentId, contentId);
            }
        });
    }

    function likeComment(type, commentId, contentId) {
        $.ajax({
            method: "POST",
            url: $('#toggleLikeComment').val(),
            data: {
                'like': type,
                'comment_id': commentId,
                'content_id': contentId,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (objData) {

            if (objData['responseCode'] == 401) {
                window.location = '/auth/login';
            }
            if (objData['responseCode'] == 200) {
                if (type == 1) {
                    if($('#icoLikedComment_' + commentId).hasClass('fa-thumbs-up')){
                        $('#icoLikedComment_' + commentId).removeClass('fa-thumbs-up');
                        $('#icoLikedComment_' + commentId).addClass('fa-thumbs-o-up');
                    }else{
                        $('#icoLikedComment_' + commentId).addClass('fa-thumbs-up');
                        $('#icoLikedComment_' + commentId).removeClass('fa-thumbs-o-up');
                        $('#icoDisLikedComment_' + commentId).removeClass('fa-thumbs-down');
                        $('#icoDisLikedComment_' + commentId).addClass('fa-thumbs-o-down');
                    }

                }
                if (type == 2) {
                    if($('#icoDisLikedComment_' + commentId).hasClass('fa-thumbs-down')){
                        $('#icoDisLikedComment_' + commentId).removeClass('fa-thumbs-down');
                        $('#icoDisLikedComment_' + commentId).addClass('fa-thumbs-o-down');
                    }else {
                        $('#icoLikedComment_' + commentId).removeClass('fa-thumbs-up');
                        $('#icoLikedComment_' + commentId).addClass('fa-thumbs-o-up');
                        $('#icoDisLikedComment_' + commentId).addClass('fa-thumbs-down');
                        $('#icoDisLikedComment_' + commentId).removeClass('fa-thumbs-o-down');
                    }

                }
                var likeCommentNumber = objData['data']['like_count'];
                $('.likeCommentNumber_' + commentId).html(likeCommentNumber);
                var disLikeCommentNumber = objData['data']['dislike_count'];
                $('.disLikeCommentNumber_' + commentId).html(disLikeCommentNumber);
            }else{
                toastr.error(objData['message'], {closeButton: true});
            }
            lockLikeComment = false;
        }).fail(function (jqXHR, textStatus) {
            lockLikeComment = false;
            toastr.error(objData['message'], {closeButton: true});
        });
    }

    //duyệt comment
    function clickApproveComment() {
        $(document).on('click', '.approve-cmt', function () {
            var isApprove = $(this).find('i').hasClass('active');
            if (isApprove == false) {
                var commentId = $(this).data('commentid');
                var statusComment = 1;
                approveComment(statusComment, commentId);
            }
        });
        $(document).on('click', '.reject-cmt', function () {
            var isReject = $(this).find('i').hasClass('active');
            if (isReject == false) {
                var commentId = $(this).data('commentid');
                var statusComment = 2;
                approveComment(statusComment, commentId);
            }
        });
    }

    function approveComment(statusComment, commentId) {
        $.ajax({
            method: "POST",
            url: $('#approveComment').val(),
            data: {
                'status': statusComment,
                'id': commentId,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (data) {
            var objData = $.parseJSON(data);
            if (objData['responseCode'] == 401) {
                window.location = '/auth/login';
            }
            if (objData['responseCode'] == 200) {
                if(statusComment == 1){
                    $('#btnReplyComment_'+commentId).removeClass('hide');
                    $('.approve-cmt-'+commentId).addClass('hide');
                    $('.reject-cmt-'+commentId).removeClass('hide');
                    $('span').find("a[data-commentid='" + commentId + "']").removeClass('hide');
                    getNumberNotification();
                }else if(statusComment == 2){
                    $('#btnReplyComment_'+commentId).addClass('hide');
                    $('.reject-cmt-'+commentId).addClass('hide');
                    $('.approve-cmt-'+commentId).removeClass('hide');
                    $('span').find("a[data-commentid='" + commentId + "']").addClass('hide');
                    getNumberNotification();
                }
                if($('#commentStatus').val() == 0 || $('#commentStatus').val() == 1 || $('#commentStatus').val() == 2){
                    $commentLength = $('#cm_'+commentId).parent('.list-comment').children().length;
                    if($commentLength > 1 ||$('#cm_'+commentId).hasClass('child')){
                        $('#cm_'+commentId).remove();
                    }else if($commentLength == 1){
                        $('#cm_'+commentId).closest('.item-comment').remove();
                    }

                }
                toastr.success(objData['message'], {closeButton: true});
            }

        }).fail(function (jqXHR, textStatus) {
            var objData = $.parseJSON(data);
            toastr.error(objData['message'], {closeButton: true});
            lockLikeComment = false;
        });
        //lọc comment
        function filterComment() {
            $(document).on('click', '.filter-cmt-user .item-popover', function () {
                var isStatus = $(this).data('status');
                var cOffset = 0;
                if (isStatus == 0 || isStatus == 1 || isStatus == 2 || isStatus == 4) {
                    $('#canLoad').val(1);
                    loadCommentChannel(cOffset, isStatus);
                    $('.sort-comment .text').text($(this).data('title'));
                }
            });
        }
        $("#view-comment").on('click', function () {
            loadCommentChannel($('#commentOffset').val(),0);
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            autoloadComment();
            viewMoreComment();
            toggleLike();
            toggleDislike();
            handleComment();
            clickLikeComment();
            clickDisLikeComment();
            handleRelpyComment();
            postReplyComment();
            clickApproveComment();
            // filterComment();
        },
        checkLogin: function () {
            return checkLogin();
        },
    };
}();

$(document).ready(function () {
    Video.init();
    $('.loadingBoxComment').show();
});

function cancelNextVideo() {
    clearInterval(waitNextVideo);
    modalNext.close();

}
$(document).on('click','.option-comment',function() {
    $('.popover-more').hide();
    $(this).find('.popover-more').toggle();
});

$(document).click(function (e)
{
    // Đối tượng container chứa popup
    var container = $(".sort-comment");

    // Nếu click bên ngoài đối tượng container thì ẩn nó đi
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.find('.popover-more').hide();
    }
});

$(document).on('click','.popover-more .item-popover',function() {
    $(this).parent('.popover-more').hide();
});

$(document).on('click', '.filter-cmt-user .item-popover', function () {
    var isStatus = $(this).data('status');
    var cOffset = 0;
    if (isStatus == 0 || isStatus == 1 || isStatus == 2 || isStatus == 4) {
        $('.sort-comment .text').text($(this).data('title'));
        $('#canLoad').val(1);
        $.ajax({
            method: "GET",
            url: $('#commentUrl').val(),
            data: {
                'status': isStatus,
                'offset': cOffset,
                'limit': $("#commentLimit").val()
            }
        }).done(function (data) {
            if (data.length > 0) {
                $("#loading-comment").hide();
                $("#commentContent").html(data);
                $('#commentStatus').val(isStatus);
                $("#commentOffset").val(parseInt($("#commentOffset").val()) + parseInt($("#commentLimit").val()));
            } else {
                var mess = "<p>"+trans('Không có dữ liệu')+"</p>";
                $("#commentContent").html(mess);
                isAllData = true;
            }
            isFirstLoad = false;
        }).fail(function (jqXHR, textStatus) {
            $("#loading-comment").hide();
            isFirstLoad = false;
        });
    }
});
$('.sort-comment').click(function () {
    $(this).find('.popover-more ').show();
})
function loadCommentById(commentId,videoId,commentStatus) {
    $("#loading-comment").show();
    $.ajax({
        method: "GET",
        url: $('#commentUrl').val(),
        data: {
            'videoId': videoId,
            'commentId': commentId,
            'status':commentStatus
        }
    }).done(function (data) {
        if (data.length > 0) {
            $("#commentContent").empty();
            $("#commentContent").append(data);
            $("#commentContent .list-comment .child").removeClass('hidden');
            $("#commentContent .showMore p").hide();
            $('html, body').animate({
                scrollTop: $('#cm_'+commentId).offset().top
            }, 'slow');
        }
    }).fail(function (jqXHR, textStatus) {
    });
}
//view detail comment from notification
var url_string = window.location.href;
var url = new URL(url_string);
var videoId = url.searchParams.get("videoId");
var commentId = url.searchParams.get("comment_id");
var commentStatus = url.searchParams.get("comment_status");

if(commentStatus == null || commentStatus == ''){
    commentStatus = 4;
}
if(commentStatus != null){
    $('.filter-cmt-user .item-popover').each(function( index ) {
        if($(this).data('status') == commentStatus){
            $('.sort-comment .text').text($(this).data('title'));
            $('#commentStatus').val(commentStatus);
        }
    });
}
if(videoId != null){
    $('.tablinks').removeClass('active');
    $('.tabcontent').css('height','0px');
    $('.tabcontent').css('overflow','hidden');
    $('#view-comment').addClass('active');
    $('#tab6').css('height','auto');
    $('#tab6').css('overflow','inherit');
    $("#commentContent").empty();
    $("#canLoad").val('0');
    loadCommentById(commentId,videoId,commentStatus);
}

