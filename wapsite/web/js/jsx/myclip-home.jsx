/* @author PHUMX */

var HomePage = React.createClass({
    getInitialState: function () {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: parseInt($("#getMoreOffset").val()),
            lockScroll: false,
            endOfData: false
        };
    },
    componentWillMount: function () {
    },
    componentDidUpdate: function (prevProps, prevState) {
        //Eliipsis
        $(".mdl-film .item .text-name, .commentions").ellipsis();
    },
    componentDidMount: function () {
        window.addEventListener('scroll', this.updateViewport, false);
        this.updateViewport();
    },
    componentWillUnmount: function () {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function () {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Dieu kien load DATA
        if (!this.state.isFirstTimeLoad && !this.state.lockScroll && (document.body.scrollHeight < window.pageYOffset + window.innerHeight+20)) {
            this.setState({lockScroll: true});
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                tryCount: 0,
                retryLimit: 3,
                data: {'id': 'video_newsfeed', 'limit': this.state.limit, 'offset': this.state.offset},
                success: function (ajaxData) {
                    var videoList = ajaxData.data.content;
                    // Neu khong lay dc content thi STOP
                    if (videoList.length == 0) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                    var oldVideoList = this.state.appendVideos;
                    oldVideoList = oldVideoList.concat(videoList);
                    this.setState({appendVideos: oldVideoList, lockScroll: false})

                    if (videoList.length > 0) {
                        this.setState({offset: this.state.offset + videoList.length, lockScroll: false});
                    } else {
                        this.setState({lockScroll: true})
                    }
                    if (videoList.length < this.state.limit) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({lockScroll: false})
                }.bind(this)
            });

        } else {
            this.state.isFirstTimeLoad = false;
        }
    },

    render: function () {
        var rows = [];
        var serverAjax = this.state.appendVideos;
        if (serverAjax) {
            rows.push(
                <NewsFeedBox videos={serverAjax}/>
            );
        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(
                <div>
                    <div className="clear-10"></div>
                    <div className="spinner">
                        <div className="quarter">
                            <div className="circle"></div>
                        </div>
                    </div>
                    <div className="clear-10"></div>
                </div>
            )
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            rows.push(
                <div className="clear-10"></div>
            );
        }

        return (
            <div>
                {rows}
            </div>
        );

    }
});
var NewsFeedBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/avatar.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <div className="users-2">
                        <a href={"/thanh-vien/"+this.props.videos[i]['userId']} className="wrap-avatar">
                            <img src={this.props.videos[i]['userAvatarImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage4x4}} width="30" className="img-circle"/>
                        </a>
                        <div className="left-users-2">
                            <a href={"/video/"+this.props.videos[i]['id']+"/"+this.props.videos[i]['slug']} className="text-name ellipsis">
                                {this.props.videos[i]['name']}
                            </a>
                            <div className="users-2-info">
                                <a href={"/thanh-vien/"+this.props.videos[i]['userId']} className="text-singer ellipsis">
                                    {this.props.videos[i]['userName']}
                                </a>
                                <span className="text-view"> <i className="fa icon-eye2"></i> {this.props.videos[i]['play_times']}</span>
                                <span className="text-duration ellipsis"><i className="fa icon-watch"></i> {this.props.videos[i]['publishedTime']}</span>
                            </div>
                        </div>
                    </div>
                    <div className="commentions">
                        {this.props.videos[i]['description']}
                    </div>
                    <div className="place-holder-video">
                        <a href={"/video/"+this.props.videos[i]['id']+"/"+this.props.videos[i]['slug']} className="link-holder">
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <span className="duration pull-right">{this.props.videos[i]['duration']}</span>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="mdl-list-videos">
                {videos}
            </div>
        );
    }
});

var ChannelBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/avatar.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <div className="users-2">
                        <a href={"/thanh-vien/"+this.props.videos[i]['userId']} className="wrap-avatar">
                            <img src={this.props.videos[i]['userAvatarImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage4x4}} width="30" className="img-circle"/>
                        </a>
                        <div className="left-users-2">
                            <a href={"/video/"+this.props.videos[i]['id']+"/"+this.props.videos[i]['slug']} className="text-name ellipsis">
                                {this.props.videos[i]['name']}
                            </a>
                            <div className="users-2-info">
                                <a href={"/thanh-vien/"+this.props.videos[i]['userId']} className="text-singer ellipsis">
                                    {this.props.videos[i]['userName']}
                                </a>
                                <span className="text-view"> <i className="fa icon-eye2"></i> {this.props.videos[i]['play_times']}</span>
                                <span className="text-duration ellipsis"><i className="fa icon-watch"></i> {this.props.videos[i]['publishedTime']}</span>
                            </div>
                        </div>
                    </div>
                    <div className="commentions">
                        {this.props.videos[i]['description']}
                    </div>
                    <div className="place-holder-video">
                        <a href={"/video/"+this.props.videos[i]['id']+"/"+this.props.videos[i]['slug']} className="link-holder">
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <span className="duration pull-right">{this.props.videos[i]['duration']}</span>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="mdl-list-videos">
                {videos}
            </div>
        );
    }
});

ReactDOM.render(
    <HomePage url="/default/get-home" loadMoreUrl="/default/get-more-content"/>,
    document.getElementById('container')
);