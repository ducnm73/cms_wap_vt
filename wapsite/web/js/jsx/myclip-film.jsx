/* @author PHUMX */
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var FilmPage = React.createClass({

    getInitialState: function () {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: 6,
            offset: 0,
            lockScroll: false
        };
    },
    componentWillMount: function () {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'GET',
            tryCount: 0,
            retryLimit: 3,
            data: {'limit': this.state.limit},
            success: function (result) {
                this.setState(
                    {clientData: result}
                );
            }.bind(this),
            error: function (xhr, textStatus, errorThrown) {
                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        //try again
                        $.ajax(this);
                        return;
                    }
                    return;
                }
                if (xhr.status == 500) {
                    //handle error
                } else {
                    //handle error
                }
            }.bind(this)
        });
    },
    componentDidUpdate: function (prevProps, prevState) {
        $('#carousel-banner').owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            paginationSpeed: 1,
            lazyFollow: false,
            responsiveRefreshRate: 1,
            autoHeight: false,
        });
    },
    componentDidMount: function () {
    },
    componentWillUnmount: function () {
    },

    render: function () {
        var rows = [];
        if (!this.state.clientData) return (
            <div>
                <div className="clear-10"></div>
                <div className="spinner">
                    <div className="quarter">
                        <div className="circle"></div>
                    </div>
                </div>
                <div className="clear-10"></div>
            </div>
        );
        var serverResponse = this.state.clientData.data;
        if (serverResponse)
            for (var i = 0; i < serverResponse.length; i++) {
                switch (serverResponse[i]['type']) {
                    case "BANNER":
                        rows.push(
                            <Slideshow slides={serverResponse[i].content}/>
                        );
                        break;
                    case "FILM":
                        rows.push(
                            <FilmBox films={serverResponse[i].content} title={serverResponse[i].name}
                                     boxId={serverResponse[i].id}/>
                        );
                        break;
                }
            }
        return (
            <div>
                <ReactCSSTransitionGroup
                    transitionName="animationreact"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}
                    transitionAppear={true}
                    transitionAppearTimeout={500}>
                    {rows}
                </ReactCSSTransitionGroup>
            </div>)
    }
});

var Slideshow = React.createClass({

    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png"
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
        $('#carousel-banner').owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 1000000,
            paginationSpeed: 1,
            lazyFollow: false,
            responsiveRefreshRate: 1,
            autoHeight: false,
        });
    },
    render: function () {
        var slides = [];
        for (var i = 0; i < this.props.slides.length; i++) {
            slides.push(
                <div className="item">
                    <a href={this.props.slides[i]['redirectUrl']}>
                        <img src={this.props.slides[i]['coverImage']}
                             onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                             onloadstart={(e)=>{e.target.src=this.state.defaultImage16x9}}
                             className="center-block" width=""/>
                    </a>
                    <div className="overlay">
                        <p>
                            <a href={this.props.slides[i]['redirectUrl']}
                               className="text-name">{this.props.slides[i]['name']}</a>
                        </p>
                    </div>
                </div>
            );
        }
        return (
            <div id="carousel-banner" className="owl-carousel">
                {slides}
            </div>
        );
    }
});

var FilmBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage2x3: "/images/2x3.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var films = [];
        for (var i = 0; i < this.props.films.length; i++) {
            films.push(
                <a href={"/phim/"+this.props.films[i]['id']+"/"+this.props.films[i]['slug']} className="item">
                    <span className="link-holder">
                    <img src={this.props.films[i]['coverImage']}
                         onError={(e)=>{e.target.src=this.state.defaultImage2x3}}/>
                    </span>
                    <p className="text-name">{ this.props.films[i]['name'] } </p>
                </a>
            );
        }
        return (
            <div className="mdl-film list-margin-0">
                <div className="title">
                    <a href={"/xem-them/"+this.props.boxId } className="txt">{this.props.title}</a>
                    <a href={"/xem-them/"+this.props.boxId} className="pull-right view-all"><i
                        className="fa fa-angle-right"></i></a>
                </div>
                <div className="content">
                    {films}
                </div>
            </div>

        );
    }
});

ReactDOM.render(
    <FilmPage url="/film/get-home-film" loadMoreUrl="/default/get-more-content"/>,
    document.getElementById('container')
);

