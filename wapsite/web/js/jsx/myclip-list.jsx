/* @author PHUMX */

$.fn.ellipsis = function () {
    return this.each(function () {
        var el = $(this);

        if (el.css("overflow") == "hidden") {
            var text = el.html();
            var multiline = true;//el.hasClass('multiline');
            var t = $(this.cloneNode(true))
                .hide()
                .css('position', 'absolute')
                .css('overflow', 'visible')
                .width(multiline ? el.width() : 'auto')
                .height(multiline ? 'auto' : el.height());

            el.after(t);

            function height() {
                return t.height() > el.height() + 1;
            };
            function width() {
                return t.width() > el.width();
            };

            var func = multiline ? height : width;

            while (text.length > 0 && func()) {
                text = text.substr(0, text.length - 8);
                t.html(text + "...");
            }

            el.html(t.html());
            t.remove();
        }
    });
};
var ListPage = React.createClass({

    getInitialState: function () {
        return {
            clientData: null,
            appendItems: [],
            isFirstTimeLoad: true,
            limit: 36,
            offset: parseInt((document.getElementById('show-more-limit')) ? document.getElementById('show-more-limit').value : 0),
            lockScroll: false,
            endOfData: false,
            type: null
        };
    },
    componentWillMount: function () {
        //Neu chua load truoc du lieu HTML  thi load = ajax
        if (this.state.offset == 0) {
            this.updateViewport();
        } else {
            this.state.isFirstTimeLoad = false
        }
    },
    componentDidMount: function () {
        window.addEventListener('scroll', this.updateViewport, false);
        //Eliipsis
        $(".mdl-film .item .text-name").ellipsis();
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    componentWillUnmount: function () {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function () {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Them dieu kien rang buoc moi thuc hien updateViewPort
        if (this.props.needActiveId) {
            if (!$(this.props.needActiveId).is(':visible')) {
                return;
            }
        }
        if (this.state.isFirstTimeLoad || (!this.state.isFirstTimeLoad && !this.state.lockScroll && (document.body.scrollHeight < window.pageYOffset + window.innerHeight + 100))) {
            this.setState({lockScroll: true, isFirstTimeLoad: false});
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                data: {'id': $("#load-more-content").val(), 'limit': this.state.limit, 'offset': this.state.offset},
                success: function (ajaxData) {
                    var itemList = ajaxData.data.content;
                    var oldItemList = this.state.appendItems;
                    // Neu khong lay dc content thi STOP
                    if (itemList.length == 0) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                    oldItemList = oldItemList.concat(itemList);
                    this.setState({appendItems: oldItemList, lockScroll: false, type: ajaxData.data.type})
                    if (itemList.length > 0 && itemList.length >= this.state.limit) {
                        this.setState({offset: this.state.offset + itemList.length, lockScroll: false});
                    }
                    // Neu item tra ve < limit thi ko load nua
                    if (itemList.length < this.state.limit) {
                        this.setState({endOfData: true, lockScroll: false});
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({lockScroll: false})
                }.bind(this)
            });
        } else {
            this.state.isFirstTimeLoad = false;

        }
    },
    render: function () {
        var rows = [];
        var serverAjax = this.state.appendItems;
        if (serverAjax && !this.state.isFirstTimeLoad) {
            switch (this.state.type) {
                case "VOD":
                    if (this.props.template == '2') {
                        rows.push(
                            <VideoBox2 videos={serverAjax} noTitle={true}/>
                        );
                    } else {
                        rows.push(
                            <VideoBox videos={serverAjax} noTitle={true}/>
                        );
                    }
                    break;
                case "FILM":
                    rows.push(
                        <FilmBox films={serverAjax} noTitle={true}/>
                    );
                    break;
                case "USER":
                    if (this.props.template == '2') {
                        rows.push(
                            <UserBox users={serverAjax} title={"U"} noTitle={true}
                                     showButton={false}/>
                        );
                    } else {
                        rows.push(
                            <UserBox users={serverAjax} title={"U"} noTitle={true}
                                     showButton={true}/>
                        );
                    }
                    break;
            }

        }

        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(
                <div>
                    <div className="clear-10"></div>
                    <div className="spinner">
                        <div className="quarter">
                            <div className="circle"></div>
                        </div>
                    </div>
                    <div className="clear-10"></div>
                </div>
            )
        }

        //Ket thuc Scroll
        if (this.state.endOfData) {
            if (serverAjax.length == 0 && this.state.offset == 0) {
                rows.push(
                    <p className="mar-left10">Không có dữ liệu.</p>
                );
            }
            ;
            rows.push(
                <div className="clear-10"></div>
            );
        }
        return (
            <div>
                {rows}
            </div>)

    }
});

var NewsFeedBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <a className="users ellipsis">
                        <img src={this.props.videos[i]['userAvatarImage']}
                             onError={(e)=>{e.target.src=this.state.defaultImage4x4}}
                             onloadstart={(e)=>{e.target.src=this.state.defaultImage4x4}}
                             className="img-circle" width="30"/>
                        {this.props.videos[i]['userName']}
                    </a>
                    <div className="commentions">
                        {this.props.videos[i]['description']}
                    </div>
                    <div className="place-holder-video">
                        <a href={"/video/"+this.props.videos[i]['id']+"/"+this.props.videos[i]['slug']}>
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage4x4}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage4x4}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <p><span className="duration">{this.props.videos[i]['duration']}</span></p>
                            <p className="ellipsis">
                                <a href={"/video/"+this.props.videos[i]['id']+"/"+this.props.videos[i]['slug']}
                                   className="text-name">{this.props.videos[i]['name']}</a>
                            </p>
                            <p>
                                <span className="viewer">
                                    <i className="fa fa-play"></i>{this.props.videos[i]['play_times']}</span>
                            </p>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="mdl-list-videos">
                {videos}
            </div>
        );
    }
});

var FilmBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage2x3: "/images/2x3.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var films = [];
        for (var i = 0; i < this.props.films.length; i++) {
            films.push(
                <a href={"/phim/"+this.props.films[i]['id']+"/"+this.props.films[i]['slug']} className="item">
                    <span className="link-holder">
                        <img src={this.props.films[i]['coverImage']}
                             onError={(e)=>{e.target.src=this.state.defaultImage2x3}}/>
                    </span>
                    <p className="text-name">{ this.props.films[i]['name'] } </p>
                </a>
            );
        }

        if (this.props.noTitle) {
            return (
                <div className="mdl-film list-margin-0">
                    <div className="content">
                        {films}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="mdl-film list-margin-0 border-top">
                    <div className="title">
                        <a className="txt">{this.props.title}</a>
                    </div>
                    <div className="content">
                        {films}
                    </div>
                </div>

            );
        }
    }
});

var VideoBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];

        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <div className="place-holder-video">
                        <a href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <p><span className="duration">{this.props.videos[i]['duration']}</span></p>
                        </div>
                    </div>
                    <p className="ellipsis text-name"><a
                        href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>{this.props.videos[i]['name']}</a>
                    </p>

                </div>
            );
        }

        if (this.props.noTitle) {
            return (
                <div className="mdl-video list-margin-0">
                    <div className="content">
                        {videos}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="mdl-video list-margin-0 border-top">
                    <div className="title">
                        <a className="txt">{this.props.title}</a>
                    </div>
                    <div className="content">
                        {videos}
                    </div>
                </div>
            );

        }
    }
});

var VideoBox2 = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];

        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <div className="place-holder-video">
                        <a className="link-holder"
                           href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <p><span className="duration">{ this.props.videos[i]['duration'] }</span></p>
                        </div>
                    </div>
                    <div className="description-video">
                        <p className="ellipsis text-name color-black">
                            <a href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>{this.props.videos[i]['name']}</a>
                        </p>
                        <p className="ellipsis text-singer"><span>{this.props.videos[i]['play_times']} trans('lượt xem')</span>
                        </p>
                    </div>
                </div>
            );
        }

        return (
            <div className="mdl-video-3 list-margin-0">
                <div className="content">
                    {videos}
                </div>
            </div>
        );

    }
});

var UserBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    handleClick: function (e) {
        //update state, etc.
        var tmpE = $(e.target).attr('id');
        var spl = tmpE.split("_");
        toggleFollow(spl[1]);

    },
    render: function () {
        var users = [];

        for (var i = 0; i < this.props.users.length; i++) {
            var tmpKey = this.props.users[i]['id'];
            if (this.props.showButton) {
                users.push(
                    <div className="item">
                        <a href={"/trang-thanh-vien/"+this.props.users[i]['id']} className="image">
                            <img src={this.props.users[i]['avatarImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage4x4}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage4x4}}
                                 className="img-circle" width="100%"/>
                        </a>
                        <div className="member-center-2">
                            <p className="ellipsis text-name"><a
                                href={"/trang-thanh-vien/"+this.props.users[i]['id']}>{this.props.users[i]['name']}</a>
                            </p>
                            <p className="ellipsis text-singer">{this.props.users[i]['videoCount']} videos <span
                                className="dot">•</span>
                            <span className={"follow_count_"+this.props.users[i]['id']}
                                  id={"follow_count_"+this.props.users[i]['id']}>{this.props.users[i]['followCount']}
                            </span> theo dõi
                            </p>
                        </div>

                        <div className="member-right-2">
                            <a href="javascript:void(0)" onClick={this.handleClick}
                               id={"follow_"+this.props.users[i]['id']}
                               className={"follow_"+this.props.users[i]['id'] +" btn-accept toggle-follow"}>
                                {(this.props.users[i]['isFollow']) ? "Hủy theo dõi" : "Theo dõi" }
                            </a>

                        </div>

                    </div>
                );
            } else {
                users.push(
                    <div className="item">
                        <a href={"/trang-thanh-vien/"+this.props.users[i]['id']} className="image">
                            <img src={this.props.users[i]['avatarImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage4x4}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage4x4}}
                                 className="img-circle" width="100%"/>
                        </a>
                        <div className="member-center-2">
                            <p className="ellipsis text-name"><a
                                href={"/trang-thanh-vien/"+this.props.users[i]['id']}>{this.props.users[i]['name']}</a>
                            </p>
                            <p className="ellipsis text-singer">{this.props.users[i]['videoCount']} videos <span
                                className="dot">•</span>
                            <span className={"follow_count_"+this.props.users[i]['id']}
                                  id={"follow_count_"+this.props.users[i]['id']}>{this.props.users[i]['followCount']}
                            </span> theo dõi
                            </p>
                        </div>
                    </div>
                );
            }

        }
        if (this.props.noTitle) {
            return (
                <div className="mdl-member">
                    <div className="content">
                        {users}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="mdl-member list-margin-0">
                    <div className="title">
                        <a className="txt">{this.props.title}</a>
                    </div>
                    <div className="content">
                        {users}
                    </div>
                </div>
            );

        }
    }
});




var ChannelBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
        };
    },
    handleClick: function (e) {
        //update state, etc.
        followChannel(e.currentTarget.getAttribute('data-column'));
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {

        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            var channelId=this.props.videos[i]['channel_id'];
            videos.push(
                <article className="userItem" id={ this.props.videos[i]['channel_id'] }>
                    <div className="ctn">
                        <div className="avatar left">
                            <a href={"channel/"+this.props.videos[i]['channel_id'] }>
                                <img src={this.props.videos[i]['avatarImage']} alt=""/>
                            </a>
                        </div>
                        <div className="ctnhld">
                            <h6 className="title"><a href={"channel/"+ this.props.videos[i]['channel_id'] }> {this.props.videos[i]['channel_name']} </a></h6>
                            <div className="txtInfo">
                                { this.props.videos[i]['num_video'] } videos
                                <i className="dot"></i>
                                { this.props.videos[i]['num_follow'] } lượt theo dõi
                            </div>
                        </div>
                        <a className="icoMore color0081FF"  data-column={channelId} onClick={this.handleClick}   >
                            <span  dangerouslySetInnerHTML={{__html: ' <svg class="ico"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../images/defs.svg#ico-follow"></use></svg>'}} />
                        </a>
                    </div>
                </article>
            );
        }

        return (
            <div className="mdl-video-3 list-margin-0">
                <div className="content">
                    {videos}
                </div>
            </div>
        );

    }
});



/**
 * Lay danh sach ban be cuar user tuong ung
 */
function loadFriendOfUser() {
    if (document.getElementById('profile_member_follow')) {
        ReactDOM.render(
            <ListPage url={ "/default/get-more-content?id="+ $("#member_follow_id").val() }
                      template={2}
                      loadMoreUrl={ "/default/get-more-content?id="+ $("#member_follow_id").val() }/>,

            document.getElementById('profile_member_follow')
        );
    }
}

/**
 * Lay danh sach video va User like
 */
function loadVideoLikeOfUser() {
    if (document.getElementById('video_user_like')) {
        ReactDOM.render(
            <ListPage url={ "/default/get-more-content?id="+ $("#video_user_like_id").val() }
                      template={2}
                      loadMoreUrl={ "/default/get-more-content?id="+ $("#video_user_like_id").val() }/>,
            document.getElementById('video_user_like')
        );
    }
}

if (document.getElementById('video_of_user')) {
    ReactDOM.render(
        <ListPage url={ "/default/get-more-content?id="+ $("#video_of_user_id").val() }
                  template={2}
                  loadMoreUrl={ "/default/get-more-content?id="+ $("#video_of_user_id").val() }/>,
        document.getElementById('video_of_user')
    );

}
/**
 * Lay danh sach thanh vien dang theo doi
 */
function loadMemberFollow() {
    $("#tabs-follow-user").show();
    if (document.getElementById("tabs-follow-user")) {
        ReactDOM.render(
            <ListPage url={ "/default/get-more-content?id=member_follow" }
                      needActiveId="#tabs-follow-user"
                      loadMoreUrl={ "/default/get-more-content?id=member_follow" }/>,
            document.getElementById('tabs-follow-user')
        );
    }
}

/**
 * Lay danh sach noi dung theo ID & bind content vao ID tuong ung
 * @param id
 */
function loadMoreContentById(id) {
    $("#"+id).show();
    ReactDOM.render(
        <ListPage url={"/default/get-more-content?id="+id}
                       loadMoreUrl={"/default/get-more-content?id="+id}
                       needActiveId={"#"+id} />,
        document.getElementById(id)
    );
}

if (document.getElementById("tabs-member")) {
    ReactDOM.render(
        <ListPage url={ "/default/get-more-content?id=member"}
                  needActiveId="#tabs-member"
                  loadMoreUrl={ "/default/get-more-content?id=member" }/>,
        document.getElementById('tabs-member')
    );
}

if (document.getElementById('load-more-content')) {
    ReactDOM.render(
        <ListPage url={ "/default/get-more-content?id="+document.getElementById('load-more-content').value }
                  loadMoreUrl={ "/default/get-more-content?id="+document.getElementById('load-more-content').value }/>,
        document.getElementById('lazy-container')
    );
}


