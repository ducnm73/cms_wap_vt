/* @author PHUMX */

var VideoPage = React.createClass({

    getInitialState: function () {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: 4,
            offset: 0,
            lockScroll: false
        };
    },
    componentWillMount: function () {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'GET',
            data: {'limit': this.state.limit},
            tryCount: 0,
            retryLimit: 3,
            success: function (result) {
                this.setState(
                    {clientData: result}
                );
            }.bind(this),
            error: function (xhr, textStatus, errorThrown) {
                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        //try again
                        $.ajax(this);
                        return;
                    }
                    return;
                }
                if (xhr.status == 500) {
                    //handle error
                } else {
                    //handle error
                }
            }.bind(this)
        });
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    componentDidMount: function () {
    },
    componentWillUnmount: function () {
    },

    render: function () {
        var rows = [];
        if (!this.state.clientData) return (
            <div>
                <div className="clear-10"></div>
                <div className="spinner">
                    <div className="quarter">
                        <div className="circle"></div>
                    </div>
                </div>
                <div className="clear-10"></div>
            </div>
        );
        var serverResponse = this.state.clientData.data;
        if (serverResponse) {
            for (var i = 0; i < serverResponse.length; i++) {
                switch (serverResponse[i]['type']) {
                    case "BANNER":
                        rows.push(
                            <Slideshow slides={serverResponse[i].content}/>
                        );
                        break;
                    case "VOD":
                        rows.push(
                            <VideoBox videos={serverResponse[i].content} title={serverResponse[i].name}
                                      boxId={serverResponse[i].id}/>
                        );
                        break;
                }
            }
            if (serverResponse.length == 0) {
                this.state.lockScroll = false;
                rows.push(
                    <p className="mar-left10">Không có dữ liệu</p>
                );
            }
        }
        return (
            <div>
                {rows}
            </div>)

    }
});
var VideoLazyPage = React.createClass({
    getInitialState: function () {
        return {
            clientData: null,
            appendContents: [],
            isFirstTimeLoad: true,
            limit: 36,
            offset: 0,
            lockScroll: false,
            endOfData: false
        };
    },
    componentWillMount: function () {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'GET',
            tryCount: 0,
            retryLimit: 3,
            data: {'limit': this.state.limit},
            success: function (result) {
                this.setState(
                    {clientData: result}
                );
            }.bind(this),
            error: function (xhr, textStatus, errorThrown) {
                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        //try again
                        $.ajax(this);
                        return;
                    }
                    return;
                }
                if (xhr.status == 500) {
                    //handle error
                } else {
                    //handle error
                }
            }.bind(this)
        });
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    componentDidMount: function () {
        window.addEventListener('scroll', this.updateViewport, false);
        window.addEventListener('resize', this.updateViewport, false);
        this.updateViewport();
    },
    componentWillUnmount: function () {
        window.removeEventListener('scroll', this.updateViewport);
        window.removeEventListener('resize', this.updateViewport);
    },
    updateViewport: function () {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Them dieu kien rang buoc moi thuc hien updateViewPort
        if (this.props.needActiveId) {
            if (!$(this.props.needActiveId).is(':visible')) return;
        }

        if (!this.state.isFirstTimeLoad && !this.state.lockScroll && (document.body.scrollHeight < window.pageYOffset + window.innerHeight+20)) {
            this.setState({lockScroll: true});
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                data: {'limit': this.state.limit, 'offset': this.state.offset},
                success: function (ajaxData) {
                    var contentList = ajaxData.data.content;
                    // Neu khong lay dc content thi STOP
                    if (contentList.length == 0) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                    var oldContentList = this.state.appendContents;
                    oldContentList = oldContentList.concat(contentList);
                    this.setState({appendContents: oldContentList, lockScroll: false})
                    if (contentList.length > 0 && contentList.length >= this.state.limit) {
                        this.setState({offset: this.state.offset + contentList.length, lockScroll: false});
                    }
                    // Neu item tra ve < limit thi ko load nua
                    if (contentList.length < this.state.limit) {
                        this.setState({endOfData: true, lockScroll: false});
                    }

                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({lockScroll: false})
                }.bind(this)
            });

        } else {
            this.state.isFirstTimeLoad = false;
        }
    },
    render: function () {
        var rows = [];
        if (!this.state.clientData) return (
            <div>
                <div className="clear-10"></div>
                <div className="spinner">
                    <div className="quarter">
                        <div className="circle"></div>
                    </div>
                </div>
                <div className="clear-10"></div>
            </div>
        );
        var serverResponse = this.state.clientData.data;
        if (serverResponse) {
            if (this.state.offset == 0) {
                this.setState({'offset': serverResponse.content.length})
            }
            switch (serverResponse['type']) {
                case 'VOD':
                    rows.push(
                        <VideoBox noTitle={true} paddingTop={true} videos={serverResponse.content}/>
                    );
                    break;
                case 'USER_FOLLOW_VIDEO':
                    for (var i = 0; i < serverResponse['content'].length; i++) {
                        rows.push(
                            <VideoUserFollowBox videos={serverResponse['content'][i].videos }
                                                userId={serverResponse['content'][i].friend_id }
                                                userName={serverResponse['content'][i].friend_name }
                                                userAvatar={serverResponse['content'][i].avatarImage}/>
                        );
                    }
                    break;

            }
            if (serverResponse.content.length == 0) {
                this.state.lockScroll = false;
                rows = [];
                rows.push(
                    <p className="mar-left10">Không có dữ liệu</p>
                );
            }
        }

        var serverAjax = this.state.appendContents;
        if (serverAjax) {
            switch (serverResponse['type']) {
                case 'VOD':
                    rows.push(
                        <VideoBox noTitle={true} paddingTop={false} videos={serverAjax}/>
                    );
                    break;
                case 'USER_FOLLOW_VIDEO':
                    for (var i = 0; i < serverAjax.length; i++) {
                        rows.push(
                            <VideoUserFollowBox videos={serverAjax[i].videos }
                                                userId={serverAjax[i].friend_id }
                                                userName={serverAjax[i].friend_name }
                                                userAvatar={serverAjax[i].avatarImage}/>
                        );
                    }
                    break;

            }
        }

        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(
                <div>
                    <div className="clear-10"></div>
                    <div className="spinner">
                        <div className="quarter">
                            <div className="circle"></div>
                        </div>
                    </div>
                    <div className="clear-10"></div>
                </div>
            )
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            rows.push(
                <div className="clear-10"></div>
            );
        }
        return (<div>{rows}</div>)

    }
});
var VideoBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/avatar.gif",
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <div className="place-holder-video">
                        <a className="link-holder"
                           href={"/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] }>
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <p><span className="duration">{this.props.videos[i]['duration']}</span></p>
                        </div>
                    </div>
                    <p className="ellipsis text-name"><a
                        href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>{this.props.videos[i]['name']}</a>
                    </p>
                </div>
            );
        }
        if (this.props.noTitle) {
            if (this.props.paddingTop) {
                return (
                    <div className="mdl-video">
                        <div className="content">
                            {videos}
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="mdl-video list-margin-0">
                        <div className="content">
                            {videos}
                        </div>
                    </div>
                );
            }


        } else {
            return (
                <div className="mdl-video">
                    <div className="title">
                        <a href={"/xem-them/"+this.props.boxId } className="txt">{this.props.title}</a>
                        <a href={"/xem-them/"+this.props.boxId} className="pull-right view-all"><i
                            className="fa fa-angle-right"></i></a>
                    </div>
                    <div className="content">
                        {videos}
                    </div>
                </div>
            );

        }

    }
});
var VideoUserFollowBox = React.createClass({
    getInitialState: function () {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
            defaultAvatarImage: "/images/avatar.gif"
        };
    },
    componentDidUpdate: function (prevProps, prevState) {
    },
    render: function () {
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                <div className="item">
                    <div className="place-holder-video">
                        <a className="link-holder"
                           href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>
                            <img src={this.props.videos[i]['coverImage']}
                                 onError={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 onloadstart={(e)=>{e.target.src=this.state.defaultImage16x9}}
                                 className="image" width="100%"/>
                        </a>
                        <div className="overlay">
                            <p><span className="duration">{this.props.videos[i]['duration'] }</span></p>
                        </div>
                    </div>
                    <p className="ellipsis text-name"><a
                        href={"/video/"+this.props.videos[i]['id'] +"/"+this.props.videos[i]['slug']}>{this.props.videos[i]['name'] }</a>
                    </p>
                </div>
            );
        }
        return (
            <div className="mdl-video">
                <div className="users">
                    <a href={"/trang-thanh-vien/"+this.props.userId} className="pull-right view-all">
                        <i className="fa fa-angle-right"></i></a>
                    <a href={"/trang-thanh-vien/"+this.props.userId}>
                        <img src={this.props.userAvatar}
                             onError={(e)=>{e.target.src=this.state.defaultAvatarImage}}
                             onloadstart={(e)=>{e.target.src=this.state.defaultAvatarImage}}
                             className="img-circle" width="30"/>
                        <span className="txt ellipsis">{this.props.userName}</span>
                    </a>
                </div>
                <div className="content">
                    {videos}
                </div>
            </div>

        );
    }
});

function loadVideoFollow() {
    ReactDOM.render(
        <VideoLazyPage url="/default/get-more-content?id=user_follow_video"
                       loadMoreUrl="/default/get-more-content?id=user_follow_video"
                       needActiveId="#tabs-video-users"/>,
        document.getElementById('tabs-video-users')
    );
}

function loadVideoNew() {
    ReactDOM.render(
        <VideoLazyPage url="/default/get-more-content?id=video_new"
                       loadMoreUrl="/default/get-more-content?id=video_new"
                       needActiveId="#tabs-video-new"/>,
        document.getElementById('tabs-video-new')
    );
}

function loadVideoOfCategory(id) {
    ReactDOM.render(
        <VideoLazyPage url={"/default/get-more-content?id="+id}
                       loadMoreUrl={"/default/get-more-content?id="+id}
                       needActiveId={"#"+id} />,
        document.getElementById(id)
    );
}