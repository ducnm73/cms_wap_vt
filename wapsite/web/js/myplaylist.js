/**
 * User: KhanhDQ
 * Date: 10/05/2017
 * Time: 04:49 PM
 */
var $body = $('main');

var MyPlaylist = function () {
    function getMyPlaylist() {
        $body.on('click','.btnLoadMyPlaylist',function() {
            if($('#userId').val()) {
                $.ajax({
                    method: "GET",
                    url: '/account/my-playlist',
                }).done(function(data) {       
                    try {
                        var objData = $.parseJSON(data);
                        if (objData.responseCode == 401) {
                            window.location = '/auth/login'
                        }    
                    } catch (err) {
                        if(data) {
                            $("#loadMyPlaylist").append(data); 
                            $('#playlistPop').fadeIn(200);    
                        } else {
                            $overlay.fadeOut(200);
                            toastr.info(trans('Bạn không có playlist. Hãy tạo mới.'), {closeButton: true})
                        }                           
                    } 
                }).fail(function( jqXHR, textStatus ) {
                    
                });      
            } else {
                window.location = '/auth/login' 
            }
        });               
    }

    function addVideoToPlaylist() {
        $body.on('click', '.addVideoPlaylist', function () {
            var playlistId = $(this).data('playlistid');
            var videoId = $('#idVideoClickOption').val();
            $.ajax({
                method: "POST",
                url: '/playlist/video-to-playlist',
                data: {
                    'id': playlistId,
                    'video_id': videoId,
                    'status': 1,
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                },
            }).done(function(response) {
                if(response.responseCode == 200) {
                    if(response.data.isAdd == true) {
                        toastr.success(trans('Thêm video thành công vào playlist.'), {closeButton: true})
                    } else {
                        toastr.error(response.message, {closeButton: true})
                    }
                    $('#playlistPop').fadeOut();
                    $('.overlay').fadeOut(200);
                    $('body main').removeClass('disScroll');
                } else {
                    toastr.error(response.message, {closeButton: true})
                }
            }).fail(function( jqXHR, textStatus ) {
                
            }); 
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            getMyPlaylist();
            addVideoToPlaylist();
        }
    };
}();

$(document).ready(function () {
    MyPlaylist.init();
});
