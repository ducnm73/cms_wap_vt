/**
 * User: KhanhDQ
 * Date: 9/28/2017
 * Time: 04:49 PM
 */
var imageExtensions = [".jpg", ".jpeg", ".gif", ".png"];
var videoExtensions = [".mp4"];

function canUseWebP() {
    var elem = document.createElement('canvas');

    if (!!(elem.getContext && elem.getContext('2d'))) {
        // was able or not to get WebP representation
        return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
    }
    else {
        // very old browser like IE 8, canvas not supported
        return false;
    }
}

var isSupportWebp = canUseWebP();

toastr.options.showDuration = 1000;
toastr.options.timeOut = 2000;

toastr.options.positionClass = 'toast-top-center'


$(document).ready(function () {

    scrollToPlay();

    $(".registerAdList").on("click", function(){
        $('#regSuggestion').fadeOut();$('#confirmRegSuggestion').fadeIn();
        $('#suggest-package img').attr("src", $(this).data('content') );
        $('#video-name').attr("src", $(this).data('text') );
    });

    $("#activate-form").submit(function () {
        var activateCode = $("#activateform-code").val().trim();
        if (activateCode.length > 50 || activateCode.length == 0) {
            alert(trans("Mã kích hoạt bắt buộc nhập, từ 1 đến 50 kí tự"))
            $("#activateform-code").focus();
            return false;
        }
        if (document.getElementById('activateform-verifycode')) {
            var captchaCode = $("#activateform-verifycode").val().trim();
            if (captchaCode.length == 0) {
                alert(trans("Mã captcha bắt buộc nhập"));
                $("#activateform-verifycode").focus();
                return false;
            }
        }
        return true;
    });

    //Lazy scroll image -----------
    processScroll();
    $(window).bind('scroll', processScroll);
    //---------------------

    loadCurrentTime();

    if (document.getElementsByClassName("mytooltip")) {

        setTimeout(() => {
            $(".tooltip").slideUp(2000);
        }, 10000);
    }

    //khoi tao datetimepicker

    if (document.getElementById("channelMenu")) {
        $('.chnlSlider').owlCarousel({
            nav: false,
            dots: false,
            loop: false,
            rows: true,
            rowsCount: 1,
            responsive: {
                0: {items: 3, autoWidth: true, margin: 5}
            }
        });
    }
    $(".ico-search-header").on("click", function () {
        search();
    });
    $(".remove-link-social").on("click", function () {

        var confirmRemove = confirm(trans("Bạn có chắc chắn xoá liên kết?"));
        if (confirmRemove) {
            var type = $(this).data("content");
            $.ajax({
                type: "GET",
                url: '/account/remove-link-social',
                data: {
                    'type': type
                },
                success: function (rawData) {
                    window.location.reload();
                }
            });
        }

    });

    $("#search-icon").on("click", function () {
        showPopup($("#searchPop"));
    });

    if (document.getElementById("google-adwords")) {
        addGAParams(".blkListVideosVer .ctnhld a");
        addGAParams(".blkListVideosVer .pthld a");
    }

    if (document.getElementById("is_not_owner")) {
        $("#btnDelete").hide();
    }

    if ($('#popupAlert .title').text()) {
        //showPopup($("#popupAlert"));
        if (document.getElementById('google_adword') || document.getElementById('register_confirm')) {
            $("#popupAlert").show();
        } else {
            showValidateMsg($('#popupAlert .title').text());
        }
    }

    if (iOSAndroid() && $('#inviteBox').length && getCookie("hideInviteBox") != 1) {
        //open box
        setTimeout(function () {
            $("#inviteBox").slideDown(200);
            $("main:first").addClass('padTop');
            setTimeout(function () {
                $("#inviteBox").slideUp(200);
                $("main:first").removeClass('padTop');
            }, 10000);

        }, 2000);
    }

    var lastTouchEnd = 0;

    document.addEventListener('touchend', function (event) {
        var now = (new Date()).getTime();
        if (now - lastTouchEnd <= 300) {
            //event.preventDefault();
        }
        lastTouchEnd = now;
    }, false);

    mobileDetect();

    setMinHeight();

    if (document.getElementById('ulMenu')) {
        loadMenu();
    }

    $("#file_video").on('change', function () {
        $("#file_video_name").html($('#file_video').val().replace(/.*(\/|\\)/, ''));
    })

    $("#file_thumbnail").on('change', function () {
        $("#file_thumbnail_name").html($('#file_thumbnail').val().replace(/.*(\/|\\)/, ''));
    })

    $("#file_avartar").on('change', function () {
        $("#file_avartar_name").html($('#file_avartar').val().replace(/.*(\/|\\)/, ''));
    })
    $("#file_banner").on('change', function () {
        $("#file_banner_name").html($('#file_banner').val().replace(/.*(\/|\\)/, ''));
    })
    $('#keyword').on("keydown", function (evt) {
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };
        var e = evt.which;
        if (e == keys.RETURN) {
            search();
        }
    });
    $('#keyword').on("keydown keypress keyup", function (evt) {
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };
        var e = evt.which;
        if (e == keys.ESC || e == keys.TAB || e == keys.LEFT || e == keys.UP || e == keys.RIGHT || e == keys.DOWN) { //  ESC
            return false;
        } else {
            $("#contentSuggess").show();
            delay(function () {
                autoSearch();
            }, 100);
        }
    });

    $('.modalPopup .okSubmit').click(function () {
        $(this).parent().submit();
    });

    //Search Channel
    $('#keyword_channel').on("keydown", function (evt) {
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };
        var e = evt.which;
        if (e == keys.RETURN) {
            searchChannel();
        }
    });
    $('#keyword_channel').on("keydown keypress keyup", function (evt) {
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };
        var e = evt.which;
        if (e == keys.ESC || e == keys.TAB || e == keys.LEFT || e == keys.UP || e == keys.RIGHT || e == keys.DOWN) { //  ESC
            return false;
        } else {
            delay(function () {
                autoSearchChannel();
            }, 100);
        }
    });

    $("body").on("click", ".add-channel-related", function () {

        var channel_id = $(this).data("id")
        $.ajax({
            type: "POST",
            url: '/channel/add-channel-related',
            dataType: 'json',
            data: {
                'channel_related_id': channel_id,
                '_csrf': $('meta[name="csrf-token"]').attr("content")

            },
            success: function (data) {
                if (data.responseCode == '200') {
                    showValidateMsg(trans('Thêm thành công'));
                    $('#channel_related_' + channel_id).remove();
                } else if (data.responseCode == '401') {
                    window.location = '/auth/login';
                } else {
                    showValidateMsg(data.message, 'error');
                }

            }
        });

    });

});


function validateUpload() {

    if ($('#file_video')[0].files.length === 0) {
        $('#file_video').focus();
        showValidateMsg(trans("Yêu cầu nhập file video"), 'error');
        return false;
    }

    if (!validateSingleInput(document.getElementById('file_video'), videoExtensions)) {
        $('#file_video').focus();
        showValidateMsg(trans("Định dang file video phải là mp4"), 'error');
        return false;
    }

    if ($('#title').val().trim().length == 0 || $('#title').val().trim().length > 255) {
        $('#title').focus();
        showValidateMsg(trans("Tiêu đề bắt buộc nhập và không được lớn hơn 255 kí tự"), 'error');
        return false;
    }

    if ($('#description').val().trim().length > 500) {
        $('#description').focus();
        showValidateMsg(trans("Mô tả không được lớn hơn 500 kí tự"), 'error');
        return false;
    }

    if (!validateSingleInput(document.getElementById('file_thumbnail'), imageExtensions)) {
        $('#file_thumbnail').focus();
        showValidateMsg(trans('Định dang file ảnh được phép')+": jpg, png, gif", 'error');
        return false;
    }

    $("#uploadForm").submit();
}

function showValidateMsg(msg, type) {
    // $("#popupMessage .clrGrey").html(msg);
    hideModal();
    if (type == 'error') {
        toastr.error(msg);
    } else {
        toastr.success(msg);
    }

    //showPopup($("#popupMessage"));

}

function showPopupNoUpload(msg) {

    $("#popupNoUpload .title").html(msg);
    showPopup($("#popupNoUpload"));

}

function validateSingleInput(oInput, validFileExtensions) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < validFileExtensions.length; j++) {
                var sCurExtension = validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                //alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function autoSearch() {
    var key = $('#keyword').val();
    var url = $('#urlSearch').val();
    $("#contentSuggess").show();
    var firstVODSperate = false;
    var firstFILMSperate = false;

    $("#main-result-search").hide();
    $("#main-suggestion-search").show();

    if (key.trim().length > 1) {
        $.ajax({
            url: url,
            data: {
                'query': key.trim(),
            },
            success: function (result) {
                var tmpHtml = "";
                for (var cc = 0; cc < result.data.length; cc++) {
                    for (var cc2 = 0; cc2 < result.data[cc].content.length; cc2++) {
                        if (result.data[cc]['type'] == 'VOD') {
                            if (!firstVODSperate) {
                                firstVODSperate = true;
                                tmpHtml += "<li><a class='color-black' style='font-weight: bold;'>"+ trans('VIDEO')+"</a></li>";
                            }
                            tmpHtml += "<li><a href='" + '/video/' + result.data[cc].content[cc2]['id'] + '/' + result.data[cc].content[cc2]['slug'] + "' >" +
                                result.data[cc].content[cc2]['name'] +
                                "</a> </li>";
                        } else {
                            if (!firstFILMSperate) {
                                firstFILMSperate = true;
                                tmpHtml += "<li><a class='color-black' style='font-weight: bold;'>"+ trans('KÊNH')+"</a></li>";
                            }
                            tmpHtml += "<li><a href='" + '/channel/' + result.data[cc].content[cc2]['id'] + "' > " +

                                result.data[cc].content[cc2]['name'] +
                                "</a> </li>";
                        }
                    }
                }
                if (tmpHtml.length == 0) {
                    tmpHtml += "<li style='color:black' class='ctn'> "+ trans('Không có dữ liệu với từ khóa tìm kiếm')+" </li>";
                }

                document.getElementById("contentSuggess").innerHTML = tmpHtml;
            }
        });
    } else {
        var tmpHtml = "";
        //tmpHtml += "<li> Nhập ít nhất 3 kí tự để thực hiện tìm kiếm </li>";
        document.getElementById("contentSuggess").innerHTML = tmpHtml;
    }

}

function search() {
    if ($('#keyword').val().trim().length < 3)
        return;
    window.location = "/default/search-result?query=" + $('#keyword').val();
}

function goSearch(textSearch) {
    window.location = "/default/search-result?query=" + textSearch;
}

function followChannel(channel_id, status) {
    if (!status) {
        status = 0;
    }
    /*
    if (status == 0) {
        if (!confirm("Bạn có thực sự muốn hủy theo dõi kênh này không?")) {
            return;
        }
    }
    */
    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': status,
            'notification_type': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")

        },
        success: function (data) {
            if (data.responseCode == '200') {
                if (status == 0) {
                    showValidateMsg(trans('Hủy theo dõi kênh thành công'));
                    if (document.getElementById("channel_btn_follow_" + channel_id)) {
                        $("#channel_btn_follow_" + channel_id).hide();
                        $("#channel_btn_unfollow_" + channel_id).show();
                    }
                } else {
                    showValidateMsg(trans('Đăng kí theo dõi kênh thành công'));
                    if (document.getElementById("channel_btn_follow_" + channel_id)) {
                        $("#channel_btn_follow_" + channel_id).show();
                        $("#channel_btn_unfollow_" + channel_id).hide();
                    }
                }
                $('#' + channel_id).remove();

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, 'error');
            }

        }
    });
}

function chooseContentFollowChannel(channel_id) {

    var data_id = $('#' + channel_id).attr('data-id');
    $.ajax({
        type: "POST",
        url: 'account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': data_id,
            'notification_type': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {
                console.log(data_id);

                var html = $('#' + channel_id).clone().wrap('<div>').parent().html();
                $('#' + channel_id).remove();
                if (data_id == 0) {
                    $('.recommendation .list4Items').append(html);
                    //$("#popupCommon .title").html('Hủy theo dõi kênh thành công');
                    showValidateMsg(data.message);

                    $('#' + channel_id).attr('data-id', 1);
                    $('#' + channel_id).removeClass('chose');
                } else {
                    $('.chose .list4Items').prepend(html);
                    //$("#popupCommon .title").html('Đăng kí theo dõi kênh thành công');
                    showValidateMsg(trans('Đăng kí theo dõi kênh thành công'));
                    $('#' + channel_id).attr('data-id', 0);
                    $('#' + channel_id).addClass('chose');
                }

                //showPopup($("#popupCommon"));

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                //$("#popupCommon .title").html(data.message);
                // showPopup($("#popupCommon"));
                showValidateMsg(data.message);
            }

        }
    });

}

function refreshCaptcha(id) {
    if (!id) {

        if (document.getElementById('getotpform-verifycode-image')) {
            id = 'getotpform-verifycode-image';
        } else if (document.getElementById('loginform-verifycode-image')) {
            id = 'loginform-verifycode-image';
        } else if (document.getElementById('mapaccountform-verifycode-image')) {
            id = 'mapaccountform-verifycode-image';
        }
    }

    $.ajax({
        type: "GET",
        url: '/auth/captcha?refresh'
    }).done(function (data) {
        $("#" + id).attr("src", data.url);
    });

}

$('.btnGetOTP').click(function () {
    window.location = '/account/get-otp';
});

function loadMenu() {


    $.ajax({
        url: '/account/get-follow-channel',
        dataType: 'json',
        data: {
            'include_hot': 1,
            'limit': 10
        },
        success: function (result) {
            console.log(result);

            if (result.responseCode == 200) {
                var strHtml = "";
                for (var ic = 0; ic < result.data.content.length; ic++) {
                    strHtml += '<li class="item" ><a href=' + '"/channel/' + result.data.content[ic].channel_id + '"' + '>' + charForLoopStrategy(result.data.content[ic].channel_name_mini) + ' </a></li> ';
                }
                $("#ulMenu").append(strHtml);

                if ($('.chnlSlider').length) {
                    $('.chnlSlider').owlCarousel({
                        nav: false,
                        dots: false,
                        loop: false,
                        rows: true,
                        rowsCount: 1,
                        responsive: {
                            0: {items: 4, autoWidth: true, margin: 5}
                        }
                    });
                }

            }
        }
    });

}

var charForLoopStrategy = function (unescapedString) {
    var i, character, escapedString = '';

    for (i = 0; i < unescapedString.length; i += 1) {
        character = unescapedString.charAt(i);
        switch (character) {
            case '<':
                escapedString += '&lt;';
                break;
            case '>':
                escapedString += '&gt;';
                break;
            case '&':
                escapedString += '&amp;';
                break;
            case '/':
                escapedString += '&#x2F;';
                break;
            case '"':
                escapedString += '&quot;';
                break;
            case "'":
                escapedString += '&#x27;';
                break;
            default:
                escapedString += character;
        }
    }

    return escapedString;

};

function setMinHeight() {
    var screenHeight = $(window).height();
    $(".blkMapAccount").css("min-height", screenHeight);
}

function validateEditChannel() {
    var minChannelNameLength = $("#min_length_name_channel").val();
    var maxChannelNameLength = $("#max_length_name_channel").val();
    var maxChannelDescriptionLength = $("#max_length_description_channel").val();
    var minChannelDescriptionLength = $("#min_length_description_channel").val();
    $("#error_message").html("");

    if (!validateSingleInput(document.getElementById('file_banner'), imageExtensions)) {
        $('#file_banner').focus();
        $("#error_message").html(trans('Định dang file ảnh được phép')+": jpg, png, gif", 'error');
        return false;
    }

    if (!validateSingleInput(document.getElementById('file_avartar'), imageExtensions)) {
        $('#file_avartar').focus();
        $("#error_message").html(trans('Định dang file ảnh được phép')+": jpg, png, gif", 'error');
        return false;
    }

    if ($('#name').val().trim().length < minChannelNameLength || $('#name').val().trim().length > maxChannelNameLength) {
        $('#name').focus();
        $("#error_message").html(trans('Tên nhập trong khoảng từ') + ' ' + minChannelNameLength + ' ' + trans('đến') + ' ' + maxChannelNameLength + ' ' + trans('kí tự'), 'error');
        return false;
    }

    if ($('#description').val().trim().length > maxChannelDescriptionLength || $('#description').val().trim().length < minChannelDescriptionLength) {
        $('#description').focus();
        // $("#error_message").html(trans('Mô tả không được lớn hơn') + maxChannelDescriptionLength + trans('kí tự'), 'error');
        $("#error_message").html(trans('Mô tả trong khoảng từ') + ' ' + minChannelDescriptionLength + ' ' + trans('đến') + ' ' + maxChannelDescriptionLength + ' ' + trans('kí tự'), 'error');
        return false;
    }

    $("#editChannel").submit();
}


function followChannelAndNotify(channel_id, status, notification_type, sendNotify) {
    if (!status) {
        status = 0;
    }


    if (!notification_type) {
        notification_type = 0;
    }

    if (!sendNotify) {
        sendNotify = true;
    }

    var sta = document.getElementById("status_" + channel_id);
    if (sta && sendNotify && status != 1) {
        status = $("#status_" + channel_id).val();
    }

    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': status,
            'notification_type': notification_type,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {

                if (status == 0) {
                    //$("#popupCommon .title").html('Hủy theo dõi kênh thành công');
                    //showValidateMsg('Hủy theo dõi kênh thành công');
                    $("#aFollow span").text(trans("Theo dõi"));
                    showValidateMsg(trans('Hủy theo dõi kênh thành công'));
                } else {
                    //$("#popupCommon .title").html('Đăng kí theo dõi kênh thành công');
                    //showValidateMsg('Đăng kí theo dõi kênh thành công');
                    $("#aFollow span").text(trans("Đã theo dõi"));
                    showValidateMsg(trans('Thành công'));
                }

                if (data.data.isFollow > 0) {
                    $("#status_" + channel_id).val(0);
                    $("#icon-follow").attr("class", "ico color0081FF");
                    $("#onOff").show();
                    if (notification_type > 0) {
                        $("#on").attr("class", "ico active");
                        $("#off").attr("class", "ico");
                    } else {
                        $("#on").attr("class", "ico");
                        $("#off").attr("class", "ico active");
                    }

                } else {
                    $("#status_" + channel_id).val(1);
                    $("#icon-follow").attr("class", "ico");

                    $("#on").attr("class", "ico");
                    $("#off").attr("class", "ico");
                }
                $("#num-follow").html(data.data.followCount + trans(" người theo dõi"));

                //showPopup($("#popupCommon"));

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                //$("#popupCommon .title").html(data.message);
                //showPopup($("#popupCommon"));
                showValidateMsg(data.message, 'error');
            }

        }
    });
}

function markNotifi(id, url) {

    var msisdn = $('#mapaccountform-msisdn').val();
    $.ajax({
        type: "POST",
        url: '/account/mark-notification',
        dataType: 'json',
        data: {
            'id': id,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            $("#" + id).removeClass("new");
            $("#" + id).addClass("old");
            console.log(data);
            if (url) {
                window.location = url;
            } else {
                return true;
            }

        }
    });
    setTimeout(function () {
    }, 30000);

}
;

function hideInviteBox() {
    $("#inviteBox").slideUp(200);
    setCookie("hideInviteBox", 1, 1);
    $("main:first").removeClass('padTop');
}

function mobileDetect() {
    if (typeof MobileDetect === "function") {

        var md = new MobileDetect(window.navigator.userAgent);

        if (md.os('iOS') == "iOS") {

            $(".linkStore").attr("href", $(".linkStore").attr("attr-ios"));
            // BLOCK ZOOM IPHONE6
            /*
            document.addEventListener('touchmove', function (event) {
                if (event.scale !== 1) {
                    event.preventDefault();
                }
            }, false);
            */
        } else {
            $(".linkStore").attr("href", $(".linkStore").attr("attr-android"));
        }
    }

    if (typeof MobileDetect === "function") {
        var md = new MobileDetect(window.navigator.userAgent);
        if (md.os('iOS') && md.version('iPhone') >= 8.0) {
            $("a.replace-href").each(function () {
                $(this).attr("href", "sms:" + $(this).data("number") + "&body=" + $(this).data("body"));
                $(this).click(function () {
                    hidePopup('#popupAlert');
                });
                $(this).removeAttr("data-target");
                $(this).removeClass("modalBtn");
            });

        } else if (md.os('iOS') && md.version('iPhone') < 8.0) {
            $("a.replace-href").each(function () {
                $(this).attr("href", "sms:" + $(this).data("number") + ";body=" + $(this).data("body"));
                $(this).removeAttr("data-target");
                $(this).removeClass("modalBtn");
                $(this).click(function () {
                    hidePopup('#popupAlert');
                });
            });

        } else if (md.mobile() && md.os('AndroidOS')) {
            $("a.replace-href").each(function () {
                $(this).attr("href", "sms:" + $(this).data("number") + "?body=" + $(this).data("body"));
                $(this).removeAttr("data-target");
                $(this).removeClass("modalBtn");
                $(this).click(function () {
                    hidePopup('#popupAlert');
                });
            });

        }
    }
}

function postFeedBack() {

    if ($('input[name="report"]:checked').length <= 0) {
        showValidateMsg(trans("Vui lòng chọn loại báo cáo lỗi"), 'error');
        return;
    }

    $.ajax({
        url: $('#postFeedbackUrl').val(),
        type: 'POST',
        data: {
            'content': $("#contentReport").val(),
            'type': $("#typeReport").val(),
            'item_id': $("#contentId").val(),
            'id': $("input[name=report]:checked").val(),
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (objData) {
            try {
                if (objData.responseCode == 401) {
                    window.location = '/auth/login'
                } else if (objData.responseCode == 444) {
                    showValidateMsg(objData.message, 'error');

                } else {
                    showValidateMsg(objData.message);
                }
            } catch (err) {
            }
        },
        error: function () {
            $("#popupCommon .title").html(trans("Hệ thống đang bận, quý khách vui lòng thử lại sau!"));
            showValidateMsg(trans("Hệ thống đang bận, quý khách vui lòng thử lại sau!"), 'error');
        }

    });
    $('input[name=report]').removeAttr('checked');
}

function removeKeyword() {
    if ($("#keyword").val().length > 0) {
        $("#keyword").val("");
    } else {
        hideModal();
    }
}

function showMe(target, checkObj) {
    if ($(checkObj).val() == "down") {
        $(target).show('slow');
        $(checkObj).val("up");
    } else {
        $(target).slideUp();
        $(checkObj).val("down");
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 86400000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


$.fn.ellipsis = function () {
    return this.each(function () {
        var el = $(this);

        if (el.css("overflow") == "hidden") {
            var text = el.html();
            var multiline = true;//el.hasClass('multiline');
            var t = $(this.cloneNode(true))
                .hide()
                .css('position', 'absolute')
                .css('overflow', 'visible')
                .width(multiline ? el.width() : 'auto')
                .height(multiline ? 'auto' : el.height());

            el.after(t);

            function height() {
                return t.height() > el.height() + 1;
            }
            ;

            function width() {
                return t.width() > el.width();
            }
            ;

            var func = multiline ? height : width;

            while (text.length > 0 && func()) {
                text = text.substr(0, text.length - 8);
                t.html(text + "...");
            }

            el.html(t.html());
            t.remove();
        }
    });
};

function iOSAndroid() {
    var result = false;
    if (typeof MobileDetect === "function") {

        var md = new MobileDetect(window.navigator.userAgent);
        if (md.os('iOS') == "iOS" || md.os('AndroidOS') == 'AndroidOS') {
            result = true;
        }
    }
    return result;
}

function hidePopup(id) {
    $(id).hide();
}

if ($('#search-video-offset').length > 0) {
    var searchVideoOffset = parseInt($('#search-video-offset').val());

    function loadSearchMoreVideo(query, obj) {
        var searchVideoLimit = parseInt($('#search-video-limit').val());
        $.ajax({
            type: "GET",
            url: '/default/search-more-video',
            data: {
                'query': query,
                'limit': searchVideoLimit,
                'offset': searchVideoOffset
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    searchVideoOffset = searchVideoOffset + searchVideoLimit;
                    $('#searchVideo').append(rawData);
                } else {
                    $(obj).hide();
                }
            }
        });
    }
}

if ($('#search-channel-offset').length > 0) {
    var searchChannelOffset = parseInt($('#search-channel-offset').val());

    function loadSearchMoreChannel(query, obj) {
        var searchChannelLimit = parseInt($('#search-channel-limit').val());
        $.ajax({
            type: "GET",
            url: '/default/search-more-channel',
            data: {
                'query': query,
                'limit': searchChannelLimit,
                'offset': searchChannelOffset
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    searchChannelOffset = searchChannelOffset + searchChannelLimit;
                    $('#searchChannel').append(rawData);
                } else {
                    $(obj).hide();
                }
            }
        });
    }
}

if ($('#category-video-offset').length > 0) {
    var categoryVideoOffset = [];
    var categoryVideoLimit = parseInt($('#category-video-limit').val());

    function loadCategoryMoreVideo(id, obj) {
        if (categoryVideoOffset[id] === undefined) {
            categoryVideoOffset[id] = parseInt($('#category-video-offset').val());
        }

        $.ajax({
            type: "GET",
            url: '/distribution/category-more-video',
            data: {
                'id': id,
                'limit': categoryVideoLimit,
                'offset': categoryVideoOffset[id]
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    categoryVideoOffset[id] = categoryVideoOffset[id] + categoryVideoLimit;
                    $('#cate-' + id).append(rawData);
                } else {
                    $(obj).hide();
                }
            }
        });
    }
}


if ($('#relatedBox').length > 0) {

    $.ajax({
        type: "GET",
        url: '/video/related-video-box',
        data: {
            'device_id': getCookie('device_id'),
            'id': $('#contentId').val(),
            'channel_id': $('#channelId').val(),
            'channel_name': $('#channel_name').val(),
            'video_name': $('#video_name').val(),
            'tag_name': $('#tag_name').val(),
            'category_id': $('#category_id').val(),
            'category_name': $('#category_name').val(),
            'category_parent_id': $('#category_parent_id').val()
        },
        success: function (rawData) {

            $('#relatedBox').html(rawData);

        }
    });
}


if ($('#showMoreReportUserUpload').length > 0) {

    var reportLimit = parseInt($('#reportLimit').val());

    function loadMoreReport() {

        $.ajax({
            type: "GET",
            url: '/account/load-more-report',
            data: {
                'limit': reportLimit,
                'offset': $('#reportUserUpload tr.display-report').length
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    $('#reportUserUpload').append(rawData);
                }

                if (($('#reportUserUpload tr.more-report').length % reportLimit) !== 0) {
                    $('#showMoreReportUserUpload').hide();
                }
            }
        });
    }
}


//--
function addGAParams(selector) {
    var addStrPopup1 = '?popup=1';
    var addStrPopup2 = '&popup=1';
    $(selector).each(function (index) {
        var url = this.href;
        if (url.indexOf("=") > 0) {
            //$(this).attr('href', url+addStrPopup2);
            this.href = url + addStrPopup2;
        } else {
            //$(this).attr('href', url+addStrPopup1);
            this.href = url + addStrPopup1;
        }
    });
}

function followChannelAndHide(channel_id, removeId) {

    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': 1,
            'notification_type': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {
                showValidateMsg(trans('Thành công'));
                $(removeId).parent().remove();
                if (!$("li[id^=channel_]").length) {
                    $("#rcmBlock").hide();
                }


            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, 'error');
            }

        }
    });
}

//-- Sua video cua toi
$("#edit_video_button").on("click", function () {
    if ($("#edit_video_name").val().length <= 0) {
        alert("Vui lòng nhập tên video");
        return false;
    }
    $("#edit_video_id").val($('#idVideoClickOption').val());
    $("#edit_video_form").submit();

});

// - Xoa video cua toi..
$("#delete_my_video_button").on("click", function () {
    var videoId = $('#idVideoClickOption').val();

    var cfOk = confirm(trans("Chắc chắn xóa video?"));
    if (!cfOk) {
        return;
    }

    //DeleteVideo
    $.ajax({
        type: "POST",
        url: '/default/delete-video',
        data: {
            'id': videoId,
            '_csrf': $('meta[name="csrf-token"]').attr("content")

        },
        dataType: 'json',
        success: function (data) {
            if (data.responseCode == 200) {
                $(".item-" + videoId).hide();
            }
        }
    });

});

function openEditVideo() {
    var videoId = $('#idVideoClickOption').val();
    $("#edit_video_name").val($(".item-" + videoId + " .title").text().trim());
    showPopup($("#editVideoModal"));

}

function removeChannelRelated(channel_id) {
    if (!confirm(trans("Xác nhận xóa kênh liên quan không?"))) {
        return;
    }
    $.ajax({
        type: "POST",
        url: '/channel/remove-channel-related',
        dataType: 'json',
        data: {
            'channel_related_id': channel_id,
            '_csrf': $('meta[name="csrf-token"]').attr("content")

        },
        success: function (data) {
            if (data.responseCode == '200') {
                showValidateMsg(trans('Xóa thành công'));
                $('#channel_related_' + channel_id).remove();
            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, 'error');
            }

        }
    });
}


function autoSearchChannel() {
    var key = $('#keyword_channel').val();

    if (key.trim().length > 1) {
        $.ajax({
            url: "/default/search-channel-related-suggestion",
            data: {
                'query': key.trim(),
            },
            success: function (result) {
                var tmpHtml = "";
                if (result)
                    for (var cc = 0; cc < result.data.length; cc++) {
                        for (var cc2 = 0; cc2 < result.data[cc].content.length; cc2++) {
                            if (result.data[cc]['type'] == 'VOD') {
                            } else {
                                tmpHtml += "<article class='chnlItem' id='channel_related_" + result.data[cc].content[cc2]['id'] + "'>\n" +
                                    "<div class='ctn'>\n" +
                                    "<a class='btn btnBlueS right add-channel-related' data-id='" + result.data[cc].content[cc2]['id'] + "'>Thêm kênh</a>\n" +
                                    "<div class='avatar'><a href='/channel/" + result.data[cc].content[cc2]['id'] + "'><img src='" + result.data[cc].content[cc2]['avatarImage'] + "' alt=''></a></div>\n" +
                                    "<div class='ctnhld'>\n" +
                                    "<h6 class='title'><a href='/channel/" + result.data[cc].content[cc2]['id'] + "'>" + result.data[cc].content[cc2]['name'] + "</a></h6>\n" +
                                    "<p class='txtInfo'>\n" +
                                    result.data[cc].content[cc2]['follow_count'] + " theo dõi <i class='dot'></i>" +
                                    result.data[cc].content[cc2]['video_count'] + " video\n" +
                                    "</p>\n" +
                                    "</div>\n" +
                                    "</div>\n" +
                                    "</article>";
                            }
                        }
                    }
                if (tmpHtml.length == 0) {
                    tmpHtml += "<li style='color:black' class='ctn'>"+ trans('Không có dữ liệu với từ khóa tìm kiếm') +" </li>";
                }

                document.getElementById("content_channel_suggess").innerHTML = tmpHtml;
            }
        });
    } else {
        var tmpHtml = "";
        //tmpHtml += "<li> Nhập ít nhất 3 kí tự để thực hiện tìm kiếm </li>";
        document.getElementById("content_channel_suggess").innerHTML = tmpHtml;
    }

}

function loadCurrentTime() {

    var idsArr = [];

    $(".load-bar").each(function () {
        if ($(this).data("content") > 0) {
            idsArr.push($(this).data("content"));
        }
        //remove class not check again
        $(this).removeClass("load-bar");
    });

    if (idsArr.length > 0) {
        $.ajax({
            type: "GET",
            url: '/account/get-duration-watched-by-ids',
            dataType: 'json',
            data: {
                'ids': idsArr.join(),
            },
            success: function (response) {

                if (response.responseCode == '200') {
                    for (var ii = 0; ii < response.data.histories.length; ii++) {

                        if (response.data.histories[ii]['duration'] > 0) {
                            var percentL = 100 * response.data.histories[ii]['time'] / (response.data.histories[ii]['duration'])
                            $("#playback-" + response.data.histories[ii]['itemId']).css("width", percentL + "%");
                        }

                    }
                }

            }
        });
    }

}

$(document).on("click", '#load-more-related-video', function (event) {
    loadMoreRelated();
});

function loadMoreRelated() {
    var videoId = $("#contentId").val();
    var limit = $("#related_limit").val();
    var offset = $("#related_offset").val();
    offset = parseInt(offset) + parseInt(limit);

    $.ajax({
        type: "GET",
        url: '/video/ajax-get-related-video?video_id=' + videoId + '&limit=' + limit + '&offset=' + offset + '&device_id=' + getCookie('device_id'),
        success: function (data) {
            if (data) {
                $(".blkListVideosVer").append(data);
                $("#related_offset").val(offset);
            } else {
                $("#load-more-related-video").hide();
            }
        }
    });
}

function checkImageExists(imageUrl, target, callBack, callBackFail) {

    if( target.hasClass("animate-running")){
        return false;
    }

    $(".animate-running").each(function(){
        $(this).removeClass("animate-running");
    });

    target.addClass("animate-running");

    if(!isSupportWebp){
        imageUrl = imageUrl.replace(".webp", ".gif");
    }
    var imageData = new Image();
    imageData.onload = function () {
        callBack(target);
    };
    imageData.onerror = function () {
        callBackFail(target);
    };
    imageData.src = imageUrl;
}

function replaceImage(target) {
    var tImg =target.data("display");
    if(!isSupportWebp){
        tImg = tImg.replace(".webp", ".gif");
    }
    target.attr("src",  tImg  );
}

function replaceImage2(target) {
    target.attr("src", target.data("src") );
}

function replaceImage3(target) {
    target.attr("src", "/images/16x9.gif");
}

function scrollToPlay() {
    var viewPortH = 300;
    function checkMedia() {

        var media = $('.animated-image');
        // Get current browser top and bottom
        var scrollTop = $(window).scrollTop() + viewPortH;
        var scrollBottom = $(window).scrollTop() + $(window).height() - viewPortH;

        media.each(function (index, el) {
            var yTopMedia = $(this).offset().top;
            var yBottomMedia = $(this).height() + yTopMedia;

            if (scrollTop < yBottomMedia && scrollBottom > yTopMedia) { //view explaination in `In brief` section above
                checkImageExists( $(this).data("display") , $(this),
                    replaceImage,
                    function () {
                    }
                );
            } else {
                checkImageExists($(this).data("src"),$(this),
                    replaceImage2,
                    replaceImage3
                );
            }
        });
        //}
    }
    $(document).on('scroll', checkMedia);
}

function loadImage($el, $fn) {
    $($el).attr('src', $($el).attr('data-src'));
    $($el).removeClass("lazy");
    $fn ? $fn() : null;
}

function elementInViewport($el) {

    if ($el === undefined) return false;
    var $rect = $el.getBoundingClientRect();
    return ($rect.top >= 0 && $rect.left >= 0 && $rect.top <= (window.innerHeight || document.documentElement.clientHeight)
    );
}

function processScroll() {
    var arrImg = $('img.lazy').toArray();
    $('img.lazy').each(function (i, img) {
        if (elementInViewport(img)) {
            loadImage(img, function () {
            });
            for (var icc = 0; icc < i; icc++) {
                loadImage(arrImg[icc], function () {
                });
            }
            if (i + 1 < arrImg.length) {
                loadImage(arrImg[i + 1], function () {
                });
            }
        }
    });
};

//-------------------------------------------------------
