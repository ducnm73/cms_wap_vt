"use strict";

/* @author KhanhDQ */
var HomePage = React.createClass({
    displayName: "HomePage",

    getInitialState: function getInitialState() {

        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: parseInt($("#getMoreOffset").val()),
            typeId: $("#load-more-id").val(),
            lockScroll: false,
            endOfData: false
        };
    },
    componentWillMount: function componentWillMount() {},
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
        loadCurrentTime();
		processScroll();
    },
    componentDidMount: function componentDidMount() {
        window.addEventListener('scroll', this.updateViewport, false);
        this.updateViewport();
		svg4everybody();
		
    },
    componentWillUnmount: function componentWillUnmount() {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function updateViewport() {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }

        if(this.props.forceLoad && this.state.offset==0){
            this.getInitialState();
        }

        // Dieu kien load DATA
        if ( (this.props.forceLoad && this.state.offset==0) ||  !this.state.isFirstTimeLoad && !this.state.lockScroll && document.body.scrollHeight < window.pageYOffset + window.innerHeight + 20) {
            this.setState({ lockScroll: true });
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                tryCount: 0,
                retryLimit: 3,
                data: { 'id': this.state.typeId, 'limit': this.state.limit, 'offset': this.state.offset },
                success: function (ajaxData) {
                    var videoList = ajaxData.data.content;
                    // Neu khong lay dc content thi STOP
                    if (videoList.length == 0) {
                        this.setState({ endOfData: true, lockScroll: false });
                        return;
                    }
                    var oldVideoList = this.state.appendVideos;
                    oldVideoList = oldVideoList.concat(videoList);
                    this.setState({ appendVideos: oldVideoList, lockScroll: false });

                    if (videoList.length > 0) {
                        this.setState({ offset: this.state.offset + videoList.length, lockScroll: false });
                    } else {
                        this.setState({ lockScroll: true });
                    }
                    if (videoList.length < this.state.limit) {
                        this.setState({ endOfData: true, lockScroll: false });
                        return;
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({ lockScroll: false });
                }.bind(this)
            });
        } else {
            this.state.isFirstTimeLoad = false;
        }
    },

    render: function render() {
        var rows = [];
        var serverAjax = this.state.appendVideos;
        if (serverAjax) {
            if(this.props.isLayout2){
                rows.push(React.createElement(NewsFeedBox2, { videos: serverAjax }));
            }else{
                rows.push(React.createElement(NewsFeedBox, { videos: serverAjax }));
            }
        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(React.createElement(
                "div",
                null,
                React.createElement("div", { className: "clear-10" }),
                React.createElement(
                    "div",
                    { className: "spinner" },
                    React.createElement(
                        "div",
                        { className: "quarter" },
                        React.createElement("div", { className: "circle" })
                    )
                ),
                React.createElement("div", { className: "clear-10" })
            ));
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            rows.push(React.createElement("div", { className: "clear-10" }));
        }

        return React.createElement(
            "div",
            null,
            rows
        );
    }
});

var NewsFeedBox = React.createClass({
    displayName: "NewsFeedBox",

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.gif",
            defaultImage4x4: "/images/avatar.gif"
        };
    },
    clickIcoMore: function clickIcoMore(event) {
        console.log('id info', event.currentTarget.getAttribute('data-id'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {},
    render: function render() {
        var _this = this;

        var videos = [];
        var videoFreeIcon = "";

        for (var i = 0; i < this.props.videos.length; i++) {
            if (this.props.videos[i]['price_play']  == 0) {
                videoFreeIcon = React.createElement("div",
                    {className: "attach-img"},
                    React.createElement("img",
                        {src: "/images/freeVideo.png" })
                );
            }
            videos.push(
                React.createElement(
                    "article", {className: "videoItem load-bar", "data-content": this.props.videos[i]['id'] },
                    React.createElement(
                        "div",
                        { className: "pthld" },

                        React.createElement(
                            "div",
                            { className: "playback" },
                            React.createElement(
                                "span",
                                { id: "playback-"+this.props.videos[i]['id'] }
                            )
                        ),

                        React.createElement(
                            "span",
                            { className: "timing" },
                            this.props.videos[i]['duration']
                        ),
                        React.createElement(
                            "a",
                            { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                            React.createElement("img", {
                                    className: "lazy animated-image",
                                    src: _this.state.defaultImage16x9,
                                    "data-src": this.props.videos[i]['coverImage'],
                                    "data-display": this.props.videos[i]['animationImage'],
                                    onError: function onError(e) {
                                        e.target.src = _this.state.defaultImage16x9;
                                    },
                            })
                        ),
                        videoFreeIcon
                    ),
                    React.createElement(
                        "div",
                        { className: "ctn" },
                        React.createElement(
                            "div",
                            { className: "avatar" },
                            React.createElement(
                                "a",
                                { href: "/channel/" + this.props.videos[i]['userId'], title: this.props.videos[i]['userName'] },
                                React.createElement("img", { src: this.props.videos[i]['userAvatarImage'],
                                    onError: function onError(e) {
                                        e.target.src = _this.state.defaultImage4x4;
                                    } 
                                })
                            )
                        ),
                        React.createElement(
                            "h6",
                            { className: "title" },
                            React.createElement(
                                "a",
                                { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                                this.props.videos[i]['name']
                            )
                        ),
                        React.createElement(
                            "p",
                            { className: "txtInfo" },
                            React.createElement(
                                "a",
                                { href: "/channel/" + this.props.videos[i]['userId'] },
                                this.props.videos[i]['userName'] + " "
                            ),
                            React.createElement(
                                "i",
                                { className: "dot" }
                            ),
                            React.createElement(
                                "a",
                                { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                                " " + this.props.videos[i]['play_times'] + trans(' lượt xem ')
                            )
                            ,
                            React.createElement(
                                "i",
                                { className: "dot" }
                            ),
                            React.createElement(
                                "a",
                                { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                                " " + this.props.videos[i]['publishedTime']
                            )

                        ),
                        React.createElement(
                            "a",
                            { className: "icoMore optMnBtn", 'data-sortorder':"asc", 'data-id':this.props.videos[i]['id'], 'data-url': this.props.videos[i]['linkSocial'] , 'data-target': "#videoOpt",'data-sortorder':"asc", 'data-status':this.props.videos[i]['status'], 'data-comment':this.props.videos[i]['can_comment'] },
                            React.createElement("span", { dangerouslySetInnerHTML: { __html: ' <svg class="ico"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../images/defs.svg#ico-more"></use></svg>' } })
                        )
                    )   
                )  
            );
        }
        return React.createElement(
            "div",
            { className: "loadMoreVideo" },
            videos
        );
    }
});



var NewsFeedBox2 = React.createClass({

    displayName: "NewsFeedBox2",

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.gif",
            defaultImage4x4: "/images/avatar.gif"
        };
    },
    clickIcoMore: function clickIcoMore(event) {
        console.log('id info', event.currentTarget.getAttribute('data-id'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {},
    render: function render() {
        var _this = this;

        var videos = [];
        var videoFreeIcon = "";

        for (var i = 0; i < this.props.videos.length; i++) {
            if (this.props.videos[i]['price_play']  == 0) {
                videoFreeIcon = React.createElement("div",
                    {className: "attach-img"},
                    React.createElement("img",
                        {src: "/images/freeVideo.png" })
                );
            }
            videos.push(React.createElement(
                "article",
                { className: "videoItem load-bar", "data-content": this.props.videos[i]['id'] },
                React.createElement(
                    "div",
                    { className: "pthld" },
                    React.createElement(
                        "div",
                        { className: "playback" },
                        React.createElement("span", { className: "width30" , id: "playback-"+this.props.videos[i]['id']  })
                    ),
                    React.createElement(
                        "span",
                        { className: "timing" },
                        this.props.videos[i]['duration']
                    ),
                    React.createElement(
                        "a",
                        { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                        React.createElement("img", {
                            className: "lazy",
                            src: _this.state.defaultImage16x9,
                            "data-src": this.props.videos[i]['coverImage'],
                            onError: function onError(e) {
                                e.target.src = _this.state.defaultImage16x9;
                            },
                        })
                    ),
                    videoFreeIcon
                ),
                React.createElement(
                    "div",
                    { className: "ctn width60" },
                    React.createElement(
                        "h6",
                        { className: "title" },
                        React.createElement(
                            "a",
                            { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                            this.props.videos[i]['name']
                        )
                    ),
                    React.createElement(
                        "p",
                        { className: "txtInfo" },
                        this.props.videos[i]['play_times'] + trans(' lượt xem '),
                        React.createElement("i", { className: "dot" }),
                        this.props.videos[i]['publishedTime']
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ctn ctnR width38" },
                    React.createElement(
                        "div",
                        { className: "avatar" },
                        React.createElement(
                            "a",
                            { href: "/channel/" + this.props.videos[i]['userId'] },
                            React.createElement("img", { src: this.props.videos[i]['userAvatarImage'],
                                onError: function onError(e) {
                                    e.target.src = _this.state.defaultImage4x4;
                                }
                            })
                        )
                    ),
                    React.createElement(
                        "a",
                        { href: "/channel/" + this.props.videos[i]['userId'] },
                        this.props.videos[i]['userName']
                    )
                )
            ));
        }
        return React.createElement(
            "div",
            { className: "loadMoreVideo blkCat" },
            videos
        );
    }
});




ReactDOM.render(React.createElement(HomePage, { url: "/", loadMoreUrl: "/default/get-more-content" }), document.getElementById('container'));

function loadHomeCategory(categoryId){
    //alert(categoryId);
    $("#root-tab").html('');
    
    $("#load-more-id").val("video_of_category_"+categoryId);
    $("#getMoreOffset").val(0);

    $(".li-cate").removeClass("selected");
    $("#li-cate-"+categoryId).addClass("selected");

	ReactDOM.unmountComponentAtNode(document.getElementById('container'));
	
    ReactDOM.render(React.createElement(HomePage, { isLayout2: false,  forceLoad: true ,   url: "/", loadMoreUrl: "/default/get-more-content" }), document.getElementById('container') );

    //PROCESS SHOW POPUP 20/10
    if(categoryId==2050 && !getCookie("hide2010Box")){
        setCookie("hide2010Box", 1, 1);
        showPopup($("#popUp2010"));
    }
}

function loadLang(lang){
    alert(lang);
    /*$("#root-tab").html('');

    $("#load-more-id").val("video_of_category_"+categoryId);
    $("#getMoreOffset").val(0);

    $(".li-cate").removeClass("selected");
    $("#li-cate-"+categoryId).addClass("selected");

    ReactDOM.unmountComponentAtNode(document.getElementById('container'));

    ReactDOM.render(React.createElement(HomePage, { isLayout2: false,  forceLoad: true ,   url: "/", loadMoreUrl: "/default/get-more-content" }), document.getElementById('container') );

    //PROCESS SHOW POPUP 20/10
    if(categoryId==2050 && !getCookie("hide2010Box")){
        setCookie("hide2010Box", 1, 1);
        showPopup($("#popUp2010"));
    }*/
}