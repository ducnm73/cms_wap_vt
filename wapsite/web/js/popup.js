var $body = $('main');

toastr.options.showDuration = 1000;
toastr.options.timeOut = 2000;

toastr.options.positionClass = 'toast-top-center'

$("#videoOpt .close").on('click', function () {
    $('#videoOpt').css('display', 'none');
});

$("#playlistOpt .btnDelete").on('click', function () {
    $('#playlistOpt').css('display', 'none');
});

$body.on('click', '.modal .close', function () {
    $('.modal').css('display', 'none');
    $('.overlay').css('display', 'none');
});

$body.on('click', '#createPlaylistModal .ok', function () {
    createPlaylist();
});

$body.on('submit', '#addPlaylistForm', function (ev) {
    ev.preventDefault(); // to stop the form from submitting
    /* Validations go here */
    createPlaylist(); // If all the validations succeeded
});

$body.on('keydown', '#editPlaylistForm', function (e) {
    var key = e.which;
    if (key == 13) {
// As ASCII code for ENTER key is "13"
        validateEditPlayslist(); // Submit form code
    }
});


$(".videoItem .optMnBtn_channel").on('click', function () {
    var id = $(this).attr('data-id');
    var EditLink = $(this).attr('data-url');
    $('#editChannelOpt li a').attr('href', EditLink);

});

function validateEditPlayslist() {

    $("#error_message").html("");
    if ($('#editName').val().trim().length == 0 || $('#editName').val().trim().length > 255) {

        $("#error_message").html(trans("Tên bắt buộc nhập và không được lớn hơn 255 kí tự"), 'error');
        return false;
    }
    if ($('#editDescription').val().trim().length > 500) {
        $("#error_message").html(trans("Mô tả không được lớn hơn 500 kí tự"), 'error');
        return false;
    }
    $('#editPlaylistModal').css('display', 'none');
    $("#editPlaylistForm").submit();
}

function createPlaylist() {
    var namePlaylist = $("input[name='namePlaylist']").val();
    var descriptionPlaylist = $("input[name='descriptionPlaylist']").val();

    $.ajax({
        type: "POST",
        url: '/playlist/create',
        dataType: 'json',
        data: {
            'name': namePlaylist,
            'description': descriptionPlaylist,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode != 401) {
                $overlay.fadeOut(200);
                $body.removeClass('disScroll');
                $('#createPlaylistModal').fadeOut(200);

                if (data.responseCode == 200) {
                    toastr.success(data.message, {closeButton: true});
                    $("#addPlaylistForm input").val("");

                    loadPlaylist();

                } else {
                    toastr.error(data.message, {closeButton: true});
                }

            } else if (data.responseCode == 401) {
                window.location = "/auth/login";
            }
        }
    });
}

function loadPlaylist() {
    if ($('#userId').val()) {
        $.ajax({
            method: "GET",
            url: '/account/my-playlist',
        }).done(function (data) {
            try {
                var objData = $.parseJSON(data);
                if (objData.responseCode == 401) {
                    window.location = '/auth/login'
                }
            } catch (err) {
                if (data) {
                    $("#loadMyPlaylist").html(data);
                } else {

                }
            }
        }).fail(function (jqXHR, textStatus) {

        });
    }
}


$(".btnDel").on('click', function () {
    var id = $("#idInt").val();
    if (confirm(trans('Bạn có chắc xóa playlist không?'))) {
        $.ajax({
            type: "POST",
            url: '/playlist/delete-playlist',
            dataType: "json",
            data: {
                'id': id,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
            success: function (data) {
                if (data.responseCode == 200) {
                    showValidateMsg(data.message);
                } else {
                    showValidateMsg(data.message, 'error');
                }

                if (data.responseCode == 200) {
                    setTimeout(function () {
                        window.location = "/danh-sach-playlist";
                    }, 2000);// delay 2s
                }
            }
        });
    }
});

$("#playlistOpt #btnDelete").on('click', function () {
    $('#playlistOpt').css('display', 'none');
    var id = $("#idInt").val();
    var videoId = document.getElementById("btnDelete").getAttribute("data-id");
    if (confirm(trans('Bạn có chắc xóa video khỏi playlist không?'))) {
        $.ajax({
            type: "POST",
            url: '/playlist/video-to-playlist',
            dataType: "json",
            data: {
                'id': id,
                'video_id': videoId,
                'status': 0,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
            success: function (data) {
                if (data.responseCode == 200) {
                    $(".item-" + videoId).remove();
                }
                if (data.responseCode == 200) {
                    showValidateMsg(data.message);
                } else {
                    showValidateMsg(data.message, 'error');
                }
            }
        });
    }
});


$body.on('click', '.blkListVideos .icoMore, .blkListVideosVer .icoMore, #shareDetail, .detailVideoBtnAddTo', function () {
	
    var id = $(this).attr("data-id");
    var url = $(this).data("url");
    $('#idVideoClickOption').val(id);
    $('#urlShare').val(url);
    var fbUrl = 'https://facebook.com/sharer/sharer.php?u=' + url;
    var ggUrl = 'https://plus.google.com/share?url=' + url;
    $('#popupShare .btnFbShare').attr('pref', fbUrl);
    $('#popupShare .btnGgShare').attr('pref', ggUrl);
    $('#popupShare .btnFbShare').attr('data-id', id);
    $('#popupShare .btnGgShare').attr('data-id', id);
	
	$('#popupShare .btnGgShare').attr('href', ggUrl);
	
	$.ajax({
        type: "GET",
        url: '/default/get-link-share',
        dataType: 'json',
        data: {
            'video_id': $(this).attr("data-id")
        },
        success: function (data) {
            if(data.shareLink){
               $('#popupShare .btnFbShare').attr('href', data.shareLink);
            }
        }
    });

    document.getElementById("btnDelete").setAttribute("data-id", id);
	
});

/*
$body.on('click', ".shareMe", function () {
    hideModal();
    //window.location= $(this).attr("pref");
    //Call ajax get Share link
    $.ajax({
        type: "GET",
        url: '/default/get-link-share',
        dataType: 'json',
        data: {
            'video_id': $(this).attr("data-id")
        },
        success: function (data) {
			
            if(data.shareLink){
                window.location.href = data.shareLink;
            }
        }
    });

});
*/

$body.on('click', "#videoOpt .btnSeeLater, #playlistOpt .btnSeeLater, #specialOpt .btnSeeLater", function () {
    var id = $('#idVideoClickOption').val();
    var status = 1;
    $.ajax({
        type: "POST",
        url: '/video/toggle-watch-later',
        dataType: 'json',
        data: {
            'id': id,
            'status': status,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            console.log(data);
            if (data.responseCode == 401) {
                window.location = "/auth/login";
            } else {
                $overlay.fadeOut(200);
                $body.removeClass('disScroll');
                $("#playlistOpt").fadeOut(200);
                $("#videoOpt").fadeOut(200);

                if (data.responseCode == 200) {
                    toastr.success(data.message, {closeButton: true});
                } else {
                    toastr.error(data.message, {closeButton: true});
                }

            }
        }
    });
});

$body.on('click', "#specialOpt .btnDelete", function () {

    var id = $('#idVideoClickOption').val();
    var status = 1;
    $.ajax({
        type: "POST",
        url: '/account/delete-history-view',
        dataType: 'json',
        data: {
            'ids': id,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            console.log(data);
            if (data.responseCode == 401) {
                window.location = "/auth/login";
            } else {
                if (data.responseCode == 200) {
                    showValidateMsg(data.message);
                } else {
                    showValidateMsg(data.message, 'error');
                }

                $('a[data-id=' + id + ']').parent().parent().hide();
            }
        }
    });
});

$("#cmtOpt .btnDelete").on('click', function () {
    var id = $('#idVideoClickOption').val();
    if (confirm(trans('Bạn có chắc xóa playlist không?'))) {
        $.ajax({
            type: "POST",
            url: '/playlist/delete-playlist',
            dataType: "json",
            data: {
                'id': id,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
            success: function (data) {
                if (data.responseCode == 200) {
                    $(".playlist-" + id).remove();
                }
                if (data.responseCode == 200) {
                    showValidateMsg(data.message);
                } else {
                    showValidateMsg(data.message, 'error');
                }

            }
        });
    }
});
$body.on('click', "#specialOpt .btnTriggercomment", function () {
    var videoId = $('#idVideoClickOption').val();
    $.ajax({
        method: "POST",
        url: '/default/trigger-comment',
        data: {
            'id': videoId,
            'status': 'auto',
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        dataType: 'json',
    }).done(function (data) {
        var type = "error";
        var text = "";
        if (data.responseCode == '200') {
            type = "info";
            if(data.status == 1){
                $('.optMnBtn[data-id="'+videoId+'"').data('comment',1);
            }else{
                $('.optMnBtn[data-id="'+videoId+'"').data('comment',0);
            }
        }
        $(".popover-more").hide();
        showValidateMsg(data.message, type);
    }).fail(function (jqXHR, textStatus) {

    });
});
// notification page
$body.on('click', '.blkNotification .notiOptBtn', function () {
    var id = $(this).attr("data-id");
    $('.blkNotification input[name="channel_id"]').val(id);
});

$body.on('click', "#notiOpt .btnOffNoti", function () {
    $('#notiOpt').css('display', 'none');
    var id = $('.blkNotification input[name="channel_id"]').val();
    followChannelAndNotify(id, 1, 0, false);
});


$('#optionNotification .ok').on('click', function () {
    $('#optionNotification').css('display', 'none');
    var value = document.querySelector('input[name=accept]:checked').value;
    var id = $("#channel_id").val();
    followChannelAndNotify(id, 1, value, false);
});


$('.btnEdit').on('click', function () {
    $('#cmtOpt').css('display', 'none');
    var id = $('#idVideoClickOption').val();
    $.ajax({
        type: "GET",
        url: '/playlist/get-detail-playlist',
        dataType: "json",
        data: {
            'id': id,
        },
        success: function (data) {
            if (data.responseCode == 200) {
                $("#idPlaylist").val(data.data.id);
                $("#editName").val(data.data.name);
                $("#editDescription").val(data.data.description);
                showPopup($("#editPlaylistModal"));
            } else {
                if (data.responseCode == 200) {
                    showValidateMsg(data.message);
                } else {
                    showValidateMsg(data.message, 'error');
                }
            }
        }
    });
});

$('#onOff').on('click', function (e) {

    var target = $(this).data('target');
    showOptMn($(target));
});

$body.on('click', "#specialOpt .btnRemoveViewLater", function () {

    var id = $('#idVideoClickOption').val();
    var status = 1;
    $.ajax({
        type: "POST",
        url: '/video/toggle-watch-later',
        dataType: 'json',
        data: {
            'id': id,
            'status': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            console.log(data);
            if (data.responseCode == 401) {
                window.location = "/auth/login";
            } else {
                if (data.responseCode == 200) {
                    showValidateMsg(data.message);
                } else {
                    showValidateMsg(data.message, 'error');
                }
                $('a[data-id=' + id + ']').parent().parent().hide();
            }
        }
    });
});

$( document ).ready(function() {
    $(document).on('click', '.optMnBtn', function(event) {
        var dataComment = $(this).data('comment');
        var dataStatus = $(this).data('status');
        if(dataComment == 1 && dataStatus == 2){
            $('#specialOpt .btnTriggercomment').show();
            $('#specialOpt .btnTriggercomment span').text(trans('Tắt bình luận'));
        }else if(dataComment == 0 && dataStatus == 2){
            $('#specialOpt .btnTriggercomment').show();
            $('#specialOpt .btnTriggercomment span').text(trans('Bật bình luận'));
        }else{
            $('#specialOpt .btnTriggercomment').hide();
        }
    });
});