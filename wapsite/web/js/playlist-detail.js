Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
var shufflePlaylist = false;
var repeatPlaylist = false;
var playlistOrder = [], qLevels = [], playlistPlayedOrder = [];
var alreadyChange = true;
var currentPlay = 0;

//Playlist
$(function () {
    $('.vjs-playlist-delete-button').click(function (event) {
        event.stopPropagation();
        alert($(this).data('delete-url'));
        // Do something
    });

    var widthPickture = $('picture:first').width();
    $('time').css('left', (widthPickture - 44) + 'px');
    $('time').css('bottom', '0px');

    $('#shuffleBtn').click(function (event) {
        shufflePlaylist = !shufflePlaylist;
        if (shufflePlaylist) {
            showValidateMsg(trans('Bật chế độ phát ngẫu nhiên'));
            $("#shuffleBtn > img").attr("src", "/images/shuffle.svg");
        } else {
            showValidateMsg(trans('Tắt chế độ phát ngẫu nhiên'));
            $("#shuffleBtn > img").attr("src", "/images/shuffle_deactive.svg");
        }
    });

    $('#repeatBtn').click(function (event) {
        repeatPlaylist = !repeatPlaylist;
        player_playlist.playlist.repeat(repeatPlaylist);
        if (repeatPlaylist) {
            $('#repeatBtn').addClass('active');
            showValidateMsg(trans('Bật chế độ phát lặp lại'));
            $("#repeatBtn > img").attr("src", "/images/repeat.svg");
        } else {
            $('#repeatBtn').removeClass('active');
            showValidateMsg(trans('Tắt chế độ phát lặp lại'));
            $("#repeatBtn > img").attr("src", "/images/repeat_deactive.svg");
        }
    });
});

function shuffleArr(arra1) {
    var ctr = arra1.length, temp, index;

    // While there are elements in the array
    while (ctr > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * ctr);
        // Decrease ctr by 1
        ctr--;
        // And swap the last element with it
        temp = arra1[ctr];
        arra1[ctr] = arra1[index];
        arra1[index] = temp;
    }
    return arra1;
}


var player_playlist = videojs('video', {
    inactivityTimeout: 0,
    controls: true,
    autoplay: true,
    preload: 'auto',
    fluid: true,
    children: {
        loadingSpinner: {},
        bigPlayButton: {},
        controlBar: {
            playToggle: {},
            volumePanel: {},
            currentTimeDisplay: {},
            timeDivider: {},
            durationDisplay: {},
            remainingTimeDisplay: false,
            progressControl: {},
            subtitlesButton: false,
            playbackRateMenuButton: false,
            captionsButton: false,
            audioTrackButton: false,
            fullscreenToggle: {}
        }
    },
    html5: {
        nativeAudioTracks: false,
        nativeVideoTracks: false,
        hls: {
            debug: true,
            overrideNative: true
        }
    }
});

player_playlist.ready(function () {
    preparePlaylist();
});

function preparePlaylist() {
    currentPlay = 0;
    playlistOrder = [];
    for (var i = 0; i < player_playlist.playlist().length; i++) {
        playlistOrder.push(i);
    }
    playlistPlayedOrder = [];
}

var data = $.parseJSON($('#playerData').val());

//list
player_playlist.playlist(data);

// Initialize the playlist-ui plugin with no option (i.e. the defaults).
player_playlist.playlistUi();
player_playlist.playlist.autoadvance(0);

player_playlist.addClass('vjs-myclip');

player_playlist.on('playlistitem', function () {
    qLevels = [];
});

var videoIdsData = jQuery.parseJSON($('#videoIdsData').val());

player_playlist.on('loadstart', function () {
    playlistOrder.remove(videoIdsData[player_playlist.playlist.currentItem()]);
    console.log("Load start " + player_playlist.playlist.currentItem());
    $("#position_video").html( player_playlist.playlist.currentItem() +1 );
    loadVideoBox(videoIdsData[player_playlist.playlist.currentItem()]);

});

player_playlist.on('ended', function () {
    if (shufflePlaylist) {
        if (playlistOrder.length === 0 && !repeatPlaylist) {
            player_playlist.reset();
            return;
        }
        alreadyChange = true;
        playItem();
    }
});

player_playlist.on('error', function (event) {

    if (player_playlist.currentSrc()) {
        $.ajax({
            method: "GET",
            url: player_playlist.currentSrc()
        }).done(function (dataText) {
            showValidateMsg(dataText, 'error');

            setTimeout(function () {
                player_playlist.playlist.next();
            }, 5000); //1s

        }).fail(function (jqXHR, textStatus) {
            if (jqXHR.responseText) {
                showValidateMsg(jqXHR.responseText, 'error');
            } else {
                showValidateMsg(trans("Có lỗi xảy ra trong quá trình phát nội dung. Vui lòng thử lại sau."), 'error');
            }

            setTimeout(function () {
                player_playlist.playlist.next();
            }, 5000); //1s
        });
    }

});

player_playlist.qualityLevels().on('addqualitylevel', function (event) {
    var quality = event.qualityLevel;
    console.log(quality);

    if (quality.height != undefined && $.inArray(quality.height, qLevels) === -1) {
        quality.enabled = true;

        qLevels.push(quality.height);

        if (!$('.quality_ul').length) {
            var h = '<div class="quality_setting vjs-menu-button vjs-menu-button-popup vjs-control vjs-button">' +
                '<button class="vjs-menu-button vjs-menu-button-popup vjs-button" type="button" aria-live="polite" aria-disabled="false" title="Quality" aria-haspopup="true" aria-expanded="false">' +
                '<span aria-hidden="true" class="vjs-icon-placeholder vjs-icon-cog"></span>' +
                '<span class="vjs-control-text">Quality</span></button>' +
                '<div class="vjs-menu"><ul class="quality_ul vjs-menu-content" role="menu"></ul></div></div>';

            $(".vjs-fullscreen-control").before(h);
        } else {
            $('.quality_ul').empty();
        }

        qLevels.sort(function (a, b) {
            return b - a
        });
        qLevels.reverse();

        var j = 0;

        $.each(qLevels, function (i, val) {
            $(".quality_ul").append('<li class="vjs-menu-item" tabindex="' + i + '" role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="false" bitrate="' + val +
                '"><span class="vjs-menu-item-text">' + val + 'p</span></li>');
            j = i;
        });

        $(".quality_ul").append('<li class="vjs-menu-item vjs-selected" tabindex="' + (j + 1) + '" role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="true" bitrate="auto">' +
            '<span class="vjs-menu-item-text">Auto</span></li>');
    }
});

$("body").on("click", ".quality_ul li", function () {
    $(".quality_ul li").removeClass("vjs-selected");
    $(".quality_ul li").prop("aria-checked", "false");

    $(this).addClass("vjs-selected");
    $(this).prop("aria-checked", "true");

    var val = $(this).attr("bitrate");

    var qualityLevels = player_playlist.qualityLevels();

    for (var i = 0; i < qualityLevels.length; i++) {
        qualityLevels[i].enabled = (val == "auto" || (val != "auto" && qualityLevels[i].height == val));
    }
    $('.quality_setting .vjs-menu-button-popup').attr('aria-expanded', 'false');
    $('.quality_setting .vjs-menu').removeClass('vjs-lock-showing');
});

$("body").on("click", ".quality_setting .vjs-menu-button-popup", function () {
    var isShowed = $(this).attr('aria-expanded');
    if (typeof isShowed !== typeof undefined && isShowed !== false) {
        isShowed = !(isShowed.toUpperCase() === 'TRUE');
        $(this).attr('aria-expanded', isShowed);
        if (isShowed) {
            $('.quality_setting .vjs-menu').addClass('vjs-lock-showing');
        } else {
            $('.quality_setting .vjs-menu').removeClass('vjs-lock-showing');
        }
    }
});

function playItem() {
    if (shufflePlaylist && alreadyChange) {
        alreadyChange = false;
        if (playlistOrder.length === 0 && repeatPlaylist) {
            preparePlaylist();
        }
        if (playlistOrder.length) {
            var positionRandom = Math.floor(Math.random() * playlistOrder.length);
            playlistPlayedOrder.push(currentPlay);
            currentPlay = playlistOrder[positionRandom];
            player_playlist.playlist.currentItem(currentPlay);
        }
    }
}

function loadVideoBox($id) {

    $('#videoId').val($id);

    $.ajax({
        method: "GET",
        url: $('#videoBoxUrl').val(),
        data: {
            'id': $id
        }
    }).done(function (data) {
        $('#video-content').html(data);
        //showMoreDescription();
        //autoloadComment();

        //MyPlaylist.init();
        isAllData = false;
        Video.init();
        hasInit = true;

        $('.loadingBoxComment').show();

    }).fail(function (jqXHR, textStatus) {

    });

}