/**
 * User: TuongLV
 * Date: 10/05/2017
 * Time: 04:49 PM
 */

// load more notification page
var Notification = function () {

    var lockLoadingNotification = false;

    function loadMoreContent() {
        var notiLimit = $('.blkNotification input[name="notiLimit"]').val();
        var notiOffSet = $('.blkNotification input[name="notiOffset"]').val();
        $("#loading-notify").show();
        $.ajax({
            type: "GET",
            url: 'account/get-notification',
            dataType: 'json',
            data: {
                'limit': notiLimit,
                'offset': notiOffSet,
                'type': 1
            },
            success: function (objData) {
                var list = objData.data.data.notifications;
                if (list.length > 0) {
                    lockLoadingNotification = false;
                    $('.blkNotification input[name="notiOffset"]').val((parseInt(notiOffSet) + parseInt(notiLimit)));
                    appendNotification(list);
                }
                setTimeout(function(){
                    $("#loading-notify").hide();
                }, 3000);s
            }
        });

    }

    function autoLoadContent() {
        $(window).scroll(function (event) {
            if ((($(window).scrollTop() + $(window).height() + 20 >= $(document).height()) || $(window).height() == $(document).height()) && lockLoadingNotification == false) {
                lockLoadingNotification = true;
                $("#loading-notify").show();
                loadMoreContent();
            }
        });
        $(".blkNotification").bind('mousewheel', function(e){
            //Firefox
            if(e.originalEvent.detail > 0 && $(window).height() == $(document).height() && lockLoadingNotification == false) {
                lockLoadingNotification = true;
                $("#loading-notify").show();
                loadMoreContent();
            }
            //IE, Opera, Safari
            if(e.originalEvent.wheelDelta < 0 && $(window).height() == $(document).height() && lockLoadingNotification == false) {
                lockLoadingNotification = true;
                $("#loading-notify").show();
                loadMoreContent();
            }
        });
    }

    function appendNotification(list) {
        $("#loading-notify").hide();
        var html = '';
        $.each(list, function (key, value) {

            html += '<article id="noti-'+ value.record_id +'" class="notiItem ';

            html += '" ><div class="ctn"><div class="avatar left">' +
                '<a href="/binh-luan?view_comment='+ value.record_id +'&comment_id='+ value.last_comment +'&comment_status=0';
            html += '"><img src="';
            if (value.avatarImage == '') {
                html += '../images/16x9.png';
            } else {
                html += value.avatarImage;
            }
            html += '"/></a>';
            html += '</div><div class="pthld right"><a href="/binh-luan?view_comment='+ value.record_id +'&comment_id='+ value.last_comment +'&comment_status=0';
            html += '"><img src="';
            if (value.coverImage == '') {
                html += '../images/16x9.png';
            } else {
                html += value.coverImage;
            }
            html += `" onerror="this.onerror=null;this.src='/images/16x9.png'"></a>`;
            html += '</div><div class="ctnhld"><h6 class="title"><a onclick="return markNotifi('+ "'"+ value.record_id +   "','"+ "video/"+value.id + "'"+ ')"  href="javascript:void(0)"';
            html += '">';
            html += value.aps.alert;
            html += '</a></h6>';
            html += '<div class="txtInfo" title="'+value.sent_time+'">';
            html += value.sent_time_format;
            html += '</div></div><a class="icoMore optMnBtn notiOptBtn" data-target="#notiOpt" data-id="';
            html += value.channel_id;
            html += '"><svg class="ico"><use xlink:href="../images/defs.svg#ico-more"></use></svg></a></div></article>';
        });

        $('.blkNotification').append(html);

    }


    return {
        //main function to initiate the module
        init: function () {
            // loadMoreContent();
            autoLoadContent();
        }
    };
}();


$(document).ready(function () {
    Notification.init();
});