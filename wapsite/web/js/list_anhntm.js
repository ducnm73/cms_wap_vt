"use strict";

/* @author dungld5 */
var HomePage = React.createClass({
    displayName: "HomePage",

    getInitialState: function getInitialState() {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: parseInt($("#getMoreOffset").val()),
            typeId: $("#load-more-id").val(),
            type: $("#type").val(),
            alias: $("#alias").val(),
            lockScroll: false,
            endOfData: false
        };
    },
    componentWillMount: function componentWillMount() {
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {

    },
    componentDidMount: function componentDidMount() {
        window.addEventListener('scroll', this.updateViewport, false);
        this.updateViewport();
        svg4everybody();
    },
    componentWillUnmount: function componentWillUnmount() {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function updateViewport() {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Dieu kien load DATA
        if (!this.state.isFirstTimeLoad && !this.state.lockScroll && document.body.scrollHeight < window.pageYOffset + window.innerHeight + 20) {
            this.setState({lockScroll: true});
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                tryCount: 0,
                retryLimit: 3,
                data: {'id': this.state.typeId, 'limit': this.state.limit, 'offset': this.state.offset},
                success: function (ajaxData) {
                    var videoList = ajaxData.data.content;
                    // Neu khong lay dc content thi STOP
                    if (videoList.length == 0) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                    var oldVideoList = this.state.appendVideos;
                    oldVideoList = oldVideoList.concat(videoList);
                    this.setState({appendVideos: oldVideoList, lockScroll: false});

                    if (videoList.length > 0) {
                        this.setState({offset: this.state.offset + videoList.length, lockScroll: false});
                    } else {
                        this.setState({lockScroll: true});
                    }
                    if (videoList.length < this.state.limit) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({lockScroll: false});
                }.bind(this)
            });
        } else {
            this.state.isFirstTimeLoad = false;
        }
    },

    render: function render() {
        var rows = [];
        var serverAjax = this.state.appendVideos;
        if (serverAjax) {
            switch (this.state.type) {
                case "CHANNEL":
                    rows.push(React.createElement(ChannelBox, {videos: serverAjax}));
                    break;
                case "PLAYLIST":
                    rows.push(React.createElement(PlaylistBox, {videos: serverAjax, alias: this.state.alias}));
                    break;
                case "VOD":
                    rows.push(React.createElement(VideoBox, {videos: serverAjax, alias: this.state.alias}));
                    break;
            }
        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(React.createElement(
                "div",
                null,
                React.createElement("div", {className: "clear-10"}),
                React.createElement(
                    "div",
                    {className: "spinner"},
                    React.createElement(
                        "div",
                        {className: "quarter"},
                        React.createElement("div", {className: "circle"})
                    )
                ),
                React.createElement("div", {className: "clear-10"})
            ));
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            rows.push(React.createElement("div", {className: "clear-10"}));
        }

        return React.createElement(
            "div",
            null,
            rows
        );
    }
});

var ChannelBox = React.createClass({
    displayName: "ChannelBox",

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/4x4.gif"
        };
    },
    handleClick: function handleClick(e) {
        //update state, etc.
        followChannel(e.currentTarget.getAttribute('data-column'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {

        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            var channelId = this.props.videos[i]['channel_id'];
            videos.push(React.createElement(
                "article",
                {className: "userItem", id: this.props.videos[i]['channel_id']},
                React.createElement(
                    "div",
                    {className: "ctn"},
                    React.createElement(
                        "div",
                        {className: "avatar left"},
                        React.createElement(
                            "a",
                            {href: "channel/" + this.props.videos[i]['channel_id']},
                            React.createElement("img", {src: this.props.videos[i]['avatarImage'], alt: ""})
                        )
                    ),
                    React.createElement(
                        "div",
                        {className: "ctnhld"},
                        React.createElement(
                            "h6",
                            {className: "title"},
                            React.createElement(
                                "a",
                                {href: "channel/" + this.props.videos[i]['channel_id']},
                                " ",
                                this.props.videos[i]['channel_name'],
                                " "
                            )
                        ),
                        React.createElement(
                            "div",
                            {className: "txtInfo"},
                            this.props.videos[i]['num_video'],
                            " videos",
                            React.createElement("i", {className: "dot"}),
                            this.props.videos[i]['num_follow'],
                            " l\u01B0\u1EE3t theo d\xF5i"
                        )
                    ),
                    React.createElement(
                        "a",
                        {className: "icoMore btn btnOutlineBlueS right follow-btn-2", "data-column": channelId, onClick: this.handleClick},
                        React.createElement("span", {dangerouslySetInnerHTML: {__html: ' <svg class="ico"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../images/defs.svg#ico-follow"></use></svg><span>'+trans("Đã theo dõi)+'</span>'}}),


                    )
                )
            ));
        }

        return React.createElement(
            "div",
            {className: "mdl-video-3 list-margin-0"},
            React.createElement(
                "div",
                {className: "content"},
                videos
            )
        );
    }
});
var PlaylistBox = React.createClass({
    displayName: "playlistBox",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/avatar.gif"
        };
    },
    clickIcoMore: function clickIcoMore(event) {
        console.log('id info', event.currentTarget.getAttribute('data-id'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {
        var _this = this;
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                React.createElement(
                    "article",
                    {className: "videoItem playlist-" + this.props.videos[i]['id']},
                    React.createElement("div",
                        {className: "ctn"},
                        React.createElement(
                            "div",
                            {className: "pthld"},
                            React.createElement(
                                "a",
                                {href: "/playlist/chi-tiet-playlist/" + this.props.videos[i]['id']},
                                React.createElement("div",
                                    {className: "plOverlay alCenter"},
                                    React.createElement("p",
                                        {className: ""},
                                        this.props.videos[i]['num_video']
                                    ),
                                    React.createElement(
                                        "svg",
                                        {className: "ico"},
                                        React.createElement(
                                            "use",
                                            {"href": "/images/defs.svg#ico-playlist",}
                                        )
                                    )
                                ),
                                React.createElement("img", {
                                    src: this.props.videos[i]['coverImage'],
                                    onError: function onError(e) {
                                        e.target.src = _this.state.defaultImage16x9;
                                    },
                                })
                            )
                        ),
                        React.createElement(
                            "div",
                            {className: "ctnhld"},
                            React.createElement(
                                "h6",
                                {className: "title"},
                                React.createElement(
                                    "a",
                                    {href: "/playlist/chi-tiet-playlist/" + this.props.videos[i]['id']},
                                    this.props.videos[i]['name']
                                )
                            ),
                            React.createElement(
                                "p",
                                {className: "txtInfo"},
                                this.props.videos[i]['num_video'] + " videos"
                            )
                        ),
                        React.createElement(
                            "a",
                            {
                                className: "icoMore optMnBtn",
                                'data-sortorder': "asc",
                                'data-id': this.props.videos[i]['id'],
                                'data-target': "#cmtOpt",
                                onClick: this.clickIcoMore
                            },
                            React.createElement("span", {dangerouslySetInnerHTML: {__html: ' <svg class="ico"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../images/defs.svg#ico-more"></use></svg>'}})
                        )
                    )
                )
            );
        }
        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});
var VideoBox = React.createClass({
    displayName: "VideoBox",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.png",
            defaultImage4x4: "/images/avatar.gif"
        };
    },
    clickIcoMore: function clickIcoMore(event) {
        console.log('id info', event.currentTarget.getAttribute('data-id'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {
        var _this = this;
        var videos = [];
        var detailLink = "";
        //dungld5 sua 15/3/2019
        var id =$("#load-more-id").val();

        for (var i = 0; i < this.props.videos.length; i++) {
            //if (this.props.videos[i]['status'] == 1) {
            if (this.props.videos[i]['status'] == 1 && id !="video_owner") {
                videos.push(
                    React.createElement(
                        "article",
                        {className: "videoItem"},
                        React.createElement("div",
                            {className: "ctn"},
                            React.createElement(
                                "div",
                                {className: "pthld"},
                                React.createElement(
                                    "a",
                                    { href: "javascript:void(0)" },
                                    React.createElement("img", {
                                        src: this.props.videos[i]['coverImage'],
                                        onError: function onError(e) {
                                            e.target.src = _this.state.defaultImage16x9;
                                        },
                                    })
                                )
                            ),
                            React.createElement(
                                "div",
                                {className: "ctnhld"},
                                React.createElement(
                                    "h6",
                                    {className: "title"},
                                    React.createElement(
                                        "a",
                                        {href: "javascript:void(0)"},
                                        this.props.videos[i]['name']
                                    )
                                ),
                                React.createElement(
                                    "p",
                                    {className: "txtInfo"},
                                    React.createElement(
                                        "span",
                                        {className: "ADDDS"},
                                        React.createElement(
                                            "a",
                                            {href: "javascript:void(0)"},
                                            trans("Chờ phê duyệt(chưa xem được)")
                                        )
                                    ),

                                    React.createElement(
                                        "i",
                                        {className: "dot"}
                                    ),
                                    React.createElement(
                                        "a",
                                        {className: "ADDDS2", href: "javascript:void(0)"},
                                        " " + this.props.videos[i]['publishedTime']
                                    )
                                )
                            )
                        )
                    ),
                    more
                );


            } //else if (this.props.videos[i]['status'] == 3) {
            else if (this.props.videos[i]['status'] == 3 && id !="video_owner") {
                videos.push(
                    React.createElement(
                        "article",
                        {className: "videoItem"},
                        React.createElement("div",
                            {className: "ctn"},
                            React.createElement(
                                "div",
                                {className: "pthld"},
                                React.createElement(
                                    "a",
                                    { href: "javascript:void(0)" },
                                    React.createElement("img", {
                                        src: this.props.videos[i]['coverImage'],
                                        onError: function onError(e) {
                                            e.target.src = _this.state.defaultImage16x9;
                                        },
                                    })
                                )
                            ),
                            React.createElement(
                                "div",
                                {className: "ctnhld"},
                                React.createElement(
                                    "h6",
                                    {className: "title"},
                                    React.createElement(
                                        "a",
                                        {href: "javascript:void(0)"},
                                        this.props.videos[i]['name']
                                    )
                                ),
                                React.createElement(
                                    "p",
                                    {className: "txtInfo"},
                                    React.createElement(
                                        "span",
                                        {className: "ADDDS"},
                                        React.createElement(
                                            "a",
                                            {href: "javascript:void(0)"},
                                            "Từ chối duyệt"  + ((this.props.videos[i]['reason'] != undefined) ? "(Lý do: " + this.props.videos[i]['reason'] + ")":"")
                                        )
                                    )
                                )
                            )
                        )
                    )
                );


            }else {
                videos.push(
                    React.createElement(
                        "article",
                        {className: "videoItem"},
                        React.createElement("div",
                            {className: "ctn"},
                            React.createElement(
                                "div",
                                {className: "pthld"},
                                React.createElement(
                                    "a",
                                    {href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug']},
                                    React.createElement("img", {
                                        src: this.props.videos[i]['coverImage'],
                                        onError: function onError(e) {
                                            e.target.src = _this.state.defaultImage16x9;
                                        },
                                    })
                                )
                            ),
                            React.createElement(
                                "div",
                                {className: "ctnhld"},
                                React.createElement(
                                    "h6",
                                    {className: "title"},
                                    React.createElement(
                                        "a",
                                        {href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug']},
                                        this.props.videos[i]['name']
                                    )
                                ),
                                React.createElement(
                                    "p",
                                    {className: "txtInfo"},
                                    React.createElement(
                                        "span",
                                        {className: "hideInfo1"},
                                        React.createElement(
                                            "a",
                                            {href: "/channel/" + this.props.videos[i]['userId']},
                                            this.props.videos[i]['userName'] + " "
                                        ),
                                        React.createElement(
                                            "i",
                                            {className: "dot"}
                                        )
                                    ),

                                    React.createElement(
                                        "a",
                                        {href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug']},
                                        " " + this.props.videos[i]['play_times'] + trans(' lượt xem ')
                                    ),
                                    React.createElement(
                                        "i",
                                        {className: "dot hideInfo3"}
                                    ),
                                    React.createElement(
                                        "a",
                                        {
                                            className: "hideInfo3",
                                            href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug']
                                        },
                                        " " + this.props.videos[i]['publishedTime']
                                    )
                                )
                            ),
                            React.createElement(
                                "a",
                                {
                                    className: "icoMore optMnBtn",
                                    'data-sortorder': "asc",
                                    'data-id': this.props.videos[i]['id'],
                                    'data-url': this.props.videos[i]['linkSocial'],
                                    'data-target': "#videoOpt",
                                    onClick: this.clickIcoMore
                                },
                                React.createElement("span", {dangerouslySetInnerHTML: {__html: ' <svg class="ico"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../images/defs.svg#ico-more"></use></svg>'}})
                            )
                        )
                    )
                );
            }

        }
        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});

ReactDOM.render(React.createElement(HomePage, {
    url: "/",
    loadMoreUrl: "/default/get-more-content"
}), document.getElementById('container'));