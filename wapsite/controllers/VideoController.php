<?php

namespace wapsite\controllers;

use common\helpers\Mobile_Detect;
use common\helpers\MobiTVRedisCache;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtConfigBase;
use common\models\VtGroupCategoryBase;
use common\models\VtPackageBase;
use common\models\VtSubBase;
use common\models\VtVideoBase;
use common\modules\v1\libs\ResponseCode;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\RecommendObj;
use common\modules\v2\libs\TrackingCode;
use common\modules\v2\libs\VideoObj;
use Yii;
use yii\helpers\Url;
use yii\web\UrlManager;

class VideoController extends \common\modules\v2\controllers\VideoController
{
    public function init()
    {
        parent::initWap(); // TODO: Change the autogenerated stub
    }

    public function behaviors()
    {
        return [];
    }

    public function actionDetail($playlist_id = '', $id)
    {

        $this->layout = "mainDetail.twig";
        $profileId = Yii::$app->session->get("vod_profile_id", false);
        //var_dump($profileId);die;
        $supportType = (Yii::$app->session->get("is_mp4_only", false)) ? S3Service::OBJCDN : S3Service::VODCDN;

        $acceptLossData = Yii::$app->getSession()->get("accept_loss_data", null);
   
        $responseData = parent::actionGetDetail($profileId, $supportType, $acceptLossData);


        if ($responseData['responseCode'] != 200) {
            $this->redirect('/default/error', 404);
        }
        //Kiem tra option hien thi popup GA(cho du noi dung dc xem mien phis van hien thi popup)
        $isShowPopup = Yii::$app->request->get('popup', 0);
        //Kiem tra hien thi popup GA (cho du noi dung dc xem mien phis van hien thi popup)
        if ($isShowPopup == 1) {
            if ($responseData['data']['detail']['suggest_package_id'] > 0) {
                //Kiem tra xem KH da dang ky goi cuoc hay chua? Neu da dang ky roi thi khong moi
                $objSub = VtSubBase::getOneSubByMsisdn($this->msisdn);
                if (!$objSub) {
                    $objPackage = VtPackageBase::getSuggestPackage($responseData['data']['detail']['suggest_package_id']);
                    $arrReplace = ['#videoName' => htmlspecialchars($responseData['data']['detail']['name'], ENT_QUOTES, 'UTF-8'), '#fee' => Utils::format_number($responseData['data']['detail']['price_play']), '#packageName' => htmlspecialchars($objPackage['name'], ENT_QUOTES, 'UTF-8'), '#cycle' => lcfirst(Utils::convertDay($objPackage['charge_range'])), '#subPrice' => Utils::format_number($objPackage['fee'])];
                    $popupGA = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.suggestion']['confirm']));
                    $confirmPopupGA = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['popup.suggestion']['confirm_register_sub']));

                }
            }
        }

        // Kiem tra video co thuoc playlist phim cu
        if (!$playlist_id) {
            $playlist_id = $responseData['data']['detail']['old_playlist_id'];
        }
        //Register metatag
        Yii::$app->view->title = $responseData['data']['detail']['name'];
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $responseData['data']['detail']['seo_description']
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $responseData['data']['detail']['seo_keywords']
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => $responseData['data']['detail']['name']
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => $responseData['data']['detail']['description']
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => $responseData['data']['detail']['coverImage']
        ]);


        Yii::$app->view->registerMetaTag([
            'property' => 'al:ios:app_store_id',
            'content' => 1186215150
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'al:ios:url',
            'content' => strtr('com.viettel.MClip://video?id=%video_id%&playlist_id=%playlist_id%', [
                '%video_id%' => $responseData['data']['detail']['id'],
                '%playlist_id%' => null
            ])
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'al:ios:app_name',
            'content' => 'MyClip – Clip Hot'
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'al:android:package',
            'content' => 'com.viettel.myclip'
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'al:android:url',
            'content' => strtr('com.viettel.myclip://video?id=%video_id%&playlist_id=%playlist_id%', [
                '%video_id%' => $responseData['data']['detail']['id'],
                '%playlist_id%' => null
            ])
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'al:android:app_name',
            'content' => 'MyClip – Clip Hot'
        ]);

        $storeUrl = '';
        $appStoreUrl = '';
        $isIOS = Yii::$app->session->get('is_iOS', 0);;

        if (!Yii::$app->session->has('deeplinkInvited')) {
            Yii::$app->session->set('deeplinkInvited', true);
            $detect = new Mobile_Detect();
            if ($detect->isiOS()) {
                $storeUrl = strtr('com.viettel.MClip://video?id=%video_id%&playlist_id=%playlist_id%', [
                    '%video_id%' => $responseData['data']['detail']['id'],
                    '%playlist_id%' => null
                ]);

                $appStoreUrl = VtConfigBase::getConfig('LINK_DOWNLOAD_IOS');
            } else if ($detect->isAndroidOS()) {
                $storeUrl = strtr('com.viettel.myclip://video?id=%video_id%&playlist_id=%playlist_id%', [
                    '%video_id%' => $responseData['data']['detail']['id'],
                    '%playlist_id%' => null
                ]);

                $appStoreUrl = VtConfigBase::getConfig('LINK_DOWNLOAD_ANDROID');
            }
        }

        $shareLink = \yii\helpers\Url::Base(true) . $_SERVER['REQUEST_URI'];

        $view = '@wapsite/views/video/detail.twig';
        if ($responseData['data']['detail']['type'] == 'FILM') {
            //$view = '@wapsite/views/film/detail.twig'; 
        }
        $showTime = null;
        if ($responseData['data']['detail']['show_times']) {
            $tmp = strtotime($responseData['data']['detail']['show_times']);
            $now = time();
            if ($now < $tmp) {
                $showTime = date("M j, Y H:i:s", $tmp);
            }
        }

        $trackingContent = RecommendObj::serialize(TrackingCode::view_video_detail, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['owner']['id'],
            'channel_name' => $responseData['data']['detail']['owner']['name'],
            'video_id' => $responseData['data']['detail']['id'],
            'video_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_views' => $responseData['data']['detail']['play_times'],
            'no_followers' => null,
            'no_like' => $responseData['data']['detail']['likeCount'],
            'no_dislike' => $responseData['data']['detail']['dislikeCount'],
            'no_share' => null,
            'no_report' => null,
            'no_comment' => null,
            'no_playlist' => null,
            'video_length' => $responseData['data']['detail']['duration'],
            'max_quality' => null,
            'created_at' => $responseData['data']['detail']['publishedTime']
        ]);

        $linkDetail = '/video/' . $id . '/' . $responseData['data']['detail']['slug'];

        return $this->render($view,
            [
                'responseData' => $responseData['data'],
                'shareLink' => $shareLink,
                'userId' => $this->userId,
                'msisdn' => Utils::hideMsisdn($this->msisdn),
                'showTime' => $showTime,
                'playlistId' => $playlist_id,
                'type' => $responseData['data']['detail']['type'],
                'avatarImage' => $this->avatarImage,
                'storeUrl' => $storeUrl,
                'appStoreUrl' => $appStoreUrl,
                'trackingContent' => $trackingContent,
                'popupGA' => $popupGA,
                'detailUrl' => $linkDetail,
                'isIOS' => $isIOS,
                'relatedLimit' => Yii::$app->params['app.video.relate.limit']
            ]
        );
    }


    public function actionRelatedVideoBox()
    {
        $this->layout = false;

        $responseData = parent::actionRelatedVideoBox();

        return $this->render('@app/views/partials/relatedVideoBox.twig', [
                'data' => $responseData['data']['content'],
                'auto' => true
            ]
        );
    }


    //Trang ban hang GA
    public function actionAd($id)
    {
        $limitRealted = 20;
        $id = trim(Yii::$app->request->get('id', 0));
        $packageId = trim(Yii::$app->request->get('package_id', 0));
        $source = trim(Yii::$app->request->get('utm_source', 0));

        $whitelistPackage = VtConfigBase::getConfig("GA_WHITELIST_PACKAGE", "1,2,3");
        if(strpos($whitelistPackage, $packageId)===false){
            Yii::$app->session->setFlash("info", Yii::t('wap','Mã gói cước không hợp lệ.'));
            return $this->redirect("/");
        }
        // xu ly session
        $session = \Yii::$app->session;
        $flashes = $session->getAllFlashes();
        $msisdn = $session->get('msisdn', 0);
        if ($msisdn) {
            $msisdn = Utils::getMobileNumber($session->get('msisdn', 0), Utils::MOBILE_SIMPLE);
        }

        $alertTypes = [
            'error' => 'alert-danger',
            'danger' => 'alert-danger',
            'success' => 'alert-success',
            'info' => 'alert-info',
            'warning' => 'alert-warning'
        ];

        $flashErrorCode = Yii::$app->session->getFlash("errorCode",9999);

        foreach ($flashes as $type => $data) {
            if (isset($alertTypes[$type])) {
                $data = (array)$data;
                foreach ($data as $i => $flash) {

                    list($message, $additionParam) = explode('|', $flash);

                    if ($additionParam == "REGISTER_CONFIRM") {
                        $smsCommand = VtConfigBase::getConfig('sms_command', "X");
                        $shortCode = VtConfigBase::getConfig('SHORTCODE',"1515");
                    }
                    if ($additionParam == "REGISTER_SUCCESS" || $additionParam =='REGISTER_SUCCESS_NOWAIT') {
                        Yii::$app->session->setFlash($type, $flash);
                        Yii::$app->session->setFlash("is_ad_register", 1);
                        Yii::$app->session->setFlash("redirect_link", "/video/".$id);
                        return $this->redirect("/ad-result/".$flashErrorCode);
                        //return $this->redirect("/video/".$id);
                    }
                    $flashStr = $this->render("@app/views/partials/alert.twig", ['message' => $message, 'addition_param' => $additionParam, 'msisdn' => $msisdn, "sms_command" => $smsCommand, "short_code"=>$shortCode]);
                }
                $session->removeFlash($type);
                // Lay ban ghi dau tien
                break;
            }
        }

        // Kiem tra xem khach hang co dang ky sub chua
        $objSub = VtSubBase::getOneSubByMsisdn($this->msisdn);
        if($objSub){
            return $this->redirect("/video/".$id);
        }

        //Kiem tra xem khach hang da dang ky SUB hay chua?
        $redisCache = Yii::$app->cache;
        $cacheKey = "AD_" . $id . "_" . $packageId."_".$source;
        $adwordDetail = $redisCache->get($cacheKey);

        if (!$adwordDetail) {
            $video = VtVideoBase::getDetail($id, "", false, true);

            $detailObj = array();
            $detailObj['id'] = $video['id'];
            $detailObj['name'] = $video['name'];

            $detailObj['coverImage'] = VtHelper::getThumbUrl($video['bucket'], $video['path'], VtHelper::SIZE_COVER);
            $detailObj['play_times'] = number_format($video['play_times'], 0, ',', '.');
            $detailObj['suggest_package_id'] = $video['suggest_package_id'];
            $detailObj['duration'] = Utils::durationToStr($video['duration']);

            $popupGA = VtConfigBase::getConfig("GA_MESSAGE_PACKAGE_" . $packageId, "");

            //Register metatag
            Yii::$app->view->title = $video['name'];
            Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $video['seo_description']
            ]);
            Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $video['seo_keywords']
            ]);
            Yii::$app->view->registerMetaTag([
                'property' => 'og:title',
                'content' => $video['name']
            ]);
            Yii::$app->view->registerMetaTag([
                'property' => 'og:description',
                'content' => $video['description']
            ]);
            Yii::$app->view->registerMetaTag([
                'property' => 'og:image',
                'content' => $video['coverImage']
            ]);

            $relateds = VideoObj::serialize(
                Obj::VIDEO_RELATE, VtVideoBase::getRelatedsGAQuery($video['category_id'], $limitRealted, 0, $id), false
            );

            $view = '@wapsite/views/video/adDetail.twig';
            $linkDetail = '/video/' . $id . '/' . $video['slug'];

            $sourceRegisterAsync = VtConfigBase::getConfig("SOURCE_REGISTER_ASYNC");
            $arrSource = explode(",",$sourceRegisterAsync);

            $adwordDetail = $this->render($view,
                [
                    'detail' => $detailObj,
                    'relateds' => $relateds,
                    'popupGA' => $popupGA,
                    'detailUrl' => $linkDetail,
                    'package_id' => $packageId,
                    'source' => $source,
                    'isRegisterAsync'=>(in_array($source, $arrSource))?1:0
                ]
            );

            $redisCache->set($cacheKey, $adwordDetail, MobiTVRedisCache::CACHE_30MINUTE);

        }

        //xu ly token
        $adwordDetail = str_replace("CSRF_TOKEN_VALUE",  Yii::$app->request->getCsrfToken(),$adwordDetail );

        return $adwordDetail . $flashStr;
    }


    public function actionToggleWatchLater()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        \Yii::$app->response->format = 'json';
        return parent::actionToggleWatchLater();
    }

    public function actionLikeVideo()
    {

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        \Yii::$app->response->format = 'json';
        if (!$this->isValidUser() && $this->isValidMsisdn()) {
            return [
                'responseCode' => ResponseCode::NOT_MEMBER,
                'message' => Yii::t('wap','Quý khách vui lòng đăng ký thành viên để sử dụng các tính năng dịch vụ')
            ];
        }
        return parent::actionToggleLikeVideo(); // TODO: Change the autogenerated stub
    }

    public function actionAjaxGetVideoStream($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = NULL)
    {
        \Yii::$app->response->format = 'json';

        $profileId = Yii::$app->session->get("vod_profile_id", false);
        $supportType = (Yii::$app->session->get("is_mp4_only", false)) ? S3Service::OBJCDN : S3Service::VODCDN;
        $acceptLossData = Yii::$app->getSession()->get("accept_loss_data", null);

        $responseData = parent::actionGetVideoStream($profileId, $supportType, $acceptLossData);

        if ($responseData['responseCode'] == ResponseCode::SUCCESS) {
            if ($responseData['data']['streams']['errorCode'] == ResponseCode::SUCCESS) {
                return [
                    'responseCode' => ResponseCode::SUCCESS,
                    'message' => ResponseCode::getMessage(ResponseCode::SUCCESS),
                    'data' => [
                        'urlStreaming' => $responseData['data']['streams']['urlStreaming']
                    ]
                ];
            }
        }
        return [
            'responseCode' => ResponseCode::UNSUCCESS,
            'message' => ResponseCode::getMessage(ResponseCode::UNSUCCESS),
            'data' => [
            ]
        ];

    }

    public function actionGetVideoStream($profileId = false, $supportType = S3Service::VODCDN, $acceptLossData = NULL)
    {

        $this->layout = false;
        $profileId = Yii::$app->session->get("vod_profile_id", false);
        $supportType = (Yii::$app->session->get("is_mp4_only", false)) ? S3Service::OBJCDN : S3Service::VODCDN;

        $acceptLossData = Yii::$app->getSession()->get("accept_loss_data", null);
        $id = Yii::$app->request->get('id', 0);
        $video = VtVideoBase::getDetail($id, "", false, false);

        //-- kiem tra ton tai video
        if (!$video) {
            Yii::$app->response->statusCode = 401;
            return $this->renderContent(Yii::t('wap','Nội dung không hợp lệ'));
        } else {
            $isShowTime = Utils::dateDiff($video['show_times']);
            // -- Neu show_time thi khong cho xem
            if ($isShowTime) {
                Yii::$app->response->statusCode = 401;
                return $this->renderContent(Yii::t('wap','Nội dung chưa đến giờ phát!'));
            }
        }

        $responseData = parent::actionGetVideoStream($profileId, $supportType, $acceptLossData);

        if ($responseData['responseCode'] == ResponseCode::SUCCESS) {
            if ($responseData['data']['streams']['errorCode'] == ResponseCode::SUCCESS) {
                return $this->redirect($responseData['data']['streams']['urlStreaming']);
            } else {
                Yii::$app->response->statusCode = 401;
                return $this->renderContent(Yii::t('wap','Nội dung không có quyền xem'));
            }
        } else {
            Yii::$app->response->statusCode = 401;
            return $this->renderContent(Yii::t('wap','Nội dung không có quyền xem'));
        }

    }


    public function actionDetailBox($playlist_id = '', $id)
    {
        $this->layout = false;
        $profileId = Yii::$app->session->get("vod_profile_id", false);
        $supportType = (Yii::$app->session->get("is_mp4_only", false)) ? S3Service::OBJCDN : S3Service::VODCDN;

        $acceptLossData = Yii::$app->getSession()->get("accept_loss_data", null);

        $responseData = parent::actionGetDetail($profileId, $supportType, $acceptLossData);

        if ($responseData['responseCode'] != 200) {
            $this->redirect('/default/error', 404);
        }
        // Kiem tra video co thuoc playlist phim cu
        if (!$playlist_id) {
            $playlist_id = $responseData['data']['detail']['old_playlist_id'];
        }
        //Register metatag
        Yii::$app->view->title = $responseData['data']['detail']['name'];
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $responseData['data']['detail']['seo_description']
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $responseData['data']['detail']['seo_keywords']
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'og:title',
            'content' => $responseData['data']['detail']['name']
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'og:description',
            'content' => $responseData['data']['detail']['description']
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'og:image',
            'content' => $responseData['data']['detail']['coverImage']
        ]);


        Yii::$app->view->registerMetaTag([
            'name' => 'al:ios:app_store_id',
            'content' => 1186215150
        ]);

        Yii::$app->view->registerMetaTag([
            'name' => 'al:ios:url',
            'content' => strtr('com.viettel.MClip://video?id=%video_id%&playlist_id=%playlist_id%', [
                '%video_id%' => $responseData['data']['detail']['id'],
                '%playlist_id%' => null
            ])
        ]);

        Yii::$app->view->registerMetaTag([
            'name' => 'al:ios:app_name',
            'content' => 'MyClip – Clip Hot'
        ]);

        Yii::$app->view->registerMetaTag([
            'name' => 'al:android:package',
            'content' => 'com.viettel.myclip'
        ]);

        Yii::$app->view->registerMetaTag([
            'name' => 'al:android:url',
            'content' => strtr('com.viettel.myclip://video?id=%video_id%&playlist_id=%playlist_id%', [
                '%video_id%' => $responseData['data']['detail']['id'],
                '%playlist_id%' => null
            ])
        ]);

        Yii::$app->view->registerMetaTag([
            'name' => 'al:android:app_name',
            'content' => 'MyClip – Clip Hot'
        ]);


        $shareLink = Yii::$app->params['cdn.site'] . $_SERVER['REQUEST_URI'];

        $showTime = null;
        if ($responseData['data']['detail']['show_times']) {
            $tmp = strtotime($responseData['data']['detail']['show_times']);
            $now = time();
            if ($now < $tmp) {
                $showTime = date("M j, Y H:i:s", $tmp);
            }
        }

        return $this->render('@wapsite/views/video/detailAjax.twig',
            ['videoId' => $id,
                'responseData' => $responseData['data'],
                'shareLink' => $shareLink,
                'userId' => $this->userId,
                'msisdn' => $this->msisdn,
                'showTime' => $showTime,
                'playlistId' => $playlist_id,
                'type' => $responseData['data']['detail']['type'],
                'avatarImage' => $this->avatarImage
            ]
        );
    }

    public function actionAjaxGetRelatedVideo()
    {
        $videoId = Yii::$app->getRequest()->get("video_id");
        $limit = Yii::$app->getRequest()->get("limit");
        $offset = Yii::$app->getRequest()->get("offset");

        $this->layout = false;
//        $defaultController = new \common\modules\v2\controllers\DefaultController();
        $relateds = \common\modules\v2\controllers\DefaultController::actionGetMoreContent(Obj::RELATED_OF_VIDEO . $videoId, $offset, $limit);

        return $this->render('@wapsite/views/video/relatedVideo.twig', [
                'data' => $relateds['data']['content'],
            ]
        );
    }

    public function actionEditVideo()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        $response = parent::actionEditVideo();
        Yii::$app->session->setFlash("info", $response["message"]);
        return $this->redirect(Yii::$app->request->referrer);
    }
    /**
     * Redirect after register package from AD page
     */
    public function actionAfterRegister(){
        $this->layout = "tiny.twig";
        $errorCode = Yii::$app->request->get('error_code');
        $message = Yii::$app->getSession()->getFlash('info');
        $isAdRegister = Yii::$app->getSession()->getFlash('is_ad_register');
        $redirectLink = Yii::$app->getSession()->getFlash('redirect_link','/');
        $arrMessage = explode("|", $message);

        return $this->render('@wapsite/views/default/afterRegister.twig', ['errorCode'=>$errorCode, 'message'=>$arrMessage[0], "redirectLink"=>$redirectLink] );

    }
}