<?php

/**
 * Created by PhpStorm.
 * User: KhanhDQ
 * Date: 9/26/2017
 * Time: 09:49 AM
 */

namespace wapsite\controllers;

use common\controllers\ApiController;
use common\helpers\Utils;
use common\libs\VtHelper;
use common\models\VtGroupCategoryBase;
use common\models\VtSubBase;
use common\models\VtUserFollowBase;
use common\models\VtVideoBase;
use common\models\VtVideoHotBase;
use common\modules\v1\libs\ResponseCode;
use common\modules\v2\libs\ChannelObj;
use common\modules\v2\libs\RecommendObj;
use common\modules\v2\libs\TrackingCode;
use common\modules\v2\libs\VideoObj;
use cp\models\VtSub;
use wap\models\VtPackage;
use Yii;
use yii\helpers\Url;
use yii\httpclient\Request;
use yii\web\Controller;
use yii\web\HttpException;
use common\modules\v2\libs\Obj;

class DistributionController extends \common\modules\v2\controllers\DefaultController
{

    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }

        $this->layout = "mainDistribution.twig";
        parent::initWap();
    }

    public function behaviors()
    {
        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }

    public function actionIndex()
    {
        //REGISTER METATAG
        Yii::$app->view->title = Yii::t('wap', "Mạng xã hội video - MyClip");
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => Yii::t('wap', 'MXH MyClip là Mạng xã hội video clip đầu tiên hàng đầu tại Việt Nam. Được phát triển bởi Tổng Công ty Viễn thông Viettel- một trong những nhà mạng Viễn thông hàng đầu thế giới. Dịch vụ cũng là MXH đầu tiên tại Việt Nam miễn phí Data 3G cho khách hàng sử dụng, mang lại những trải nghiệm tuyệt vời nhất về chất lượng dịch vụ và chất lượng nội dung cho người dụng')
        ]);

//        $trackingContent = RecommendObj::serialize(TrackingCode::view_home_page, $this->getUserTracking(), [
//            'show_recommendation' => false,
//        ]);

        $categories = VtGroupCategoryBase::getCategoryByIds($this->categoryIds);

        $data = [];
        foreach($categories as $category){
            $data[$category['id']] = VideoObj::serialize(
                Obj::VIDEO_NEW_CATE . "_" . $category['id'], VtVideoBase::getVideosByCate($category['id'], Yii::$app->params['app.distribution.showMore.limit'], 0, 'NEW'), true, $category['name']);
            $data[$category['id']]['categoryImage'] = VtHelper::getThumbUrl($category['bucket'], $category['path'], VtHelper::SIZE_CHANNEL_LOGO_LIST);
        }

        $popup = [];

        if(!Yii::$app->session->get('isShowPopupPromotion')) {
            $objSub = null;
            if (!empty($this->msisdn)) {
                $objSub = VtSub::getSub($this->msisdn);

                if (!$objSub) {
                    $objPackage = VtPackage::getDistributionPackage($this->distributionId);

                    if ($objPackage) {
                        $arrReplace = [
                            '#packageName' => htmlspecialchars($objPackage['name'], ENT_QUOTES, 'UTF-8'),
                            '#cycle' => lcfirst(Utils::convertDay($objPackage['charge_range'])),
                            '#subPrice' => Utils::format_number($objPackage['fee']),
                            '#subDomain' => $this->subDomain
                        ];

                        $popup['is_register_sub'] = 1;
                        $popup['confirm'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['distribution.popup.suggestion']['confirm']));
                        $popup['confirm_register_sub'] = str_replace(array_keys($arrReplace), array_values($arrReplace), Yii::t('web', Yii::$app->params['distribution.popup.suggestion']['confirm_register_sub']));
                        $popup['package_id'] = $objPackage['id'];
                    }
                }
            }
        }

        $msisdn = null;
        $sub = null;
        if ($this->msisdn) {
            $msisdn = Utils::hideMsisdn(Utils::getMobileNumber($this->msisdn, Utils::MOBILE_SIMPLE));
            $sub = VtSubBase::getOneSubByMsisdn($this->msisdn);
        }

        return $this->render('@wapsite/views/distribution/index.twig', [
            'data' => $data,
            'msisdn' => $msisdn,
            'popup' => $popup,
            'sub' => $sub,
            'offset' => Yii::$app->params['app.distribution.showMore.limit'],
            'limit' => Yii::$app->params['app.distribution.showMore.limit']
        ]);

    }


    public function actionCategoryMoreVideo()
    {
        $this->layout = false;
        $boxId = Yii::$app->request->get('id');
        $limit = Yii::$app->request->get('limit', Yii::$app->params['app.distribution.showMore.limit']);
        $offset = Yii::$app->request->get('offset', 0);

        $id = str_replace(Obj::VIDEO_NEW_CATE . "_", '', $boxId);

        $category = VtGroupCategoryBase::getActiveById($id);

        $data = VideoObj::serialize(
            Obj::VIDEO_NEW_CATE . $category['id'], VtVideoBase::getVideosByCate($category['id'], $limit, $offset, 'NEW'), false, $category['name']);
        $data['categoryImage'] = VtHelper::getThumbUrl($category['bucket'], $category['path'], VtHelper::SIZE_CHANNEL_LOGO_LIST);

        return $this->render('@wapsite/views/partials/videoSmallBox.twig', [
            'hideChannel' => true,
            'data' => $data,
            'hasTitle' => false,
            'hasPage' => false,
            'id' => $data['id'],
            'iconMore' => false
        ]);

    }
}
