<?php
namespace wapsite\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use wapsite\models\PasswordResetRequestForm;
use wapsite\models\ResetPasswordForm;
use wapsite\models\SignupForm;
use wapsite\models\ContactForm;

/**
 * Site controller
 */
class KpiController extends \common\modules\v2\controllers\KpiController
{
    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors()
    {
        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }

    public function actionInit()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionInit();
    }

    public function actionTrace()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionTrace();
    }

}
