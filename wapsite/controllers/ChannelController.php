<?php

namespace wapsite\controllers;

use common\modules\v1\libs\Obj;
use common\modules\v1\libs\ResponseCode;
use common\modules\v2\libs\RecommendObj;
use common\modules\v2\libs\TrackingCode;
use Yii;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ChannelController extends \common\modules\v2\controllers\ChannelController
{

    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors()
    {
        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }

    public function actionGetUserDetail()
    {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_DETAIL");
        $responseData = parent::actionGetUserDetail();

        $trackingContent = RecommendObj::serialize(TrackingCode::view_channel_home, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['id'],
            'channel_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_followers' => $responseData['data']['detail']['num_follow'],
            'no_videos' => $responseData['data']['detail']['num_video'],
            'created_at' => $responseData['data']['detail']['created_at'],
            'no_views' => $responseData['data']['detail']['totalViews']
        ]);

        return $this->render('@wapsite/views/channel/detailUser.twig', [
                'responseData' => $responseData,
                'title' => $responseData['data']['detail']['name'],
                'trackingContent' => $trackingContent,
            ]
        );
    }

    public function actionGetDetail()
    {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_DETAIL");
        $responseData = parent::actionGetDetail();

        $trackingContent = RecommendObj::serialize(TrackingCode::view_channel_home, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['id'],
            'channel_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_followers' => $responseData['data']['detail']['num_follow'],
            'no_videos' => $responseData['data']['detail']['num_video'],
            'created_at' => $responseData['data']['detail']['created_at'],
            'no_views' => $responseData['data']['detail']['totalViews']
        ]);
        if($this->userId == $responseData['data']['detail']['user_id']){
            $type = 'channel';
        }
        if($responseData['responseCode'] != ResponseCode::SUCCESS) {
            return $this->goHome();
        }
        return $this->render('@wapsite/views/channel/detailChannel.twig', [
                'responseData' => $responseData,
                'title' => $responseData['data']['detail']['name'],
                'trackingContent' => $trackingContent,
                'type' => $type
            ]
        );
    }

    // chi tiet user
    public function actionGetUserAbout()
    {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_ABOUT");
        $responseData = parent::actionGetUserDetail();

        $trackingContent = RecommendObj::serialize(TrackingCode::view_channel_intro, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['id'],
            'channel_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_followers' => $responseData['data']['detail']['num_follow'],
            'no_videos' => $responseData['data']['detail']['num_video'],
            'created_at' => $responseData['data']['detail']['created_at'],
            'no_views' => $responseData['data']['detail']['totalViews']
        ]);

        return $this->render('@wapsite/views/channel/about.twig', [
                'responseData' => $responseData,
                'title' => $responseData['data']['detail']['name'],
                'trackingContent' => $trackingContent,
                'type' => 'user'
            ]
        );
    }

    public function actionGetAbout()
    {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_ABOUT");
        $responseData = parent::actionGetDetail();

        $trackingContent = RecommendObj::serialize(TrackingCode::view_channel_intro, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['id'],
            'channel_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_followers' => $responseData['data']['detail']['num_follow'],
            'no_videos' => $responseData['data']['detail']['num_video'],
            'created_at' => $responseData['data']['detail']['created_at'],
            'no_views' => $responseData['data']['detail']['totalViews']
        ]);

        return $this->render('@wapsite/views/channel/about.twig', [
                'responseData' => $responseData,
                'title' => $responseData['data']['detail']['name'],
                'trackingContent' => $trackingContent
            ]
        );
    }

    public function actionUpdate()
    {

        $id = Yii::$app->request->get("id", 0);
        // if ($id != $this->userId) {

        //     Yii::$app->session->setFlash("info", Yii::t('wap','Không có quyền truy cập'));
        //     return $this->redirect("/ca-nhan");
        // }

        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_SHOW");

        if (!$this->isValidUser()) {
            $this->redirect('/auth/login');
        }
        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }

            $response = parent::actionUpdate();

            Yii::$app->session->setFlash('info', $response['message']);
            if ($response['responseCode'] == ResponseCode::SUCCESS) {

                $name = trim(Yii::$app->request->post('name', ''));
                return $this->redirect("/ca-nhan");
            } else {
                $id = trim(Yii::$app->request->post('id', $this->userId));
                return $this->redirect("/channel/update/" . $id);
            }

        } else {
            $responseData = parent::actionGetInfoChannel();

            if($responseData['responseCode'] == ResponseCode::NOT_FOUND) {
                return $this->goHome();
            }
        }

        return $this->render('@wapsite/views/channel/update.twig', [
            'responseData' => $responseData,
            'title' => Yii::t('wap','Chỉnh sửa kênh'),
            'type'  => "edit"
        ]);
    }

    public function actionGetListPlaylist()
    {

        $this->layout = 'main.twig';
        Yii::$app->meta->addDoctrineMetas("LIST_PLAYLIST");
        $id = Yii::$app->request->get('id');
        $detailChannel = parent::actionGetDetail();

        $limit = Yii::$app->params['app.home.limit'];
        $defaultController = new \common\modules\v2\controllers\DefaultController('get-more-content','default');
        $response = $defaultController->actionGetMoreContent(\common\modules\v2\libs\Obj::PLAYLIST_PUBLIC_OF_USER . $id, 0, $limit);

        return $this->render('listPlaylist.twig', ['title' => $detailChannel['data']['detail']['name'], 'response' => $response, "id" => \common\modules\v2\libs\Obj::PLAYLIST_PUBLIC_OF_USER . $id, "idPlaylist" => $id]);
    }

    public function actionGetChannelRecommend()
    {
        $responseData = parent::actionGetChannelRecommend();
        return $this->render("/channel/channelList.twig", ['responseData' => $responseData]);
    }

    public function actionChannelRelated()
    {
       $id = Yii::$app->request->get("channel_id");
        $responseData = parent::actionGetChannelRelated();

        return $this->render("/channel/channelRelated.twig", [ "id"=>$id, "userId"=>$this->userId, 'responseData' => $responseData, "isOwner" => ($this->userId == $id) ? true : false]);
    }

    public function actionRemoveChannelRelated()
    {
        $this->layout = false;
        Yii::$app->response->format = "json";
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        return parent::actionRemoveChannelRelated(); // TODO: Change the autogenerated stub
    }

    public function actionAddChannelRelated()
    {
        $this->layout = false;
        Yii::$app->response->format = "json";
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        return parent::actionAddChannelRelated(); // TODO: Change the autogenerated stub
    }

    public function actionSearchChannel()
    {
        if (!$this->userId) {
            return $this->redirect('/auth/login');
        }
        //$relatedByCategory = parent::actionGetChannelByCategory($this->userId);
        $relatedByCategory = parent::actionGetChannelRelated($this->userId);
        return $this->render("/channel/searchChannel.twig",["userId"=>$this->userId, 'channelRelated'=>$relatedByCategory,"isOwner"=>true]);
    }

    // edit user
    public function actionUserUpdate()
    {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_SHOW");

        if (!$this->isValidUser()) {
            $this->redirect('/auth/login');
        }
        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }

            $response = parent::actionUserUpdate();

            Yii::$app->session->setFlash('info', $response['message']);
            if ($response['responseCode'] == ResponseCode::SUCCESS) {

                $name = trim(Yii::$app->request->post('name', ''));
                $session = Yii::$app->session;
                $session->set('fullName', $name);
                $this->fullName = $session->get('fullName', '');

                return $this->redirect("/user/update/" . $this->userId);
            } else {
                return $this->redirect("/user/update/" . $this->userId);
            }

        } else {

            // thong tin chi tiet user
            $responseData = parent::actionGetUserUpdate();
            if($responseData['responseCode'] == ResponseCode::NOT_FOUND) {
                return $this->goHome();
            }
        }

        return $this->render('@wapsite/views/account/update.twig', [
            'responseData' => $responseData,
            'title' => Yii::t('wap','Chỉnh sửa người dùng')
        ]);
    }
    public function actionAdd()
    {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_SHOW");
        $responseData = [];
        if (!$this->isValidUser()) {
            $this->redirect('/auth/login');
        }

        if (Yii::$app->request->isPost) {

            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }
            $responseData = parent::actionInsert();

            Yii::$app->session->setFlash('info', $responseData['message']);
            if ($responseData['responseCode'] == ResponseCode::SUCCESS) {
                return $this->redirect("/channel/update/" . $responseData['id']);
            } else {
                return $this->redirect("/ca-nhan");
            }
        }

        return $this->render('@wapsite/views/channel/update.twig', [
            'responseData' => $responseData,
            'title' => Yii::t('wap','Thêm kênh'),
            'type'  => "add"
        ]);

    }

}
