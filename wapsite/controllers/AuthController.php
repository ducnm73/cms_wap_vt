<?php
/**
 * Created by PhpStorm.
 * User: Tuonglv
 * Date: 26/09/2017
 * Time: 10:24 AM
 */

namespace wapsite\controllers;

use common\helpers\Utils;
use common\libs\S3Service;
use common\models\VtUserBase;
use Yii;
use  common\libs\VtHelper;
use wapsite\models\LoginForm;
use common\helpers\MobiTVRedisCache;
use wapsite\models\ChangePasswordForm;
use yii\httpclient\Client;

class AuthController extends \common\modules\v1\controllers\AuthController
{
    public function init()
    {
        parent::initWap();
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * Logs in a user.
     * @author phumx@viettel.com.vn
     * @return mixed
     */
    public function actionLogin()
    {
        //var_dump($this->language);die;
        $this->language;
        $this->layout = 'mainLogin.twig';
        $showPopup = Yii::$app->request->get('popup', 0);
        if ($showPopup) {
            Yii::$app->session->setFlash('info', Yii::t('wap','Không nhận diện được thuê bao, Quý khách vui lòng truy cập bằng 3G của Viettel hoặc đăng nhập để sử dụng dịch vụ.'));
        }
        $session = Yii::$app->session;
        $redisCache = Yii::$app->cache;
        $backUrl = Yii::$app->request->get("backUrl", false);
        //var_dump($backUrl);die;//bool(false)
        if (!$backUrl) {
            $backUrl = $session->get("backUrl", false);
        }

        $referer = Yii::$app->request->referrer;
        //var_dump($referer);die;//string(19) "http://m.myclip.la/"
        if (!$backUrl || $referer) {
            if (($referer != Yii::$app->request->getAbsoluteUrl()) && $referer) {
                $session->set("backUrl", $referer);
            } else {
                $session->set("backUrl", "/");
            }
        }
        // neu co sdt hoac userId thi khong dang nhap nua
        if ($session->get("msisdn") || $session->get("userId")) {
            $this->goBack();
        }

        $ip = md5(VtHelper::getAgentIp());
        //var_dump($ip);die;//string(32) "aa914ffa2ef1c21e7ec0e7664a283ebe"
        $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
        $configCaptchaShow = \Yii::$app->params['login.captcha.show.count'];

        $model = new LoginForm(['countFail' => $countLoginFail, 'countCaptchaShow' => $configCaptchaShow]);
        //var_dump($model->load(Yii::$app->request->post()));die;//bool(false)
        if ($model->load(Yii::$app->request->post())) {
            //var_dump($model->load(Yii::$app->request->post()));die;
            $model->username = Utils::getMobileNumber(trim($model->username), Utils::MOBILE_GLOBAL);
            //var_dump($model->username);die;
            if ($model->validate()) {
                //var_dump($model);die;
                $redisCache = \Yii::$app->cache;
                $ip = md5(VtHelper::getAgentIp());

                $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
                $ipUser = md5(VtHelper::getAgentIp() . trim($model->username));
                $countLock = intval($redisCache->get('count_lock_' . $ipUser));

                //-- Kiem tra khoa tai khoan trong 10phut
                $isLockIPUser = $redisCache->get('lock_login_' . $ipUser);

                $configCountLock = \Yii::$app->params['login.lock.count'];

                if ($countLock >= $configCountLock || $isLockIPUser) {
                    if ($isLockIPUser) {
                        $redisCache->set('count_lock_' . $ipUser, 0, MobiTVRedisCache::CACHE_1DAY);
                    } else {
                        $redisCache->set('lock_login_' . $ipUser, 1, MobiTVRedisCache::CACHE_10MINUTE);
                    }
                    //$session->setFlash("error", 'Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!');
                    $model->addError('', Yii::t('wap','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!'));
                    return $this->render('login.php', [
                        'model' => $model,
                        'countLoginFail' => $countLoginFail,
                        'configCaptchaShow' => $configCaptchaShow
                    ]);
                }

                $user = $model->getUser();
                // Neu khong ton tai user
                // Yii::t('wap','Tài khoản hoặc mật khẩu đăng nhập không đúng')
                if (!$user) {
                    $model->addError('',Yii::t('wap','Tài khoản hoặc mật khẩu đăng nhập không đúng'));
                    $redisCache->set('count_lock_' . $ipUser, $countLock + 1, MobiTVRedisCache::CACHE_10MINUTE);
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                } elseif ($model->validatePassword()) {
                    $isBlacklist = false;
                    if ($isBlacklist) {
                        //$session->setFlash('error', 'Tài khoản của quý khách đã bị khóa. Vui lòng liên hệ tổng đài CSKH (19008198) để được hỗ trợ');
                        $model->addError('', Yii::t('wap','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!'));
                    } else {
                        // - Dang nhap thanh cong thi xoa lock
                        $redisCache->delete('count_login_fail_' . $ip);
                        $redisCache->delete('count_lock_' . $ipUser);
                        $session->set('msisdn', $user->msisdn);
                        $session->set('userId', $user->id);
                        $session->set('fullName', ($user['full_name']) ? $user['full_name'] : Utils::hideMsisdn($user['msisdn']));
                        $user->last_login = date("Y-m-d H:i:s");
                        $session->set('isShowSuggest', ($user->is_show_suggest == 1) ? 0 : 1);
                        session_regenerate_id(false);
                        $this->msisdn = $user->msisdn;

                        $session->set('avatarImage', ($user['bucket'] && $user['path']) ? VtHelper::getThumbUrl($user['bucket'], $user['path'], VtHelper::SIZE_AVATAR) : '');
                        $session->set('cpId', $user->cp_id);

                        if ($user->changed_password == 1) {
                            $session->set("backUrl", false);
                            return $this->redirect($backUrl);
                        } else {
                            $session->set("backUrl", false);
                            $session->set("needChangePassword", 1);
                            return $this->redirect('/auth/change-password');
                        }
                    }
                } else {
                    //$session->setFlash('error', 'Tài khoản hoặc mật khẩu đăng nhập không đúng');
                    $model->addError('', Yii::t('wap','Tài khoản hoặc mật khẩu đăng nhập không đúng'));

                    $redisCache->set('count_lock_' . $ipUser, $countLock + 1, MobiTVRedisCache::CACHE_10MINUTE);
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                }
            } else {

                $countLoginFail++;
                $redisCache->set('count_login_fail_' . $ip, $countLoginFail, MobiTVRedisCache::CACHE_1DAY);
            }
        }

        return $this->render('login.php', [
            'model' => $model,
            'countLoginFail' => $countLoginFail,//0
            'configCaptchaShow' => $configCaptchaShow//3
        ]);
    }

    public function actionChangePassword()
    {
        $isEmptyPassword = 0;

        $this->layout = 'main.twig';
        $session = Yii::$app->session;

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        if (!$session->get("msisdn", false)) {
            return Yii::$app->getResponse()->redirect('/auth/login');
        }
        // Kiem tra xem co mat khau chua?
        $userId = $session->get("userId", "");
        if ($userId) {
            $objUser = VtUserBase::getById($userId);

            if (!$objUser->password) {
                $session->set("isEmptyPassword", 1);
                $isEmptyPassword = 1;
            }
        }

        $model = new ChangePasswordForm();
        $model->username = $session->get("msisdn");

        if ($model->load(Yii::$app->request->post()))
            if ($model->validate()) {
                $user = $model->getUser();
                // Neu khong ton tai user
                if (!$user) {
                    $model->addError('', Yii::t('wap','Tài khoản hoặc mật khẩu đăng nhập không đúng'));
                } elseif ($model->validatePassword() || $isEmptyPassword == 1) {
                    $session->set("isEmptyPassword", 0);

                    $user->setPassword($model->newPassword);

                    $model = new ChangePasswordForm();
                    $model->addError('success', Yii::t('wap','Thay đổi mật khẩu thành công'));
                    $session->setFlash('success', Yii::t('wap','Thay đổi mật khẩu thành công'));
                    $session->set("needChangePassword", 0);
                    return $this->redirect("/");

                } else {
                    $model->addError('', Yii::t('wap','Mật khẩu cũ không đúng'));
                }
            }

        return $this->render('changePassword.twig', [
            'model' => $model,
            'layout' => $layout,
            'isEmptyPassword' => $isEmptyPassword,
            'title' => Yii::t('wap','Đổi Mật Khẩu'),
        ]);


    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionDetect()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $session->destroy();
        session_regenerate_id(true);
        session_commit();
        //- Mark customer has been logout
        Yii::$app->session->set("has_been_logout", 0);

        return $this->redirect("/auth/login");
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $session->destroy();
        session_regenerate_id(true);
        session_commit();
        //- Mark customer has been logout
        Yii::$app->session->set("has_been_logout", 1);

        return $this->goHome();
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
                'class' => 'common\helpers\CaptchaGenerate',
                'testLimit' => '1',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * @author PHUMX
     * callback sso
     * @param $client
     */
    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();

        $id = null;
        $fullName = '';
        $email = '';
        $imageUrl = '';
        $loginVia = '';
        if ($client instanceof \yii\authclient\clients\Google) {
            Yii::$app->getSession()->set("loginVia", "GOOGLE");
            $loginVia = "GOOGLE";
            $id = $attributes['id'];
            $fullName = $attributes['displayName'];
            $email = $attributes['emails'][0]['value'];
            $imageUrl = str_replace('sz=50', 'sz=200', $attributes['image']['url']);
        } elseif ($client instanceof \yii\authclient\clients\Facebook) {
            Yii::$app->getSession()->set("loginVia", "FACEBOOK");
            $loginVia ="FACEBOOK";
            $id = $attributes['id'];
            $fullName = $attributes['name'];
            $email = $attributes['email'];
            $imageUrl = $attributes['picture']['data']['url'];
        }

        if (isset($id) && $id) {
            $objUser = VtUserBase::getByOauthId($id);

            $client = new Client([
                'transport' => 'yii\httpclient\CurlTransport'
            ]);

            if (!$objUser) {
                $bucketName = '';
                $newPath = '';
                try {
                    $response = $client->createRequest()
                        ->setMethod('get')
                        ->setUrl($imageUrl)
                        ->setOptions([
                            CURLOPT_CONNECTTIMEOUT => 5, // connection timeout
                            CURLOPT_TIMEOUT => 30, // data receiving timeout
                        ])
                        ->send();
                    if ($response->isOk) {

                        $ext = 'jpg';
                        $tmpFile = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                        $fp = fopen($tmpFile, 'w');
                        fwrite($fp, $response->content);
                        fclose($fp);

                        $bucketName = Yii::$app->params['s3']['static.bucket'];
                        $newPath = Utils::generatePath($ext);

                        S3Service::putObject($bucketName, $tmpFile, $newPath);
                        VtHelper::generateAllThumb($bucketName, $tmpFile, $newPath);
                        unlink($tmpFile);

                    }
                } catch (\Exception $ex) {
                    \Yii::error('{OAUTH} Sync avatar image error: ' . $ex->getMessage(), 'error');
                }

                $objUser = new VtUserBase();
                $objUser->insertUserSocial($attributes['id'], $fullName, $email, $bucketName, $newPath, $loginVia);
                Yii::$app->getSession()->set("userId", $objUser->id);
                Yii::$app->getSession()->set("fullName", $objUser->full_name);
                Yii::$app->getSession()->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');

                $this->userId = $objUser->id;
                $this->msisdn = $objUser->msisdn;

                return $this->redirect('/account/get-otp');
            } else {

                Yii::$app->getSession()->set("userId", $objUser->id);
                Yii::$app->getSession()->set("msisdn", $objUser->msisdn);
                Yii::$app->getSession()->set("fullName", $objUser->full_name);
                Yii::$app->getSession()->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');

                $this->userId = $objUser->id;
                $this->msisdn = $objUser->msisdn;

                if(Yii::$app->session->get("NEED_LINK_ACCOUNT")){
                    Yii::$app->session->set("NEED_LINK_ACCOUNT",0);
                    return $this->redirect('/account/get-otp');
                }

                if ($objUser['is_show_suggest'] <= 0) {
                    $objUser->is_show_suggest = 1;
                    $objUser->save(false);
                    return $this->redirect('/');
                };
            }

        }

    }

    public function actionPushOtp($msisdn = '')
    {
        \Yii::$app->response->format = 'json';
        return parent::actionPushOtp();
    }

    /**
     * change theme dark - light mode
     * @author Manhdt
     */
    public function actionChangeTheme($theme = '')
    {
        \Yii::$app->response->format = 'json';
        return parent::actionThemeMsisdn($theme);
    }

}