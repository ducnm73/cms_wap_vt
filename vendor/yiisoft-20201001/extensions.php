<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-twig' => 
  array (
    'name' => 'yiisoft/yii2-twig',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/twig' => $vendorDir . '/yiisoft/yii2-twig',
    ),
  ),
  'nterms/yii2-pagesize-widget' => 
  array (
    'name' => 'nterms/yii2-pagesize-widget',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@nterms/pagesize' => $vendorDir . '/nterms/yii2-pagesize-widget',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.4.4.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.0.6.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-dynagrid' => 
  array (
    'name' => 'kartik-v/yii2-dynagrid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/dynagrid' => $vendorDir . '/kartik-v/yii2-dynagrid',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'sjaakp/yii2-taggable' => 
  array (
    'name' => 'sjaakp/yii2-taggable',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@sjaakp/taggable' => $vendorDir . '/sjaakp/yii2-taggable',
    ),
  ),
  '2amigos/yii2-ckeditor-widget' => 
  array (
    'name' => '2amigos/yii2-ckeditor-widget',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@dosamigos/ckeditor' => $vendorDir . '/2amigos/yii2-ckeditor-widget/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis',
    ),
  ),
  'nusoap/lib' => 
  array (
    'name' => 'dtsosie/nusoap',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@nusoap/lib' => $vendorDir . '/nusoap/lib',
    ),
  ),
  'bupy7/yii2-widget-cropbox' => 
  array (
    'name' => 'bupy7/yii2-widget-cropbox',
    'version' => '4.0.0.0',
    'alias' => 
    array (
      '@bupy7/cropbox' => $vendorDir . '/bupy7/yii2-widget-cropbox',
    ),
  ),
  'mdmsoft/yii2-captcha' => 
  array (
    'name' => 'mdmsoft/yii2-captcha',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@mdm/captcha' => $vendorDir . '/mdmsoft/yii2-captcha',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-elasticsearch' => 
  array (
    'name' => 'yiisoft/yii2-elasticsearch',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/elasticsearch' => $vendorDir . '/yiisoft/yii2-elasticsearch',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '2.4.0.0',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'kartik-v/yii2-date-range' => 
  array (
    'name' => 'kartik-v/yii2-date-range',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/daterange' => $vendorDir . '/kartik-v/yii2-date-range',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.2.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'cornernote/yii2-linkall' => 
  array (
    'name' => 'cornernote/yii2-linkall',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@cornernote/linkall' => $vendorDir . '/cornernote/yii2-linkall/src',
    ),
  ),
  'skeeks/yii2-assets-auto-compress' => 
  array (
    'name' => 'skeeks/yii2-assets-auto-compress',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@skeeks/yii2/assetsAuto' => $vendorDir . '/skeeks/yii2-assets-auto-compress',
    ),
  ),
  'yiisoft/yii2-mongodb' => 
  array (
    'name' => 'yiisoft/yii2-mongodb',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/mongodb' => $vendorDir . '/yiisoft/yii2-mongodb',
    ),
  ),
  'mickgeek/yii2-actionbar' => 
  array (
    'name' => 'mickgeek/yii2-actionbar',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@mickgeek/actionbar' => $vendorDir . '/mickgeek/yii2-actionbar',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '1.4.3.0',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'richardfan1126/yii2-sortable-gridview' => 
  array (
    'name' => 'richardfan1126/yii2-sortable-gridview',
    'version' => '0.0.5.0',
    'alias' => 
    array (
      '@richardfan/sortable' => $vendorDir . '/richardfan1126/yii2-sortable-gridview',
    ),
  ),
  'unclead/yii2-multiple-input' => 
  array (
    'name' => 'unclead/yii2-multiple-input',
    'version' => '2.12.0',
    'alias' => 
    array (
      '@unclead/multipleinput' => $vendorDir . '/unclead/yii2-multiple-input/src',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => '2.0.2',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform/src',
    ),
  ),
);
