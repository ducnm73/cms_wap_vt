<?php
return [
    'cache.enabled' => false,
    'data.sync.limit' => 1000,
    'auto.approve.filter.by.channel' => false,
    'auto.approve.crawler' => false,
    'auto.grant.outsource' => false,
];
