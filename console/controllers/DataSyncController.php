<?php

namespace console\controllers;

use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtCommentBase;
use common\models\VtConfigBase;
use common\models\VtMappingWaitApproveBase;
use common\models\VtPlaylistBase;
use common\models\VtUserBase;
use common\models\VtVideoBase;
use common\models\VtGroupCategoryBase;
use cp\models\VtConfig;
use yii\console\Controller;
use Yii;

class DataSyncController extends Controller
{
    /*public $approveVideo;

    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID), [
            'approveVideo',
        ]);
    }*/

    public function actionChangeApprove()
    {
        // \Yii::info('{START} changeApprove:approveVideo' . strval($this->approveVideo), 'console');
        \Yii::info('{START} changeApprove:approveVideo', 'console');
        set_time_limit(600);
        ini_set('memory_limit', '512M');
        $limit = Yii::$app->params['data.sync.limit'];

        $videos = VtVideoBase::getDataSynNeedApprove($limit);
        $lastId = \Yii::$app->redis->get('change-approve-last');

        if(!$lastId) {
            $lastId = 1;
        }

        $videos = VtVideoBase::find()
            ->where([
                'status' => VtVideoBase::STATUS_JUST_SYNC,
                'convert_status' => VtVideoBase::CONVERT_STATUS_SUCCESS
            ])
            ->andWhere(['not', ['syn_id' => null]])
            ->orderBy(['id' => SORT_ASC])
            ->limit($limit)
            ->all();
        \Yii::info('{TRACE} DataSynNeedApprove: ' . count($videos), 'console');
        $ownedVideos = array();
        $categoryIds = [];

        foreach ($videos as $video) {
            \Yii::info('{BeginVideo} changeApprove: ' . $video->id, 'console');
            $categoryIds[] = $video['category_id'];
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($video['path']);
            $storageType = Yii::$app->params['storage.type'];
                
            if($storageType == 1) {
                $result = S3Service::getObject($video['bucket'], $video['path'], $tempPath);

                if (empty($result) || $result['errorCode'] != S3Service::SUCCESS) {
                    \Yii::error('changeApprove: cannot get file from s3 server', 'console');
                    continue;
                }
            } else {
                $videoImagePath = Yii::$app->params['storage.san.rootPath'] . $video['path'];
                $result = copy($videoImagePath, $tempPath);

                if(!$result) {
                    \Yii::error("changeApprove: cannot copy file from path $videoImagePath", 'console');
                    continue;
                }
            }

            VtHelper::generateAllThumb($video['bucket'], $tempPath, $video['path']);

            if(!Yii::$app->params['auto.approve.crawler']){
                if(VtMappingWaitApproveBase::isWaitApprove($video['created_by'])) {
                    $status = VtVideoBase::STATUS_APPROVE;
                } else {
                    $status = VtVideoBase::STATUS_DRAFT;
                    if (Yii::$app->params['auto.grant.outsource']) {
                        $video->outsource_status = VtVideoBase::OUTSOURCE_DRAFT;
                        $video->outsource_need_review = VtVideoBase::OUTSOURCE_NEED_APPROVE;
                    }
                }
            } else {
                $status = VtVideoBase::STATUS_APPROVE;
            }

            if(isset($status)) {
                $video->status = $status;
            }

            $video->name = str_ireplace("%world cup%", "W.Cup", $video->name);
            $video->name_slug = Utils::removeSignOnly($video->name);
            $video->description_slug = Utils::removeSignOnly($video->description);

            $badWord = VtConfigBase::getConfig('data.sync.bad.words');
            if (preg_match_all("/\b(" . $badWord . ")\b/i", $video->name)) {
                $video->status = 3;
                $video->reason = 'Tự động từ chối do chứa từ khóa vi phạm bản quyền';
            }

            $video->admin_review_first_times_at = date("Y-m-d H:i:s");
            $video->save(false);

            try {
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }

            \Yii::info('{EndVideo} changeApprove: ' . $video->id, 'console');
        }

        $categories = VtGroupCategoryBase::find()->where(['id' => $categoryIds])->all();

        foreach ($categories as $category) {
            $category->generateThumbs();
            $category->generateAvatarThumbs();
        }
    }

    public function actionChangeApproveBackup()
    {
        \Yii::info('{START} changeApprove:approveVideo' . strval($this->approveVideo), 'console');
        set_time_limit(600);
        ini_set('memory_limit', '512M');
        $limit = Yii::$app->params['data.sync.limit'];

        $videos = VtVideoBase::getDataSynNeedApprove($limit);
        $lastId = \Yii::$app->redis->get('change-approve-last');

        if(!$lastId) {
            $lastId = 1;
        }

        $videos = VtVideoBase::find()
            ->where([
                'status' => VtVideoBase::STATUS_JUST_SYNC,
                'convert_status' => VtVideoBase::CONVERT_STATUS_SUCCESS
            ])
            ->andWhere(['not', ['syn_id' => null]])
            // ->andWhere(['>', 'id', $lastId])
            ->orderBy(['id' => SORT_ASC])
            ->limit($limit)
            ->all();
        \Yii::info('{TRACE} DataSynNeedApprove: ' . count($videos), 'console');
        $ownedVideos = array();

        foreach ($videos as $video) {
        //     $id = $video->id;

        //     if(!(\Yii::$app->redis->hget('change-approve-queue', $video->id))) {
        //         $ownedVideos[] = $video;
        //         \Yii::$app->redis->hset('change-approve-queue', $video->id, $video->id);
        //     }
        // }

        // if(isset($id)) {
        //     \Yii::$app->redis->set('change-approve-last', $id);
        // }

        // $ownedVideos[] = VtVideoBase::findOne(['syn_id' => 9091660]);

        // foreach ($ownedVideos as $video) {
            \Yii::info('{BeginVideo} changeApprove: ' . $video->id, 'console');
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($video['path']);
            copy( Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $video['path'], $tempPath);
            // var_dump($tempPath); die;
            //$result = S3Service::getObject($video['bucket'], $video['path'], $tempPath);

            //if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS) {
                VtHelper::generateAllThumb($video['bucket'], $tempPath, $video['path']);

                if(!Yii::$app->params['auto.approve.crawler'] && (empty($video['attributes']) || $video['attributes'] == 1)){
                    if($this->approveVideo === true && VtMappingWaitApproveBase::isWaitApprove($video['created_by'])) {
                        $status = VtVideoBase::STATUS_APPROVE;
                    }else{
                        $status = VtVideoBase::STATUS_DRAFT;
                        if (Yii::$app->params['auto.grant.outsource']) {
                            $video->outsource_status = VtVideoBase::OUTSOURCE_DRAFT;
                            $video->outsource_need_review = VtVideoBase::OUTSOURCE_NEED_APPROVE;
                        }
                    }
                }else{
                    $status = VtVideoBase::STATUS_APPROVE;
                }

                if(isset($status)) {
                    $video->status = $status;
                }

                $video->name = str_ireplace("%world cup%", "W.Cup", $video->name);
                $video->name_slug = Utils::removeSignOnly($video->name);
                $video->description_slug = Utils::removeSignOnly($video->description);

                $badWord = VtConfigBase::getConfig('data.sync.bad.words');
                if (preg_match_all("/\b(" . $badWord . ")\b/i", $video->name)) {
                    $video->status = 3;
                    $video->reason = 'Tự động từ chối do chứa từ khóa vi phạm bản quyền';
                }

                $video->admin_review_first_times_at = date("Y-m-d H:i:s");
                
                $video->save(false);
            //}

            try {
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }

            \Yii::info('{EndVideo} changeApprove: ' . $video->id, 'console');
        }

        // foreach ($ownedVideos as $video) {
        //     \Yii::$app->redis->hdel('change-approve-queue', $video->id);
        // }

        /*$playlists = VtPlaylistBase::getDataSynNeedApprove($limit);
        \Yii::info('{TRACE} DataSynNeedApprove: ' . count($playlists), 'console');
        foreach ($playlists as $playlist) {
            \Yii::info('{BeginPlaylist} changeApprove: ' . $playlist->id, 'console');
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($playlist['path']);
            $result = S3Service::getObject($playlist['bucket'], $playlist['path'], $tempPath);

            if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS) {
                VtHelper::generateAllThumb($playlist['bucket'], $tempPath, $playlist['path']);
                $playlist->status = 2;
                $playlist->name_slug = Utils::removeSignOnly($playlist->name);
                $playlist->description_slug = Utils::removeSignOnly($playlist->description);
                $playlist->save(false);
            }

            try {
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }

            \Yii::info('{EndPlaylist} changeApprove: ' . $playlist->id, 'console');


        }*/
//        $this->actionSyncDatabase();
    }

    /**
     *
     * Tao lai anh thumb cho video
     * @param $id 
     */
    public function actionRegenThumb($id) {
        \Yii::info("RegenThumb: $id", 'console');
        $video = VtVideoBase::findOne($id)->toArray();
        
        if(!$video || !$video['path']) {
            \Yii::info("RegenThumb: $id - not found", 'console');
            return;
        }

        $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($video['path']);
        $result = S3Service::getObject($video['bucket'], $video['path'], $tempPath);
        
        if(!empty($result) && $result['errorCode'] == S3Service::SUCCESS){
            \Yii::info("RegenThumb: $id - begin gen thumb", 'console');
            VtHelper::generateAllThumb($video['bucket'], $tempPath, $video['path']);
        }
        
        unlink($tempPath);
        \Yii::info("RegenThumb: $id - complete", 'console');
    }

    public function actionGenerateAllThumb()
    {
        \Yii::info('{START} generateAllThumb', 'console');
        set_time_limit(600);
        ini_set('memory_limit', '512M');
        $limit = Yii::$app->params['data.sync.limit'];
        $videos = VtVideoBase::getAllVideoByTime($limit, '2020-07-02 00:00:00');
        // \Yii::info('{TRACE} taila generateAllThumb: ' . count($videos), 'console');
        foreach ($videos as $video) {
            \Yii::info('{BeginVideo} generateAllThumb: ' . $video->id, 'console');
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($video['path']);
            $result = S3Service::getObject($video['bucket'], $video['path'], $tempPath);
            $result = ['errorCode' => S3Service::SUCCESS];

            // Yii::info('taila test results' . json_encode($result), 'console');
            if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS) {
                VtHelper::generateAllThumb($video['bucket'], $tempPath, $video['path']);
                // Yii::info('taila test 1' . $video->id, 'console');
                $video->name = str_ireplace("%world cup%", "W.Cup", $video->name);
                $video->name_slug = Utils::removeSignOnly($video->name);
                $video->description_slug = Utils::removeSignOnly($video->description);

                $badWord = VtConfigBase::getConfig('data.sync.bad.words');
                if (preg_match_all("/\b(" . $badWord . ")\b/i", $video->name)) {
                    $video->status = 3;
                    $video->reason = 'Tự động từ chối do chứa từ khóa vi phạm bản quyền';
                }

                $video->admin_review_first_times_at = date("Y-m-d H:i:s");

                $video->save(false);
                // Yii::info('taila test' . $video->id, 'console');
            }

            try {
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }

            \Yii::info('{EndVideo} generateAllThumb: ' . $video->id, 'console');
        }
    }

    public function actionSyncDatabase()
    {
        //select created_by, count(id) as video_count, sum(play_times) as view_count from vt_video where status=2 group by created_by;
        \Yii::info('{syncDatabase} Update Video User', 'console');
        echo '{syncDatabase}Update Video User\n';
        $arrSyncVideo = VtVideoBase::find()
            ->asArray()
            ->select('v.created_by, count(v.id) as video_count, sum(play_times) as view_count')
            ->from('vt_video v')
            ->where(['v.status' => VtVideoBase::STATUS_APPROVE])
            ->groupBy('v.created_by')
            ->all();

        foreach ($arrSyncVideo as $item) {
            \Yii::info('{syncDatabase} Update User |' . $item['created_by'] . "|video count: " . $item['video_count'] . "|view count: " . $item['view_count'], 'console');
            echo '{syncDatabase} Update User |' . $item['created_by'] . "|video count: " . $item['video_count'] . "|view count: " . $item['view_count'] . "\n";
            $isUpdate = Yii::$app->db->createCommand()
                ->update('vt_user', ['video_count' => $item['video_count'], 'view_count' => $item['view_count']], ['id' => $item['created_by']])
                ->execute();
        }
        \Yii::info('{syncDatabase} Update Film to Video', 'console');
        echo '{syncDatabase} Update Film to Video\n';
        //- Update film to video
        Yii::$app->db
            ->createCommand("update vt_video v, vt_playlist_item p set v.old_playlist_id = p.playlist_id, v.type='VOD' where v.id = p.item_id AND v.type='FILM'")
            ->execute();

        \Yii::info('{syncDatabase} Update Total Comment to Video', 'console');
        echo '{syncDatabase} Update Total Comment to Video\n';

        $arrComment = VtCommentBase::find()->asArray()
            ->select('count(id) as comment_count, c.content_id, c.type')
            ->from("vt_comment c")
            ->where(['c.status' => 1])
            ->groupBy('c.content_id')
            ->all();

        foreach ($arrComment as $comment) {
            \Yii::info('{syncDatabase} Update User |' . $comment['content_id'] . "|video comment_count: " . $comment['comment_count'], 'console');
            echo '{syncDatabase} Update User |' . $comment['content_id'] . "|video comment_count: " . $comment['comment_count'] . "\n";
            $isUpdate = Yii::$app->db->createCommand()
                ->update('vt_video', ['comment_count' => $comment['comment_count']], ['id' => $comment['content_id']])
                ->execute();
        }

    }

    public function actionBackupVideoIphone()
    {
        $dbName = "campaign_iphonex_" . date("Ymd_His");
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("create table " . $dbName . "   select * from vt_video where syn_id is null and created_at > '2017-11-01 00:00:00' ORDER BY play_times_real desc limit 10000 ");

        $result = $command->query();

    }

    public function actionRandomAvatar()
    {
        \Yii::info('{START} actionRandomAvatar', 'console');
        set_time_limit(600);
        ini_set('memory_limit', '512M');

        $arrChannels = VtUserBase::find()->asArray()
            ->where( ['is', 'bucket', null] )
            ->orWhere(['is', 'channel_bucket', null])
            ->orWhere(['=', 'bucket', ''])
            ->orWhere(['=', 'channel_bucket', ''])
            ->limit(10000)
            ->all();

        foreach ($arrChannels as $channel) {
            \Yii::info('{BeginRandom} actionRandomAvatar of CHANNEL:  ' . $channel['id'], 'console');
            VtUserBase::updateRandomAvatar($channel["id"]);
        }
    }
}
