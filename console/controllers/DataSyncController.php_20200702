<?php

namespace console\controllers;

use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtCommentBase;
use common\models\VtConfigBase;
use common\models\VtMappingWaitApproveBase;
use common\models\VtPlaylistBase;
use common\models\VtUserBase;
use common\models\VtVideoBase;
use cp\models\VtConfig;
use yii\console\Controller;
use Yii;

class DataSyncController extends Controller
{

    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID), [
        ]);
    }

    public function actionChangeApprove()
    {
        \Yii::info('{START} changeApprove', 'console');
        set_time_limit(600);
        ini_set('memory_limit', '512M');
        $limit = Yii::$app->params['data.sync.limit'];

        $videos = VtVideoBase::getDataSynNeedApprove($limit);

        \Yii::info('{TRACE} DataSynNeedApprove: ' . count($videos), 'console');
        foreach ($videos as $video) {

            \Yii::info('{BeginVideo} changeApprove: ' . $video->id, 'console');
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($video['path']);
            $result = S3Service::getObject($video['bucket'], $video['path'], $tempPath);

            if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS) {
                VtHelper::generateAllThumb($video['bucket'], $tempPath, $video['path']);

                if(!Yii::$app->params['auto.approve.crawler'] && (empty($video['attributes']) || $video['attributes'] == 1)){
                    if(VtMappingWaitApproveBase::isWaitApprove($video['created_by'])) {
                        $video->status = VtVideoBase::STATUS_APPROVE;
                    }else{
                        $video->status = VtVideoBase::STATUS_DRAFT;
                        if (Yii::$app->params['auto.grant.outsource']) {
                            $video->outsource_status = VtVideoBase::OUTSOURCE_DRAFT;
                            $video->outsource_need_review = VtVideoBase::OUTSOURCE_NEED_APPROVE;
                        }
                    }
                }else{
                    $video->status = VtVideoBase::STATUS_APPROVE;
                }

                $video->name = str_ireplace("%world cup%", "W.Cup", $video->name);
                $video->name_slug = Utils::removeSignOnly($video->name);
                $video->description_slug = Utils::removeSignOnly($video->description);

                $badWord = VtConfigBase::getConfig('data.sync.bad.words');
                if (preg_match_all("/\b(" . $badWord . ")\b/i", $video->name)) {
                    $video->status = 3;
                    $video->reason = 'Tự động từ chối do chứa từ khóa vi phạm bản quyền';
                }

                $video->admin_review_first_times_at = date("Y-m-d H:i:s");
                
                $video->save(false);
            }

            try {
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }

            \Yii::info('{EndVideo} changeApprove: ' . $video->id, 'console');
        }


        $playlists = VtPlaylistBase::getDataSynNeedApprove($limit);
        \Yii::info('{TRACE} DataSynNeedApprove: ' . count($playlists), 'console');
        foreach ($playlists as $playlist) {
            \Yii::info('{BeginPlaylist} changeApprove: ' . $playlist->id, 'console');
            $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($playlist['path']);
            $result = S3Service::getObject($playlist['bucket'], $playlist['path'], $tempPath);

            if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS) {
                VtHelper::generateAllThumb($playlist['bucket'], $tempPath, $playlist['path']);
                $playlist->status = 2;
                $playlist->name_slug = Utils::removeSignOnly($playlist->name);
                $playlist->description_slug = Utils::removeSignOnly($playlist->description);
                $playlist->save(false);
            }

            try {
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }

            \Yii::info('{EndPlaylist} changeApprove: ' . $playlist->id, 'console');


        }
//        $this->actionSyncDatabase();
    }

    public function actionSyncDatabase()
    {
        //select created_by, count(id) as video_count, sum(play_times) as view_count from vt_video where status=2 group by created_by;
        \Yii::info('{syncDatabase} Update Video User', 'console');
        echo '{syncDatabase}Update Video User\n';
        $arrSyncVideo = VtVideoBase::find()
            ->asArray()
            ->select('v.created_by, count(v.id) as video_count, sum(play_times) as view_count')
            ->from('vt_video v')
            ->where(['v.status' => VtVideoBase::STATUS_APPROVE])
            ->groupBy('v.created_by')
            ->all();

        foreach ($arrSyncVideo as $item) {
            \Yii::info('{syncDatabase} Update User |' . $item['created_by'] . "|video count: " . $item['video_count'] . "|view count: " . $item['view_count'], 'console');
            echo '{syncDatabase} Update User |' . $item['created_by'] . "|video count: " . $item['video_count'] . "|view count: " . $item['view_count'] . "\n";
            $isUpdate = Yii::$app->db->createCommand()
                ->update('vt_user', ['video_count' => $item['video_count'], 'view_count' => $item['view_count']], ['id' => $item['created_by']])
                ->execute();
        }
        \Yii::info('{syncDatabase} Update Film to Video', 'console');
        echo '{syncDatabase} Update Film to Video\n';
        //- Update film to video
        Yii::$app->db
            ->createCommand("update vt_video v, vt_playlist_item p set v.old_playlist_id = p.playlist_id, v.type='VOD' where v.id = p.item_id AND v.type='FILM'")
            ->execute();

        \Yii::info('{syncDatabase} Update Total Comment to Video', 'console');
        echo '{syncDatabase} Update Total Comment to Video\n';

        $arrComment = VtCommentBase::find()->asArray()
            ->select('count(id) as comment_count, c.content_id, c.type')
            ->from("vt_comment c")
            ->where(['c.status' => 1])
            ->groupBy('c.content_id')
            ->all();

        foreach ($arrComment as $comment) {
            \Yii::info('{syncDatabase} Update User |' . $comment['content_id'] . "|video comment_count: " . $comment['comment_count'], 'console');
            echo '{syncDatabase} Update User |' . $comment['content_id'] . "|video comment_count: " . $comment['comment_count'] . "\n";
            $isUpdate = Yii::$app->db->createCommand()
                ->update('vt_video', ['comment_count' => $comment['comment_count']], ['id' => $comment['content_id']])
                ->execute();
        }

    }

    public function actionBackupVideoIphone()
    {
        $dbName = "campaign_iphonex_" . date("Ymd_His");
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("create table " . $dbName . "   select * from vt_video where syn_id is null and created_at > '2017-11-01 00:00:00' ORDER BY play_times_real desc limit 10000 ");

        $result = $command->query();

    }

    public function actionRandomAvatar()
    {
        \Yii::info('{START} actionRandomAvatar', 'console');
        set_time_limit(600);
        ini_set('memory_limit', '512M');

        $arrChannels = VtUserBase::find()->asArray()
            ->where( ['is', 'bucket', null] )
            ->orWhere(['is', 'channel_bucket', null])
            ->orWhere(['=', 'bucket', ''])
            ->orWhere(['=', 'channel_bucket', ''])
            ->limit(10000)
            ->all();

        foreach ($arrChannels as $channel) {
            \Yii::info('{BeginRandom} actionRandomAvatar of CHANNEL:  ' . $channel['id'], 'console');
            VtUserBase::updateRandomAvatar($channel["id"]);
        }
    }
}
