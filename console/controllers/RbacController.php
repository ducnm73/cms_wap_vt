<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class RbacController extends Controller {

    public function actionInit() {
        $user = new User();
        $user->username = "admin";
        $user->email = "admin@viettel.com.vn";
        $user->status = 1;
        $user->password_hash = '123456a@';
        $user->save(false);
    }

}
