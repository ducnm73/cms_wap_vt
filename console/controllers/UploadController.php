<?php

namespace console\controllers;

use common\libs\S3Service;
use common\models\KpiUploadBase;
use common\models\VtVideoBase;
use yii\console\Controller;
use Yii;

class UploadController extends Controller
{
    public $id;
    public $orgPath;
    public $sessionId;

    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID), [
            'id',
            'orgPath',
            'sessionId'
        ]);
    }

    public function actionPutObject()
    {
        \Yii::info('{Begin} putObject: ' . $this->id . '|' . $this->orgPath . '|' . $this->sessionId, 'console');
        $video = VtVideoBase::findOne($this->id);

        if(!$video){
            \Yii::error('{Finish} putObject: ' . $this->id . ' fail|Reason:Video not found', 'console');
        }else{
            $startTime = strtotime('now');

            $result = S3Service::putObject($video->file_bucket, $this->orgPath, $video->file_path);

            if(!empty($result) && $result['errorCode'] == S3Service::SUCCESS){

                \Yii::info('{Finish} putObject: ' . $this->id . ' success', 'console');
                $video->convert_status = VtVideoBase::CONVERT_STATUS_DRAFT;
                $video->save(false);
                unlink($this->orgPath);

                $errorCode = 0;
            }else{
                \Yii::error('{Finish} putObject: ' . $this->id . ' fail|Reason:put to S3 fail', 'console');

                $errorCode = 0;
            }

            $kpi = KpiUploadBase::getBySessionId($this->sessionId);
            if($kpi){
                $kpi->s3_start_time = date('Y-m-d H:i:s', $startTime);
                $kpi->s3_end_time = date('Y-m-d H:i:s');
                $kpi->s3_duration = strtotime('now') - $startTime;
                $kpi->error_code = $errorCode;
                $kpi->save(false);
            }

        }
    }
	
	// public function actionTaila() {
		// $userId = 'QbQ3Ul7SNWcj22ItDS941efUgSYEvR519katT50eyn4/Bxs89MVjj/hv+ef8lCT3cZBpRnPmzHWUy6g3a20NB39Lslje0YOp6/qsy4VeSLU00U2R0aTt+cyJw787yEhLWfCOg1IyXD7eoAWWCrIRgIjNhcTm7aUyyRDIMct+DdhJtfOxWZ97VtDcEdvRmBpfB2aoXvySIjF+XlRjkFLgbpDM9Oud5WLoFgadgM2Q6xGro+jceNxXNPTLA8DHZFNyXX4Uzq7iV9sCFTdT77DBOVx9ID4NqCVY0wnEiYZHsQJhuKPfxhdl2o6Vjv2j6pCNEjhFNzLdFCPrCJsBiXd7fA==';
		// $rsa = new RSA('/u01/congphim/apps/myclip-web-lao/common/config/public_key.pem', '/u01/congphim/apps/myclip-web-lao/common/config/private_key.pem');
        // $userIdDecode = $rsa->decrypt($userId);
		
		// var_dump($userIdDecode); die;
	// }

}
