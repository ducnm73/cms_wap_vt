<?php

namespace console\controllers;

use api\models\VtConvertedFile;
use api\models\VtVideo;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtFlow;
use common\libs\VtHelper;
use common\models\VtConvertedFileBase;
use common\models\VtPlaylistBase;
use common\models\VtVideoBase;
use yii\console\Controller;

class MobitvController extends Controller
{
    public $process;

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ['process']);
    }

    protected static function getConvertedFile()
    {
        return VtConvertedFile::find()
            ->select('v.*')
            ->from(VtConvertedFile::tableName() . ' v')
            ->where([
                'v.bucket' => 'video_mobitv',
                'v.encrypted_path' => null
            ])
            ->orderBy('v.id DESC')
            ->limit(1000)
            ->all();
    }

    protected static function getFileNeedDownload($process)
    {
        return VtConvertedFile::find()
            ->select('v.*')
            ->from(VtConvertedFile::tableName() . ' v')
            ->where([
                'v.bucket' => 'video_mobitv',
                'v.is_downloaded' => 0
            ])
            ->orderBy('v.id DESC')
            ->offset(100 * $process)
            ->limit(10)
            ->all();

    }

    protected static function getFileNeedUpload()
    {
        return VtConvertedFile::find()
            ->select('v.*')
            ->from(VtConvertedFile::tableName() . ' v')
            ->where([
                'v.bucket' => 'video_mobitv',
                'v.is_migrated' => 0,

            ])
            ->andWhere('v.is_downloaded <> :is_downloaded', [':is_downloaded' => 0])
            ->orderBy('v.id DESC')
            ->limit(10)
            ->all();

    }

    public function actionIndex()
    {
        $convertFiles = self::getConvertedFile();

        while (count($convertFiles) > 0) {
            foreach ($convertFiles as $convertFile) {

                $streamUrl = S3Service::generateStreamURL($convertFile);

//                $streamUrl = 'zzz';
                $convertFile->encrypted_path = $streamUrl;
                $convertFile->save(false);
            }
            $convertFiles = self::getConvertedFile();
        }
    }

    public function actionDownload()
    {
        ini_set("memory_limit", "500M");
        $aContext = array(
            'http' => array(
                'proxy' => 'tcp://192.168.193.23:3128',
                'request_fulluri' => true,
            ),
        );
        $cxContext = stream_context_create($aContext);

        $files = self::getFileNeedDownload($this->process);

        while (count($files) > 0) {
            foreach ($files as $file) {

                $localPath = \Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $file->id . '.mp4';
                try {
                    echo "\r\nBegin download " . $file->id;
                    $streamContent = file_get_contents($file->encrypted_path, false, $cxContext);
                    echo "\r\nFinish download " . $file->id;
                    echo "\r\nBegin put to file " . $file->id;
                    file_put_contents($localPath, $streamContent);
                    echo "\r\nEnd put to file " . $file->id;
                } catch (\Exception $ex) {
                    echo "\r\nError download " . $file->id;
                }

                $file->is_downloaded = $this->process;
                $file->save(false);
            }

            $files = self::getFileNeedDownload($this->process);
        }

    }

    public function actionUpload()
    {

        $files = self::getFileNeedUpload();


        while (true) {
            foreach ($files as $file) {
                //Download file
                $localPath = \Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $file->id . '.mp4';


                if (is_file($localPath)) {
                    //Upload file
                    echo "\r\nBegin put to S3 storage " . $file->id;
                    S3Service::putObject('video1', $localPath, $file->path);
                    echo "\r\nEnd put to S3 storage " . $file->id . '|path: ' . $file->path;

                    unlink($localPath);
                }
                $file->is_migrated = 9;
                $file->save(false);
            }
            sleep(5);
            $files = self::getFileNeedUpload();
        }

    }

    public function actionCheckFileSize()
    {
        while(true){
            $convertedFiles = VtConvertedFileBase::find()
                ->where([
                        'profile_id' => [2, 3],
                        'is_migrated' => 1,
                        'bucket' => 'video1',
                    ]
                )
                ->andWhere('file_size_real is null')
                ->limit(1000)
                ->all();

            foreach($convertedFiles as $convertedFile){

                $bucket = $convertedFile['bucket'];
                $path = $convertedFile['path'];
                \Yii::info('{Begin} CheckFileSize|Bucket: ' . $bucket . '|Path: '. $path, 'console');
                $fileSize = S3Service::getFileSize($bucket, $path);
                \Yii::info('{End} CheckFileSize|Bucket: ' . $bucket . '|Path: ' . $path . '|Size: '. $fileSize, 'console');

                $convertedFile->file_size_real = $fileSize;
                $convertedFile->save(false);
            }

        }

    }

    public function actionGenerateSlug()
    {
        while (true) {
            echo "\r\nBegin generate Slug";
            $videos = VtVideoBase::find()
                ->where([
                        'syn_id' => null
                    ]
                )
                ->limit(1000)
                ->all();

            foreach ($videos as $video) {
                $video->slug = Utils::removeSign($video->name);
                $video->save(false);
            }

            sleep(5);
        }
    }

    public function actionGenerateFilmSlug()
    {
        while (true) {
            echo "\r\nBegin generate Slug";
            $playlists = VtPlaylistBase::find()
                ->limit(10000)
                ->all();

            foreach ($playlists as $playlist) {
                $playlist->slug = Utils::removeSign($playlist->name);
                $playlist->save(false);
            }

            sleep(5);
        }
    }
}
