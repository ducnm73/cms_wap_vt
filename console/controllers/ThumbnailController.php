<?php

namespace console\controllers;

use common\models\VtGroupCategoryBase;
use common\models\VtUserBase;
use Yii;
use common\helpers\Utils;
use common\libs\S3Service;
use common\libs\VtHelper;
use common\models\VtVideoBase;
use yii\console\Controller;

class ThumbnailController extends Controller
{
    public $beginId;
    public $endId;

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ['beginId', 'endId']);
    }

    public function actionGenerateThumbVideo()
    {
        ini_set("memory_limit", "500M");
        echo "\r\nBegin generate actionGenerateThumbVideo|beginId: " . $this->beginId . "|endId: " . $this->endId;

        $offset = 0;
        $limit = 5000;

        $videos = VtVideoBase::find()
            ->asArray()
            ->where([
                'bucket' => ['image1', 'video1'],
                'is_active' => 1
            ])
//            ->andWhere('id >= :beginId and id <= :endId', [':beginId' => $this->beginId, ':endId' => $this->endId])
//            ->offset($offset)
//            ->limit($limit)
            ->andWhere(['id' => 1540686])
            ->orderBy('id ASC')
            ->all();


        $size = count($videos);

        echo "\r\nRecords|offset: " . $offset . "|limit: " . $limit . '|size: ' . $size . '|size: ' . $size;

        foreach ($videos as $video) {

            try {

                $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($video['path']);
                $result = S3Service::getObject($video['bucket'], $video['path'], $tempPath);

                if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS && strpos($video['path'], '.gif') === false) {
                    echo "\r\nRecord|id: " . $video['id'] . '|bucket: ' . $video['bucket'] . '|path: ' . $video['path'] . "|result: " . $result['errorCode'] . " => SUCCESS";

                    VtHelper::generateAllThumb($video['bucket'], $tempPath, $video['path']);
                } else {
                    echo "\r\nRecord|id: " . $video['id'] . '|bucket: ' . $video['bucket'] . '|path: ' . $video['path'] . "|result: " . $result['errorCode'] . " => FAIL";
                }

#                try{
                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }
        }

        sleep(1);

        $offset += $limit;


        echo "\r\nEnd generate actionGenerateThumbVideo|beginId: " . $this->beginId . "|endId: " . $this->endId;
    }


    public function actionGenerateThumbChannel()
    {
        ini_set("memory_limit", "500M");
        echo "\r\nBegin generate actionGenerateThumbChannel|beginId: " . $this->beginId . "|endId: " . $this->endId;

        $offset = 0;
        $limit = 5000;

        $channels = VtUserBase::find()
            ->asArray()
            ->where('bucket is not null')
            ->andWhere('id >= :beginId and id <= :endId', [':beginId' => $this->beginId, ':endId' => $this->endId])
//            ->offset($offset)
//            ->limit($limit)
            ->orderBy('id ASC')
            ->all();

        $size = count($channels);

        echo "\r\nRecords|offset: " . $offset . "|limit: " . $limit . '|size: ' . $size . '|size: ' . $size;

        foreach ($channels as $channel) {
            try {
                $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($channel['path']);
                $result = S3Service::getObject($channel['bucket'], $channel['path'], $tempPath);

                if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS && strpos($channel['path'], '.gif') === false) {
                    echo "\r\nRecord|id: " . $channel['id'] . '|bucket: ' . $channel['bucket'] . '|path: ' . $channel['path'] . "|result: " . $result['errorCode'] . " => SUCCESS";

                    VtHelper::generateAllThumb($channel['bucket'], $tempPath, $channel['path'], [
                        VtHelper::SIZE_CHANNEL_LOGO_LIST,
                        VtHelper::SIZE_CHANNEL_LOGO_DETAIL
                    ]);
                } else {
                    echo "\r\nRecord|id: " . $channel['id'] . '|bucket: ' . $channel['bucket'] . '|path: ' . $channel['path'] . "|result: " . $result['errorCode'] . " => FAIL";
                }


                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }
        }
        sleep(1);

        $offset += $limit;

        echo "\r\nEnd generate actionGenerateThumbChannel|beginId: " . $this->beginId . "|endId: " . $this->endId;
    }



    public function actionGenerateThumbCategory()
    {
        ini_set("memory_limit", "500M");
        echo "\r\nBegin generate actionGenerateThumbCategory|beginId: " . $this->beginId . "|endId: " . $this->endId;

        $offset = 0;
        $limit = 5000;

        $channels = VtGroupCategoryBase::find()
            ->asArray()
            ->where('avatar_bucket is not null')
//            ->offset($offset)
//            ->limit($limit)
            ->orderBy('id ASC')
            ->all();

        $size = count($channels);

        echo "\r\nRecords|offset: " . $offset . "|limit: " . $limit . '|size: ' . $size . '|size: ' . $size;

        foreach ($channels as $channel) {
            try {
                $tempPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . basename($channel['avatar_path']);
                $result = S3Service::getObject($channel['avatar_bucket'], $channel['avatar_path'], $tempPath);

                if (!empty($result) && $result['errorCode'] == S3Service::SUCCESS && strpos($channel['path'], '.gif') === false) {
                    echo "\r\nRecord|id: " . $channel['id'] . '|bucket: ' . $channel['avatar_bucket'] . '|path: ' . $channel['avatar_path'] . "|result: " . $result['errorCode'] . " => SUCCESS";

                    VtHelper::generateAllThumb($channel['avatar_bucket'], $tempPath, $channel['avatar_path'], [
                        VtHelper::SIZE_CHANNEL_LOGO_LIST
                    ]);
                } else {
                    echo "\r\nRecord|id: " . $channel['id'] . '|bucket: ' . $channel['avatar_bucket'] . '|path: ' . $channel['avatar_path'] . "|result: " . $result['errorCode'] . " => FAIL";
                }


                unlink($tempPath);
            } catch (\Exception $exception) {
                \Yii::error('{EndPlaylist} Delete file exception: ' . $exception->getMessage(), 'console');
            }
        }
        sleep(1);


        echo "\r\nEnd generate actionGenerateThumbCategory|beginId: " . $this->beginId . "|endId: " . $this->endId;
    }

}
