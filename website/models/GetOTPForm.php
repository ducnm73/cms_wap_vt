<?php
namespace website\models;

use Yii;
use yii\base\Model;
use common\helpers\Utils;
use common\models\VtUserBase;

/**
 * Login form
 */
class GetOTPForm extends Model
{
    public $msisdn;
    public $verifyCode; // add this varible to your model class.

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'verifyCode'], 'required'],
            ['msisdn', 'filter', 'filter' => [$this, 'trim']],
            ['msisdn', 'filter', 'filter' => [$this, 'format']],
            ['verifyCode', 'filter', 'filter' => [$this, 'trim']],
            ['msisdn', 'match', 'pattern' => \Yii::$app->params['msisdn.regx'], 'message' => 'Số thuê bao không hợp lệ'],
            ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha']
        ];
    }

    public function attributeLabels()
    {
        return [
            'msisdn' => Yii::t('app', 'Số thuê bao'),
            'verifyCode' => Yii::t('app', 'Mã xác nhận'),

        ];
    }

    public function format($msisdn)
    {
        return Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
    }

    public function trim($str)
    {
        return trim($str);
    }

}
