<?php
namespace website\models;

use common\models\VtUserBase;
use Yii;
use yii\base\Model;
use common\helpers\Utils;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    
    private $_user;
    public $countFail;
    public $countCaptchaShow;
    public $verifyCode; // add this varible to your model class.

    public function __construct(array $config)
    {
        $this->countFail = $config['countFail'];
        $this->countCaptchaShow = $config['countCaptchaShow'];
        parent::__construct($config);
    }

    public function setCountFail($count)
    {
        $this->countFail = $count;
    }

    public function attributeLabels()
    {
        return [
            'username'=>Yii::t('backend','Số thuê bao'),
            'password'=>Yii::t('backend','Mật khẩu'),
            'verifyCode'=>Yii::t('backend','Mã xác nhận')

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->countFail >= $this->countCaptchaShow) {
            return [
                [['verifyCode'], 'trim'],
                [['username', 'password'], 'required'],
                // ['username', 'match', 'pattern' => \Yii::$app->params['msisdn.regx'], 'message'=>Yii::t('web','Tài khoản hoặc mật khẩu đăng nhập không đúng') ],
                ['password', 'validatePassword'],
                ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha', 'message'=>Yii::t('web','Mã xác nhận không đúng') ],
            ];
        } else {
            return [
                //[['username','password'], 'trim'],
//                [['username', 'password'], 'required'],
                [['username'],'required', 'message' => Yii::t('web', 'Username can not be blank.')],
                [['password'], 'required', 'message' => Yii::t('web', 'Password can not be blank.')],
                ['username', 'match', 'pattern' => \Yii::$app->params['msisdn.regx'], 'message'=>Yii::t('web','Tài khoản hoặc mật khẩu đăng nhập không đúng') ],
                ['password', 'validatePassword'],

            ];
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->checkPassword($this->password)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = VtUserBase::getByMsisdn(Utils::getMobileNumber($this->username, Utils::MOBILE_GLOBAL));
        }

        return $this->_user;
    }
}
