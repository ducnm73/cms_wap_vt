<?php

namespace website\models;

use Yii;

class VtCp extends \common\models\VtCpBase {

    public static function getById($id)
    {
        return self::find()
            ->asArray()
            ->where([
                'id' => $id
            ])
            ->all();
    }
}