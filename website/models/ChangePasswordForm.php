<?php
namespace website\models;

use Yii;
use yii\base\Model;
use common\helpers\Utils;
use common\models\VtUserBase;

/**
 * Login form
 */
class ChangePasswordForm extends Model
{
    public $username;
    public $password;
    public $newPassword;
    public $repeatPassword;
    private $_user;
    public $verifyCode; // add this varible to your model class.

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $isEmptyPassword = Yii::$app->session->get("isEmptyPassword", 0);
        if ($isEmptyPassword) {
            return [
                [['newPassword', 'repeatPassword',], 'required'],
                [['newPassword', 'repeatPassword', 'verifyCode'], 'filter', 'filter' => 'trim'],
                ['newPassword', 'string', 'min' => 6, 'max' => 128, 'tooShort' => Yii::t('web','Mật khẩu mới từ 6 đến 128 kí tự.'), 'tooLong' => Yii::t('web','Mật khẩu mới từ 6 đến 128 kí tự.')],
                ['newPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '!=', 'message' => Yii::t('web','Mật khẩu mới không được trùng mật khẩu cũ.')],
                ['repeatPassword', 'compare', 'compareAttribute' => 'newPassword', 'operator' => '==', 'message' => Yii::t('web','Xác nhận mật khẩu không đúng.')],
//                ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha']
            ];
        }
        return [
            [['password', 'newPassword', 'repeatPassword'], 'required'],
            [['newPassword', 'repeatPassword', 'verifyCode'], 'filter', 'filter' => 'trim'],
            ['password', 'validatePassword'],
            ['newPassword', 'string', 'min' => 6, 'max' => 128, 'tooShort' => Yii::t('web','Mật khẩu mới từ 6 đến 128 kí tự.'), 'tooLong' => Yii::t('web','Mật khẩu mới từ 6 đến 128 kí tự.')],
            ['newPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '!=', 'message' => Yii::t('web','Mật khẩu mới không được trùng mật khẩu cũ.')],
            ['repeatPassword', 'compare', 'compareAttribute' => 'newPassword', 'operator' => '==', 'message' => Yii::t('web','Xác nhận mật khẩu không đúng.')],
//            ['verifyCode', 'captcha', 'captchaAction' => 'auth/captcha']
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('web', 'Mật khẩu cũ'),
            'newPassword' => Yii::t('web', 'Mật khẩu mới'),
            'repeatPassword' => Yii::t('web', 'Xác nhận mật khẩu'),
//            'verifyCode' => Yii::t('web', 'Mã xác nhận'),

        ];
    }

    /**
     * @return bool
     * @throws sfException
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->checkPassword($this->password)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = VtUserBase::getByMsisdn($this->username);
        }
        return $this->_user;
    }
}
