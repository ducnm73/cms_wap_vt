<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/../../common/config/pools.php'), require(__DIR__ . '/params.php')
);
$config = [
    'id' => 'app-web',
    'basePath' => dirname(__DIR__),
    'language'=>'vi',
    'bootstrap' => ['log',
     'wapsite\components\MyclipWapRoute'
    ],
    'controllerNamespace' => 'website\controllers',
    'layout' => 'main.twig',
    'components' => [
        'i18n' => [
            'translations' => [
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
//                    'fileMap' => [
//                        'en' => 'en.php',
//                    ],
                ],
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
//                    'fileMap' => [
//                        'vi' => 'vi.php',
//                    ],
                ],
                'vi*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
//                    'fileMap' => [
//                        'vi' => 'vi.php',
//                    ],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'website\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logFile' => '@logs/web/error.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logFile' => '@logs/web/warning.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => '@logs/web/info.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'logFile' => '@logs/web/queries.log',
                ],
                    [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['promotion'],
                    'levels' => ['info'],
                    'logFile' => '@logs/web/promotion.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'default/error',
        ],
        'session' => [
            'name' => 'myvideo-frontend',
            'class' => 'yii\redis\Session',
            'redis' => 'redis' // id of the connection application component
        ],
        'cache' => [
            'class' => 'common\helpers\MobiTVRedisCache',
            'redis' => 'redis' // id of the connection application component
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                    ],
                    'functions' => array(
                        't' => 'Yii::t',
                    ),
                    'globals' => [
                        'html' => '\yii\helpers\Html',
                        'Url' => '\yii\helpers\Url',
                        'Alert' => 'website\widgets\Alert',
                        'HeaderWidget' => 'website\components\common\HeaderWidget',
                        'MenuLeftWidget' => 'website\components\common\MenuLeftWidget',
                        'SigninWidget' => 'website\components\common\SigninWidget',
                        'FooterWidget' => 'website\components\common\FooterWidget',
                        'SearchWidget' => 'website\components\common\SearchWidget',
                        'ChangePasswordWidget' => 'website\components\common\ChangePasswordWidget',
                        'TrackingWidget' => 'website\components\common\TrackingWidget',
                    ],
                    'uses' => ['yii\bootstrap'],
                    'extensions' => [
                            ['yii2-twig', new \wapsite\components\WapExtension()]
                    ],
                ],
            // ...
            ],
        ],
    ],
    'params' => $params,
];

// Cau hinh route
$config['defaultRoute'] = 'default/index';

$config['components']['urlManager'] = [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    // chi nhung rule trong list moi dc phep truy cap
    'enableStrictParsing' => false,
    'rules' => require(__DIR__ . '/routes.php'),
];
$config['components']['meta'] = [
    'class' => 'wapsite\components\VtMeta',
];

return $config;
