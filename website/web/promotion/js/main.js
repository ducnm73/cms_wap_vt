var $humberger,$overMenu,$close,$overlay,$package,$toggleModal,$modal;
var $loader = $('#loader');

window.onload = function() {
    $loader.hide(100);
};

$(document).ready(function() {

    $humberger = $('#humberger');
    $overMenu = $('nav');
    $close = $('nav .close');

    $overlay = $('.overlay');
    $package = $('.package');

    $toggleModal = $('.toggleModal');
    $modal = $('.modal');

    //main menu function
    $humberger.click(function(){
        $('body').addClass('disScroll');
        $overMenu.css({marginLeft:'-70%'});
        //show menu
        $overMenu.show();
        //animation handler
        $overMenu.animate({marginLeft:'0%'},100, function(){
            $overlay.show();
            $close.find('i').addClass('rotate0');
        });
        //close menu when click on overlay
        $overlay.click(hideNav);
    });
    //close menu
    $close.click(hideNav);

    //packages handler
    $package.bind('click', function () {
        $package.removeClass('package-active');
        $(this).addClass('package-active');
    });

    $toggleModal.bind('click', function () {
        var $modalID = $($(this).data("modal"));
        $modalID.show();
        $overlay.show();
        $overlay.click(hideModal);
    });
});

function hideNav() {
    $overlay.hide(150);
    $close.find('i').removeClass('rotate0');
    $overMenu.delay(100).animate({marginLeft:'-70%'},100, function(){
        $overlay.hide();
        $overMenu.css({marginLeft:'0%'});
        $overMenu.hide();
    });
    $('body').removeClass('disScroll');
}

function hideModal() {
    $overlay.hide();
    $modal.hide();
}

function showVideoPromotion(id, id2){
    $( id +" .hide-video-promotion").show();
    $( id2).hide();
}

function hideShowDiv(hideId, showId){
    $(hideId).hide();
    $(showId).show();
    $(this).addClass("active");
}