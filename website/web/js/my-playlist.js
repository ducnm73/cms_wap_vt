"use strict";

/* @author KhanhDQ */
var MyPlaylist = React.createClass({
    displayName: "MyPlaylist",

    getInitialState: function getInitialState() {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: parseInt($("#getMoreOffset").val()),
            lockScroll: false,
            endOfData: false
        };
    },
    componentWillMount: function componentWillMount() {},
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    componentDidMount: function componentDidMount() {
        window.addEventListener('scroll', this.updateViewport, false);
        this.updateViewport();
    },
    componentWillUnmount: function componentWillUnmount() {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function updateViewport() {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Dieu kien load DATA
        if (this.state.isFirstTimeLoad || (this.state.lockScroll == false && document.body.scrollHeight < window.pageYOffset + window.innerHeight + 100)) {
            this.state.isFirstTimeLoad = false;
            this.setState({ lockScroll: true });
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                tryCount: 0,
                retryLimit: 3,
                data: { 'limit': this.state.limit, 'offset': this.state.offset },
                success: function (ajaxData) {
                    var myPlaylist = ajaxData.content;
                    // Neu khong lay dc content thi STOP
                    if (myPlaylist.length == 0) {
                        this.setState({ endOfData: true, lockScroll: false });
                        return;
                    }
                    var prevMyPlaylist = this.state.appendVideos;
                    prevMyPlaylist = prevMyPlaylist.concat(myPlaylist);
                    this.setState({ appendVideos: prevMyPlaylist, lockScroll: false });

                    if (myPlaylist.length > 0) {
                        this.setState({ offset: this.state.offset + myPlaylist.length, lockScroll: false });
                    } else {
                        this.setState({ lockScroll: true });
                    }
                    if (myPlaylist.length < this.state.limit) {
                        this.setState({ endOfData: true, lockScroll: false });
                        return;
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({ lockScroll: false });
                }.bind(this)
            });
        }
    },

    render: function render() {
        var rows = [];
        var serverAjax = this.state.appendVideos;
        if (serverAjax) {
            rows.push(React.createElement(MyPlaylistBox, { videos: serverAjax }));
        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(React.createElement(
                "div",
                null,
                React.createElement('div', {className: 'clearfix'}),
                React.createElement(
                    'div',
                    {className: 'spinner'},
                    React.createElement(
                        'div',
                        {className: 'quarter'},
                        React.createElement('div', {className: 'circle'})
                    )
                ),
                React.createElement('div', {className: 'clearfix'})
            ));
        }

        return React.createElement(
            "div",
            null,
            rows
        );
    }
});
var MyPlaylistBox = React.createClass({
    displayName: "MyPlaylistBox",

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.png",
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {},
    render: function render() {
        var _this = this;

        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                React.createElement(
                    "div", {className: "item" },
                    React.createElement(
                        "a",
                        { className: "image",href: (this.props.videos[i]['num_video'] == 0) ? "javascript:void(0)" : '/playlist/chi-tiet-playlist/'+this.props.videos[i]['id'] },
                        React.createElement("img", { src: this.props.videos[i]['coverImage'],
                            onError: function onError(e) {
                                e.target.src = _this.state.defaultImage16x9;
                            }
                        }),
                        React.createElement(
                            "span",
                            { className: "number" },
                            this.props.videos[i]['num_video'],
                            React.createElement("br"),
                            React.createElement(
                                "i",
                                { className: "icon-playlist" }
                            )
                        ),
                        React.createElement(
                            "span",
                            { className: "overlay" },
                            React.createElement(
                                "i",
                                { className: "fa fa-play" },
                            ),
                            trans("PHÁT TẤT CẢ"),
                        ),
                    ),
                    React.createElement(
                        "div",
                        { className: "description" },
                        React.createElement(
                            "a",
                            { className: "name-video",title:this.props.videos[i].name,href: (this.props.videos[i]['num_video'] == 0) ? "javascript:void(0)" : '/playlist/chi-tiet-playlist/'+this.props.videos[i]['id'] },
                            this.props.videos[i].name
                        )
                    )   
                )  
            );
        }
        return React.createElement(
            "div",
            { className: "loadMoreMyPlaylist" },
            videos
        );
    }
});
ReactDOM.render(React.createElement(MyPlaylist, { url: "/", loadMoreUrl: "/playlist/load-my-playlist" }), document.getElementById('contentMyPlaylist'));