/**
 * User: KhanhDQ
 * Date: 9/29/2017
 * Time: 09:00 AM
 */
Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

var VideoPlaylist = function () {
    var isFirstLoad = true;
    var lockLoadingComment = false;
    var isAllData = false;
    var scope = {statusLike: null, statusDislike: null, status: null}
    var lockToogleLike = false;
    var lockFollowVideo = false;
    var lockLikeComment = false;
    var player = null;
    var shufflePlaylist = false;
    var repeatPlaylist = false;


    toastr.options.showDuration = 1000;
    toastr.options.timeOut = 2000;

    function loadComment(offset) {

        if (isAllData == true) {
            return;
        }
        $("#loading-comment").show();
        $.ajax({
            method: "GET",
            url: $('#commentUrl').val(),
            data: {
                'type': $('#commentType').val(),
                'content_id': $('#contentId').val(),
                'offset': offset,
                'limit': $("#commentLimit").val()
            }
        }).done(function (data) {
            if (data.length > 0) {
                $("#loading-comment").hide();
                $("#commentContent").append(data);
                $("#commentOffset").val(parseInt($("#commentOffset").val()) + parseInt($("#commentLimit").val()));
            } else {
                if (isFirstLoad == true) {
                    $('#boxNoComment').show();
                }
                isAllData = true;
            }

            if (document.getElementById('comment-empty')) {
                isAllData = true;
                lockLoadingComment = true;
            }

            lockLoadingComment = false;
            $("#loading-comment").hide();
            $('.btnLikeComment').bind('click', clickLikeComment());
            isFirstLoad = false;
        }).fail(function (jqXHR, textStatus) {
            $("#loading-comment").hide();
            isFirstLoad = false;
        });
    }

    function autoloadComment() {
        $(window).scroll(function (event) {
            if ((($(window).scrollTop() + $(window).height() + 20 >= $('.body-detail-left').height()) || $(window).height() == $(document).height()) && lockLoadingComment == false) {
                lockLoadingComment = true;
                loadComment($("#commentOffset").val());
            }
        });
        if (lockLoadingComment == false) {
            lockLoadingComment = true;
            loadComment($("#commentOffset").val());
        }
    }

    function toggleLike() {
        // $('.likeBtn').click(function() {
        $(document).on("click", '.likeBtn', function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            if (lockToogleLike == false) {
                if (scope.statusLike == null) {
                    scope.statusLike = $(this).data('like');
                }
                lockToogleLike = true;
                processLikeVideo($(this).data('id'), 'like');
            }
        });
    }

    function toggleDislike() {
        // $('.dislikeBtn').click(function() {
        $(document).on("click", '.dislikeBtn', function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            if (lockToogleLike == false) {
                if (scope.statusDislike == null) {
                    scope.statusDislike = $(this).data('dislike');
                }
                lockToogleLike = true;
                processLikeVideo($(this).data('id'), 'dislike');
            }
        });
    }

    function processLikeVideo(videoId, type) {
        if (type == 'like') {
            if (scope.statusLike == 1) {
                scope.statusLike = 0;
            } else {
                scope.statusLike = 1;
            }
            scope.status = scope.statusLike;
            if (scope.statusDislike == null)
                scope.statusDislike = $('#linkDislike_' + videoId).data('dislike');
        } else {
            if (scope.statusDislike == -1) {
                scope.statusDislike = 0;
            } else {
                scope.statusDislike = -1;
            }
            scope.status = scope.statusDislike;
            if (scope.statusLike == null)
                scope.statusLike = $('#linkLike_' + videoId).data('like');
        }
        $.ajax({
            method: "POST",
            url: $('#toggleLikeUrl').val(),
            data: {
                'id': videoId,
                'status': scope.status,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            }
        }).done(function (obj) {
            processIconLike(videoId, type, obj);
        }).fail(function (jqXHR, textStatus) {
            lockToogleLike = false;
        });
    }

    function processIconLike(videoId, type, obj) {
        if (obj['responseCode'] == '200') {
            var currentLike = parseInt($('#numLike_' + videoId).html());
            var currentDislike = parseInt($('#numDislike_' + videoId).html());
            if (type == 'like') {
                if (scope.statusLike == 1) {
                    $('#likeBoxId' + videoId).addClass("choosed");
                    $('#linkLike_' + videoId).attr("title", trans("Bỏ thích"));
                    $('#numLike_' + videoId).html(currentLike + 1);
                    toastr.success(trans("Thích video thành công"), {closeButton: true});
                } else if (scope.statusLike != 1) {
                    $('#likeBoxId' + videoId).removeClass("choosed");
                    $('#linkLike_' + videoId).attr("title", trans("Tôi thích video này"));
                    $('#numLike_' + videoId).html(currentLike - 1);
                    toastr.success(trans("Bỏ thích video thành công"), {closeButton: true});
                }
                $('#linkLike_' + videoId).attr('data-like', scope.statusLike);
                // remove dislike if exist
                if (scope.statusDislike == -1) {
                    $('#numDislike_' + videoId).html(currentDislike - 1);
                    scope.statusDislike = 0;
                    $('#dislikeBoxId' + videoId).removeClass("choosed");
                }
            } else {
                if (scope.statusDislike == -1) {
                    $('#numDislike_' + videoId).html(currentDislike + 1);
                    $('#dislikeBoxId' + videoId).addClass("choosed");
                    $('#linkDislike_' + videoId).attr("title", trans("Bỏ không thích video này"));
                    toastr.success(trans("Không thích video thành công"), {closeButton: true});
                } else if (scope.statusDislike != -1) {
                    $('#numDislike_' + videoId).html(currentDislike - 1);
                    $('#dislikeBoxId' + videoId).removeClass("choosed");
                    $('#linkDislike_' + videoId).attr("title", trans("Tôi không thích video này"));
                    toastr.success(trans("Bỏ không thích video thành công"), {closeButton: true});
                }
                $('#linkDislike_' + videoId).attr('data-like', scope.statusLike);
                // remove like if exist
                if (scope.statusLike == 1) {
                    $('#numLike_' + videoId).html(currentLike - 1);
                    scope.statusLike = 0;
                    $('#likeBoxId' + videoId).removeClass("choosed");
                }
            }
            lockToogleLike = false;
        } else if (obj['responseCode'] == '401' || obj['responseCode'] == '201') {
            window.location = '/auth/login';
        } else if (obj.responseCode == 444) {
            toastr.error(obj.message, {closeButton: true});
        }
    }

    function handleComment() {
        $(document).on("click", '.btnClearComment', function () {
            $('#inputComment').val("");
        });
        $(document).on('click', '.btnClearReply', function () {

            if( $('#inputComment_' + $(this).data('parentid')).val() == "" ){
                $(this).parent().parent().hide();
            }

            $('#inputComment_' + $(this).data('parentid')).val("");
        });
        $(document).on("focus", "#inputComment", function () {
            $("#control_comment").show();
            checkLogin();
            $("#one-time-comment").parent().hide();
            $('.btnCommentBox').show();
        });

        // $('#btnSubmitComment').click(function () {
        $(document).on("click", '#btnSubmitComment', function () {
            var content = $.trim($("#inputComment").val());
            $("#control_comment").hide();
            postComment(0, content);
        });
    }

    function checkLogin() {
        if ($('#userId').val() == '') {
            window.location = '/auth/login';
        }
    }

    function handleRelpyComment() {
        $(document).on('click', '.btnClearReply', function () {
            $('#inputComment_' + $(this).data('parentid')).val("");
        });
        $(document).on('click', '.btnReplyComment', function () {
            $(".boxReply").show();
            $("#control_comment").hide();
            var commentId = $(this).data('id');
            showReplyForm(commentId);
        });
    }

    function validateComment(content) {
        if (content.length <= 0) {
            toastr.error(trans("Vui lòng nhập nội dung bình luận"), {closeButton: true})
            return false;
        }
        return true;
    }

    function postComment(parentId, content) {
        if (validateComment(content) == false)
            return;
        if(parentId == 0){
            $(".btnPostReplyComment[data-parentid="+parentId+"]").parent().hide();
        }else{
            $(".btnPostReplyComment[data-parentid="+parentId+"]").parent().parent().hide();
        }
        var commentType = $('#commentType').val();
        var contentId = $('#contentId').val();
        $.ajax({
            method: "POST",
            url: $('#postCommentUrl').val(),
            data: {
                'parent_id': parentId,
                'content_id': contentId,
                'type': commentType,
                'comment': content,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (data) {
            try {
                var objData = $.parseJSON(data);
                if (objData.responseCode == 401) {
                    window.location = '/auth/login';
                } else {
                    toastr.error(objData.message, {closeButton: true});
                }
            } catch (err) {
                $("#content-one-time").val("");
                $("#content").val("");
                if (parentId > 0) {
                    $(data).insertAfter("#cm_" + parentId + " > .media-body > .boxReply");
                    $("#inputComment_" + parentId).val("");
                    $("#inputComment_" + parentId).blur();
                } else {
                    $("#commentContent").prepend(data);
                    $("#inputComment").val("");
                    $("#inputComment").blur();
                    $("#boxNoComment").hide();
                }
                $('.txt-singer').addClass('comment-textarea-linhtvn');
                $("#blank_comment").hide();
            }
        }).fail(function (jqXHR, textStatus) {

        });
    }

    function showReplyForm(commentId) {
        $("#one-time-comment").remove();
        $("#cm_" + commentId + " > .media-body > .boxReply").append('<div class="media item-typing" id="one-time-comment"><textarea id="inputComment_' + commentId + '" class="ipt iptLg mb10" placeholder="'+trans("Thêm câu trả lời")+'"></textarea><p>'
                + '<input type="button" class="btn-default btnPostReplyComment" data-type="VOD" data-parentid="' + commentId + '" value="'+trans("Trả lời")+'" /><input type="button" class="btn-cancel btnClearReply" data-parentid="' + commentId + '"  value="'+trans("Hủy")+'"></p></div>');
        $('#inputComment_' + commentId).focus(function () {
            checkLogin();
        });
    }

    function postReplyComment() {
        $(document).on('click', '.btnPostReplyComment', function () {
            var pCommentId = $(this).data('parentid');
            var content = $.trim($("#inputComment_" + pCommentId).val());
            postComment(pCommentId, content);
        });
    }

    function viewMoreComment() {
        // $('.viewMoreComment').click(function(){
        $(document).on("click", '.viewMoreComment', function () {
            $(this).parent().hide();
            $('.cm_group_' + $(this).data('id')).removeClass('hidden');
        });
    }

    function followVideo() {

        $('body').on("click", "#followVideo",function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            var channelId = $(this).data("id");
            var status = $("#status-channel").val();
            var channelName = $(this).data("name");
            var status = $("#status-channel").val();
            if (lockFollowVideo == false) {
                lockFollowVideo = true;
                $.ajax({
                    url: $('#followVideoURL').val(),
                    type: 'POST',
                    data: {
                        'follow_user_id': $(this).data('id'),
                        'status': status,
                        '_csrf': $('meta[name="csrf-token"]').attr("content")
                    }
                }).done(function (objData) {
                    if (objData['responseCode'] == '200') {
                        if (objData['data']['isFollow'] == true) {
                            var currentFollow = parseInt($('#countFollowId').html());
                            $('#countFollowId').html(currentFollow + 1);
                            $('#txtFollow').html(trans('Hủy theo dõi'));
                            $("#status-channel").val("0");
                            toastr.success(trans("Theo dõi kênh thành công"), {closeButton: true});

                            if (document.getElementById('channel_related_detail')) {
                                $.ajax({
                                    type: "GET",
                                    url: '/channel/get-channel-recommend',
                                    data: {
                                        '_csrf': $('meta[name="csrf-token"]').attr("content")
                                    },
                                    success: function (data) {
                                        $("#channel_related_detail").html(data);

                                        $(".owl-carousel").owlCarousel({
                                            loop: false,
                                            margin: 10,
                                            nav: false,
                                            navText: [],
                                            responsive: {
                                                0: {
                                                    items: 1
                                                },
                                                600: {
                                                    items: 4
                                                },
                                                1000: {
                                                    items: 4
                                                }
                                            },
                                            dot: false
                                        });

                                    }
                                });

                            }




                        } else {
                            var currentFollow = parseInt($('#countFollowId').html());
                            $('#countFollowId').html(currentFollow - 1);
                            $('#txtFollow').html(trans('Theo dõi'));
                            $("#status-channel").val("1");
                            toastr.success(trans("Hủy theo dõi kênh thành công"), {closeButton: true});
                        }
                    } else if (objData['responseCode'] == '401') {
                        window.location = '/auth/login';
                    } else {
                        toastr.error(objData.message, {closeButton: true});
                    }
                    $('#modal-confirm-follow').hide();
                    lockFollowVideo = false;
                }).fail(function (jqXHR, textStatus) {
                    lockFollowVideo = false;
                });
            }


        });
    }



    function follow() {
        $('#modal-confirm-follow .btn-ok').click(function () {
            var status = $("#status-channel").val();
            if (lockFollowVideo == false) {
                lockFollowVideo = true;
                $.ajax({
                    url: $('#followVideoURL').val(),
                    type: 'POST',
                    data: {
                        'follow_user_id': $(this).data('id'),
                        'status': status,
                        '_csrf': $('meta[name="csrf-token"]').attr("content")
                    }
                }).done(function (objData) {
                    if (objData['responseCode'] == '200') {
                        if (objData['data']['isFollow'] == true) {
                            var currentFollow = parseInt($('#countFollowId').html());
                            $('#countFollowId').html(currentFollow + 1);
                            $('#txtFollow').html(trans('Hủy theo dõi'));
                            $("#status-channel").val("0");
                            toastr.success(trans("Theo dõi kênh thành công"), {closeButton: true});
                        } else {
                            var currentFollow = parseInt($('#countFollowId').html());
                            $('#countFollowId').html(currentFollow - 1);
                            $('#txtFollow').html(trans('Theo dõi'));
                            $("#status-channel").val("1");
                            toastr.success(trans("Hủy theo dõi kênh thành công"), {closeButton: true});
                        }
                    } else if (objData['responseCode'] == '401') {
                        window.location = '/auth/login';
                    } else {
                        toastr.error(objData.message, {closeButton: true});
                    }
                    $('#modal-confirm-follow').hide();
                    lockFollowVideo = false;
                }).fail(function (jqXHR, textStatus) {
                    lockFollowVideo = false;
                });
            }
        });
    }

    function clickLikeComment() {
        $(document).on('click', '.btnLikeComment', function () {
            if (lockLikeComment == false) {
                lockLikeComment = true;
                var commentId = $(this).data('commentid');
                var commentType = $('#commentType').val();
                var contentId = $('#contentId').val();
                likeComment(commentType, commentId, contentId);
            }
        });
    }

    function likeComment(type, commentId, contentId) {
        $.ajax({
            method: "POST",
            url: $('#toggleLikeComment').val(),
            data: {
                'type': type,
                'comment_id': commentId,
                'content_id': contentId,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (objData) {

            if (objData['responseCode'] == 401) {
                window.location = '/auth/login';
            }

            if (objData['data']['isLike'] == true) {
                $('#icoLikedComment_' + commentId).addClass('fa-heart');
                $('#icoLikedComment_' + commentId).removeClass('fa-heart-o');
                var likeCommentNumber = parseInt($('.likeCommentNumber_' + commentId).html()) + 1;
                $('.likeCommentNumber_' + commentId).html(likeCommentNumber);
            } else {
                $('#icoLikedComment_' + commentId).removeClass('fa-heart');
                $('#icoLikedComment_' + commentId).addClass('fa-heart-o');
                ;
                var likeCommentNumber = parseInt($('.likeCommentNumber_' + commentId).html()) - 1;
                $('.likeCommentNumber_' + commentId).html(likeCommentNumber);
            }

            lockLikeComment = false;
        }).fail(function (jqXHR, textStatus) {
            lockLikeComment = false;
        });
    }
    var countDown = null;
    var player_playlist = null;
    function getDetailVideo(videoId) {
        clearInterval(countDown);
        $.ajax({
            method: "GET",
            url: '/video/ajax-detail-video?id=' + videoId,
        }).done(function (objData) {
            document.getElementById("countdown").innerHTML = "";
            if (objData.responseCode == 200) {
                if (objData.data != null) {
                    var timeVideo = new Date(objData.data).getTime();
                    var currentTime = new Date().getTime();
                    if (timeVideo > currentTime) {
                        player_playlist.pause();
                        $('.vjs-myclip').hide();
                        $('#countdown').show();
                    }
                    countdownTimer(objData.data);
                } else {
                    player_playlist.play();
                    $('.vjs-myclip').show();
                    $('#countdown').hide();
                }
            }
        }).fail(function (jqXHR, textStatus) {

        });
    }

    function countdownTimer(time) {
        "use strict";
        var countDownDate = new Date(time).getTime();

        // Update the count down every 1 second
        countDown = setInterval(function () {
            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;
            if (distance < 0) {
                clearInterval(countDown);
                document.getElementById("countdown").innerHTML = "";
                $('#countdown').hide();
                $('.vjs-myclip').show();
                player_playlist.play();
                return;
            }
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
           
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            var time = "<p class='text-1'>Chương trình sẽ bắt đầu sau</p><p><span class='number'><span>" + days + "</span>Ngày</span><span class='number'><span>" + hours + "</span>Giờ</span><span class='number'><span>" + minutes + "</span>Phút</span><span class='number'><span>" + seconds + "</span>Giây</span></p>";

            document.getElementById("countdown").innerHTML = time;

            // If the count down is over, write some text

        }, 1000);
    }

    function videoPlayer() {
        $(function () {
            $('.vjs-playlist-delete-button').click(function (event) {
                event.stopPropagation();
                alert($(this).data('delete-url'));
                // Do something
            });
        });

        player_playlist = videojs('video', {
            html5: {
                nativeAudioTracks: false,
                nativeVideoTracks: false,
                hls: {
                    debug: false,
                    enableLowInitialPlaylist: true,
                    overrideNative: true
                }
            },
            flash: {
                hls: {
                    enableLowInitialPlaylist: true
                }
            },
            inactivityTimeout: 0,
            controls: true,
            autoplay: true,
            preload: 'auto',
            aspectRatio: '16:6',
            playbackRates: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2],
            children: {
                loadingSpinner: {},
                bigPlayButton: {},
                controlBar: {
                    playToggle: {},
                    volumePanel: {},
                    currentTimeDisplay: {},
                    timeDivider: {},
                    durationDisplay: {},
                    remainingTimeDisplay: false,
                    progressControl: {},
                    subtitlesButton: false,
                    playbackRateMenuButton: {},
                    captionsButton: false,
                    subsCapsButton: false,
                    audioTrackButton: false,
                    fullscreenToggle: {}
                }
            }
        });

        var videoIdsData = jQuery.parseJSON($('#videoIdsData').val());
        var data = jQuery.parseJSON($('#playerData').val());
        player_playlist.playlist(data);
        // Initialize the playlist-ui plugin with no option (i.e. the defaults).
        player_playlist.playlistUi();
        player_playlist.playlist.autoadvance(0);

        player_playlist.addClass('vjs-myclip');

        var playlistOrder = [], qLevels = [], playlistPlayedOrder = [];
        var alreadyChange = true;
        var currentPlay = 0;
        player_playlist.on('playlistitem', function () {

        });

        player_playlist.on('loadstart', function () {
            qLevels = [];
            playlistOrder.remove(videoIdsData[player_playlist.playlist.currentItem()]);
            getDetailVideo(videoIdsData[player_playlist.playlist.currentItem()]);
            loadVideoBox(videoIdsData[player_playlist.playlist.currentItem()]);
            isAllData = false;
            lockLoadingComment = false;
	    $("#position_video").html( player_playlist.playlist.currentItem() +1 );

        });

        // player_playlist.on('error', function (event) {
        //     showValidateMsg(trans('Nội dung không có quyền xem'));
        //     setTimeout(function () {
        //         btnNext.click();
        //     }, 5000); //1s
        // });

        player_playlist.on('ended', function () {
            if (shufflePlaylist) {
                if (playlistOrder.length === 0 && !repeatPlaylist) {
                    player_playlist.reset();
                    return;
                }
                alreadyChange = true;
                playItem();
            }
        });

        player_playlist.ready(function () {
            preparePlaylist();
            this.hotkeys({
                volumeStep: 0.1,
                seekStep: 5,
                enableModifiersForNumbers: false
            });
        });

        function preparePlaylist() {
            currentPlay = 0;
            playlistOrder = [];
            for (var i = 0; i < player_playlist.playlist().length; i++) {
                playlistOrder.push(i);
            }
            playlistPlayedOrder = [];
        }

        player_playlist.qualityLevels().on('addqualitylevel', function (event) {
            var quality = event.qualityLevel;
            if (quality.height !== undefined && $.inArray(quality.height, qLevels) === -1) {
                quality.enabled = true;

                qLevels.push(quality.height);

                if (!$('.quality_ul').length) {
                    var h = '<div class="quality_setting vjs-menu-button vjs-menu-button-popup vjs-control vjs-button">' +
                            '<button class="vjs-menu-button vjs-menu-button-popup vjs-button" type="button" aria-live="polite" aria-disabled="false" title="Quality" aria-haspopup="true" aria-expanded="false">' +
                            '<span aria-hidden="true" class="vjs-icon-cog"></span>' +
                            '<span class="vjs-control-text">Quality</span></button>' +
                            '<div class="vjs-menu"><ul class="quality_ul vjs-menu-content" role="menu"></ul></div></div>';

                    $(".vjs-fullscreen-control").before(h);
                } else {
                    $('.quality_ul').empty();
                }

                qLevels.sort(function (a, b) {
                    return b - a
                });
                qLevels.reverse();

                var j = 0;

                $.each(qLevels, function (i, val) {
                    $(".quality_ul").append('<li class="vjs-menu-item" tabindex="' + i + '" role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="false" bitrate="' + val +
                            '"><span class="vjs-menu-item-text">' + val + 'p</span></li>');
                    j = i;
                });

                $(".quality_ul").append('<li class="vjs-menu-item vjs-selected" tabindex="' + (j + 1) + '" role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="true" bitrate="auto">' +
                        '<span class="vjs-menu-item-text">Auto</span></li>');
            }
        });

        $("body").on("click", ".quality_ul li", function () {
            $(".quality_ul li").removeClass("vjs-selected");
            $(".quality_ul li").prop("aria-checked", "false");

            $(this).addClass("vjs-selected");
            $(this).prop("aria-checked", "true");

            var val = $(this).attr("bitrate");

            var qualityLevels = player_playlist.qualityLevels();

            for (var i = 0; i < qualityLevels.length; i++) {
                qualityLevels[i].enabled = (val == "auto" || (val != "auto" && qualityLevels[i].height == val));
            }
        });

        function playItem() {
            if (shufflePlaylist && alreadyChange) {
                alreadyChange = false;
                if (playlistOrder.length === 0 && repeatPlaylist) {
                    preparePlaylist();
                }
                if (playlistOrder.length) {
                    var positionRandom = Math.floor(Math.random() * playlistOrder.length);
                    playlistPlayedOrder.push(currentPlay);
                    currentPlay = playlistOrder[positionRandom];
                    player_playlist.playlist.currentItem(currentPlay);
                }
            }
        }

        function addNewButton(data) {
            var myPlayer = data.player,
                    controlBar,
                    newElement = document.createElement('span'),
                    newLink = document.createElement('a');

            newElement.id = data.id;
            newElement.className = 'vjs-control';

            newElement.innerHTML = "<i class='fa " + data.icon + "' aria-hidden='true'></i>";
            //newElement.appendChild(newElement);
            controlBar = document.getElementsByClassName('vjs-control-bar')[0];
            insertBeforeNode = document.getElementsByClassName(data.previousControl)[0];
            controlBar.insertBefore(newElement, insertBeforeNode);

            return newElement;
        }
        if (!document.getElementById("previousButton")) {
            var btnPrevious = addNewButton({
                player: player_playlist,
                icon: "fa-step-backward ",
                previousControl: "vjs-play-control",
                id: "previousButton"
            });
            btnPrevious.onclick = function () {
                if (shufflePlaylist) {
                    if (playlistPlayedOrder.length > 0) {
                        alreadyChange = false;
                        playlistOrder.push(currentPlay);
                        currentPlay = playlistPlayedOrder.pop();
                        player_playlist.playlist.currentItem(currentPlay);
                    }
                } else {
                    player_playlist.playlist.previous();
                }
            };
        }
        if (!document.getElementById("nextButton")) {
            var btnNext = addNewButton({
                player: player_playlist,
                icon: "fa-step-forward",
                previousControl: "vjs-volume-panel",
                id: "nextButton"
            });
            btnNext.onclick = function () {
                if (shufflePlaylist) {
                    alreadyChange = true;
                    playItem();
                } else {
                    player_playlist.playlist.next();
                }
            };
        }
        $('.btn-repeat-list').on("click", function (event) {
            repeatPlaylist = !repeatPlaylist;
            player_playlist.playlist.repeat(repeatPlaylist);
            if (repeatPlaylist) {
                $('.btn-repeat-list').addClass('active');
                showValidateMsg(trans('Bật chế độ vòng lặp'));
            } else {
                $('.btn-repeat-list').removeClass('active');
                showValidateMsg(trans('Tắt chế độ vòng lặp'));
            }
        });

        $('.btn-random-list').on("click", function (event) {
            shufflePlaylist = !shufflePlaylist;
            $('.vjs-selected .vjs-up-next-text').hide();
            if (shufflePlaylist) {
                $('.btn-random-list').addClass('active');
                if (localStorage.clickPlayRandomWatchLater == 0) {
                    showValidateMsg(trans('Bật chế độ phát ngẫu nhiên'));
                }
                $('.vjs-up-next-text').hide();
            } else {
                if (localStorage.clickPlayRandomWatchLater == 1) {
                    localStorage.setItem("clickPlayRandomWatchLater", 0);
                }
                $('.vjs-up-next .vjs-up-next-text').show();
                $('.btn-random-list').removeClass('active');
                showValidateMsg(trans('Tắt chế độ phát ngẫu nhiên'));
            }
        });
    }

    function showMoreDescription() {
        if ($('#descriptionVideo').height() <= 40) {
            $('#btnMoreDescription').hide();
        } else {
            $('.info-video-description').removeClass('open-description');
        }
    }

    function report() {
        // $('#btnReport').click(function(){
        $(document).on("click", '#btnReport', function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            $('#reportModal').show();
        });
    }

    function handleReport() {
        // $('#submitpostFeedBack').click(function(){
        $(document).on("click", '#submitpostFeedBack', function () {
            if ($('input[name="report"]:checked').length <= 0) {
                toastr.error(trans("Vui lòng chọn loại báo cáo"), {closeButton: true});
                return;
            }

            $.ajax({
                url: $('#postFeedbackUrl').val(),
                type: 'POST',
                data: {
                    'content': $("input[name=report]:checked").attr("data-content"),
                    'type': $("#typeReport").val(),
                    'item_id': $("#contentId").val(),
                    'id': $("input[name=report]:checked").val(),
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                },
                success: function (objData) {
                    try {
                        if (objData.responseCode == 401) {
                            window.location = '/auth/login'
                        } else if (objData.responseCode == 444) {
                            toastr.error(objData.message, {closeButton: true});
                        } else {
                            toastr.success(objData.message, {closeButton: true});
                        }
                    } catch (err) {
                    }
                    $('#reportModal').hide();
                },
                error: function () {
                    toastr.error(trans("Hệ thống đang bận, quý khách vui lòng thử lại sau!"), {closeButton: true});
                    $('#reportModal').hide();
                }

            });
            $('input[name=report]').removeAttr('checked');
        });

    }

    function loadVideoBox($id) {
        $('#videoId').val($id);
        $.ajax({
            method: "GET",
            url: $('#videoBoxUrl').val(),
            data: {
                'id': $id
            }
        }).done(function (data) {
            $('#video-content').html(data);
            showMoreDescription();
            autoloadComment();
        }).fail(function (jqXHR, textStatus) {

        });
    }
    function addOnError(){
        if($('.vjs-playlist-item').length > 0){
            $('.vjs-playlist-item').each(function( index ) {
                $(this).find('img').attr("onerror","this.onerror=null;this.src='/images/data/16x9.png';");
            });
        }
    }
    return {
        //main function to initiate the module
        init: function () {
            // autoloadComment();
            viewMoreComment();
            toggleLike();
            toggleDislike();
            handleComment();
            followVideo();
            follow();
            clickLikeComment();
            handleRelpyComment();
            videoPlayer();
            postReplyComment();
            showMoreDescription();
            report();
            handleReport();
            //loadVideoBox($('#videoId').val());
            addOnError();
        }
        ,
        checkLogin: function () {
            return checkLogin();
        }
        ,
    }
    ;
}();

$(document).ready(function () {
    VideoPlaylist.init();
    $('.loadingBoxComment').show();
});