"use strict";

/* @author KhanhDQ */
var VideoChannel = React.createClass({
    displayName: "VideoChannel",

    getInitialState: function getInitialState() {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: parseInt($("#getMoreOffset").val()),
            typeId: $("#load-more-id").val(),
            lockScroll: false,
            endOfData: false
        };
    },
    componentWillMount: function componentWillMount() {},
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {

    },
    componentDidMount: function componentDidMount() {
        window.addEventListener('scroll', this.updateViewport, false);
        this.updateViewport();
    },
    componentWillUnmount: function componentWillUnmount() {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function updateViewport() {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Dieu kien load DATA
        if (!this.state.isFirstTimeLoad && !this.state.lockScroll && document.getElementById('contentVideoBlock').scrollHeight < window.pageYOffset + window.innerHeight + 100) {
            this.setState({ lockScroll: true });
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                tryCount: 0,
                retryLimit: 3,
                data: { 'id': this.state.typeId, 'limit': this.state.limit, 'offset': this.state.offset },
                success: function (ajaxData) {
                    var videoList = ajaxData.data.content;
                    // Neu khong lay dc content thi STOP
                    if (videoList.length == 0) {
                        this.setState({ endOfData: true, lockScroll: false });
                        return;
                    }
                    var oldVideoList = this.state.appendVideos;
                    oldVideoList = oldVideoList.concat(videoList);
                    this.setState({ appendVideos: oldVideoList, lockScroll: false });

                    if (videoList.length > 0) {
                        this.setState({ offset: this.state.offset + videoList.length, lockScroll: false });
                    } else {
                        this.setState({ lockScroll: true });
                    }
                    if (videoList.length < this.state.limit) {
                        this.setState({ endOfData: true, lockScroll: false });
                        return;
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({ lockScroll: false });
                }.bind(this)
            });
        } else {
            this.state.isFirstTimeLoad = false;
        }
    },

    render: function render() {
        var rows = [];
        var serverAjax = this.state.appendVideos;
        if (serverAjax) {
            rows.push(React.createElement(NewsFeedBox, { videos: serverAjax }));
        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(React.createElement(
                "div",
                null,
                React.createElement("div", { className: "clearfix" }),
                React.createElement(
                    "div",
                    { className: "spinner" },
                    React.createElement(
                        "div",
                        { className: "quarter" },
                        React.createElement("div", { className: "circle" })
                    )
                ),
                React.createElement("div", { className: "clearfix" })
            ));
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            rows.push(React.createElement("div", { className: "clear-10" }));
        }

        return React.createElement(
            "div",
            null,
            rows
        );
    }
});
var NewsFeedBox = React.createClass({
    displayName: "NewsFeedBox",

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.gif",
            defaultImage4x4: "/images/avatar.gif"
        };
    },
    clickIcoMore: function clickIcoMore(event) {
        console.log('id info', event.currentTarget.getAttribute('data-id'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {},
    render: function render() {
        var _this = this;

        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(
                React.createElement(
                    "article", {className: "videoItem" },
                    React.createElement(
                        "div",
                        { className: "pthld" },
                        React.createElement(
                            "a",
                            { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                            React.createElement("img", { src: this.props.videos[i]['coverImage'],
                                    onError: function onError(e) {
                                        e.target.src = _this.state.defaultImage16x9;
                                    },
                            })
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ctn" },
                        React.createElement(
                            "div",
                            { className: "avatar" },
                            React.createElement(
                                "a",
                                { href: "/channel/" + this.props.videos[i]['userId'], title: this.props.videos[i]['userName'] },
                                React.createElement("img", { src: this.props.videos[i]['userAvatarImage'],
                                    onError: function onError(e) {
                                        e.target.src = _this.state.defaultImage4x4;
                                    } 
                                })
                            )
                        ),
                        React.createElement(
                            "h6",
                            { className: "title" },
                            React.createElement(
                                "a",
                                { href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'] },
                                this.props.videos[i]['name']
                            )
                        ),
                        React.createElement(
                            "p",
                            { className: "txtInfo" },
                            React.createElement(
                                "a",
                                { href: "/channel/" + this.props.videos[i]['userId'] },
                                this.props.videos[i]['userName'] + " "
                            ),
                            React.createElement(
                                "i",
                                { className: "dot" }
                            ),
                            " " + this.props.videos[i]['play_times'] + " lượt xem ",
                            React.createElement(
                                "i",
                                { className: "dot" }
                            ),
                            " " + this.props.videos[i]['publishedTime']
                        ),
                        React.createElement(
                            "a",
                            { className: "icoMore optMnBtn", 'data-sortorder':"asc", 'data-id':this.props.videos[i]['id'], 'data-url': this.props.videos[i]['linkSocial'] , 'data-target': "#videoOpt" },
                            React.createElement(
                                "svg",
                                { className: "ico" },
                                React.createElement(
                                    "use",
                                    {"href": "/images/defs.svg#ico-more", }
                                )
                            )
                            
                        )
                    )   
                )  
            );
        }
        return React.createElement(
            "div",
            { className: "loadMoreVideo" },
            videos
        );
    }
});
ReactDOM.render(React.createElement(VideoChannel, { url: "/", loadMoreUrl: "/default/load-more" }), document.getElementById('loadMoreVideo'));