/**
 * User: KhanhDQ
 * Date: 10/16/2017
 * Time: 09:05 AM
 */

 function refreshCaptcha(id) {
    if (!id) {
        if (document.getElementById('getotpform-verifycode-image')) {
            id = 'getotpform-verifycode-image';
        } else if (document.getElementById('loginform-verifycode-image')) {
            id = 'loginform-verifycode-image';
        } else if (document.getElementById('mapaccountform-verifycode-image')) {
            id = 'mapaccountform-verifycode-image';
        }
    }
    $.ajax({
        type: "GET",
        url: '/auth/captcha?refresh'
    }).done(function (data) {
        $("#" + id).attr("src", data.url);
    });
}
var Account = function () {
    function showRegisterModal() {
        $('.modal-vietnt').hide();
        $('#modalRegister').show();
    }

    function showForgotPassworModal() {
        $('.modal-vietnt').hide();
        $('#forgotPasswordModal').show();
    }

    function showSignInModal() {
        $('#errorLogin').html('');
        $('.modal-vietnt').hide();
        $('#modalLogin').show();
    }
    return {
        //main function to initiate the module
        registerModal: function () {
            return showRegisterModal();
        },
        forgotPasswordModal: function () {
            return showForgotPassworModal();
        },
        signInModal: function () {
            return showSignInModal();
        },
    };
}();

var lockSignIn = false;
$(document).ready(function () {
    lockSignIn = false;
    $('.btnReg').click(function () {
        Account.registerModal();
    });
    $('.btnForgotPassword').click(function () {
        Account.forgotPasswordModal();
    });

    $('#btnSignIn').click(function () {
        Account.signInModal();
    });

    $('#submitSignIn').click(function () {
        login();
    });

});

function login() {
    if (lockSignIn == false) {
        lockSignIn = true;
        $.ajax({
            method: "POST",
            url: '/auth/ajax-login',
            dataType: 'json',
            data: {
                'username': $('#username').val(),
                'password': $('#password').val(),
                'verifyCode': $('#loginform-verifycode').val(),
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            }
        }).done(function (obj) {
            if (obj.responseCode == 200) {
                if (obj.data.redirect) {
                    window.location = obj.data.redirect;
                } else {
                    location.reload();
                }
            } else {
                refreshCaptcha();
                var errors = obj.message;
                var errorHtml = "";
                if (errors === Array) {
                    $.each(errors, function (key, error) {
                        errorHtml += "<div>" + errors + "</div>";
                    });
                } else {
                    errorHtml += "<div>" + errors + "</div>";
                }
                var loginFail = ((obj.data.countLoginFail) ? parseInt(obj.data.countLoginFail) : 0);
               
                if (loginFail >= obj.data.configCaptchaShow) {
                    $('#showCaptchaSignIn').show();
                }
                $('#errorLogin').html(errorHtml);
            }
            lockSignIn = false;
        }).fail(function (jqXHR, textStatus) {
            refreshCaptcha();
            $('#errorLogin').html(trans("Có lỗi xảy ra vui lòng thử lại"));
            lockSignIn = false;
        });
    }
}
;

function loadFirstNotification() {
    if (notificationHeight == 0) {
        loadNotification(0);
        showMore();
    }

}

function loadNotification(offset) {
    is_busy = true;
    $('.spinner').show();
    var limit = 10;
    $.ajax({
        type: "GET",
        url: '/account/get-notification',
        data: {
            'offset': offset,
            'limit':limit
        },
        success: function (data) {

            if (data.trim() == '') {
                if (offset == 0) {
                    $('.itemEmpty').show();
                }
                $('.spinner').hide();
            } else {
                setTimeout(function(){
                    $('.spinner').hide();
                    if (offset == 0) {
                        $('#notificationsContent .item').remove();
                    }
                    $('#notificationsContent').append(data);
                    is_busy = false;
                    notificationHeight = $('#notifications-scroll .item').height() * $('#notifications-scroll .item').length;
                }, 3000);
            }
        },
        fail: function (data) {
            setTimeout(function(){
                $('.spinner').hide();
            }, 3000);
        }
    });
}
function getNumberNotification() {
    $.ajax({
        type: "GET",
        url: '/account/get-number-notification',
        success: function (data) {
            if(data.data.count > 0){
                $('#notification-count').text(data.data.count);
            }
        },
        fail: function (data) {
        }
    });
}



function showMore() {

    var win = $('#notifications-scroll');
    // var win = $(window);

    // Each time the user scrolls
    win.scroll(function () {
        notificationHeight = $('#notifications-scroll .item').outerHeight() * $('#notifications-scroll .item').length;

        // ELement hiển thị chữ loadding
        // $loadding = $('#loadding');

        // Nếu màn hình đang ở dưới cuối thẻ thì thực hiện ajax
        if (($(win).scrollTop() + $(win).height()) >= notificationHeight) {
            // Nếu đang gửi ajax thì ngưng
            if (is_busy == true) {
                return false;
            }
            // Nếu hết dữ liệu thì ngưng
            if (stopped == true) {
                return false;
            }

            loadNotification($('#notificationsContent .item').length);

            // alert('Scroll active');
        }
    });
}

var is_busy = false;

// Biến lưu trữ rạng thái phân trang
var stopped = false;

var notificationHeight = 0;

var UserPlaylist = function () {

    function addFunctionMore() {

        $(document).on("click", '.function-more', function (event) {
            var x = event.screenY;
            var x2 = $(this).children('.popover-more').height();
            var heightWindow = $(window).height();
            var totalHeight = x + x2 + 20; //20 is height of button
            $(".function-more .popover-more").hide();
            $(".function-add-playlist .content").hide();
            if (totalHeight > heightWindow) {
                $('.popover-more').addClass('popover-more-top');
                $('.addVideoToPLBox').addClass('popover-more-top');
            } else {
                $('.popover-more').removeClass('popover-more-top');
                $('.addVideoToPLBox').removeClass('popover-more-top');
            }
            $(this).children('.popover-more').show();
        });

        var $win = $(window); // or $box parent container

        $win.on("click.Bst", function (event) {
            if ($(".function-more").has(event.target).length === 0 && !$(".function-more").is(event.target)) {
                $('.popover-more').removeClass('popover-more-top');
                $('.addVideoToPLBox').removeClass('popover-more-top');
                $(".function-more .popover-more").hide();
                $(".function-add-playlist .content").hide();
            }
        });
    }

    function addPlaylistModal() {

        $(document).on("click", '.function-add-playlist', function () {
            if ($('#userId').val() == "") {
                $('#modalLogin').show();
                return;
            }
            $('#videoIdSelected').val($(this).attr('data-videoid'));
            var idBox = ($(this).data('idbox') == undefined) ? "" : $(this).data('idbox');
            if ($('#userId').val()) {
                //$(this).parent().hide();
                getMyPlaylist($(this).data('videoid'), idBox);

                $('.function-more .popover-more').hide();

                $('#blockAddMorePL' + idBox + $(this).data('videoid')).show();

            } else {
                $(".popover-more").hide();

                showValidateMsg(trans("Vui lòng đăng nhập để sử dụng tính năng này"), "error");
            }
        });

    }
    function addPlayLater() {
        $(document).on("click", '.function-add-playlater', function () {
            var videoId = $(this).data('videoid');
            $.ajax({
                method: "POST",
                url: '/video/toggle-watch-later',
                data: {
                    'id': videoId,
                    'status': 1,
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                },
                dataType: 'json',
            }).done(function (data) {
                var type = "error";
                if (data.responseCode == '200') {
                    type = "info";
                }
                $(".popover-more").hide();
                showValidateMsg(data.message, type);
            }).fail(function (jqXHR, textStatus) {

            });
        });

    }

    function addShare() {
        $(document).on("click", '.function-add-share', function () {
            $(".function-more .popover-more").hide();
            var url = $(this).data("url");
            var fbUrl = 'https://facebook.com/sharer/sharer.php?u=' + url;
            var ggUrl = 'https://plus.google.com/share?url=' + url;
            var dataId = $(this).attr("data-id");

            $('#centerShare .btn-facebook').attr('pref', fbUrl);
            $('#centerShare .btn-google').attr('pref', ggUrl);
            $('#centerShare .btn-facebook').attr('data-id', dataId);
            $('#centerShare .btn-google').attr('data-id', dataId);

			$('#centerShare .btn-google').attr('href', ggUrl);

			$.ajax({
                type: "GET",
                url: '/default/get-link-share',
                dataType: 'json',
                data: {
                    'video_id': $(this).attr("data-id")
                },
                success: function (data) {
                    if(data.shareLink){
						$('#centerShare .btn-facebook').attr('href', data.shareLink);
                    }
                }
            });
            $('#centerShare').show();
            return false;


            $('#centerShare').hide();
            //window.open($(this).attr("pref"), '_blank');
            $.ajax({
                type: "GET",
                url: '/default/get-link-share',
                dataType: 'json',
                data: {
                    'video_id': $(this).attr("data-id")
                },
                success: function (data) {
                    if(data.shareLink){
						$('#centerShare .btn-facebook').attr('href', data.shareLink);
                    }
                }
            });
        });
    }

    function getMyPlaylist(videoId, idbox) {
        if ($('#userId').val()) {
            if ($('#myPlaylistBox' + idbox + videoId).attr('data-firstload') == 0) {
                $.ajax({
                    method: "GET",
                    url: '/account/my-playlist-with-video',
                    data: {
                        'video_id': videoId
                    },

                }).done(function (data) {
                    try {
                        var objData = $.parseJSON(data);
                    } catch (err) {
                        if (data) {
                            $('#myPlaylistBox' + idbox + videoId).attr('data-firstload', 1);
                            $("#myPlaylistBox" + idbox + videoId).html(data);
                        }
                    }
                }).fail(function (jqXHR, textStatus) {
                });
            } else {
            }
        } else {
            $(".popover-more").hide();
            $("#blockAddMorePL" + videoId).hide();

            showValidateMsg(trans("Vui lòng đăng nhập để sử dụng tính năng này"), "error")
        }
    }

    function addVideoToPlaylist() {
        $('body').on('click', '.addVideoPlaylist', function () {

            var status = this.checked ? 1 : 0;
            var playlistId = $(this).data('playlistid');
            var videoId = $(this).parent().parent().parent().parent().parent().data('videoid');
            processAddVideoToPlaylist(playlistId, videoId, status, 'add', '');
        });
    }

    function processAddVideoToPlaylist(playlistId, videoId, status, type, message) {
        $.ajax({
            method: "POST",
            url: '/playlist/video-to-playlist',
            data: {
                'id': playlistId,
                'video_id': videoId,
                'status': status,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (response) {
            try {
                if (response.data.isAdd == true) {
                    $('.addVideoToPLBox').fadeOut();
                    if (type == 'add') {
                        toastr.success(trans('Thêm video thành công vào playlist.'), {closeButton: true});
                    } else {
                        $('.addVideoPlaylist' + playlistId + '_' + videoId).attr('checked', 'checked');
                        toastr.success(message, {closeButton: true});
                    }
                } else {
                    toastr.success(trans('Bỏ video thành công khỏi playlist.'), {closeButton: true});
                }
            } catch (err) {
                toastr.error(response.message, {closeButton: true});
            }
            // $('#blockAddMorePL').hide();
        }).fail(function (jqXHR, textStatus) {
            toastr.error(trans('Có lỗi xảy ra vui lòng thử lại'), {closeButton: true});
        });
    }

    function showModalCreatePlaylist() {
        $('body').on('click', ' .btn-addplaylist,.btnShowAddNewPLbtnShowAddNewPL', function () {
            $('#blockAddMorePL').hide();
            $('#createPlaylistModal').show();
        });
    }

    function createPlaylist() {
        $('#submitCreatePlaylist').on('click', function () {

            var namePlaylist = $("input[name='namePlaylist']").val();
            var descriptionPlaylist = $("input[name='descriptionPlaylist']").val();
            $.ajax({
                type: "POST",
                url: '/playlist/create',
                dataType: 'json',
                data: {
                    'name': namePlaylist,
                    'description': descriptionPlaylist,
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                },
                success: function (data) {
                    if (data.responseCode != 401) {
                        $('#createPlaylistModal').fadeOut(200);
                        if (data.responseCode == 200) {
                            var playlistId = data.data.playlist.id;
                            var videoId = $('#videoIdSelected').val();
                            if (!videoId && document.getElementById("contentId")) {
                                videoId = $('#contentId').val();
                            }
                            $('.myPlaylistBox').append('<p><label><input type="checkbox" class="ipt-check addVideoPlaylist addVideoPlaylist' + playlistId + '_' + videoId + '" data-playlistid="' + data.data.playlist.id + '">' + htmlEntities(data.data.playlist.name) + '</label></p>');
                            setTimeout( function () {
                                processAddVideoToPlaylist(playlistId, videoId, 1, "create", data.message);
                            }, 1000);
                        } else {
                            toastr.error(data.message, {closeButton: true});
                        }
                    } else if (data.responseCode == 401) {
                        window.location = "/auth/login";
                    }
                }
            });
        });
    }
    ;
    function triggerComment() {
        $(document).on("click", '.function-trigger-comment', function () {
            var videoId = $(this).data('videoid');
            var status = $(this).data('status');
            var statusCanComment = 1;
            if(status == 1){
                statusCanComment = 0;
            }else{
                statusCanComment = 1;
            }
            $.ajax({
                method: "POST",
                url: '/default/trigger-comment',
                data: {
                    'id': videoId,
                    'status': 'auto',
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                },
                dataType: 'json',
            }).done(function (data) {
                var type = "error";
                var text = "";
                if (data.responseCode == '200') {
                    type = "info";
                    if(data.status == 1){
                        text = trans('Tắt bình luận');
                        $(".function-trigger-comment-"+videoId).text(text);
                        $(".function-trigger-comment-"+videoId).data('status',0);
                    }else{
                        text = trans('Bật bình luận')
                        $(".function-trigger-comment-"+videoId).text(text);
                        $(".function-trigger-comment-"+videoId).data('status',1);
                    }
                }
                $(".popover-more").hide();
                showValidateMsg(data.message, type);
            }).fail(function (jqXHR, textStatus) {

            });
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            addFunctionMore();
            addPlayLater();
            addPlaylistModal();
            addShare();
            // getMyPlaylist();
            addVideoToPlaylist();
            showModalCreatePlaylist();
            createPlaylist();
            triggerComment();
        },
        createPlaylist: function () {
            return createPlaylist();
        },
    };
}();
$(document).ready(function () {

    UserPlaylist.init();
    $('#copy-success').hide();
    // MyPlaylist.init();
    $('body').on('click', '#shareVideoDetail', function () {
        // if ($('#userId').val() == ""  && $('#isCheck').val() == 0) {
        //     $('#modalLogin').show();
        //     return;
        // }
        $('#containerShare').show();
        // $('#popupShare').show();
    });
    $('body').on('click', '#embedVideo', function () {
        $('#copy-success').hide();
        $('#copy-success-iFrame').hide();
        // $('#shareModal').show();
        $('#shareModal').show();
        // $('#popupShare').show();
    });

    // MyPlaylist.init();
    $('body').on('click', '#closeVideoShare', function () {
        $('#copy-success').hide();

        $('#shareModal').hide();
    });
    $('body').on('click', '#closeContainerShare', function () {
        $('#containerShare').hide();
        $('#copy-success').hide();
    });

    // MyPlaylist.init();
    $('body').on('click', '#copyVideoShare', function () {/* Get the text field */
        var copyText = document.getElementById("embed-code");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
        $('#copy-success-iFrame').show();
    });

    $('body').on('click', '#copyLinkShare', function () {/* Get the text field */
        var copyText = document.getElementById("shareWebsite");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
        $('#copy-success').show();
    });

    $('body').on('click', '#shareDetail', function () {
        $('#containerShare').hide();
        var url = $(this).attr('data-url');
        var dataId= $(this).attr('data-id');
        var fbUrl = 'https://facebook.com/sharer/sharer.php?u=' + url;
        var ggUrl = 'https://plus.google.com/share?url=' + url;
        // console.log(data-url);
        // console.log("gau gau");
        // console.log(url);

        $('#popupShare .btn-facebook').attr('data-id', dataId);
        $('#popupShare .btn-google').attr('data-id', dataId);
        $('#popupShare .btn-facebook').attr("href", "https://facebook.com/sharer/sharer.php?u=" + url);
        $('#popupShare .btn-google').attr('href', ggUrl);
			$.ajax({
                type: "GET",
                url: '/default/get-link-share',
                dataType: 'json',
                data: {
                    'video_id': $(this).attr("data-id")
                },
                success: function (data) {
                    if(data.shareLink){
						$('#popupShare .btn-facebook').attr('href', data.shareLink);
                    }
                }
            });

        $('#popupShare').show();
    });
    $('.loadingBoxComment').show();
    // enter login

    $('#username, #password, #loginform-verifycode').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            login();
        }
    });

    $('#iptNamePlaylist').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $('#submitCreatePlaylist').trigger('click');
        }
    });

    $('.btn-rd-watch-later').click(function () {
        localStorage.setItem("clickPlayRandomWatchLater", 1);
        localStorage.setItem("firstLoadWatchLater", true);
        window.location = "/playlist/detail-watch-later";
    });
    $('.btn-watch-later').click(function () {
        localStorage.setItem("clickPlayRandomWatchLater", 0);
        localStorage.setItem("firstLoadWatchLater", true);
        window.location = "/playlist/detail-watch-later";
    });
    if (localStorage.clickPlayRandomWatchLater == 1 && $('#isWatchLater').val() == '/playlist/detail-watch-later') {
        $('#btnPlayRandomList').trigger('click');
    }
//----
    $('.btnGetOTP').click(function () {
        window.location = '/account/get-otp';
    });
    //load number notification
    getNumberNotification();
});

// Tab-Pane change function
function tabChange() {
    var tabs = $('.nav-tabs > li');
    
    if(tabs.length > 0) {
        var active = tabs.filter('.active');
        var next = active.next('li').length? active.next('li').find('a') : tabs.filter(':first-child').find('a');
        next.tab('show');
    }
}

$('.tab-pane').hover(function() {
    clearInterval(tabCycle);
}, function() {
    tabCycle = setInterval(tabChange, 5000);
});

// Tab Cycle function
var tabCycle = setInterval(tabChange, 5000)

// Tab click event handler
$(function(){
    $('.nav-tabs a').click(function(e) {
        e.preventDefault();
        clearInterval(tabCycle);
        $(this).tab('show')
        tabCycle = setInterval(tabChange, 5000);
    });
});

function loadCommentById(commentId,videoId,commentStatus) {
    $("#loading-comment").show();
    $.ajax({
        method: "GET",
        url: $('#commentUrl').val(),
        data: {
            'videoId': videoId,
            'commentId': commentId,
            'status':commentStatus
        }
    }).done(function (data) {
        if (data.length > 0) {
            $("#commentContent").append(data);
            $("#commentContent .list-comment .child").removeClass('hidden');
            $("#commentContent .showMore p").hide();
            if ($('#cm_'+commentId).length) {
                $('html, body').animate({
                        scrollTop: $('#cm_'+commentId).offset().top
                }, 'slow');
            }

        }
    }).fail(function (jqXHR, textStatus) {
    });
}

//view detail comment from notification
var url_string = window.location.href;
var url = new URL(url_string);
var videoId = url.searchParams.get("view_comment");
var commentId = url.searchParams.get("comment_id");
var commentStatus = url.searchParams.get("comment_status");
if(commentStatus == null || commentStatus == ''){
    commentStatus = 4;
}

if(videoId != null){
    $('.tablinks').removeClass('active');
    $('.tabcontent').css('height','0px');
    $('.tabcontent').css('overflow','hidden');
    $('#view-comment').addClass('active');
    $('#tab6').css('height','auto');
    $('#tab6').css('overflow','inherit');
    $("#commentContent").empty();
    $("#canLoad").val('0');
    loadCommentById(commentId,videoId,commentStatus);
}

$(document).click(function (e)
{
    // Đối tượng container chứa popup
    var container = $(".sort-video");

    // Nếu click bên ngoài đối tượng container thì ẩn nó đi
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.find('.popover-more').hide();
    }
 });

function timeSince(datetime) {
    var seconds = Math.floor((new Date() - new Date(datetime)) / 1000);

    var interval = seconds / 31536000;

    if (interval > 1) {
        return Math.floor(interval) + trans('năm trước');
    }
    interval = seconds / 2592000;
    if (interval > 1) {
        return Math.floor(interval) + trans('tháng trước');
    }
    interval = seconds / 86400;
    if (interval > 1) {
        return Math.floor(interval) + trans('ngày trước');
    }
    interval = seconds / 3600;
    if (interval > 1) {
        return Math.floor(interval) + trans('giờ trước');
    }
    interval = seconds / 60;
    if (interval > 1) {
        return Math.floor(interval) + trans('phút trước');
    }
    return trans('Vừa xong');
}
