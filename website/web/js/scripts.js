toastr.options.showDuration = 1000;
toastr.options.timeOut = 2000;
toastr.options.positionClass = 'toast-bottom-left';

function canUseWebP() {
    var elem = document.createElement('canvas');

    if (!!(elem.getContext && elem.getContext('2d'))) {
        // was able or not to get WebP representation
        return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
    }
    else {
        // very old browser like IE 8, canvas not supported
        return false;
    }
}

var isSupportWebp = canUseWebP();

// $(function () {

    $(document).on("click", ".feedback-upload", function () {

        $("#feedback_video_id").val($(this).data("id"));
        $("#feedback_content").val("");

        $.ajax(
            {
                type: "GET",
                url: "/video/get-feedback-upload",
                dataType: 'json',
                data: {"id": $(this).data("id")},
                success: function (rawData) {

                    if (rawData.responseCode == 201) {
                        $("#popupUploadFeedback").show();
                    } else {
                        if (rawData.data.status == 0) {
                            $("#popupUploadFeedback").show();
                        } else if (rawData.data.status == 1) {
                            alert(trans("Phản hồi đang được quản trị viên xem xét"));
                        } else if (rawData.data.status == 2) {
                            alert("Nội dung đã được ghi nhận xử lý");
                        } else if (rawData.data.status == 3) {
                            if (rawData.data.reject_reason) {
                                alert(trans("Nội dung không được chấp nhận với lý do: ") + rawData.data.reject_reason);
                            } else {
                                alert(trans("Nội dung không được chấp nhận"));
                            }
                        } else {
                            $("#popupUploadFeedback").show();
                        }
                    }

                },
                error: function () {
                    $("#popupUploadFeedback").show();
                }
            }
        );
    });

    $("#load-infomation").on("click", function () {
        if ($(this).is(":checked")) {
            $.ajax({
                type: "GET",
                url: '/contract/load-infomation',
                dataType: 'json',
                data: {'_csrf': $('meta[name="csrf-token"]').attr("content")},
                success: function (rawData) {

                    if (rawData.responseCode == "200" && rawData.data != null) {
                        $("input[name=msisdn]").val(rawData.data.msisdn);
                        $("input[name=name]").val(rawData.data.name);
                        $("input[name=email]").val(rawData.data.email);
                        $("input[name=id_card_number]").val(rawData.data.id_card_number);
                        $("input[name=id_card_created_at]").val(rawData.data.id_card_created_at);
                        $("input[name=id_card_created_by]").val(rawData.data.id_card_created_by);
                        if ($("#id_card_created_at").val()) {
                            $('#id_card_created_at').datepicker("update", $("#id_card_created_at").val());
                        }


                        if (rawData.data.id_card_image_frontside) {
                            $("#id_card_fontside_viewer").attr("src", rawData.data.id_card_image_frontside);
                            $("#tem_image_id_frontside_image").val(rawData.data.id_card_image_frontside);
                            $("#id_card_fontside_viewer").show();
                        }

                        if (rawData.data.id_card_image_backside) {
                            $("#id_card_backside_viewer").attr("src", rawData.data.id_card_image_backside);
                            $("#tem_image_id_backside_image").val(rawData.data.tem_image_id_backside_image);
                            $("#id_card_backside_viewer").show();
                        }

                        $("#info_type").val(rawData.data.infoType);
                        $("#info_type").attr("name", "info_type");
                    }

                }
            });

        } else {
            $("#info_type").removeAttr("name");
            $("#info_type").val("");
            $("#id_card_fontside_viewer").hide();
            $("#id_card_backside_viewer").hide();
        }

    });

    var lockMouseAnimate = false;

    $('body').on("mouseover", ".animated-image", function () {
        setTimeout(() => {
        }, 300);
        if (lockMouseAnimate) {
            return false;
        }
        lockMouseAnimate = true;
        $(this).parent().removeAttr('title');
        $(this).removeAttr('title');

        $(".animated-image").each(function () {
            if($(this).attr("src")!= $(this).data("src")){
                $(this).attr("src", $(this).data("src"));
            }
        });

        $(this).addClass('animate-running');

        checkImageExists($(this).data("display"), $(this),
            replaceImage,
            function () {
            }
        );
        lockMouseAnimate = false;
    });

    $('body').on("mouseleave", ".animated-image", function () {
        checkImageExists($(this).data("src"), $(this),
            replaceImage2,
            replaceImage3
        );
    });


    $("#activate-form").submit(function () {
        var activateCode = $("#activateform-code").val().trim();
        if (activateCode.length > 50 || activateCode.length == 0) {
            showValidateMsg(trans("Mã kích hoạt bắt buộc nhập, từ 1 đến 50 kí tự"), "error")
            $("#activateform-code").focus();
            return false;
        }
        if (document.getElementById('activateform-verifycode')) {
            var captchaCode = $("#activateform-verifycode").val().trim();
            if (captchaCode.length == 0) {
                showValidateMsg(trans("Mã captcha bắt buộc nhập"), "error");
                $("#activateform-verifycode").focus();
                return false;
            }
        }
        return true;
    });
    //Lazy scroll image -----------
    processScroll();
    $(window).bind('scroll', processScroll);
    //---------------------

    if (document.getElementById('datepicker_fromdate')) {
        var evenStartDate = $("#event_start_date").val();

        var dateFormat = "dd/mm/yy",
            from = $("#datepicker_fromdate")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: dateFormat
                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                }).attr('readonly', 'readonly'),
            to = $("#datepicker_todate").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
                .on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));

                }).attr('readonly', 'readonly');
        var nowT = new Date();
        var dayT = ("0" + nowT.getDate()).slice(-2);
        var monthT = ("0" + (nowT.getMonth() + 1)).slice(-2);
        var todayT = (dayT) + "/" + (monthT) + "/" + nowT.getFullYear();

        // to.datepicker("option", "maxDate", $.datepicker.parseDate(dateFormat, todayT));
        from.datepicker("option", "maxDate", $.datepicker.parseDate(dateFormat, todayT));
        if (evenStartDate) {
            to.datepicker("option", "minDate", $.datepicker.parseDate("yy-mm-dd", evenStartDate));
            from.datepicker("option", "minDate", $.datepicker.parseDate("yy-mm-dd", evenStartDate));
        }


        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
    }

    $("#form-award-week").on('submit', function () {

        if ($("#datepicker_fromdate").val().length && $("#datepicker_todate").val().length) {
            return true;
        }

        showValidateMsg(trans("Yêu cầu nhập Từ ngày-Đến ngày"), 'error');
        return false;
    });

    $("#form-award-month").on('submit', function () {
        if ($("#month_report").val().length) {
            return true;
        }
        showValidateMsg(trans("Yêu cầu chọn Tháng"), 'error');
        return false;
    });

    if (document.getElementById('id_card_created_at')) {
        $("#id_card_created_at").datepicker({
            dateFormat: 'dd/mm/yy',
            todayBtn: true,
            language: "mz",
            closeBtn: true,
            autoclose: true,
            todayHighlight:true,
            endDate: new Date(new Date().setDate(new Date().getDate()))
        });
        // $('#id_card_created_at').datepicker("update", $("#id_card_created_at").val());

    }

    //Submit form Contract OTP(contract)
    $("#form-contract-otp").submit(function () {
        $(".error-message-contract").remove();

        return true;
    });

    //Submit form Contract Account
    $("#form-contract-account").submit(function () {
        $(".error-message-contract").remove();

        //Trim space all input
        $("#form-contract-account input").each(function () {
            $(this).val($.trim($(this).val()));
        });
        if ($("#payment_type").val() == "BANK_ACCOUNT") {

            document.getElementById("tax_code2").removeAttribute("name");
            document.getElementById("address2").removeAttribute("name");
            document.getElementById("tax_code1").setAttribute("name", "tax_code");
            document.getElementById("address1").setAttribute("name", "address");

            if (!$("input[name=account_number]").val()) {
                $("input[name=account_number]").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Tài khoản ngân hàng') +"</p>").insertAfter("input[name=account_number]");
                return false;
            }
            if (!$("input[name=bank_name]").val()) {
                $("input[name=bank_name]").focus();

                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ngân hàng') +"</p>").insertAfter($("input[name=bank_name]").parent().parent());

                return false;
            }
            if (!$("input[name=bank_department]").val()) {
                $("input[name=bank_department]").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập chi nhánh ngân hàng') +"</p>").insertAfter("input[name=bank_department]");
                return false;
            }
            if (!$("#address1").val()) {
                $("#address1").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập địa chỉ') +"</p>").insertAfter("#address1");
                return false;
            }
            if (!$("#tax_code1").val()) {
                $("#tax_code1").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập mã số thuế') +"</p>").insertAfter("#tax_code1");
                return false;
            }

        } else {

            document.getElementById("tax_code1").removeAttribute("name");
            document.getElementById("address1").removeAttribute("name");
            document.getElementById("tax_code2").setAttribute("name", "tax_code");
            document.getElementById("address2").setAttribute("name", "address");

            if (!$("input[name=payment_mobile_number]").val()) {
                $("input[name=payment_mobile_number]").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số điện thoại') +"</p>").insertAfter("input[name=payment_mobile_number]");
                return false;
            }
            if (!$("#address2").val()) {
                $("#address2").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập địa chỉ') +"</p>").insertAfter("#address2");
                return false;
            }
            if (!$("#tax_code2").val()) {
                $("#tax_code2").focus();
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập mã số thuế') +"</p>").insertAfter("#tax_code2");
                return false;
            }


        }

        return true;

    });
    //Submit form contract info
    $("#form-contract").submit(function () {
        //Trim space all input
        $("#form-contract input").each(function () {
            if (this.name != 'id_card_image_frontside' && this.name != 'id_card_image_backside') {
                $(this).val($.trim($(this).val()));
            }
        });

        $(".error-message-contract").remove();
        var temError = false;
        var hasFocus = false;
        if (!$("input[name=name]").val()) {
            $("input[name=name]").focus();
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập tên') +"</p>").insertAfter("input[name=name]");
            temError = true;
            hasFocus = true;
        }
        if (!$("input[name=email]").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=email]").focus();
            }

            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập email') +"</p>").insertAfter("input[name=email]");
            temError = true;
        } else {
            if (!validateEmail($("input[name=email]").val())) {
                $("<p class='error-message-contract'>"+ trans('Định dạng email không đúng') +"</p>").insertAfter("input[name=email]");
                temError = true;
            }
        }

        if (!$("input[name=id_card_number]").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_number]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số chứng minh thư') +"</p>").insertAfter("input[name=id_card_number]");
            temError = true;
        }
        if (!$("input[name=id_card_created_by]").val()) {

            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_created_by]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Nơi cấp chứng minh thư') +"</p>").insertAfter(".id_card");
            temError = true;
        }

        if (!$("input[name=id_card_created_at]").val()) {

            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_created_at]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ngày cấp chứng minh thư') +"</p>").insertAfter(".id_card");
            temError = true;
        }

        if ($("#info_type").val().length == 0) {
            if (!$("input[name=id_card_image_frontside]").val() && !$("#tem_image_id_frontside_image").val()) {
                if (!hasFocus) {
                    hasFocus = true;
                    $("input[name=id_card_image_frontside]").focus();
                }
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ảnh mặt trước chứng minh thư') +"</p>").insertAfter("input[name=id_card_image_frontside]");
                temError = true;
            } else if (!hasExtension('id_card_image_frontside', ['.jpg', '.gif', '.png', '.jpeg']) && (!$("#tem_image_id_frontside_image").val() || $("input[name=id_card_image_frontside]").val())) {
                if (!hasFocus) {
                    hasFocus = true;
                    $("input[name=id_card_image_frontside]").focus();
                }
                $("<p class='error-message-contract'>"+ trans('Định dạng ảnh mặt trước cho phép') +" (jpg, gif, png, jpeg)</p>").insertAfter("input[name=id_card_image_frontside]");
                temError = true;
            }

            if (!$("input[name=id_card_image_backside]").val() && !$("#tem_image_id_backside_image").val()) {
                if (!hasFocus) {
                    hasFocus = true;
                    $("input[name=id_card_image_backside]").focus();
                }
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ảnh mặt sau chứng minh thư') +"</p>").insertAfter("input[name=id_card_image_backside]");
                temError = true;
            } else if (!hasExtension('id_card_image_backside', ['.jpg', '.gif', '.png', '.jpeg']) && (!$("#tem_image_id_backside_image").val() || $("input[name=id_card_image_backside]").val())) {
                if (!hasFocus) {
                    hasFocus = true;
                    $("input[name=id_card_image_backside]").focus();
                }
                $("<p class='error-message-contract'>"+ trans('Định dạng ảnh mặt sau cho phép') +" (jpg, gif, png, jpeg)</p>").insertAfter("input[name=id_card_image_backside]");
                temError = true;
            }
        } else {
            if (!hasExtension('id_card_image_frontside', ['.jpg', '.gif', '.png', '.jpeg']) && (!$("#tem_image_id_frontside_image").val() || $("input[name=id_card_image_frontside]").val())) {
                if (!hasFocus) {
                    hasFocus = true;
                    $("input[name=id_card_image_frontside]").focus();
                }
                $("<p class='error-message-contract'>"+ trans('Định dạng ảnh mặt trước cho phép') +" (jpg, gif, png, jpeg)</p>").insertAfter("input[name=id_card_image_frontside]");
                temError = true;
            }

            if (!hasExtension('id_card_image_backside', ['.jpg', '.gif', '.png', '.jpeg']) && (!$("#tem_image_id_backside_image").val() || $("input[name=id_card_image_backside]").val())) {
                if (!hasFocus) {
                    hasFocus = true;
                    $("input[name=id_card_image_backside]").focus();
                }
                $("<p class='error-message-contract'>"+ trans('Định dạng ảnh mặt sau cho phép') +" (jpg, gif, png, jpeg)</p>").insertAfter("input[name=id_card_image_backside]");
                temError = true;
            }
        }

        if (!$("input[name=msisdn]").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=msisdn]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số điện thoại Unitel') +"</p>").insertAfter("input[name=msisdn]");
            temError = true;
        } else {
            var inputMsisdn = $("input[name=msisdn]").val();
            if ((inputMsisdn % 1 == 0) && (inputMsisdn > 0)) {
            } else {
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số điện thoại Unitel') +"</p>").insertAfter("input[name=msisdn]");
                temError = true;
            }
        }

        if (!$("input[name=otp]").val()) {

            if (!hasFocus) {
                hasFocus = true;
                $("input[name=otp]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập mã OTP') +"</p>").insertAfter("#get_otp_contract");
            temError = true;
        }

        if (temError) {
            return false;
        }
    });
//Submit form contract info
    $("#form-account-infomation").submit(function () {
        //Trim space all input
        $("#form-account-infomation input").each(function () {
            $(this).val($.trim($(this).val()));
        });

        $(".error-message-contract").remove();
        var temError = false;
        var hasFocus = false;
        if (!$("input[name=name]").val()) {
            $("input[name=name]").focus();
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập tên') +"</p>").insertAfter("input[name=name]");
            temError = true;
            hasFocus = true;
        }
        if (!$("input[name=email]").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=email]").focus();
            }

            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập email') +"</p>").insertAfter("input[name=email]");
            temError = true;
        } else {
            if (!validateEmail($("input[name=email]").val())) {
                $("<p class='error-message-contract'>"+ trans('Định dạng email không đúng') +"</p>").insertAfter("input[name=email]");
                temError = true;
            }
        }

        if (!$("input[name=id_card_number]").val()) {

            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_number]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số chứng minh thư') +"</p>").insertAfter("input[name=id_card_number]");
            temError = true;
        }
        if (!$("input[name=id_card_created_by]").val()) {

            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_created_by]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Nơi cấp chứng minh thư') +"</p>").insertAfter(".id_card");
            temError = true;
        }

        if (!$("input[name=id_card_created_at]").val()) {

            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_created_at]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ngày cấp chứng minh thư') +"</p>").insertAfter(".id_card");
            temError = true;
        }
        if (!$("input[name=id_card_image_frontside]").val() && !$("#tem_image_id_frontside_image").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_image_frontside]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ảnh mặt trước chứng minh thư') +"</p>").insertAfter("input[name=id_card_image_frontside]");
            temError = true;
        }

        if (!$("input[name=id_card_image_backside]").val() && !$("#tem_image_id_backside_image").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_image_backside]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập ảnh mặt sau chứng minh thư') +"</p>").insertAfter("input[name=id_card_image_backside]");
            temError = true;
        }

        if (!hasExtension('id_card_image_frontside', ['.jpg', '.gif', '.png', '.jpeg']) && (!$("#tem_image_id_frontside_image").val() || $("input[name=id_card_image_frontside]").val())) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_image_frontside]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Định dạng ảnh mặt trước cho phép') +" (jpg, gif, png, jpeg)</p>").insertAfter("input[name=id_card_image_frontside]");
            temError = true;
        }

        if (!hasExtension('id_card_image_backside', ['.jpg', '.gif', '.png', '.jpeg']) && (!$("#tem_image_id_backside_image").val() || $("input[name=id_card_image_backside]").val())) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=id_card_image_backside]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Định dạng ảnh mặt sau cho phép') +" (jpg, gif, png, jpeg)</p>").insertAfter("input[name=id_card_image_backside]");
            temError = true;
        }

        if (!$("input[name=msisdn]").val()) {
            if (!hasFocus) {
                hasFocus = true;
                $("input[name=msisdn]").focus();
            }
            $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số điện thoại Unitel') +"</p>").insertAfter("input[name=msisdn]");
            temError = true;
        } else {
            var inputMsisdn = $("input[name=msisdn]").val();
            if ((inputMsisdn % 1 == 0) && (inputMsisdn > 0)) {
            } else {
                $("<p class='error-message-contract'>"+ trans('Yêu cầu nhập Số điện thoại Unitel') +"</p>").insertAfter("input[name=msisdn]");
                temError = true;
            }
        }


        if (temError) {
            return false;
        }
    });

    $("#check-accept-account").on("click", function () {
        if ($(this).is(':checked')) {
            $("#btn-submit-account-info").removeAttr("disabled");
            $("#btn-submit-account-info").addClass("btn-upload");
            $("#btn-submit-account-info").removeClass("btn-cancel");
        } else {
            $("#btn-submit-account-info").attr("disabled", "disabled");
            $("#btn-submit-account-info").addClass("btn-cancel");
            $("#btn-submit-account-info").removeClass("btn-upload");
        }
    });

    //xu ly o trang chu
    if (document.getElementById('home-page-right')) {
        slideBoxVideo();
        //--
        $("#video_recommend .item:gt(7)").css("display", "none");
        $("#video_recommend .item:gt(7)").addClass("hide-video-item");

        $("#video_home .item:gt(7)").css("display", "none");
        $("#video_home .item:gt(7)").addClass("hide-video-item");

        $("#video_hot .item:gt(7)").css("display", "none");
        $("#video_hot .item:gt(7)").addClass("hide-video-item");

        $("#video_hot_2 .item:gt(7)").css("display", "none");
        $("#video_hot_2 .item:gt(7)").addClass("hide-video-item");
        //Click xem them
        $(".href-view-more").on("click", function (e) {
            var countHidden = $(this).parent().find(".hide-video-item")
            countHidden.css("display", "block");
            countHidden.removeClass("hide-video-item");

            if (countHidden.length == 0) {
                window.location = $(this).data('url');
            }
        })
    }
    // xu ly lay OTP hop dong

    $("#get_otp_contract").on("click", function () {
        var msisdn = $("input[name=msisdn]").val()
        $.ajax({
            type: "POST",
            url: '/contract/get-otp',
            dataType: 'json',
            data: {'_csrf': $('meta[name="csrf-token"]').attr("content"), 'msisdn': msisdn},
            success: function (rawData) {
                if (rawData.responseCode == 200) {
                    showValidateMsg(trans("OTP đã gửi thành công"), "success")
                } else {
                    showValidateMsg(rawData.message, "error")
                }
            }
        });
    });


    $("#get_otp_account").on("click", function () {

        $(this).hide();
        $.ajax({
            type: "POST",
            url: '/contract/get-account-otp',
            dataType: 'json',
            data: {
                '_csrf': $('meta[name="csrf-token"]').attr("content"),
                'msisdn': $("#account-info-msisdn").val()
            },
            success: function (rawData) {
                setTimeout(function () {
                    $("#get_otp_account").show();
                }, 3000);
                if (rawData.responseCode == 200) {
                    showValidateMsg(trans("OTP đã gửi thành công"), "success")
                } else {
                    showValidateMsg(rawData.message, "error")
                }
            }
        });
    });

    $(".remove-link-social").on("click", function () {

        var confirmRemove = confirm(trans("Bạn có chắc chắn xoá liên kết?"));
        if (confirmRemove) {
            var type = $(this).data("content");
            $.ajax({
                type: "GET",
                url: '/account/remove-link-social',
                data: {
                    'type': type
                },
                success: function (rawData) {
                    window.location.reload();
                }
            });
        }
    });

    if (document.getElementById('history-search')) {
        getHistorySearch(0);
    }

    if (document.getElementById("google-adwords")) {
        addGAParams("div.content-right a");
    }
    // - Xoa video cua toi..
    $("body").on("click", ".my-video-delete", function () {
        var lang = $(this).attr('language');
        var videoId = $(this).attr('data-videoid');
        var message;
        if(lang == 'en'){
            message = 'Definitely delete the video?';
        }else
        {
            message ='Excluir definitivamente o vídeo?';
        }
        if(confirm(message)) {
            //DeleteVideo
            $.ajax({
                type: "POST",
                url: '/default/delete-video',
                data: {
                    'id': $(this).attr('data-videoid'),
                    '_csrf': $('meta[name="csrf-token"]').attr("content")

                },
                dataType: 'json',
                success: function (data) {
                    if (data.responseCode == 200) {
                        $(".video-item-" + videoId).hide();
                        alert(trans("Thành công"));
                    }
                }
            });
        }

    });

    //-- Sua video cua toi
    $("body").on("click", ".my-video-edit", function () {
        var videoId = $(this).attr("data-videoid");
        $("#edit_video_name").val($(".video-item-" + videoId + " .name-video").html());
        $("#edit_video_id").val(videoId);
        $("#popupEditVideo").show();
    });
    //-- Sua video cua toi
    $("#edit_video_button").on("click", function () {
        if ($("#edit_video_name").val().length <= 0) {
            alert(trans("Vui lòng nhập tên video"));
            return false;
        }
        $("#edit_video_form").submit();

    });


    if ($('#popupAlert .row-form').text()) {
        showPopup($("#popupAlert"));
    }
    $('#keyword').on("keydown keypress keyup", function (evt) {
        var e = evt.which;
        var result = false;
        var blackKeys = [27, 9, 13, 37, 38, 39, 40, 17, 18];
        for (var i = 0; i < blackKeys.length; i++) {
            if (e == blackKeys[i]) {
                result = true;
            }
        }
        if (result) { //  ESC
            return false;
        } else {
            $("#contentSuggess").show();
            delay(function () {
                autoSearch();
            }, 100);
        }
    });
    $('.btn-search').on("click", function (e) {
        var key = $('#keyword').val();
        insertHistorySearch(key);
    });
    $('.box-search .input-text').on('keydown', function (e) {
        var key = $('#keyword').val();
        if (e.which == 13) {
            insertHistorySearch(key);
            $("#search-item").submit();
        }
    });
    $('#history-search .remove-video').on('click', function () {

        var dataId = $(this).attr("data-id");
        removeHistorySearch(dataId);
        // var nameVideo = $(this).data("name");
        // var item = "";
        // var typeDelete = document.getElementById("typeDelete") ? document.getElementById("typeDelete").value : "";
        // switch (typeDelete) {
        //     case "HISTORYSEARCH":
        //         item = "lịch sử tìm kiếm";
        //         break;
        // }
        // $("#modal-confirm .notice").html('Bạn có muốn xóa video ' + htmlEntities(nameVideo) + ' khỏi ' + item + ' không ?');
        // $("#modal-confirm .btn-ok").attr("data-id", dataId);
        // $("#modal-confirm").addClass("modal-vietnt delete-video");
        // showPopup("#modal-confirm");
    });
    // Hien thi menu tai khoan
    clickMenu();
    clickSearch();
    //Chan zoom
    var obj = document.body;  // obj=element for example body
    if (obj.addEventListener) {
        obj.addEventListener('DOMMouseScroll', mouseWheel, false);
        obj.addEventListener("mousewheel", mouseWheel, false);
    } else
        obj.onmousewheel = mouseWheel;

    function mouseWheel(e) {
        // disabling
        e = e ? e : window.event;
        if (e.ctrlKey) {
            if (e.preventDefault)
                e.preventDefault();
            else
                e.returnValue = false;
            return false;
        }
    }

    //Search Channel
    $('#keyword_channel').on("keydown", function (evt) {
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };
        var e = evt.which;
        if (e == keys.RETURN) {
            searchChannel();
        }
    });
    $('#keyword_channel').on("keydown keypress keyup", function (evt) {
        keys = {
            ESC: 27,
            TAB: 9,
            RETURN: 13,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40
        };
        var e = evt.which;
        if (e == keys.ESC || e == keys.TAB || e == keys.LEFT || e == keys.UP || e == keys.RIGHT || e == keys.DOWN) { //  ESC
            return false;
        } else {
            delay(function () {
                autoSearchChannel();
            }, 100);
        }
    });

    $("body").on("click", ".add-channel-related", function () {

        var channel_id = $(this).data("id")
        $.ajax({
            type: "POST",
            url: '/channel/add-channel-related',
            dataType: 'json',
            data: {
                'channel_related_id': channel_id,
                '_csrf': $('meta[name="csrf-token"]').attr("content")

            },
            success: function (data) {
                if (data.responseCode == '200') {
                    showValidateMsg(trans('Thêm thành công'));
                    $('#channel_related_' + channel_id).remove();
                } else if (data.responseCode == '401') {
                    window.location = '/auth/login';
                } else {
                    showValidateMsg(data.message, 'error');
                }

            }
        });

    });


// });

var imageExtensions = [".jpg", ".jpeg", ".gif", ".png"];
var videoExtensions = [".mp4"];

function slideBoxVideo() {
    $(".has-slider").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 4,
        slideBy: 4,
        dots: false,
        nav: true,
        navText: [],
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 4]
    });
}


function validateSingleInput(oInput, validFileExtensions) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < validFileExtensions.length; j++) {
                var sCurExtension = validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                //alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

function showValidateMsg(msg, type) {
//    $("#popupMessage .row-form").html(msg);
//    hideModal();
    if (type == 'error') {
        toastr.error(msg);
    } else {
        toastr.success(msg);
    }

//    showPopup($("#modal-login"));

}

function showPopup(id) {
    $(id).fadeIn();
}

function validateEditChannel() {
    var minName = $('#name-min-lenght').val();
    var maxName = $('#name-max-lenght').val();
    var minDes = $('#description-min-lenght').val();
    var maxDes = $('#description-max-lenght').val();
    var validate = true;
    if (!validateSingleInput(document.getElementById('file_banner'), imageExtensions)) {
        $('#file_banner').focus();
        validate = false;
        $("#errorBanner").html("<div>"+ trans('Định dang file ảnh được phép')+": jpg, png, gif</div>");
    }


    if (!validateSingleInput(document.getElementById('file_avartar'), imageExtensions)) {
        $('#file_avartar').focus();
        validate = false;

        $("#errorAvartar").html("<div>"+ trans('Định dang file ảnh được phép')+": jpg, png, gif</div>");
    }
    var name = $('#name').val().trim();

    if (name.length == 0 || name.length > maxName || name.length < minName) {
        //alert(name);
        $('#name').focus();
        validate = false;
        $("#errorName").html("<div>"+ trans('Quý khách vui lòng nhập tên từ')+" " + minName + " " + trans('đến') + " " + maxName + " " + trans('kí tự') +"</div>");
    }
    var des = $('#comment').val().trim();
    //alert(des);
    if (des.length == 0 ||des.trim().length > maxDes || des.trim().length < minName) {
        //alert("aaaaa"+des);
        validate = false;
        $('#comment').focus();
        $("#errorDes").html("<div>" + trans('Quý khách vui lòng nhập mô tả từ') +" " + minName + " " + trans('đến') + " " + maxDes + " " + trans('kí tự') +"</div>");
    }
    if (validate) {
        $("#editChannel").submit();
    }
}

function eraseText() {
    document.getElementById("name").value = "";
    document.getElementById("comment").value = "";
    document.getElementById("file_avartar").value = "";
    document.getElementById("file_banner").value = "";
}

function followChannelAndNotify(channel_id, status, notification_type, sendNotify) {

    var sta = document.getElementById("status_" + channel_id);
    if (sta && sendNotify) {
        status = $("#status_" + channel_id).val();
    }
    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': status,
            'notification_type': notification_type,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {
                if (status == 0) {
                    showValidateMsg(trans('Hủy theo dõi kênh thành công'), "info");
                    if (document.getElementById("action") && document.getElementById("action").value) {
                        $('.channel-item-' + channel_id).remove();
//                        if ($('.list-video').hasClass('item-channel') == false) {
//                            $('.list-video').html('<p>Không có dữ liệu</p>');
//                        }
                    }
                } else {
                    if ($("#hotChannelUrl").val() == 1) {
                        $('.channel-item-' + channel_id).remove();
                    }
                    if (sendNotify == true) {
                        showValidateMsg(trans('Đăng kí theo dõi kênh thành công'), "info");
                    } else {
                        showValidateMsg(trans('Thành công'), "info");
                    }
                }
                if (document.getElementById("notification-type")) {
                    $("#notification-type").val(notification_type);
                }
                if (data.data.isFollow > 0) {
                    $("#icon-follow").attr("class", "btn-follow btn-followed");
                    $('.btn-follow .statusFollow').html(trans("Đã theo dõi"));
                    $("#status_" + channel_id).val(0);
                    $(".status-follow").show();
                    $("#onOff").css("display", "inline-block"); //hien thi
                    if (notification_type > 0) {
                        $(".notify").attr("class", "fa icon-ringoff notify");
                    } else {
                        $(".notify").attr("class", "fa icon-notify notify");
                    }

                } else {
                    $(".status-follow").hide();
                    $("#icon-follow").attr("class", "btn-follow");
                    $('.btn-follow .statusFollow').html(trans("Theo dõi"));
                    $("#status_" + channel_id).val(1);
                    $("#onOff").css("display", "none"); //an di
                }
                var count = (data.data.followCount) ? data.data.followCount.format(0, 3, '.', ',') : data.data.followCount;
                $("#num-follow").html(count);
            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, "error");
            }

        }
    });
}


function changeNotificationType(channel_id, notification_type) {
    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': 1,
            'notification_type': notification_type,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {

                if (notification_type == 0) {
                    showValidateMsg(trans('Tắt thông báo thành công'), "info");
                    $(".list-notify .item").removeClass("selected");
                } else {
                    showValidateMsg(trans('Chuyển trạng thái thông báo thành công'), "info");
                }

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, "error");
            }

        }
    });
}


Number.prototype.format = function (n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));
    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};
$('#onOff').on('click', function () {
    var target = $(this).data('target');
    var notificationType = $("#notification-type").val();
    $("#accept" + notificationType).prop("checked", true);
    showPopup($(target));
});
$('#modal-notify .btn-ok').on('click', function () {
    $('#modal-notify').css('display', 'none');
    var value = document.querySelector('input[name=accept]:checked').value;
    var id = $("#channel_id").val();
    followChannelAndNotify(id, 1, value, false);
});

$(document).on('click', '.rmVideoHistory', function () {
    var dataId = $(this).data("id");
    var nameVideo = $(this).data("name");

    var item = "";
    var typeDelete = document.getElementById("typeDelete") ? document.getElementById("typeDelete").value : "";
    switch (typeDelete) {
        case "HISTORYVIEW":
            item = trans("lịch sử xem");
            break;
        case "PLAYLATE":
            item = trans("danh sách xem sau");
            break;
        default :
            item = "playlist";
    }
    $("#modal-confirm .notice").html(trans('Bạn có muốn xóa video ') + htmlEntities(nameVideo) + ' ' + trans("khỏi") + ' ' + item + ' ' + trans("không") +' ?');
    $("#modal-confirm .btn-ok").attr("data-id", dataId);
    showPopup("#modal-confirm");
});


$('#modal-confirm .btn-ok').on('click', function () {
    $('#modal-confirm').css('display', 'none');

    var videoId = ($(this).attr("data-id") !== undefined) ? $(this).attr("data-id") : null;
    //alert($(this).attr("data-id"));
    if (document.getElementById("typeDelete") == null) {
        return;
    }
    var typeDelete = document.getElementById("typeDelete") ? document.getElementById("typeDelete").value : "";

    //alert(typeDelete);//HISTORYVIEW
    switch (typeDelete) {
        case "HISTORYVIEW": // Xoa lich su xem
            if (videoId) {
                //alert("aaaaaa---"+videoId);
                deleteHistoryView(videoId);
                videoId = null;
            } else {
                //alert("2---"+videoId);
                deleteAllHistoryView();
            }
            break;
        case "PLAYLATE":
            if (videoId) {
                addOrDelPlayLate(videoId, 0);
            } else {
                deleteAll();
            }
            break;
        case "HISTORYSEARCH": // Xoa lich su tim kiem
            //alert("111---"+videoId);
            if (videoId) {
                //alert("3---"+videoId);
                removeHistorySearch(videoId);
            } else {
                //alert("4---"+videoId);
                removeHistorySearch();
            }
            break;
        case "PLAYLIST":
            if (videoId) {
                deletePlaylist(videoId);// Mac dinh xoa playlist
            } else {
                deleteAllPlaylist();
            }
            break;
    }
    videoId = null;

});

function addOrDelPlayLate(videoId, status) {
    $.ajax({
        type: "POST",
        url: '/video/toggle-watch-later',
        dataType: 'json',
        data: {
            'id': videoId,
            'status': status,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == 401) {
                window.location = "/auth/login";
            } else {
                if (data.responseCode == 200) {
                    var msg = data.message;
                    if (status == 0) {
                        var number = $("#number").val();
                        msg = trans("Xóa video thành công ");
                        $(".item-" + videoId).remove();
                        $("#number").val(number - 1);
                        var x = document.getElementsByClassName("number");
                        var i;
                        for (i = 0; i < x.length; i++) {
                            console.log(x[i].id);
                            $("#" + x[i].id).html(i + 1);
                        }
                    }
                    showValidateMsg(msg, "info");
                } else {
                    showValidateMsg(data.message, 'error');
                }

            }
        }
    });
}
;

function deleteHistoryView(videoId) {

    $.ajax({
        type: "POST",
        url: '/account/delete-history-view',
        dataType: "json",
        data: {
            'ids': videoId,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var message = data.message;
            var type = "error";
            if (data.responseCode == 200) {
                type = "info";
                $(".video-item-" + videoId).remove();
                message = trans("Xóa video khỏi lịch sử thành công");
            }
            showValidateMsg(message, type);

        }
    });

    // return result;
}

function removeHistorySearch(id) {
    var value = "KEYS_SEARCH";
    var result = false;
    if (!supportsLocalStorage()) {     // Ko ho tro
        console.log('Not support');
    } else {
        if (id) {
            var items = localStorage.getItem(value);
            var item = JSON.parse(items);
            if (item.length > 0) {
                for (var index = 0; index < item.length; ++index) {
                    var obj = JSON.parse(item[index]);
                    if (obj.id == id) {
                        item.splice(index, 1);
                        var encode = JSON.stringify(item);
                        localStorage.setItem(value, encode);
                        result = true;
                        break;
                    }

                }

            }
        } else {
            localStorage.removeItem(value);
            result = true;
        }
    }
    var message = trans("Xóa lịch sử tìm kiếm thất bại");
    var type = "error";
    if (result) {
        type = "info";
        if (id) {
            $(".item-" + id).remove();
        } else {
            $("#history-search").remove();
        }
        message = trans("Xóa lịch sử tìm kiếm thành công");
    }
    showValidateMsg(message, type);
    return result;
}


function deletePlaylist(videoId) {
    var id = $("#playlistId").val();
    var number = $("#number").val();

    $.ajax({
        type: "POST",
        url: '/playlist/video-to-playlist',
        dataType: "json",
        data: {
            'id': id,
            'video_id': videoId,
            'status': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var message = data.message;
            var type = "error";
            if (data.responseCode == 200) {
                type = "info";
                $(".item-" + videoId).remove();
                $("#number").val(number - 1);
                message = trans("Xóa video thành công ");
                var x = document.getElementsByClassName("number");
                var i;
                for (i = 0; i < x.length; i++) {
                    $("#" + x[i].id).html(i + 1);
                }
                var currentVideo = parseInt($('.countVideoPlaylist').html());
                $('.countVideoPlaylist').html(currentVideo - 1);
            }
            showValidateMsg(message, type);
        }
    });
}

$("#icon-follow").on('click', function () {
    if ($('#userId').val() == "") {
        $('#modalLogin').show();
        return;
    }
    var channelId = $(this).data("id");
    var status = $(this).data("status");
    followChannelAndNotify(channelId, status, 2, true);
});

$(".right-content .follow").on('click', function () {
    if ($('#userId').val() == "") {
        $('#modalLogin').show();
        return;
    }
    var channelId = $(this).data("id");
    var status = $(this).data("status");
    var me = $(this);
    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channelId,
            'status': status,
            'notification_type': 2,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {

                if (status == 0) {
                    showValidateMsg(trans('Hủy theo dõi kênh thành công'), "info");
                } else {
                    showValidateMsg(trans('Đăng kí theo dõi kênh thành công'), "info");
                }

                if (data.data.isFollow > 0) {
                    me.html(trans("Đã theo dõi"));
                    me.data("status", 0);
                } else {
                    me.html(trans("Theo dõi"));
                    me.data("status", 1);
                }

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, "error");
            }
        }
    });

});

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function confirmDelete() {
    $("#modal-confirm .notice").html(trans('Bạn có muốn xóa danh sách xem sau không ?'));
    $('#modal-confirm .btn-ok').removeAttr('data-id');
    showPopup("#modal-confirm");
}

function deleteAll() {
    $.ajax({
        type: "POST",
        url: '/video/del-all-watch-later',
        dataType: "json",
        data: {
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var message = data.message;
            var type = "error";
            if (data.responseCode == 200) {
                type = "info";
                message = trans("Xóa danh sách xem sau thành công");
                setTimeout(function () {
                    window.location = "/default/video-watch-later";
                }, 2000);// delay 2s
            }
            showValidateMsg(message, type);
        }
    });
}

$('.menu-history .item-clear').on('click', function () {
    var typeDelete = document.getElementById("typeDelete") ? document.getElementById("typeDelete").value : "";
    var item = "";
    switch (typeDelete) {
        case "HISTORYVIEW": // Xoa lich su xem 
            item = trans("lịch sử xem");
            break;
        case "HISTORYSEARCH": // Xoa lich su tim kiem
            item = trans("lịch sử tìm kiếm");
            break;
    }

    $("#modal-confirm-all .notice").html(trans('Bạn có muốn xóa ') + htmlEntities(item) + trans(' không ?'));
    //alert("1350");
    showPopup("#modal-confirm-all");
});


$('#modal-confirm-all .btn-ok').on('click', function () {
    $('#modal-confirm-all').css('display', 'none');

    var videoId = ($(this).attr("data-id") !== undefined) ? $(this).attr("data-id") : null;
    //alert($(this).attr("data-id"));
    if (document.getElementById("typeDelete") == null) {
        return;
    }
    var typeDelete = document.getElementById("typeDelete") ? document.getElementById("typeDelete").value : "";

    //alert(typeDelete);//HISTORYVIEW
    switch (typeDelete) {
        case "HISTORYVIEW": // Xoa lich su xem

                //alert("2---"+videoId);
                deleteAllHistoryView();

            break;

        case "HISTORYSEARCH": // Xoa lich su tim kiem
            //alert("111---"+videoId);
            if (videoId) {
                //alert("3---"+videoId);
                removeHistorySearch(videoId);
            } else {
                //alert("4---"+videoId);
                removeHistorySearch();
            }
            break;

    }
    videoId = null;

});

function deleteAllHistoryView() {
    $.ajax({
        type: "POST",
        url: '/account/delete-all-history-view',
        dataType: "json",
        data: {
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var message = data.message;
            var type = "error";
            if (data.responseCode == 200) {
                type = "info";
                message = trans(trans('Xóa danh lịch sử xem thành công'));
                setTimeout(function () {
                    window.location = "/default/history";
                }, 2000);// delay 2s
            }
            showValidateMsg(message, type);
        }
    });
}

function deleteHistorySearch(videoId) {

    $.ajax({
        type: "POST",
        url: '/default/delete-history-search',
        dataType: "json",
        data: {
            'id': videoId,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            var message = data.message;
            var type = "error";
            if (data.responseCode == 200) {
                type = "info";
                if (videoId) {
                    $(".item-" + videoId).remove();
                    message = trans("Xóa nội dung thành công");
                } else {
                    message = trans("Xóa lịch sử xem thành công");
                    $('.list-video').remove();
                    $('.content').html("<div class='.list-video .list-video-horizontal-left'><p class='text-info'>Không có dữ liệu</p></div>");
                }
            }
            showValidateMsg(message, type);
        }
    });
}

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function autoSearch() {
    var key = $('#keyword').val();
    var url = $('#urlSearch').val();
    $(".suggesstion").show();
    if (key.trim().length > 2) {
        $.ajax({
            url: url,
            data: {
                'query': key.trim(),
            },
            success: function (result) {
                document.getElementById("contentSuggess").innerHTML = result;
                search();
            }
        });
    } else {
        var tmpHtml = "";
        tmpHtml += "<li> " + trans('Nhập ít nhất 3 kí tự để thực hiện tìm kiếm') + " </li>";
        document.getElementById("contentSuggess").innerHTML = tmpHtml;
    }

}

function supportsLocalStorage() {
    return typeof (Storage) !== 'undefined';
}

function insertHistorySearch(key) {
    var value = "KEYS_SEARCH";
    var limit = $('#limit-history').val();
    if (!supportsLocalStorage()) {     // Ko ho tro 
        console.log('Not support');
    } else {
        if (key.trim().length == 0) {
            return;
        }
        var now = new Date();
        var time = now.toString('yyyy-MM-dd h:mm:ss');
        var id = now.getTime();
        $.ajax({
            url: '/site/generate-id',
            data: {
                'keyword': key,
                'time': now.getTime()
            },
            success: function (result) {
                if (result.responseCode == 200) {
                    id = result.data.id;
                }
            }
        });

        var data = {
            "id": id,
            "name": key,
            "time": time
        };
        var strData = JSON.stringify(data);
        if (localStorage.getItem(value)) {
            // console.log(localStorage.getItem(value)); return false;
            var items = localStorage.getItem(value);
            var item = JSON.parse(items);
            if (item.length < limit) { // nho hon gio han
                item.unshift(strData);
            } else {
                //Xoa phan tu cuoi cung day them gia tri
                item.pop();
                item.unshift(strData);
            }
            var encode = JSON.stringify(item);
            localStorage.setItem(value, encode);
        } else { // insert moi
            var arr = [];
            arr[0] = strData;
            var encode = JSON.stringify(arr);
            localStorage.setItem(value, encode);
        }
    }
}

function getHistorySearch(offset, limit) {
    var value = "KEYS_SEARCH";
    var arr = [];
    var tmpHtml = "";
    if (!supportsLocalStorage()) {     // Ko ho tro 
        console.log('Not support');
        tmpHtml += '<p class="text-info">' + trans('Không có dữ liệu') + '</p>';

    } else {

        if (localStorage.getItem(value)) {
            var items = localStorage.getItem(value);
            var item = JSON.parse(items);
            if (limit) {
                arr = item.slice(offset, limit);
            } else {
                arr = item;
            }
            if (arr.length > 0) {
                for (var index = 0; index < arr.length; ++index) {
                    var obj = JSON.parse(arr[index]);
                    tmpHtml += '<div class="item search item-' + obj.id + '" style="min-height: auto">'
                        + '<div class="description">'
                        + ' <a data-id="' + obj.id + '" href="/default/search-result?query=' + obj.name + '" class="name-video">' + htmlEntities(obj.name) + '</a>'
                        + '<div class="info">'
                        + '   <span class="cols-info">' + time_elapsed_string(obj.time) + '</span>'
                        + '</div>'
                        + '</div>'
                        + '<span data-id="' + obj.id + '" data-name="' + obj.name + '" class="remove-video"><i class="fa icon-delete"></i></span>'
                        + '</div>';
                }
            }
        } else {
            tmpHtml += '<p class="text-info">' + trans('Không có dữ liệu') + '</p>';

        }
    }
    document.getElementById("history-search").innerHTML = tmpHtml;
}



function time_elapsed_string(datetime) {
    var now = new Date();
    var ago = new Date(datetime);

    var d1Y = now.getFullYear();
    var d2Y = ago.getFullYear();

    if ((d1Y - d2Y) > 0) {
        return (d1Y - d2Y) + trans(" năm trước");
    }
    var d1M = now.getMonth();
    var d2M = ago.getMonth();
    if ((d1M + 12 * d1Y) - (d2M + 12 * d2Y) > 0) {
        return ((d1M + 12 * d1Y) - (d2M + 12 * d2Y)) + trans(" tháng trước");
    }
    var t1 = now.getTime();
    var t2 = ago.getTime();
    if (parseFloat((t1 - t2) / (24 * 3600 * 1000 * 7)) >= 1) {
        return parseInt((t1 - t2) / (24 * 3600 * 1000 * 7)) + trans(" tuần trước");
    }
    if (parseFloat((t1 - t2) / (24 * 3600 * 1000)) >= 1) {
        return parseInt((t1 - t2) / (24 * 3600 * 1000)) + trans(" ngày trước");
    }
    if (parseFloat((t1 - t2) / (1 * 3600 * 1000)) >= 1) {
        return parseInt((t1 - t2) / (1 * 3600 * 1000)) + trans(" giờ trước");
    }
    if (parseInt((t1 - t2) / (1 * 60 * 1000)) > 0) {
        return parseInt((t1 - t2) / (1 * 60 * 1000)) + trans(" phút trước");
    }
    return trans("Vừa xong");
}


function clickMenu() {
    var $win = $(window); // or $box parent container
    var $box = $(".link-account");

    $win.on("click.Bst", function (event) {
        if ($box.has(event.target).length == 0 && !$box.is(event.target)) {
            $("header .link-account").removeClass("selected");
        }
    });
}

function showChangePass() {
    $("header .link-account").removeClass("selected");
    $('.modal-vietnt').hide();
    $('#modalChangePass').show();
    $('#changepasswordform-password').val('');
    $('#changepasswordform-newpassword').val('');
    $('#changepasswordform-repeatpassword').val('');
    $('#changepasswordform-verifycode').val('');
    $('#errorChangePass').html("");
}

$('#change-password').on('click', function () {
    //alert("aaaaaaaaa");
    $.ajax({
        type: "POST",
        url: '/auth/update-password',
        dataType: 'json',
        data: {
            'password': $('#changepasswordform-password').val(),
            'newPassword': $('#changepasswordform-newpassword').val(),
            'repeatPassword': $('#changepasswordform-repeatpassword').val(),
            'verifyCode': $('#changepasswordform-verifycode').val(),
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (obj) {
            $('#changepasswordform-verifycode').val('');

            if (obj.responseCode == 401) {
                window.location.href = obj.redirectUrl;
            }
            if (obj.responseCode == 200) {
                showValidateMsg(obj.message, "info");
                refreshCaptcha('changepasswordform-verifycode-image');
                $('#modalChangePass').hide();
            } else {
                //alert(obj.responseCode);
                var errors = obj.message;
                //alert(errors);
                var errorHtml = "";
                $.each(errors, function (key, error) {
                    errorHtml += "<div>" + trans(error) + "</div>";
                });
                //alert(errorHtml);
                $('#errorChangePass').html(errorHtml);
                //reload captcha
                refreshCaptcha('changepasswordform-verifycode-image');
            }
        }
    });
});

$(".edit-playlist").on('click', function () {
    var name = $("#namPlaylist").val();
    var description = $("#namDescription").val();
    $("#iptPlaylistName").val(name);
    $("#iptPlaylistDescription").val(description);
    $('#editPlaylistModal').show();
});

$("#submitEditPlaylist").on('click', function () {
    var name = $('#iptPlaylistName').val();
    var description = $('#iptPlaylistDescription').val();
    name = (name) ? name.trim() : name;
    description = (description) ? description.trim() : description;
    if (name && name.length == 0 || name.length > 255) {
        $('#iptPlaylistName').focus();
        $('#editPlaylistModal').css('display', 'none');
        showValidateMsg(trans("Tên bắt buộc nhập và không được lớn hơn 255 kí tự"), 'error');
    } else if (description && description.length > 500) {
        $('#iptPlaylistDescription').focus();
        $('#editPlaylistModal').css('display', 'none');
        showValidateMsg(trans("Mô tả không được lớn hơn 500 kí tự"), 'error');
    } else {
        var id = $('#idPlaylist').val();

        // Thuc hien lu  du lieu
        $.ajax({
            type: "POST",
            url: '/playlist/update-playlist',
            dataType: 'json',
            data: {
                'id': id,
                'name': name,
                'description': description,
                '_csrf': $('meta[name="csrf-token"]').attr("content")
            },
            success: function (data) {
                var type = "error";
                if (data.responseCode == 200) {
                    type = "info";
                    $('#name-playlist').html(htmlEntities(name));
                    $('#iptPlaylistName,#namPlaylist').val(htmlEntities(name));
                    $('#menuLeftPL' + id).html(htmlEntities(name));
                    $('#iptPlaylistDescription,#namDescription').val(htmlEntities(description));
                }

                showValidateMsg(data.message, type);
                $('#editPlaylistModal').hide();

            }
        });
    }

});

function confirmDeletePlaylist() {
    $("#modal-confirm .notice").html(trans('Bạn có muốn xóa danh sách phát không ?'));
    $('#modal-confirm .btn-ok').removeAttr('data-id');
    showPopup("#modal-confirm");
}

function deleteAllPlaylist() {
    var id = $("#playlistId").val();
    $.ajax({
        type: "POST",
        url: '/playlist/delete-playlist',
        dataType: "json",
        data: {
            'id': id,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == 200) {
                showValidateMsg(data.message, 'info');
                setTimeout(function () {
                    window.location = "/";
                }, 2000);// delay 2s
            } else {
                showValidateMsg(data.message, 'error');
            }
        }
    });
}

function hidePopup(id) {
    $(id).hide();
}

function clickSearch() {
    $(".box-search .delete-search").on('click', function () {
        $(this).parent().hide();
    });
    var $win = $(window); // or $box parent container
    var $box = $(".suggesstion");

    $win.on("click.Bst", function (event) {
        if ($box.has(event.target).length == 0 && !$box.is(event.target)) {
            $("header .suggesstion").hide();
        }
    });
}

function markNotifi(id, url) {

    var msisdn = $('#mapaccountform-msisdn').val();
    $.ajax({
        type: "POST",
        url: '/account/mark-notification',
        dataType: 'json',
        data: {
            'id': id,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            $(".item" + id).removeClass("unread");
            if (url) {
                window.location = url;
            } else {
                return true;
            }
        }
    });
    setTimeout(function () {
    }, 30000);

}

if ($('#search-video-offset').length > 0) {
    var searchVideoOffset = parseInt($('#search-video-offset').val());

    function loadSearchMoreVideo(query, obj) {
        var searchVideoLimit = parseInt($('#search-video-limit').val());
        $.ajax({
            type: "GET",
            url: '/default/search-more-video',
            data: {
                'query': query,
                'limit': searchVideoLimit,
                'offset': searchVideoOffset
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    searchVideoOffset = searchVideoOffset + searchVideoLimit;
                    $('#searchVideo').append(rawData);
                } else {
                    $(obj).hide();
                }
            }
        });
    }
}

if ($('#search-channel-offset').length > 0) {
    var searchChannelOffset = parseInt($('#search-channel-offset').val());

    function loadSearchMoreChannel(query, obj) {
        var searchChannelLimit = parseInt($('#search-channel-limit').val());
        $.ajax({
            type: "GET",
            url: '/default/search-more-channel',
            data: {
                'query': query,
                'limit': searchChannelLimit,
                'offset': searchChannelOffset
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    searchChannelOffset = searchChannelOffset + searchChannelLimit;
                    $('#searchChannel').append(rawData);
                } else {
                    $(obj).hide();
                }
            }
        });
    }
}

//--
function addGAParams(selector) {
    var addStrPopup1 = '?popup=1';
    var addStrPopup2 = '&popup=1';
    $(selector).each(function (index) {
        var url = this.href;
        if (url.indexOf("=") > 0) {
            //$(this).attr('href', url+addStrPopup2);
            this.href = url + addStrPopup2;
        } else {
            //$(this).attr('href', url+addStrPopup1);
            this.href = url + addStrPopup1;
        }
    });
}


$(document).on("click", '#load-more-related-video', function (event) {
    loadMoreRelated();
});

function loadMoreRelated() {
    var videoId = $("#video_id").val();
    var limit = $("#related_limit").val();
    var offset = $("#related_offset").val();
    offset = parseInt(offset) + parseInt(limit);

    $.ajax({
        type: "GET",
        url: '/video/ajax-get-related-video?video_id=' + videoId + '&limit=' + limit + '&offset=' + offset + '&device_id=' + getCookie('device_id'),
        success: function (data) {
            if (data) {
                $(".list-video-horizontal").append(data);
                $("#related_offset").val(offset);
            } else {
                $("#load-more-related-video").hide();
            }
        }
    });
}


//Load home page
if (document.getElementById('home-page-right')) {
    var el = $(window),
    // initialize last scroll position
    lastY = el.scrollTop(),
    lastX = el.scrollLeft();

    var canLoad = true;
    el.on('scroll', function () {
        // get current scroll position
        var currY = el.scrollTop(),
            currX = el.scrollLeft(),

            // determine current scroll direction
            x = (currX > lastX) ? 'right' : ((currX === lastX) ? 'none' : 'left'),
            y = (currY > lastY) ? 'down' : ((currY === lastY) ? 'none' : 'up');
        // do something here…
        if ($(window).scrollTop() + $(window).height() + 300 >= $(document).height() && canLoad) {
            canLoad = false;
            var offset = $("#offset").val();
            var limit = 5;
            $("#spinner").show();
            $.ajax({
                type: "GET",
                url: '/default/load-more-video-channel?limit=' + limit + '&offset=' + offset,
                success: function (rawData) {
                    // $("#home-page-right").append(rawData);
                    if (rawData) {
                        $(rawData).each(function() {
                            $(this).find('.has-slider').css('display', 'block');
                            $(this).appendTo($('#home-page-right'));
                        });
                        // slideBoxVideo();
                        $("#offset").val(parseInt(offset) + parseInt(limit));
                        canLoad = true;
                    }
                    $("#spinner").hide();
                }
            });
        }
        // update last scroll position to current position
        lastY = currY;
        lastX = currX;
    });

    var offset = $("#offset").val();
    var limit = 5;
    $("#spinner").show();

    var cookie = document.cookie;
    var cookieItems = cookie.split(';');

    /*
    $.ajax({
        type: "GET",
        url: '/default/load-more-video-channel-recommend?limit=' + limit + '&offset=' + offset + "&device_id=" + getCookie("device_id"),
        success: function (rawData) {
            $("#home-page-right").prepend(rawData);
            if (rawData) {
                slideBoxVideo();
                $("#offset").val(parseInt(offset) + parseInt(limit));
                canLoad = true;
            }
            $("#spinner").hide();
        }
    });
    */

}

//TODO contract page

$("#contract_condition").on("click", function () {
    if ($(this).is(":checked")) {
        $(".btn-contract-condition").removeAttr("disabled");
        $(".btn-contract-condition").removeClass("btn-cancel");
    } else {
        $(".btn-contract-condition").attr("disabled", "disabled");
        $(".btn-contract-condition").addClass("btn-cancel");
    }
});

$("#upload_condition").on("click", function () {
    if ($(this).is(":checked")) {
        $(".btn-contract-condition").removeAttr("disabled");
        $(".btn-contract-condition").removeClass("btn-cancel");
    } else {
        $(".btn-contract-condition").attr("disabled");
        $(".btn-contract-condition").addClass("btn-cancel");
    }
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function hasExtension(inputID, exts) {
    var fileName = document.getElementById(inputID).value;
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName.toLowerCase());
}

function openDatePicker(id) {
    $(id).datepicker('show');
}

if ($('#relatedBox').length > 0) {

    $.ajax({
        type: "GET",
        url: '/video/related-video-box',
        data: {
            'device_id': getCookie('device_id'),
            'id': $('#video_id').val(),
            'channel_id': $('#channel_id').val(),
            'channel_name': $('#channel_name').val(),
            'video_name': $('#video_name').val(),
            'tag_name': $('#tag_name').val(),
            'category_id': $('#category_id').val(),
            'category_name': $('#category_name').val(),
            'category_parent_id': $('#category_parent_id').val()
        },
        success: function (rawData) {

            $('#relatedBox').html(rawData);

        }
    });
}

if ($('#showMoreReportUserUpload').length > 0) {

    var reportLimit = parseInt($('#reportLimit').val());

    function loadMoreReport() {

        $.ajax({
            type: "GET",
            url: '/account/load-more-report',
            data: {
                'limit': reportLimit,
                'offset': $('#reportUserUpload tr.display-report').length
            },
            success: function (rawData) {
                if (rawData.length > 0) {
                    $('#reportUserUpload').append(rawData);
                }

                if (($('#reportUserUpload tr.more-report').length % reportLimit) !== 0) {
                    $('#showMoreReportUserUpload').hide();
                }

            }
        });
    }
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function showHidePlaylistMenu(e) {
    if ($(e).hasClass("active")) {
        $(e).removeClass("active");
        $("#playlist_parts").slideUp(200);
    } else {
        $("#playlist_parts").slideDown(200);
        $(e).addClass("active");
    }
}

function followChannelAndHide(channel_id, removeId) {

    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': 1,
            'notification_type': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")
        },
        success: function (data) {
            if (data.responseCode == '200') {
                showValidateMsg(trans('Thành công'));
                $(removeId).parent().remove();
                if (!$("li[id^=channel_]").length) {
                    $("#rcmBlock").hide();
                }

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, 'error');
            }

        }
    });
}

function followChannel(channel_id, status) {
    if (!status) {
        status = 0;
    }
    $.ajax({
        type: "POST",
        url: '/account/follow-channel',
        dataType: 'json',
        data: {
            'id': channel_id,
            'status': status,
            'notification_type': 0,
            '_csrf': $('meta[name="csrf-token"]').attr("content")

        },
        success: function (data) {
            if (data.responseCode == '200') {
                if (status == 0) {
                    showValidateMsg(trans('Hủy theo dõi kênh thành công'));
                    if (document.getElementById("channel_btn_follow_" + channel_id)) {
                        $("#channel_btn_follow_" + channel_id).show();
                        $("#channel_btn_unfollow_" + channel_id).hide();
                    }
                } else {
                    showValidateMsg(trans('Đăng kí theo dõi kênh thành công'));
                    if (document.getElementById("channel_btn_follow_" + channel_id)) {
                        $("#channel_btn_follow_" + channel_id).hide();
                        $("#channel_btn_unfollow_" + channel_id).show();
                    }
                }
                $('#' + channel_id).remove();

            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, 'error');
            }

        }
    });
}

function removeChannelRelated(channel_id) {
    if (!confirm(trans("Xác nhận xóa kênh liên quan không?"))) {
        return;
    }
    $.ajax({
        type: "POST",
        url: '/channel/remove-channel-related',
        dataType: 'json',
        data: {
            'channel_related_id': channel_id,
            '_csrf': $('meta[name="csrf-token"]').attr("content")

        },
        success: function (data) {
            if (data.responseCode == '200') {
                showValidateMsg(trans('Xóa thành công'));
                $('#channel_related_' + channel_id).remove();
            } else if (data.responseCode == '401') {
                window.location = '/auth/login';
            } else {
                showValidateMsg(data.message, 'error');
            }

        }
    });
}


function autoSearchChannel() {
    var key = $('#keyword_channel').val();

    if (key.trim().length > 1) {
        $.ajax({
            url: "/default/search-channel-related-suggestion",
            data: {
                'query': key.trim(),
            },
            success: function (result) {
                var tmpHtml = "";
                if (result)
                    for (var cc = 0; cc < result.data.length; cc++) {
                        for (var cc2 = 0; cc2 < result.data[cc].content.length; cc2++) {
                            if (result.data[cc]['type'] == 'VOD') {
                            } else {

                                tmpHtml += "<div class='item' id='channel_related_" + result.data[cc].content[cc2]['id'] + "'> " +
                                    "                    <a href='/channel/" + result.data[cc].content[cc2]['id'] + "' class='image'> " +
                                    "                        <img src='" + result.data[cc].content[cc2]['avatarImage'] + "'/> " +
                                    "                    </a> " +
                                    "                    <div class='description'> " +
                                    "                        <a href='/channel/" + result.data[cc].content[cc2]['id'] + "' class='name-channel'>" + result.data[cc].content[cc2]['name'] + "</a>" +
                                    "                        <a href='/channel/" + result.data[cc].content[cc2]['id'] + "' class='text-1'>" + result.data[cc].content[cc2]['follow_count'] + trans(' người theo dõi') + " </a>" +
                                    "                        <a href='javascript:void(0)' class='add-channel-related btn-register' data-id='" + result.data[cc].content[cc2]['id'] + "' >" + trans('Thêm kênh') + "</a>\n" +
                                    "                    </div>" +
                                    "                </div>";

                            }
                        }
                    }
                if (tmpHtml.length == 0) {
                    tmpHtml += "<li style='color:black' class='ctn'> " + trans('Không có dữ liệu với từ khóa tìm kiếm') + " </li>";
                }

                document.getElementById("content_channel_suggess").innerHTML = tmpHtml;
            }
        });
    } else {
        var tmpHtml = "";
        //tmpHtml += "<li> Nhập ít nhất 3 kí tự để thực hiện tìm kiếm </li>";
        document.getElementById("content_channel_suggess").innerHTML = tmpHtml;
    }

}

function loadImage($el, $fn) {
    $($el).attr('src', $($el).attr('data-src'));
    $($el).removeClass("lazy");
    $fn ? $fn() : null;
}

function elementInViewport($el) {
    if ($el === undefined) return false;
    var $rect = $el.getBoundingClientRect();

    return ($rect.top >= 0 && $rect.left >= 0 && $rect.top <= (window.innerHeight || document.documentElement.clientHeight)
    );
}

function processScroll() {
    var arrImg = $('img.lazy').toArray();
    $('img.lazy').each(function (i, img) {
        if (elementInViewport(img)) {
            loadImage(img, function () {
            });
            for (var icc = 0; icc < i; icc++) {
                loadImage(arrImg[icc], function () {
                });
            }
        }

    });
};

function checkImageExists(imageUrl, target, callBack, callBackFail) {
    if (!isSupportWebp) {
        imageUrl = imageUrl.replace(".webp", ".gif");
    }
    var imageData = new Image();
    imageData.onload = function () {
        callBack(target);
    };
    imageData.onerror = function () {
        callBackFail(target);
    };
    imageData.src = imageUrl;
}

function replaceImage(target) {
    var tImg = target.data("display");
    if (!isSupportWebp) {
        tImg = tImg.replace(".webp", ".gif");
    }
    if(target.attr("src")!=tImg){
        target.attr("src", tImg);
    }
}

function replaceImage2(target) {
    target.attr("src", target.data("src"));
}

function replaceImage3(target) {
    target.attr("src", "/images/data/16x9.png");
}
