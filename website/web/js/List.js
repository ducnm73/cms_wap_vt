"use strict";
/* @author dungld5 */

var HomePage = React.createClass({
    displayName: "HomePage",
    getInitialState: function getInitialState() {
        return {
            clientData: null,
            appendVideos: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: parseInt($("#getMoreOffset").val()),
            typeId: $("#load-more-id").val(),
            type: $("#type").val(),
            alias: $("#alias").val(),
            lockScroll: false,
            endOfData: false,
        };
    },
    componentWillMount: function componentWillMount() {
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {

    },
    componentDidMount: function componentDidMount() {
        window.addEventListener('scroll', this.updateViewport, false);
        this.updateViewport();
    },
    componentWillUnmount: function componentWillUnmount() {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function updateViewport() {
        // Neu khong con DATA thi return ''
        if (this.state.endOfData) {
            return;
        }
        // Dieu kien load DATA

        if (!this.state.isFirstTimeLoad && !this.state.lockScroll && document.getElementById('contentVideoBlock').scrollHeight < window.pageYOffset + window.innerHeight + 100) {
            this.setState({lockScroll: true});
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                tryCount: 0,
                retryLimit: 3,
                data: {'id': this.state.typeId, 'limit': this.state.limit, 'offset': this.state.offset},
                success: function (ajaxData) {
                    var videoList = ajaxData.data.content;
                    // Neu khong lay dc content thi STOP
                    if (!videoList || videoList.length == 0) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                    var oldVideoList = this.state.appendVideos;
                    oldVideoList = oldVideoList.concat(videoList);
                    this.setState({appendVideos: oldVideoList, lockScroll: false});
                    if (videoList.length > 0) {
                        this.setState({offset: this.state.offset + videoList.length, lockScroll: false});
                    } else {
                        this.setState({lockScroll: true});
                    }
                    if (videoList.length < this.state.limit) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({lockScroll: false});
                }.bind(this)
            });
        } else {
            this.state.isFirstTimeLoad = false;
        }
    },
    render: function render() {
        var rows = [];
        var serverAjax = this.state.appendVideos;
        if (serverAjax) {
            switch (this.state.type) {
                case "VOD":
                    rows.push(React.createElement(VideoChanelBox, {videos: serverAjax}));
                    break;
                case "CHANNEL":
                    rows.push(React.createElement(channelBox, {videos: serverAjax}));
                    break;
                case "PLAYLIST":
                    rows.push(React.createElement(VideoPlaylist, {videos: serverAjax}));
                    break;
                case "CATEGORY":
                    rows.push(React.createElement(categoryBox, {videos: serverAjax}));
                    break;
                case "CATEGORY_CHILD":
                    rows.push(React.createElement(categoryChildBox, {videos: serverAjax}));
                    break;
            }

        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(React.createElement(
                "div",
                null,
                React.createElement("div", {className: "clearfix"}),
                React.createElement(
                    "div",
                    {className: "spinner"},
                    React.createElement(
                        "div",
                        {className: "quarter"},
                        React.createElement("div", {className: "circle"})
                    )
                ),
                React.createElement("div", {className: "clearfix"})
            ));
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            rows.push(React.createElement("div", {className: "clearfix"}));
        }

        return React.createElement(
            "div",
            null,
            rows
        );
    }
});

var VideoChanelBox = React.createClass({
    displayName: "channelBox",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.png",
            defaultImage4x4: "/images/data/4x4.png",
            showDescription: $("#showDescriptionId").val(),
            icoDelVideoHistory: $("#icoDelVideoHistory").val(),
            functionMore: document.getElementById('function-more') ? $('#function-more').val() : 1,
            mutiLine: document.getElementById('mutiLine') ? $('#mutiLine').val() : false,
            isMyVideo: document.getElementById('isMyVideo') ? $('#isMyVideo').val() : '',
        };
    },
    clickIcoMore: function clickIcoMore(event) {
        console.log('id info', event.currentTarget.getAttribute('data-id'));
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {
        var _this = this;
        var videos = [];
        var type = $("#type").val();
        var img = _this.state.defaultImage16x9;
        var box = "image16x9";
        var videoFreeIcon = "";

        if (type == "CHANNEL") {
            img = _this.state.defaultImage4x4;
            box = "image4x4";
        }
        for (var i = 0; i < this.props.videos.length; i++) {
            var description = "";
            if (_this.state.showDescription == 1) {
                var description = React.createElement("div", {className: "des des-playlist"}, this.props.videos[i]['description']);
            }
            var deleteVideoHistory = "";
            if (_this.state.icoDelVideoHistory == 1) {
                var deleteVideoHistory = React.createElement("a", {
                        className: "rmVideoHistory",
                        href: "javascript:void(0);",
                        "data-id": this.props.videos[i].id,
                        "data-name": this.props.videos[i]['name']
                    },
                    React.createElement("span", {className: "remove-video"},
                        React.createElement("i", {className: "fa icon-delete"}, "")
                    )
                );
            }
            var functionMore = "";
            var triggerComment = "";
            if(this.props.videos[i]['status'] == 2) {
                if (this.props.videos[i]['can_comment'] == 1) {
                    triggerComment = React.createElement("div",
                        {
                            className: "item-popover function-trigger-comment function-trigger-comment-" + this.props.videos[i]['id'],
                            "data-videoid": this.props.videos[i]['id'],
                            "data-status": this.props.videos[i]['can_comment']
                        },
                        trans("Tắt bình luận")
                    );
                } else {
                    triggerComment = React.createElement("div",
                        {
                            className: "item-popover function-trigger-comment function-trigger-comment-" + this.props.videos[i]['id'],
                            "data-videoid": this.props.videos[i]['id'],
                            "data-status": this.props.videos[i]['can_comment']
                        },
                        trans("Bật bình luận")
                    );
                }
            }
            if(_this.state.isMyVideo == 'myVideo' && this.props.videos[i]['status'] == 2){
                var functionMore = React.createElement("div",
                    {className: "function-more"},
                    React.createElement("span",
                        {className: "button-link-more"},
                        React.createElement("i",
                            {className: "fa fa-ellipsis-v"}
                        )
                    ),

                    React.createElement("div",
                        {className: "popover-more"},
                        React.createElement("div",
                            {
                                className: "item-popover my-video-delete",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Xoá")
                        ),
                        triggerComment
                    )
                );
            }
            else if(_this.state.isMyVideo == 'myVideo' && this.props.videos[i]['status'] == 1){
                var functionMore = React.createElement("div",
                    {className: "function-more"},
                    React.createElement("span",
                        {className: "button-link-more"},
                        React.createElement("i",
                            {className: "fa fa-ellipsis-v"}
                        )
                    ),

                    React.createElement("div",
                        {className: "popover-more"},
                        React.createElement("div",
                            {
                                className: "item-popover my-video-delete",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Xoá")
                        ),

                        React.createElement("div",
                            {
                                className: "item-popover my-video-edit",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Chỉnh sửa")
                        ),
                        triggerComment
                    )

                );

            }
            else if (_this.state.isMyVideo == 'myVideo' && this.props.videos[i]['status'] == 3) {
                var functionMore = React.createElement("div",
                    {className: "function-more"},
                    React.createElement("span",
                        {className: "button-link-more"},
                        React.createElement("i",
                            {className: "fa fa-ellipsis-v"}
                        )
                    ),

                    React.createElement("div",
                        {className: "popover-more"},
                        React.createElement("div",
                            {
                                className: "item-popover my-video-delete",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Xoá")
                        ),

                        React.createElement("div",
                            {
                                className: "item-popover my-video-edit",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Chỉnh sửa")
                        ),
                        triggerComment
                    )
                );
            }
            else if (_this.state.functionMore == 1) {
                var functionMore = React.createElement("div",
                    {className: "function-more"},
                    React.createElement("span",
                        {className: "button-link-more"},
                        React.createElement("i",
                            {className: "fa fa-ellipsis-v"}
                        )
                    ),
                    React.createElement("div",
                        {className: "popover-more"},
                        React.createElement("div",
                            {
                                className: "item-popover function-add-playlist",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Thêm vào danh sách phát")
                        ),
                        // triggerComment,
                        React.createElement("div",
                            {
                                className: "item-popover function-add-playlater",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            trans("Thêm vào xem sau")
                        ),
                        // React.createElement("div",
                        //     {
                        //         className: "item-popover function-add-share",
                        //         "data-id": this.props.videos[i]['id'],
                        //         "data-url":  this.props.videos[i]['linkSocial']
                        //     },
                        //     trans("Chia sẻ")
                        // ),
                        // React.createElement("div",
                        //     {
                        //         className: "item-popover my-video-delete",
                        //         "data-videoid": this.props.videos[i]['id']
                        //     },
                        //     trans("Xoá")
                        // ),

                    ),
                    React.createElement("div",
                        {className: "cols-info"},
                        React.createElement("div", {
                                className: "function-add-playlist",
                                "data-videoid": this.props.videos[i]['id']
                            },
                            React.createElement("div",
                                {
                                    className: "content addVideoToPLBox",
                                    id: "blockAddMorePL" + this.props.videos[i]['id']
                                },
                                React.createElement("div", {className: "title"}, trans("Thêm vào")),
                                React.createElement("div", {
                                    className: "scroll myPlaylistBox",
                                    id: "myPlaylistBox" + this.props.videos[i]['id'],
                                    'data-firstload': 0
                                }),
                                React.createElement("div", {className: "bottom-playlist"},
                                    React.createElement("span", {className: "btn-addplaylist"},
                                        React.createElement("i", {className: "icon-plus btnShowAddNewPL"}),
                                        trans("Tạo playlist mới")
                                    )
                                )
                            )
                        )
                    )
                );
            };
            var channel = "";
            var point = "";
            var viewTime = "";
            if (this.props.videos[i]['channel_id'] && _this.state.isMyVideo != "myVideo") {
                if (_this.state.mutiLine == false || _this.state.mutiLine == "false") {
                    point = React.createElement("i",
                        {className: "fa fa-circle"}
                    );
                }
                channel = React.createElement("a",
                    {
                        href: "/channel/" + this.props.videos[i]['channel_id'],
                        className: "name-author",
                        title: this.props.videos[i]['fullUserName']
                    },
                    React.createElement(
                        "span",
                        {className: "pd-right6"},
                        "" + this.props.videos[i]['fullUserName']
                    ),
                    point
                );
            }
            if (_this.state.isMyVideo == "myVideo" && this.props.videos[i]['status'] == 1) {

                viewTime = React.createElement("div",
                    {className: "info"},
                    React.createElement("span",
                        {className: "cols-info-full"},
                        trans("Chờ phê duyệt(Không xem được)")
                    ),
                    React.createElement("span",
                        {className: "cols-info"},
                        React.createElement("i",
                            {className: "fa fa-circle"}
                        ),
                        this.props.videos[i]['publishedTime'] + ""
                    )
                );
            } else if (_this.state.isMyVideo == "myVideo" && this.props.videos[i]['status'] == 3) {
                viewTime = React.createElement("div",
                    {className: "info"},
                    React.createElement("span",
                        {className: "cols-info-full"},
                        trans("Từ chối duyệt") + ((this.props.videos[i]['reason'] != undefined) ? trans("(Lý do: ") + this.props.videos[i]['reason'] + ")" : "")
                    ),

                    React.createElement("a",
                        {className: "feedback-upload",
                            "data-id": this.props.videos[i]["id"],
                            "id": "upload_feedback_"+this.props.videos[i]["id"],
                        },
                        React.createElement("p", {className: ""}, (this.props.videos[i]["feedback_status"])?trans("ĐÃ GỬI Ý KIẾN"): trans("GỬI Ý KIẾN") )
                    )

                );
            } else {
                //alert("ssssssssssss");
                viewTime = React.createElement("div",
                    {className: "info"},
                    React.createElement("span",
                        {className: "cols-info"},
                        "" +
                        this.props.videos[i]['play_times'] + trans(" lượt xem")
                    ),
                    React.createElement("span",
                        {className: "cols-info"},
                        React.createElement("i",
                            {className: "fa fa-circle"}
                        ),
                        this.props.videos[i]['publishedTime'] + ""
                    )
                );
            }
            if (this.props.videos[i]['price_play']  == 0) {
                videoFreeIcon = React.createElement("div",
                    {className: "attach-img"},
                    React.createElement("img",
                        {src: "/images/freeVideo.png" })
                );
            }
            videos.push(
                React.createElement(
                    "div",
                    {className: "item video-item-" + this.props.videos[i]['id']},
                    React.createElement(
                        "a",
                        {
                            href: (_this.state.isMyVideo == "myVideo" && this.props.videos[i]['status'] != 2) ? "javascript:void(0);" : "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'],
                            className: "image",
                            title: this.props.videos[i]['fullName']
                        },
                        React.createElement(
                            "img", {
                                src: (type == "CHANNEL") ? this.props.videos[i]['avatarImage'] : this.props.videos[i]['coverImage'],
                                onError: function onError(e) {
                                    e.target.src = img;
                                },
                                className: box,
                                alt: this.props.videos[i]['fullName'],
                            }
                        ),
                        videoFreeIcon,
                        React.createElement(
                            "span",
                            {className: "time"},
                            "" + this.props.videos[i]['duration']
                        )
                    ),

                    React.createElement(
                        "div",
                        {className: "description"},
                        functionMore
                        ,
                        React.createElement("a",
                            {
                                href: (_this.state.isMyVideo == "myVideo" && this.props.videos[i]['status'] != 2) ? "javascript:void(0);" : "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'],
                                className: "name-video",
                                title: this.props.videos[i]['fullName']
                            },
                            "" + this.props.videos[i]['fullName']
                        ),
                        channel
                        ,
                        viewTime,
                        description
                    ),
                    deleteVideoHistory
                )
            );
        }
        ;
        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});

var VideoPlaylist = React.createClass({
    displayName: "VideoPlaylist",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.png",
            defaultImage4x4: "/images/4x4.gif",
            number: parseInt(document.getElementById('number') ? document.getElementById('number').value : 0),
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {
        var _this = this;
        var videos = [];
        var num = parseInt(document.getElementById('number') ? document.getElementById('number').value : 0);
        for (var i = 0; i < this.props.videos.length; i++) {
            num = num + 1;
            var channel = "";
            if (this.props.videos[i]['channel_id']) {
                channel = React.createElement("a",
                    {
                        href: "/channel/" + this.props.videos[i]['channel_id'],
                        className: "name-author",
                        title: this.props.videos[i]['fullUserName']
                    },
                    "" + this.props.videos[i]['userName']
                );
            }
            videos.push(React.createElement(
                "div",
                {className: "item has-number item-" + this.props.videos[i]['id']},
                React.createElement("div",
                    {className: "number", "id": "number" + this.props.videos[i]['id']},
                    "" + (num)
                ),
                React.createElement("a",
                    {
                        href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'],
                        className: "image",
                        title: this.props.videos[i]['fullName']
                    },
                    React.createElement(
                        "img", {
                            src: this.props.videos[i]['coverImage'],
                            onError: function onError(e) {
                                e.target.src = _this.state.defaultImage16x9;
                            },
                            className: "image16x9"
                        }
                    ),
                    React.createElement(
                        "span",
                        {className: "time"},
                        "" + this.props.videos[i]['duration']
                    )
                ),
                React.createElement(
                    "div",
                    {className: "description"},
                    React.createElement("a",
                        {
                            href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'],
                            className: "name-video",
                            title: this.props.videos[i]['fullName']
                        },
                        "" + this.props.videos[i]['name']
                    ),
                    channel,
                    React.createElement("div",
                        {className: "info"},
                        React.createElement("span",
                            {className: "cols-info"},
                            React.createElement("i",
                                {className: (this.props.videos[i]['userId']) ? "fa fa-circle" : ""}
                            ),
                            this.props.videos[i]['play_times'] + trans(" lượt xem")
                        ),
                        React.createElement("span",
                            {className: "cols-info"},
                            React.createElement("i",
                                {className: "fa fa-circle"}
                            ),
                            this.props.videos[i]['publishedTime'] + ""
                        )
                    ), React.createElement("div", {className: "des des-playlist"}, this.props.videos[i]['description'])
                ),
                React.createElement("p", {
                        className: "rmVideoHistory",
                        href: "javascript:void(0);",
                        "data-id": this.props.videos[i]["id"],
                        "data-name": this.props.videos[i]["name"]
                    },
                    React.createElement("span", {className: "remove-video"},
                        React.createElement("i", {className: "fa icon-delete"}, "")
                    )
                )
                )
            );
        }

        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});

var channelBox = React.createClass({
    displayName: "channelBox",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.jpg",
            defaultImage4x4: "/images/data/4x4.png",
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {
        var _this = this;
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            var status = this.props.videos[i]['isFollow'] ? 0 : 1;
            var actName = this.props.videos[i]['isFollow'] ? trans('Hủy theo dõi') : trans('Theo dõi');
            var channel = "";
            if (this.props.videos[i]['userId'] || this.props.videos[i]['channel_id']) {
                channel = React.createElement("a",
                    {
                        href: "/channel/" + this.props.videos[i]['channel_id'],
                        className: "name-video",
                        title: this.props.videos[i]['channel_name']
                    },
                    "" + this.props.videos[i]['channel_name']
                );
            }
            videos.push(
                React.createElement("div",
                    {className: "item item-channel has-follow channel-item-" + this.props.videos[i]['channel_id']},
                    React.createElement("div",
                        {className: "right-content"},
                        React.createElement("span",
                            {
                                className: "btn-default follow",
                                "data-id": this.props.videos[i]['channel_id'],
                                "data-name": this.props.videos[i]['channel_name'],
                                "data-status": status
                            },
                            "" + actName
                        )
                    ),
                    React.createElement("a",
                        {
                            href: "/channel/" + this.props.videos[i]['channel_id'],
                            className: "image",
                            'data-id': this.props.videos[i]['channel_id']
                        },
                        React.createElement(
                            "img", {
                                src: (this.props.videos[i]['avatarImageHX'].length > 0) ? this.props.videos[i]['avatarImageHX'] : _this.state.defaultImage16x9,
                                onError: function onError(e) {
                                    e.target.src = _this.state.defaultImage16x9;
                                },
                                className: "image16x9"
                            }
                        )
                    ),
                    React.createElement("div",
                        {className: "description"},
                        channel,
                        React.createElement("div",
                            {className: "info"},
                            React.createElement("span",
                                {className: "cols-info"},
                                React.createElement("i",
                                    {className: "fa " + (channel == "") ? "" : "fa-circle"}
                                )
                                ,
                                this.props.videos[i]['num_follow'] + trans(" người theo dõi")
                            ),
                            React.createElement("span",
                                {className: "cols-info"},
                                React.createElement("i",
                                    {className: "fa fa-circle"}
                                ),
                                this.props.videos[i]['num_video'] + " videos"
                            )
                        ),
                        React.createElement("div", {className: "des"}, this.props.videos[i]['description'])
                    )
                )
            );
        }

        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});

var categoryBox = React.createClass({
    displayName: "categoryBox",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.jpg",
            defaultImage4x4: "/images/data/4x4.png",
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    },
    render: function render() {
        var _this = this;
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {

            videos.push(
                React.createElement(
                    "div",
                    {className: "item"},
                    React.createElement("a",
                        {
                            href: "/chi-tiet-the-loai/" + this.props.videos[i]['id'],
                            className: "image",
                            title: this.props.videos[i]['name']
                        },
                        React.createElement(
                            "img", {
                                src: this.props.videos[i]['avatarImage'],
                                onError: function onError(e) {
                                    e.target.src = _this.state.defaultImage4x4;
                                }
                            }
                        )
                    ),
                    React.createElement(
                        "div",
                        {className: "description"},
                        React.createElement("a",
                            {
                                href: "/chi-tiet-the-loai/" + this.props.videos[i]['id'],
                                className: "name-channel",
                                title: this.props.videos[i]['name']
                            },
                            "" + this.props.videos[i]['name']
                        ),
                        React.createElement("a",
                            {
                                href: "/chi-tiet-the-loai/" + this.props.videos[i]['id'],
                                className: "name-channel",
                                title: this.props.videos[i]['name']
                            },
                            this.props.videos[i]['play_times'] + trans(" lượt xem")
                        )
                    )
                )
            );
        }

        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});

var categoryChildBox = React.createClass({
    displayName: "categoryChildBox",
    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.jpg",
            defaultImage4x4: "/images/data/4x4.png",
            offset: parseInt($("#getMoreOffset").val())
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
        slide();
        reSizeImage();
    },
    render: function render() {
        var _this = this;
        var videos = [];
        var offset = _this.state.offset;
        for (var i = 0; i < this.props.videos.length; i++) {
            var items = this.props.videos[i]['content'];
            if (items.length > 0) {
                var id = this.props.videos[i]["id"].replace("category_child_", "");
                var name = this.props.videos[i]["name"];
                var video = [];
                for (var j = 0; j < items.length; j++) {
                    var user = "";
                    if (items[j]["channel_id"]) {
                        user = React.createElement("a",
                            {
                                href: "/channel/" + items[j]['channel_id'],
                                className: "name-author",
                                title: items[j]['fullUserName']
                            },
                            "" + items[j]['userName']
                        );
                    }
                    var v =
                        React.createElement("div",
                            {className: "item"},
                            React.createElement("a",
                                {
                                    href: "/video/" + items[j]['id'] + "/" + items[j]['slug'],
                                    className: "image",
                                    title: items[j]['fullName']
                                },
                                React.createElement(
                                    "img",
                                    {
                                        src: items[j]['coverImage'],
                                        onError: function onError(e) {
                                            e.target.src = _this.state.defaultImage16x9;
                                        },
                                        className: "image16x9"
                                    }
                                ),
                                React.createElement("span",
                                    {className: "time"},
                                    "" + items[j]['duration']
                                )
                            ),
                            React.createElement("div",
                                {className: "description"},
                                React.createElement("a",
                                    {
                                        href: "/video/" + items[j]['id'] + "/" + items[j]['slug'],
                                        className: "name-video",
                                        title: items[j]['fullName']
                                    },
                                    "" + items[j]['name']
                                ),
                                user
                                ,
                                React.createElement("div",
                                    {className: "info"},
                                    React.createElement("div",
                                        {className: "cols-info"},
                                        items[j]['play_times'] + " lượt xem"
                                    ),
                                    React.createElement("div",
                                        {className: "cols-info"},
                                        React.createElement("i",
                                            {className: "fa fa-circle"}
                                        ),
                                        items[j]['publishedTime']
                                    )
                                )
                            )
                        )

                    video.push(v);
                }

                videos.push(
                    React.createElement("div",
                        {className: "mdl-general"},
                        React.createElement("h1",
                            {className: "title"},
                            React.createElement("a",
                                {href: "/chi-tiet-the-loai/" + id, className: "name-channel", title: name},
                                "" + name
                            ),
                            React.createElement("a",
                                {href: "/video/" + items[0]['id'] + "/" + items[0]['slug'], className: "text-1"},
                                "Phát tất cả"
                            )
                        ),
                        React.createElement("div",
                            {className: "content"},
                            React.createElement("div",
                                {className: "list-video"},
                                React.createElement("div",
                                    {className: "owl-carousel owl-carousel-" + (offset + 1)},
                                    video
                                )
                            )
                        )
                    )
                );
            }
        }

        return React.createElement(
            "div",
            {className: "loadMoreVideo"},
            videos
        );
    }
});

ReactDOM.render(React.createElement(HomePage, {
    url: "/",
    loadMoreUrl: "/default/load-more-content"
}), document.getElementById('container'));

function slide() {
    if(typeof owlCarousel == "undefined") {
        return;
    }

    var maxNumberCarousel = 100;
    for (var x = 0; x < maxNumberCarousel; x++) {
        var nameOfCarousel = '.owl-carousel-' + x;
        $(nameOfCarousel).owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
                ,
                1400: {
                    items: 5
                },
                1800: {
                    items: 6
                },
                2000: {
                    items: 7
                }
            },
            dot: false
        });
    }
}