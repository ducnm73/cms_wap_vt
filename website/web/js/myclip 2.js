/* CSS Document */

$(function () {
    "use strict";

    /*01.collapsed*/
    //collapsed();

    /*02.modal*/
    modal();

    //03.configMenu*/
    configMenu();

    //04.toogleAuto*/
    toogleAuto();

    /*05.shortDescription*/
    shortDescription();

    /*06.swicthView*/
    swicthView();

    /*07.openTab*/
    openTab();

    /*08.according*/
    accroding();

    /*09.search*/
    //search();

    /*09.notify*/
    notify();

    /*10.popoverAll*/
    popoverAll();

    /*12.offzoomPage*/
    window.addEventListener("resize", function () {
        rtimeResizeend = new Date();
        if (timeoutResizeend === false) {
            timeoutResizeend = true;
            setTimeout(resizeEnd, deltaResizeend);
        }
    }, false);
    /*14.fireOwlcarousel*/
    // fireOwlcarousel();

    /*11.reSizeImage*/
    reSizeImage();
});

/*-----------------------*/
/*01.collapsed*/
/*-----------------------*/
function collapsed() {
    "use strict";
    var triggerObject00 = $('.link-view-more');

    triggerObject00.click(function () {
        $(this).parent().css('height', 'auto');
        $(this).hide();
    });
}

/*-----------------------*/
/*02.modal*/
/*-----------------------*/
function modal() {
    "use strict";
    var targetObject00 = $(".modal-vietnt");
    var targetObject01 = $('.modal-content-vietnt');
    // var triggerObject01 = $('.action-vietnt');

    /*01 open*/
    // triggerObject01.click(function () {
    $(document).on("click", '.action-vietnt', function () {
        //alert("2222222");
        var nameTargetObject = '#' + $(this).attr('data-target');
        //var nameTargetObject = $(this).attr('data-target');
        //alert("1111"+ nameTargetObject);
        /*show modal*/
        $(nameTargetObject).show();

        /*hide modal*/
        if ($(this).attr('data-dismiss') === 'close') {
            $(".modal-vietnt").hide();
        }
    });

    /*02 close*/
    // targetObject00.click(function () {
    $(document).on("click", '.modal-vietnt', function () {
        $(this).hide();
    });

    /*03.disable event on volume content*/
    // targetObject01.click(function (event) {
    $(document).on("click", '.modal-content-vietnt', function (event) {
        event.stopPropagation();
    });
    //Escape exists
    $(document).keyup(function (e) {
        if (e.keyCode === 27) { // escape key maps to keycode `27`
            targetObject00.hide();
        }
    });
}

/*-----------------------*/
/*03.configMenu*/
/*-----------------------*/
function configMenu() {
    "use strict";

    var leftMenu = $(".left-menu");
    var rightContentBody = $(".body-content-right");
    var leftContentBody = $(".body-content-left");
    var scrollMenu = $(".left-menu-scroll");
    var buttonTrigger = $(".header-button");
    var taggetContentLeft = $(".body-content-left");
    var closeMenuLeft = "<div class='close-left-menu'></div>";
        configMenuResize();
    //Close leftmenu
    $("body").on('click touchstart mousedown','.close-left-menu', function(){
        leftMenu.toggle();
        taggetContentLeft.toggle();
        
        $('div').remove('.close-left-menu');
        rightContentBody.css("width",$(window).width() - leftContentBody.width() - 20); // Not click
        rightContentBody.removeClass('full-body-content');
    });
    
    //Open leftmenu
    $("body").on('click','.header-button', function(){
        
        //alert("xxx");
        leftMenu.toggle();
        taggetContentLeft.toggle(); 
        
        if(rightContentBody.hasClass('full-body-content')){ // Clicked  
            rightContentBody.css("width",$(window).width() - leftContentBody.width()); // Not click
            rightContentBody.removeClass('full-body-content');
        }else{          
            rightContentBody.css("width","100%");
            rightContentBody.addClass('full-body-content');
        }   
        
        //Remove overlay 
        var checkOpenmenu = $('.close-left-menu').length;
        if(checkOpenmenu === 0){
            $("body").append(closeMenuLeft);
        }else{
            $('div').remove('.close-left-menu');
        }
        
        
        
        //Resize image
        reSizeImage();
        
        //alert("xxx");
        //console.log("Fire event open menu");
        
        //alert("xxx");
        
    });

}

function configMenuResize() {
    "use strict";
    var leftMenu = $(".left-menu");
    var rightContentBody = $(".body-content-right");
    var leftContentBody = $(".body-content-left");
    var scrollMenu = $(".left-menu-scroll");
    
    leftMenu.css("height",$(window).height() - 48);//48 is height of header
    scrollMenu.css("max-height",$(window).height() - 48);//48 is height of header
    rightContentBody.css("width",$(window).width() - leftContentBody.width() - 20);//240 is width of left menu, 15 id margin left of item
}

/*-----------------------*/
/*04.toogleAuto*/
/*-----------------------*/
function toogleAuto() {
    "use strict";

//    var triggerObject01 = $('.channel-info .btn-follow');
    $('body').on('click', '.control-play .line', function () {
        $(this).toggleClass("line-active");
    });
//    triggerObject01.click(function () {
//        $(this).toggleClass("btn-followed");
//    });

}

/*-----------------------*/
/*05.shortDescription*/
/*-----------------------*/
function shortDescription() {
    "use strict";


    var objectTrigger = ".btn-view-full";
    var objectTaget = ".info-video-description";
    $("body").on("click", objectTrigger, function () {
        $(objectTaget).toggleClass("open-description", 1000, "easing");
    });

    var objectTrigger2 = ".link-edit-video";
    var objectTaget2 = ".accordion-wrap";
    $(objectTrigger2).click(function () {
        $(objectTaget2).toggleClass("open", 1000);
    });
}


/*-----------------------*/
/*06.switchView*/
/*-----------------------*/
function swicthView() {
    "use strict";

    var objectTrigger = $(".switch-view span");
    var objectTaget = $(".list-video");

    var objectTriggerV = $(".switch-verizontal");
    var objectTriggerH = $(".switch-horizontal");

    objectTrigger.click(function () {
        $(".switch-view span").removeClass("selected");
        $(this).addClass("selected");
        objectTaget.addClass("list-video-horizontal-left list-video-horizontal-left-full");
    });

    objectTriggerV.click(function () {
        objectTaget.removeClass("list-video-horizontal-left list-video-horizontal-left-full");
    });

    objectTriggerH.click(function () {
        objectTaget.addClass("list-video-horizontal-left list-video-horizontal-left-full");
    });
}


/*-----------------------*/
/*06.Tab*/
/*-----------------------*/

function openTab() {
    "use strict";
}


// Get the element with id="defaultOpen" and click on it

var item = document.getElementById("defaultOpen");
if (item !== undefined && item !== null) {
    item.click();
}

function tab(evt, cityName) {
    "use strict";

    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.height = "0";
        tabcontent[i].style.overflow = "hidden";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.height = "auto";
    document.getElementById(cityName).style.overflow = "initial";
    evt.currentTarget.className += " active";
}

/*-----------------------*/
/*08.according*/
/*-----------------------*/
function accroding() {
    "use strict";

    var objectTrigger = $(".list-tab .item .text-1");
    var objectTaget = $(".list-tab .item");

    objectTrigger.click(function () {
        objectTaget.removeClass("selected");
        $(this).parent().toggleClass("selected", 1000, "easing");
    });

}

/*-----------------------*/
/*09.search*/
/*-----------------------*/
function search() {
    "use strict";

//  var objectTrigger = $(".box-search .input-text");
    var objectTrigger01 = $(".box-search .delete-search");
    var objectTaget = $(".box-search .suggesstion");

//  objectTrigger.keypress(function(){
//      objectTaget.show();
//  });
//
    objectTrigger01.click(function () {
        $(this).parent().hide();

    });

//    $(window).click(function () {
//
//        objectTaget.hide();
//    });

//  //prevent click effection
//  $(".box-search, a, button, input").click(function(event){
//      event.stopPropagation();
//  });
}

/*-----------------------*/
/*09.notify*/
/*-----------------------*/
function notify() {
    "use strict";

    var objectTrigger = $("header .mdl-notify .button-link-more");
    var objectTrigger02 = $("header .link-notify");
    var objectTrigger03 = $("header .list-notify .item");
    var objectTaget = $("header .mdl-notify");
    var objectTagetSub = $(".mdl-notify .item-popover");

    var objectTrigger04 = $(".channel-info .link-notify");
    var objectTrigger05 = $(".list-subscription .link-notify");
    var currentTop = 0;


    var scrollHandler = function () {
        window.scrollTo(0, currentTop);
        //alert(top);
    };


    //Step 01
    objectTrigger02.click(function () {
        objectTrigger03.removeClass("selected");
        objectTaget.toggle();
        currentTop = $(window).scrollTop();
        $(window).scroll(scrollHandler);
    });

    //Step 02
    $(document).on('click', '.list-notify .button-link-more', function () {
        $(window).off("scroll", scrollHandler);
        if ($(this).parent().parent().hasClass("selected")) {
            $(this).parent().parent().toggleClass("selected");
            return false;
        }

        objectTrigger03.removeClass("selected");
        $(this).parent().parent().addClass("selected");
    });

    $(document).click(function (e) {
        var objPrevent = 'i.fa.icon-notify, span.link-notify, i.icoMoreNotification.fa.fa-ellipsis-v, span.button-link-more';
        if (!$(objPrevent).is(e.target)) {
            $(window).off("scroll", scrollHandler);
            if (objectTrigger03.hasClass("selected")) {
                objectTrigger03.removeClass("selected");

                return false;
            }
            objectTaget.hide();

        }
    });

    //Step 04
    $(objectTagetSub).click(function () {
        /*$(this).parent().hide();*/
        objectTrigger03.removeClass("selected");
    });


    // //prevent click  effection
    // $(".mdl-notify, .link-notify, button, input").click(function (event) {
    //     event.stopPropagation();
    // });


    //OnOffNotify
    // objectTrigger04.click(function () {
    //     $(this).children().toggleClass("icon-notify");
    //     $(this).children().toggleClass("icon-ringoff");
    // });
    objectTrigger05.click(function () {
        $(this).children().toggleClass("icon-notify");
        $(this).children().toggleClass("icon-ringoff");
    });
}

/*-----------------------*/
/*10.popoverAll*/
/*-----------------------*/
function popoverAll() {
    "use strict";

    var objectTrigger = [".sort-comment", ".sort-video", ".function-more", ".sort-type-video", ".btn-share", ".function-add-playlist", ".link-account"];

    //Prevert event
    var objectPrevert = [".sort-comment", ".sort-comment .popover-more", "a", "button", "input", ".sort-video", ".sort-video .popover-more", " .function-more", ".function-more .popover-more", ".sort-type-video", ".sort-type-video .popover-more", ".btn-share", ".btn-share .content-share", ".function-add-playlist", ".function-add-playlist .content", ".link-account", ".link-account .popover-more"];


    //Step 01
    for (var i = 0; i < objectTrigger.length; i++) {
        $(document).on("click", objectTrigger[i], function (event) {
            //var x = $(this).offset();//coodiate 1
            var x = event.screenY;
            var x2 = $(this).children('.popover-more').height();
            var heightWindow = $(window).height();
            var totalHeight = x + x2 + 20; //20 is height of button


            $('.popover-more').removeClass('popover-more-top');
            if (totalHeight > heightWindow) {
                $('.popover-more').addClass('popover-more-top');
            }
            if ($(this).hasClass("selected")) {
                $(this).toggleClass("selected");
                return false;
            }

            for (var j = 0; j < objectTrigger.length; j++) {
                $(objectTrigger[j]).removeClass("selected");
            }

            $(this).addClass("selected");

        });
    }

    //Step 02
    $(window).click(function () {
        for (var i = 0; i < objectTrigger.length; i++) {
            $(objectTrigger[i]).removeClass("selected");
        }
    });


    for (var e = 0; e < objectPrevert.length; e++) {
        $(document).on("click", objectPrevert[e], function (event) {
            //$(objectTarget).toggle();
            event.stopPropagation();
        });
    }


    //Step 03
    // $(objectTargetSub).click(function(){
    //  //Close if click in child item
    //     $(objectTarget).hide();
    //     $(this).parent(objectTarget).hide();
    // });
    var itemTarget = ".sort-video .item-popover";
    $(itemTarget).click(function () {
        //Close if click in child item
        $(itemTarget).removeClass("item-selected");
        $(this).parents(".sort-video").removeClass("selected");
        $(this).addClass("item-selected");
    });
}

/*-----------------------*/
/*11.reSizeImage*/
/*-----------------------*/
function reSizeImage() {
    "use strict";
    var height01, height02, height03, height04, height05;
    var image01 = $(".image16x9");
    var image02 = $(".image4x3");
    var image03 = $(".image4x4");
    var image04 = $(".image6x1");

    var image05 = $(".image3x1");

    height01 = (image01.parent(".image").width() * 9) / 16;//Ratio 16x9
    height02 = (image02.parent(".image").width() * 3) / 4;//Ratio 4x3
    height03 = image03.parent(".image").width();//Ratio 4x4
    height04 = (image04.parent(".mdl-banner").width() * 1) / 6;//Ratio 6x1

    image01.css("height", height01);
    image02.css("height", height02);
    image03.css("height", height03);
    image04.css("height", height04);

    //alert("xxx" + image01.parent(".image").width());
}
/*-----------------------*/
/*12.offZoompage*/
/*-----------------------*/
var rtimeResizeend;
var timeoutResizeend = false;
var deltaResizeend = 200;

function resizeEnd() {
    "use strict";
    if (new Date() - rtimeResizeend < deltaResizeend) {
        setTimeout(resizeEnd, deltaResizeend);
    } else {
        timeoutResizeend = false;
        reSizeImage();
        configMenuResize();
    }
}
// function configMenuResize() {
//     var leftMenu = $(".left-menu");
//     var rightContentBody = $(".body-content-right");
//     var leftContentBody = $(".body-content-left");
//     var scrollMenu = $(".left-menu-scroll");
//
//     leftMenu.css("height", $(window).height() - 48);//48 is height of header
//     scrollMenu.css("max-height", $(window).height() - 48);//48 is height of header
//     rightContentBody.css("width", $(window).width() - leftContentBody.width());//240 is width of left menu, 15 id margin left of item
// }


/*-----------------------*/
/*14.fireOwlcarousel*/
/*-----------------------*/
function fireOwlcarousel() {
    "use strict";
    if ($(".owl-carousel")[0]) {
        var maxNumberCarousel = 10;
        //var countItem,countItemPramater;

        for (var x = 0; x < maxNumberCarousel; x++) {
            var nameOfCarousel = '.owl-carousel-' + x;

            $(nameOfCarousel).on('initialized.owl.carousel', function (event) {
                hideControl(event.item.count, nameOfCarousel);
            });

            $(nameOfCarousel).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 4
                    },
                    1000: {
                        items: 4
                    },
                    1400: {
                        items: 5
                    },
                    1800: {
                        items: 6
                    },
                    2000: {
                        items: 7
                    }
                },
                dot: false
            });
        }


    }
}

function hideControl(countItemPramater, name) {
    //console.log('Count: ' + countItemPramater);
    var control = name + ' .owl-controls';
    if (countItemPramater <= 4) {
        $(control).hide();
    }
}

    fireOwlcarousel();
// $(document).ready(function () {
    $("#selectLanguages").selectmenu({
        width: false,
        classes: {
            "ui-selectmenu-button": "select-language"
        },
        change: function (event, data) {
            var x = document.getElementsByName('myform');
            x[0].submit();

        }
    });

    $('.showMoreDes').on('click', function () {
        $('#descriptionVideo').css('height', 'auto');
        $('#descriptionVideo').css('display', 'inline-block');
    });

    $('.hideDes').on('click', function () {
        $('#descriptionVideo').css('height', '40px');
        $('#descriptionVideo').css('display', '');
    });
// });
