"use strict";

/* @author dungld5 */
var VideoLazyPage = React.createClass({
    displayName: 'VideoLazyPage',

    getInitialState: function getInitialState() {
        return {
            clientData: null,
            appendContents: [],
            isFirstTimeLoad: true,
            limit: parseInt($("#homeItemLimit").val()),
            offset: 0,
            lockScroll: false,
            endOfData: false,
        };
    },
    componentWillMount: function componentWillMount() {
        if (this.state.offset == 0) {
            this.updateViewport();
        } else {
            this.state.isFirstTimeLoad = false;
        }
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
//        console.log(prevProps.needActiveId);
//        console.log(prevState.needActiveId);
    },
    componentDidMount: function componentDidMount() {
        window.addEventListener('scroll', this.updateViewport, false);
    },
    componentWillUnmount: function componentWillUnmount() {
        window.removeEventListener('scroll', this.updateViewport);
    },
    updateViewport: function updateViewport() {
        // Neu khong con DATA thi return ''
        // Neu chuyen link thi ko ch
        if (this.state.endOfData) {
            return;
        }
        if (this.props.tab) {
            if (!document.getElementById(this.props.tab))
                return;
        }
        if (this.state.isFirstTimeLoad || (!this.state.isFirstTimeLoad && !this.state.lockScroll && document.getElementById('contentVideoBlock').scrollHeight < window.pageYOffset + window.innerHeight + 100)) {
            this.setState({lockScroll: true, isFirstTimeLoad: false});
            $.ajax({
                url: this.props.loadMoreUrl,
                dataType: 'json',
                type: 'GET',
                data: {'limit': this.state.limit, 'offset': this.state.offset},
                success: function (ajaxData) {
                    var contentList = ajaxData.data.content;

                    // Neu khong lay dc content thi STOP
                    if (contentList == null || contentList.length == 0) {
                        this.setState({endOfData: true, lockScroll: false});
                        return;
                    }
                    var oldContentList = this.state.appendContents;
                    oldContentList = oldContentList.concat(contentList);

                    if (contentList.length > 0 && contentList.length >= this.state.limit) {
                        this.setState({offset: this.state.offset + contentList.length, lockScroll: false});
                    }
                    this.setState({appendContents: oldContentList, lockScroll: false, type: ajaxData.data.type});

                    // Neu item tra ve < limit thi ko load nua
                    if (contentList.length < this.state.limit) {
                        this.setState({endOfData: true, lockScroll: false});
                    }
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    this.setState({lockScroll: false});
                }.bind(this)
            });
        } else {
            this.state.isFirstTimeLoad = false;
        }
    },
    render: function render() {
        var rows = [];
        var serverResponse = this.state.appendContents;
        if (!serverResponse)
            return spinner;

        if (serverResponse && serverResponse.length > 0) {

            if (this.state.offset == 0) {
                this.setState({'offset': (serverResponse) ? serverResponse.length : 0});
            }
            if (document.getElementById('playAllVideoChannel')) {
                $("#playAllVideoChannel").attr("href", '/channel/play-all/' + $('#idChannel').val());
            }
            switch (this.state.type) {
                case 'VOD':
                    rows.push(React.createElement(VideoBox, {videos: serverResponse, tab: this.props.tab}));
                    break;
                case 'USER_PLAYLIST':
                    rows.push(React.createElement(PlaylistBox, {videos: serverResponse, tab: this.props.tab}));
                    break;
            }

        }
        //Hien thi loading
        if (this.state.lockScroll == true) {
            rows.push(spinner);
        }
        //Ket thuc Scroll
        if (this.state.endOfData) {
            if (serverResponse.length == 0) {
                this.state.lockScroll = false;
                rows = [];
                return React.createElement(
                    'div',
                    null,
                    trans("Không có dữ liệu")
                );
            }

            rows.push(React.createElement('div', {className: 'clearfix'}));
        }
        return React.createElement(
            'div',
            null,
            rows
        );
    }
});
var VideoBox = React.createClass({
    displayName: 'VideoBox',

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/data/16x9.png",
            defaultImage4x4: "/images/data/4x4.png",
            channel: $("#channel-display") ? true : false
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
        reSizeImage();
    },
    componentDidMount: function componentDidMount() {
        reSizeImage();
    },
    componentWillUnmount: function componentWillUnmount() {
        reSizeImage();
    },
    render: function render() {
        var _this = this;

        var videos = [];

        for (var i = 0; i < this.props.videos.length; i++) {
            var userChannel = "";
            var videoFreeIcon = "";
            var triggerComment = "";
            if (this.props.videos[i]['price_play'] == 0) {
                videoFreeIcon = React.createElement("div",
                    {className: "attach-img"},
                    React.createElement("img",
                        {src: "/images/freeVideo.png" })
                );
            }

            if (this.props.videos[i]['channel_id'] && _this.state.channel) {
                userChannel = React.createElement("a",
                    {href: "/channel/" + this.props.videos[i]['channel_id'], className: "name-author", title: this.props.videos[i]['fullUserName']},
                    "" + this.props.videos[i]['fullUserName']+'123',
                    // React.createElement(
                    //         "span",
                    //         {className: "status-follow"},
                    //         React.createElement("i",
                    //                 {className: "icon-checker"}
                    //         )
                    //         )
                );
            }


            videos.push(
                React.createElement(
                    "div",
                    {className: "item list-video-" + this.props.videos[i]['id']},
                    React.createElement("div",
                        {className: "image"},
                        React.createElement("a",
                            {href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'], title: this.props.videos[i]['fullName'], className: "image"},
                            React.createElement(
                                "img", {src: this.props.videos[i]['coverImage'],
                                    onError: function onError(e) {
                                        e.target.src = _this.state.defaultImage16x9
                                    },
                                    className: "image16x9",
                                    style: {height: "100%"},
                                    alt: this.props.videos[i]['fullName']
                                }
                            )
                        ),
                        videoFreeIcon,
                        React.createElement("span",
                            {className: "time"},
                            "" + this.props.videos[i]['duration'])
                    ),

                    React.createElement("div",
                        {className: "description"},
                        React.createElement("div",
                            {className: "function-more"},
                            React.createElement("span",
                                {className: "button-link-more"},
                                React.createElement("i",
                                    {className: "fa fa-ellipsis-v"}
                                )
                            ),
                            React.createElement("div",
                                {className: "popover-more"},
                                React.createElement("div",
                                    {className: "item-popover function-add-playlist", "data-idbox": this.props.tab, "data-videoid": this.props.videos[i]['id']},
                                    trans("Thêm vào danh sách phát")
                                ),
                                React.createElement("div",
                                    {className: "item-popover function-add-playlater", "data-videoid": this.props.videos[i]['id']},
                                    trans("Thêm vào xem sau")
                                ),
                                // React.createElement("div",
                                //         {className: "item-popover function-add-share", "data-id": this.props.videos[i]['id'], "data-url": this.props.videos[i]['linkSocial'] },
                                //     trans("Chia sẻ")
                                //         ),

                                // React.createElement("div",{className: "item-popover my-video-delete", "data-videoid": this.props.videos[i]['id']}, trans("Xoá")),
                            ),
                            React.createElement("div",
                                {className: "cols-info"},
                                React.createElement("div", {className: "function-add-playlist", "data-videoid": this.props.videos[i]['id']},
                                    React.createElement("div",
                                        {className: "content", id: "blockAddMorePL" + this.props.tab + this.props.videos[i]['id']},
                                        React.createElement("div", {className: "title"}, trans("Thêm vào")),
                                        React.createElement("div", {className: "scroll myPlaylistBox", id: "myPlaylistBox" + this.props.tab + this.props.videos[i]['id'], 'data-firstload': 0}),
                                        React.createElement("div", {className: "bottom-playlist"},
                                            React.createElement("span", {className: "btn-addplaylist"},
                                                React.createElement("i", {className: "icon-plus btnShowAddNewPL"}),
                                                trans("Tạo playlist mới")
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        React.createElement("a",
                            {href: "/video/" + this.props.videos[i]['id'] + "/" + this.props.videos[i]['slug'], className: "name-video", title: this.props.videos[i]['fullName']},
                            this.props.videos[i]['name']
                        ), userChannel,
                        React.createElement("div",
                            {className: "info"},
                            React.createElement("div",
                                {className: "cols-info"},
                                this.props.videos[i]['play_times'] + trans(" lượt xem")
                            ),
                            React.createElement("div",
                                {className: "cols-info"},
                                React.createElement("i",
                                    {className: "fa fa-circle"}
                                ),
                                "" + this.props.videos[i]['publishedTime']
                            )
                        )
                    )
                )
            );
        }

//        formatImage();

        return React.createElement(
            'div',
            {className: 'list-video'},
            videos
        );

    }
});


var PlaylistBox = React.createClass({
    displayName: "PlaylistBox",

    getInitialState: function getInitialState() {
        return {
            defaultImage16x9: "/images/16x9.gif",
            defaultImage4x4: "/images/4x4.gif"
        };
    },
    componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
        reSizeImage();
    },
    componentDidMount: function componentDidMount() {
        reSizeImage();
    },
    componentWillUnmount: function componentWillUnmount() {
        reSizeImage();
    },
    render: function render() {
        var videos = [];
        for (var i = 0; i < this.props.videos.length; i++) {
            videos.push(React.createElement(
                "div",
                {className: "item"},
                React.createElement(
                    "a",
                    {href: "/playlist/xem-toan-bo-playlist/" + this.props.videos[i]['id'], className: "image", title: this.props.videos[i]['name']},
                    React.createElement("img", {src: this.props.videos[i]['coverImage'], onError: function onError(e) {
                            e.target.src = '/images/data/16x9.png';
                        }, className: "frame-image"}),
                    React.createElement(
                        "span",
                        {className: "number"},
                        this.props.videos[i]['num_video'],
                        React.createElement("br", null),
                        React.createElement("i", {className: "icon-playlist"})
                    ),
                    React.createElement(
                        "span",
                        {className: "overlay"},
                        React.createElement("i", {className: "fa fa-play"}),
                        trans("PHÁT TẤT CẢ")
                    )
                ),
                React.createElement(
                    "div",
                    {className: "description"},
                    React.createElement(
                        "a",
                        {href: "/playlist/chi-tiet-playlist/" + this.props.videos[i]['id'], className: "name-video"},
                        this.props.videos[i]['name']
                    )
                )
            ));
        }
        return React.createElement(
            "div",
            null,
            videos
        );
    }
});


var spinner = React.createElement(
    "div",
    null,
    React.createElement('div', {className: 'clearfix'}),
    React.createElement(
        'div',
        {className: 'spinner'},
        React.createElement(
            'div',
            {className: 'quarter'},
            React.createElement('div', {className: 'circle'})
        )
    ),
    React.createElement('div', {className: 'clearfix'})
);
$("#most-popular, #video-old, #video-new").on('click', function () {
    var id = $(this).attr("data-id");
    loadVideo(id);
});

$("#view-video").on('click', function () {
    var id = $(this).attr("data-id");
    loadVideo(id);
});

$("#view-playlist").on('click', function () {
    var id = $(this).attr("data-id");
    loadVideo2(id);
});
$("#most-popular-cate, #video-new-cate, #video-most-view-cate").on('click', function () {
    var id = $(this).attr("data-id");
    var name = $(this).attr("data-name");
    $("#title-cate").html(name);
    $('.content-item').remove();
    $('#list-content').append('<div class="content-item" id="' + id + '"></div>');
    loadVideo(id);
});


function loadVideo(id) {
    $("#content-tab").html('');
    $("#content-tab").html('<div class="content-item ' + tab + '" id="' + id + '" class=""></div>');
    ReactDOM.render(React.createElement(VideoLazyPage, {url: "/default/load-more-content?id=" + id,
        loadMoreUrl: "/default/load-more-content?id=" + id,
        tab: id,
        needActiveId: "#" + id}), document.getElementById(id));
}
function loadVideo2(id) {
    $("#" + id).html("");
    ReactDOM.render(React.createElement(VideoLazyPage, {url: "/default/load-more-content?id=" + id,
        loadMoreUrl: "/default/load-more-content?id=" + id,
        tab: id,
        needActiveId: "#" + id}), document.getElementById(id));
}


$(document).ready(function () {
    if (document.getElementById("auto-load")) {

        var id = $("#auto-load").val();
        var name = $("#auto-load").attr("data-name");
        $("#title-cate").html(name);
        $('.content-item').remove();
        $('#list-content').append('<div class="content-item" id="' + id + '"></div>');
        loadVideo(id);
    }
});


