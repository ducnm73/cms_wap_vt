var isError = false;

function enableUpload(id) {
    $(id).text(trans("Đăng tải"));
    $(id).removeAttr("disabled");
}

function showPopup(id) {
    $(id).fadeIn();
}
if (document.getElementById('uploader')) {
    var sessionId = guid();
    setCookie('upload_session_id', sessionId, 1);
    var newFileName = "";
    var uploader = new qq.FineUploader({
        debug: false,
        element: document.getElementById("uploader"),
        template: 'qq-template-gallery',
        request: {
            endpoint: '/account/push-file',
        },
        multiple: false,
        thumbnails: {
            placeholders: {
                waitingPath: '',
                notAvailablePath: ''
            }
        },
        validation: {
            allowedExtensions: ['mp4', 'flv', 'mov', 'm4u'],
            sizeLimit: 900000000
        },
        messages: {
            typeError: "{file}"+ trans("File không đúng định dạng:") +"{extensions}.",
            sizeError: "{file}"+ trans("Kích thước file quá lớn") +" {sizeLimit}",
            minSizeError: "{file}"+ trans("Kích thước file quá nhỏ") +" {minSizeLimit}.",
            emptyError: "{file} "+ trans("rỗng, vui lòng chọn file khác."),
            noFilesError: trans("Vui lòng chọn file upload"),
            onLeave: trans("Đang thực hiện upload file, nếu thoát sẽ hủy thực hiện upload."),
            unsupportedBrowserIos8Safari: trans("Trình duyệt không hỗ trợ")
        },
        showMessage: function (message) {
            toastr.error(message, {closeButton: true});
        },
        autoUpload: true,
        debug: true,
        text: {
            defaultResponseError: trans("Upload không thành công"),
            uploadButton: trans("Chọn video file")
        },
        callbacks: {

            onUpload: function (id, name){
                $.ajax({
                    type: "POST",
                    url: '/account/kpi-upload-start',
                    data: {
                        'session_id': getCookie('upload_session_id')
                    }
                });
            },

            onError: function (id, name, errorReason, xhrOrXdr) {
                // $('#showProgressBar').css("width", "0%");
                // // $('#showProgressBar').html('');
                if (xhrOrXdr) {
                    // if(errorReason){
                    //     toastr.error(errorReason, {closeButton: true});
                    // }else{
                    // toastr.error(trans("Upload không thành công, vui lòng thử lại sau!"), {closeButton: false});
                    // var popup = window.openZZZZ
                    //}
                    // var popup = window.open("Upload không thành công, vui lòng thử lại sau!", '', "width=400, height=400");
                    this.cancelAll();
                }
                isError = true;
                $('#show_error').show();
            },
            onSubmit: function (id, fileName) {
                var lastIndex = fileName.lastIndexOf(".");
                newFileName = fileName.substring(0, lastIndex);
                newFileName = capitalizeFirstLetter(newFileName);
                newFileName = removeSpecialCharacters(newFileName);
                $('#videoFileName').text(htmlEntities(newFileName));
                $('#txtVideoName').val(htmlEntities(newFileName));
                $('#category').val(htmlEntities(newFileName));
                var newParams = {
                    'title': newFileName,
                    '_csrf': $('meta[name="csrf-token"]').attr("content"),
                    'session_id': getCookie('upload_session_id')
                };
                
                this.setParams(newParams);
            },
            onComplete: function (event, id, name, responseJSON) {
                var videoId = null;
                if (!isError) {
                    toastr.success(trans("Đăng tải thành công"), {closeButton: true});
                    $('.modifyInfoVideo').attr("data-processing", 0);
                    $('#showProgressBar').css("width", "100%");
                    $('#showProgressBar').html(trans("Hoàn Thành"));
                    var arr = JSON.parse(responseJSON.response);
                    videoId = arr.video_id;
                    $('#idVideo').val(videoId);
                    $('#textOnProgress').hide();
                    $('#boxUploadImage').show();
                    //$('.accordion-wrap').removeClass("open");
                    $('#btnVideoBox').show();

                    $(".modifyInfoVideo").val(trans("Đồng ý"));
                    $(".modifyInfoVideo").removeAttr("disabled");
					$('.modifyInfoVideo').css("background", "#388dde");
                    if(!arr.isHasAccountInfo){
                        $("#need-show-popup-update-info").val(1);
                    }else{
                        $("#need-show-popup-update-info").val(0);
                    }

                } else {
                    $('#step2').hide();
                    $('#step1').show();
                    $('#boxUploadImage').hide();
                }

            },
            onProgress: function (id, name, uploadedBytes, totalBytes) {

                var tmp = (uploadedBytes / totalBytes) * 100;
                var percentUpload = Math.round(tmp);
                $('#textOnProgress').show();
                if (percentUpload == 100) {
                    percentUpload = 99;
                }
                $('#showProgressBar').css("width", percentUpload + "%");
                $('#showProgressBar').html(percentUpload + "%");

            },
            onStatusChange: function (id, oldStatus, newStatus) {
                $(".modifyInfoVideo").val(trans("Đang chờ upload..."));
                $(".modifyInfoVideo").attr("disabled", "disabled");
				$('.modifyInfoVideo').css("background", "#ccc");
				
                $('.accordion-wrap').addClass("open");
                $('.modifyInfoVideo').attr("data-processing", 1);
                //$('#btnVideoBox').hide();
                if (isError == false) {
                    $('#step1').fadeOut();
                    $('#step2').fadeIn();
                } else {
                    isError = false;
                    $('#step1').fadeIn();
                    $('#step2').fadeOut();
                }
            }
        }
    });
    $('#cancelUploadId').click(function () {
        $('.row-form.notice').html(trans("Bạn có muốn hủy upload file này?"));
        $('#modal-confirm').show();
        $('#modal-confirm .btn-ok').click(function () {
            $(".qq-upload-cancel").trigger("click");
            $('#showProgressBar').css("width", "0%");
            $('#showProgressBar').html('');
            $('#step1').fadeIn();
            $('#step2').fadeOut();
            $('#modal-confirm').hide();
        });
    });
}
if (document.getElementById('uploader-image')) {

    new qq.FineUploader({
        element: document.getElementById("uploader-image"),
        template: 'qq-template-image',
        request: {
            endpoint: '/account/push-image-file'
        },
        multiple: false,
        thumbnails: {
            placeholders: {
                waitingPath: '',
                notAvailablePath: ''
            }
        },
        validation: {
            allowedExtensions: ['png', 'gif', 'jpg', 'jpeg'],
            sizeLimit: 8388608
        },
        autoUpload: true,
        debug: false,
        text: {
            defaultResponseError: trans("Upload không thành công"),
            uploadButton: "<div>Chọn video file</div>"
        },
        messages: {
            typeError: "{file} "+ trans("File không đúng định dạng:") +" {extensions}.",
            sizeError: "{file} "+ trans("Kích thước file quá lớn") +" {sizeLimit}",
            minSizeError: "{file}"+ trans("Kích thước file quá nhỏ") +" {minSizeLimit}.",
            emptyError: "{file} "+ trans("rỗng, vui lòng chọn file khác."),
            noFilesError: trans("Vui lòng chọn file upload"),
            onLeave: trans("Đang thực hiện upload file, nếu thoát sẽ hủy thực hiện upload."),
            unsupportedBrowserIos8Safari: trans("Trình duyệt không hỗ trợ")
        },
        showMessage: function (message) {
            toastr.error(message, {closeButton: true});
        },
        callbacks: {
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (xhrOrXdr) {
                    // if(errorReason){
                    //     toastr.error(errorReason, {closeButton: true});
                    // }else{
                    // toastr.error(trans("Upload không thành công, vui lòng thử lại sau!"), {closeButton: true});
                    //}
                    this.cancelAll();
                }

                isError = true;
                $('#show_error').show();
            },
            onProgress: function (id, name, uploadedBytes, totalBytes) {
                $('#loadingUploadImage').show();
                $('input:file').attr('disabled', 'disabled');
            },
            onSubmit: function (id, name) {
                var params = {
                    video_id: $('#idVideo').val(),
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                };
                this.setParams(params);
            },
            onComplete: function (event, id, name, responseJSON) {
                isError = false;
                if (!isError) {
                    $('#showImageVideo').show();
                    var arr = JSON.parse(responseJSON.response);
                    $("#imageUploadVideo").attr("src", arr.file_path);
                }
                $('#loadingUploadImage').hide();
                $('input:file').removeAttr('disabled');
            }
        }
    });
}
function validateForm(){
    $validate = true;
    $message = [];
    $mess = '';
    if($('#txtVideoName').val() == '') {
        $mess = $('#txtVideoName').attr('alt') + " " + trans('không được để trống')
        $message.push($mess);
        $('#txtVideoName').focus();
        $validate = false;
    }else if($('#txtVideoName').val().length > 255){
        $mess = $('#txtVideoName').attr('alt') + " " + trans('không được quá 255 ký tự')
        $message.push($mess);
        $validate = false;
    }else if($('#category').val() == ''){
        $mess = $('#category').attr('alt') + " " + trans('không được để trống')
        $message.push($mess);
        $validate = false;
    }else if($('#channel_id').val()== ''){
        $mess = $('#channel_id').attr('alt') + " " + trans('không được để trống')
        $message.push($mess);
        $validate = false;
    }else{
        $validate = true;
    }
    if($message.length > 0){
        $message.forEach(function(item) {
            toastr.error(item, {closeButton: true});
            isError = true;
            $('#show_error').show();
        });

    }
   return $validate;
}
$(document).ready(function () {
    $('.link-edit-video').click(function () {
        $('#btnVideoBox').hide();
    });
    var selectedCat = 0;
    $('#category').change(function(){
        selectedCat = $(this).children("option:selected").val();
    });
    $('input[name="qqfile"]').attr("title",trans("Kích thước file không quá") + " 900MB");
    $toolTip = '<div class="color_red">' + trans("Kích thước file không quá") + " 5MB" + '</div>';
    $('#uploader-image input[name="qqfile"]').attr("title",trans("Kích thước file không quá") + " 5MB");
    $('#uploader-image #boxUploadImage').after($toolTip);
    $('.modifyInfoVideo').click(function () {
        if ($('.modifyInfoVideo').attr("data-processing") == 1) {
            toastr.error(trans("Đang trong quá trình tải video, không thể sửa."), {closeButton: true});
            return;
        }
        if(validateForm()){
            $.ajax({
                type: "POST",
                url: '/account/update-media',
                dataType: 'json',
                data: {
                    'id': $('#idVideo').val(),
                    'name': $('#txtVideoName').val(),
                    'description': $('#description').val(),
                    'tag': $('#tags').val(),
                    'mode': $('#mode').val(),
                    'category': selectedCat,
                    'channel_id': $('#channel_id').val(),
                    '_csrf': $('meta[name="csrf-token"]').attr("content")
                },
                success: function (data) {
                    if (data.responseCode == 200) {
                        toastr.success(data.message, {closeButton: true});
                        $('.accordion-wrap').removeClass("open");
                        $('#btnVideoBox').show();

                        //--
                        if( $("#need-show-popup-update-info").val() ==1 ){
                            $("#modal-upload-complete").show();
                        }




                    } else {
                        toastr.error(data.message, {closeButton: true});
                    }
                }
            });
        }

    });
});
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function removeSpecialCharacters(string) {
    string = string.replace(/#|_|-|@|\$|%|\^|!|&|\*/g, ' ');
    return string.replace(/\s{2,}/g, ' ');
}



function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}