$(document).ready(function () {
    "use strict";
    var maxNumberCarousel = 10;
    //var countItem,countItemPramater;

    for (var x = 0; x < maxNumberCarousel; x++) {
        var nameOfCarousel = '.owl-carousel-' + x;

        $(nameOfCarousel).on('initialized.owl.carousel', function (event) {
            hideControl(event.item.count, nameOfCarousel);
        });

        $(nameOfCarousel).owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 4
                },
                1000: {
                    items: 4
                },
                1400: {
                    items: 5
                },
                1800: {
                    items: 6
                },
                2000: {
                    items: 7
                }
            },
            dot: false
        });
    }

    function hideControl(countItemPramater, name) {
        //console.log('Count: ' + countItemPramater);
        var control = name + ' .owl-controls';
        if (countItemPramater <= 4) {
            $(control).hide();
        }
    }
})