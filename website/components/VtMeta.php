<?php

/**
 * User: dungld5
 * Date: 09/12/2016
 * Time: 4:38 PM
 */

namespace wapsite\components;

use Yii;
use yii\base\Component;

class VtMeta extends Component {

    const TITLE = 'Mạng xã hội video - Myclip';
    const KEYWORD = 'Mạng xã hội video - Myclip';
    const DESCRIPTION = 'MXH MyClip là Mạng xã hội video clip đầu tiên hàng đầu tại Việt Nam. Được phát triển bởi Tổng Công ty Viễn thông Viettel- một trong những nhà mạng Viễn thông hàng đầu thế giới. Dịch vụ cũng là MXH đầu tiên tại Việt Nam miễn phí Data 3G cho khách hàng sử dụng, mang lại những trải nghiệm tuyệt vời nhất về chất lượng dịch vụ và chất lượng nội dung cho người dụng';
    const SITENAME = 'Mạng xã hội video';
    const TYPE = 'website';

    /**
     * Register title meta and open title meta
     * @param string $title
     */
    public function setTitle($title) {
        if (!empty($title)) {
            Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $title], 'title');
            Yii::$app->view->registerMetaTag(['content' => $title, 'property' => 'og:title'], 'og:title');
            Yii::$app->view->title = $title;
        }
    }

    /**
     * Register description meta and open graph description meta
     * @param string $description
     */
    public function setDescription($description) {
        if (!empty($description)) {
            Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $description], 'description');
            Yii::$app->view->registerMetaTag(['content' => $description, 'property' => 'og:description'], 'og:description');
        }
    }

    /**
     * Register keywords meta
     * @param string $keywords
     */
    public function setKeywords($keywords) {
        if (!empty($keywords)) {
            Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $keywords], 'keywords');
        }
    }

    /**
     * Register image meta
     * @param string $keywords
     */
    public function setImage($keywords) {
        if (!empty($keywords)) {
            Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $keywords], 'og:image');
        }
    }

    /**
     * Register site_name meta
     * @param string $keywords
     */
    public function setSiteName($keywords) {
        if (!empty($keywords)) {
            Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => $keywords], 'og:site_name');
            Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => $keywords], 'og:type');
        }
    }

    /**
     * Register app meta
     * @param string $keywords
     */
    public function setApp($keywords) {
        if (!empty($keywords))
            Yii::$app->view->registerMetaTag(['name' => 'apple-itunes-app', 'content' => $keywords], 'apple-itunes-app');
    }

    /**
     * Register type meta
     * @param string $keywords
     */
    public function setType($keywords) {
        if (!empty($keywords)) {
            Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => $keywords], 'og:type');
        }
    }

    /**
     * SetMeta
     * @param array $metadata
     */
    public function setMetas($metas) {
        $this->setSiteName(self::SITENAME);
        $this->setType(self::TYPE);
        if (!empty($metas)) {
            $this->setTitle((array_key_exists('seo_title', $metas)) ? $metas['seo_title'] : self::TITLE);
            $this->setKeywords((array_key_exists('seo_keyword', $metas)) ? $metas['seo_keyword'] : self::KEYWORD);
            $this->setDescription((array_key_exists('seo_description', $metas)) ? $metas['seo_description'] : self::DESCRIPTION);
        } else {
            //set mac dinh
            $this->setTitle(self::TITLE);
            $this->setKeywords(self::KEYWORD);
            $this->setDescription(self::DESCRIPTION);
        }
    }

    public function addDoctrineMetas($location) {
        // Sau xu ly nghiep vu seo, tam thoi tra ve mac dinh 
        $metas = array();
        self::setMetas($metas);
    }

}
