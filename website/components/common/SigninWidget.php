<?php

/**
 * Created by PhpStorm.
 * User: KhanhDQ
 * Date: 10/16/2017
 * Time: 4:21 PM
 */

namespace website\components\common;

use common\helpers\Mobile_Detect;
use website\models\VtConfig;
use yii\base\Widget;
use website\models\LoginForm;
use common\libs\VtHelper;
use Yii;

class SigninWidget extends Widget {

    public $message;
    public $head;

    public function init() {
        parent::init();
    }

    public function run() {

        $show = trim(Yii::$app->request->get('login', false));
        $url = trim(Yii::$app->request->get('backUrl', ''));
        $ip = md5(VtHelper::getAgentIp());
        $redisCache = Yii::$app->cache;
        $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
        $session = Yii::$app->session;
        if (!$url) {
            $backUrl = $session->get("historyUrl", '/');
        } else {
            $backUrl = base64_decode($url);
        }
        $configCaptchaShow = \Yii::$app->params['login.captcha.show.count'];
        $model = new LoginForm(['countFail' => $countLoginFail, 'countCaptchaShow' => $configCaptchaShow]);
        return $this->render('@website/views/partials/accountModal.php', ['head' => $this->head, 'model' => $model,
                    'countLoginFail' => $countLoginFail,
                    'configCaptchaShow' => $configCaptchaShow,
                    'show' => $show,
                    'urlBack' => $backUrl,
        ]);
    }

}
