<?php

/**
 * Created by PhpStorm.
 * User: KhanhDQ
 * Date: 09/26/2017
 * Time: 2:08 PM
 */

namespace website\components\common;

use common\modules\v2\libs\RecommendObj;
use common\modules\v2\libs\TrackingCode;
use yii\base\Widget;
use Yii;

class TrackingWidget extends Widget {

    public $msisdn;
    public $userId;

    public function init() {
        parent::init();
    }

    public function run() {
        $this->msisdn = Yii::$app->session->get('msisdn', 0);
        $this->userId = Yii::$app->session->get('userId', 0);

        $trackingContent = RecommendObj::serialize(TrackingCode::tracking_event, $this->getUserTracking(), [
            'show_recommendation' => false
        ]);

        return $this->render('@website/views/partials/trackingEvent.twig', [
            'trackingContent' => $trackingContent
        ]);

    }


    protected function getUserTracking(){
        return [
            'user_info' => json_encode([
                'user_id_db' => $this->userId,
                'email' => null,
                'name' => null,
//                'phone_number' => $this->msisdn,
                'goi_cuoc_id' => null,
                'age' => null,
                'gender' => null,
                'no_followers' => null,
            ])
        ];
    }

}
