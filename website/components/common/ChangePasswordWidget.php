<?php

/**
 * Created by PhpStorm.
 * User: KhanhDQ
 * Date: 10/16/2017
 * Time: 4:21 PM
 */

namespace website\components\common;

use yii\base\Widget;
use website\models\ChangePasswordForm;
use common\models\VtUserBase;
use Yii;
class ChangePasswordWidget extends Widget {

    public $message;
    public $head;

    public function init() {
        parent::init();
    }

    public function run() {
        $isEmptyPassword = 0;
        $session = Yii::$app->session;
        if (!$session->get("msisdn", false)) {
            return $this->redirect(\yii\helpers\Url::to(['/', 'login' => true, 'backUrl' => base64_encode(Yii::$app->request->getAbsoluteUrl())]));
        }
        // Kiem tra xem co mat khau chua?
        $userId = $session->get("userId", "");

        if ($userId) {
            $objUser = VtUserBase::getById($userId);

            if (!$objUser->password) {
                $session->set("isEmptyPassword", 1);
                $isEmptyPassword = 1;
            }
        }

        $model = new ChangePasswordForm();
        $model->username = $session->get("msisdn");
        return $this->render('@website/views/auth/changePasswordModal.twig', ['model' => $model,
        ]);
    }

}
