<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 2/6/2016
 * Time: 9:21 AM
 */
namespace wap\components\common;

use common\models\VtConfigBase;
use yii\base\Widget;

class SearchWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = 'Myclip';
        }
    }

    public function run()
    {
        $hotKeywords = VtConfigBase::getConfig("HOT_SEARCH_KEYWORD", false);

        if ($hotKeywords) {
            $topKeywords = explode(",", $hotKeywords);
        } else {
            $topKeywords = false;
        }
        
        return $this->render('@wap/views/partials/boxSearch.twig', [
            "topKeywords" => $topKeywords
        ]);
    }
}