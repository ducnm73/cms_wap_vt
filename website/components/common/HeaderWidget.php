<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 2/6/2016
 * Time: 9:21 AM
 */
namespace website\components\common;

use common\helpers\Mobile_Detect;
use website\models\VtConfig;
use yii\base\Widget;

class HeaderWidget extends Widget
{
    public $message;
    public $head;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $route = \Yii::$app->controller->getRoute();

        $detect = new Mobile_Detect();

        if($detect->isiOS()){
            $downloadUrl = VtConfig::getConfig('LINK_DOWNLOAD_IOS', '');
        }else if($detect->isAndroidOS()){
            $downloadUrl = VtConfig::getConfig('LINK_DOWNLOAD_ANDROID', '');
        }else{
            $downloadUrl = '';
        }

        return $this->render('@website/views/partials/header.twig', ['head' => $this->head, 'route' => $route, 'downloadUrl' => $downloadUrl]);
    }


}