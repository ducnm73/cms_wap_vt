<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 2/6/2016
 * Time: 9:21 AM
 */

namespace website\components\common;

use Yii;
use common\models\VtChannelFollowBase;
use common\models\VtUserPlaylistItemBase;
use yii\base\Widget;
use common\modules\v2\libs\Obj;
use common\modules\v2\libs\ChannelObj;
use common\modules\v2\libs\UserPlaylistObj;

class MenuLeftWidget extends Widget {

    public $message;

    public function init() {
        parent::init();
        if ($this->message === null) {
            $this->message = 'MobiTV';
        }
    }

    public function run() {
        $msisdn = \Yii::$app->session->get("msisdn", '');
        $userId = \Yii::$app->session->get("userId", '');
        $fullName = \Yii::$app->session->get("fullName", '');
        $isDetect3G = \Yii::$app->session->get("isDetect3G", '');
        $avatarImage = \Yii::$app->session->get("avatarImage");
        $idPlaylist = Obj::VIDEO_OF_PLAYLIST;
        $channelFollow = "";
        $myPlaylist = "";
        $limitHotChannel = Yii::$app->params['app.home.hotchannel.limit'];
        $includeHot = 1;

        if ($userId) {
            $channelFollow = ChannelObj::serialize(
                            Obj::CHANNEL_FOLLOW, VtChannelFollowBase::getChannelFollowQuery($userId, $limitHotChannel, 0), false, false, ($includeHot == 0) ? false : true, $limitHotChannel
            );
            $myPlaylist = UserPlaylistObj::serialize(
                            Obj::MY_PLAYLIST, VtUserPlaylistItemBase::getPlaylistByUserQuery($userId, Yii::$app->params['app.home.lazy.limit'] + 1, 0), false, false
            );
        } else {
            // Lay danh sach kenh
            $channelFollow = ChannelObj::serialize(
                            Obj::CHANNEL_FOLLOW, \common\models\VtUserBase::getHotUserQuery($limitHotChannel), true, false
            );
        }
        // lay chuyen muc host
        $offset = 0;
        $limit = Yii::$app->params['app.category.menu.limit'] + 1;
        $category = \common\modules\v2\libs\CategoryObj::serialize(
                        Obj::CATEGORY_PARENT, \website\models\VtGroupCategory::getParents($offset, $limit, true), true
        );
        $route = \Yii::$app->request->getAbsoluteUrl();
        $route = str_replace("index.php/", "", $route);
        $length = strlen("?");
        $route = ($length === 0 || (substr($route, -$length) === "?")) ? substr($route, 0, strlen($route) - 1) : $route;

//        echo '<pre>';
//        print_r($channelFollow['content']);
//        echo '</pre>';
//        die();

        return $this->render('@website/views/partials/menuLeft.twig', [
                    "msisdn" => $msisdn,
                    "userId" => $userId,
                    "fullName" => $fullName,
                    "route" => $route,
                    "channelFollow" => $channelFollow['content'],
                    "myPlaylist" => $myPlaylist['content'],
                    "menu" => $menu,
                    "isDetect3G" => $isDetect3G,
                    "avatarImage" => $avatarImage,
                    'idPlaylist' => $idPlaylist,
                    'category' => $category
        ]);
    }

}
