<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace website\controllers;

use common\modules\v2\libs\Obj;
use Yii;
use common\models\VtGroupCategoryBase;
use \common\libs\VtHelper;

class CategoryController extends \common\modules\v2\controllers\CategoryController {

    public function init() {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors() {

        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }

    public function actionGetCategory() {
        $this->layout = "main.twig";
        $offset = 0;
        $limit = Yii::$app->params['app.category.limit'];
        $id = Obj::CATEGORY_PARENT;
        $responseData = parent::actionGetHomeCategory($offset, $limit);
        Yii::$app->view->title = Yii::t('web','Mạng xã hội video - Myclip');
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => Yii::t('web','MXH MyClip là Mạng xã hội video clip đầu tiên hàng đầu tại Việt Nam. Được phát triển bởi Tổng Công ty Viễn thông Viettel- một trong những nhà mạng Viễn thông hàng đầu thế giới. Dịch vụ cũng là MXH đầu tiên tại Việt Nam miễn phí Data 3G cho khách hàng sử dụng, mang lại những trải nghiệm tuyệt vời nhất về chất lượng dịch vụ và chất lượng nội dung cho người dụng')
        ]);
        return $this->render('index.twig', ['responseData' => $responseData, 'id' => $id]);
    }

    public function actionGetCateChild() {
        $this->layout = "mainList.twig";
        $id = intval(trim(Yii::$app->request->get('id', 0)));
        $offset = 0;
        $limit = Yii::$app->params['app.category.limit'];
        $responseData = parent::actionGetCategoryChild($id, $offset, $limit);
        if ($responseData['responseCode'] != 200) {
            $this->redirect('/default/error', 404);
        }
        $id = Obj::CATEGORY_CHILD . "_" . $id;
        $playIdPrefix = Obj::CATEGORY_CHILD_VIDEO . "_";
        return $this->render('categoryChild.twig', ['responseData' => $responseData, 'id' => $id, 'playIdPrefix' => $playIdPrefix]);
    }

    public function actionGetCateDetail() {
        $this->layout = "mainList.twig";
        $id = intval(trim(Yii::$app->request->get('id', 0)));
        $limit = Yii::$app->params['app.home.lazy.limit'];
        $responseData = parent::actionGetDetailCategory($id, 0, $limit, "MOSTTRENDING");
        if ($responseData['responseCode'] != 200) {
            $this->redirect('/default/error', 404);
        }
        return $this->render('detailCategory.twig', ['responseData' => $responseData]);
    }

}
