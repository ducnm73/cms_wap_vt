<?php

/**
 * Created by PhpStorm.
 * User: Tuonglv
 * Date: 26/09/2017
 * Time: 10:24 AM
 */

namespace website\controllers;

use common\helpers\Utils;
use common\libs\S3Service;
use common\models\VtUserBase;
use Yii;
use common\libs\VtHelper;
use website\models\LoginForm;
use common\helpers\MobiTVRedisCache;
use wapsite\models\ChangePasswordForm;
use yii\authclient\clients\Facebook;
use yii\httpclient\Client;
use common\modules\v2\libs\ResponseCode;
use yii\helpers\Url;

class AuthController extends \common\modules\v2\controllers\AuthController
{

    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * Logs in a user.
     * @author phumx@viettel.com.vn
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'mainLogin.twig';
        $session = Yii::$app->session;
        $redisCache = Yii::$app->cache;
        $backUrl = Yii::$app->request->get("backUrl", false);

        if (!$backUrl) {
            $backUrl = $session->get("backUrl", false);
        }

        $isRedirect = $session->get("isRedirect", false);
        $referer = Yii::$app->request->referrer;
        if (!$backUrl || $referer) {
            if (($referer != Yii::$app->request->getAbsoluteUrl()) && $referer) {
                if (!$isRedirect) {
                    $session->set("backUrl", base64_encode($referer));
                }
            } else {
                $session->set("backUrl", "/");
            }
        }
        // neu co sdt hoac userId thi khong dang nhap nua
        if ($session->get("msisdn") || $session->get("userId")) {
            $this->goBack();
        }

        $ip = md5(VtHelper::getAgentIp());
        $countLoginFailCacheKey = 'count_login_fail_' . $ip;
        $countLoginFail = intval($redisCache->get($countLoginFailCacheKey));
        $configCaptchaShow = \Yii::$app->params['login.captcha.show.count'];
        $model = new LoginForm(['countFail' => $countLoginFail,'countCaptchaShow' => $configCaptchaShow]);

        if ($model->load(Yii::$app->request->post())) { 
            $model->username = Utils::getMobileNumber(trim($model->username), Utils::MOBILE_GLOBAL);
            
            if ($model->validate()) {
                $ipUser = md5(VtHelper::getAgentIp() . trim($model->username));
                $countLockCacheKey = 'count_lock_' . $ipUser;
                $countLock = intval($redisCache->get($countLockCacheKey));
                $loginLockCacheKey = 'lock_login_' . $ipUser;

                //-- Kiem tra khoa tai khoan trong 10phut
                $isLockIPUser = $redisCache->get($loginLockCacheKey);

                $configCountLock = \Yii::$app->params['login.lock.count'];

                if ($countLock >= $configCountLock || $isLockIPUser) {
                    if ($isLockIPUser) {
                        $redisCache->set($countLockCacheKey, 0, MobiTVRedisCache::CACHE_1DAY);
                    } else {
                        $redisCache->set($loginLockCacheKey, 1, MobiTVRedisCache::CACHE_10MINUTE);
                    }

                    $model->addError('', Yii::t('web','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!'));
                    return $this->render('login.php', [
                        'model' => $model,
                        'countLoginFail' => $countLoginFail,
                        'configCaptchaShow' => $configCaptchaShow
                    ]);
                }

                $user = $model->getUser();
                // Neu khong ton tai user
                if (!$user) {
                    $model->addError('', Yii::t('web','Tài khoản hoặc mật khẩu đăng nhập không đúng'));
                    $countLock ++;
                    $countLoginFail ++;
                    $redisCache->set($countLockCacheKey, $countLock, MobiTVRedisCache::CACHE_10MINUTE);
                    $redisCache->set($countLoginFailCacheKey, $countLoginFail, MobiTVRedisCache::CACHE_1DAY);
                } elseif ($model->validatePassword()) {
                    $isBlacklist = false;
                    if ($isBlacklist) {
                        $model->addError('', Yii::t('web','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!'));
                    } else {
                        // - Dang nhap thanh cong thi xoa lock
                        $redisCache->delete($countLoginFailCacheKey);
                        $redisCache->delete($countLockCacheKey);
                        $session->set('msisdn', $user->msisdn);
                        $session->set('userId', $user->id);
                        $session->set('fullName', ($user['full_name']) ? $user['full_name'] : Utils::hideMsisdn($user['msisdn']));

                        $user->last_login = date("Y-m-d H:i:s");
                        $session->set('isShowSuggest', ($user->is_show_suggest == 1) ? 0 : 1);
                        session_regenerate_id(false);
                        $this->msisdn = $user->msisdn;

                        $session->set('avatarImage', ($user['bucket'] && $user['path']) ? VtHelper::getThumbUrl($user['bucket'], $user['path'], VtHelper::SIZE_AVATAR) : '');
                        $session->set('cpId', $user->cp_id);

                        if ($user->changed_password == 1) {
                            $session->set("isRedirect", false);
                            $session->set("backUrl", false);
                            return $this->redirect(base64_decode($backUrl));
                        } else {
                            $session->set("backUrl", false);
                            $session->set("needChangePassword", 1);

                            return $this->redirect('/auth/change-password');
                        }
                    }
                } else {
                    $model->addError('', Yii::t('web','Tài khoản hoặc mật khẩu đăng nhập không đúng'));
                    $countLock ++;
                    $countLoginFail ++;
                    $redisCache->set($countLockCacheKey, $countLock, MobiTVRedisCache::CACHE_10MINUTE);
                    $redisCache->set($countLoginFailCacheKey, $countLoginFail, MobiTVRedisCache::CACHE_1DAY);
                }
            } else {
                $countLoginFail++;
                $redisCache->set($countLoginFailCacheKey, $countLoginFail, MobiTVRedisCache::CACHE_1DAY);
            }
        }

        return $this->render('login.php', [
            'model' => $model,
            'countLoginFail' => $countLoginFail,
            'configCaptchaShow' => $configCaptchaShow
        ]);
    }

    public function actionAjaxLogin()
    {
        $session = Yii::$app->session;
        $redisCache = Yii::$app->cache;
        \Yii::$app->response->format = 'json';
        $ip = md5(VtHelper::getAgentIp());
        $countLoginFail = intval($redisCache->get('count_login_fail_' . $ip));
        $configCaptchaShow = \Yii::$app->params['login.captcha.show.count'];
        // neu co sdt hoac userId thi khong dang nhap nua
        if ($session->get("msisdn") || $session->get("userId")) {
            return [
                'responseCode' => ResponseCode::SUCCESS,
                'message' => [ResponseCode::getMessage(ResponseCode::SUCCESS)],
                'data' => [
                    'userId' => $session->get("userId"),
                    'isLogin' => true,
                    'countLoginFail' => $countLoginFail,
                    'configCaptchaShow' => $configCaptchaShow
                ]
            ];
        }

        if (Yii::$app->request->post()) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }
            $request = Yii::$app->request;
            $model = new LoginForm(['countFail' => $countLoginFail, 'countCaptchaShow' => $configCaptchaShow]);
            $model->username = Utils::getMobileNumber(trim($request->post('username')), Utils::MOBILE_GLOBAL);
            $model->password = $request->post('password');
            $model->verifyCode = $request->post('verifyCode');

            if ($model->validate()) {
                $ipUser = md5(VtHelper::getAgentIp() . trim($model->username));
                $countLock = intval($redisCache->get('count_lock_' . $ipUser));

                //-- Kiem tra khoa tai khoan trong 10phut
                $isLockIPUser = $redisCache->get('lock_login_' . $ipUser);

                $configCountLock = \Yii::$app->params['login.lock.count'];

                if ($countLock >= $configCountLock || $isLockIPUser) {
                    if ($isLockIPUser) {
                        $redisCache->set('count_lock_' . $ipUser, 0, MobiTVRedisCache::CACHE_1DAY);
                    } else {
                        $redisCache->set('lock_login_' . $ipUser, 1, MobiTVRedisCache::CACHE_10MINUTE);
                    }
                    return [
                        'responseCode' => ResponseCode::LOGIN_FAIL,
                        'message' => Yii::t('web','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!'),
                        'data' => [
                            'countLoginFail' => $countLoginFail,
                            'configCaptchaShow' => $configCaptchaShow
                        ]
                    ];
                }

                $user = $model->getUser();
                // Neu khong ton tai user
                if (!$user) {
                    $redisCache->set('count_lock_' . $ipUser, $countLock + 1, MobiTVRedisCache::CACHE_10MINUTE);
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                    return [
                        'responseCode' => ResponseCode::LOGIN_FAIL,
                        'message' => [Yii::t('web','Tài khoản hoặc mật khẩu đăng nhập không đúng')],
                        'data' => [
                            'countLoginFail' => $countLoginFail,
                            'configCaptchaShow' => $configCaptchaShow
                        ]
                    ];
                }
                if ($model->validatePassword()) {
                    $isBlacklist = false;
                    if ($isBlacklist) {
                        return [
                            'responseCode' => ResponseCode::LOGIN_FAIL,
                            'message' => [Yii::t('web','Quý khách đăng nhập lỗi nhiều lần, xin vui lòng thử lại sau 10 phút!')],
                            'data' => [
                                'countLoginFail' => $countLoginFail,
                                'configCaptchaShow' => $configCaptchaShow
                            ]
                        ];
                    } else {
                        // - Dang nhap thanh cong thi xoa lock
                        $redisCache->delete('count_login_fail_' . $ip);
                        $redisCache->delete('count_lock_' . $ipUser);
                        $session->set('msisdn', $user->msisdn);
                        $session->set('userId', $user->id);
                        $session->set('fullName', ($user['full_name']) ? $user['full_name'] : Utils::hideMsisdn($user['msisdn']));
                        $user->last_login = date("Y-m-d H:i:s");
                        $session->set('isShowSuggest', ($user->is_show_suggest == 1) ? 0 : 1);
                        session_regenerate_id(false);
                        $this->msisdn = $user->msisdn;

                        $session->set('avatarImage', ($user['bucket'] && $user['path']) ? VtHelper::getThumbUrl($user['bucket'], $user['path'], VtHelper::SIZE_AVATAR) : '');
                        $session->set('cpId', $user->cp_id);

                        if ($user->changed_password == 1) {
                            return [
                                'responseCode' => ResponseCode::SUCCESS,
                                'message' => [],
                                'data' => [
                                    'userId' => $user->id,
                                    'isLogin' => true,
                                ]
                            ];
                        } else {
                            return [
                                'responseCode' => ResponseCode::SUCCESS,
                                'message' => [],
                                'data' => ['redirect' => '/auth/change-password']
                            ];
                        }
                    }
                } else {
                    $redisCache->set('count_lock_' . $ipUser, $countLock + 1, MobiTVRedisCache::CACHE_10MINUTE);
                    $redisCache->set('count_login_fail_' . $ip, $countLoginFail + 1, MobiTVRedisCache::CACHE_1DAY);
                    return [
                        'responseCode' => ResponseCode::LOGIN_FAIL,
                        'message' => [Yii::t('web','Tài khoản hoặc mật khẩu đăng nhập không đúng')],
                        'data' => [
                            'countLoginFail' => $countLoginFail,
                            'configCaptchaShow' => $configCaptchaShow
                        ]
                    ];
                }
            } else {
                $errors = array();
                if ($model->errors['username']) {
                    $errors[] = $model->errors['username'][0];
                }
                if ($model->errors['password']) {
                    $errors[] = $model->errors['password'][0];
                }
                if ($model->errors['verifyCode']) {
                    $errors[] = $model->errors['verifyCode'][0];
                }
                $countLoginFail++;
                $redisCache->set('count_login_fail_' . $ip, $countLoginFail, MobiTVRedisCache::CACHE_1DAY);
                return [
                    'responseCode' => ResponseCode::LOGIN_FAIL,
                    'message' => $errors,
                    'data' => [
                        'countLoginFail' => $countLoginFail,
                        'configCaptchaShow' => $configCaptchaShow
                    ]
                ];
            }
        }
    }

    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionChangePassword()
    {

        $isEmptyPassword = 0;

        $this->layout = 'mainLogin.twig';
        $session = Yii::$app->session;

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        if (!$session->get("msisdn", false)) {
            return Yii::$app->getResponse()->redirect('/auth/login');
        }
        // Kiem tra xem co mat khau chua?
        $userId = $session->get("userId", "");
        if ($userId) {
            $objUser = VtUserBase::getById($userId);

            if (!$objUser->password) {
                $session->set("isEmptyPassword", 1);
                $isEmptyPassword = 1;
            }
        }

        $model = new ChangePasswordForm();
        $model->username = $session->get("msisdn");

        if ($model->load(Yii::$app->request->post()))
            if ($model->validate()) {
                $user = $model->getUser();
                // Neu khong ton tai user
                if (!$user) {
                    $model->addError('', Yii::t('web','Tài khoản hoặc mật khẩu cũ không đúng'));
                } elseif ($model->validatePassword() || $isEmptyPassword == 1) {
                    $session->set("isEmptyPassword", 0);

                    $user->setPassword($model->newPassword);
                    $model = new ChangePasswordForm();
                    $model->addError('success', Yii::t('web','Thay đổi mật khẩu thành công'));
                    $session->setFlash('success', Yii::t('web','Thay đổi mật khẩu thành công'));
                    $session->set("needChangePassword", 0);
                    return $this->redirect("/");

                } else {
                    $model->addError('', Yii::t('web','Mật khẩu cũ không đúng'));
                }
            }

        return $this->render('changePassword.twig', [
            'model' => $model,
            'isEmptyPassword' => $isEmptyPassword,
            'title' => Yii::t('web','Đổi Mật Khẩu'),
        ]);
    }

    public function actionUpdatePassword()
    {
        \Yii::$app->response->format = 'json';
        $isEmptyPassword = 0;
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        $session = Yii::$app->session;
        if (!$session->get("msisdn", false)) {
            return [
                'responseCode' => ResponseCode::UNAUTHORIZED,
                'message' => [Yii::t('web','Bạn chưa đăng nhập tài khoản')],
                'redirectUrl' => Url::toRoute(['auth/login'], true),
                'data' => []
            ];
        }
        // Kiem tra xem co mat khau chua?
        $userId = $session->get("userId", "");

        if ($userId) {
            $objUser = VtUserBase::getById($userId);

            if (!$objUser->password) {
                $session->set("isEmptyPassword", 1);
                $isEmptyPassword = 1;
            }
        }

        if (Yii::$app->request->post()) {
            $model = new ChangePasswordForm();
            $model->username = $session->get("msisdn");
            $model->password = Yii::$app->request->post('password');
            $model->newPassword = Yii::$app->request->post('newPassword');
            $model->repeatPassword = Yii::$app->request->post('repeatPassword');
            $model->verifyCode = Yii::$app->request->post('verifyCode');
            if ($model->validate()) {
                $user = $model->getUser();
                // Neu khong ton tai user
                if ($model->validatePassword() || $isEmptyPassword == 1) {
                    $session->set("isEmptyPassword", 0);

                    $user->setPassword($model->newPassword);
                    $model = new ChangePasswordForm();
                    $session->set("needChangePassword", 0);
                    return [
                        'responseCode' => ResponseCode::SUCCESS,
                        'message' => [Yii::t('web','Thay đổi mật khẩu thành công')],
                    ];
                } else {
                    return [
                        'responseCode' => ResponseCode::INVALID_OLD_PASSWORD,
                        'message' => [Yii::t('web','Mật khẩu cũ không đúng')],
                    ];
                }
            } else {
                $errors = array();
                if ($model->errors['password']) {
                    $errors[] = $model->errors['password'][0];
                }
                if ($model->errors['newPassword']) {
                    $errors[] = $model->errors['newPassword'][0];
                }
                if ($model->errors['repeatPassword']) {
                    $errors[] = $model->errors['repeatPassword'][0];
                }
                if ($model->errors['verifyCode']) {
                    $errors[] = $model->errors['verifyCode'][0];
                }
                return [
                    'responseCode' => ResponseCode::UNSUCCESS,
                    'message' => $errors,
                ];
            }
        } else {
            return [
                'responseCode' => ResponseCode::UNSUCCESS,
                'message' => Yii::t('web','Đổi mật khẩu thất bại'),
            ];
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        session_regenerate_id(false);
        $session->destroy();
        return $this->goHome();
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
                'class' => 'common\helpers\CaptchaGenerate',
                'testLimit' => '1',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * @author PHUMX
     * callback sso
     * @param $client
     */
    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();

        $id = null;
        $fullName = '';
        $email = '';
        $imageUrl = '';
        $loginVia ='';

        if ($client instanceof \yii\authclient\clients\Google) {
            Yii::$app->getSession()->set("loginVia", "GOOGLE");
            $loginVia ='GOOGLE';
            $id = $attributes['id'];
            $fullName = $attributes['displayName'];
            $email = $attributes['emails'][0]['value'];
            $imageUrl = str_replace('sz=50', 'sz=200', $attributes['image']['url']);
        } elseif ($client instanceof \yii\authclient\clients\Facebook) {
            Yii::$app->getSession()->set("loginVia", "FACEBOOK");
            $loginVia ='FACEBOOK';
            $id = $attributes['id'];
            $fullName = $attributes['name'];
            $email = $attributes['email'];
            $imageUrl = $attributes['picture']['data']['url'];
        }



        if (isset($id) && $id) {
            $objUser = VtUserBase::getByOauthId($id);

            $client = new Client([
                'transport' => 'yii\httpclient\CurlTransport'
            ]);

            if (!$objUser) {

                $bucketName = '';
                $newPath = '';
                try {
                    $response = $client->createRequest()
                        ->setMethod('get')
                        ->setUrl($imageUrl)
                        ->setOptions([
                            CURLOPT_CONNECTTIMEOUT => 5, // connection timeout
                            CURLOPT_TIMEOUT => 30, // data receiving timeout
                        ])
                        ->send();
                    if ($response->isOk) {

                        $ext = 'jpg';
                        $tmpFile = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                        $fp = fopen($tmpFile, 'w');
                        fwrite($fp, $response->content);
                        fclose($fp);

                        $bucketName = Yii::$app->params['s3']['static.bucket'];
                        $newPath = Utils::generatePath($ext);

                        S3Service::putObject($bucketName, $tmpFile, $newPath);
                        VtHelper::generateAllThumb($bucketName, $tmpFile, $newPath);
                        unlink($tmpFile);
                    }
                } catch (\Exception $ex) {
                    \Yii::error('{OAUTH} Sync avatar image error: ' . $ex->getMessage(), 'error');
                }

                $objUser = new VtUserBase();
                $objUser->insertUserSocial($attributes['id'], $fullName, $email, $bucketName, $newPath, $loginVia );
                Yii::$app->getSession()->set("userId", $objUser->id);
                Yii::$app->getSession()->set("fullName", $objUser->full_name);
                Yii::$app->getSession()->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');

                return $this->redirect('/account/get-otp');
            } else {

                Yii::$app->getSession()->set("userId", $objUser->id);
                Yii::$app->getSession()->set("msisdn", $objUser->msisdn);
                Yii::$app->getSession()->set("fullName", $objUser->full_name);
                Yii::$app->getSession()->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');

                if(Yii::$app->session->get("NEED_LINK_ACCOUNT")){
                    Yii::$app->session->set("NEED_LINK_ACCOUNT",0);
                    return $this->redirect('/account/get-otp');
                }

                if ($objUser['is_show_suggest'] <= 0) {
                    $objUser->is_show_suggest = 1;
                    $objUser->save(false);
                    return $this->redirect('/');
                };
            }
        }
    }

    public function actionPushOtp($msisdn = '')
    {
        \Yii::$app->response->format = 'json';
        return parent::actionPushOtp($msisdn);
    }

    /**
     * change theme dark - light mode
     * @author Manhdt
     */
    public function actionChangeTheme($theme = '')
    {
        \Yii::$app->response->format = 'json';
        return parent::actionThemeMsisdn($theme);
    }
}
