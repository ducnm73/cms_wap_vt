<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 9/29/2016
 * Time: 10:24 AM
 */

namespace website\controllers;

use common\helpers\MobiTVRedisCache;
use common\helpers\Utils;
use common\libs\S3Service;
use common\models\MongoDBModel;
use common\models\VtContractBase;
use common\models\VtUserBase;
use common\models\VtUserOtpBase;
use common\modules\v2\libs\ResponseCode;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;
use yii\db\Exception;
use yii\web\Request;
use \common\modules\v1\controllers\AuthController;


class ContractController extends \common\modules\v2\controllers\ContractController
{

    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors()
    {
        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }

    public function actionFinal()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }

        $ok = Yii::$app->request->get("ok", 0);
        $this->layout = 'mainContract.twig';
        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);

        return $this->render("final.twig", ["ok" => $ok, "contract" => $contract]);
    }

    public function actionCondition()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }

        $ok = Yii::$app->request->get("ok", 0);
        $this->layout = 'mainContract.twig';
        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);

        $response = parent::actionCondition();

        if($response['responseCode']==ResponseCode::SUCCESS){
            $contract = $response['data']['contract'];
        }

        if($contract["status"]==1){
//            return $this->redirect("/contract/account");
            return $this->redirect("/contract/contract");
        }


        return $this->render("condition.twig", ["ok" => $ok, "contract" => $contract, "condition"=> $response["data"]["condition"] ]);
    }

    public function actionAccount()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }

        $this->layout = 'mainContract.twig';
        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);

        if (!$contract) {
            return $this->redirect("/contract/condition");
        }

        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }
            //Neu khong phai draff thi khong duoc sua
            if ($contract->status != 0) {
                return $this->redirect("/");
            }
            $response = parent::actionAccount();
            if ($response['responseCode'] == ResponseCode::UNSUCCESS) {
                Yii::$app->session->setFlash("info", $response['message']);
                if( in_array(Yii::$app->request->post('payment_type'), ["BANK_ACCOUNT", "CASH"]) ){
                    $contract->payment_type = Yii::$app->request->post('payment_type');
                }

                return $this->render("account.twig", ['contract' => $contract]);
            }

            return $this->redirect("/contract/contract");
        }
        return $this->render("account.twig", ['contract' => $contract]);
    }

    public function actionContract()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }
        $this->layout = 'mainContract.twig';
        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);

        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }

            $btnContract = Yii::$app->request->post("action-contract");
            $response = parent::actionContract();
            if ($btnContract == 'modify-contract') {

                if($response['responseCode']==ResponseCode::SUCCESS){
                    return $this->redirect("/contract/personal-infomation");
                }else{
                    Yii::$app->session->setFlash("info", $response['message']);
                }


            } else if ($btnContract == 'change-failure-contract') {
                if($response['responseCode']==ResponseCode::SUCCESS){
                    return $this->redirect("/contract/personal-infomation");
                }else{
                    Yii::$app->session->setFlash("info", $response['message']);
                }

            } else {
                if($response['responseCode']==ResponseCode::SUCCESS){
                    return $this->redirect("/contract/final");
                }else{
                    Yii::$app->session->setFlash("info", $response['message']);
                }


            }
        }
        if ($contract->status > 0) {
            return $this->redirect("/contract/final");
        }
        return $this->render("contract.twig", ["contract" => $contract]);
    }

    public function actionPersonalInfomation()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }
        $isNew = false;
        $this->layout = 'mainContract.twig';
        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);

        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }

            $postData = Yii::$app->request->post();

            if (!$contract) {
                $contract = $postData;
                $isNew = true;
            }
            //Neu khong phai draff thi khong duoc sua
            if ($contract->status == 2) {
                return $this->redirect("/");
            }

            $response = parent::actionPersonalInfomation();

            if ($response['responseCode'] == ResponseCode::UNSUCCESS) {
                Yii::$app->session->setFlash("info", $response['message']);
                return $this->render("personalInfomation.twig", ['contract' => $contract]);
            }

            if(Yii::$app->request->post("submit-info")=="DONE" && $isNew){
               return $this->redirect("/account/upload");
            }

            /// return $this->redirect("/contract/account");
            return $this->redirect("/contract/contract");
        }
        return $this->render("personalInfomation.twig", ['contract' => $contract, 'imageUrl' => 'http://static3.myclip.vn']);
    }

    public function actionGetOtp()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        \Yii::info('Qua token', 'api');
        if (!$this->userId) {
            return [
                'responseCode' => 401,
                'message' => Yii::t('wap','Yêu cầu đăng nhập')
            ];
        }
        $contract = VtContractBase::getByUserId($this->userId);
        return AuthController::actionPushOtp($contract->msisdn);
    }

    public function actionViewContract()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }

        $user = VtUserBase::getById($this->userId);

        $contract = VtContractBase::getByUserId(['user_id' => $this->userId]);
        $filePath = Yii::$app->basePath . '/web/temp/';
        $fileName = "Hop-dong-" . uniqid() . '.docx';
        $templateProcessor = new TemplateProcessor($filePath . "ContractForm.docx");
        $templateProcessor->setValue('contract_code', $contract->contract_code); //${name}
        $templateProcessor->setValue('name', $contract->name); //${name}
        $templateProcessor->setValue('address', $contract->address); //${name}
        $templateProcessor->setValue('id_card_number', $contract->id_card_number); //${name}
        $templateProcessor->setValue('tax_code', $contract->tax_code); //${name}
        $templateProcessor->setValue('msisdn', $contract->msisdn); //${name}

        //$arrPaymentType=["CASH"=>"Chuyển tiền qua VTPAY", "MOBILE_ACCOUNT"=>"Tài khoản di động"];
        //Thay tai khoan di dong thanh tai khoan ngan hang
        $arrPaymentType = ["CASH" => Yii::t('wap','Chuyển tiền qua VTPAY'), "MOBILE_ACCOUNT" => Yii::t('wap','Tài khoản di động'), "BANK_ACCOUNT" => Yii::t('wap','Tài khoản ngân hàng')];

        $templateProcessor->setValue('payment_type', $arrPaymentType[$contract->payment_type]); //${name}

        $templateProcessor->setValue('account_number', $contract->account_number); //${name}
        $templateProcessor->setValue('bank_name', $contract->bank_name); //${name}
        $templateProcessor->setValue('bank_department', $contract->bank_department); //${name}
        $templateProcessor->setValue('channel_name', $user->full_name); //${name}
        $templateProcessor->setValue('email', $contract->email); //${name}

        $templateProcessor->saveAs($filePath . $fileName);

        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/octetstream");
        header("Content-Disposition: attachment; filename=" . basename($filePath . $fileName));
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($filePath . $fileName));
        @readfile($filePath . $fileName);
        @unlink($filePath . $fileName);
    }

    public function actionGetAccountOtp()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        if (!$this->userId) {
            return [
                'responseCode' => 401,
                'message' => Yii::t('wap','Yêu cầu đăng nhập')
            ];
        }
        $msisdn = Yii::$app->request->post('msisdn');
        if (!$msisdn) {
            return [
                'responseCode' => 401,
                'message' => Yii::t('wap','Yêu cầu nhập số thuê bao')
            ];
        }

        $limitOtpByUser = Yii::$app->cache->get("acc_otp_limit_" . $this->userId);

        if (!$limitOtpByUser) {
            Yii::$app->cache->set("acc_otp_limit_" . $this->userId, 1, MobiTVRedisCache::CACHE_1DAY);
        } else {
            Yii::$app->cache->set("acc_otp_limit_" . $this->userId, $limitOtpByUser + 1, MobiTVRedisCache::CACHE_1DAY);
        }

        if ($limitOtpByUser >= 3) {
            return [
                'responseCode' => 401,
                'message' => Yii::t('wap','Quá hạn mức nhận OTP trong ngày')
            ];
        }

        $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
        $authController = new \common\modules\v1\controllers\AuthController();
        return $authController->actionPushOtp($msisdn);

    }

    public function actionLoadInfomation()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::actionLoadInfomation();
    }
}