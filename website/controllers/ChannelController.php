<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 9/29/2016
 * Time: 10:24 AM
 */

namespace website\controllers;

use common\helpers\Utils;
use common\models\VtUserBase;
use common\modules\v2\libs\RecommendObj;
use common\modules\v2\libs\TrackingCode;
use Yii;
use yii\web\HttpException;
use common\modules\v2\libs\ResponseCode;

class ChannelController extends \common\modules\v2\controllers\ChannelController {

    public function init() {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors() {

        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }
    public function actionGetUserDetail() {

        $this->layout = "mainList.twig";
        $id = trim(Yii::$app->request->get('id', 0));

        if(!$id) {
            return $this->redirect('/');
        }
        Yii::$app->meta->addDoctrineMetas("CHANNEL_DETAIL");
        $responseData = parent::actionGetUserDetail();
//        echo '<pre>';
//        var_dump($responseData['data']);die;
        $trackingContent = RecommendObj::serialize(TrackingCode::view_channel_home, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['id'],
            'channel_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_followers' => $responseData['data']['detail']['num_follow'],
            'no_videos' => $responseData['data']['detail']['num_video'],
            'created_at' => $responseData['data']['detail']['created_at'],
            'no_views' => $responseData['data']['detail']['totalViews']
        ]);

        //Lay kenh lien quan
        $channelRelated = parent::actionGetChannelRelated($id);

        return $this->render('@website/views/channel/userdetail.twig', [
            'responseData' => $responseData,
            'channelId'=>$id,
            'trackingContent' => $trackingContent,
            'channelRelated'=>$channelRelated,
            'isOwner'=>($this->userId==$id)?true:false
        ]);
    }

    public function actionGetDetail() {
        //REGISTER METATAG
        $this->layout = "mainList.twig";
        $id = trim(Yii::$app->request->get('id', 0));
        if(!$id) {
            return $this->redirect('/');
        }
        Yii::$app->meta->addDoctrineMetas("CHANNEL_DETAIL");
        $responseData = parent::actionGetDetail();

        $trackingContent = RecommendObj::serialize(TrackingCode::view_channel_home, $this->getUserTracking(), [
            'show_recommendation' => false,
            'channel_id' => $responseData['data']['detail']['id'],
            'channel_name' => $responseData['data']['detail']['name'],
            'utm_source' => null,
            'utm_medium' => null,
            'no_followers' => $responseData['data']['detail']['num_follow'],
            'no_videos' => $responseData['data']['detail']['num_video'],
            'created_at' => $responseData['data']['detail']['created_at'],
            'no_views' => $responseData['data']['detail']['totalViews']
        ]);

        //Lay kenh lien quan
        $channelRelated = parent::actionGetChannelRelated($id);
        return $this->render('@website/views/channel/detail.twig', [
            'responseData' => $responseData,
            'channelId'=>$id,
            'trackingContent' => $trackingContent,
            'channelRelated'=>$channelRelated,
            'isOwner'=>($this->userId==$responseData['data']['detail']['user_id'])?true:false
        ]);
    }

    public function actionCancelCurrentChangePhoneRequest() {
        $responseData = parent::actionCancelCurrentChangePhoneRequest();

        if($responseData['responseCode'] == ResponseCode::SUCCESS) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Hủy yêu cầu thay đổi số điện thoại thành công'));
            return $this->redirect(\yii\helpers\Url::to(['channel/create-change-phone-request']));
        } elseif($responseData['responseCode'] == ResponseCode::NOT_FOUND) {
            Yii::$app->session->setFlash('success', $responseData['message']);
        }

        return $this->goHome();
    }

    public function actionCreateChangePhoneRequest() {
        $responseData = parent::actionCreateChangePhoneRequest();

        if ($responseData['responseCode'] == ResponseCode::UNAUTHORIZED) {
            $uri = Yii::$app->request->getAbsoluteUrl();
            Yii::$app->session->set("backUrl", $uri);
            Yii::$app->session->set("isRedirect", true);
            return $this->redirect('/auth/login');
        }

        $this->layout = "main.twig";
        $model = new UpdatePhoneNumberForm();
        Yii::$app->request->setQueryParams(['id' => $this->userId]);
        $channelData = parent::actionGetInfoChannel();

        if($responseData['responseCode'] == ResponseCode::SUCCESS) {
            if(Yii::$app->request->isGet) {
                $model->setAttributes($responseData['model']);
                $model->msisdn = $responseData['model']['msisdn'];
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Gửi yêu cầu thay đổi số điện thoại thành công!'));
                return $this->goHome();
            }
        } elseif($responseData['responseCode'] == ResponseCode::ALREADY_EXISTS) {
            $model->setAttributes($responseData['model']);
            $model->msisdn = $responseData['model']['msisdn'];

            return $this->render('@website/views/channel/viewCurrentChangePhoneRequest.twig', [
                'model' => $model,
                'responseData' => $responseData,
                'title' => Yii::t('app', 'Yêu cầu đổi số thuê bao'),
            ]);
        } elseif(Yii::$app->request->isPost) {
            $model->setAttributes($responseData['model']);
            $model->msisdn = $responseData['model']['msisdn'];
            $model->addErrors($responseData['errors']);
        }

        return $this->render('@website/views/channel/updatePhoneNumber.twig', [
            'model' => $model,
            'responseData' => $channelData,
            'title' => Yii::t('app', 'Yêu cầu đổi số thuê bao'),
        ]);
    }
    public function actionAdd()
    {
        if (!$this->isValidUser()) {
            $uri = Yii::$app->request->getAbsoluteUrl();
            Yii::$app->session->set("backUrl", $uri);
            Yii::$app->session->set("isRedirect", true);
            return $this->redirect('/auth/login');
        }

        if (Yii::$app->request->isPost) {

            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }

            $response = parent::actionInsert();

            Yii::$app->session->setFlash('info', $response['message']);
            if ($response['responseCode'] == ResponseCode::SUCCESS) {
                return $this->redirect("/channel/update/" . $response['data']['channel']['id']);
            } else {
                return $this->redirect("/channel/add");
            }
        }

        return $this->render('@website/views/channel/update.twig', [
            //'responseData' => $responseData,
            'title' => Yii::t('web','Thêm kênh'),
            'type'  => "add"
        ]);
    }
    public function actionUpdate() {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_SHOW");
        if (!$this->isValidUser()) {

            $uri = Yii::$app->request->getAbsoluteUrl();
            Yii::$app->session->set("backUrl", $uri);
            Yii::$app->session->set("isRedirect", true);
            return $this->redirect('/auth/login');
        }
        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }
            $response = parent::actionUpdate();
            Yii::$app->session->setFlash('info', $response['message']);
            return $this->redirect("/channel/update/" . $response['data']['channel']['channel_id']);
        } else {
            $responseData = parent::actionGetInfoChannel();

            if($responseData['responseCode'] == ResponseCode::NOT_FOUND) {
                return $this->goHome();
            }
        }

        return $this->render('@website/views/channel/update.twig', [
            'responseData' => $responseData,
            'title' => Yii::t('web','Chỉnh sửa kênh')
        ]);
    }

    public function actionGetChannelRecommend()
    {
        $responseData =  parent::actionGetChannelRecommend();
        return $this->render("/channel/channelList.twig", ['responseData'=>$responseData]);
    }


    public function actionRemoveChannelRelated()
    {
        $this->layout = false;
        Yii::$app->response->format = "json";
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        return parent::actionRemoveChannelRelated(); // TODO: Change the autogenerated stub
    }

    public function actionAddChannelRelated()
    {
        $this->layout = false;
        Yii::$app->response->format = "json";
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        return parent::actionAddChannelRelated(); // TODO: Change the autogenerated stub
    }

    public function actionSearchChannel()
    {
        if (!$this->userId) {
            return $this->redirect('/auth/login');
        }
        //$relatedByCategory = parent::actionGetChannelByCategory($this->userId);
        $relatedByCategory = parent::actionGetChannelRelated($this->userId);
        return $this->render("/channel/searchChannel.twig",["userId"=>$this->userId, "channelRelated"=>$relatedByCategory,"isOwner"=>true]);
    }

    public function actionUserUpdate() {
        $this->layout = "main.twig";
        Yii::$app->meta->addDoctrineMetas("CHANNEL_SHOW");
        if (!$this->isValidUser()) {
            $uri = Yii::$app->request->getAbsoluteUrl();
            Yii::$app->session->set("backUrl", $uri);
            Yii::$app->session->set("isRedirect", true);
            return $this->redirect('/auth/login');
        }
        if (Yii::$app->request->isPost) {
            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }
            $response = parent::actionUserUpdate();
            $session = Yii::$app->session;
            if ($response["responseCode"] == 200) {
                $session->set("avatarImage", $response["data"]["channel"]["avatarImage"]);
                $session->set("coverImage", $response["data"]["channel"]["coverImageWeb"]);
                $session->set("fullName", $response["data"]["channel"]["name"]);
                $session->set("description", $response["data"]["channel"]["description"]);
            }
            Yii::$app->session->setFlash('info', $response['message']);

            return $this->redirect("/user/update/" . $this->userId);
        } else {
            // thong tin chi tiet user
            $responseData = parent::actionGetUserUpdate();
            if($responseData['responseCode'] == ResponseCode::UNSUCCESS) {
                return $this->goHome();
            }
        }

        return $this->render('@website/views/account/update.twig', [
            'responseData' => $responseData,
            'title' => Yii::t('web','Chỉnh sửa người dùng')
        ]);
    }
}
