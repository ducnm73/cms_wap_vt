<?php
namespace website\controllers;

use Yii;

/**
 * Site controller
 */
class KpiController extends \common\modules\v2\controllers\KpiController
{
    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors()
    {
        if (Yii::$app->params['cache.enabled']) {
            return [
            ];
        } else {
            return [];
        }
    }

    public function actionInit()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionInit();
    }

    public function actionTrace()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionTrace();
    }

}
