<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 9/29/2016
 * Time: 10:24 AM
 */

namespace website\controllers;

use api\models\VtAccountInfomation;
use common\helpers\MobiTVRedisCache;
use common\helpers\Utils;
use common\models\KpiUploadBase;
use common\models\LogMapAccountBase;
use common\models\VtAccountInfomationBase;
use common\models\VtChannelBase;
use common\models\VtUserBase;
use common\models\VtUserOtpBase;
use website\models\GetOTPForm;
use website\models\VtCp;
use website\models\VtPlaylistItem;
use Yii;
use common\modules\v2\libs\ResponseCode;
use common\helpers\FineUploaderTraditional;
use common\models\VtGroupCategoryBase;

//use wap\models\VtVideo;
use common\models\VtVideoBase;
use common\models\VtActionLogBase;
use cp\models\VtVideo;
use common\helpers\ConsoleRunner;
use common\libs\S3Service;
use website\models\VtUserPlaylist;
use website\models\VtUserPlaylistItem;
use yii\helpers\ArrayHelper;
use common\libs\VtHelper;

class AccountController extends \common\modules\v2\controllers\AccountController
{

    public function init()
    {
        if (Yii::$app->request->getIsAjax()) {
            $this->layout = false;
        }
        parent::initWap();
    }

    public function behaviors()
    {

        if (Yii::$app->params['cache.enabled']) {
            return [
                'access' => [
                    'class' => \yii\filters\AccessControl::class,
                    'only' => ['report-upload'],
                    'rules' => [
                        [
                            'allow' => false,
                        ],
                    ],
                ],
            ];
        } else {
            return [
                'access' => [
                    'class' => \yii\filters\AccessControl::class,
                    'only' => ['report-upload'],
                    'rules' => [
                        [
                            'allow' => false,
                        ],
                    ],
                ],
            ];
        }
    }

    public function actionFollowChannel()
    {
        \Yii::$app->response->format = 'json';
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        return parent::actionFollowChannel();
    }

    /**
     * Dang ky dich vu
     * @return array|string|\yii\web\Response
     */
    public function actionRegisterService()
    {


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        // neu khach hang khong co so dien thoai ma co tai khoan(G, F) thi chuyen sang trang map tai khoan
        if (!$this->isValidMsisdn() && $this->isValidUser()) {
            return $this->redirect("/account/get-otp");
        }

        $refer = Yii::$app->request->referrer;
        $responseData = parent::actionRegisterService();
        switch ($responseData['responseCode']) {
            case 200:
                //An popup trang promotion
                Yii::$app->session->set('isShowPopupPromotion', true);
                Yii::$app->getSession()->setFlash('info', $responseData['message'] . '|REGISTER_SUCCESS');
                return $this->redirect($refer);
            case 201:
                //An popup trang promotion
                Yii::$app->session->set('isShowPopupPromotion', true);
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->redirect($refer);
            case 440:
                //An popup trang promotion
                Yii::$app->session->set('isShowPopupPromotion', true);
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->redirect($refer);
            case 401:
                Yii::$app->session->remove();
                return $this->redirect("/auth/login");
        }

        return $responseData;
    }

    public function actionUnregisterService()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        // neu khach hang khong co so dien thoai ma co tai khoan(G, F) thi chuyen sang trang map tai khoan
        if (!$this->isValidMsisdn() && $this->isValidUser()) {
            return $this->redirect("/account/get-otp");
        }

        $refer = Yii::$app->request->referrer;
        $responseData = parent::actionUnregisterService();
        switch ($responseData['responseCode']) {
            case 200:
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->redirect($refer);
            case 201:
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->redirect($refer);
            case 401:
                return $this->redirect("/auth/login");
        }

        return $responseData;
    }

    public function actionBuy()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        // neu khach hang khong co so dien thoai ma co tai khoan(G, F) thi chuyen sang trang map tai khoan
        if (!$this->isValidMsisdn() && $this->isValidUser()) {
            return $this->redirect("/account/get-otp");
        }

        $refer = Yii::$app->request->referrer;

        $responseData = parent::actionBuy();

        $type = trim(\Yii::$app->request->post('type', ''));
        $itemId = trim(\Yii::$app->request->post('item_id', ''));

        switch ($responseData['responseCode']) {
            case 200:
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->redirect($refer);
            case 201:
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->redirect($refer);
            case 401:
                $session = Yii::$app->session;
                session_regenerate_id(false);
                $session->destroy();
                return $this->redirect("/auth/login");
        }

        return $responseData;
    }

    public function actionGetListMember()
    {
        $this->layout = 'mainList.twig';
        $responseData = parent::actionGetListMember();
        return $this->render('listMember.twig', ['responseData' => $responseData, 'userId' => $this->userId]);
    }

    public function actionGetUserProfile()
    {
        $userId = trim(Yii::$app->request->get('id', 0));
        $responseData = parent::actionGetUserProfile();

        switch ($responseData['responseCode']) {
            case ResponseCode::USER_UNREGISTERED:
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->goHome();
            case ResponseCode::USER_INACTIVE:
                Yii::$app->getSession()->setFlash('info', $responseData['message']);
                return $this->goHome();
            case 201:
                $this->redirect('/default/error', 404);
                break;
            case 401:
                return $this->redirect("/auth/login");
        }

        return $this->render('profile.twig', ['responseData' => $responseData, 'userId' => (!empty($userId)) ? $userId : $this->userId]);
    }

    public function actionViewProfile()
    {
        $userId = Yii::$app->request->get('id', 0);
        $responseData = parent::actionViewProfile();
        return $this->render('view-profile.twig', ['responseData' => $responseData, 'userId' => $userId]);
    }

    /**
     * @return string
     * @author PhuMX
     * Lay ma OTP
     */
    public function actionGetOtp()
    {
        $this->layout = 'mainLogin.twig';

        if (!$this->isValidUser()) {
            return $this->redirect('/auth/login');
        }

        $objUser = VtUserBase::getById($this->userId);
        if ($objUser && strlen($objUser->msisdn) > 0) {
            // Yii::$app->session->setFlash('info', "Tài khoản của quý khách đã có số thuê bao");
            // return $this->redirect('/');
        }

        $model = new GetOTPForm();
        if ($model->load(Yii::$app->request->post()))
            if ($model->validate()) {

                /*
                $objUser = VtUserBase::getByMsisdn(Utils::getMobileNumber($model->msisdn, Utils::MOBILE_GLOBAL));
                if ($objUser) {
                    Yii::$app->session->setFlash('info', "Số thuê bao đã tồn tại, vui lòng chọn số thuê bao khác.");
                    return $this->redirect('');
                }
                */
                // Tra ve OTP
                $auth = new AuthController();
                $responseData = $auth->actionPushOtp(Utils::getMobileNumber($model->msisdn, Utils::MOBILE_GLOBAL));
                if ($responseData['responseCode'] == 200) {
                    Yii::$app->getSession()->set("OTP_FAIL_COUNT", 0);
                    Yii::$app->getSession()->set("IS_HAS_OTP", 1);
                    $arrTmp = Yii::$app->request->post("GetOTPForm");
                    return $this->redirect("/account/map-account?msisdn=" . $arrTmp["msisdn"]);
                } else {
                    $model->addError("", $responseData["message"]);
                }
            }
        return $this->render('getOTP.twig', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     * @author PhuMX
     * MAp tai khoan viettel voi tai khoan google, facebook
     */
    public function actionMapAccount()
    {
        $this->layout = 'mainLogin.twig';
        if (!$this->isValidUser()) {
            return $this->redirect('/auth/login');
        }

        $objUser = VtUserBase::getById($this->userId);

        if ($objUser && strlen($objUser->msisdn) > 0 && $objUser->msisdn == Utils::getMobileNumber(Yii::$app->request->get("msisdn"), Utils::MOBILE_GLOBAL)) {
            Yii::$app->session->setFlash('info', Yii::t('web', 'Số thuê bao của quý khách đã liên kết với tài khoản này!'));
            return $this->redirect('/');
        }
        // NEU CHUA LAY OTP THI KHONG DC TRUY CAP MAP ACCOUNT

        $model = new \wapsite\models\MapAccountForm();
        if (Yii::$app->request->isGet) {
            $model->msisdn = Utils::getMobileNumber(Yii::$app->request->get("msisdn"), Utils::MOBILE_GLOBAL);
        }

        if ($model->load(Yii::$app->request->post()))
            if ($model->validate()) {
                $arrTmp = Yii::$app->request->post("MapAccountForm");
                // Kiem tra OTP
                if (VtUserOtpBase::checkOTP($arrTmp["msisdn"], $arrTmp["otp"])) {
                    $loginVia = Yii::$app->getSession()->get("loginVia", "");
                    $userIdTarget = 0;
                    //Kiem tra xem so thue bao muon Map da co chua
                    $objMsisdn = VtUserBase::getByMsisdn($arrTmp["msisdn"]);
                    if ($objMsisdn) {
                        LogMapAccountBase::insertLog($arrTmp["msisdn"], $this->userId, $arrTmp["msisdn"], $objMsisdn->id, "Ton tai msisdn " . $arrTmp["msisdn"] . ' voi id=' . $objMsisdn->id . "Thuc hien update oauth_id=" . $objUser->oauth_id . " sang tai khoan cu " . $this->userId . ", va xoa oauth_id");

                        //Neu chua co full name thi map
                        if (!$objMsisdn->full_name) {
                            $objMsisdn->full_name = $objUser->full_name;
                            $objMsisdn->full_name_slug = Utils::removeSignOnly($objUser->full_name);
                        }

                        if (!$objMsisdn->email) {
                            $objMsisdn->email = $objUser->email;
                        }

                        if ($loginVia == 'GOOGLE') {
                            $objMsisdn->google_oauth_id = $objUser->google_oauth_id;
                        } else {
                            $objMsisdn->oauth_id = $objUser->oauth_id;
                        }

                        $objMsisdn->save(false);

                        // Xoa so thue bao dang ton tai khoi vt_user va ha user cu xuong
                        if ($loginVia == 'GOOGLE') {
                            //Neu googleid = facebookid thi xoa ca 2
                            if ($objUser->google_oauth_id == $objUser->oauth_id) {
                                $objUser->oauth_id = '';
                            }
                            $objUser->google_oauth_id = '';
                        } else {
                            //Neu googleid = facebookid thi xoa ca 2
                            if ($objUser->google_oauth_id == $objUser->oauth_id) {
                                $objUser->google_oauth_id = '';
                            }
                            $objUser->oauth_id = '';
                        }

                        $objUser->save(false);

                        // Set session va xoa loi OTP
                        Yii::$app->getSession()->set("msisdn", $arrTmp["msisdn"]);
                        Yii::$app->getSession()->set("userId", $objMsisdn->id);
                        Yii::$app->getSession()->set("fullName", $objMsisdn->full_name);

                    } else {
                        LogMapAccountBase::insertLog($arrTmp["msisdn"], 0, $objUser->msisdn, $objUser->id, "So thue bao " . $arrTmp["msisdn"] . " chua ton tai trong tai khoan nao, thuc hien map moi.");

                        $objUser->msisdn = $arrTmp["msisdn"];
                        $objUser->save(false);
                        // Set session va xoa loi OTP
                        Yii::$app->getSession()->set("msisdn", $arrTmp["msisdn"]);
                        Yii::$app->getSession()->set("userId", $objUser->id);
                        Yii::$app->getSession()->set("fullName", $objUser->full_name);
                    }

                    Yii::$app->getSession()->setFlash('info', Yii::t('web', 'Liên kết tài khoản thành công'));
                    Yii::$app->getSession()->set("OTP_FAIL_COUNT", 0);
                    Yii::$app->getSession()->set("IS_HAS_OTP", 0);

                    return $this->goHome();
                } else {
                    $count = intval(Yii::$app->getCache()->get("OTP_FAIL_COUNT_" . $arrTmp['msisdn']));
                    $count++;
                    Yii::$app->getCache()->set("OTP_FAIL_COUNT_" . $arrTmp['msisdn'], $count, MobiTVRedisCache::CACHE_15MINUTE);

                    // HUY OTP khi khach hang nhap sai qua 3 lan
                    if ($count >= Yii::$app->params['otp']['failToDestroy']) {
                        VtUserOtpBase::deactiveAllOtp($arrTmp['msisdn']);
                        Yii::$app->getSession()->setFlash("info", Yii::t('web', 'Quý khách vui lòng lấy lại mã OTP'));
                        Yii::$app->getSession()->set("IS_HAS_OTP", 0);
                        return $this->redirect("/account/get-otp");
                    }
                }
                $model->addError("otp", Yii::t('web', 'Mã OTP không đúng'));
            }
        return $this->render('mapAccount.twig', [
            'model' => $model,
        ]);
    }

    /*
     * @author PhuMX
     * Upload video
     */

    public function actionUpload()
    {
        $this->layout = 'mainDetail.twig';

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        if (!$this->isValidUser()) {
            $this->redirect('/auth/login');
        }

        $accountInfo = VtAccountInfomationBase::getByUserId($this->userId);

        if (!$accountInfo) {
            $objVideo = VtVideoBase::findOne(["created_by" => $this->userId]);
            //Neu chua co thong tin ma da co video thi bat buoc nhap thong tin khach hang
            //if ($objVideo) {
            return $this->redirect("/contract/condition");
            //}
        } else if ($accountInfo->status == 1) {
            Yii::$app->session->setFlash("info", Yii::t('web', 'Thông tin tài khoản đang chờ duyệt.'));
            return $this->redirect("/");
        } else if ($accountInfo->status == 3) {
            if ($accountInfo->reason) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Thông tin cá nhân bị từ chối với lý do:') . $accountInfo->reason . Yii::t('web', '. Vui lòng nhập lại thông tin.'));
            } else {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Thông tin cá nhân bị từ chối. Vui lòng nhập lại thông tin.'));
            }
            return $this->redirect("/contract/personal-infomation");
        } else if ($accountInfo->status == 4) {
            if ($accountInfo->reason) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Tính năng bị tạm khóa với lý do:') . $accountInfo->reason);
            } else {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Tính năng bị tạm khóa'));
            }
            return $this->redirect("/");
        }

        $channels_all = VtChannelBase::getChannelAllById($this->userId, $status = 1);
        if (count($channels_all) == 0) {
            Yii::$app->session->setFlash("info", Yii::t('web', 'Vui lòng tạo kênh trước khi upload'));
            return $this->redirect("/channel/add");
        }

        /*
        $objVideoUser = VtVideoBase::findOne(["created_by" => $this->userId]);
        if (!$objVideoUser && !Yii::$app->session->get("UPLOAD_FIRST") || !$accountInfo->is_confirm) {
            return $this->redirect("/account/condition");
        }
        */
        $channels = VtChannelBase::getChannelListById($this->userId);
        if (Yii::$app->request->isPost) {
            $response = parent::actionUploadFile();
            Yii::$app->session->setFlash('info', $response['message']);
            if ($response['responseCode'] == ResponseCode::SUCCESS) {
                return $this->redirect("/");
            }
        }

        $category = VtGroupCategoryBase::getMenuByType('VOD');
        return $this->render('upload.twig', [
            'categories' => $category,
            'channels' => $channels
        ]);
    }

    public function actionKpiUploadStart()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionKpiUploadStart();
    }

    public function actionKpiUploadEnd()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionKpiUploadEnd();
    }


    public function actionCondition()
    {
        if (!$this->isValidUser()) {
            return $this->redirect("/auth/login");
        }

        $this->layout = 'mainDetail.twig';

        $accountInfo = VtAccountInfomationBase::getByUserId($this->userId);

        if ($accountInfo && $accountInfo->is_confirm) {
            Yii::$app->session->set("UPLOAD_FIRST", "OK");
            return $this->redirect("/account/upload");
        }

        if (Yii::$app->request->isPost) {
            Yii::$app->session->set("UPLOAD_FIRST", "OK");

            $accountInfo->is_confirm = 1;
            $accountInfo->save(false);

            return $this->redirect("/account/upload");
        }

        return $this->render('condition.twig');
    }


    /*
     * @author PhuMX
     * Upload anh cho video
     */

    public function actionUploadImage()
    {
        if (!$this->isValidUser()) {
            return $this->redirect("/auth/login");
        }
        $videoId = Yii::$app->request->get('id');
        if (!VtVideoBase::checkCustomerPermissionEditVideo($this->userId, $videoId)) {
            Yii::$app->session->setFlash("info", Yii::t('web', 'Không có quyền truy cập'));
            return $this->redirect("/");
        };
        return $this->render('uploadImage.twig', [
            'videoId' => $videoId
        ]);
    }

    public function actionPushFileOld()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        if (!$this->isValidUser()) {
            echo json_encode([
                "success" => false,
                "error" => Yii::t('web', 'Qúy khách vui lòng đăng nhập'),
            ]);
            return false;
        }

        $uploader = new FineUploaderTraditional();
        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('flv', 'mp4', 'mov', 'm4u'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit = null;
        // Specify the input name set in the javascript.
        $uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = "chunks";

        $method = $_SERVER["REQUEST_METHOD"];

        if ($method == "POST") {
            header("Content-Type: text/plain");
            set_time_limit(1200);
            $ext = pathinfo($_FILES[$uploader->inputName]['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($ext), ['mp4', 'flv', '.mp4', '.flv', 'mov', '.mov', 'm4u', '.m4u'])) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Định dạng file upload không đúng'),
                ]);
                return false;
            }
            if ($_FILES[$uploader->inputName]['size'] > 943718400) { //900 MB (size is also in bytes)
                return [
                    'success' => ResponseCode::UNSUCCESS,
                    'error' => Yii::t('web', 'Kích thước file video tối đa 900MB')
                ];
            }

            $fileBucket = Yii::$app->params['s3']['video.bucket'];
            $filePath = Utils::generatePath($ext);

            $videoName = trim(Yii::$app->request->post("title"));
            $description = trim(Yii::$app->request->post("description"));
            $sessionId = trim(Yii::$app->request->post("session_id"));
            $mode = Yii::$app->request->post("mode");
            $tags = Yii::$app->request->post("tags");

            if (mb_strlen($videoName, 'UTF-8') > 255 || mb_strlen($videoName) <= 0) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Tên video phải nhập và số kí tự không lớn hơn 255 kí tự'),
                ]);
                return false;
            }

            //Change to backgroud upload
//            $arrS3 = S3Service::putObject($fileBucket, $_FILES[$uploader->inputName]['tmp_name'], $filePath);
//            Yii::info("S3 PUSH FILE=" . json_encode($arrS3) . "|" . $_FILES[$uploader->inputName]['tmp_name'] . "|" . $filePath);
//
//            if ($arrS3['errorCode'] == S3Service::UNSUCCESS) {
//                echo json_encode([
//                    "success" => false,
//                    "error" => "Upload file cloud không thành công. Vui lòng thử lại!",
//                ]);
//                exit();
//            }

            $type = $_POST['type'];
            $playlistId = $_POST['playlist_id'];

            $cpId = null;
            $cpCode = null;
            $outsourceNeedReview = VtVideo::OUTSOURCE_NEED_APPROVE;

            if (!empty($this->cpId)) {
                $cp = VtCp::getOneById($this->cpId);

                if (!empty($cp)) {
                    $cpId = $cp['id'];
                    $cpCode = $cp['cp_code'];

                    //Kiem tra xem CP_CODE co phai user upload khong
                    if (strstr($cpCode, "USER_UPLOAD")) {
                        $outsourceNeedReview = VtVideo::OUTSOURCE_NEED_APPROVE;
                    } else {
                        $outsourceNeedReview = VtVideo::OUTSOURCE_DONT_NEED_APPROVE;
                    }

                }
            }

            //- Kiem tra xem khach hang da co thong tin ca nhan hay chua?Neu chua co thi chuyen status =0
            $objInfo = VtAccountInfomationBase::getByUserId($this->userId);

            if ($objInfo) {
                $status = VtVideoBase::STATUS_DRAFT;
            } else {
                $status = VtVideoBase::STATUS_TEMP;
            }
            //Backgroud upload
            $storageType = Yii::$app->params['storage.type'];
            if ($storageType == 0) { // Ko co CDN
                $fileBucket = "";
                $san = Yii::$app->params['local.video'];
                $folder = $san . DIRECTORY_SEPARATOR . Utils::generateFolder();
                $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder;
                if (!mkdir($childFolder, 0777, true)) {
                    return ('Failed to create folders...');
                }
                $filePath = $folder . DIRECTORY_SEPARATOR . Utils::generateUniqueFileName($ext);
                $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $filePath;
            }

            $video = new VtVideo();
            $convertStatus = ($storageType == 0) ? VtVideoBase::CONVERT_STATUS_DRAFT : VtVideoBase::CONVERT_STATUS_TEMP;
            $video->insertVideo($videoName, $description, $fileBucket, $filePath, VtVideoBase::TYPE_VOD, $cpId,
                $this->userId, 0, $convertStatus, $bucket = '', $path = '',
                $mode, $tags, $cpCode, $outsourceNeedReview, $status);

            if ($storageType != 0) {
                $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $video->id . '.' . $ext;
            }

            move_uploaded_file($_FILES[$uploader->inputName]['tmp_name'], $orgpath);

            $kpi = KpiUploadBase::getBySessionId($sessionId);
            if ($kpi) {
                $now = strtotime('now');
                $kpi->video_id = $video->id;
                $kpi->upload_end_time = date('Y-m-d H:i:s', $now);
                $kpi->upload_duration = $now - strtotime($kpi->upload_start_time);
                $kpi->size = $_FILES[$uploader->inputName]['size'];
                $kpi->save(false);
            }
            if (!empty($fileBucket)) { // neu ton tại bucket
                $cr = new ConsoleRunner(['file' => '@yiiConsole']);
                $cr->run('upload/put-object --id=' . $video->id . ' --orgPath=' . $orgpath . ' --sessionId=' . $sessionId);
            }

            //@todo: dang hardcode bo ghi log
//            $actionLog = "(" . $this->userId . ")" . "[" . $this->fullName . "]" . " " . $videoName;
//            VtActionLogBase::insertLog(VtActionLogBase::M_FRONTEND_VIDEO, $video->id, VtActionLogBase::T_WAIT_APPROVED, $actionLog, $this->userId, "WEB");

            //Backgroud upload
            //$orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $video->id . '.' . $ext;
            //($_FILES[$uploader->inputName]['tmp_name'], $orgpath);
            //$cr = new ConsoleRunner(['file' => '@yiiConsole']);
            //$cr->run('upload/put-object --id=' . $video->id . ' --orgPath=' . $orgpath);

            if (!empty($playlistId)) {
                $playlistItem = new VtPlaylistItem();
                $playlistItem->insertItem($video->id, $playlistId);
            }

            echo json_encode([
                "success" => true,
                "video_id" => $video->id,
                "isHasAccountInfo" => ($objInfo) ? true : false
            ]);
            return false;
        } else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function actionPushFile()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        if (!$this->isValidUser()) {
            echo json_encode([
                "success" => false,
                "error" => Yii::t('web', 'Qúy khách vui lòng đăng nhập'),
            ]);
            return false;
        }

        $uploader = new FineUploaderTraditional();
        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('flv', 'mp4', 'mov', 'm4u'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit = null;
        // Specify the input name set in the javascript.
        $uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = "chunks";

        $method = $_SERVER["REQUEST_METHOD"];

        if ($method == "POST") {
            header("Content-Type: text/plain");
            set_time_limit(1200);
            $ext = pathinfo($_FILES[$uploader->inputName]['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($ext), ['mp4', 'flv', '.mp4', '.flv', 'mov', '.mov', 'm4u', '.m4u'])) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Định dạng file upload không đúng'),
                ]);
                return false;
            }
            if ($_FILES[$uploader->inputName]['size'] > 943718400) { //900 MB (size is also in bytes)
                return [
                    'success' => ResponseCode::UNSUCCESS,
                    'error' => Yii::t('web', 'Kích thước file video tối đa 900MB')
                ];
            }

            $fileBucket = Yii::$app->params['s3']['video.bucket'];
            $filePath = Utils::generatePath($ext);
            $videoName = trim(Yii::$app->request->post("title"));
            $description = trim(Yii::$app->request->post("description"));
            $sessionId = trim(Yii::$app->request->post("session_id"));
            $mode = Yii::$app->request->post("mode");
            $tags = Yii::$app->request->post("tags");
            $channel_id = trim(Yii::$app->request->post("channel_id"));

            if (mb_strlen($videoName, 'UTF-8') > 255 || mb_strlen($videoName) <= 0) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Tên video phải nhập và số kí tự không lớn hơn 255 kí tự'),
                ]);
                return false;
            }

            $type = $_POST['type'];
            $playlistId = $_POST['playlist_id'];

            $cpId = null;
            $cpCode = null;
            $outsourceNeedReview = VtVideoBase::OUTSOURCE_NEED_APPROVE;

            if (!empty($this->cpId)) {
                $cp = VtCp::getOneById($this->cpId);

                if (!empty($cp)) {
                    $cpId = $cp['id'];
                    $cpCode = $cp['cp_code'];

                    //Kiem tra xem CP_CODE co phai user upload khong
                    if (strstr($cpCode, "USER_UPLOAD")) {
                        $outsourceNeedReview = VtVideo::OUTSOURCE_NEED_APPROVE;
                    } else {
                        $outsourceNeedReview = VtVideo::OUTSOURCE_DONT_NEED_APPROVE;
                    }

                }
            }

            //- Kiem tra xem khach hang da co thong tin ca nhan hay chua?Neu chua co thi chuyen status =0
            $objInfo = VtAccountInfomationBase::getByUserId($this->userId);

            if ($objInfo) {
                $status = VtVideoBase::STATUS_DRAFT;
            } else {
                $status = VtVideoBase::STATUS_TEMP;
            }
            //Backgroud upload
            $storageType = Yii::$app->params['storage.type'];
            if ($storageType == 0) { // Ko co CDN
                $fileBucket = "";
                $san = Yii::$app->params['local.video'];
                $folder = $san . DIRECTORY_SEPARATOR . Utils::generateFolder();
                $fileName = Utils::generateUniqueFileName($ext);
                $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder;
                if (!mkdir($childFolder, 0777, true)) {
                    return ('Failed to create folders...');
                }
                $filePath = '' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $fileName;
                $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $fileName;
                move_uploaded_file($_FILES[$uploader->inputName]['tmp_name'], $orgpath) or die("Problems with upload");
            }

            $video = new VtVideoBase();
            $convertStatus = ($storageType == 0) ? VtVideoBase::CONVERT_STATUS_DRAFT : VtVideoBase::CONVERT_STATUS_TEMP;
            $video->insertVideo($videoName, $description, $fileBucket, $filePath, VtVideoBase::TYPE_VOD, $cpId,
                $this->userId, 0, $convertStatus, $bucket = '', $path = '',
                $mode, $tags, $cpCode, $outsourceNeedReview, $status, $channel_id);
            $orgpath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $video->id . '.' . $ext;
            // echo ($orgpath); die;
//            Yii::info('actionPushFile|move_uploaded_file|' . $_FILES[$uploader->inputName]['tmp_name'] . '|' . $orgpath);
//            move_uploaded_file($_FILES[$uploader->inputName]['tmp_name'], $orgpath);

            $kpi = KpiUploadBase::getBySessionId($sessionId);
            if ($kpi) {
                $now = strtotime('now');
                $kpi->video_id = $video->id;
                $kpi->upload_end_time = date('Y-m-d H:i:s', $now);
                $kpi->upload_duration = $now - strtotime($kpi->upload_start_time);
                $kpi->size = $_FILES[$uploader->inputName]['size'];
                $kpi->save(false);
            }
            //tam dong lai
//		    if(!empty( $fileBucket)){ // neu ton tại bucket
//				$cr = new ConsoleRunner(['file' => '@yiiConsole']);
//                $cr->run('upload/put-object --id=' . $video->id . ' --orgPath=' . $orgpath . ' --sessionId=' . $sessionId);
////                 var_dump($output); die;
//			}

            if (!empty($playlistId)) {
                $playlistItem = new VtPlaylistItem();
                $playlistItem->insertItem($video->id, $playlistId);
            }

            echo json_encode([
                "success" => true,
                "video_id" => $video->id,
                "isHasAccountInfo" => ($objInfo) ? true : false
            ]);
            return false;
        } else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    /**
     * upload anh tren WAP
     * @return string
     */
    public function actionPushImageFile()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        if (!$this->isValidUser()) {
            echo json_encode([
                "success" => false,
                "error" => Yii::t('web', 'Quý khách vui lòng đăng nhập'),
            ]);
            return false;
        }
        $uploader = new FineUploaderTraditional();

        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('gif', 'png', 'jpg', 'jpeg'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit = null;

        // Specify the input name set in the javascript.
        $uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = "chunks";

        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {
            header("Content-Type: text/plain");

            $ext = pathinfo($_FILES[$uploader->inputName]['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Định dạng file upload không đúng'),
                ]);
                return false;
            }
            if ($_FILES[$uploader->inputName]['size'] > 5242880) { //5 MB (size is also in bytes)
                return [
                    'success' => ResponseCode::UNSUCCESS,
                    'error' => Yii::t('web', 'Kích thước file ảnh tối đa 5MB')
                ];
            }

            $videoId = Yii::$app->request->post("video_id");
            if (!$videoId) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Bạn chưa đăng tải video')
                ]);
                return false;
            }
            if (!VtVideoBase::checkCustomerPermissionEditVideo($this->userId, $videoId)) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Không có quyền truy cập')
                ]);
                return false;
            };

            $fileBucket = "";
            $storageType = Yii::$app->params['storage.type'];
            if ($storageType == 0) { // Ko co CDN

                //lấy thư mục ảnh
                $san = Yii::$app->params['local.image'];
                //sinh ra tên thư mục con
                $folder = $san . DIRECTORY_SEPARATOR . Utils::generateFolder();
                //sinh ra tên file
                $fileName = Utils::generateUniqueFileName($ext);
                //tạo thư mục lưu ảnh từ đường dẫn server
                $childFolder = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder;
                if (!mkdir($childFolder, 0777, true)) {
                    return ('Failed to create folders...');
                }
                //đường dẫn lưu vào cơ sở dữ liệu
                $filePath = '' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $fileName;
                //đường dẫn cắt ảnh
                $fileAvatarLocalPath = Yii::getAlias('@uploadFolder') . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $fileName;
                //đường dẫn lưu ảnh đã cắt ( lưu đường dẫn cắt ảnh thì lấy cùng tên)
                $orgpath = $fileAvatarLocalPath;
                // hàm lưu ảnh vào server
                move_uploaded_file($_FILES[$uploader->inputName]['tmp_name'], $orgpath);
                // tạo ảnh cắt thumb
                VtHelper::generateAllThumb($fileBucket, $fileAvatarLocalPath, $orgpath, [], true);
            } else {
                $fileBucket = Yii::$app->params['s3']['static.bucket'];
                $filePath = Utils::generatePath($ext);
                $fileName = pathinfo($_FILES[$uploader->inputName]['name'], PATHINFO_FILENAME);
                S3Service::putObject($fileBucket, $_FILES[$uploader->inputName]['tmp_name'], $filePath);
                VtHelper::generateAllThumb($fileBucket, $_FILES[$uploader->inputName]['tmp_name'], $filePath);
            }
            $video = VtVideo::findOneById($videoId);
            $video->bucket = $fileBucket;
            $video->path = $filePath;
            $video->save(false);

            // Luu action log
            $actionLog = "(" . $this->userId . ")" . "[" . $this->fullName . "]" . $video->name . "|UPLOAD IMAGE";
            VtActionLogBase::insertLog(VtActionLogBase::M_FRONTEND_VIDEO, $video->id, VtActionLogBase::T_UPDATE, $actionLog, $this->userId, "WAP");

            echo json_encode([
                "success" => true,
                "file_path" => VtHelper::getThumbUrl($video->bucket, $video->path, VtHelper::SIZE_VIDEO, true),
            ]);
            return false;
        } else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function actionWatchTime()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionWatchTime(); // TODO: Change the autogenerated stub
    }

    /**
     * upload avatar tren WAP
     * @return string
     */
    public function actionUploadAvatar()
    {
        if (!$this->isValidUser()) {
            if ($this->isValidMsisdn()) {
                // Query lai xem co khach hang co dang ky MK ko?
                $objUser = VtUserBase::getByMsisdn($this->msisdn);
                // - Neu chua dang ky thanh vien thi thuc hien thong bao
                if (!$objUser) {
                    Yii::$app->session->setFlash('info', Yii::t('web', 'Để sử dụng tính năng này quý khách vui lòng soạn MK gửi 1515!'));
                    return $this->redirect("/");
                }
            } else {
                $this->redirect('/auth/login');
            }
        }

        return $this->render('upload-avatar.twig', [
        ]);
    }

    /**
     * upload anh tren WAP
     * @return string
     */
    public function actionPushImageAvatar()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return "CSRF TOKEN INVALID";
        }
        if (!$this->isValidUser()) {
            echo json_encode([
                "success" => false,
                "error" => (Yii::t('web', 'Qúy khách vui lòng đăng nhập')),
            ]);
            return false;
        }
        $uploader = new FineUploaderTraditional();

        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('gif', 'png', 'jpg', 'jpeg'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit = null;

        // Specify the input name set in the javascript.
        $uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = "chunks";

        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "POST") {
            header("Content-Type: text/plain");


            $ext = pathinfo($_FILES[$uploader->inputName]['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                echo json_encode([
                    "success" => false,
                    "error" => Yii::t('web', 'Định dạng file upload không đúng'),
                ]);
                return false;
            }

            $fileBucket = Yii::$app->params['s3']['static.bucket'];
            // if(empty()){
            // 	// Luu san


            // }else{
            $filePath = Utils::generatePath($ext);
            $fileName = pathinfo($_FILES[$uploader->inputName]['name'], PATHINFO_FILENAME);
            S3Service::putObject($fileBucket, $_FILES[$uploader->inputName]['tmp_name'], $filePath);
            VtHelper::generateAllThumb($fileBucket, $_FILES[$uploader->inputName]['tmp_name'], $filePath);
            // }


            $objUser = VtUserBase::getById($this->userId);
            $objUser->bucket = $fileBucket;
            $objUser->path = $filePath;
            $objUser->save(false);

            Yii::$app->getSession()->set('avatarImage', ($objUser['bucket'] && $objUser['path']) ? VtHelper::getThumbUrl($objUser['bucket'], $objUser['path'], VtHelper::SIZE_AVATAR) : '');

            echo json_encode([
                "success" => true,
                "error" => Yii::t('web', 'Đăng tải thành công'),
            ]);
            return false;
        } else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function actionAcceptLossData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        Yii::$app->getSession()->set("accept_loss_data", 1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionFollowList()
    {

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        if (!$this->isValidUser()) {
            $this->redirect('/auth/login');
        }
        if (Yii::$app->request->isPost) {
            $response = parent::actionGetFollowChannel();
            Yii::$app->session->setFlash('info', $response['message']);
            if ($response['responseCode'] == ResponseCode::SUCCESS) {
                return $this->redirect("/");
            }
        }

        return $this->render('followList.twig');
    }

    public function actionListPlaylist()
    {
        $this->layout = 'main.twig';
        Yii::$app->meta->addDoctrineMetas("LIST_PLAYLIST");
        $id = \common\modules\v2\libs\Obj::MY_PLAYLIST;
        $idPlaylist = \common\modules\v2\libs\Obj::VIDEO_OF_PLAYLIST;

        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit', Yii::$app->params['app.home.limit']));

        $responseCode = parent::actionGetMyPlaylists($limit, $offset);

        return $this->render('listPlaylist.twig', ['responseCode' => $responseCode, "id" => $id, "idPlaylist" => $idPlaylist]);
    }

    public function actionDetail()
    {
        return $this->render('detail.twig', ['userId' => $this->userId, 'userName' => $this->fullName, 'avatarImage' => $this->avatarImage, 'coverImage' => $this->coverImage]);
    }

    public function actionDeleteHistoryView()
    {

        \Yii::$app->response->format = 'json';

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        return parent::actionDeleteHistoryView();
    }

    public function actionMyPlaylist()
    {
        $responseData = parent::actionGetMyPlaylists();
        $playlists = isset($responseData['data']) ? $responseData['data'] : [];

        return $this->render('@website/views/account/playlistModal.twig', ['playlists' => $playlists]);
    }

    public function actionGetFollowContent()
    {
        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }
        $this->layout = 'main.twig';

        $responseData = parent::actionGetFollowContent();
        //REGISTER METATAG
        Yii::$app->view->title = Yii::t('wap', 'Mạng xã hội video - UClip');
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => Yii::t('wap', 'MXH UClip là Mạng xã hội video clip đầu tiên hàng đầu tại Lào. Được phát triển bởi Tổng Công ty Viễn thông Unitel- một trong những nhà mạng Viễn thông hàng đầu thế giới. Dịch vụ cũng là MXH đầu tiên tại Lào miễn phí Data 3G cho khách hàng sử dụng, mang lại những trải nghiệm tuyệt vời nhất về chất lượng dịch vụ và chất lượng nội dung cho người dụng')
        ]);
        $title = Yii::t('web', 'Chọn Nội Dung');
        return $this->render('@wapsite/views/account/followContent.twig', [
            'responseData' => $responseData,
            'title' => $title
        ]);
    }

    public function actionGetFollowChannel()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionGetFollowChannel(); // TODO: Change the autogenerated stub
    }

    public function actionMarkNotification()
    {
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        \Yii::$app->response->format = 'json';
        return parent::actionMarkNotification();
    }

    /**
     * API yeu thich/bo yeu thich Video
     * @return array
     */
    public function actionFollowVideo()
    {
        \Yii::$app->response->format = 'json';
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        if (!$this->isValidUser() && $this->isValidMsisdn()) {
            return [
                'responseCode' => ResponseCode::NOT_MEMBER,
                'message' => Yii::t('web', 'Để sử dụng tính năng này quý khách vui lòng soạn MK gửi 1515!')
            ];
        }
        return parent::actionToggleFollow();
    }

    public function actionDeleteAllHistoryView()
    {
        \Yii::$app->response->format = 'json';
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        if (!$this->isValidUser()) {
            return $this->authJson;
        }
        \common\models\VtHistoryViewBase::deleteAll($this->userId, $this->msisdn);
        return [
            'responseCode' => ResponseCode::SUCCESS,
            'message' => Yii::t('web', 'Thành công')
        ];
    }

    public function actionGetNotification()
    {

        $responseData = parent::actionGetNotification();
        $notifications = [];
        if ($responseData['responseCode'] == 200) {
            $notifications = $responseData['data']['notifications'];
        }

        return $this->render('@website/views/partials/notification.twig', [
            'notifications' => $notifications
        ]);
    }

    public function actionGetNumberNotification()
    {
        \Yii::$app->response->format = 'json';
        return parent::actionGetNumberNotification();
    }

    public function actionMyPlaylistWithVideo()
    {
        $videoId = Yii::$app->request->get('video_id', 0);

        $responseData = parent::actionGetMyPlaylists();
        $playlists = isset($responseData['data']) ? $responseData['data'] : [];
        $playlistIds = ArrayHelper::getColumn($playlists['content'], 'id');

        $addedPlaylists = VtUserPlaylistItem::getByVideoId($playlistIds, $videoId);
        $addedPlaylistIds = ArrayHelper::getColumn($addedPlaylists, 'playlist_id');

        return $this->render('@website/views/account/playlistModal.twig', [
            'playlists' => $playlists,
            'addedPlaylistIds' => $addedPlaylistIds,
            'userId' => $this->userId
        ]);
    }

    public function actionUpdateMedia()
    {
        \Yii::$app->response->format = 'json';
        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }
        $responseData = parent::actionUpdateMedia();
        return $responseData;
    }

    public function actionCheckLinkSocial()
    {
        if ($this->userId && !$this->msisdn) {
            return $this->redirect("/account/get-otp");
        } elseif (!$this->msisdn) {
            return $this->redirect("/");
        }
        Yii::$app->session->set("NEED_LINK_ACCOUNT", 1);

        $response = parent::actionCheckLinkSocial();
        return $this->render("linkSocial.twig", ["response" => $response]);
    }

    public function actionRemoveLinkSocial()
    {

        if (!Yii::$app->getRequest()->validateCsrfToken()) {
            return Yii::t('backend', "CSRF TOKEN INVALID");
        }

        \Yii::$app->response->format = 'json';
        $response = parent::actionRemoveLinkSocial();
        Yii::$app->session->setFlash("info", $response['message']);
        return $response;

    }

    public function actionAccountInfomation()
    {

        if (!$this->userId) {
            return $this->redirect("/auth/login");
        }

        $this->layout = 'mainContract.twig';
        $account = VtAccountInfomationBase::getByUserId(['user_id' => $this->userId]);
        if ($account) {
            return $this->redirect("/account/upload");
        }
        if (Yii::$app->request->isPost) {

            if (!Yii::$app->getRequest()->validateCsrfToken()) {
                return Yii::t('backend', "CSRF TOKEN INVALID");
            }
            $postData = Yii::$app->request->post();

            $isNew = false;
            if (!$account) {
                $account = $postData;
                $isNew = true;
            }
            //TODO validate
            if (!trim($postData['name'])) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Yêu cầu nhập họ và tên'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }

            if (!trim($postData['email'])) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Yêu cầu nhập email'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }

            if (!trim($postData['id_card_number'])) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Yêu cầu nhập Số chứng minh thư'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }
            $msisdn = trim($postData['msisdn']);
            $otp = trim($postData['otp']);
            if (!$msisdn) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Yêu cầu nhập số điện thoại unitel'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }
            if (($_FILES["id_card_image_frontside"]["size"] == 0)) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Yêu cầu nhập mặt trước ảnh chứng minh thư'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }

            if (($_FILES["id_card_image_backside"]["size"] == 0)) {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Yêu cầu nhập mặt sau ảnh chứng minh thư'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }

            $fileBucket = Yii::$app->params['s3']['static.bucket'];

            if ($_FILES["id_card_image_frontside"]['name']) {
                //-- Validate image card frontside
                $ext = pathinfo($_FILES["id_card_image_frontside"]['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {

                    Yii::$app->session->setFlash("info", Yii::t('web', 'Định dạng file ảnh mặt trước không đúng'));
                    return $this->render("accountInfomation.twig", ['account' => $account]);
                }

                if ($_FILES["id_card_image_frontside"]['size'] > 8388608) { //8 MB (size is also in bytes)
                    Yii::$app->session->setFlash("info", Yii::t('web', 'File ảnh mặt trước dung lượng tối đa 8MB'));
                    return $this->render("accountInfomation.twig", ['account' => $account]);
                }
                try {
                    $filePathFrontside = Utils::generatePath($ext);
                    S3Service::putObject($fileBucket, $_FILES["id_card_image_frontside"]['tmp_name'], $filePathFrontside);
                } catch (\Exception $exception) {
                    Yii::$app->session->setFlash("info", Yii::t('web', 'Có lỗi upload ảnh, vui lòng thử lại sau'));
                    return $this->render("accountInfomation.twig", ['account' => $account]);
                }

            }

            if ($_FILES["id_card_image_backside"]['name']) {
                //-- Validate image card backside
                $ext = pathinfo($_FILES["id_card_image_backside"]['name'], PATHINFO_EXTENSION);
                if (!in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg', '.gif', '.png', '.jpg', '.jpeg'])) {
                    Yii::$app->session->setFlash("info", Yii::t('web', 'Định dạng file ảnh mặt sau không đúng'));
                    return $this->render("accountInfomation.twig", ['account' => $account]);
                }

                if ($_FILES["id_card_image_backside"]['size'] > 8388608) { //8 MB (size is also in bytes)
                    Yii::$app->session->setFlash("info", Yii::t('web', 'File ảnh mặt sau dung lượng tối đa 8MB'));
                    return $this->render("accountInfomation.twig", ['account' => $account]);
                }


                try {
                    $filePathBackside = Utils::generatePath($ext);
                    S3Service::putObject($fileBucket, $_FILES["id_card_image_backside"]['tmp_name'], $filePathBackside);
                } catch (\Exception $exception) {
                    Yii::$app->session->setFlash("info", Yii::t('web', 'Có lỗi upload ảnh, vui lòng thử lại sau'));
                    return $this->render("accountInfomation.twig", ['account' => $account]);
                }
            }
            $msisdn = Utils::getMobileNumber($msisdn, Utils::MOBILE_GLOBAL);
            if (VtUserOtpBase::checkOTP($msisdn, $otp)) {
            } else {
                Yii::$app->session->setFlash("info", Yii::t('web', 'Mã OTP không hợp lệ, vui lòng lấy lại mã OTP'));
                return $this->render("accountInfomation.twig", ['account' => $account]);
            }
            // Deactive OTP sau moi lan su dung
            if ($otp) {
                VtUserOtpBase::deactiveAllOtp($msisdn);
            }

            //
            if ($isNew) {
                $account = new VtAccountInfomationBase();
            }

            $account->name = trim($postData['name']);
            $account->email = trim($postData['email']);
            $account->id_card_number = trim($postData['id_card_number']);
            $account->id_card_created_at = trim($postData['id_card_created_at']);
            $account->id_card_created_by = trim($postData['id_card_created_by']);
            $account->user_id = $this->userId;
            $account->msisdn = $msisdn;

            if ($filePathFrontside) {
                $account->id_card_image_frontside = $filePathFrontside;
                $account->bucket = $fileBucket;
            }
            if ($filePathBackside) {
                $account->id_card_image_backside = $filePathBackside;
                $account->bucket = $fileBucket;
            }

            //--
            if (!$account->created_at) {
                $account->created_at = date("Y-m-d H:i:s");
                $account->created_by = $this->userId;
            }
            //--
            $account->updated_at = date("Y-m-d H:i:s");
            $account->updated_by = $this->userId;
            $account->save(false);

            return $this->redirect("/account/upload");
        }

        return $this->render('accountInfomation.twig', ['account' => $account, 'msisdn' => $this->msisdn]);

    }


    public function actionReportUpload()
    {
        if (!$this->isValidUser()) {
            return $this->redirect('/auth/login');
        }
        $response = parent::actionReportUserUpload(Yii::$app->params['report.limit']);

        return $this->render("reportUserUpload.twig", [
            "responseData" => $response['data'],
            "limit" => Yii::$app->params['report.limit']
        ]);
    }

    public function actionLoadMoreReport()
    {

        $this->layout = false;

        if (!$this->isValidUser()) {
            return $this->redirect("/auth/login");
        }

        $offset = trim(Yii::$app->request->get('offset', 0));
        $limit = trim(Yii::$app->request->get('limit', Yii::$app->params['report.limit']));

        $response = parent::actionReportUserUploadHistory($limit, $offset);

        return $this->render("@app/views/partials/report.twig", [
            "responseData" => $response['data']
        ]);
    }

    public function actionGetDurationWatchedByIds()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::actionGetDurationWatchedByIds();
    }

}
