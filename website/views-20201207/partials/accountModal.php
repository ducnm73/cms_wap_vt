<?php

use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>
<div id="modalLogin" class="modal-vietnt" style="<?php echo ($show) ? "display:block" : "display:none" ?>">
    <div class="modal-content-vietnt">
        <h3 class="title"> <?= Yii::t('wap', 'Đăng nhập') ?> </h3>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'enableClientScript' => false,
                    'errorCssClass' => 'error-class',
                    'action' => ['auth/login'],
                    'fieldConfig' => [
                        'template' => "{input}",
                    ],
        ]);
        ?>
        <input type="hidden" id="urlBack" value="<?php echo $urlBack ?> "/>
        <span class="action-vietnt close" data-dismiss="close"><i class="fa icon-delete"></i></span>
        <div class="row-form text-error" id="errorLogin">

        </div>
        <div class="row-form">
            <input type="text" class="input-text" placeholder="<?= Yii::t('wap', 'Số điện thoại') ?>" name="LoginForm[username]" value="" id="username" autocomplete="off" />
        </div>
        <div class="row-form"> 
            <input type="password" name="LoginForm[password]" id="password" class="input-text" placeholder="<?= Yii::t('wap', 'Mật khẩu') ?>">
        </div>
        <div class="row-form" id="showCaptchaSignIn" <?php if ($countLoginFail < $configCaptchaShow) { ?> style="display: none;" <?php } ?>>
            <?=
            $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'options' => ['placeholder' =>  Yii::t('wap', 'Mã xác nhận') , 'id' => 'loginform-verifycode', 'class' => 'input-text capcha', 'autocomplete' => "off"],
                'captchaAction' => 'auth/captcha',
                // 'enableClientScript' => false,
                'template' => '
                	{input}
                    <div class="capcha-text">
                        <span class="btnRefreshCaptcha" onclick="refreshCaptcha()">
                            {image}
                            <i class="fa fa-repeat" ></i>
                        </span>
                    </div>
	            ',
            ])
            ?>
        </div>
        <div class="row-form">
            <input type="button" class="btn-login"  id="submitSignIn" value="<?= Yii::t('wap', 'Đăng nhập') ?>">
        </div>
        <?php ActiveForm::end(); ?>
<!--        <div class="row-form">-->
<!--            <a href="/auth/auth?authclient=facebook" class="btn-facebook">Facebook</a>-->
<!--            <a href="/auth/auth?authclient=google" class="btn-google">Google +</a>-->
<!--        </div>-->
        <p></p>
        <div class="row-form" align="center">
            <a href="javascript:void(0)" class="text-2 btnReg"><?= Yii::t('wap', 'Tạo tài khoản mới') ?></a>
        </div>
        <div class="row-form" align="center">
            <a href="javascript:void(0)" class="text-2 btnForgotPassword"><?= Yii::t('wap', 'Lấy mật khẩu') ?> ?</a>
        </div>			
    </div>
</div>
<div id="modalRegister" class="modal-vietnt">					
    <div class="modal-content-vietnt">
        <h3 class="title"> <?= Yii::t('wap', 'Đăng ký') ?> </h3>
        <span class="action-vietnt close" data-dismiss="close"><i class="fa icon-delete"></i></span>
        <div class="row-form">
            <?= Yii::t('web', 'Tính năng này hiện tại dành cho thuê bao Viettel, để đăng ký bạn vui lòng soạn') ?> <strong>DK</strong> <strong><?= Yii::t('wap', 'gửi') ?> 1515</strong> <?= Yii::t('wap', '(chọn Gói ngày) và đăng nhập lại hệ thống bằng số điện thoại Viettel của mình') ?>.
        </div>		
    </div>
</div>
<div id="forgotPasswordModal" class="modal-vietnt">	
    <div class="modal-content-vietnt">
        <h3 class="title"> <?= Yii::t('wap', 'Lấy mật khẩu') ?> </h3>
        <span class="action-vietnt close" data-dismiss="close"><i class="fa icon-delete"></i></span>
        <div class="row-form" style="text-align: center;">
            <?= Yii::t('web', 'Để lấy mật khẩu, quý khách vui lòng dùng thuê bao Viettel soạn MK gửi 1515. Trân trọng!') ?>
        </div>		
    </div>
</div>