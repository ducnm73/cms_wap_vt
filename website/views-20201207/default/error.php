
<div class="full-body-content">

    <div class="mdl-general" style="text-align: center">
        <h1 class="title"><?php echo $statusCode ?></h1>
        <p><?php Yii::t('web','Trang này không tồn tại') ?>.</p>
        <p><?php Yii::t('web','Vui lòng quay lại') ?> <a href="/"><?php Yii::t('web','trang chủ') ?></a></p>
    </div>
</div>