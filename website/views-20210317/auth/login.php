
<?php
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableClientValidation' => false,
    'enableClientScript' => false,
    'errorCssClass' => 'error-class',
    'action' => Url::current(),
    'fieldConfig' => [
        'template' => "{input}",
    ],
]); ?>
<div class="mdl-login">

    <div class="modal-content-vietnt">
        <h3 class="title"> <?= yii::t('wap','Đăng nhập') ?> </h3>
        <a href="/"><span class="action-vietnt close" data-dismiss="close"><i class="fa icon-delete"></i></span></a>
        <div class="row-form text-error">
            <?= $form->errorSummary($model, ['header' => '', 'class' => 'txt-warning']); ?>
        </div>
        <div class="row-form">
            <input type="text" class="input-text" placeholder="<?= yii::t('wap','Số điện thoại') ?>" name="LoginForm[username]">
        </div>
        <div class="row-form">
            <input type="password" class="input-text" placeholder="<?= yii::t('wap','Mật khẩu') ?>" name="LoginForm[password]">
        </div>
        <div class="row-form" id="showCaptchaSignIn" <?php if ($countLoginFail < $configCaptchaShow){ ?> style="display: none;" <?php } ?>>
            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'options' => ['placeholder' => yii::t('wap','Mã xác nhận'), 'id'=>'loginform-verifycode', 'class' => 'input-text capcha', 'autocomplete' => "off"],
                'captchaAction' => 'auth/captcha',
                // 'enableClientScript' => false,
                'template' => '
                	{input}
                    <div class="capcha-text">
                        <span class="btnRefreshCaptcha" onclick="refreshCaptcha()">
                            {image}
                            <i class="fa fa-repeat" ></i>
                        </span>
                    </div>
	            ',
            ]) ?>
        </div>
        <div class="row-form">
            <input type="submit" class="btn-login" value="<?= yii::t('wap','Đăng nhập') ?>">
        </div>
        <?php ActiveForm::end(); ?>
        <div class="row-form" align="center">
            <a href="javascript:void(0)" class="text-2 btnReg"><?= yii::t('wap','Tạo tài khoản mới') ?></a>
        </div>
        <div class="row-form" align="center">
            <a href="javascript:void(0)" class="text-2 btnForgotPassword"><?= yii::t('wap','Lấy mật khẩu') ?> ?</a>
        </div>
    </div>
</div>
<div id="modalRegister" class="modal-vietnt">
    <div class="modal-content-vietnt">
        <h3 class="title"> <?= Yii::t('web','Đăng ký')?> </h3>
        <span class="action-vietnt close" data-dismiss="close"><i class="fa icon-delete"></i></span>
        <div class="row-form">
            <?= yii::t('wap','Vui lòng đăng ký gói: Gửi ON (6 kip/ngày), ON7 (30 kip/7 ngày) hoặc ON 30 (100 kip/30 ngày) đến 1583. Để có thể xem thêm video nội dung đặc biệt, thú vị và hấp dẫn. Để lấy mật khẩu, gửi P đến 1583') ?>.
        </div>
    </div>
</div>
<div id="forgotPasswordModal" class="modal-vietnt">
    <div class="modal-content-vietnt">
        <h3 class="title"> <?= yii::t('wap','Lấy mật khẩu') ?> </h3>
        <span class="action-vietnt close" data-dismiss="close"><i class="fa icon-delete"></i></span>
        <div class="row-form">
            <?= Yii::t('wap','Để lấy mật khẩu đăng nhập, quý khách vui lòng soạn P gửi 1583. Trân trọng') ?>!
        </div>
    </div>
</div>