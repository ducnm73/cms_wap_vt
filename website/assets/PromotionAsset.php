<?php

namespace website\assets;

use yii\web\AssetBundle;

/**
 * Main wapsite application asset bundle.
 */
class PromotionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/promotion/css/reset.css?t=2112',
        '/promotion/css/style.css?t=2112',
        '/promotion/css/responsive.css?t=2112',
        '/promotion/css/coder-update.css?t=2112',
        '/css/jquery-ui.css'
    ];
    public $js = [
        '/promotion/js/jquery-1.12.0.min.js?t=2112',
		'/js/jquery-ui.js',
        '/promotion/js/main.js?t=2112',
    ];


}
