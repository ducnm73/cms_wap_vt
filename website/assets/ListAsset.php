<?php

namespace website\assets;

use yii\web\AssetBundle;

class ListAsset extends AssetBundle {

    public $css = [
        '/css/bootstrap.min.css',
        '/css/body_css.css?t=202103021647',
        '/css/myclip.css?t=202102241140',
        '/libs/owlcarousel2/owl.carousel.css',
        '/css/font-awesome.css',
        '/css/coder-update.css?t=20211228',
        '/libs/toastr/toastr.min.css',
        '/css/jquery-ui.css'
    ];
    public $js = [
        '/js/jquery-1.11.2.min.js',
		'/js/jquery-ui.js',
        '/js/myclip.js?t=4',
        '/js/react-dom-merge.js',
        '/libs/toastr/toastr.min.js',
        '/js/load-more.js',
        '/js/scripts.js?t=1',
        '/js/common.js',
        '/js/comment.js',
    ];
    public $depends = [
    ];

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub

        $staticsCssPath = \Yii::$app->params["statics.css.cache.path"];//  /statics/css/xxx/
        $staticsJsPath = \Yii::$app->params["statics.js.cache.path"];//  /statics/js/xxx/

        if ($staticsCssPath && $staticsJsPath) {
            //Replace js path
            for ($cI = 0; $cI < count($this->js); $cI++) {
                $this->js[$cI] = str_replace("/js/", $staticsJsPath, $this->js[$cI]);
            }
            //Replace css path
            for ($cI = 0; $cI < count($this->css); $cI++) {
                $this->css[$cI] = str_replace("/css/", $staticsCssPath, $this->css[$cI]);
            }

            if (\Yii::$app->params['bundle.css.enabled']) {
                $this->css = [
                    $staticsCssPath .'bootstrap.min.css?t=3007',
                    $staticsCssPath . 'bundle.website.min.css?t=3007',
                ];
            }
        } else {
            if (\Yii::$app->params['bundle.css.enabled']) {
                $this->css = [
                    '/css/bootstrap.min.css?t=2503',
                    '/css/bundle.website.min.css?t=2503'
                ];
            }
        }
    }

}
