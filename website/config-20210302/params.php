<?php

return [
    'cache.enabled' => false,
    'bundle.css.enabled'=> false,
    'app.source' => 'WEB',
    'app.home.limit' => 8,
    'app.page.limit' => 4,
    'app.page.film.limit' => 6,
    'app.search.suggestion.limit' => 24,
    'app.search.first_page.limit' => 3,
    'app.search.page.limit' => 24,
    'app.slideshow.limit' => 5,
    'slideshow.limit' => 5,
    'app.focus.limit' => 24,
    'show.suggestion.page' => false,
    'app.home.lazy.limit' => 12,
    'app.history.limit' => 100,
    'app.playlist.limit' => 30,
    'app.video.channel.limit' => 8, 
    'comment.minlength' => 0,
    // 'app.category.limit' => 12,
    'category.load.more.limit' => 12,
    'app.category.menu.limit' => 6,
    'category.load.more.limit' => 10,
    'package.filter' => [
            [
            "type" => '',
            "name" => 'Toàn bộ gói cước'
        ],
            [
            "type" => 'MOBITV',
            "name" => 'Gói VIP'
        ],
            [
            "type" => 'LIVE',
            "name" => 'Gói TV'
        ],
            [
            "type" => 'VOD',
            "name" => 'Gói Video'
        ],
            [
            "type" => 'FILM',
            "name" => 'Gói Phim'
        ],
            [
            "type" => 'KIDS',
            "name" => 'Gói Kids'
        ],
            [
            "type" => 'GAME',
            "name" => 'Gói Game'
        ],
    ],
    'setting.quality' => [
            [
            "name" => '360p',
            "vod_profile_id" => 7,
            "live_profile_id" => 12
        ],
            [
            "name" => '480p',
            "vod_profile_id" => 20,
            "live_profile_id" => 11
        ],
            [
            "name" => '720p',
            "vod_profile_id" => 21,
            "live_profile_id" => 19
        ]
    ]
];
