<?php

/**
 * Created by PhpStorm.
 * User: PHUMX
 * Date: 7/20/2016
 * Time: 10:57 AM
 */
return [
    '' => 'default/index',
    '/' => 'default/index',
    '/<id:\d+>' => 'default/index-promotion',
    'playlist/view/<id:\w+>' => 'default/playlist',
    'danh-sach-video/<id:\w+>' => 'default/video-channel',
    '/thinh-hanh' => 'default/trending',
    '/lich-su' => 'default/history',
    '/goi-cuoc' => 'default/list-package',
    'danh-sach-theo-doi' => 'default/list-follow-channel',
    '/xem-them/<id:\w+>/<slug>' => 'default/load-more',
    '/xem-them/<id:\w+>' => 'default/load-more',
    '/xem-them' => 'default/index',
    'channel/play-all/<id:\d+>' => 'default/play-channel-video',
    'channel/<id:\d+>' => 'channel/get-detail',
    'channel/update/<id:\d+>' => 'channel/update',
    // Nhom routing Video
    '/video/<playlist_id:\d+>/<id:\d+>/<slug>/<play_later>' => 'video/detail',
    '/video/<playlist_id:\d+>/<id:\d+>/<slug>' => 'video/detail',
    '/video/<id:\d+>/<slug>' => 'video/detail',
    '/video/<id:\d+>' => 'video/detail',
    '/nhac' => 'video/home-music',
    '/playlist/chi-tiet-playlist/<id:\d+>' => 'playlist/get-detail-playlist',
    '/playlist/xem-toan-bo-playlist/<id:\d+>' => 'playlist/detail-play-all',
    '/playlist/xem-toan-bo-danh-sach-xem-sau' => 'playlist/detail-watch-later',
    '/playlist/xem-toan-bo-lich-su' => 'playlist/detail-watch-history',
    'xem-sau' => 'default/video-watch-later',
    'gioi-thieu-dich-vu' => 'default/introduction',
    '/ho-tro' => 'default/support',
    '/lien-he' => 'default/contact',
    '/dieu-khoan-su-dung' => 'default/term-condition',
    '/operating-regulations' => 'default/rule',
    '/chinh-sach-rieng-tu' => 'default/privacy',
    '/video-cua-toi' => '/default/my-video',
    'default/error',
    'trung-thuong-iphone-x' => 'promotion/index',
    'danh-sach-the-loai' => 'category/get-category',
    'the-loai/<id:\d+>' => 'category/get-cate-child',
    'chi-tiet-the-loai/<id:\d+>' => 'category/get-cate-detail',
    'phat-tat-ca/<id:\d+>' => 'category/play-all-video',
    '/de-xuat' => 'default/recommend',
    '/tet' => 'default/tet-holiday',
    [
        'pattern' => 'tet1',
        'route' => 'video/detail',
        'defaults' => ['id' => '1397998', 'utm_source'=>'GA1', 'popup'=>1],
    ],
	 [
        'pattern' => 'hot',
        'route' => 'video/detail',
        'defaults' => ['id' => '1854519', 'utm_source'=>'GA', 'popup'=>1],
    ],
	 [
        'pattern' => 'phim',
        'route' => 'video/detail',
        'defaults' => ['id' => '1393888', 'utm_source'=>'GA1', 'popup'=>1],
    ],
    '/ad/<id:\d+>/<slug>' => 'video/detail',
    '/lien-ket' => 'account/check-link-social',
    '/event/main-index' => 'event/index',
    '/Worldcup' => 'default/world-cup',
    '/wcup' => 'default/world-cup',
    '/world-cup' => 'default/world-cup',
    '/doanh-thu' => 'account/report-upload',
    'activate'=>'tv/activate'

];
