cd $HOME/apps/meuclip_www

# rsync -R api/modules/v2/controllers/SuperappController.php ~/apps/meuclip_20210927_backup
# rsync -R backend/controllers/Report2021Controller.php ~/apps/meuclip_20210927_backup
# rsync -R backend/controllers/UserViewsController.php ~/apps/meuclip_20210927_backup
rsync -R backend/controllers/VtVideoController.php ~/apps/meuclip_20210927_backup
# rsync -R backend/models/UserViews.php ~/apps/meuclip_20210927_backup
# rsync -R backend/models/UserViewsSearch.php ~/apps/meuclip_20210927_backup
# rsync -Rr backend/views/report2021 ~/apps/meuclip_20210927_backup
rsync -R backend/views/vt-video/_video_form.php ~/apps/meuclip_20210927_backup
rsync -R common/controllers/SuperapiController.php ~/apps/meuclip_20210927_backup
rsync -R common/libs/VtFlow.php ~/apps/meuclip_20210927_backup
rsync -R common/models/VtVideoBase.php ~/apps/meuclip_20210927_backup
rsync -R common/modules/v2/controllers/SuperappController.php ~/apps/meuclip_20210927_backup
rsync -R common/modules/v2/controllers/VideoController.php ~/apps/meuclip_20210927_backup
rsync -R website/web/css/coder-update.css ~/apps/meuclip_20210927_backup
rsync -R website/web/js/video-detail.js ~/apps/meuclip_20210927_backup